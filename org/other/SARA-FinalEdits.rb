# SARA - Final Edits
class Game_Battler
  
  #--------------------------------------------------------------------------#
  # * alias-method :maxhp/maxmp/maxen/atk/def/spi/agi
  #--------------------------------------------------------------------------#     
  [:maxhp, :maxmp, :maxen, :atk, :def, :spi, :agi].each { |m|  
    module_eval( %Q(
    alias :sara_gmb_#{m.to_s} #{m} unless $@
    def #{m.to_s}( *args, &block )
      @cached_#{m.to_s} ||= sara_gmb_#{m.to_s}( *args, &block )
      return @cached_#{m.to_s}  
    end 
    
    alias :sara_gmb_#{m.to_s}_set #{m}= unless $@
    def #{m.to_s}=( new_value )
      sara_gmb_#{m.to_s}_set( new_value )
      @cached_#{m.to_s} = nil if @cached_#{m.to_s} != new_value
    end 
    ) )
  }  
  
  def item_growth_effect( user, item )
    if item.parameter_type > 0 and item.parameter_points != 0
      case item.parameter_type
      when 1  # Maximum HP
        @maxhp_plus += item.parameter_points
        @cached_maxhp = nil
      when 2  # Maximum MP
        @maxmp_plus += item.parameter_points
        @cached_maxmp = nil
      when 3  # Attack
        @atk_plus += item.parameter_points
        @cached_atk = nil
      when 4  # Defense
        @def_plus += item.parameter_points
        @cached_def = nil
      when 5  # Spirit
        @spi_plus += item.parameter_points
        @cached_spi = nil
      when 6  # Agility
        @agi_plus += item.parameter_points
        @cached_agi = nil
      end
    end
  end
  
  #alias :ttt_maxhp :sara_gmb_maxhp unless $@
  #def sara_gmb_maxhp( *args, &block )
  #  puts "Maxhp"
  #  return ttt_maxhp()
  #end
  
  #--------------------------------------------------------------------------#
  # * new-method :clear_stat_cache
  #--------------------------------------------------------------------------#   
  def clear_stat_cache()
    @cached_maxhp = nil
    @cached_maxmp = nil
    @cached_maxen = nil
    @cached_atk   = nil
    @cached_def   = nil
    @cached_spi   = nil
    @cached_agi   = nil
    
    @character_need_refresh = true
  end
  
  #--------------------------------------------------------------------------#
  # * alias-method :add_state
  #--------------------------------------------------------------------------#   
  alias :sara_gmb_add_state :add_state unless $@
  def add_state( *args, &block )
    old_states = self.states
    sara_gmb_add_state( *args, &block )
    clear_stat_cache() unless self.states == old_states
  end
  
  #--------------------------------------------------------------------------#
  # * alias-method :remove_state
  #--------------------------------------------------------------------------#   
  alias :sara_gmb_remove_state :remove_state unless $@
  def remove_state( *args, &block )
    old_states = self.states
    sara_gmb_remove_state( *args, &block )
    clear_stat_cache() unless self.states == old_states
  end
  
end  

class Game_Actor < Game_Battler
  
  #--------------------------------------------------------------------------#
  # * alias-method :level_up
  #--------------------------------------------------------------------------#   
  alias :ygg_gb_level_up :level_up unless $@
  def level_up( *args, &block )
    clear_stat_cache()
    ygg_gb_level_up( *args, &block )
    clear_stat_cache()
  end
  
  #--------------------------------------------------------------------------#
  # * alias-method :level_down
  #--------------------------------------------------------------------------#   
  alias :ygg_gb_level_down :level_down unless $@
  def level_down( *args, &block )
    clear_stat_cache()
    ygg_gb_level_down( *args, &block )
    clear_stat_cache()
  end
  
  alias :sara_gma_final_setup :setup unless $@
  def setup( *args, &block )
    sara_gma_final_setup( *args, &block )
    create_maxen_table()
    clear_stat_cache()
  end
  
end  
