# SkillDraw - SARA
module SARA
  module SkillDraw
  INCL = %Q(
  #--------------------------------------------------------------------------#
  # overwrite method :draw_item
  #--------------------------------------------------------------------------#
  def draw_item(index)
    rect = item_rect(index)
    self.contents.clear_rect(rect)
    skill = @data[index]
    return if skill == nil
    enabled = enabled?(skill)
    rect2 = rect.clone
    rect2.x += 28; rect2.y += 14 ; rect2.height = 8 ; rect2.width /= 4
    
    #draw_grad_bar
    draw_round_grad_bar(rect2.clone, skill.skl_unlocklevel-1, skill.skl_maxlevel-1, 
      normal_color, Color.new(200, 200, 200), Color.new(20, 20, 20),
      2, enabled)
    draw_round_grad_bar(rect2.clone, skill.skl_level-1, skill.skl_maxlevel-1, 
      mp_gauge_color1, mp_gauge_color2, Color.new(20, 20, 20),
      2, enabled)
    # //  
    if IEO::SKILL_LEVEL::SKILL_MODE == 1 
      rect3 = rect2.clone
      rect3.x += rect3.width+16 ; rect3.width -= 16  
      bc = skill.level_skl_exp ; bm = skill.next_skl_expr
      if (bm > 0) && !skill.unlocked_level?( skill.skl_maxlevel )
        draw_grad_bar(rect3.clone, bc, bm, 
          hp_gauge_color1, hp_gauge_color2, Color.new(20, 20, 20),
          2, enabled)  
      end   
      rect3.height = WLH  ; rect3.y -= 16
      #self.contents.draw_text(rect3, sprintf("%s/%s", bc, bm))
    end    
    te = draw_obj_name( skill, rect.clone, enabled )
    rect4 = rect.clone 
    rect4.y -= 6 ; rect4.height = WLH ; rect4.x += te.width+28
    draw_obj_level( skill, rect4.clone, 0, enabled )  
    if $imported["IEO-CustomSkillCosts"]
      draw_obj_cost( skill, rect.clone, enabled )
    else
      rect.width -= 8 ; self.contents.draw_text( rect.clone, skill.mp_cost, 2 )
    end  
  end
  
  #--------------------------------------------------------------------------#
  # new method :draw_obj_level
  #--------------------------------------------------------------------------#  
  def draw_obj_level(obj, rect, align=0, enabled=true)
    rect.width -= 8
    self.contents.font.size = 14
    self.contents.font.color = normal_color
    self.contents.font.color.alpha = enabled ? 255 : 128
    self.contents.draw_text(rect, sprintf("%s%d", Vocab.level_a, obj.skl_level), align)
  end
  )
  end  
end  

#==============================================================================#
# Window_Skill
#==============================================================================#
class Window_Skill < Window_Selectable
      
  module_eval( SARA::SkillDraw::INCL )
  
end

#==============================================================================
# Window_Action
#==============================================================================
class Window_Action < Window_Selectable
  
  module_eval( SARA::SkillDraw::INCL )
  
end  
