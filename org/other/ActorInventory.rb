# ActorInventory
#~ # // Cacao Item Inventory SRPG patch by IceDragon
#~ class Game_Battler
#~   def has_item?( item ) ; false ; end
#~ end

#~ class Game_Actor
#~   
#~   def item_can_use?( item )
#~     return false unless item.is_a?( RPG::Item )
#~     return false if item_number( item ) == 0
#~     if $game_temp.in_battle or $game_temp.in_srpg
#~       if item.nomove?    # 移動不可アイテムならば移動済みの場合に使用不可
#~         return false if $scene.event != nil and $scene.event.move_end
#~       end
#~       return item.battle_ok?
#~     else
#~       return item.menu_ok?
#~     end
#~   end

#~   def has_item?( item )
#~     return false unless item.is_a?( RPG::Item )
#~     return item_number( item ) == 0
#~   end
#~   
#~ end

#~ class Window_Item
#~   
#~   attr_accessor :target
#~   
#~   def item_can_use?( obj )
#~     return false unless obj.is_a?( RPG::Item )
#~     return @target.item_can_use?( obj )
#~   end
#~   
#~ end

#~ class Scene_Srpg < Scene_Base
#~   
#~   def open_item_window( event )
#~     @item_window.target = event.unit
#~     @item_window.refresh
#~     @item_window.open()
#~     @item_window.active = true
#~     @help_window.visible = true
#~   end
#~   
#~   def update_item_window
#~     if Input.trigger?(Input::C)     # Ｃボタンが押された
#~       if not @item_window.item_can_use?(@item_window.item)
#~         Sound.play_buzzer
#~       else
#~         Sound.play_decision
#~         @event.unit.action.set_item(@item_window.item.id)
#~         change_scene_state(4)         # シーンの状態をアイテム位置選択に変更
#~       end
#~     elsif Input.trigger?(Input::B)  # Ｂボタンが押された
#~       Sound.play_cancel
#~       change_scene_state(60)        # シーンの状態をキャラコマンドに変更
#~     end
#~   end
#~   
#~   def change_scene_state( id, index=0 )
#~     $game_srpg.scene_state = id
#~     case id
#~     when 0    # neutral
#~       @menu_window.close_ex                 # メニューウィンドウを閉じる
#~       @command_window.close_ex              # コマンドウィンドウを閉じる
#~       @unit_window.close_ex                 # ユニットウィンドウを閉じる
#~       @spriteset.clear_flash                # エリア表示をクリア
#~     when 1    # select state moving position
#~       @command_window.close_ex              # コマンドウィンドウを閉じる
#~       make_movable_cell                     # 移動可能範囲を作成
#~       check_effect_cell                     # 効果範囲の作成
#~       @spriteset.refresh_area               # エリア表示の更新
#~       @message_mini_window.refresh("Select a Destination", 60)
#~     when 2..4 # attack/skill/item conditions
#~       check_attack_cell                     # 攻撃可能範囲の作成
#~       check_effect_cell                     # 効果範囲の作成
#~       @skill_window.close_ex                # スキルウィンドウを閉じる
#~       @item_window.close_ex                 # アイテムウィンドウを閉じる
#~       @help_window.visible = false          # ヘルプウィンドウを非表示にする
#~       @spriteset.refresh_area               # エリア表示の更新
#~     when 5    # select orientation
#~       focus_event(@event)
#~       check_attack_cell                     # 攻撃可能範囲の作成
#~       check_effect_cell                     # 効果範囲の作成
#~       @spriteset.refresh_area               # エリア表示の更新
#~       @message_mini_window.refresh("Set Face Direction", 60)
#~     when 50   # status menu command
#~       open_menu_window(index)
#~       @unit_window.close_ex                 # ユニットウィンドウを閉じる
#~       @info_window.close
#~       @message_mini_window.close
#~     when 51   # end turn confirmation
#~       @menu_window.close_ex
#~       @message_mini_window.refresh("End your turn?", 999999)
#~     when 52,54  # list of selected state or target state
#~       @menu_window.close_ex
#~       @unit_window.refresh
#~       @last_focus_x = @cursor_x
#~       @last_focus_y = @cursor_y
#~     when 53   # information state strategy
#~       @info_window.refresh(nil)
#~       @menu_window.active = false
#~     when 55   # waiting confirmation
#~       @command_window.close_ex              # close command window
#~       @message_mini_window.refresh("Set this unit to wait?", 999999)
#~     when 60   # command state character
#~       open_command_window(@event, index)
#~       @skill_window.close_ex
#~       @item_window.close_ex
#~       @info_window.close
#~       @help_window.visible = false
#~     when 61   # state skill window
#~       open_skill_window(@event)             # スキルウィンドウの作成
#~       @command_window.close_ex              # コマンドウィンドウを閉じる
#~       @spriteset.clear_flash                # エリア表示をクリア
#~     when 62   # item window status
#~       open_item_window(@event)              # アイテムウィンドウを開く
#~       @command_window.close_ex              # コマンドウィンドウを閉じる
#~       @spriteset.clear_flash                # エリア表示をクリア
#~     when 63   # state unit status
#~       @info_window.refresh(@event)
#~       @command_window.active = false        # コマンドウィンドウを非アクティブに
#~     when 70   # check the state action
#~       @skill_window.close_ex                # スキルウィンドウを閉じる
#~       @item_window.close_ex                 # アイテムウィンドウを閉じる
#~       @help_window.visible = false          # ヘルプウィンドウを非表示にする
#~       @spriteset.refresh_area               # エリア表示の更新
#~       refresh_status_window_attack(@cursor_x, @cursor_y)  # ステータスウィンドウの更新
#~       @message_mini_window.refresh("Confirm action?", 999999)
#~     end
#~   end
#~  
#~   def execute_action_obj(targets)
#~     level = 0
#~     animation_lock = false
#~     obj = @event.unit.action.obj
#~     if obj.double_anime > 0         # ダブルアニメーション
#~       @event.animation_id = obj.double_anime
#~       wait_for_animation              # アニメーション終了待ち
#~     end
#~     for event in targets
#~       @event.back_attack?(event)    # 背後を取っているかチェック
#~       if obj.is_a?(RPG::Skill)      # スキルの場合
#~         @event.unit.mp -= @event.unit.calc_mp_cost(obj) if level == 0 # MP消費
#~         if obj.id == 2                              # 会話
#~           talk(event)
#~           return @event.unit.level  # 自分のレベルを返す
#~         elsif obj.id == 3                           # 防御
#~           execute_action_guard
#~           return @event.unit.level-5  # 自分のレベルを返す
#~         end
#~         if event.unit.reflect?                      # 魔法反射
#~           if obj.spi_f > 0 and not obj.physical_attack and
#~              not obj.pierce? and @event.id != event.id
#~             event.animation_id = TSRPG::ANIME_REFLECT
#~             wait_for_animation      # アニメーション終了待ち
#~             level += @event.unit.level
#~             targets.push(@event)
#~             next
#~           end
#~         end
#~         event.unit.skill_effect(@event.unit, obj)   # スキルの効果適用
#~       else                          # アイテムの場合
#~         event.unit.consume_item(obj) if level == 0 # アイテム消費
#~         event.unit.item_effect(@event.unit, obj)    # アイテムの効果適用
#~       end
#~       if event.unit.hp_damage > 0
#~         steal(event) if rand(100) < obj.steal_rate  # 盗む
#~         knock_back(event, obj)                      # ノックバック
#~       end
#~       @event.move_end = true if obj.nomove?         # 移動不可
#~       event.reset if obj.for_dead_friend?           # 蘇生処理
#~       animation_lock = set_animation(event, @event, obj) unless animation_lock
#~       @message_mini_window.refresh(obj.name)        # スキル（アイテム）名表示
#~       set_popup(event, @event)
#~       event.damage(@event)
#~       event.refresh_state                           # キャラクターの状態を更新
#~       if obj.common_event_id > 0                    # コモンイベント
#~         $game_temp.common_event_id = obj.common_event_id
#~         $game_temp.common_event_user = @event.id
#~       end
#~       level += (event.unit.level == 0 ? @event.unit.level : event.unit.level)
#~     end
#~     return level / targets.size  # 行動対象の平均レベルを返す
#~   end

#~ end  

