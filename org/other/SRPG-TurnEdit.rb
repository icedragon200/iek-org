# SRPG - Turn Edit
class Game_Srpg
  
  def change_turn
    $game_temp.unit_back_attack = false   
    for event in unit_list do event.refresh_turn end
    if enemy_turn?                        
      $game_troop.increase_turn            
      $game_variables[TSRPG::TURN_COUNT] = $game_troop.turn_count
      @scene_state = 0                     
      $game_variables[TSRPG::CHANGE_TURN] = 1
      enemy_end_turn()
      player_start_turn()
    else                                  
      @scene_state = 100                  
      $game_variables[TSRPG::CHANGE_TURN] = 2
      player_end_turn()
      enemy_start_turn()
    end
    @need_refresh = true
  end
  
  def player_start_turn()
    alive_actor_list.each { |e| 
      add_popup( e.real_x, e.real_y,
        e.unit.regen_en().to_s, Color.new( 200, 208, 192 ) )
    }
    do_event_popstacks( alive_actor_list )
  end
  
  def enemy_start_turn()
    alive_enemy_list.each { |e| 
      add_popup( e.real_x, e.real_y,
        e.unit.regen_en().to_s, Color.new( 200, 208, 192 ) )
    }
    do_event_popstacks( alive_enemy_list )
  end
  
  def player_end_turn()
    
  end
  
  def enemy_end_turn()
    
  end
  
  def do_event_popstacks( eventlist )
    eventlist.each { |e|
      e.unit.__popstack.each { |poparra|
        case poparra[0]
        when :hp
          text  = poparra[1].abs.to_s
          color = poparra[1] > 0 ? (event.enemy_flag ? 
           TSRPG::COLOR_ACTOR_HP_DAMAGE : TSRPG::COLOR_ENEMY_HP_DAMAGE) : 
           TSRPG::COLOR_HP_GAIN
        when :mp
          text  = poparra[1].abs.to_s
          color = poparra[1] > 0 ? (event.enemy_flag ? 
           TSRPG::COLOR_ACTOR_MP_DAMAGE : TSRPG::COLOR_ENEMY_MP_DAMAGE) : 
           TSRPG::COLOR_MP_GAIN
        when :en
          text  = poparra[1].abs.to_s
          color = poparra[1] > 0 ? Color.new( 198, 128, 128 ) : Color.new( 200, 208, 192 )
        when :ammo
          text  = poparra[1].abs.to_s
          color = poparra[1] > 0 ? Color.new( 128, 128, 128 ) : Color.new( 178, 172, 232 )
        else ; next  
        end 
        add_popup( e.real_x, e.real_y, text, color )
        $scene.wait(12)
      }
      e.unit.__popstack.clear()
    }
  end
  
end  
