# MapCursor
#~ class ScreenObject
#~   
#~   attr_accessor :x, :y
#~   
#~   def initialize()
#~     @x, @y = 0, 0
#~   end
#~   
#~   def moveto( x, y )
#~     self.x, self.y = x, y
#~   end
#~   
#~   def map_pos()
#~     return ISS::Pos.new( @x, @y )
#~   end
#~   
#~   #--------------------------------------------------------------------------#
#~   # * new-method :offset_x
#~   #--------------------------------------------------------------------------#         
#~   def offset_x() ; return ($camera.nil?() ? 0 : $camera.screen_x) ; end
#~   
#~   #--------------------------------------------------------------------------#
#~   # * new-method :offset_y
#~   #--------------------------------------------------------------------------#         
#~   def offset_y() ; return ($camera.nil?() ? 0 : $camera.screen_y) ; end
#~   
#~   #--------------------------------------------------------------------------#
#~   # * new-method :screen_x
#~   #--------------------------------------------------------------------------#         
#~   def screen_x() 
#~     return Integer((@x * 32) - offset_x) 
#~   end
#~   
#~   #--------------------------------------------------------------------------#
#~   # * new-method :screen_y
#~   #--------------------------------------------------------------------------#         
#~   def screen_y() 
#~     return Integer((@y * 32) - offset_y)
#~   end
#~   
#~ end

#~ class TargetCursor < ScreenObject
#~   
#~   attr_accessor :disabled
#~   attr_reader :opacity
#~   
#~   def initialize
#~     super()
#~     @opacity = 255
#~   end
#~   
#~   def opacity=( new_opacity )
#~     @opacity = [[new_opacity, 0].max, 255].min
#~   end  
#~   
#~ end

#~ class MapCursor < ScreenObject

#~   def update()
#~     x, y = ISS::Mouse.pos
#~     @x = ($camera.screen_x / 32) + (x / 32)
#~     @y = ($camera.screen_y / 32) + (y / 32)
#~   end  
#~     
#~ end

#~ class BaseCursor < ISS::KeyCursor
#~   
#~ end


#~ $target = TargetCursor.new()
#~ $map_cursor = MapCursor.new()
#~ $main_cursor = BaseCursor.new()
#~ $cursor_sprite = Sprite.new()
#~ $cursor_sprite.bitmap = Cache.system("Cursor")
#~ $cursor_sprite.z = 9999
#~ $target.disabled = true

#~ module Graphics
#~   
#~   class << self
#~     
#~     alias :mpcr_update :update unless $@
#~     def update()
#~       mpcr_update()
#~       $main_cursor.update()
#~       $map_cursor.update()
#~       $cursor_sprite.x = $target.screen_x()
#~       $cursor_sprite.y = $target.screen_y()
#~       $cursor_sprite.visible = $scene.is_a?(Scene_Map)
#~       $cursor_sprite.opacity = $target.opacity
#~       unless $game_player.nil?()
#~         if $main_cursor.left_click?()
#~           pos = $map_cursor.map_pos
#~           if EVTOOLS.adjacent?( pos, $game_player )
#~             $game_player.turn_to_xy( pos.x, pos.y )
#~             $game_player.check_action_event()            
#~           end  
#~          
#~         elsif $main_cursor.left_press?() 
#~           $target.disabled = false
#~           pos ||= $map_cursor.map_pos
#~           $target.x, $target.y = pos.x, pos.y
#~         end  
#~         if $main_cursor.right_click?()
#~           $target.disabled = true
#~         end  
#~       end 
#~       $target.disabled ? $target.opacity -= 255/60.0 : $target.opacity += 255/60.0
#~       if $scene.is_a?(Scene_Map) && !$target.disabled
#~         $game_player.move_toward_xy( $target.x, $target.y ) if !$game_player.moving?()
#~       end  
#~     end
#~     
#~   end  
#~   
#~ end  
