# SRPG - Projectile
class Game_Srpg
  
  attr_accessor :projectiles
  attr_accessor :new_projectiles
  
  alias :sara_en_gmsrpg_setup :setup unless $@
  def setup( *args, &block )
    sara_en_gmsrpg_setup( *args, &block )
    setup_projectiles()
  end
  
  alias :sara_en_gmsrpg_engage_setup :engage_setup unless $@
  def engage_setup( *args, &block )
    sara_en_gmsrpg_engage_setup( *args, &block )
    setup_projectiles()
  end
  
  def setup_projectiles()
    @projectiles ||= []
    @new_projectiles ||= []
    @projectiles.clear()
    @new_projectiles.clear()
  end
  
  def add_projectile()
    prj = SARA::TSRPG::Handlers::Projectile.new()
    @projectiles << prj
    @new_projectiles << prj
    return prj
  end
  
  alias :sara_en_gmsrpg_update :update unless $@
  def update( *args, &block )
    sara_en_gmsrpg_update( *args, &block )
    update_projectiles()
  end
  
  def update_projectiles()
    @projectiles = @projectiles.inject([]) { |r, prj|
      prj.update() ; r << prj unless prj.complete?() ;  r
    } unless @projectiles.empty?()
  end
  
  def projectiles_complete?()
    @projectiles.each { |prj| return false unless prj.complete? }
    return true
  end
  
end

class SARA::TSRPG::Handlers::Projectile
  
  attr_accessor :x, :y, :z # // Standard X, Y, Z
  attr_accessor :rx, :ry   # // Real X, Y
  attr_accessor :ox, :oy   # // Offset X, Y (Screen)
  attr_accessor :tx, :ty   # // Target X, Y
  attr_accessor :opacity
  attr_accessor :zoom_x, :zoom_y
  attr_accessor :current_action
  
  def initialize()
    @x, @y, @z = 0, 0, 200
    @rx, @ry   = 0.0, 0.0
    @ox, @oy   = 0, 0
    @tx, @ty   = 0, 0
    @jump_peak = 0
    @jump_count= 0
    @stop_count= 0
    @opacity   = 255
    @zoom_x, @zoom_y = 1.0, 1.0
    @current_action = []
    @completed = false
    @move_speed = 3
    
    @action_obj = $scene.action_obj
    @user = $scene.user
    @target = $scene.target
    @targets = $scene.targets
    @original_user = $scene.original_user
    @original_target = $scene.original_target
    @original_targets = $scene.original_targets
  end
  
  def target_reached?()
    return (@rx == @tx * 256 && @ry == @ty * 256)
  end
  
  def complete?()
    return (@completed)
  end
  
  def moving?()
    return (@rx != @x * 256 or @ry != @y * 256)
  end
  
  def jumping?()
    return @jump_count > 0
  end
  
  def stopping?()
    return (!(moving? || jumping?))
  end
  
  def moveto( x, y, quickset=false )
    @x, @y   = x, y
    @rx, @ry = @x*256, @y*256 if quickset
  end
  
  def set_target_xy( x, y )
    @tx, @ty = x, y
  end
  
  def jump( x_plus, y_plus, peak=10 )
    #if x_plus.abs > y_plus.abs            
    #  x_plus < 0 ? turn_left : turn_right
    #elsif y_plus.abs > x_plus.abs         
    #  y_plus < 0 ? turn_up : turn_down
    #end
    @x += x_plus
    @y += y_plus
    distance = Math.sqrt(x_plus * x_plus + y_plus * y_plus).round
    @jump_peak = peak + distance - @move_speed
    @jump_count = @jump_peak * 2
    @stop_count = 0
  end
  
  def jump_to_xy( tx, ty )
    jump( tx-self.x, ty-self.y )
  end
  
  def screen_x()
    return ($game_srpg.adjust_x(@rx) + 8007) / 8 - 1000 #+ 16
  end
  
  def screen_y()
    y = ($game_srpg.adjust_y(@ry) + 8007) / 8 - 1000 #+ 32
    if @jump_count >= @jump_peak ; n = @jump_count - @jump_peak
    else                         ; n = @jump_peak - @jump_count
    end
    return y - (@jump_peak * @jump_peak - n * n) / 2
  end
  
  def screen_z()
    return @z
  end
  
  def update()
    if jumping?
      update_jump()
    elsif moving?
      update_move()
    end  
    if target_reached?()
      #self.opacity = [self.opacity-(255/60.0), 0].max
      update_current_action() 
    end  
    @completed = (target_reached?() && @current_action.empty?())
  end
  
  def update_jump()
    @jump_count -= 1
    @rx = (@rx * @jump_count + @x * 256) / (@jump_count + 1)
    @ry = (@ry * @jump_count + @y * 256) / (@jump_count + 1)
  end
  
  def update_move()
    distance = 2 ** @move_speed   # Convert to movement distance
    #distance *= 2 if dash?        # If dashing, double it
    @rx = [@rx - distance, @x * 256].max if @x * 256 < @rx
    @rx = [@rx + distance, @x * 256].min if @x * 256 > @rx
    @ry = [@ry - distance, @y * 256].max if @y * 256 < @ry
    @ry = [@ry + distance, @y * 256].min if @y * 256 > @ry
    #if @rx > @x   ; @rx = [@rx-@move_rate, @x * 256].max
    #elsif @rx < @x ; @rx = [@rx+@move_rate, @x * 256].min
    #end
    #if @ry > @y   ; @ry = [@ry-@move_rate, @y * 256].max
    #elsif @ry < @y ; @ry = [@ry+@move_rate, @y * 256].min
    #end
  end  
    
  include ::SARA::TSRPG::ACTION

#~   def __action_obj()
#~     return $scene.action_obj
#~   end  
#~       
#~   def __action_user()
#~     return $scene.user
#~   end  
#~       
#~   def __action_target()
#~     return $scene.target
#~   end
#~       
#~   def __action_targets()
#~     return $scene.targets
#~   end
#~       
#~   def __action_original_user()
#~     return $scene.original_user
#~   end  
#~       
#~   def __action_original_target()
#~     return $scene.original_target
#~   end  
#~       
#~   def __action_original_targets() 
#~     return $scene.original_targets 
#~   end  
#~       
#~   def __set_target( new_target )
#~     $scene.target = new_target
#~   end
#~       
#~   def __set_user( new_user )
#~     $scene.user = new_user
#~   end
#~     
#~   def __set_targets( new_targets )
#~     $scene.targets = new_targets
#~   end
      
  def __animation_lock()
    return $scene.animation_lock
  end
      
  def __animation_lock=( new_lock )
    $scene.animation_lock = new_lock
  end
     
  def __action_update_basic()
    $scene.update_basic()
  end
  
  def __action_spriteset()
    return $scene.spriteset
  end
  
  def set_animation( *args, &block )
    $scene.set_animation( *args, &block )
  end
  
  def wait_for_animation()
    #$scene.wait_for_animation()
  end  
  
  def wait( *args, &block )
    $scene.wait( *args, &block )
  end
  
  def damage_event( *args, &block )
    $scene.damage_event( *args, &block )
  end
  
end

class SARA::TSRPG::Sprite_Projectile < ::Sprite_Base
  
  def initialize( viewport, handle )
    super( viewport )
    @handle = handle
    self.bitmap = Cache.character( "projectileball" ) # Bitmap.new( 32, 32 )
  end
  
  def dispose()
    self.bitmap.dispose()
    super()
  end
  
  def complete?()
    return (!animation?() && @handle.complete?())
  end
  
  def update
    super()
    self.x = @handle.screen_x
    self.y = @handle.screen_y
    self.z = @handle.screen_z
    self.opacity = @handle.opacity
    self.zoom_x  = @handle.zoom_x
    self.zoom_y  = @handle.zoom_y
  end
  
end

class Spriteset_Srpg
  
  alias :sara_en_sps_initialize :initialize unless $@
  def initialize( *args, &block )
    sara_en_sps_initialize( *args, &block )
    create_projectiles()
  end
  
  def create_projectiles()
    @projectiles = []
    $game_srpg.new_projectiles.clear()
    $game_srpg.projectiles.each { |p| add_projectile( p ) }
  end
  
  def add_projectile( handle )
    @projectiles << ::SARA::TSRPG::Sprite_Projectile.new( @viewport1, handle )
  end
  
  alias :sara_en_sps_dispose :dispose unless $@
  def dispose( *args, &block )
    sara_en_sps_dispose( *args, &block )
    dispose_projectiles()
  end
  
  def dispose_projectiles()
    @projectiles.each { |p| p.dispose() }
    @projectiles.clear()
  end
  
  alias :sara_en_sps_update :update unless $@
  def update( *args, &block )
    sara_en_sps_update( *args, &block )
    update_projectiles() 
  end
  
  def update_projectiles
    return if @projectiles.nil?()
    $game_srpg.new_projectiles.each { |p| add_projectile( p ) }
    $game_srpg.new_projectiles.clear()
    @projectiles = @projectiles.inject([]) { |r, prj|
      prj.update() ; prj.dispose() if prj.complete?() ; 
      r << prj unless prj.disposed?() ; r } unless @projectiles.empty?()
  end
  
end
