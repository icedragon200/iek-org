# SARA - Ammo System
module SARA ; end
#==============================================================================#
# ** SARA::Ammo_Handler
#==============================================================================#
class SARA::Ammo_Handler
  
  attr_reader :amount
  attr_writer :max
  
  def initialize( parent, obj )
    @parent = parent
    @obj    = obj
    @max    = 0
    @amount = self.max()
  end
  
  def value()
    return self.amount
  end
  
  def value=( val )
    self.amount = val
  end
  
  def max_bonus()
    return 0
  end
  
  def max()
    return @obj.ammo + @max + max_bonus()
  end
  
  def amount=( val )
    @amount = [[val, max()].min(), 0].max
  end
  
  def to_s()
    return "#{@obj.name}: Ammo #{self.amount}/#{self.max}"
  end
  
end

#==============================================================================#
# ** RPG::Skill
#==============================================================================#
class RPG::Skill
  
  attr_accessor :ammo
  
  def sara_ammo_skillcache()
    @ammo = nil
    self.note.split( /[\r\n]+/ ).each { |line|
      case line
      when /<AMMO:[ ](\d+)>/i
        @ammo = $1.to_i
      end  
    }
  end
  
end

#==============================================================================#
# ** RPG::Item
#==============================================================================#
class RPG::Item
  
  attr_accessor :recover_ammo
  
  def sara_ammo_itemcache()
    @recover_ammo = 0
    self.note.split( /[\r\n]+/ ).each { |line|
      case line
      when /<AMMO:[ ]([\+\-]\d+)>/i
        @recover_ammo = $1.to_i
      end  
    }
  end
  
end  

#==============================================================================#
# ** Game_Battler
#==============================================================================#
class Game_Battler
  
  attr_accessor :ammo_damage
  
  alias :sara_ammo_gmbt_initialize :initialize unless $@
  def initialize( *args, &block )
    @ammo_damage = 0
    sara_ammo_gmbt_initialize( *args, &block )
    @ammos = []
  end
  
  def setup_ammo( skill_id )
    skill = self.actor? ? @skl_skills[skill_id] : $data_skills[skill_id]
    if (!skill.nil?() && @ammos[skill_id].nil?() && !skill.ammo.nil?())
      @ammos[skill_id] = SARA::Ammo_Handler.new( self, skill )
    end
  end
  
  def get_ammo( skill_id )
    @ammos[skill_id]
  end
  
  def restore_ammo( id, amount )
    change_ammo( :add, id, amount )
  end
  
  def restore_all_ammo( amount )
    change_ammo( :add, -1, amount )
  end
  
  def max_all_ammo()
    change_ammo( :max, -1, 0 )
  end
  
  def change_ammo( type, id, amount )
    ammos = []
    case id
    when -1
      ammos += @ammos
    else  
      ammos << @ammos[id]
    end  
    ammos.compact!()
    case type
    when :set
      ammos.each { |a| a.value = amount }
    when :add
      ammos.each { |a| a.value += amount }
    when :sub
      ammos.each { |a| a.value -= amount }
    when :set_percent
      ammos.each { |a| a.value = amount * a.value / a.max }
    when :add_percent
      ammos.each { |a| a.value += amount * a.value / a.max }
    when :sub_percent  
      ammos.each { |a| a.value -= amount * a.value / a.max }
    when :max
      ammos.each { |a| a.value = a.max }
    when :empty
      ammos.each { |a| a.value = 0 }
    end  
  end
  
  alias :sara_ammo_gmbt_item_effectiveq :item_effective? unless $@
  def item_effective?( user, item )
    return true if item.recover_ammo > 0
    sara_ammo_gmbt_item_effectiveq( user, item )    
  end
  
  alias :sara_ammo_gmbt_item_effect :item_effect unless $@
  def item_effect( user, item )
    sara_ammo_gmbt_item_effect( user, item )
    unless @missed || @evaded || @skipped || item.nil?()
      self.restore_all_ammo( item.recover_ammo ) if item.recover_ammo > 0
    end  
  end
  
end

#==============================================================================#
# ** Game_Enemy
#==============================================================================#
class Game_Enemy
  
  alias :sara_ammo_gmen_initialize :initialize unless $@
  def initialize( *args, &block )
    sara_ammo_gmen_initialize( *args, &block )
    skills.each { |s| setup_ammo( s.id ) }
  end
  
  def skills()
    return enemy.actions.inject([]) { |r, a|
      r << $data_skills[a.skill_id] if a.kind == 1 ; r }
  end
  
end

#==============================================================================#
# ** Game_Actor
#==============================================================================#
class Game_Actor
  
  alias :sara_ammo_gmact_setup :setup unless $@
  def setup( actor_id )
    @ammos = []
    sara_ammo_gmact_setup( actor_id )
    skills.each { |s| setup_ammo( s.id ) }
  end
  
  alias :sara_ammo_gmact_learn_skill :learn_skill unless $@
  def learn_skill( skill_id )
    sara_ammo_gmact_learn_skill( skill_id ) ; setup_ammo( skill_id )
  end
  
end  
  
#==============================================================================#
# ** Scene_Title
#==============================================================================#
class Scene_Title < Scene_Base
  
  #--------------------------------------------------------------------------#
  # * alias method :load_database
  #--------------------------------------------------------------------------#
  alias :sara_ammo_sct_load_database :load_database unless $@
  def load_database()
    sara_ammo_sct_load_database()
    load_sara_ammo_cache()
  end
  
  #--------------------------------------------------------------------------#
  # * alias method :load_bt_database
  #--------------------------------------------------------------------------#
  alias :sara_ammo_sct_load_bt_database :load_database unless $@
  def load_bt_database()
    sara_ammo_sct_load_bt_database()
    load_sara_ammo_cache()
  end  
  
  #--------------------------------------------------------------------------#
  # * new method :load_sara_ammo_cache
  #--------------------------------------------------------------------------#
  def load_sara_ammo_cache()
    objs = [$data_items, $data_skills, $data_enemies]
    objs.each { |group| group.each { |obj| next if obj.nil?() 
      obj.sara_ammo_skillcache() if obj.is_a?(RPG::Skill) 
      obj.sara_ammo_itemcache() if obj.is_a?(RPG::Item) 
    } }  
  end
  
end

#=*==========================================================================*=#
# ** END OF FILE
#=*==========================================================================*=#
