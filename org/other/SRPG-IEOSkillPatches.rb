# SRPG - IEO Skill Patches
class Game_Battler
  
  #--------------------------------------------------------------------------#
  # * overwrite method :skill_can_use?
  #--------------------------------------------------------------------------#
  def skill_can_use?( skill )
    return false unless skill.is_a?( RPG::Skill )
    return false unless movable?()
    return false if silent?() and skill.spi_f > 0
    #---
    return false unless custom_skill_cost( skill, :can_use )
    return false unless custom_skill_subcost( skill, :can_use )
    #---
    if $game_temp.in_battle or $game_temp.in_srpg
      if skill.nomove?()    # 移動不可スキルならば移動済みの場合に使用不可
        return false if $scene.event != nil and $scene.event.move_end
      end
      return skill.battle_ok?()
    else
      return skill.menu_ok?()
    end
  end
  
end

class Game_Actor < Game_Battler
  
  #--------------------------------------------------------------------------#
  # * overwrite method :skill_can_use?
  #--------------------------------------------------------------------------#
  def skill_can_use?(skill)
    return false unless super
    element = TSRPG::WEAPON_ELEMENT & skill.element_set
    return false unless element_set & element == element
    for state in states
      for id in state.temp_skill
        return true if id == skill.id
      end
    end
    for item in equips.compact    
      for id in item.temp_skill
        return true if id == skill.id
      end
    end
    return true if skill.id <= 3  
    return true ##return skill_learn?(skill)
  end
  
end
