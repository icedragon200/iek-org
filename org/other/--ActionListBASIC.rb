# --Action List (BASIC)
module SARA
  module TSRPG
    module ACTION_LIST
      # // Attack Default Action
      ACTIONS["TargetTurn"] = [
        ["OBJ_ANIMATION", ["WAIT"]],
        ["SCRIPT", [" 
         user.move_end   = true ; 
         user.action_end = true"
        ]],
        ["SCRIPT", ["$scene.event = target ; 
         $scene.event.move_end   = true ; 
         $scene.event.action_end = true"
        ]],
        ["SCRIPT", ["$scene.change_scene_state( 5 )"]],
      ]
      ACTIONS["TargetShift"] = [
        ["OBJ_ANIMATION", ["WAIT"]],
        ["SCRIPT", [" 
         user.move_end   = true ; 
         user.action_end = true"
        ]],
        ["SCRIPT", ["$scene.event = target"]],
        ["SCRIPT", ["$scene.change_scene_state( 1 )"]]
      ]
    end  
    
  end
end

class Scene_Srpg < Scene_Base
  
  attr_accessor :event
  
end  
