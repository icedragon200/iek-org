# SRPG - GraphicalChanges
class Spriteset_Srpg
  
  BOX_BIT = Cache.system("range_overdraw")
  
  EFFECT_COLOR = Color.new( 0, 255, 32, 128 )

  # // 0x22f - Bright Blue
  # // 1185  - Lime Green
  # // 1585  - Dark Orange
  #--------------------------------------------------------------------------
  # ● エリア表示フラッシュデータの更新
  #--------------------------------------------------------------------------
  def refresh_area
    state = $game_srpg.enemy_turn? ? $game_srpg.scene_state - 100 : $game_srpg.scene_state
    if (state >= 2 and state <= 5) or state == 70
      # 攻撃可能範囲の描画
      for pos in $scene.attack_cell
        @tilemap.flash_data[pos[0], pos[1]] = 1585 #0xf22
      end
    else
      # 移動可能範囲の描画
      for pos in $scene.movable_cell.keys
        if $scene.movable_cell[pos] != ""
          @tilemap.flash_data[pos[0], pos[1]] = 1111 #0x22f
        end
      end
    end
    refresh_effect_area   # ついでに効果範囲エリアも再描画
  end
  
  def refresh_effect_area
    @effect_area_sprite.bitmap.clear()
    color = EFFECT_COLOR #Color.new(255, 255, 0, 128)
    for pos in $scene.effect_cell
      x, y = (((Graphics.width/32)/2)-1 + pos[0]) << 5, (((Graphics.height/32)/2)-1 + pos[1]) << 5
      @effect_area_sprite.bitmap.fill_rect( x, y, 32, 32, color )  
      #@effect_area_sprite.bitmap.blt( x, y, BOX_BIT, BOX_BIT.rect, 128 )
    end
    @effect_area_sprite.visible = true
  end
  
end  
