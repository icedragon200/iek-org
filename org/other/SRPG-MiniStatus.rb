# SRPG - MiniStatus
#~ class SARA::TSRPG::Sprite_CharacterDemo < ::Sprite_Character
#~ end

#~ class SARA::TSRPG::Sprite_MiniStatus < ::Sprite      
#~       
#~   def initialize( x, y, viewport=nil )
#~     super( viewport )
#~     self.x = x
#~     self.y = y
#~     self.z = 136
#~     @back_sprite = Sprite.new
#~     @back_sprite.bitmap = Cache.picture("MiniStatus")
#~     @back_sprite.visible = true
#~     @back_sprite.x   = self.x
#~     @back_sprite.y   = self.y
#~     @back_sprite.z   = 135
#~     self.bitmap      = Bitmap.new( @back_sprite.bitmap.width, 
#~      @back_sprite.bitmap.height )
#~     @dummy_character = Game_Character.new
#~     @demo_sprite     = SARA::TSRPG::Sprite_CharacterDemo.new( self.viewport, @dummy_character )
#~     @demo_sprite.x   = self.x
#~     @demo_sprite.y   = self.y
#~     @cx, @cy = 0, 0
#~   end
#~       
#~   #------------------------------------------------------------------------#
#~   # ● super method :dispose
#~   #------------------------------------------------------------------------#
#~   def dispose()
#~     @back_sprite.dispose()
#~     @back_sprite     = nil
#~     @dummy_character = nil
#~     @demo_sprite.dispose()
#~     @demo_sprite     = nil
#~     super()
#~   end
#~   
#~   def update
#~     if $game_srpg.scene_state <= 5
#~       if @cx != $game_cursor.x or @cy != $game_cursor.y
#~         @cx, @cy = $game_cursor.x, $game_cursor.y
#~         refresh($game_srpg.alive_unit_xy(@cx, @cy))
#~       end
#~     end
#~     @demo_sprite.update()
#~     @demo_sprite.x = self.x + 25
#~     @demo_sprite.y = self.y + 46
#~     @demo_sprite.z = self.z + 4
#~     @back_sprite.x = self.x
#~     @back_sprite.y = self.y
#~   end
#~       
#~   def refresh( attacker, target = nil )
#~     self.bitmap.clear()
#~     # 描画対象がいなければ地形効果だけを描画
#~     if attacker.nil?()
#~       @demo_sprite.character = @dummy_character
#~       #self.contents.draw_text( 0, 0, 240, WLH, make_text_tile(@cx, @cy) )
#~       #@back_sprite.visible = false
#~       return
#~     end
#~     @demo_sprite.character = attacker
#~     rect = Rect.new( 25+32, 8, 192, 24 )
#~     self.bitmap.draw_text( rect, attacker.unit.name )
#~     x, y, width = 25+18, 10, 128
#~     gc1 = hp_gauge_color1
#~     gc2 = hp_gauge_color2
#~     val, max = attacker.unit.hp, attacker.unit.maxhp
#~     rect = Rect.new( x, y+WLH-6, width, 4 )
#~     draw_round_grad_bar( rect, val, max, gc1, gc2, gauge_back_color )
#~     x, y, width = 25+20, 18, 128
#~     gc1 = mp_gauge_color1
#~     gc2 = mp_gauge_color2
#~     val, max = attacker.unit.mp, attacker.unit.maxmp
#~     rect = Rect.new( x, y+WLH-6, width, 4 )
#~     draw_round_grad_bar( rect, val, max, gc1, gc2, gauge_back_color )
#~   end
#~     
#~ end

#~ class Scene_Srpg < Scene_Base
#~   
#~   alias :sara_scsrpg_ministatus_start :start unless $@
#~   def start()
#~     sara_scsrpg_ministatus_start()
#~     @mini_status = SARA::TSRPG::Sprite_MiniStatus.new( Graphics.width-192, 0 )
#~   end
#~   
#~   alias :sara_scsrpg_ministatus_update :update unless $@
#~   def update()
#~     sara_scsrpg_ministatus_update()
#~     @mini_status.update() unless @mini_status.nil?()
#~   end
#~   
#~   alias :sara_scsrpg_ministatus_terminate :terminate unless $@
#~   def terminate()
#~     @mini_status.dispose() unless @mini_status.nil?()
#~     sara_scsrpg_ministatus_terminate()
#~   end
#~   
#~ end  
