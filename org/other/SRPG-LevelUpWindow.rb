# SRPG - LevelUp Window
class Window_LevelUp < Window_Base
  
  attr_accessor :unit1
  attr_accessor :unit2
  
  def initialize( unit1, unit2 )
    super( 0, 0, Graphics.width-128, Graphics.height-128 )
    self.x = (Graphics.width-self.width) / 2
    self.y = (Graphics.height-self.height) / 2
    @unit1, @unit2 = unit1, unit2
    refresh()
  end
  
  def dispose()
    super()
  end
  
  def icon( s )
    case s
    when :maxhp, :hp
      return IEO::Icon.stat( :hp_ic )
    when :maxmp, :mp
      return IEO::Icon.stat( :mp_ic )
    when :atk  
      return IEO::Icon.stat( :atk_i )
    when :def
      return IEO::Icon.stat( :def_i )
    when :spi
      return IEO::Icon.stat( :spi_i )
    when :agi
      return IEO::Icon.stat( :agi_i )
    end  
  end
  
  def vocab( s )
    case s 
    when :maxhp, :hp
      return Vocab.hp
    when :maxmp, :mp
      return Vocab.mp
    else
      return Vocab.send( s )
    end  
  end
  
  def refresh()
    stats = [:maxhp, :maxmp, :atk, :def, :spi, :agi]
    y_offset = WLH
    self.contents.font.size = Font.default_size + 4
    self.contents.font.color = system_color
    self.contents.draw_text( 0, 0, self.contents.width/2, WLH, "New Stats" )
    self.contents.draw_text( self.contents.width/2, 0, self.contents.width/2, WLH, "New Skills" )
    self.contents.font.size = Font.default_size
    self.contents.font.color = normal_color
    for i in 0...stats.size
      s = stats[i]
      self.contents.font.color = system_color
      draw_icon( icon( s ), 0, y_offset+(i*WLH) )
      self.contents.draw_text( 24, y_offset+(i*WLH), 56, WLH, vocab( s ) )
      self.contents.font.color = normal_color
      self.contents.draw_text( 56, y_offset+(i*WLH), 56, WLH, @unit1.send(s) )
      self.contents.font.color = hp_gauge_color1
      self.contents.draw_text( 128, y_offset+(i*WLH), 56, WLH, @unit2.send(s) )
    end
    ss = Proc.new { |s| s.inject([]) { |r, s| r << s.id } }
    @skills = (ss.call(@unit2.skills) - ss.call(@unit1.skills))
    @skills = @unit2.skills.inject([]) { |r, s| r << s if @skills.include?(s.id); r }
    for i in 0...@skills.size
      sk = @skills[i]
      rect = Rect.new(self.contents.width/2, i*WLH, self.contents.width/2, WLH)
      draw_icon( sk.icon_index, rect.x + (24 * (i % (rect.width/24))), y_offset+(24 * (i / (rect.width/24))) )
      rect.y += y_offset + WLH
      draw_obj_name( sk, rect, true )
    end  
  end
  
end

class Scene_Srpg < Scene_Base
  
  def display_levelup( old_unit, new_unit )
    @levelup_window = Window_LevelUp.new( old_unit, new_unit )
    loop do
      update_basic()
      @levelup_window.update()
      if Input.trigger?( Input::C )
        break
      end  
    end  
    @levelup_window.dispose()
  end
  
end
