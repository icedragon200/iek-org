# SRPG - Status Window
class SARA::Container_Status < SARA::Container_Base
  
  7.times { |i| Cache.picture( "BattleHud-DamageLv#{i}" ) } # // Load all huds
  NUMBER_BMP = Bitmap.new( 24*10, 24 )
  10.times { |i| NUMBER_BMP.draw_text( i*24, 0, 24, 24, i, 1 ) }
  
  def self.draw_numbers( x, y, number, bitmap, spacing=8 )
    numbers = number.to_s.split("")
    for i in 0...numbers.size
      bitmap.blt( x+(i*spacing), y, NUMBER_BMP, 
        Rect.new( numbers[i].to_i*24, 0, 24, 24 ) )
    end  
  end
  
  class ValueSlider
    
    attr_accessor :max
    attr_accessor :value
    attr_accessor :target_value
    attr_accessor :slide_rate
    
    def initialize()
      @max          = 0.0
      @value        = 0.0
      @target_value = 0.0
      @slide_rate   = 30.0
    end
    
    def dmax
      return [@max, 1.0].max
    end
    
    def set( value, max, quick=false )
      @max          = max.to_f
      @target_value = value.to_f
      @value        = [@value, @max].min
      @value        = @target_value if quick
    end
    
    def update()
      if @target_value > @value
        @value = [@value + ([@max, 1.0].max) / @slide_rate, @target_value].min 
      elsif @target_value < @value
        @value = [@value - ([@max, 1.0].max) / @slide_rate, @target_value].max
      end  
    end
    
  end
  
  class Value_Tracker
    
    attr_accessor :parent
    
    def initialize( parent, cproc, tproc )
      @last_value = -1
      @last_max   = -1
      @change_proc  = cproc
      @tracker_proc = tproc
      @parent = parent
    end
    
    def changed?( new_value, new_max )
      return (@last_value != new_value || @last_max != new_max)
    end
    
    def update()
      new_value, new_max = *@tracker_proc.call( @parent )
      if changed?( new_value, new_max )
        @change_proc.call( @parent )
        @last_value, @last_max = new_value, new_max
      end  
    end
    
  end
  
  class Event_Status < ::SARA::Container_Base
    
    attr_accessor :attacker
    
    attr_accessor :hp_slider, :mp_slider, :en_slider
    
    def initialize( x, y )
      super( x, y )
      @disposed    = false
      @base_sprite = Sprite.new()
      @face_sprite = Sprite.new()
      @hp_sprite   = Sprite.new()
      @hp_sprite.bitmap = Cache.picture( "LF-Orb" )
      @mp_sprite   = Sprite.new()
      @mp_sprite.bitmap = Cache.picture( "PSI-Orb" )
      @en_sprite   = Sprite.new()
      @en_sprite.bitmap = Cache.picture( "EN-Orb" )
    
      @text_sprite = Sprite.new()
      @text_sprite.bitmap = Bitmap.new( 236, 128 )
      
      @state_sprite= Sprite.new()
      @state_sprite.bitmap = Bitmap.new( 236, 24 )
      @state_sprite.z = 1
      
      @dummy_bitmap = Bitmap.new( 32,32 )
    
      @hp_slider = ValueSlider.new()
      @mp_slider = ValueSlider.new()
      @en_slider = ValueSlider.new()
    
      cproc = Proc.new { |parent| parent.redraw_hp }
      tproc = Proc.new { |parent| 
        (parent.attacker.nil?()||parent.attacker.unit.nil?()) ? [0, 0] :
          [parent.hp_slider.value, parent.hp_slider.max]
          #[parent.attacker.unit.hp, parent.attacker.unit.maxhp] 
      }
      @hp_tracker = Value_Tracker.new( self, cproc, tproc )
      
      cproc = Proc.new { |parent| parent.redraw_mp }
      tproc = Proc.new { |parent| 
        (parent.attacker.nil?()||parent.attacker.unit.nil?()) ? [0, 0] :
          [parent.mp_slider.value, parent.mp_slider.max]
          #[parent.attacker.unit.mp, parent.attacker.unit.maxmp] 
      }
      @mp_tracker = Value_Tracker.new( self, cproc, tproc )
      
      cproc = Proc.new { |parent| parent.redraw_en }
      tproc = Proc.new { |parent| 
        (parent.attacker.nil?()||parent.attacker.unit.nil?()) ? [0, 0] :
          [parent.en_slider.value, parent.en_slider.max] 
          #[parent.attacker.unit.en, parent.attacker.unit.maxen] 
      }
      @en_tracker = Value_Tracker.new( self, cproc, tproc )
      
      @face_sprite.bitmap = @dummy_bitmap
      @sprites = [@base_sprite, @face_sprite, @text_sprite, @state_sprite,
       @hp_sprite, @mp_sprite, @en_sprite]
      @bars = [@hp_sprite, @mp_sprite, @en_sprite]
      refresh( nil )
      update_position()
      #@___last_frame = 0
      #@tracker_thread = Thread.new { 
      #  loop do
      #    if @___last_frame != Graphics.frame_count
      #      #puts "track #{@___last_frame} #{Graphics.frame_count}"
      #      @hp_tracker.update() unless @hp_tracker.nil?()
      #      @mp_tracker.update() unless @mp_tracker.nil?()
      #      @en_tracker.update() unless @en_tracker.nil?()
      #      @___last_frame = Graphics.frame_count
      #      break if @disposed
      #    end  
      #    #(@hp_tracker.nil?() && @mp_tracker.nil?() && @en_tracker.nil?())  ||
      #  end
      #}  
    end
  
    def srect()
      return SRect.new(self.x, self.y, 
       @base_sprite.bitmap.width, @base_sprite.bitmap.height)
    end
    
    def refresh( attacker )
      @face_sprite.bitmap = @dummy_bitmap
      @base_sprite.bitmap = @dummy_bitmap
      @attacker = attacker
      setup_font()
      if attacker.nil?() || attacker.unit.nil?()
        @hp_slider.set( 0, 1 )
        @mp_slider.set( 0, 1 )
        @en_slider.set( 0, 1 )
        @state_sprite.bitmap.clear()
        @text_sprite.bitmap.clear()
        redraw_hp() ; redraw_mp() ; redraw_en()
        basic_update()
        return 
      end  
      set_face()
      draw_states()
      
      value, max = @attacker.unit.hp, @attacker.unit.maxhp
      @hp_slider.set( value, max )
      
      value, max = @attacker.unit.mp, @attacker.unit.maxmp
      @mp_slider.set( value, max )
      
      value, max = @attacker.unit.en, @attacker.unit.maxen
      @en_slider.set( value, max ) 
      
      @text_sprite.bitmap.clear()
      redraw_hp() ; redraw_mp() ; redraw_en()
    end
  
    def draw_states()
      @state_sprite.bitmap.clear()
      #16, 130
      draw_actor_state( @attacker.unit, 0, 0, 6*28, @state_sprite.bitmap )
    end
    
    def draw_actor_state( actor, x, y, width, bitmap )
      count = 0
      for state in actor.states
        next if state.icon_index == 0
        next if state.hide_state
        bitmap.draw_icon(state.icon_index, x + 28 * count, y)
        draw_state_turns(x + 28 * count, y, state, actor, bitmap)
        count += 1
        break if (28 * count > width - 28)
      end
      bitmap.font.color = bitmap.normal_color
      bitmap.font.bold = Font.default_bold
      bitmap.font.size = Font.default_size
    end
  
    def draw_state_turns( x, y, state, actor, bitmap )
      return if state.nil?()
      return unless actor.state_turns.include?( state.id )
      dy = y - (16 - 10)
      duration = actor.state_turns[state.id] 
      if state.auto_release_prob > 0 and duration >= 0
        bitmap.font.color = bitmap.text_color(state.turn_colour)
        bitmap.font.size = 16
        bitmap.font.bold = false
        bitmap.draw_text(x, dy, 24, 24, duration, 2)
      end
    end
  
    def set_face()
      findex = @attacker.face_index
      fname  = @attacker.face_name
      
      @face_sprite.bitmap = Cache.face( fname )
      @face_sprite.src_rect.set( (findex%4)*96, (findex/4)*96, 96, 96 ) 
    end
    
    def setup_font()
      @text_sprite.bitmap.font.name = "ProggyCleanTT"
      @text_sprite.bitmap.font.size = 13 #Font.default_size - 4
    end
    
    def setup_font_color( color1, color2, value, max )
      @text_sprite.bitmap.font.color = ISS.color_gradient_calc( color1, color2, 
       100.0, 100.0-(100 * value.to_f / max.to_f) )
    end
    
    def redraw_hp()
      #setup_font()
      rect = Rect.new( 114+6, 30-8, 96, 24 )
      @text_sprite.bitmap.clear_rect( rect )
      return if attacker.nil?() || attacker.unit.nil?()   
      color1, color2 = @text_sprite.normal_color, @text_sprite.knockout_color
      setup_font_color( color1, color2, @hp_slider.value, @hp_slider.max )
      @text_sprite.bitmap.draw_text( rect, 
       sprintf("%s/%s", Integer(@hp_slider.value), Integer(@hp_slider.max) ) )
      hud = Cache.picture( "BattleHud-DamageLv#{6-Integer(6*@hp_slider.target_value/@hp_slider.max)}" )
      @base_sprite.bitmap &&= hud 
    end
    
    def redraw_mp()
      #setup_font()
      rect = Rect.new( 114+6, 30-8+40, 96, 24 )
      @text_sprite.bitmap.clear_rect( rect )
      return if attacker.nil?() || attacker.unit.nil?()
      color1, color2 = Color.new( 43, 145, 204 ), @text_sprite.crisis_color
      setup_font_color( color1, color2, @mp_slider.value, @mp_slider.max )
      @text_sprite.bitmap.draw_text( rect, 
       sprintf("%s/%s", Integer(@mp_slider.value), Integer(@mp_slider.max) ) )
    end
    
    def redraw_en()
      #setup_font()
      rect = Rect.new( 114+6, 30-8+80, 96, 24 )
      @text_sprite.bitmap.clear_rect( rect )
      return if attacker.nil?() || attacker.unit.nil?()
      color1, color2 = Color.new( 64, 176, 73 ), Color.new( 73, 33, 33 )
      setup_font_color( color1, color2, @en_slider.value, @en_slider.max )
      @text_sprite.bitmap.draw_text( rect, 
       sprintf("%s/%s", Integer(@en_slider.value), Integer(@en_slider.max) ) )
    end
    
    def disposed?() ; return @disposed ; end
      
    def dispose()
      @base_sprite.dispose()
      @face_sprite.dispose()
      @hp_sprite.dispose()
      @mp_sprite.dispose()
      @en_sprite.dispose()
      @text_sprite.bitmap.dispose() ; @text_sprite.dispose()
      @state_sprite.bitmap.dispose(); @state_sprite.dispose()
      @dummy_bitmap.dispose() unless @dummy_bitmap.nil?()
      @sprites.clear() ; @bars.clear()
      @base_sprite  = nil
      @face_sprite  = nil
      @hp_sprite    = nil
      @mp_sprite    = nil
      @en_sprite    = nil
      @text_sprite  = nil
      @state_sprite = nil
      @dummy_bitmap = nil
      @sprites      = nil
      @bars         = nil
      @hp_tracker   = nil
      @mp_tracker   = nil
      @en_tracker   = nil
      @disposed     = true
    end
  
    def update()    
      basic_update()
    end
  
    def basic_update()
      @sprites.each { |sp| 
        sp.visible = self.visible && !@attacker.nil?() 
        sp.opacity = self.opacity
      }
      @hp_tracker.update()
      @mp_tracker.update()
      @en_tracker.update()
      update_bars()
      update_position()
    end
  
    def update_bars()  
      
      unless @attacker.nil?()
        value, max = @attacker.unit.hp, @attacker.unit.maxhp
        @hp_slider.set( value, max )
      
        value, max = @attacker.unit.mp, @attacker.unit.maxmp
        @mp_slider.set( value, max )
      
        value, max = @attacker.unit.en, @attacker.unit.maxen
        @en_slider.set( value, max )
      end  
      
      @hp_slider.update()
      @mp_slider.update()
      @en_slider.update()
      h  = @hp_sprite.bitmap.height
      sh = Integer(h * @hp_slider.value / @hp_slider.dmax)
      @hp_sprite.src_rect.set( 0, h-sh, @hp_sprite.bitmap.width, sh )
      h  = @mp_sprite.bitmap.height
      sh = Integer(h * @mp_slider.value / @mp_slider.dmax)
      @mp_sprite.src_rect.set( 0, h-sh, @mp_sprite.bitmap.width, sh )
      h  = @en_sprite.bitmap.height
      sh = Integer(h * @en_slider.value / @en_slider.dmax)
      @en_sprite.src_rect.set( 0, h-sh, @en_sprite.bitmap.width, sh )
    end
  
    def update_position()
      @base_sprite.x,  @base_sprite.y  = @x, @y
      @face_sprite.x,  @face_sprite.y  = @x + 16, @y + 20
      @text_sprite.x,  @text_sprite.y  = @x, @y
      @state_sprite.x, @state_sprite.y = @x + 16, @y + 130
      @hp_sprite.x = @mp_sprite.x = @en_sprite.x = @x + 192
      @hp_sprite.y = @y + 10 + @hp_sprite.src_rect.y
      @mp_sprite.y = @y + 50 + @mp_sprite.src_rect.y
      @en_sprite.y = @y + 90 + @en_sprite.src_rect.y
    end
  
  end  
  
  class SRect < ::Rect
    def in_field?( fx, fy )
      return (fx.between?(self.x, self.x+self.width) && 
       fy.between?(self.y, self.y+self.height))
    end  
  end
  
  Pos = Struct.new( :x, :y )
  ScreenDivision = Struct.new( :rect, :new_pos )
  
  def initialize( x, y )
    super( x, y )
    @disposed = false
    @attacker_status = Event_Status.new( self.x, self.y )
    @target_status = Event_Status.new( self.x+256, self.y )
    @hover_count = 0
    setup_0x_screen()
    
    update()
  end
  
  def setup_0x_screen()
    @screen_divisions = []
  end
  
  def setup_2x_screen()
    @screen_divisions = []
    # // Top
    @screen_divisions[0] = ScreenDivision.new( 
      SRect.new( 0, 0, Graphics.width, Graphics.height/2 ), 
      Pos.new( 0, 264 ) 
    )
    # // Bottom
    @screen_divisions[1] = ScreenDivision.new( 
      SRect.new( 0, Graphics.height/2, Graphics.width, Graphics.height/2 ), 
      Pos.new( 0, 0 ) 
    )
  end  
  
  def setup_4x_screen()
    @screen_divisions = []
    # // Top Left
    @screen_divisions[0] = ScreenDivision.new( 
      SRect.new(0,0,Graphics.width/2, Graphics.height/2), 
      Pos.new(0,264) 
    )
    # // Top Right
    @screen_divisions[1] = ScreenDivision.new( 
      SRect.new(Graphics.width/2,0,Graphics.width/2, Graphics.height/2), 
      Pos.new(Graphics.width/2,264) 
    )
    # // Bottom Left
    @screen_divisions[2] = ScreenDivision.new( 
      SRect.new(0,Graphics.height/2,Graphics.width/2, Graphics.height/2), 
      Pos.new(0,0) 
    )
    # // Bottom Right
    @screen_divisions[3] = ScreenDivision.new( 
      SRect.new(Graphics.width/2,Graphics.height/2,Graphics.width/2, Graphics.height/2), 
      Pos.new(Graphics.width/2,0) 
    )
  end
  
  def update()
    
    if $game_srpg.scene_state <= 5
      if @cx != $game_cursor.x or @cy != $game_cursor.y
        @cx, @cy = $game_cursor.x, $game_cursor.y
        refresh($game_srpg.alive_unit_xy(@cx, @cy))
      end
    end
    
    #@screen_divisions.each { |s| 
    #  if s.rect.in_field?( $game_cursor.screen_x, $game_cursor.screen_y )
    #    self.x, self.y = s.new_pos.x, s.new_pos.y
    #    break
    #  end  
    #}
    #Input.press?(Input::Z)
    #if @attacker_status.srect.in_field?( $game_cursor.screen_x, $game_cursor.screen_y )
    #  @hover_count += 1 unless @hover_count >= 72
    #  @attacker_status.opacity = [@attacker_status.opacity-(255/30.0), 56].max if @hover_count >= 72
    #else
    #  @hover_count -= 1 unless @hover_count <= 0
    #  @attacker_status.opacity = [@attacker_status.opacity+(255/30.0), 255].min if @hover_count <= 0
    #end  
    
    @attacker_status.x, @attacker_status.y = self.x, self.y
    @target_status.x, @target_status.y = self.x+256, self.y
    @attacker_status.update()
    @target_status.update()
  end
  
  def disposed?() ; return @disposed ; end
    
  def dispose()
    @attacker_status.dispose() ; @target_status.dispose()
    @attacker_status = nil ; @target_status = nil
    @disposed = true
    @screen_divisions.clear() ; @screen_divisions = nil
  end
  
  def refresh( attacker, target = nil )
    @attacker_status.refresh( attacker )
    @target_status.refresh( target == attacker ? nil : target )
  end
  
end  

class Scene_Srpg < Scene_Base
  
  def create_window
    @help_window = Window_Help.new
    @help_window.visible = false
    @message_mini_window = Window_Message_Mini.new(0, 0)
    @status_window = SARA::Container_Status.new(0, 264)
    @unit_window = Window_Unit.new( 0, 0, 160, 24 * 6 + 32 )
    @info_window = Window_Info.new( 160, 0 )
    @menu_window = Window_Command.new( 160, ["End Turn", "Unit List", "Information", "Save"] )
    @menu_window.openness = 0
    @menu_window.active = false
    @command_window = Window_Command.new(160, [Vocab::status])
    @command_window.openness = 0
    @command_window.active = false
    @skill_window = Window_Action.new($game_party.members[0])
    @skill_window.openness = 0
    @skill_window.active = false
    @skill_window.help_window = @help_window
    @item_window = Window_Item.new(0, 56, 256, 32 + 24 * 6)
    @item_window.set_column(1)
    @item_window.openness = 0
    @item_window.active = false
    @item_window.help_window = @help_window
  end
  
  #--------------------------------------------------------------------------
  # ● 基本更新処理
  #--------------------------------------------------------------------------
  def update_basic
    Graphics.update                 # ゲーム画面を更新
    Input.update                    # 入力情報を更新
    $game_srpg.update               # マップを更新
    $game_cursor.update             # カーソルを更新
    @spriteset.update               # スプライトセットを更新
    @message_mini_window.update     # ミニメッセージウィンドウ更新
    @status_window.update  
  end
  
end  
