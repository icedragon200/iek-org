# SRPG - BugFixes
class Game_Srpg
  
  #--------------------------------------------------------------------------#
  # * alias-method :initialize
  #--------------------------------------------------------------------------# 
  alias :iss003_gsrpg_initialize :initialize unless $@
  def initialize() ; iss003_gsrpg_initialize() ; create_posRegister() ; end
    
end

class Game_SrpgEvent
  
  #--------------------------------------------------------------------------#
  # * Include Module(s)
  #--------------------------------------------------------------------------#      
  include ISS::MixIns::ISS007::Event
  
  #--------------------------------------------------------------------------#
  # * Public Instance Variables
  #--------------------------------------------------------------------------#
  attr_accessor :temp_event
  
  #--------------------------------------------------------------------------#
  # * alias-method :initialize
  #--------------------------------------------------------------------------#  
  alias :iss007_ge_initialize :initialize unless $@
  def initialize( map_id, event )
    @temp_event = false
    @z_over = nil
    @reset_temp_on_page = $game_system.reset_temp_on_page
    iss007_ge_initialize( map_id, event )
    iss007_initcache()
  end
  
  #--------------------------------------------------------------------------#
  # * overwrite-method :conditions_met?
  #--------------------------------------------------------------------------#
  def conditions_met?( page )
    c = page.condition
    if c.switch1_valid      # Switch 1
      return false if $game_switches[c.switch1_id] == false
    end
    if c.switch2_valid      # Switch 2
      return false if $game_switches[c.switch2_id] == false
    end
    if c.variable_valid     # Variable
      return false if $game_variables[c.variable_id] < c.variable_value
    end
    if @temp_event # // Temp Event Internal Self Switch
      return false unless @self_switches[c.self_switch_ch] if c.self_switch_valid 
    else  
      if c.self_switch_valid  # Self switch
        key = [@map_id, @event.id, c.self_switch_ch]
        return false if $game_self_switches[key] != true
      end
    end  
    if c.item_valid         # Item
      item = $data_items[c.item_id]
      return false if $game_party.item_number(item) == 0
    end
    if c.actor_valid        # Actor
      actor = $game_actors[c.actor_id]
      return false unless $game_party.members.include?(actor)
    end
    return true   # Conditions met
  end
  
  #--------------------------------------------------------------------------#
  # * new-method :posRegister
  #--------------------------------------------------------------------------#
  def posRegister() ; return $game_srpg.posRegister ; end
  
  #--------------------------------------------------------------------------#
  # * alias-method :setup
  #--------------------------------------------------------------------------#
  alias :iss007_ge_setup :setup unless $@
  def setup( new_page )
    iss007_ge_setup( new_page )
    iss007_eventcache()
  end
  
  #--------------------------------------------------------------------------#
  # * alias-method :screen_z
  #--------------------------------------------------------------------------#
  alias :iss007_ge_screen_z :screen_z unless $@
  def screen_z( *args, &block )
    return @z_over.nil?() ? iss007_ge_screen_z( *args, &block ) : @z_over
  end  
  
  #--------------------------------------------------------------------------#
  # * overwrite-method :map_passable?
  #--------------------------------------------------------------------------#
  def map_passable?( x, y )
    return $game_srpg.ship_passable?( x, y ) if @water_event
    return super( x, y )
  end
  
  def check_event_trigger_touch(x, y)
  end
  
end  

