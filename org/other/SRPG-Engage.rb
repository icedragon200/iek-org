# SRPG - Engage!!!
module SARA
  module TSRPG
    module ENGAGE
      # //      [event_id, event_id, event_id]
      REFMAP  = 30
      PLAYER  = 1
      ENEMY   = 2
      BATTLE_PROGRESS = 10
      
      module_function()
      
      def usable_tile?( orx, ory, tgx, tgy ) 
        #return false unless $game_srpg.passable?( tgx, tgy ) 
        return false unless $game_srpg.possible_path?( orx, ory, tgx, tgy, false, 0)
        return true
      end
        
      def create_area( orx, ory, width, height )
        result = []
        for px in 0..width
          for py in 0..height
            result << [orx+px, ory+py] if usable_tile?( orx, ory, orx+px, ory+py )
            result << [orx+px, ory-py] if usable_tile?( orx, ory, orx+px, ory-py )
            result << [orx-px, ory+py] if usable_tile?( orx, ory, orx-px, ory+py )
            result << [orx-px, ory-py] if usable_tile?( orx, ory, orx-px, ory-py )
          end
        end
        return result.uniq
      end
      
    end  
  end
end

class Game_Event
  attr_accessor :event
end  
 
class Game_Map
  
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # * Find Path
  #    src_x, src_y   : the source coordinates
  #    trgt_x, trgt_y : the target coordinates
  #    diagonal       : Is diagonal movement allowed?
  #    max_iterations : maximum number of iterations
  #    char           : character to follow the path
  #--------------------------------------------------------------------------
  #  Uses the A* method of pathfinding to find a path
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  def possible_path?( src_x, src_y, trgt_x, trgt_y, diagonal, max_iterations )
    # No possible path if the target itself is impassable
    path = []
    # Finished if Target Location is closed
    return false if !passable?(trgt_x, trgt_y)
    # Initialize
    max_elements = width*height + 2
    openx = Table.new(max_elements)
    openy = Table.new(max_elements)
    @open_nodes = Table.new(max_elements)
    @total_cost = Table.new(max_elements)
    heuristic = Table.new(max_elements)
    step_cost = Table.new(width, height)
    parent_x = Table.new(width, height)
    parent_y = Table.new(width, height)
    actual_list = Table.new(width, height)
    # Add the source node to the open list
    new_openid = 1
    @open_nodes[1] = 1
    openx[1] = src_x
    openy[1] = src_y
    dist = [(trgt_x - src_x).abs, (trgt_y - src_y).abs]
    heuristic[1] = diagonal ? (dist.max*14) + (dist.min*10) : (dist[0] + dist[1])*10
    @total_cost[1] = heuristic[1]
    actual_list[src_x, src_y] = 1
    @listsize = 1
    count = 0
    loop do
      break if actual_list[trgt_x, trgt_y] != 0 
      count += 1
      # Update Graphics every 500 iterations
      Graphics.update if count % 500 == 0
      return false if count == max_iterations
      return false if @listsize == 0
      node = @open_nodes[1]
      # Set the x and y value as parent to all possible children
      parent_xval, parent_yval = openx[node], openy[node]
      actual_list[parent_xval, parent_yval] = 2
      removefrom_binaryheap
      # Check all adjacent squares
      for i in 0...8
        break if i > 3 && !diagonal
        # Get the node
        x, y = case i
        when 0 then [parent_xval, parent_yval - 1] # UP
        when 1 then [parent_xval, parent_yval + 1] # DOWN 
        when 2 then [parent_xval - 1, parent_yval] # LEFT 
        when 3 then [parent_xval + 1, parent_yval] # RIGHT
        when 4 then [parent_xval - 1, parent_yval - 1] # UP LEFT
        when 5 then [parent_xval + 1, parent_yval - 1] # UP RIGHT
        when 6 then [parent_xval - 1, parent_yval + 1] # DOWN LEFT
        when 7 then [parent_xval + 1, parent_yval + 1] # DOWN RIGHT
        end
        # Next if this node is already in the closed list
        next if actual_list[x,y] == 2
        # Next if this tile in impassable
        next unless passable?(x, y) # Is the tile passable?
        # Take into account diagonal passability concerns
        if i > 3
          next unless case i
          when 4 then passable?(x + 1, y) || passable?(x, y + 1)
          when 5 then passable?(x - 1, y) || passable?(x, y + 1)
          when 6 then passable?(x + 1, y) || passable?(x, y - 1)
          when 7 then passable?(x - 1, y) || passable?(x, y - 1)
          end
        end
        # Check if this node already open
        plus_step_cost = ((x - parent_xval).abs + (y - parent_yval).abs) > 1 ? 14 : 10
        temp_step_cost = step_cost[parent_xval, parent_yval] + plus_step_cost
        if actual_list[x,y] == 1
          # If this is a better path to that node
          if temp_step_cost < step_cost[x, y]
            # Change Parent, step, and total cost
            parent_x[x, y] = parent_xval
            parent_y[x, y] = parent_yval
            step_cost[x, y] = temp_step_cost
            # Find index of this position
            index = 1
            while index < @listsize
              index += 1
              break if openx[@open_nodes[index]] == x &&
                                                openy[@open_nodes[index]] == y
            end
            @total_cost[@open_nodes[index]] = temp_step_cost + heuristic[@open_nodes[index]]
          else
            next
          end
        else # If not on open nodes
          # Add to open nodes
          new_openid += 1 # New Id for new item
          @listsize += 1 # Increase List Size
          @open_nodes[@listsize] = new_openid
          step_cost[x, y] = temp_step_cost
          # Calculate Heuristic
          d = [(trgt_x - x).abs, (trgt_y - y).abs]
          heuristic[new_openid] = diagonal ? (d.max*14) + (d.min*10) : (d[0] + d[1])*10
          @total_cost[new_openid] = temp_step_cost + heuristic[new_openid]
          parent_x[x, y] = parent_xval
          parent_y[x, y] = parent_yval
          openx[new_openid] = x
          openy[new_openid] = y
          index = @listsize
          actual_list[x, y] = 1
        end
        # Sort Binary Heap
        while index != 1
          temp_node = @open_nodes[index]
          if @total_cost[temp_node] <= @total_cost[@open_nodes[index / 2]] 
            @open_nodes[index] = @open_nodes[index / 2]
            index /= 2
            @open_nodes[index] = temp_node
          else
            break
          end
        end
      end
    end
    # Get actual target node
    path_x, path_y = trgt_x, trgt_y
    # Make an array of MoveRoute Commands
    while path_x != src_x || path_y != src_y
      # Get Parent x, Parent Y
      prnt_x, prnt_y = parent_x[path_x, path_y], parent_y[path_x, path_y]
      # DOWN = 1, LEFT = 2, RIGHT = 3, UP = 4, DL = 5, DR = 6, UL = 7, UR = 8
      if path_x < prnt_x # LEFT
        # Determine if upper, lower or direct left
        code = path_y < prnt_y ? 7 : path_y > prnt_y ? 5 : 2
      elsif path_x > prnt_x # RIGHT
        # Determine if upper, lower or direct right
        code = path_y < prnt_y ? 8 : path_y > prnt_y ? 6 : 3 
      else # UP or DOWN
        code = path_y < prnt_y ? 4 : 1
      end
      path.push(RPG::MoveCommand.new(code))
      path_x, path_y = prnt_x, prnt_y
    end
    return !path.empty?()
  end
  
end

class Game_Srpg < Game_Map
  
  def engage_setup( stx, sty, x, y, sizew, sizeh, map_id=$game_map.map_id )
    @actor_list = []
    $game_troop.setup( 1 )                        # 便宜上ダミートループを作成
    @map_id = map_id
    crea = create_engage_map( x, y, sizew, sizeh )
    $game_srpg.screen.start_tone_change($game_map.screen.tone, 0)
    @map = crea[0]
    @display_x = 0
    @display_y = 0
    @passages = $data_system.passages
    unless $game_swapxt.loaded_system.nil?()
      @passages = $game_swapxt.loaded_system.passages 
    end
    @events = {}
    setup_bypass_passages( map_id )
    setup_pass_table # // Prebuild Passability Table
    setup_events
    setup_engage_events( stx, sty, x, y, crea[1] )
    setup_pass_table # // Rebuild Passability Table
    setup_scroll
    setup_parallax
    
    setup_unit_list                             # ユニットリストを作成
    @need_refresh = false
    @scene_state = 0
    $game_variables[TSRPG::CHANGE_TURN] = 1     # ターン切り替え通知
    $game_variables[TSRPG::TURN_COUNT]  = 0     # ターン経過数
    $game_variables[TSRPG::GAME_RESULT] = 0     # 勝敗結果
    $game_cursor = Game_Cursor.new
    autoplay                                    # BGM と BGS の自動切り替え

  end
  
  def setup_engage_events( stx, sty, init_x, init_y, filt_rect )
    # // Setup Events
    ev_list = []
    for i in $game_map.events.keys
      ev = $game_map.events[i]
      if ev.x.between?( filt_rect.x, filt_rect.x + (filt_rect.width-1) )
        if ev.y.between?( filt_rect.y, filt_rect.y + (filt_rect.height-1) )
          next if ev.event.name.scan(/<engage>/i).size > 0
          ev_list.push ev
        end
      end  
    end
    reid = 0
    used_pos = []
    for i in 0...ev_list.size
      ev = ev_list[i]
      @events[i] = Game_SrpgEvent.new( @map_id, ev.event )
      @events[i].moveto( ev.x - filt_rect.x, ev.y - filt_rect.y )
      used_pos << [@events[i].x, @events[i].y]
      reid  = i
    end  
    # // Setup Enemies
    reid += 1
    enev = $game_map.get_event( SARA::TSRPG::ENGAGE::REFMAP, 
      SARA::TSRPG::ENGAGE::ENEMY ).clone
    usable_pos = [] 
    enx = stx - filt_rect.x
    eny = sty - filt_rect.y    
    usable_pos = SARA::TSRPG::ENGAGE.create_area( enx, eny, 6, 6 )
    #$game_srpg.possible_path?( orx, ory, tgx, tgy, false, 0)
    usable_pos = usable_pos.randomize() - used_pos
    #usable_pos.compact!()
    for i in 0..4
      evv      = enev.clone
      evv.id   = reid  
      evv.name = "Enemy#{i+1}<en=1>" #"Enemy#{i+1}<en=#{i}>"
      @events[reid] = Game_SrpgEvent.new( @map_id, evv )
      pos = usable_pos.shift() 
      used_pos << pos
      @events[reid].moveto( pos[0], pos[1] )
      @events[reid].set_last_pos()
      reid += 1
    end  
    # // Setup Players
    reid += 1
    plyev = $game_map.get_event( SARA::TSRPG::ENGAGE::REFMAP, 
      SARA::TSRPG::ENGAGE::PLAYER ).clone
    usable_pos = [] 
    plx = init_x - filt_rect.x
    ply = init_y - filt_rect.y
    usable_pos = SARA::TSRPG::ENGAGE.create_area( plx, ply, 3, 3 )
    usable_pos = (usable_pos.randomize()) - used_pos
    #usable_pos.compact!()
    for i in 0...$game_party.members.size
      evv      = plyev.clone
      evv.id   = reid  
      evv.name = "Player#{i+1}<pl=#{i}>"
      @events[reid] = Game_SrpgEvent.new( @map_id, evv )
      pos = usable_pos.shift
      @events[reid].moveto( pos[0], pos[1] )
      @events[reid].set_last_pos()
      reid += 1
    end  
    # // Setup progression event
    reid += 1
    prgev = $game_map.get_event( SARA::TSRPG::ENGAGE::REFMAP, 
      SARA::TSRPG::ENGAGE::BATTLE_PROGRESS ).clone
    prgev.id = reid  
    @events[reid] = Game_SrpgEvent.new( @map_id, prgev )
    @events[reid].moveto(0, 0)
  end
  
  def create_engage_map( x, y, sizew, sizeh )
    newmap      = RPG::Map.new( sizew, sizeh )
    newmap.data = Table.new( sizew, sizeh, 3 )
    hw  = sizew/2 ; hh  = sizeh/2
    hw += 1 if hw%2 > 0 ; hh += 1 if hh%2 > 0
    sx  = [x - hw, 0].max ; sy  = [y - hh, 0].max
    sx = $game_map.width - (sizew+1) if (x+hw) >= ($game_map.width)
    sy = $game_map.height - (sizeh+1) if (y+hh) >= ($game_map.height)
    for px in 0...(sizew)
      for py in 0...(sizeh)
        for z in 0...3
          newmap.data[ px, py, z ] = $game_map.data[ px+sx, py+sy, z ]
        end  
      end
    end
    return [ newmap, Rect.new( sx, sy, sizew, sizeh ) ]
  end
  
end
