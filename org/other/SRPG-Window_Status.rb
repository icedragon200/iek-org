# SRPG - Window_Status
#==============================================================================
# ■ Window_Status_Srpg
#------------------------------------------------------------------------------
# 　SRPGでステータスを表示するウィンドウです。
#==============================================================================

class Window_Status_Srpg < Window_Base
  #--------------------------------------------------------------------------
  # ● オブジェクト初期化
  #     x : ウィンドウの X 座標
  #     y : ウィンドウの Y 座標
  #--------------------------------------------------------------------------
  def initialize(x, y)
    super(x, y, Graphics.width, 152)
    self.z = 140
    self.opacity = 0
    self.visible = false
    @back_sprite = Sprite.new
    @back_sprite.bitmap = Cache.system("MessageBack")
    @back_sprite.visible = false
    @back_sprite.x = x
    @back_sprite.y = y
    @back_sprite.z = 135
    @cx, @cy = 0, 0
  end
  #--------------------------------------------------------------------------
  # ● 解放
  #--------------------------------------------------------------------------
  def dispose
    @back_sprite.dispose
    super
  end
  #--------------------------------------------------------------------------
  # ● 更新
  #--------------------------------------------------------------------------
  def update
    if $game_srpg.scene_state <= 5
      if @cx != $game_cursor.x or @cy != $game_cursor.y
        @cx, @cy = $game_cursor.x, $game_cursor.y
        refresh($game_srpg.alive_unit_xy(@cx, @cy))
      end
    end
  end
  #--------------------------------------------------------------------------
  # ● リフレッシュ
  #update1.011
  #--------------------------------------------------------------------------
  def refresh(attacker, target = nil)
    self.contents.clear
    # 描画対象がいなければ地形効果だけを描画
    if attacker == nil
      self.contents.draw_text(0, 0, 240, WLH, make_text_tile(@cx, @cy))
      @back_sprite.visible = false
      return
    end
    self.contents.font.color.alpha = 128 if attacker.unit.float?
    self.contents.draw_text(0, 0, 240, WLH, make_text_tile(attacker.x, attacker.y))
    self.contents.font.color.alpha = 255
    draw_face(attacker.face_name, attacker.face_index, 0, WLH * 1)
    self.contents.draw_text(0, WLH * 1, 96, WLH, attacker.unit.name)
    draw_actor_state(attacker.unit, 96, WLH * 2, 120)
    draw_actor_hp(attacker.unit, 96, WLH * 3)
    draw_actor_mp(attacker.unit, 96, WLH * 4)
    if target != nil
      attacker.back_attack?(target)           # 背後を取っているかチェック
      @attacker = attacker
      @target = target
      # ターゲットステータス
      self.contents.font.color.alpha = 128 if target.unit.float?
      self.contents.draw_text(272, 0, 240, WLH, make_text_tile(target.x, target.y), 2)
      self.contents.font.color.alpha = 255
      draw_face(target.face_name, target.face_index, 406, WLH * 1)
      self.contents.draw_text(406, WLH * 4, 96, WLH, target.unit.name, 2)
      draw_actor_hp(target.unit, 286, WLH * 1)
      draw_actor_mp(target.unit, 286, WLH * 2)
      draw_actor_state(target.unit, 286, WLH * 3, 120)
      # 行動結果予測
      self.contents.draw_text(240, WLH * 3 - 12, 32, WLH, "->", 1)
      # 通常攻撃
      if attacker.unit.action.attack?
        @obj = nil
        @damage = target.unit.get_attack_damage(attacker.unit)
        text = sprintf("%+d (%d%%)", (0 - @damage), get_hit_rate)
        n = $scene.get_counter_rate(attacker, target)
        if n > 0
          self.contents.draw_text(230, WLH * 4, 168, WLH, sprintf("Counter %d%", n), 2)
        end
      # スキル
      elsif attacker.unit.action.skill?
        @obj = attacker.unit.action.skill
        @damage = target.unit.get_obj_damage(attacker.unit, @obj)
        text = make_text_obj(@obj.damage_to_mp)
      # アイテム
      elsif attacker.unit.action.item?
        @obj = attacker.unit.action.item
        hp_recovery = target.unit.calc_hp_recovery(attacker.unit, @obj) # HP 回復量計算
        mp_recovery = target.unit.calc_mp_recovery(attacker.unit, @obj) # MP 回復量計算
        @damage = target.unit.get_obj_damage(attacker.unit, @obj)
        if target.unit.zombie?                    # ゾンビ状態の回復量反転
          hp_recovery = 0 - hp_recovery
          mp_recovery = 0 - mp_recovery
        end
        @damage -= hp_recovery                     # HP 回復量を差し引く
        if @obj.damage_to_mp or (@damage == 0 and mp_recovery != 0)
          @damage -= mp_recovery                   # MP 回復量を差し引く
          text = make_text_obj(true)
        else
          text = make_text_obj(false)
        end
      end
      self.contents.draw_text(112, WLH * 1, 176, WLH, text)
      @back_sprite.visible = true
    else
      @back_sprite.visible = false
    end
    self.visible = true
  end
  #--------------------------------------------------------------------------
  # ● 命中率（回避率含む）を返す
  #--------------------------------------------------------------------------
  def get_hit_rate
    hit = @target.unit.calc_hit(@attacker.unit, @obj)
    hit = hit * (100 - @target.unit.calc_eva(@attacker.unit, @obj)) / 100
    return hit
  end
  #--------------------------------------------------------------------------
  # ● 付加（解除）ステートをひとつ返す
  #--------------------------------------------------------------------------
  def get_state_text
    if @obj.plus_state_set.size > 0
      text = sprintf("+%s", $data_states[@obj.plus_state_set[0]].name)
    elsif @obj.minus_state_set.size > 0
      text = sprintf("-%s", $data_states[@obj.minus_state_set[0]].name)
    else
      text = ""
    end
    return text
  end
  #--------------------------------------------------------------------------
  # ● スキル、アイテムの結果テキスト作成
  #--------------------------------------------------------------------------
  def make_text_obj(mp_flag)
    if @obj.id == 3   # 防御
      text = sprintf("%s (100%%)", Vocab::guard)
    elsif @damage == 0 and @obj.base_damage == 0
      text = sprintf("%s (%d%%)", get_state_text, get_hit_rate)
    else
      text = sprintf("%+d (%d%%) %s", (0 - @damage), get_hit_rate, get_state_text)
    end
    text = sprintf("%s %s",Vocab::mp , text) if mp_flag
    return text
  end
  #--------------------------------------------------------------------------
  # ● 地形効果のテキスト作成
  #--------------------------------------------------------------------------
  def make_text_tile(x, y)
    tile_effect = $game_srpg.tile_effect(x, y)
    return TSRPG::TILE_EFFECT[tile_effect][0]
  end
end


