# SARA - Core
module SARA ; end
module SARA::TSRPG ; end
module SARA::TSRPG::Handlers ; end  
#==============================================================================#
# ** SARA::Container_Base
#==============================================================================#
class SARA::Container_Base
  
  #--------------------------------------------------------------------------#
  # * Public Instance Variables
  #--------------------------------------------------------------------------#  
  attr_accessor :x
  attr_accessor :y
  attr_accessor :z
  attr_accessor :width
  attr_accessor :height
  attr_accessor :opacity
  attr_accessor :index
  attr_accessor :item_max
  attr_accessor :column_max
  attr_accessor :active
  attr_accessor :visible 
  
  #--------------------------------------------------------------------------#
  # * overwrite method :initialize
  #--------------------------------------------------------------------------#
  def initialize( x=0, y=0, width=0, height=0 )
    @x, @y, @z, @width, @height, @opacity = x, y, 0, width, height, 255
    @index, @item_max, @column_max = 0, 0, 1
    @active = true
    @visible = true
  end
  
end 

#==============================================================================#
# ** RPG::Skill
#==============================================================================#
class RPG::Skill
  
  attr_accessor :group

  #--------------------------------------------------------------------------#
  # * new method :sara_skillcache
  #--------------------------------------------------------------------------#
  def sara_skillcache()
    @group = [:all]
    self.note.split(/[\r\n]+/).each { |line|
      case line
      when /<group:[ ]*(\w+)>/i
        @group << $1.to_s.downcase.to_sym
      end
    }  
  end
  
end  

#==============================================================================#
# ** Scene_Title
#==============================================================================#
class Scene_Title
  
  #--------------------------------------------------------------------------#
  # * new method :load_database
  #--------------------------------------------------------------------------#
  alias :saracore_load_database :load_database unless $@
  def load_database()
    saracore_load_database()
    $data_skills.each { |s| s.sara_skillcache() unless s.nil?() } 
  end
  
  #--------------------------------------------------------------------------#
  # * new method :load_bt_database
  #--------------------------------------------------------------------------#
  alias :saracore_load_bt_database :load_bt_database unless $@
  def load_bt_database()
    saracore_load_bt_database()
    $data_skills.each { |s| s.sara_skillcache() unless s.nil?() } 
  end
  
end  

#=*==========================================================================*=#
# ** END OF FILE
#=*==========================================================================*=#
