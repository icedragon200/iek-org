# SRPG - Window_Action(sqaue)
class Sprite_ActionTab < ::Sprite_Base
  
  def initialize( tab_type, viewport=nil )
    @tab_type = tab_type
    super( viewport )
    self.bitmap = Cache.picture("ActionTab_#{@tab_type}")
  end
  
end

#==============================================================================#
# ** Container_ActionTabs
#==============================================================================#
class Container_ActionTabs < SARA::Container_Base
  
  TABS = [
    :normal,
    :psi,
    :lifeforce,
    :all
  ]
  
  attr_accessor :viewport
  
  def initialize( x, y )
    super( x, y )
    @index = @last_index = 0
    @tabs = []
    TABS.each { |t|
      @tabs << Sprite_ActionTab.new( t, @viewport )
      @item_max += 1
    }
  end
    
  def dispose
    @tabs.each { |t| t.dispose() }
    @tabs.clear
  end
  
  def update
    for i in 0...@tabs.size
      tab = @tabs[i]
      tab.x = self.x + (i*36)
      tab.y = self.y + (i==@index ? 4 : 0)
      tab.z = self.z
      tab.visible = self.visible
      tab.viewport = self.viewport
      tab.update()
    end  
    if @last_index != @index
      @tabs[@index].start_animation( $data_animations[2] )
      @tabs[@index].flash( Color.new(255, 255, 255, 196), 120 )
      @last_index = @index
    end
    return unless self.visible 
    return unless self.active
    if Input.trigger?(Input::Y)
      Sound.play_cursor
      self.index = (self.index + 1) % self.item_max
    elsif Input.trigger?(Input::X)
      Sound.play_cursor
      self.index = (self.index - 1) % self.item_max
    end
  end
  
  def get_group()
    return TABS[self.index]
  end
  
end  
#==============================================================================#
# Window_Action
#==============================================================================#
class Window_Action < Window_Selectable
  
  ACT_DRAWMODE = 1
  
  alias :srpg_act_initialize :initialize unless $@
  def initialize( actor )
    @current_group = :normal
    @last_group    = :normal
    srpg_act_initialize( actor )
    @tabs = Container_ActionTabs.new( self.x+4, self.y+self.height-8 )
    @tabs.z = self.z + 1
    @tabs.update()
  end
  
  def refresh()
    @data = []
    @data.push( @actor.attack_skill() ) if include?( @actor.attack_skill() )
    @data.push( @actor.defend_skill() ) if include?( @actor.defend_skill() )
    @data.push( @actor.talk_skill() ) if include?( @actor.talk_skill() )
    for skill in @actor.skills
      next unless include?( skill )
      @data.push(skill)
      self.index = @data.size - 1 if skill.id == @actor.last_skill_id
    end
    @item_max   = 7*3 #@data.size
    @spacing    = 2
    @column_max = 7#[ @data.size, 1 ].max if ACT_DRAWMODE == 1
    self.width  = (@column_max*(32+@spacing))+32#Graphics.width
    self.height = 172
    #self.y      = Graphics.height - self.height - 96
    create_contents()
    for i in 0...@item_max do draw_item(i) end
    self.index = [ self.index, @item_max-1 ].min  
  end
   
  alias :srpg_act_dispose :dispose unless $@
  def dispose()
    @tabs.dispose()
    @tabs = nil
    srpg_act_dispose()    
  end
  
  alias :srpg_act_update :update unless $@
  def update()
    srpg_act_update()  
    @tabs.x = self.x+4
    @tabs.y = self.y+self.height-8
    @tabs.z = self.z+1
    @tabs.visible = (self.visible && self.openness > 0)
    @tabs.active  = self.active
    @tabs.update()
    @current_group = @tabs.get_group
    if @last_group != @current_group
      refresh()
      @last_group = @current_group
    end 
    self.index = self.index % [self.item_max, 1].max
  end
  
  def visible=( bool )
    super( bool )
    @tabs.visible = bool
  end  
  
  def active=( bool )
    super( bool )
    @tabs.active = bool
  end  
  
  def include?( skill )
    return false unless skill.battle_ok?()
    return false unless skill.group.include?( @current_group )
    return true
  end
  
if ACT_DRAWMODE == 1  
  
  def item_rect( index )
    rect = Rect.new(0, 0, 0, 0)
    rect.width  = 32 #(contents.width + @spacing) / @column_max - @spacing
    rect.height = 32
    rect.x = index % @column_max * (rect.width + @spacing )
    rect.y = index / @column_max * (rect.height + @spacing)
    return rect
  end
  
  alias :sara_scsrpg_winaction_refresh :refresh unless $@
  def refresh()
    sara_scsrpg_winaction_refresh()
    rect = Rect.new( 0, self.height-56, self.contents.width, 24 )
    draw_skill( rect, skill )
    @last_skill = skill
  end  
  
  def draw_item( index, enabled=true )
    rect = item_rect( index )
    obj  = @data[index]  
    rect2= rect.clone ; 
    rect2.x += 2 ; rect2.y += 2 ; 
    rect2.width -= 4 ; rect2.height -= 4 ;
    unless obj.nil?()
      draw_outlined_item( obj, rect2, 2, system_color, enabled?( obj ) )
    else
      draw_border_rect( rect2, 2, system_color )
    end  
  end
  
  alias :sara_scsrpg_winaction_update :update unless $@
  def update()
    sara_scsrpg_winaction_update()
    if skill != @last_skill
      refresh_help()
      @last_skill = skill
    end  
    if Input.trigger?( Input::L )
      Sound.play_cursor
      skill.level_down( true )
      refresh_help()
    elsif Input.trigger?( Input::R )  
      Sound.play_cursor
      skill.level_up( true )
      refresh_help()
    end unless skill.nil?()  
  end

  def refresh_help
    rect = Rect.new( 0, self.height-56, self.contents.width, 24 )
    draw_skill( rect, skill )
  end
  
  def draw_skill( rect, skill )
    self.contents.clear_rect( rect )
    return if skill.nil?()
    enabled = enabled?( skill )
    rect2 = rect.clone
    rect2.x += 28; rect2.y += 14 ; rect2.height = 4 ; rect2.width /= 4
    
    #draw_grad_bar
    draw_round_grad_bar( rect2.clone, skill.skl_unlocklevel-1, skill.skl_maxlevel-1, 
      normal_color, Color.new( 200, 200, 200 ), Color.new( 120, 120, 120 ),
      2, enabled )
    draw_round_grad_bar( rect2.clone, skill.skl_level-1, skill.skl_maxlevel-1, 
      mp_gauge_color1, mp_gauge_color2, Color.new( 120, 120, 120 ),
      2, enabled )
    # //  
    if IEO::SKILL_LEVEL::SKILL_MODE == 1 
      rect3 = rect2.clone
      rect3.x += 4 ; rect3.width += 16 ; rect3.y += 6 
      bc = skill.level_skl_exp ; bm = skill.next_skl_expr
      if (bm > 0) && !skill.unlocked_level?( skill.skl_maxlevel )
        draw_round_grad_bar( rect3.clone, bc, bm, 
          hp_gauge_color1, hp_gauge_color2, Color.new( 120, 120, 120 ),
          2, enabled )  
      end   
      rect3.height = WLH  ; rect3.y -= 16
      #self.contents.draw_text(rect3, sprintf("%s/%s", bc, bm))
    end    
    te = draw_obj_name( skill, rect.clone, enabled )
    rect4 = rect.clone 
    rect4.y -= 6 ; rect4.height = WLH ; rect4.x += te.width+28
    draw_obj_level( skill, rect4.clone, 0, enabled )  
    if $imported["IEO-CustomSkillCosts"]
      draw_obj_cost( skill, rect.clone, enabled )
    else
      rect.width -= 8 ; self.contents.draw_text( rect.clone, skill.mp_cost, 2 )
    end
  end

end  

  #--------------------------------------------------------------------------#
  # * kill method :cursor_pageup
  #--------------------------------------------------------------------------#   
  def cursor_pageup   ; end
  #--------------------------------------------------------------------------#
  # * kill method :cursor_pagedown
  #--------------------------------------------------------------------------# 
  def cursor_pagedown ; end
    
end
