# SPRG - Action Core
module SARA
  module TSRPG
    module REGEXP
      
      ANIMATION = /ANIMATION[ ](\d+)/i
      CHANGE    = /CHANGE[ ](.*)/i
      EFFECT    = /(.*)[ ]EFFECT/i
      EXECUTE   = /EXECUTE[ ](.*)/i
     
    end
    
    module ACTION
      
      # // Set the REGEX Module to a constant instead of continously using the long name
      AREGEX  = ::SARA::TSRPG::REGEXP
      ACTIONS = ::SARA::TSRPG::ACTION_LIST::ACTIONS
      
      module_function()
      
      def run_current_action()
        @action_index = 0
        @wait_count = 0
        update_current_action() until @action_index >= @current_action.size
      end
      
      def update_current_action()
        @wait_count = [@wait_count - 1, 0].max
        return unless @wait_count == 0
        @action_set = @current_action[@action_index]
        unless @action_set.nil?()
          raise "Warning!!! \n Parameter:\n (#{@action_set[1]})\n Is not an Array" if !@action_set[1].is_a?( Array )
          update_action( @action_set[0], @action_set[1] ) 
        end  
        @action_index += 1
      end
      
      def action?()
        return !@current_action.empty?() || @action_index < @current_action.size
      end
      
      def __action_obj
        return @action_obj
      end  
      
      def __action_user
        return @user
      end  
      
      def __action_target
        return @target
      end
      
      def __action_targets
        return @targets
      end
      
      def __action_original_user
        return @original_user
      end  
      
      def __action_original_target
        return @original_target
      end  
      
      def __action_original_targets 
        return @original_targets 
      end  
      
      def __set_target( new_target )
        @target = new_target
      end
      
      def __set_user( new_user )
        @user = new_user
      end
      
      def __set_targets( new_targets )
        @targets = new_targets
      end
      
      def __animation_lock
        return @animation_lock
      end
      
      def __animation_lock=( new_lock )
        @animation_lock = new_lock
      end
      
      def __action_update_basic()
        update_basic()
      end
      
      def __action_spriteset()
        return @spriteset
      end
      
      def update_action( action, parameters )
        case action.upcase
        when AREGEX::ANIMATION, "OBJ_ANIMATION", "ATK_ANIMATION"
          action_animation( action, parameters )
        when "CAST ANIMATION", "DOUBLE ANIME"  
          if __action_obj.double_anime > 0
            action_animation( "ANIMATION #{__action_obj.double_anime}", ["USER"] )
          end  
        when AREGEX::CHANGE
          action_change( action, parameters )
        when "COST", "CONSUME"
          action_cost( action, parameters )
        when "BACKATTACK", "BACK ATTACK"  
          action_backattack( action, parameters )
        when AREGEX::EFFECT
          action_effect( action, parameters )
        when AREGEX::EXECUTE
          action_execute( action, parameters )
        when "INVINCIBLE", "IMMORTAL"  
          action_immortal( action, parameters )
        when "SCRIPT"  
          action_script( action, parameters )
        when "TARGET"  
          action_target( action, parameters, false )
        when "TURN"  
          action_turn( action, parameters )
        when "PROJECTILE"  
          action_projectile( action, parameters )
        when "WAIT", "WAIT FOR ANIMATION", "WAIT FOR PROJECTILE" 
          action_wait( action, parameters )
        end  
      end
      
      def action_animation( action, parameters )
        case action.upcase
        when AREGEX::ANIMATION
          anim_id = $1.to_i
          action_target( "TARGET", [parameters[0]], true ).each { |target| 
            target.animation_id = anim_id }
        when "OBJ_ANIMATION"
          user = action_target( "TARGET", [parameters[0]], true )[0]
          action_target( "TARGET", [parameters[1]], true ).each { |t|
            __animation_lock = set_animation( t, user, __action_obj ) unless __animation_lock
          }  
        when "ATK_ANIMATION"
          user = action_target( "TARGET", [parameters[0]], true )[0]
          action_target( "TARGET", [parameters[1]], true ).each { |t|
            __animation_lock = set_animation( t, user ) unless __animation_lock
          }  
        end
        action_wait( "WAIT FOR ANIMATION", [] ) if parameters.any? { |s| s.to_s.upcase == "WAIT" }
      end
      
      def action_backattack( action, parameters )
        # // User
        user = action_target( "TARGET", [parameters[0]], true )[0]
        # // Target
        target = action_target( "TARGET", [parameters[1]], true )[0] 
        # // Operation
        case parameters[2].upcase
        when "CALC"
          user.back_attack?( target )    
        when "TRUE"  
          $game_temp.unit_back_attack = true
        when "FALSE"  
          $game_temp.unit_back_attack = false
        end  
      end
            
      def action_change( action, parameters )
        case action
        when AREGEX::CHANGE
          type = $1
        end  
        optargets = action_target( "TARGET", [parameters[0]], true )
        ch_proc = Proc.new { |opera, opval|
          result = opval
          case opera
          when /([\=\-\+\/\*])(\d+)([%%])?/i
            sign = $1
            val  = eval($2)
            perc = $3 != nil
            pval = result * val / 100 
            case sign
            when "=" ; result = perc ? pval : val
            when "-" ; result -= perc ? pval : val
            when "+" ; result += perc ? pval : val
            when "/" ; result /= perc ? pval : val
            when "*" ; result *= perc ? pval : val
            end  
          end
          result
        }
        case type.upcase
        when "HP", "LP"
          as_proc = Proc.new { |target, value|
            target.hp = value
          }
        when "MP", "SP"  
          as_proc = Proc.new { |target, value|
            target.mp = value
          }
        end  
        optargets.each { |optarget|
          case parameters[1]
          when "HP", "LP"
            as_proc.call( optarget.unit, 
             ch_proc.call( parameters[2], optarget.unit.hp ) )
          when "MAXHP", "MAXLP"
            as_proc.call( optarget.unit, 
             ch_proc.call( parameters[2], optarget.unit.maxhp ) )
          when "MP", "SP"
            as_proc.call( optarget.unit, 
             ch_proc.call( parameters[2], optarget.unit.mp ) )
          when "MAXMP", "MAXSP"    
            as_proc.call( optarget.unit, 
             ch_proc.call( parameters[2], optarget.unit.maxmp ) )
          end  
        }  
      end
      
      def action_cost( action, parameters )
        case action.upcase
        when "COST"
          # // User
          optargets = action_target( "TARGET", [parameters[0]], true )
          optargets.each { |optarget|
            if $imported["IEO-CustomSkillCosts"]
              optarget.unit.custom_skill_cost( __action_obj, :perform )
              optarget.unit.custom_skill_subcost( __action_obj, :perform )
            else  
              optarget.unit.mp -= user.unit.calc_mp_cost( __action_obj ) 
            end  
          }  
        when "CONSUME"  
          @user.unit.consume_item( __action_obj )
        end  
      end
      
      def action_effect( action, parameters )
        case action.upcase
        when AREGEX::EFFECT
          type   = $1
          # // parameters[0] User, parameters[1] Target, parameters[2] Object, Pop_Opertaion, Pop_Wait
          # // User
          user = action_target( "TARGET", [parameters[0]], true )[0]  
          # // Targets
          targets = action_target( "TARGET", [parameters[1]], true )
          # // Effects
          case type.upcase
          when "ATTACK"
            targets.each { |target|
              target.unit.attack_effect( user.unit )
              set_popup_actseq( target, user, [parameters[3], parameters[4]] )
              damage_event( target )
            }
          when "SKILL"
            obj = __action_obj
            obj = user.unit.action_get_skill( parameters[2] ) unless parameters[2].nil?
            targets.each { |target|
              target.unit.skill_effect( user.unit, obj )
              set_popup_actseq( target, user, [parameters[3], parameters[4]] )
              damage_event( target )
            }  
          when "ITEM"
            obj = __action_obj
            obj = user.unit.action_get_item( parameters[2] ) unless parameters[2].nil?
            targets.each { |target|
              target.unit.item_effect( user.unit, obj )
              set_popup_actseq( target, user, [parameters[3], parameters[4]] )
              damage_event( target )
            }  
          end  
        end 
      end
      
      def action_execute( action, parameters )
        case action
        when AREGEX::EXECUTE
          type = $1
          case type.upcase
          when "ATTACK"
            
          when "SKILL"
            
          when "ITEM"
            
          when "GUARD"
            
          end  
        end  
      end
      
      def action_immortal( action, parameters )
        # // User
        targets = action_target( "TARGET", [parameters[0]], true )
        targets.each { |target|
          case parameters[1].to_s.upcase
          when "TRUE", "ON", "YES"
            target.immortal = true
          when "FALSE", "OFF", "NO"  
            target.immortal = false
          end  
        }
      end
      
      def action_popup( action, parameters )
        set_popup_actseq
      end
      
      def action_script( action, parameters )
        target  = @target
        targets = @targets
        user    = @user
        eval( parameters[0] )
      end
      
      def action_steal( action, parameters )
      end
        
      def action_target( action, parameters, return_only=false )
        case parameters[0].upcase
        # // Pure Target Types
        when "TARGET"
          targets   = [ __action_target ]
        when "ORIGINALTARGET", "ORIGINAL_TARGET", "ORIGINAL TARGET"  
          targets   = [ __action_original_target ]
        when "USER"  
          targets   = [ __action_user ]
        when "ORIGINALUSER", "ORIGINAL_USER", "ORIGINAL USER"  
          targets   = [ __action_original_user ]  
        when "TARGETS"  
          targets   = __action_targets 
        when "ORIGINALTARGETS", "ORIGINAL_TARGETS", "ORIGINAL TARGETS"  
          targets   = __action_original_targets 
        when "ALLIES"  
          targets   = __action_user.action.friends_unit.existing_members()
        when "ENEMIES"  
          targets   = __action_user.action.opponents_unit.existing_members() 
        when "DEADALLIES", "DEAD_ALLIES", "DEAD ALLIES"  
          targets   = __action_user.action.friends_unit.dead_members()
        when "DEADENEMIES", "DEAD_ENEMIES", "DEAD ENEMIES"    
          targets   = __action_user.action.opponents_unit.dead_members()  
        # // Derived Target Types  
        when "EVERYONE", "EVERYBODY"  
          targets   = action_target( "TARGET", ["ALLIES"], true ) +
           action_target( "TARGET", ["ENEMIES"], true )
        when "DEAD"
          targets   = action_target( "TARGET", ["DEAD ALLIES"], true ) +  
            action_target( "TARGET", ["DEAD ENEMIES"], true )
        when "NONTARGET", "NON_TARGET", "NON TARGET"
          targets   = action_target( "TARGET", ["EVERYONE"], true ) -
           action_target( "TARGET", ["TARGET"], true )   
        when "NONTARGETS", "NON_TARGETS", "NON TARGETS"
          targets   = action_target( "TARGET", ["EVERYONE"], true ) -
           action_target( "TARGET", ["TARGETS"], true )
        when "NONUSER", "NON_USER", "NON USER"   
          targets   = action_target( "TARGET", ["EVERYONE"], true ) -
           action_target( "TARGET", ["USER"], true )
        when "RANDTARGET", "RAND_TARGET", "RAND TARGET"  
          targets   = [action_target( "TARGET", ["TARGETS"], true ).randomize()[0]]  
        when "RANDALLY", "RAND_ALLY", "RAND ALLY"  
          targets   = [action_target( "TARGET", ["ALLIES"], true ).randomize()[0]]  
        when "RANDENEMY", "RAND_ENEMY", "RAND ENEMY"    
          targets   = [action_target( "TARGET", ["ENEMIES"], true ).randomize()[0]]   
        when "RANDOM"
          targets   = [action_target( "TARGET", ["EVERYONE"], true ).randomize()[0]]    
        end
        targets = targets#.uniq.compact
        case parameters[1].upcase
        when "TARGET"
          __set_target( targets[0] )
        when "USER"
          __set_user( targets[0] )
        when "TARGETS"  
          __set_targets( targets )
        end unless return_only
        return targets
      end
      
      def action_turn( action, parameters )
      end
      
      def action_projectile( action, parameters )
        # // User
        user = action_target( "TARGET", [parameters[0]], true )[0]  
        # // Targets
        targets = action_target( "TARGET", [parameters[1]], true )
        actioncodez = SARA::TSRPG::ACTION_LIST.get_action( parameters[2] )
        targets.each { |t|
          prj = $game_srpg.add_projectile()
          prj.current_action = actioncodez.clone
          prj.moveto( user.x, user.y, true )
          prj.jump_to_xy( t.x, t.y )
          prj.set_target_xy( t.x, t.y )
        }  
      end
      
      def action_wait( action, parameters )
        case action.upcase
        when "WAIT"
          @wait_count = parameters[0].to_i
        when "WAIT FOR ANIMATION"
          wait_for_animation()
        when "WAIT FOR PROJECTILE"  
          loop do 
            __action_update_basic() 
            break if $game_srpg.projectiles_complete?()
          end  
        end  
      end      
      
      #--------------------------------------------------------------------------#
      # * new_method :set_popup_actseq
      #--------------------------------------------------------------------------#
      def set_popup_actseq( target, attacker, parameters )
        case parameters[0].upcase
        when "WAIT"
          wait_for_animation()
        when "SKIP" ; return
        when "CONTINUE"
        end  
        obj = attacker.unit.action.obj
        $game_srpg.popup_action_result(target, obj)
        if target.unit.absorbed
          if target.unit.hp_damage >= 0 or parameters[3].to_s.upcase == "HP"
            $game_srpg.add_popup(attacker.real_x, attacker.real_y,
            target.unit.hp_damage.to_s, ::TSRPG::COLOR_HP_GAIN)
          end  
          if target.unit.mp_damage >= 0 or parameters[3].to_s.upcase == "MP"
            $game_srpg.add_popup(attacker.real_x, attacker.real_y,
            target.unit.mp_damage.to_s, ::TSRPG::COLOR_MP_GAIN)
          end
        end
        case parameters[1].upcase
        when "WAIT"
          wait(::TSRPG::ACTION_AFTER_WAIT) unless Input.press?(Input::Z)
        when "SKIP" ; return
        when "CONTINUE"
        end
      end
      
    end # // ACTION  
  end # // TSRPG
end # // SARA 

class RPG::BaseItem
  
  attr_accessor :start_action
  attr_accessor :whole_action
  attr_accessor :target_action
  attr_accessor :finish_action
  
  def sara_act_baseitemcache 
    @start_action  = nil
    @whole_action  = nil
    @target_action = nil
    @finish_action = nil
    @__reading_action_name = :nil
    action_inject = proc { |name, arra|
      case name
      when :start  ; (@start_action  ||= []) << arra
      when :target ; (@target_action ||= []) << arra
      when :whole  ; (@whole_action  ||= []) << arra
      when :finish ; (@finish_action ||= []) << arra
      end  
    }
    self.note.split(/[\r\n]+/).each { |line|
      case line
    # ---------------------------------------------------------------------- #
      when /<start action=(.*)>/i
        @start_action  = SARA::TSRPG::ACTION_LIST.get_action( $1 )
      when /<target action=(.*)>/i
        @target_action = SARA::TSRPG::ACTION_LIST.get_action( $1 )
      when /<whole action=(.*)>/i
        @whole_action  = SARA::TSRPG::ACTION_LIST.get_action( $1 )
      when /<finish action=(.*)>/i
        @finish_action = SARA::TSRPG::ACTION_LIST.get_action( $1 )
      when /<start action>/i  
        @__reading_action_name = :start
      when /<\/start action>/i  
        @__reading_action_name = :nil
      when /<target action>/i  
        @__reading_action_name = :target
      when /<\/target action>/i
        @__reading_action_name = :nil
      when /<whole action>/i  
        @__reading_action_name = :whole
      when /<\/whole action>/i
        @__reading_action_name = :nil
      when /<finish action>/i  
        @__reading_action_name = :finish
      when /<\/finish action>/i  
        @__reading_action_name = :nil
      else 
        next if @__reading_action_name == :nil
        case line
        # // Action 8
        when /(.*):[ ](.*),[ ](.*),[ ](.*),[ ](.*),[ ](.*),[ ](.*),[ ](.*),[ ](.*)/i  
          action_inject.call( @__reading_action_name, [$1, [$2,$3,$4,$5,$6,$7,$8,$9]] )
        # // Action 7
        when /(.*):[ ](.*),[ ](.*),[ ](.*),[ ](.*),[ ](.*),[ ](.*),[ ](.*)/i  
          action_inject.call( @__reading_action_name, [$1, [$2,$3,$4,$5,$6,$7,$8]] )
        # // Action 6
        when /(.*):[ ](.*),[ ](.*),[ ](.*),[ ](.*),[ ](.*),[ ](.*)/i  
          action_inject.call( @__reading_action_name, [$1, [$2,$3,$4,$5,$6,$7]] )
        # // Action 5
        when /(.*):[ ](.*),[ ](.*),[ ](.*),[ ](.*),[ ](.*)/i  
          action_inject.call( @__reading_action_name, [$1, [$2,$3,$4,$5,$6]] )
        # // Action 4
        when /(.*):[ ](.*),[ ](.*),[ ](.*),[ ](.*)/i  
          action_inject.call( @__reading_action_name, [$1, [$2,$3,$4,$5]] )
        # // Action 3
        when /(.*):[ ](.*),[ ](.*),[ ](.*)/i   
          action_inject.call( @__reading_action_name, [$1, [$2,$3,$4]] )
        # // Action 2
        when /(.*):[ ](.*),[ ](.*)/i 
          action_inject.call( @__reading_action_name, [$1, [$2,$3]] )
        # // Action 1
        when /(.*):[ ](.*)/i      
          action_inject.call( @__reading_action_name, [$1, [$2]] )
        # // Action 
        else
          action_inject.call( @__reading_action_name, [line, []] )
        end  
      end
    # ---------------------------------------------------------------------- #
    }  
    case self
    when RPG::Skill
      @start_action  ||= SARA::TSRPG::ACTION_LIST.get_action( "SkillStart" )
      @whole_action  ||= SARA::TSRPG::ACTION_LIST.get_action( "SkillWhole" )
      @target_action ||= SARA::TSRPG::ACTION_LIST.get_action( "SkillTarget" )
      @finish_action ||= SARA::TSRPG::ACTION_LIST.get_action( "SkillEnd" )
    when RPG::Item
      @start_action  ||= SARA::TSRPG::ACTION_LIST.get_action( "ItemStart" )
      @whole_action  ||= SARA::TSRPG::ACTION_LIST.get_action( "ItemWhole" )
      @target_action ||= SARA::TSRPG::ACTION_LIST.get_action( "ItemTarget" )
      @finish_action ||= SARA::TSRPG::ACTION_LIST.get_action( "ItemEnd" )
    else  
      @start_action||=[];@whole_action||=[]
      @target_action||=[];@finish_action||=[]
    end 
    @__reading_action = false
  end
  
end
  
#==============================================================================#
# ** Scene_Title
#==============================================================================#
class Scene_Title < Scene_Base
  
  #--------------------------------------------------------------------------#
  # * alias method :load_database
  #--------------------------------------------------------------------------#
  alias :srpg_sct_load_database :load_database unless $@
  def load_database()
    srpg_sct_load_database()
    load_srpg_cache()
  end
  
  #--------------------------------------------------------------------------#
  # * alias method :load_bt_database
  #--------------------------------------------------------------------------#
  alias :srpg_sct_load_bt_database :load_database unless $@
  def load_bt_database()
    srpg_sct_load_bt_database()
    load_srpg_cache()
  end  
  
  #--------------------------------------------------------------------------#
  # * new method :load_srpg_cache
  #--------------------------------------------------------------------------#
  def load_srpg_cache()
    objs = [ $data_items, $data_skills, $data_weapons ]
    objs.each { |group| group.each { |obj|
        next if obj.nil?() 
        obj.sara_act_baseitemcache() if obj.is_a?( RPG::BaseItem )  
      } 
    }
  end
  
end

class Scene_Srpg < Scene_Base
  
  include ::SARA::TSRPG::ACTION

  attr_accessor :spriteset
  attr_accessor :action_obj
  attr_accessor :user
  attr_accessor :target
  attr_accessor :targets
  attr_accessor :original_user
  attr_accessor :original_target
  attr_accessor :original_targets
  attr_accessor :animation_lock
  
end  
