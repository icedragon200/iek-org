# SRPG - Rewrites
module SARA
  module TSRPG
  
    MOVE = "M1"
    #           //[0, 1, 2, 3, 4, 5, 6, 7, 8]
    ACTOR_MOVES = [3, 3, 3, 3, 3, 3, 3, 3, 3]
  
    module_function
  
    def get_actor_move(actor_id)
      return ACTOR_MOVES[actor_id] unless ACTOR_MOVES[actor_id].nil?
      return ACTOR_MOVES[0]
    end
    
  end
end

class IEO::SKILL_LEVEL::Level_Struct
  
  (nw = [:atk_range, :atk_range_type, :atk_size, :atk_size_type, 
    :atk_hole, :atk_size_hole, :damage]).each { |a| attr_accessor a }
  ATTRS += nw
  
end

class RPG::BaseItem
  
  attr_writer :damage
  attr_writer :maxen
  
  #--------------------------------------------------------------------------#
  # * new-method :atk_size_hole
  #--------------------------------------------------------------------------#  
  def atk_size_hole()
    @atk_size_hole ||= self.note =~ /<size_hole=(\d+)>/i ? $1.to_i : -1
    return @atk_size_hole
  end
  
  #--------------------------------------------------------------------------#
  # * new-method :damage
  #--------------------------------------------------------------------------#  
  def damage()
    @damage ||= self.note =~ /<damage:[ ](.*)>/i ? $1 : case self ; 
                                                        when RPG::Skill ; "NORMALSKILL"
                                                        when RPG::Weapon; "NORMALATTACK"   
                                                        else ; ""  
                                                        end   
    return @damage                                                    
  end
 
  #--------------------------------------------------------------------------#
  # * new-method :custom_stat_set
  #--------------------------------------------------------------------------#  
  def custom_stat_set( stat, value )
    @maxen ||= 0
    case stat.upcase
    when "EN", "MAXEN"
      @maxen = value
    end  
  end
  
  def maxen()
    return (@maxen ||= 0)
  end
  
end

class RPG::State
  
  attr_writer :maxen_level_mod
  
  def custom_stat_lvl_mod( stat, val )
    @maxen_level_mod ||= 0
    case stat.upcase
    when "EN", "MAXEN"
      @maxen_level_mod = value
    end  
  end
  
  def maxen_level_mod()
    return (@maxen_level_mod ||= 0)
  end
  
end

class RPG::Enemy
  
  #--------------------------------------------------------------------------#
  # * new-method :atk_size_hole
  #--------------------------------------------------------------------------#  
  def atk_size_hole()
    @atk_size_hole ||= @note =~ /<size_hole=(\d+)>/i ? $1.to_i : -1
    return @atk_size_hole
  end
  
end

class RPG::Skill
  
  #--------------------------------------------------------------------------#
  # * new-method :setup_skl_custom_type
  #--------------------------------------------------------------------------# 
  def setup_skl_custom_type( type, value )
    result = nil
    case type.upcase() 
    when "RANGE"
      result = [:atk_range, value]
    when "RANGETYPE", "RANGE_TYPE", "RANGE TYPE"  
      result = [:atk_range_type, value]
    when "SIZE"  
      result = [:atk_size, value]
    when "SIZETYPE", "SIZE_TYPE", "SIZE TYPE"  
      result = [:atk_size_type, value] 
    when "HOLE"  
      result = [:atk_hole, value] 
    when "SIZEHOLE", "SIZE_HOLE", "SIZE HOLE"  
      result = [:atk_size_hole, value]  
    when "DAMAGE"  
      result = [:damage, value]  
    end  
    return result
  end
  
  #--------------------------------------------------------------------------#
  # * new-method :build_firstlevel_cache_custom
  #--------------------------------------------------------------------------#   
  def build_firstlevel_cache_custom()
    # // Prepare all the default parameters
    atk_range()
    atk_range_type()
    atk_size()
    atk_size_type()
    atk_hole()
    atk_size_hole()
    damage()
    # // Start real caching
    @skl_levelcache[@skl_min_level][:atk_range]      = @atk_range
    @skl_levelcache[@skl_min_level][:atk_range_type] = @atk_range_type
    @skl_levelcache[@skl_min_level][:atk_size]       = @atk_size
    @skl_levelcache[@skl_min_level][:atk_size_type]  = @atk_size_type
    @skl_levelcache[@skl_min_level][:atk_hole]       = @atk_hole
    @skl_levelcache[@skl_min_level][:atk_size_hole]  = @atk_size_hole
    @skl_levelcache[@skl_min_level][:damage]         = @damage
  end 
  
  #--------------------------------------------------------------------------#
  # * new-method :build_custom_mainsklcache
  #--------------------------------------------------------------------------#   
  def build_custom_mainsklcache( index, procs )
    level_proc, atlevel_proc, atlevel_array_proc = *procs
    i = index
    # // Perlevel
    @skl_levelcache[i][:atk_range]      = level_proc.call( @skl_levelcache[i][:atk_range]      , @skl_perlevelmod[:atk_range] )
    @skl_levelcache[i][:atk_range_type] = level_proc.call( @skl_levelcache[i][:atk_range_type] , @skl_perlevelmod[:atk_range_type] )
    @skl_levelcache[i][:atk_size]       = level_proc.call( @skl_levelcache[i][:atk_size]       , @skl_perlevelmod[:atk_size] )
    @skl_levelcache[i][:atk_size_type]  = level_proc.call( @skl_levelcache[i][:atk_size_type]  , @skl_perlevelmod[:atk_size_type] )
    @skl_levelcache[i][:atk_hole]       = level_proc.call( @skl_levelcache[i][:atk_hole]       , @skl_perlevelmod[:atk_hole] )
    @skl_levelcache[i][:atk_size_hole]  = level_proc.call( @skl_levelcache[i][:atk_size_hole]  , @skl_perlevelmod[:atk_size_hole] )
    #@skl_levelcache[i][:damage]         = level_proc.call( @skl_levelcache[i][:damage]         , @skl_perlevelmod[:damage] )
    
    # // At Level
    @skl_levelcache[i][:atk_range]      = atlevel_proc.call( @skl_levelcache[i][:atk_range]      ,@skl_atlevelmod[i][:atk_range] )      
    @skl_levelcache[i][:atk_range_type] = atlevel_proc.call( @skl_levelcache[i][:atk_range_type] ,@skl_atlevelmod[i][:atk_range_type] ) 
    @skl_levelcache[i][:atk_size]       = atlevel_proc.call( @skl_levelcache[i][:atk_size]       ,@skl_atlevelmod[i][:atk_size] )       
    @skl_levelcache[i][:atk_size_type]  = atlevel_proc.call( @skl_levelcache[i][:atk_size_type]  ,@skl_atlevelmod[i][:atk_size_type] )  
    @skl_levelcache[i][:atk_hole]       = atlevel_proc.call( @skl_levelcache[i][:atk_hole]       ,@skl_atlevelmod[i][:atk_hole] )       
    @skl_levelcache[i][:atk_size_hole]  = atlevel_proc.call( @skl_levelcache[i][:atk_size_hole]  ,@skl_atlevelmod[i][:atk_size_hole] )       
    @skl_levelcache[i][:damage]         = @skl_atlevelmod[i][:damage] unless @skl_atlevelmod[i][:damage].nil?()
  end  
  
  #--------------------------------------------------------------------------#
  # * new-method :change_level_custom
  #--------------------------------------------------------------------------#  
  def change_level_custom( new_level )
    @atk_range      = @skl_levelcache[new_level][:atk_range] 
    @atk_range_type = @skl_levelcache[new_level][:atk_range_type] 
    @atk_size       = @skl_levelcache[new_level][:atk_size] 
    @atk_size_type  = @skl_levelcache[new_level][:atk_size_type] 
    @atk_hole       = @skl_levelcache[new_level][:atk_hole] 
    @atk_size_hole  = @skl_levelcache[new_level][:atk_size_hole] 
    @damage         = @skl_levelcache[new_level][:damage]
  end 
  
end

class Game_Battler
  
  def action_get_skill( skill_id )
    unless @action.skill.nil?
      return @action.skill if @action.skill.id == skill_id
    end  
    skills.each { |sk| return sk if sk.id == skill_id } if self.actor?
    return $data_skills[skill_id]
  end
  
  def action_get_item( item_id )
    unless @action.item.nil?
      return @action.item if @action.item.id == item_id
    end  
    return $data_items[item_id]
  end
  
  def actor? ; return false end

end
  
#==============================================================================
# ■ Game_BattleAction
#==============================================================================
class Game_BattleAction
  
  #--------------------------------------------------------------------------
  # ○ 行動内容のオブジェクトを返す
  #--------------------------------------------------------------------------
  def obj
    if attack?
      result = @battler #.attack_skill()           
    elsif skill?
      result = skill              
    elsif item?
      result = item               
    elsif guard?
      result = @battler.defend_skill()    
    end
    return result
  end
  
  #--------------------------------------------------------------------------
  # * Set Normal Attack
  #--------------------------------------------------------------------------
  def set_attack
    #@kind = 0
    #@basic = 0
    set_skill( 1 )
  end
  
end

class Game_Battler
  
  attr_accessor :__popstack
  
  alias :isara001_gb_initialize :initialize unless $@
  def initialize()
    @_move_power = 0
    @__popstack = []
    isara001_gb_initialize()
  end
  
  def move_power_bonus
    n = 0
    for state in states do n += state.move_power end
    n = apply_tile_effect(n, SARA::TSRPG::MOVE)      # 地形効果による補正を適用
    return n
  end 
  
  #--------------------------------------------------------------------------#
  # * new_method :move_power
  #--------------------------------------------------------------------------#
  def move_power
    return @_move_power + move_power_bonus
  end
  
  #--------------------------------------------------------------------------#
  # * overwrite_method :agi
  #--------------------------------------------------------------------------#
  def agi
    n = [[base_agi + @agi_plus, 0].max, 999].min
    for state in states do n *= state.agi_rate / 100.0 end
    n = apply_tile_effect(n, TSRPG::AGI)      # 地形効果による補正を適用
    n = [[Integer(n), 0].max, 999].min
    return n
  end
  
  def attack_skill() 
    return $data_skills[1] 
  end
  
  def talk_skill()
    return $data_skills[2]
  end
  
  def defend_skill()
    return $data_skills[3]
  end
  
  def consume_item( item ) ; end
  
  def has_item?( item ) ; return false ; end
   
  def on_slip_damage()
    if @hp_damage != 0
      @__popstack << [:hp, @hp_damage]
    end  
    if @mp_damage != 0
      @__popstack << [:mp, @mp_damage]
    end  
    if @en_damage != 0
      @__popstack << [:en, @en_damage]
    end  
    if @ammo_damage != 0
      @__popstack << [:ammo, @ammo_damage]
    end  
  end
  
end

class Game_Enemy < Game_Battler
 
  alias :isara001_ge_initialize :initialize unless $@
  def initialize( index, enemy_id )
    isara001_ge_initialize( index, enemy_id )
    @_move_power = 3 #SARA_TSRPG.get_actor_move( )
  end
  
  #--------------------------------------------------------------------------#
  # * new_method :move_power
  #--------------------------------------------------------------------------#
  def move_power
    return @_move_power + move_power_bonus
  end
  
  def atk_size_hole
    return enemy.atk_size_hole
  end
  
end

class Game_Actor < Game_Battler
  
  alias :isara001_ga_initialize :initialize unless $@
  def initialize( actor_id )
    @core_skills = [] 
    isara001_ga_initialize( actor_id )
    @core_skills[0] = $data_skills[1].clone # // Attack
    @core_skills[1] = $data_skills[2].clone # // Talk
    @core_skills[2] = $data_skills[3].clone # // Defend
    if $imported["IEO-SkillLevelSystem"]
      @core_skills[0].skl_skill = true
      @core_skills[1].skl_skill = true
      @core_skills[2].skl_skill = true
    end  
    @_move_power = SARA::TSRPG.get_actor_move( @actor_id )
  end
  
  #--------------------------------------------------------------------------#
  # * new_method :move_power
  #--------------------------------------------------------------------------#
  def move_power()
    return @_move_power + move_power_bonus
  end
  
  def attack_skill() 
    return @core_skills[0] 
  end
  
  def talk_skill()
    return @core_skills[1] 
  end
  
  def defend_skill()
    return @core_skills[2] 
  end
  
  #--------------------------------------------------------------------------#
  # * alias method :match_skl_skill_to_id
  #--------------------------------------------------------------------------#
  alias :isara001_ga_match_skl_skill_to_id :match_skl_skill_to_id unless $@
  def match_skl_skill_to_id( skill_id )
    case skill_id
    when 1 ; return attack_skill()
    when 2 ; return talk_skill()
    when 3 ; return defend_skill()
    else ; return isara001_ga_match_skl_skill_to_id( skill_id )
    end  
  end
  
  def consume_item( item )
    $game_party.consume_item( item )
  end
 
  def has_item?( item )
    return $game_party.has_item?( item )
  end
  
end

class Game_Party
  
  def on_player_walk
    #for actor in members
    #  if actor.slip_damage?
    #    actor.hp -= 1 if actor.hp > 1   # Poison damage
    #    $game_map.screen.start_flash(Color.new(255,0,0,64), 4)
    #  end
    #  if actor.auto_hp_recover and actor.hp > 0
    #    actor.hp += 1                   # HP auto recovery
    #  end
    #end
  end
  
end

#==============================================================================
# Scene_Srpg
#==============================================================================
class Scene_Srpg < Scene_Base
  
  def execute_action( x, y )
    # 攻撃可能セルが選択されていなければ終了
    unless @attack_cell.include?([x, y])
      Sound.play_buzzer
      return
    end
    # 現在の効果範囲上に生きている誰かがいれば実行
    @action_result_exp = 0
    @action_result_item.clear
    @action_result_gold = 0
    targets = []                        # ターゲットの配列を作成
    @coop_unit_list = []
    for pos in @effect_cell
      event = $game_srpg.target_xy(x + pos[0], y + pos[1])[0]
      targets.push(event) if event != nil
    end
    obj = @event.unit.action.obj
    if obj.is_a?(RPG::BaseItem) and obj.dual? # 連続攻撃なら同じターゲットを追加
      targets += targets
    end
    if targets.empty?   # ターゲットがいない
      Sound.play_buzzer
      @message_mini_window.refresh("Invalid Target", 60)
      return
    end
    @event.turn_cell(x, y)              # 攻撃する方向を向く
    level = 0
    if @event.unit.action.attack?       # 通常攻撃
      level = execute_action_attack(targets)
    elsif @event.unit.action.skill?     # スキル
      level = execute_action_obj(targets)
    elsif @event.unit.action.item?      # アイテム
      level = execute_action_obj(targets)
    end
    @event.action_end = true              # 行動済みフラグを立てる
    unless $game_srpg.enemy_turn?         # 味方ターン限定の処理
      $game_srpg.scene_state = 0            # ニュートラルへ移行する
      @status_window.refresh($game_srpg.alive_unit_xy(x, y))  # ステータスウィンドウの再描画
    end
    unless @event.enemy_flag              # 味方行動時限定の処理
      for event in @coop_unit_list          # 協力攻撃者も経験値取得
        next if event.enemy_flag
        exp = @action_result_exp + event.unit.action_exp
        exp = [exp + exp * (level - event.unit.level) * 10 / 100, 1].max
        event.unit.gain_exp(exp / 2 + 1, false)
        event.unit.gain_job_exp(exp, false) if $include_tjex
        text = sprintf("EXP+%d", exp / 2 + 1)
        $game_srpg.add_popup(event.real_x, event.real_y, text, Color.new(255, 255, 255), 1)
      end
      exp = @action_result_exp + @event.unit.action_exp
      exp = [exp + exp * (level - @event.unit.level) * 10 / 100, 1].max
      old_uni = @event.unit.deep_clone
      @event.unit.gain_exp(exp, false)
      @event.unit.gain_job_exp(exp, false) if $include_tjex
      display_levelup( old_uni, @event.unit ) if old_uni.level < @event.unit.level
      text = sprintf("EXP+%d", exp)
      $game_srpg.add_popup(@event.real_x, @event.real_y, text, Color.new(255, 255, 255), 1)
    end
    if @action_result_gold > 0        # お金の取得
      $game_party.gain_gold(@action_result_gold)
      text = sprintf(Vocab::ObtainGold, @action_result_gold, Vocab::gold)
      $game_message.texts.push(text)
    end
    if @action_result_item.size > 0   # アイテムの取得
      for item in @action_result_item
        $game_party.gain_item(item, 1)
        text = sprintf(Vocab::ObtainItem, item.name)
        $game_message.texts.push(text)
      end
    end
    wait_for_message
  end
  
  #--------------------------------------------------------------------------
  # ● 通常攻撃の実行
  #--------------------------------------------------------------------------
  def execute_action_attack(targets)
    level = 0
    animation_lock = false
    for event in targets
      @event.back_attack?(event)                    # 背後を取っているかチェック
      event.unit.attack_effect(@event.unit)         # 通常攻撃の効果適用
      if level == 0 and @event.unit.mp >= @event.unit.atk_mp_cost
        @event.unit.mp -= @event.unit.atk_mp_cost   # MP消費
      end
      animation_lock = set_animation(event, @event) unless animation_lock
      @message_mini_window.refresh(Vocab::attack)
      set_popup(event, @event)
      coop(event)
      counter(event)
      event.damage(@event)
      event.refresh_state                           # キャラクターの状態を更新
      level += (event.unit.level == 0 ? @event.unit.level : event.unit.level)
    end
    return level / targets.size  # 行動対象の平均レベルを返す
  end
  
  def setup_actions( type )
    @start_action  = [] # // Handles setup
    @whole_action  = [] # // Handles multi target actions
    @target_action = [] # // Handles single target actions
    @finish_action = [] # // Handles Finalization
    @current_action= [] # // Is a clone of the action set currently being done.
    
    @action_set    = [] # // [0] Action, [1] Parameters
  end
  
  #--------------------------------------------------------------------------#
  # * new-method :perform_skl_p_gain
  #--------------------------------------------------------------------------#  
  def perform_skl_p_gain( type = :attack, battler = nil )
    return if battler.nil?()
    IEO::SKILL_LEVEL.skl_p_gain( type, battler ) if $imported["IEO-SkillLevelSystem"]
  end
    
  def execute_action_obj( targets )
    if @event.unit.action.attack?()
      type = :attack
    elsif @event.unit.action.skill?()
      type = :skill
    elsif @event.unit.action.item?()
      type = :item
    elsif @event.unit.action.guard?()
      type = :guard
    end  
    return execute_action_sequence( type, [targets] )
  end
  
  def execute_action_sequence( type, parameters )
    targets = parameters[0]
    return_type = :normal
    @return_level  = 0
    @animation_lock = false
    obj = @event.unit.action.obj
    @action_obj = obj
    
    if @event.unit.action.skill?()
      return 0 unless @event.unit.custom_skill_cost( @action_obj, :can_use )
      return 0 unless @event.unit.custom_skill_subcost( @action_obj, :can_use )
      @event.unit.custom_skill_cost( @action_obj, :perform )
      @event.unit.custom_skill_subcost( @action_obj, :perform )
    elsif @event.unit.action.item?()
      return 0 unless @event.unit.has_item?( @action_obj )
      @event.unit.consume_item( @action_obj )
    end  
    
    # // Setup Combatants
    @original_user    = @user    = @event
    @original_targets = @targets = targets
    @original_target  = @target  = nil
    
    # // Setup Actions
    setup_actions( :startup )
    case type
    when :attack
      @start_action = ACTIONS["AttackStart"]
      @whole_action = ACTIONS["AttackWhole"]
      @target_action= ACTIONS["AttackTarget"]
      @finish_action= ACTIONS["AttackEnd"]
    when :skill, :item 
      @start_action = obj.start_action  #ACTIONS[ obj.start_action  ]
      @whole_action = obj.whole_action  #ACTIONS[ obj.whole_action  ]
      @target_action= obj.target_action #ACTIONS[ obj.target_action ]
      @finish_action= obj.finish_action #ACTIONS[ obj.finish_action ]
    when :guard
      @start_action = ACTIONS["GuardStart"]
      @whole_action = ACTIONS["GuardWhole"]
      @target_action= ACTIONS["GuardTarget"]
      @finish_action= ACTIONS["GuardEnd"]
    end
    # // Start Action
    @current_action = @start_action.clone()
    run_current_action()
    
    # // Whole Action
    @current_action = @whole_action.clone()
    run_current_action()
    
    # // Target Action
    @targets.each { |event|
      if obj.is_a?(RPG::Skill)
        @message_mini_window.refresh(obj.name)        # スキル（アイテム）名表示
        if obj.id == 1
          perform_skl_p_gain( :attack, @user.unit )
        elsif obj.id == 2                              # 会話
          talk(event)
          return_type = :talk
        elsif obj.id == 3                           # 防御
          execute_action_guard()
          return_type = :guard
          perform_skl_p_gain( :guard, @user.unit )
        else  
          perform_skl_p_gain( :skill, @user.unit )
        end
      
        # // Process Relflect
        if event.unit.reflect?                      # 魔法反射
          if obj.spi_f > 0 and not obj.physical_attack and
             not obj.pierce? and @event.id != event.id
            event.animation_id = TSRPG::ANIME_REFLECT
            wait_for_animation()      # アニメーション終了待ち
            level += @event.unit.level
            targets.push(@event)
            next
          end
        end 
      else
        perform_skl_p_gain( :item, @user.unit ) 
      end  
      # // Action Process
      @original_target = @target = event
      @current_action = @target_action.clone
      run_current_action()
      
      # // Perform After Target Effects
      if event.unit.hp_damage > 0
        steal(event) if rand(100) < obj.steal_rate  # 盗む
        knock_back(event, obj)                      # ノックバック
      end
      @event.move_end = true if obj.nomove?         # 移動不可
      event.reset if obj.for_dead_friend?           # 蘇生処理
      if obj.common_event_id > 0                    # コモンイベント
        $game_temp.common_event_id = obj.common_event_id
        $game_temp.common_event_user = @event.id
      end
      # // Level Calc
      @return_level += (event.unit.level == 0 ? @event.unit.level : event.unit.level)
    }
    
    # // Finish Action
    @current_action = @finish_action.clone
    run_current_action()
    
    # // Closing Stuff
    @action_obj = nil
    @user       = nil
    @targets    = []
    @target     = nil
    @action_index = 0
    setup_actions( :clear ) # // Clear Actions
    
    level = @return_level
    @return_level = 0
    case return_type
    when :guard
      return @event.unit.level-5 # 自分のレベルを返す
    when :talk
      return @event.unit.level  # 自分のレベルを返す
    else # // :normal
      return level / targets.size  # 行動対象の平均レベルを返す
    end     
  end
  
  def damage_event( event )
    event.damage( @event )
    event.refresh_state()                           # キャラクターの状態を更新
  end
  
  #--------------------------------------------------------------------------#
  # * overwrite_method :update_skill_window
  #--------------------------------------------------------------------------#
  def update_skill_window
    if Input.trigger?(Input::C)   # Ｃボタンが押された
      skill = @skill_window.skill
      if not @event.unit.skill_can_use?(skill)
        Sound.play_buzzer
      #elsif skill.id == 1           # 攻撃
      #  Sound.play_decision
      #  @event.unit.action.set_attack
      #  if @event.unit.atk_range == 0 # 射程０なら位置選択を無視する
      #    check_attack_cell             # 攻撃可能範囲の作成
      #    check_effect_cell             # 効果範囲の作成
      #    change_scene_state(70)        # シーンの状態を行動確認に変更
      #  else
      #    change_scene_state(2)         # シーンの状態を攻撃位置選択に変更
      #  end
      else                          # スキル / 防御 / 会話
        Sound.play_decision
        @event.unit.action.set_skill(skill.id)
        if skill.atk_range == 0       # 射程０なら位置選択を無視する
          check_attack_cell             # 攻撃可能範囲の作成
          check_effect_cell             # 効果範囲の作成
          change_scene_state(70)        # シーンの状態を行動確認に変更
        else
          change_scene_state(3)         # シーンの状態をスキル位置選択に変更
        end
      end
    elsif Input.trigger?(Input::B)  # Ｂボタンが押された
      Sound.play_cancel
      change_scene_state(60)        # シーンの状態をキャラコマンドに変更
    end
  end
  
  #--------------------------------------------------------------------------
  # ○ カウンター処理
  #update1.007
  #--------------------------------------------------------------------------
  def counter(event)
    return if get_counter_rate(@event, event) <= rand(100)
    event.turn_cell(@event.x, @event.y)     # ターゲットがいる方向を向く
    #Audio.se_play("Audio/SE/Flash2.ogg", 80, 150) # 反撃効果音
    event.balloon_id = 5                    # 反撃フキダシアイコン
    @message_mini_window.refresh(Vocab::counter_action)
    wait(20)
    event.back_attack?(@event)              # 背後を取っているかチェック
    @event.unit.skill_effect( event.unit, event.unit.attack_skill() )   # 通常攻撃の効果適用
    #@event.unit.attack_effect(event.unit)   # 通常攻撃の効果適用
    set_animation(@event, event)            # アニメーションをセット
    set_popup(@event, event)                # ポップアップをセット
    @event.damage(event)
    @event.refresh_state                    # キャラクターの状態を更新
    unless event.enemy_flag                 # 反撃したのがアクター
      exp = (@action_result_exp + event.unit.action_exp) / 2
      exp = [exp + exp * (event.unit.level - @event.unit.level) * 10 / 100, 1].max
      old_uni = event.unit.deep_clone
      event.unit.gain_exp(exp, false)
      display_levelup( old_uni, event.unit ) if old_uni.level < @event.unit.level
      text = sprintf("XP+%d", exp)
      $game_srpg.add_popup(event.real_x, event.real_y, text, Color.new(255, 255, 255), 1)
    end
  end
  
  def check_effect_cell
    if $game_srpg.scene_state == 1 or $game_srpg.scene_state == 5 or
       $game_srpg.scene_state == 101  # 移動位置選択 or 向き選択の場合
      @effect_cell = [[0, 0]]           # 効果範囲は１セルとする
      return
    end
    @effect_cell.clear
    obj       = @event.unit.action.obj
    size      = obj.atk_size
    size_type = obj.atk_size_type
    size_hole = obj.atk_size_hole
    if TSRPG::CUSTOM_RANGE[size_type] != nil    # カスタム範囲タイプ
      for pos in TSRPG::CUSTOM_RANGE[size_type]
        @effect_cell.push(pos)
      end
    elsif size_type == 3                        # 方向範囲タイプ
      dx, dy = @event.x - @cursor_x, @event.y - @cursor_y
      if dx == 0 and dy == 0
        return
      elsif dx.abs > dy.abs
        n = dy.to_f / dx
        for i in [0, dx].min..[0, dx].max
          @effect_cell.push([i, (i * n).round]) if i != dx
        end
      else
        n = dx.to_f / dy
        for i in [0, dy].min..[0, dy].max
          @effect_cell.push([(i * n).round, i]) if i != dy
        end
      end
    else                                        # 通常 / 直線 / 箱タイプ
      for x in -size...size + 1
        for y in -size...size + 1
          d = x.abs + y.abs
          next if d <= size_hole
          if size_type == 0
            next if d > size                # 距離外のセルを除外
          elsif size_type == 1
            next if x != 0 and y != 0       # 上下左右一直線以外を除外
          end
          @effect_cell.push([x, y])
        end
      end
    end
  end

end
