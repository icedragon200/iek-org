# SceneAutomations
class ISS::Scene_Automation
  
  USER_AUTOMATIONS = {} # // Do Not Remove!!!
  
  USER_AUTOMATIONS["JukeboxTutorial"] = {}
  USER_AUTOMATIONS["JukeboxTutorial"][:update] = [
    ["DISABLE_INPUT", []],                     # // Disable User Input
    ["MESSAGE1", ["CREATE"]],                  # // Create Message Strip
    ["MESSAGE1", ["FONT", "SIZE", 16]],        # // Create Message Strip
    ["SCENE_SEND", ["ASSIGN1I", "index=", 0]], # // Reset index to 0 (Play)
  ] 
  USER_AUTOMATIONS["JukeboxTutorial"][:update] += create_fading_text( 
   "You can select a new button by pressing left or right", 1, 60 )
  USER_AUTOMATIONS["JukeboxTutorial"][:update] += [    
    ["SLEEP", [120]],                          # // Sleep for 3 seconds
    ["SCENE_SEND", ["SEND", "command_right"]], # // Send command_right() 
    ["SLEEP", [30]],                           # // Sleep for 0.5 seconds
    ["SCENE_SEND", ["SEND", "command_right"]], # // Send command_right()
    ["SLEEP", [120]],                          # // Sleep for 2 seconds
    ["SCENE_SEND", ["SEND", "command_left"]],  # // Send command_left() 
    ["SLEEP", [30]],                           # // Sleep for 0.5 seconds
    ["SCENE_SEND", ["SEND", "command_left"]],  # // Send command_left()
    ["SLEEP", [180]],                          # // Sleep for 3 seconds
  ]  
  USER_AUTOMATIONS["JukeboxTutorial"][:update] += create_fading_text( 
   "Press Z, Space of Enter on your keyboard to press the selected button", 1, 60 )
  USER_AUTOMATIONS["JukeboxTutorial"][:update] += [     
    ["SLEEP", [120]],                          # // Sleep for 1 seconds
    ["SCENE_SEND", ["SEND", "command_action"]],# // Send command_action (Play)
    ["SLEEP", [300]],                          # // Sleep for 5 seconds
    ["SCENE_SEND", ["SEND", "command_right"]], # // Send command_right()
    ["SLEEP", [30]],                           # // Sleep for 0.5 seconds
    ["SCENE_SEND", ["SEND", "command_action"]],# // Send command_action (Stop)
    ["SLEEP", [120]],                          # // Sleep for 2 seconds
    ["SCENE_SEND", ["SEND", "command_right"]], # // Send command_right()
    ["SLEEP", [60]],                           # // Sleep for 1 seconds
    ["SCENE_SEND", ["SEND", "command_action"]],# // Send command_action (Next)
    ["SLEEP", [30]],                           # // Sleep for 0.5 seconds
    ["SCENE_SEND", ["SEND", "command_action"]],# // Send command_action (Next)
    ["SLEEP", [30]],                           # // Sleep for 0.5 seconds
    ["SCENE_SEND", ["SEND", "command_action"]],# // Send command_action (Next)
    ["SLEEP", [120]],                          # // Sleep for 2 seconds
    ["SCENE_SEND", ["SEND", "command_right"]], # // Send command_right()
    ["SLEEP", [60]],                           # // Sleep for 1 seconds
    ["SCENE_SEND", ["SEND", "command_action"]],# // Send command_action (Prev)
    ["SLEEP", [30]],                           # // Sleep for 0.5 seconds
    ["SCENE_SEND", ["SEND", "command_action"]],# // Send command_action (Prev)
    ["SLEEP", [180]],                          # // Sleep for 3 seconds
    ["SCENE_SEND", ["SEND", "command_left"]],  # // Send command_left() 
    ["SLEEP", [30]],                           # // Sleep for 0.5 seconds
    ["SCENE_SEND", ["SEND", "command_left"]],  # // Send command_left() 
    ["SLEEP", [30]],                           # // Sleep for 0.5 seconds
    ["SCENE_SEND", ["SEND", "command_left"]],  # // Send command_left() 
    ["SLEEP", [30]],                           # // Sleep for 0.5 seconds
    ["SCENE_SEND", ["SEND", "command_action"]],# // Send command_action (Play)
    ["SLEEP", [300]],                          # // Sleep for 5 seconds
    ["SCENE_SEND", ["SEND", "command_right"]], # // Send command_right()
    ["SLEEP", [30]],                           # // Sleep for 0.5 seconds
    ["SCENE_SEND", ["SEND", "command_action"]],# // Send command_action (Stop)
    ["SLEEP", [180]],                          # // Sleep for 3 seconds
    ["SCENE_SEND", ["SEND", "command_left"]],  # // Send command_left() 
    ["SLEEP", [180]],                          # // Sleep for 3 seconds
  ]
  USER_AUTOMATIONS["JukeboxTutorial"][:update] += create_fading_text( 
   "So thats the basics of the Jukebox (JUKE)", 1, 60 )
  USER_AUTOMATIONS["JukeboxTutorial"][:update] += [ 
    ["SLEEP", [120]],                           # // Sleep for 2 seconds
  ]  
  USER_AUTOMATIONS["JukeboxTutorial"][:update] += create_fading_text( 
   "Whoops almost forgot, you can use the up and down arrows to change songs also", 1, 30 )
  USER_AUTOMATIONS["JukeboxTutorial"][:update] += [ 
    ["SLEEP", [60]],                           # // Sleep for 1 seconds
    ["SCENE_SEND", ["SEND", "command_down"]],  # // Send command_down()
    ["SLEEP", [30]],                           # // Sleep for 0.5 seconds
    ["SCENE_SEND", ["SEND", "command_down"]],  # // Send command_down()
    ["SLEEP", [30]],                           # // Sleep for 0.5 seconds
    ["SCENE_SEND", ["SEND", "command_down"]],  # // Send command_down()
    ["SLEEP", [60]],                           # // Sleep for 1 seconds
    ["SCENE_SEND", ["SEND", "command_up"]],    # // Send command_up()
    ["SLEEP", [30]],                           # // Sleep for 0.5 seconds
    ["SCENE_SEND", ["SEND", "command_up"]],    # // Send command_up()
    ["SLEEP", [30]],                           # // Sleep for 0.5 seconds
    ["SCENE_SEND", ["SEND", "command_up"]],    # // Send command_up()
    ["SLEEP", [120]],                          # // Sleep for 2 seconds
  ]  
  USER_AUTOMATIONS["JukeboxTutorial"][:update] += create_fading_text( 
   "Enjoy JUKE", 1, 90 )
  USER_AUTOMATIONS["JukeboxTutorial"][:update] += [ 
    ["SLEEP", [180]],                          # // Sleep for 3 seconds
    ["MESSAGE1", ["EFFECT", "fadeout", 0, 255, 60]],
    ["SLEEP", [60]],                           # // Sleep for 1 seconds
    ["MESSAGE1", ["DISPOSE"]],
    ["ENABLE_INPUT", []],                      # // Enable User Input
    ["CLEAR", []] # // Stop Automating
  ]
  
  AUTOMATIONS.merge!( USER_AUTOMATIONS )
  
end  
