# LVL Param Blocks 
#~ class Sprite_LevelParam < ::Sprite
#~   
#~   attr_reader :selected
#~   
#~   def initialize( actor, stat )
#~     super( nil )
#~     @actor = actor
#~     @stat  = stat
#~     
#~     @over_sprite = ::Sprite.new
#~     @over_sprite.bitmap = Bitmap.new( 76, 48 )
#~     @glow_sprite = ::Sprite.new
#~     @glow_sprite.blend_type = 1
#~     @glow_sprite.bitmap = Cache.picture( "StatHud-SelectionFlasher" )
#~     
#~     @dummy_bitmap = Bitmap.new( 32, 32 )
#~     deselect()
#~     refresh()
#~   end
#~   
#~   def dispose()
#~     super()
#~     @over_sprite.bitmap.dispose()
#~     @over_sprite.dispose()
#~     @glow_sprite.dispose()
#~     @dummy_bitmap.dispose()
#~   end
#~   
#~   def select()
#~     @selected = true
#~     @glow_sprite.visible = true
#~     refresh_bitmap()
#~   end
#~   
#~   def deselect()
#~     @selected = false
#~     @glow_sprite.visible = false
#~     refresh_bitmap()
#~   end
#~   
#~   def refresh()
#~     self.bitmap = @dummy_bitmap
#~     vocab    = Vocab.send( @stat )  
#~     icon     = ::IEO::Icon.stat( @stat )  
#~     value    = @actor.send( @stat )
#~     level    = @actor.send( @stat.to_s+"_level" )
#~     maxlevel = @actor.send( @stat.to_s+"_maxlevel" )
#~     
#~     leveltxt = sprintf( "%s: %s", Vocab.level_a, level )
#~     refresh_bitmap()
#~     @over_sprite.bitmap.clear()
#~     brect = Rect.new( 4, 30, 68, 7 )
#~     @over_sprite.draw_round_grad_bar( 
#~       brect, level, maxlevel,
#~       Color.new( 50, 58, 42 ), Color.new( 100, 108, 92 ), @over_sprite.gauge_back_color,
#~       2, true 
#~     )
#~     @over_sprite.bitmap.draw_icon( icon, 3, 3 )
#~     @over_sprite.bitmap.font.size = Font.default_size - 4
#~     @over_sprite.bitmap.font.color = @over_sprite.bitmap.system_color
#~     @over_sprite.bitmap.draw_text( 28, 0, self.width-28, 20, vocab )
#~     @over_sprite.bitmap.font.color = @over_sprite.bitmap.normal_color
#~     @over_sprite.bitmap.draw_text( 28+14, 11, self.width-28, 20, value )
#~     @over_sprite.bitmap.font.size = Font.default_size - 6
#~     brect.y -= 7 ; brect.height = 20
#~     @over_sprite.bitmap.draw_text( brect, leveltxt, 1 )
#~   end
#~   
#~   def refresh_bitmap()
#~     level    = @actor.send( @stat.to_s+"_level" )
#~     maxlevel = @actor.send( @stat.to_s+"_maxlevel" )
#~     if level == maxlevel
#~       bitm = Cache.picture( @selected ? "StatHud-MaxedSelected" : "StatHud-Maxed" )
#~     else
#~       bitm = Cache.picture( @selected ? "StatHud-NormalSelected" : "StatHud-Normal" )
#~     end  
#~     self.bitmap &&= bitm
#~   end
#~   
#~   def update()
#~     super()
#~     @glow_sprite.visible = @selected && self.visible
#~     @glow_sprite.opacity = (255 * Math.cos( (((Graphics.frame_count*3) % 360)/180.0)*Math::PI )).abs if @selected 
#~     @glow_sprite.x, @glow_sprite.y = self.x, self.y
#~     @over_sprite.x, @over_sprite.y = self.x, self.y
#~   end
#~   
#~ end  

#~ class Window_Parameter < SARA::Container_Base
#~   
#~   attr_accessor :help_window
#~   
#~   def initialize( actor, x, y )
#~     @actor = actor
#~     super( x, y, Graphics.width/2, Graphics.height-56 )
#~     self.active = true
#~     @sprites = []
#~     @last_index = -1
#~     self.index = 0
#~     #self.opacity = 0
#~     refresh()
#~   end
#~   
#~   def dispose()
#~     #super()
#~     dispose_sprites() ; @sprites = nil
#~   end
#~   
#~   def dispose_sprites()
#~     @sprites.each { |sp| sp.dispose() } ; @sprites.clear()
#~   end
#~   
#~   def stat( index=self.index ) ; return @stats[index] ; end
#~     
#~   def refresh()
#~     @stats = ::IEO::LEVEL_PARAMETERS::STATS
#~     @item_max = @stats.size
#~     @column_max = 7
#~     dispose_sprites()
#~     @stats.each { |st| @sprites << Sprite_LevelParam.new( @actor, st ) }
#~     wof, hof = (Graphics.width - (@column_max * 76)) / 2, 0
#~     for i in 0...@sprites.size
#~       @sprites[i].x = self.x + i % @column_max * 76 + wof
#~       @sprites[i].y = self.y + i / @column_max * 54 + hof
#~     end  
#~   end
#~   
#~   def update()
#~     #super()
#~     if Input.repeat?( Input::RIGHT )
#~       @index = (@index + 1) % @item_max
#~     elsif Input.repeat?( Input::LEFT ) 
#~       @index = (@index - 1) % @item_max
#~     end  
#~     @sprites.each { |sp| sp.update() }
#~     if @last_index != self.index
#~       Sound.play_cursor()
#~       @sprites[@last_index].deselect() if @last_index > -1
#~       @last_index = self.index
#~       @sprites[@last_index].select()
#~       update_help()
#~     end  
#~     update_stat_changes() if self.active
#~   end
#~   
#~   def update_stat_changes()
#~     if Input.trigger?( Input::C )
#~       unless @actor.send( stat.to_s+"_level_max?" )
#~         Sound.play_decision()
#~         level = @actor.send( stat.to_s+"_level" )
#~         @actor.send( stat.to_s+"_level=", level+1 )
#~         @sprites[self.index].refresh()
#~       else
#~         Sound.play_buzzer()
#~       end  
#~     end  
#~   end
#~   
#~   def update_help()
#~     #self.help_window
#~   end  
#~   #def update_cursor()
#~   #  self.cursor_rect.empty()
#~   #end
#~   
#~ end
