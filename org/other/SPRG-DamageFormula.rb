# SPRG - Damage Formula
class Game_Battler
  
  def calculate_damage_value( sdamage, user, target, formula, obj=nil, type=:all )
    damage = sdamage
    critical = false
    case formula.upcase
    when "NORMALATTACK"
      damage = user.atk * 4 - target.def * 2 
      damage = 0 if damage < 0                        
      damage *= elements_max_rate(user.element_set)
      damage /= 100
      damage = rand(2) if damage < 0
      if damage > 0                                
        critical = (rand(100) < user.cri)     
        critical = false if target.prevent_critical      
        damage *= 3 if critical                   
      end  
      damage = apply_variance(damage, 20)             
      damage = apply_guard(damage)                    
    when "NORMALSKILL"  
      if damage > 0                                   
        damage += user.atk * 4 * obj.atk_f / 100      
        damage += user.spi * 2 * obj.spi_f / 100      
        unless obj.ignore_defense                     
          damage -= target.def * 2 * obj.atk_f / 100  
          damage -= target.spi * 1 * obj.spi_f / 100  
        end
        damage = 0 if damage < 0                      
      elsif damage < 0                                
        damage -= user.atk * 4 * obj.atk_f / 100      
        damage -= user.spi * 2 * obj.spi_f / 100      
      end
      damage *= elements_max_rate(obj.element_set)    
      damage /= 100
      damage = apply_variance(damage, obj.variance)   
      damage = apply_guard(damage)                    
    when "SONIC"  
      damage = obj.base_damage      
    when "PAPERMARIO"  
      if obj.nil?
        damage = user.atk - target.def
      else
        damage = user.atk * obj.atk_f / 100  
        damage = user.spi * obj.atk_f / 100
        damage = damage - target.def
      end
    when /([\+\-]\d+)(?:[%％])[ ](USER|TARGET)[ ](?:MAXLP|MAXHP)/i  
      aff = $2.upcase == "USER" ? user : target
      damage = aff.maxhp * $1.to_i / 100.0
    when /([\+\-]\d+)(?:[%％])[ ](USER|TARGET)[ ](?:MAXSP|MAXMP)/i  
      aff = $2.upcase == "USER" ? user : target
      damage = aff.maxmp * $1.to_i / 100.0
    when /([\+\-]\d+)(?:[%％])[ ](USER|TARGET)[ ](?:LP|HP)/i  
      aff = $2.upcase == "USER" ? user : target
      damage = aff.hp * $1.to_i / 100.0
    when /([\+\-]\d+)(?:[%％])[ ](USER|TARGET)[ ](?:SP|MP)/i  
      aff = $2.upcase == "USER" ? user : target
      damage = aff.mp * $1.to_i / 100.0  
    end  
    damage = Integer( damage )
    return [damage, critical] if type == :all
    return damage if type == :damage
  end  
  
  #--------------------------------------------------------------------------
  # * Calculation of Damage From Normal Attack
  #     attacker : Attacker
  #    The results are substituted for @hp_damage
  #--------------------------------------------------------------------------
  def make_attack_damage_value( attacker )
    data = calculate_damage_value( 0, attacker, self, "NORMALATTACK", nil, :all )
    @critical = data[1]
    damage    = data[0]
    # //
    if $game_temp.in_srpg
      if $game_temp.unit_back_attack        # 背後攻撃ボーナス
        damage += damage * TSRPG::BACK_ATTACK_BONUS / 100
      end
    end
    @hp_damage = damage                             # damage HP
  end
  
  #--------------------------------------------------------------------------
  # * Calculation of Damage Caused by Skills or Items
  #     user : User of skill or item
  #     obj  : Skill or item (for normal attacks, this is nil)
  #    The results are substituted for @hp_damage or @mp_damage.
  #--------------------------------------------------------------------------
  def make_obj_damage_value( user, obj )
    data = calculate_damage_value( obj.base_damage, user, self, obj.damage, obj, :all )
    @critical = data[1]
    damage    = data[0]
    # //
    if $game_temp.in_srpg
      if $game_temp.unit_back_attack        # 背後攻撃ボーナス
        damage += damage * TSRPG::BACK_ATTACK_BONUS / 100
      end
    end
      
    if obj.damage_to_mp
      if (change_damage?)
        @hp_damage = damage                         # HP にダメージ
      else
        @mp_damage = damage                         # MP にダメージ
      end
    else
      if (change_damage? and self.mp > 0)
        @mp_damage = damage                         # MP にダメージ
      else
        @hp_damage = damage                         # HP にダメージ
      end
    end
  end
    
end  
