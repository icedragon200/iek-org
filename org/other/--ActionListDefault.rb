# --Action List (Default)
module SARA
  module TSRPG
    module ACTION_LIST
      
      ACTIONS = {}
      # // Attack Default Action
      ACTIONS["AttackStart"] = [
      ]
      ACTIONS["AttackWhole"] = [
      ]
      ACTIONS["AttackTarget"]= [
        ["ATK_ANIMATION", ["USER", "TARGET", "WAIT"]], 
        ["BACK ATTACK", ["USER", "TARGET", "CALC"]],
        ["ATTACK EFFECT", ["USER", "TARGET", nil, "WAIT", "WAIT"]]
      ]
      ACTIONS["AttackEnd"]   = [
        ["WAIT", [3]]
      ]
      
      # // Skill Default Action
      ACTIONS["SkillStart"] = [
        ["DOUBLE ANIME", []]
      ]
      ACTIONS["SkillWhole"] = [
      ]
      ACTIONS["SkillTarget"]= [
        ["OBJ_ANIMATION", ["USER", "TARGET", "WAIT"]], 
        ["BACK ATTACK", ["USER", "TARGET", "CALC"]],
        ["SKILL EFFECT", ["USER", "TARGET", nil, "WAIT", "WAIT"]]
      ]
      ACTIONS["SkillEnd"]   = [
        ["WAIT", [3]]
      ]
      
      # // Item Default Action
      ACTIONS["ItemStart"] = [
        ["DOUBLE ANIME", []]
      ]
      ACTIONS["ItemWhole"] = [
      ]
      ACTIONS["ItemTarget"]= [
        ["OBJ_ANIMATION", ["USER", "TARGET", "WAIT"]], 
        ["BACK ATTACK", ["USER", "TARGET", "CALC"]],
        ["ITEM EFFECT", ["USER", "TARGET", nil, "WAIT", "WAIT"]]
      ]
      ACTIONS["ItemEnd"]   = [
        ["WAIT", [3]]
      ]
      
      # // Guard Default Action
      ACTIONS["GuardStart"] = [
      ]
      ACTIONS["GuardWhole"] = [
      ]
      ACTIONS["GuardTarget"]= [
      ]
      ACTIONS["GuardEnd"]   = [
      ]
      
      def self.get_action( name )
        raise "Action: #{name} does not exist or is nil" if ACTIONS[name].nil?()
        return ACTIONS[name]
      end
      
    end  
    
  end
end
