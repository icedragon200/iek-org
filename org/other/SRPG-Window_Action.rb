# SRPG - Window_Action
#==============================================================================#
# Window_Action
#==============================================================================#
class Window_Action < Window_Selectable
  
  module_eval( IEO::CSC::WIN_SKILL::INCL )
  module_eval( IEO::SKILL_LEVEL::WIN_SKILL::INCL )  
    
  def refresh
    @data = []
    @data.push( @actor.attack_skill() )
    for skill in @actor.skills
      next unless skill.battle_ok?()
      @data.push(skill)
      self.index = @data.size - 1 if skill.id == @actor.last_skill_id
    end
    @data.push( @actor.talk_skill() )
    @data.push( @actor.defend_skill() )
    @item_max = @data.size
    create_contents()
    for i in 0...@item_max do draw_item(i) end
  end
    
end


