# SARA - EN Stat
module Vocab
  
  def self.en
    return "Energy"
  end
  
  def self.en_a
    return "EN"
  end
  
  def self.maxen ; return self.en ; end
  def self.maxen_a ; return self.en_a ; end
    
end

class Game_Battler
  
  attr_accessor :en_damage
  attr_reader :en
  
  alias :sara_en_gmb_initialize :initialize unless $@
  def initialize( *args, &block )
    @en_damage = 0
    sara_en_gmb_initialize( *args, &block )
    @maxen_plus = 0
    create_maxen_table()
  end
  
  def create_maxen_table()
    @maxen_parameter = Table.new( self.maxen_maxlevel+1 )#(101)
    for i in 0..@maxen_parameter.xsize
      @maxen_parameter[i] = 5 + (30.0 * i / (@maxen_parameter.xsize.to_f-1))
    end  
    @en = @maxen_parameter[0]
  end
  
  def maxen_level_value( level=self.maxen_level_p )
    return @maxen_parameter[level]
  end   
  
  def maxen()
    return self.base_maxen() + @maxen_plus
  end
  
  def maxen=( new_maxen )
    @maxen_plus += new_maxen - self.maxen
    @maxen_plus = [[@maxen_plus, -9999].max, 9999].min
    @en = [@en, self.maxen].min
  end
  
  def en=( value )
    @en = [[value, 0].max, self.maxen].min
  end
  
  def regen_en()
    v = 1
    self.en += v
    return v
  end
  
end

class Window_Base
  
  def draw_actor_mp_gauge(actor, x, y, width = 120)
    gw = width * actor.mp / [actor.maxmp, 1].max
    gc1 = mp_gauge_color1
    gc2 = mp_gauge_color2
    self.contents.fill_rect(x, y + WLH - 8, width, 4, gauge_back_color)
    self.contents.gradient_fill_rect(x, y + WLH - 8, gw, 4, gc1, gc2)
    draw_actor_en_gauge( actor, x, y+4, width )
  end
  
  def draw_actor_en_gauge( actor, x, y, width = 120 )
    gw = width * actor.en / [actor.maxen, 1].max
    gc1 = Color.new( 64, 176, 16 )
    gc2 = Color.new( 192, 240, 16 )
    self.contents.fill_rect(x, y + WLH - 8, width, 4, gauge_back_color)
    self.contents.gradient_fill_rect(x, y + WLH - 8, gw, 4, gc1, gc2)
    fonsize = self.contents.font.size
    ocolor  = self.contents.font.color
    self.contents.font.color = normal_color
    self.contents.font.size  = Font.default_size - 6
    self.contents.draw_text( x+28, y+8, width, 16, actor.en )
    self.contents.font.size  = fonsize
    self.contents.font.color = ocolor 
  end
  
end
