#-switch INCUR:ON
#-define MACLPATH#=C:/Lib/Git/RGSS3-MACL/
#-undefine xSTANDALONE
#-//
#-define SKPVERSION#=0x10000
#-define MACLREQ_VER#=0x10000
#-define HDR_TYP#=:type=>"class"
#-define HDR_GNM#=:name=>"IER - Random Hue"
#-define HDR_GDC#=:dc=>"06/02/2012"
#-define HDR_GDM#=:dm=>"06/03/2012"
#-define HDR_GAUT#=:author=>"IceDragon"
#-define HDR_VER#=:version=>"SKPVERSION"
#-inject gen_script_header_wotail HDR_TYP,HDR_GNM,HDR_GAUT,HDR_GDC,HDR_GDM,HDR_VER
 ──────────────────────────────────────────────────────────────────────────────
#-inject gen_script_des 'Requirements'
  This script is 'standalone'
 
#-inject gen_script_des 'Introduction' 
  Welcome to IER - Random Hue.
  Introducing a rather old script, the ICY/IEX - Random Hue.
  Most users would remember this for GTBS.
  This script allows you to set a variable/random hue
  for battlers ingame, that way you can have the
  same enemy, but different hues (color,shades)

#-inject gen_script_des 'Instruction Manual'
  Place all notetags in their appropriate noteboxes.
  NOTE:
    hueset names are case sensitive.
    'default24' is not the same as 'Default24'
  NOTE:
    Even if you call set_random_hue during battle, it wont
    make any changes, since the sprite has to reload
    the bitmap for changes to take place.

#-inject gen_script_des 'Reference Manual'     
  functions (Game_Enemy)
    set_random_hue
  
  Notetags   
    Enemy
      <randomhueset: n>, <random hueset: n> <random_hueset: n>

      EG:
        <random hueset: default24>

#-inject gen_script_des 'Change Log'   
  (MM/DD/YYYY)
   06/14/2012 - 0x10000 
     Ported IEX - Random Enemy Hue

 ──────────────────────────────────────────────────────────────────────────────
=end 
#-inject gen_script_import "IER::RandomHue", 'SKPVERSION'
#-//include ASMxROOT/header/rgss3macl_require.rb
#-switch INCUR:OFF
#-inject gen_module_header 'IER::RandomHue'
module IER
  module RandomHue
    HUES = {}
    HUES['none']      = [] # // Do not change this
    # // 16 Hues
    HUES['default16'] = 16.times.collect do |i| 256 / 16 * i end
    # // 24 Hues
    HUES['default24'] = 24.times.collect do |i| 256 / 24 * i end
    # // 32 Hues
    HUES['default32'] = 32.times.collect do |i| 256 / 32 * i end
    # // 64 Hues
    HUES['default64'] = 64.times.collect do |i| 256 / 64 * i end
    # // Missing Hues
    HUES.default = HUES['none']
  end
end
#-inject gen_class_header 'Game_Enemy'
class RPG::Enemy
  def hue_set_name
    @hue_set_name ||= (@note.match(/<random[\s_]?hueset:\s(.+)>/i)||[nil,'none'])[1]
  end
  def hue_set
    IER::RandomHue::HUES[@hue_set_name]
  end
  def random_hue?
    !hue_set.empty?
  end
end
#-inject gen_class_header 'Game_Enemy'
class Game_Enemy
  alias ier_rndhue_initialize initialize
  def initialize *args,&block
    ier_rndhue_initialize *args,&block
    set_random_hue
  end
  def set_random_hue
    return unless enemy.random_hue?
    @battler_hue = enemy.hue_set.sample
  end
end
#-inject gen_script_footer