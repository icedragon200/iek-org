#-switch INCUR:ON
#-define MACLPATH#=C:/Lib/Git/RGSS3-MACL/
#-undefine xSTANDALONE
#-//
#-define SKPVERSION#=0x10000
#-define MACLREQ_VER#=0x10000
#-define HDR_TYP#=:type=>"class"
#-define HDR_GNM#=:name=>"IER - Random Hue"
#-define HDR_GDC#=:dc=>"06/02/2012"
#-define HDR_GDM#=:dm=>"06/03/2012"
#-define HDR_GAUT#=:author=>"IceDragon"
#-define HDR_VER#=:version=>"SKPVERSION"
#-inject gen_script_header_wotail HDR_TYP,HDR_GNM,HDR_GAUT,HDR_GDC,HDR_GDM,HDR_VER
 ──────────────────────────────────────────────────────────────────────────────
#-inject gen_script_des 'Requirements'
  This script requires the RGSS3-MACL

#-inject gen_script_des 'Introduction'


#-inject gen_script_des 'Instruction Manual'


#-inject gen_script_des 'Reference Manual'
  functions


  Notetags


#-inject gen_script_des 'Change Log'
  (MM/DD/YYYY)
   06/14/2012 - 0x10000
     Ported IEX - *Can Use series

 ──────────────────────────────────────────────────────────────────────────────
=end
#-inject gen_script_import "IER::CanUseEx", 'SKPVERSION'
#-include ASMxROOT . "/header/rgss3macl_require.rb"
#-switch INCUR:OFF
#-inject gen_module_header 'IER::CanUseEx'
module IER
  module CanUseEx
  end
end
