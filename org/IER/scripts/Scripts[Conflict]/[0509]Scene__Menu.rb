# Scene::Menu
# // 01/24/2012
# // 01/24/2012
class Window::MenuCommand < Window::Command
  #--------------------------------------------------------------------------
  # ● コマンド選択位置の初期化（クラスメソッド）
  #--------------------------------------------------------------------------
  def self.init_command_position
    @@last_command_symbol = nil
  end
  w = WindowAddons
  class AddonBin_WMC < w::AddonBin[w::Header,w::WinButtons,w::Mouse_MoveWindow,w::Footer,w::Base_Tail]
    def header_text
      return "Menu"
    end
    def header_cube()
      super.xset(self.x,nil,nil,self.width-winbuttons_width-2)
    end  
  end  
  def addon_bin
    AddonBin_WMC
  end
  #--------------------------------------------------------------------------
  # ● オブジェクト初期化
  #--------------------------------------------------------------------------
  def initialize(x=0,y=0)
    super(x,y)
    select_last
  end
  #--------------------------------------------------------------------------
  # ● ウィンドウ幅の取得
  #--------------------------------------------------------------------------
  def window_width
    return 160
  end
  #--------------------------------------------------------------------------
  # ● 表示行数の取得
  #--------------------------------------------------------------------------
  def visible_line_number
    item_max
  end
  #--------------------------------------------------------------------------
  # ● コマンドリストの作成
  #--------------------------------------------------------------------------
  def make_command_list
    add_main_commands
    add_formation_command
    add_original_commands
    add_save_command
    add_game_end_command
  end
  #--------------------------------------------------------------------------
  # ● 主要コマンドをリストに追加
  #--------------------------------------------------------------------------
  def add_main_commands
    add_command(Vocab::item,   :item,   main_commands_enabled)
    add_command(Vocab::skill,  :skill,  main_commands_enabled)
    add_command(Vocab::equip,  :equip,  main_commands_enabled)
    add_command(Vocab::status, :status, main_commands_enabled)
  end
  #--------------------------------------------------------------------------
  # ● 並び替えをコマンドリストに追加
  #--------------------------------------------------------------------------
  def add_formation_command
    add_command(Vocab::formation, :formation, formation_enabled)
  end
  #--------------------------------------------------------------------------
  # ● 独自コマンドの追加用
  #--------------------------------------------------------------------------
  def add_original_commands
  end
  #--------------------------------------------------------------------------
  # ● セーブをコマンドリストに追加
  #--------------------------------------------------------------------------
  def add_save_command
    add_command(Vocab::save, :save, save_enabled)
  end
  #--------------------------------------------------------------------------
  # ● ゲーム終了をコマンドリストに追加
  #--------------------------------------------------------------------------
  def add_game_end_command
    add_command(Vocab::game_end, :game_end)
  end
  #--------------------------------------------------------------------------
  # ● 主要コマンドの有効状態を取得
  #--------------------------------------------------------------------------
  def main_commands_enabled
    $game_party.exists
  end
  #--------------------------------------------------------------------------
  # ● 並び替えの有効状態を取得
  #--------------------------------------------------------------------------
  def formation_enabled
    $game_party.members.size >= 2 && !$game_system.formation_disabled
  end
  #--------------------------------------------------------------------------
  # ● セーブの有効状態を取得
  #--------------------------------------------------------------------------
  def save_enabled
    !$game_system.save_disabled
  end
  #--------------------------------------------------------------------------
  # ● 決定ボタンが押されたときの処理
  #--------------------------------------------------------------------------
  def process_ok
    @@last_command_symbol = current_symbol
    super
  end
  #--------------------------------------------------------------------------
  # ● 前回の選択位置を復帰
  #--------------------------------------------------------------------------
  def select_last
    select_symbol(@@last_command_symbol)
  end  
end 
class Window::MenuParty < Container::WinSel #Window::Base
  include Container::Addons::Background_Glass
  class AddonBin_WMP < ::Window::MenuCommand::AddonBin_WMC
    def header_text
      return "Party"
    end 
  end  
  def addon_bin
    AddonBin_WMP
  end
  def initialize(x=0,y=0)
    super(x,y,window_width,window_height)
    refresh()
    #activate()
  end  
  def window_width
    Graphics.width - 48
  end
  def window_height
    item_height*3 + (standard_padding*2)
  end 
  def party
    $game_party
  end  
  def members
    party.members
  end  
  def item_max
    members.size
  end  
  def item_width
    window_width - (standard_padding * 2)
  end  
  def item_height
    52 #48
  end  
  def draw_item( index )
    rect = item_rect(index)
    #rect.x += standard_padding
    #rect.y += standard_padding
    battler = members[index]
    rect.contract!(1)
    ext_draw_box1(
      :x             => rect.x,
      :y             => rect.y,
      :width         => rect.width,
      :height        => rect.height,
      :base_color    => Pallete[:paper1].xset(:alpha=>198),
      :padding_color => Pallete[:brown1]
    )
    rect.contract!(1)
    #draw_text( rect.x + 4, rect.y + 4, rect.width, 24, actor.name )
    #draw_horz_line( rect.y + 24, rect.x + 4, rect.width-8, 2 )
    hw = rect.width / 2
    bw = rect.width / 4
    dx = rect.x
    dy = rect.y
    chn, chi, chu = battler.character_name, battler.character_index, battler.character_hue
    wd = draw_tiny_character( 
      {
        :character_name  => chn,
        :character_index => chi, 
        :character_hue   => chu,
        :x => dx + 16, 
        :y => dy + 32,  
      }
    ).width
    contents.font.set_style( :default )
    draw_actor_name( battler, dx + wd, dy )
    draw_actor_level( battler, hw - 56, dy )
    draw_battler_exp( 
      { 
        :battler => battler,
        :x => hw - (56 + 72),
        :y => dy + 4, 
        :height => 16,
        :width => 72 
      } 
    )
    draw_battler_hp( 
      {  
        :battler => battler,
        :x => hw + (bw * 0), 
        :y => dy+4, 
        :height => 16, 
        :width => bw 
      } 
    )
    draw_battler_mp(
      { 
        :battler => battler,
        :x => hw + (bw * 1), 
        :y => dy+4, 
        :height => 16, 
        :width => bw 
      }
    )
    draw_horz_line( dy + 20, rect.x + 2, rect.width-4, 2 )
    dy += 24
    draw_actor_icons( battler, dx, dy, rect.width / 4 ) 
    dx = (rect.width / 4)
    contents.font.size = Font.default_size
    for i in 0...6
      draw_parameter( battler, dx + ((i % 8) * 64), dy + ((i / 8) * 12), 2+i )
    end
  end  
  #--------------------------------------------------------------------------
  # ● 決定ボタンが押されたときの処理
  #--------------------------------------------------------------------------
  def process_ok
    super
    party.menu_actor = party.members[index]
  end
  #--------------------------------------------------------------------------
  # ● 前回の選択位置を復帰
  #--------------------------------------------------------------------------
  def select_last
    select(party.menu_actor.index || 0)
  end
  #--------------------------------------------------------------------------
  # ● 保留位置（並び替え用）の設定
  #--------------------------------------------------------------------------
  def pending_index=(index)
    last_pending_index = @pending_index
    @pending_index = index
    redraw_item(@pending_index)
    redraw_item(last_pending_index)
  end
end  
class Window::MenuStatus < Window::MenuParty ; end
class Window::MenuActor < Window::MenuStatus
  #--------------------------------------------------------------------------
  # ● オブジェクト初期化
  #--------------------------------------------------------------------------
  def initialize
    super(0, 0)
    self.visible = false
  end
  #--------------------------------------------------------------------------
  # ● 決定ボタンが押されたときの処理
  #--------------------------------------------------------------------------
  def process_ok
    party.target_actor = party.members[index] unless @cursor_all
    call_ok_handler
  end
  #--------------------------------------------------------------------------
  # ● 前回の選択位置を復帰
  #--------------------------------------------------------------------------
  def select_last
    select($game_party.target_actor.index || 0)
  end
  #--------------------------------------------------------------------------
  # ● アイテムのためのカーソル位置設定
  #--------------------------------------------------------------------------
  def select_for_item(item)
    @cursor_fix = item.for_user?
    @cursor_all = item.for_all?
    if @cursor_fix
      select(party.menu_actor.index)
    elsif @cursor_all
      select(0)
    else
      select_last
    end
  end
end
class Scene::Menu < Scene::MenuBase
  def start()
    super()
    create_all_windows()
    windows_auto_z()
    @windows.reverse!
    @lock_button = SystemButton.new(@viewport, 0)
    @lock_button.set_handler(:click, method(:lock_space))
    @lock_button.x = @viewport.rect.x
    @lock_button.y = @viewport.rect.vy2 - @lock_button.height
    @lock_button.z = 1
    @lock_button.org_button_index = 0
    @lock_button.active = true
    restore_window_positions()
    clamp_windows_to_space()
    @windows.each { |w| w.set_to_close() }
  end  
  def post_start
    super()
    @windows.each { |w| w.start_open();wait(5) } 
  end  
  def pre_terminate
    super()
    remember_window_positions()
    @windows.each { |w| w.start_close();wait(5) }
    wait_for_window(*@windows)
  end  
  def terminate
    super() 
    dispose_background() 
    @lock_button.dispose
  end 
  def restore_window_positions()
    hsh = $game_system.window_positions[:menu]
    return unless hsh
    @command_window.x, @command_window.y = hsh[:command] if hsh[:command]
    @time_window.x   , @time_window.y    = hsh[:time]    if hsh[:time]
    @status_window.x , @status_window.y  = hsh[:status]  if hsh[:status]
  end  
  def remember_window_positions()
    hsh = {}
    hsh[:command] = @command_window.to_rect.xto_a(:x,:y)
    hsh[:time]    = @time_window.to_rect.xto_a(:x,:y)
    hsh[:status]  = @status_window.to_rect.xto_a(:x,:y)
    $game_system.window_positions[:menu] = hsh
  end  
  def lock_space
    @windows.each { |w| w.can_move_window = @lock_button.toggle_state }
  end  
  def create_all_windows()
    create_command_window()
    create_status_window()
    create_time_window()
  end  
  def create_command_window()
    @command_window = Window::MenuCommand.new(0,0)
    @command_window.set_handler(:item,      method(:command_item))
    @command_window.set_handler(:skill,     method(:command_personal))
    @command_window.set_handler(:equip,     method(:command_personal))
    @command_window.set_handler(:status,    method(:command_personal))
    @command_window.set_handler(:formation, method(:command_formation))
    @command_window.set_handler(:save,      method(:command_save))
    @command_window.set_handler(:game_end,  method(:command_game_end))
    @command_window.set_handler(:cancel,    method(:return_scene))
    add_window( @command_window )
  end    
  def create_time_window()
    @time_window = Container::BigClock.new(0,0)
    @time_window.salign(2,-1)
    add_window( @time_window )
  end  
  #--------------------------------------------------------------------------
  # ● ゴールドウィンドウの作成
  #--------------------------------------------------------------------------
  def create_gold_window
    #@gold_window = Window::Gold.new
    #@gold_window.x = 0
    #@gold_window.y = Graphics.height - @gold_window.height
  end
  #--------------------------------------------------------------------------
  # ● ステータスウィンドウの作成
  #--------------------------------------------------------------------------
  def create_status_window()
    @status_window = Window::MenuParty.new(0, 0)
    @status_window.y = @command_window.vy2 + 42
    @status_window.salign(1,-1)
    add_window( @status_window )
  end
  #--------------------------------------------------------------------------
  # ● コマンド［アイテム］
  #--------------------------------------------------------------------------
  def command_item
    SceneManager.call(Scene::Item)
  end
  #--------------------------------------------------------------------------
  # ● コマンド［スキル］［装備］［ステータス］
  #--------------------------------------------------------------------------
  def command_personal
    @status_window.select_last
    @status_window.activate
    @status_window.set_handler(:ok,     method(:on_personal_ok))
    @status_window.set_handler(:cancel, method(:on_personal_cancel))
  end
  #--------------------------------------------------------------------------
  # ● コマンド［並び替え］
  #--------------------------------------------------------------------------
  def command_formation
    @status_window.select_last
    @status_window.activate
    @status_window.set_handler(:ok,     method(:on_formation_ok))
    @status_window.set_handler(:cancel, method(:on_formation_cancel))
  end
  #--------------------------------------------------------------------------
  # ● コマンド［セーブ］
  #--------------------------------------------------------------------------
  def command_save
    SceneManager.call(Scene::Save)
  end
  #--------------------------------------------------------------------------
  # ● コマンド［ゲーム終了］
  #--------------------------------------------------------------------------
  def command_game_end
    SceneManager.call(Scene::End)
  end
  #--------------------------------------------------------------------------
  # ● 個人コマンド［決定］
  #--------------------------------------------------------------------------
  def on_personal_ok
    case @command_window.current_symbol
    when :skill
      SceneManager.call(Scene::Skill)
    when :equip
      SceneManager.call(Scene::Equip)
    when :status
      SceneManager.call(Scene::Status)
    end
  end
  #--------------------------------------------------------------------------
  # ● 個人コマンド［終了］
  #--------------------------------------------------------------------------
  def on_personal_cancel
    @status_window.unselect
    @command_window.activate
  end
  #--------------------------------------------------------------------------
  # ● 並び替え［決定］
  #--------------------------------------------------------------------------
  def on_formation_ok
    if @status_window.pending_index >= 0
      $game_party.swap_order(@status_window.index,
                             @status_window.pending_index)
      @status_window.pending_index = -1
      @status_window.redraw_item(@status_window.index)
    else
      @status_window.pending_index = @status_window.index
    end
    @status_window.activate
  end
  #--------------------------------------------------------------------------
  # ● 並び替え［キャンセル］
  #--------------------------------------------------------------------------
  def on_formation_cancel
    if @status_window.pending_index >= 0
      @status_window.pending_index = -1
      @status_window.activate
    else
      @status_window.unselect
      @command_window.activate
    end
  end

  def update
    super() 
    update_background()
    @lock_button.update
  end  
  def update_background
  end  
end  
