# ● P`(PK8) - HeaderStuff
=begin
 
 Fancy Header Template (Replace this with script title)
 by PK8 (Replace this with author)
 Created: 4/15/2012 (Replace this with creation date)
 Hurhur, extra info here. (Yeah, extra info.)
 ──────────────────────────────────────────────────────────────────────────────
 ■ Table of Contents
   ○ Duplicate me by pressing CTRL + D             - Line 00-00
   ○ Duplicate me by pressing CTRL + D             - Line 00-00
   ○ Duplicate me by pressing CTRL + D             - Line 00-00
   ○ Duplicate me by pressing CTRL + D             - Line 00-00
   ○ Duplicate me by pressing CTRL + D             - Line 00-00
     ○ Section                                     - Line 00-00
       ○ Subsection                                - Line 00-00
 ──────────────────────────────────────────────────────────────────────────────
 ■ Header title
   Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eu neque vel
   nisi pellentesque convallis a sit amet leo. Vivamus quis lorem ut urna
   vestibulum tincidunt id sit amet metus. Phasellus vel dui at est euismod
   condimentum ut a diam. Vivamus suscipit turpis eget tellus molestie rhoncus.
   Fusce pulvinar ligula eget libero pharetra eu dignissim justo iaculis.
   Pellentesque nec nunc id turpis blandit aliquet sit amet vel odio. Maecenas
   ut massa lectus. Suspendisse condimentum, massa eu laoreet varius, neque
   lorem luctus enim, a vestibulum est arcu ac dolor. Cras lacinia ligula at
   velit cursus ornare. Aenean pulvinar feugiat quam ac commodo. Morbi vitae
   turpis quis neque rhoncus lobortis. Etiam eleifend vulputate neque, a
   tincidunt diam posuere in. Donec tristique molestie nulla sit amet mattis. 
 ──────────────────────────────────────────────────────────────────────────────
 ■ Header title 2
 ┌──────────────────────────────────────────────────────────────────────────┐
 │ ■ Section 1                                                              │
 └──────────────────────────────────────────────────────────────────────────┘
   Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eu neque vel
   nisi pellentesque convallis a sit amet leo. Vivamus quis lorem ut urna
   vestibulum tincidunt id sit amet metus. Phasellus vel dui at est euismod
   condimentum ut a diam. Vivamus suscipit turpis eget tellus molestie rhoncus.
   Fusce pulvinar ligula eget libero pharetra eu dignissim justo iaculis.
   Pellentesque nec nunc id turpis blandit aliquet sit amet vel odio. Maecenas
   ut massa lectus. Suspendisse condimentum, massa eu laoreet varius, neque
   lorem luctus enim, a vestibulum est arcu ac dolor. Cras lacinia ligula at
   velit cursus ornare. Aenean pulvinar feugiat quam ac commodo. Morbi vitae
   turpis quis neque rhoncus lobortis. Etiam eleifend vulputate neque, a
   tincidunt diam posuere in. Donec tristique molestie nulla sit amet mattis.
 
 ┌──────────────────────────────────────────────────────────────────────────┐
 │ ■ Section 2                                                              │
 └┬─────────────┬───────────────────────────────────────────────────────────┘
  │ Sub-section │
  └─────────────┘
  Stuff. Stuff. More stuff.
  
 └┬───────────────┬─────────────────────────────────────────────────────────┘
  │ Sub-section 2 │
  └───────────────┘
  Yes! Yes! Yes!
  
 ──────────────────────────────────────────────────────────────────────────────
 ■ Characters you can use to further fancy up your header.
 ┌──┬──┐ ╔══╦══╗ ╒══╤══╕ ╓──╥──╖ ▲ ► ◄ ▼ ☺ ☻ ▪ ▫
 │  │  │ ║  ║  ║ │  │  │ ║  ║  ║ ░ ▒ ▓ █ ♠ ♣ ♥ ♦
 ├──┼──┤ ╠══╬══╣ ╞══╪══╡ ╟──╫──╢ ○ ● □ ■ ♪ ♫ ↔ ↕
 │  │  │ ║  ║  ║ │  │  │ ║  ║  ║ ▌ ▄ ▀ ▐ ↓ ← → ↑
 └──┴──┘ ╚══╩══╝ ╘══╧══╛ ╙──╨──╜
 ──────────────────────────────────────────────────────────────────────────────

=end
