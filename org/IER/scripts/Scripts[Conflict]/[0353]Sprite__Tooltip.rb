# Sprite::Tooltip
# // 02/03/2012
# // 02/03/2012
class Sprite::Tooltip < Sprite
  def initialize(viewport=nil,text="")
    super(viewport)
    @mtime = 10
    @time = 0
    set_text(text)
  end  
  def set_text(text)
    @time = @mtime
    if @text != text
      self.opacity = 0
      @text = text
      refresh
    end  
  end  
  def clear
    set_text("")
  end 
  def dispose()
    dispose_bitmap()
    super()
  end  
  def standard_style
    :window_header 
  end  
  def refresh
    b = Bitmap.prep_test_bmp
    b.font.set_style(standard_style)
    dispose_bitmap()
    r = b.text_size(@text)
    self.bitmap = Bitmap.new((r.width+32)+8,24)#(r.height+8).clamp(24,24))
    self.bitmap.font.set_style(standard_style)
    self.bitmap.gui_draw_help_header()
    self.bitmap.draw_text(4,0,width-8,height,@text,1)
  end 
  def fadein
    self.opacity += 255 / 20
  end
  def fadeout
    self.opacity -= 255 / 20
  end  
  def update
    super
    (@time = @time.pred.max(0)) > 0 ? fadein : fadeout
  end  
end  
