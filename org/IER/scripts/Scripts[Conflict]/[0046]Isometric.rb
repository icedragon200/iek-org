# Isometric
#==============================================================================#
# ■ Isometric
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/23/2011
# // • Data Modified : 12/23/2011
# // • Version       : 1.0 
#==============================================================================#
# ● Change Log
#     ♣ 12/23/2011 V1.0 
#
#==============================================================================#
module Isometric
  def self.calc_screen_xy( x, y, width, height, mwidth, mheight )
    hw, hh = width / 2, height / 2
    # // 0, 0 == Top, Right
    #xo = mwidth - x * width + y * height
    #yo = x * hw + y * hh 
    # // 0, 0 == Bottom, Left
    xo = x * width + y * height
    yo = mheight - x * hw + y * hh 
    xo /= 2
    yo /= 2
    yo -= height
    return xo, yo
  end  
  def self.run_iso_test()
    tab = Table.new( 12, 12 )
    for x in 1...tab.xsize-1
      for y in 1...tab.ysize-1
        tab[x, y] = 1
      end 
    end  
    sprite = Sprite::Minimap.new( nil )  
    sprite.data = tab
    loop do
      Graphics.update
      Input.update
      sprite.update
    end 
  end  
end 
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
