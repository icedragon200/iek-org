# Keyboard
# // 02/07/2012
# // 02/07/2012
module Keyboard
  # // Key Codes
  LETTERS = {} 
  LETTERS[:A] = 0x41 ; LETTERS[:B] = 0x42 ; LETTERS[:C] = 0x43 
  LETTERS[:D] = 0x44 ; LETTERS[:E] = 0x45 ; LETTERS[:F] = 0x46
  LETTERS[:G] = 0x47 ; LETTERS[:H] = 0x48 ; LETTERS[:I] = 0x49
  LETTERS[:J] = 0x4A ; LETTERS[:K] = 0x4B ; LETTERS[:L] = 0x4C 
  LETTERS[:M] = 0x4D ; LETTERS[:N] = 0x4E ; LETTERS[:O] = 0x4F 
  LETTERS[:P] = 0x50 ; LETTERS[:Q] = 0x51 ; LETTERS[:R] = 0x52 
  LETTERS[:S] = 0x53 ; LETTERS[:T] = 0x54 ; LETTERS[:U] = 0x55 
  LETTERS[:V] = 0x56 ; LETTERS[:W] = 0x57 ; LETTERS[:X] = 0x58 
  LETTERS[:Y] = 0x59 ; LETTERS[:Z] = 0x5A
  LETTERS_I = LETTERS.invert
  
  NUMBERS = {}
  NUMBERS[0] = 0x30 ; NUMBERS[1] = 0x31 ; NUMBERS[2] = 0x32 ; NUMBERS[3] = 0x33
  NUMBERS[4] = 0x34 ; NUMBERS[5] = 0x35 ; NUMBERS[6] = 0x36 ; NUMBERS[7] = 0x37
  NUMBERS[8] = 0x38 ; NUMBERS[9] = 0x39
  NUMBERS_I = NUMBERS.invert
  
  NUMSHIF = {}
  [')','!','@','#','$','%','^','&','*','('].each_with_index{|s,i|NUMSHIF[s]=NUMBERS[i]}
  NUMSHIF_I = NUMSHIF.invert
  
  NUMPAD = {}
  NUMPAD[0] = 0x60 ; NUMPAD[1] = 0x61 ; NUMPAD[2] = 0x62 ; NUMPAD[3] = 0x63
  NUMPAD[4] = 0x64 ; NUMPAD[5] = 0x65 ; NUMPAD[6] = 0x66 ; NUMPAD[7] = 0x67
  NUMPAD[8] = 0x68 ; NUMPAD[9] = 0x69
  NUMPAD_I = NUMPAD.invert
  
  NPAD_MULT = 0x6A ; NPAD_ADD  = 0x6B ; NPAD_SEP  = 0x6C ; NPAD_SUB  = 0x6D
  NPAD_DECI = 0x6E ; NPAD_DIV  = 0x6F
  
  F1 = 0x70 ; F2 = 0x71 ; F3 = 0x72 ; F4 = 0x73 ; F5 = 0x74 ; F6 = 0x75
  F7 = 0x76 ; F8 = 0x77 ; F9 = 0x78 ; F10 = 0x79 ; F11 = 0x7A ; F12 = 0x7B
  
  LEFT = 0x25 ; UP = 0x26 ; RIGHT = 0x27 ; DOWN = 0x28
  
  BACKSPACE = 0x08 ; TAB       = 0x09 
  ENTER     = 0x0D 
  SHIFT     = 0x10 ; CTRL      = 0x11 ; ALT       = 0x12
  LSHIFT    = 0xA0 ; LCTRL     = 0xA2 ; LALT      = 0xA4
  RSHIFT    = 0xA1 ; RCTRL     = 0xA3 ; RALT      = 0xA5
  ESCAPE    = 0x1B
  SPACEBAR  = 0x20 ; PAGEUP    = 0x21 ; PAGEDOWN  = 0x22 ; KEND = 0x23
  HOME      = 0x24 ;
  
  INSERT = 0x2D ; DELETE = 0x2E 
  
  CAPLOCK    = 0x14
  NUMLOCK    = 0x90 ; SCROLLLOCK = 0x91
  
  SEMICOLON = COLON      = 0xBA
  EQUAL     = PLUS       = 0xBB
  COMMA     = ALEFT      = 0xBC
  MINUS     = UNDERSCORE = 0xBD
  PERIOD    = ARIGHT     = 0xBE
  BACKSLASH = QUESTION   = 0xBF
  TILDE     = ACCENT     = 0xC0
  LBRACE    = LBRACK     = 0xDB
  FSLASH    = SEPERATOR  = 0xDC
  RBRACE    = RBRACK     = 0xDD
  QUOTE     = DQUOTE     = 0xDE
  
  TYPING = LETTERS.values + NUMBERS.values + NUMPAD.values + [ SPACEBAR,
    COLON, EQUAL, COMMA, UNDERSCORE, PERIOD, BACKSLASH, TILDE, LBRACE, FSLASH,
    RBRACE, QUOTE, NPAD_MULT, NPAD_ADD, NPAD_SEP, NPAD_SUB, NPAD_DECI, NPAD_DIV 
  ]
  OTHER_KEYS_S = {}
  OTHER_KEYS_S[SEMICOLON] = [';', ':']
  OTHER_KEYS_S[EQUAL]     = ['=', '+']
  OTHER_KEYS_S[COMMA]     = [',', '<']
  OTHER_KEYS_S[MINUS]     = ['-', '_']
  OTHER_KEYS_S[PERIOD]    = ['.', '>']
  OTHER_KEYS_S[BACKSLASH] = ['/', '?']
  OTHER_KEYS_S[TILDE]     = ['`', '~']
  OTHER_KEYS_S[LBRACE]    = ['[', '{']
  OTHER_KEYS_S[FSLASH]    = %w(\\ |)
  OTHER_KEYS_S[RBRACE]    = [']', '}']
  OTHER_KEYS_S[QUOTE]     = %w(' ")
  def self.key_to_s(key,upcase=[false,false]) # // Shift, Caps Lock
    if LETTERS.has_value?(key)
      n = key.chr; return (upcase.any? && !upcase.all?) ? n.upcase : n.downcase
    end  
    return NUMBERS_I[key] if NUMBERS_I.has_key?(key) if(!upcase[0])
    return NUMSHIF_I[key] if NUMSHIF_I.has_key?(key) if(upcase[0])
    return NUMPAD_I[key] if NUMPAD_I.has_key?(key)
    return " " if(key == SPACEBAR)
    return OTHER_KEYS_S[key][upcase[0] ? 1 : 0]
  end 
  # // 02/24/2012 [
  SYM_TO_KEY = {}
  LETTERS.each_pair(){|key,value|SYM_TO_KEY[key.to_sym]=value}
  NUMBERS.each_pair(){|key,value|SYM_TO_KEY["a#{key}".to_sym] = value }
  NUMPAD.each_pair(){ |key,value|SYM_TO_KEY["n#{key}".to_sym] = value }
  for i in 1..12;SYM_TO_KEY["F#{i}".to_sym]=const_get("F#{i}");end
  SYM_TO_KEY[:ESC] = SYM_TO_KEY[:ESCAPE] = ESCAPE
  SYM_TO_KEY[:BACKSPACE] = BACKSPACE
  SYM_TO_KEY[:ENTER]     = ENTER
  SYM_TO_KEY[:SPACEBAR]  = SPACEBAR
  SYM_TO_KEY[:LEFT]      = LEFT
  SYM_TO_KEY[:RIGHT]     = RIGHT
  SYM_TO_KEY[:UP]        = UP
  SYM_TO_KEY[:DOWN]      = DOWN
  SYM_TO_KEY[:HOME]      = HOME
  SYM_TO_KEY[:END]       = KEND
  SYM_TO_KEY[:INSERT]    = INSERT
  SYM_TO_KEY[:DELETE]    = DELETE
  def self.sym_to_key(key_sym)
    SYM_TO_KEY[key_sym]
  end  
  # // 02/24/2012 ]
  # // Key Sequences used with the key_sequence method
  KEY_SEQUENCES = {}
  KEY_SEQUENCES["FULLSCREEN"] = [ [ALT, 0], [ENTER, 0], [ENTER, 2], [ALT, 2] ]
  
  @@krc  = {} # // Key Repeat Counter
  @@kcl  = {} # // Key Cooling (used to fix trigger errors)
  COOL_COUNT = 1 # // Frames for key cooling . x .
  def self.key_spressed?( key )
    InputEx.GetAsyncKeyState( key ) & 0x8000 == 0x8000
  end
  def self.key_pressed?( key )
    if key_spressed?(key);@@krc[key]=0;return true;end
    return false
  end  
  def self.adjust_key( key ) 
    key = sym_to_key(key) if key.is_a?(Symbol)
    key -= 130 if key.between?( 130, 158 ) ; return key 
  end
  def self.set_key_state( key, state ) 
    InputEx.keybd_event( key, 0, state, 0 ) 
  end
  def self.send_key( key, state )
    # // 0 - Mouse, 1 - Keyboard
    # // 0x0001 - Extended, 0x0002 - KeyUp, 0x0008 - ScanCode, 0x0004 - Unicode
    SNDI.call( 1, [0, 0, 2, 0, 0], 256 ) #0x0002 # // Still Buggy
  end
  def self.correct_keys( keys ) ; return keys ; end#.flatten() ; end
  def self.key_sequence( name ) 
    KEY_SEQUENCES[name].each do |a| set_key_state(*a) end
  end
  def self.repeating_update()
    @@krc.keys.each do |key| 
      if key_spressed?( key )
        @@krc[key] += 1 ; @@kcl[key] = COOL_COUNT
      else
        if @@kcl[key] == 0 # // If key cool 0
          @@krc.delete( key ) ; @@kcl.delete( key )
        else
          @@kcl[key] -= 1 if @@kcl.has_key?( key )
        end  
      end  
    end 
  end
  def self.num_lock?()    ; InputEx.GetKeyState( NUMLOCK    ) == 0x01 ; end
  def self.caps_lock?()   ; InputEx.GetKeyState( CAPLOCK   ) == 0x01 ; end
  def self.scroll_lock?() ; InputEx.GetKeyState( SCROLLLOCK ) == 0x01 ; end    
  def self.trigger?(key)  
    adjk = adjust_key( key ) 
    count = @@krc[adjk]
    (count == 0) || (count.nil?() ? key_pressed?( adjk ) : false)
  end  
  def self.repeat?(key)
    adjk = adjust_key( key ) 
    count = @@krc[adjk]
    return true if count == 0
    count.nil?() ? key_pressed?( adjk ) : (count >= 23 && (count - 23) % 6 == 0) 
  end  
  def self.press?( key )
    adjk = adjust_key( key ) 
    @@krc[adjk].nil?() ? key_pressed?( adjk ) : true
  end  
  def self.trigger_any?(*args)
    args.any? { |k| trigger?(k) }
  end  
  def self.trigger_all?(*args)
    args.all? { |k| trigger?(k) }
  end  
  def self.repeat_any?(*args)
    args.any? { |k| repeat?(k) }
  end  
  def self.repeat_all?(*args)
    args.all? { |k| repeat?(k) }
  end  
  def self.press_any?(*args)
    args.any? { |k| press?(k) }
  end  
  def self.press_all?(*args)
    args.all? { |k| press?(k) }
  end
  def self.update()
    repeating_update()
  end
  def self.get_typing()
    TYPING.select { |key| repeat?(key) }
  end
  def self.get_typing_s()
    get_typing.collect { |key| key_to_s(key, get_cap_state) }
  end  
  def self.get_cap_state()
    [press?(SHIFT),caps_lock?()]
  end  
  def self.run_test()
    loop do
      Graphics.update
      Keyboard.update
      t = Keyboard.get_typing_s
      print t.join('') unless t.empty?
    end  
  end   
end 
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
