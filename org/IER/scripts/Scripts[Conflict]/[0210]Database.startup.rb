# Database.startup
#==============================================================================#
# ■ Database.startup
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/07/2011
# // • Data Modified : 12/07/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/07/2011 V1.0 
#==============================================================================#
module Database
  Splash = RPG::Splash
def self.build_startup()  
  @startup = RPG::Startup.new()
  base_splash = proc { |filename="", t=60|
    [
      Splash::SequenceCommand.new(Splash::FREEZE_CODE       , []),
      Splash::SequenceCommand.new(Splash::SHOW_PICTURE_CODE , [0, filename]),
      Splash::SequenceCommand.new(Splash::ALIGN_PICTURE_CODE, [0, 0, 1]),
      Splash::SequenceCommand.new(Splash::ALIGN_PICTURE_CODE, [0, 1, 1]),
      Splash::SequenceCommand.new(Splash::TRANSITION_CODE   , [t])
    ]
  }  
   # RPG::Splash::SequenceCommand.new(WAIT_CODE         , [wait_count]),
   # RPG::Splash::SequenceCommand.new(FREEZE_CODE       , []),
   # RPG::Splash::SequenceCommand.new(ERASE_PICTURE_CODE, [0]),
   # RPG::Splash::SequenceCommand.new(SPLASH_END_CODE   , [])
  @splashes = []
  @splashes[0] = Splash.new()
  @splashes[0].sequence = base_splash.call("TurtleFactory_Splash", 30) 
  trans = @splashes[0].sequence.pop
  @splashes[0].sequence += [
    Splash::SequenceCommand.new(Splash::FLASH_PICTURE_CODE, [0]+Color.new(0,0,0).to_a+[120]),
    trans,
    Splash::SequenceCommand.new(Splash::WAIT_CODE         , [180]),
    Splash::SequenceCommand.new(Splash::FREEZE_CODE       , []),
    Splash::SequenceCommand.new(Splash::ERASE_PICTURE_CODE, [0]),
    Splash::SequenceCommand.new(Splash::SPLASH_END_CODE   , [])
  ]
  @splashes[1] = Splash.new()
  @splashes[1].sequence = base_splash.call("RGSS3_Splash", 30) 
  trans = @splashes[1].sequence.pop
  @splashes[1].sequence += [
    Splash::SequenceCommand.new(Splash::FLASH_PICTURE_CODE, [0]+Color.new(0,0,0).to_a+[120]),
    trans,
    Splash::SequenceCommand.new(Splash::WAIT_CODE         , [180]),
    Splash::SequenceCommand.new(Splash::FREEZE_CODE       , []),
    Splash::SequenceCommand.new(Splash::ERASE_PICTURE_CODE, [0]),
    Splash::SequenceCommand.new(Splash::SPLASH_END_CODE   , [])
  ]
  @splashes[2] = Splash.new()
  @splashes[2].sequence = base_splash.call("REI_Splash", 30) 
  trans = @splashes[2].sequence.pop
  @splashes[2].sequence += [
    Splash::SequenceCommand.new(Splash::FLASH_PICTURE_CODE, [0]+Color.new(0,0,0).to_a+[120]),
    trans,
    Splash::SequenceCommand.new(Splash::WAIT_CODE         , [180]),
    Splash::SequenceCommand.new(Splash::FREEZE_CODE       , []),
    Splash::SequenceCommand.new(Splash::ERASE_PICTURE_CODE, [0]),
    Splash::SequenceCommand.new(Splash::SPLASH_END_CODE   , [])
  ]
  @startup.splash_screens = [@splashes[0], @splashes[1], @splashes[2]]
end  
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
