# [IEI]Arts System
# // 02/22/2012
# // 02/22/2012
# // Icy Engine Iliks
class IEI::Art < RPG::BaseItem # // >_> Nothing here <_< Come get going.. 
  def self.[](id,icon_index,name,description,*features)
    art = new()
    art.initialize_add()
    art.id          = id
    art.icon_index  = icon_index
    art.name        = name
    art.description = description
    art.features    = features
    art # // . x . You get teh newly created art
  end  
end
module IEI
  module ArtsSystem
    Art = IEI::Art # // Don't tamper with this >_____>
    @arts = []     # // Don't touch this either >_____>
    def self.arts() ; @arts ; end # // And this +____+
    Feature = RPG::BaseItem::Feature # // >_>... You know the drill  
    module MixIns ; end # // >_> Same goes for this
    def self.element_id(n) ; DB.element_id(n) ; end
    def self.eid(*a,&b) ; element_id(*a,&b) ; end  
    def self.param_id(n) ; DB.param_id(n) ; end
    def self.pid(*a,&b) ; param_id(*a,&b) ; end
    @art_group = {}
    def self.add_art2groups(art, *groups)
      groups.each { |g|
        @art_group[g] ||= Set.new
        @art_group[g].add(art)
      }  
    end  
    def self.find_arts4groups(*groups)
      groups.inject(@arts){|r,g|r&@art_group[g].to_a}
    end 
  def self.mk_objs()
    # // How to create a new art? Look below
    # //       Art[id,icon_index,name,description,*features]
    #@arts[0] = Art[0,16,"Dummy","Does Absolutely Nothing",Feature.new(0,0,0.0)]
    @arts[ 0] = Art[ 0,   0,'----------','',Feature.new(0,0,0.0)] # // Does nothing . x .
    # // And finally you can start making your own arts here
    # // If your having difficulty with the Features try getting the Features4Dummies XD
    # // >_> Now if your smart you'll figure out a way to manage your 'arts'
    # Increases movement, defense, and prevents knock-back
    @arts[ 1] = Art[ 1,172,'Footing'    ,'',MkFeature.def_r(1.1)]  
    # Decreases magic cost, and lowers cast time
    @arts[ 2] = Art[ 2,228,'Rune'       ,'',MkFeature.mp_cost_r(0.75)]  
    # Increases weapon effectiveness
    @arts[ 3] = Art[ 3,121,'Stance'     ,'',MkFeature.mat_dam_r(1.1)]
    # Identify all items, enemies, objects, increases hit ratio
    @arts[ 4] = Art[ 4,359,'Optics'     ,'',MkFeature.hit_r(1.05)]
    # Higher natural Hp recovery rate, endurance ability (prevents 1ko's and may survive from a lethal attack)  
    @arts[ 5] = Art[ 5,323,'Survival'   ,'',MkFeature.hp_regen_r(0.07)]
    # Increases speed, magic can be used even when silenced 
    @arts[ 6] = Art[ 6,159,'Ninjutsu'   ,'',MkFeature.agi_r(1.1)]
    # Set(Re) and Disarm Traps, ablility to spot traps in a room
    @arts[ 7] = Art[ 7,267,'Mechanic'   ,'',Feature.new(0,0,0.0)]
    # Enables AOC(Area of Control) effect, prevents fear and confusion 
    @arts[ 8] = Art[ 8,143,'Control'    ,'',Feature.new(0,0,0.0)]
    # Prevents paralasis, increases evasion rate  
    @arts[ 9] = Art[ 9,286,'Nimble'     ,'',MkFeature.eva_r(1.1)]
    # // 
    # Increases Attack, Lowers Defense
    @arts[10] = Art[10,398,'Risk'       ,'',MkFeature.atk_r(1.1),MkFeature.def_r(0.9)]
    # Increases Defense, Lowers Agility
    @arts[11] = Art[11,509,'Phalanx'    ,'',MkFeature.def_r(1.1),MkFeature.agi_r(0.9)]
    # Lowers Magic Cost Greatly, Lowers Magic Effectiveness
    @arts[12] = Art[12,365,'Dust Mage'  ,'',MkFeature.mp_cost_r(0.3),MkFeature.mat_dam_r(0.5)]
    # Prevents Counter Attacks, lowers attack
    @arts[13] = Art[13,488,'Loose Grip' ,'',MkFeature.atk_r(0.9)]
    # Prevents Charm, sleep, confusion, lowers magic defense, and lowers Agility
    @arts[14] = Art[14,460,'Focused'    ,'',MkFeature.mdf_r(0.8),MkFeature.agi_r(0.8)]
    # Increases gold rate, decreases exp rate
    @arts[15] = Art[15,285,'Scavenge'   ,'',MkFeature.exp_gain_r(0.7)]
    # Increases exp rate, decreases gold rate
    @arts[16] = Art[16,230,'Scholar'    ,'',MkFeature.exp_gain_r(1.3)]
    # // 
    # Increases Magic Learn Rate sharply, lowers all parameters (except Hp & Mp)
    @arts[17] = Art[17,232,'Pen & Paper','',*((2...6).collect{|i|MkFeature.param_r(i,0.9)})]
    # Increases Exp Rate, Increases Magic Learn Rate, lowers gold sharply
    @arts[18] = Art[18,469,'Discipline' ,'',MkFeature.exp_gain_r(1.15)]
    $data_arts = @arts
  end   
  end  
end 
IEI::Core.on_data_load { IEI::ArtsSystem.mk_objs }
module IEI::ArtsSystem::MixIns::Battler
  def pre_init_iei()
    super() 
    @arts = [] # // Error Prevention ? D: : =3=
  end  
  def init_iei()
    super()
    init_arts()
    flush_arts()
  end 
  def post_init_iei()
    super()
    # // Something else .x .
  end
  # //
  def init_arts() # // . x . You add custom things here
    @arts = [] 
  end  
  def all_arts() # // .x. Returns all present arts on this character
    @arts.collect{|i|$data_arts[i]}
  end  
  def arts() # // .x. You can filter certain arts here
    all_arts()
  end  
  def arts_features() # // . x . If you need all the features it adds
    arts().inject([]){|r,obj|r+obj.features()}
  end
  # // 
  def feature_objects() 
    super + arts() # // States + Arts :3
  end
  # // Usage Stuff
  def remove_art(art_id)
    @arts.delete(art_id)
  end  
  def set_art(art_id,index)
    @arts[index] = art_id
  end 
  def set_art_wf(art_id,index) # // Set Art, With Flush . x .
    set_art(art_id,index)
    flush_arts()
  end  
  def swap_art_order(index1, index2)
    @arts[index1], @arts[index2] = @arts[index2], @arts[index1]
  end 
  def change_equip_art(art_id,index)
    set_art(art_id,index) if allowed_art?(art_id)
  end  
  def equip_art(art,index)
    change_equip_art(art ? art.id : 0, index)
  end  
  def equip_arts(*arts)
    (0...arts_equip_size).select{|i|@arts[i]==0||@arts[i].nil?}.each do |i|
      equip_art(arts.shift,i)
    end
  end  
  def unequip_arts()
    (0...arts_equip_size).each do |i| change_equip_art(0,i) end
  end  
  def has_art?(id)
    @arts.include?(id)
  end
  # // 
  def flush_arts()
    @arts.select!{|a|allowed_art?(a)}
    @arts.pad!(arts_equip_size,0)
  end  
  # // Settings
  def allowed_art?(id)
    true
  end  
  def arts_equip_size # // Try to keep it reasonable >_>
    4
  end 
  def arts_point_limit # // NYI (Not yet Implemented)
    50
  end  
end
module IEI::ArtsSystem::MixIns::Party
  def pre_init_iei()
    super() 
    @arts = {} 
  end  
  def init_iei()
    super()
  end 
  def post_init_iei()
    super()
  end  
  def gain_art(art_id,n)
    return unless $data_arts[art_id]
    @arts[art_id] ||= 0
    @arts[art_id] = (@arts[art_id] + n).max(0)
    @arts.delete(art_id) if @arts[art_id] == 0
  end  
  def lose_art(art_id,n)
    gain_art(art_id,-n)
  end  
  def art_number(art_id)
    @arts[art_id] || 0
  end  
  def arts()
    @arts.keys.sort.select{|k|art_number(k)>0}.collect{|k|$data_arts[k]}
  end  
  def has_art?(art_id)
    return art_number(art_id) > 0
  end 
end  
class Game::RogueCharacter2
  include IEI::ArtsSystem::MixIns::Battler # // And now for teh MAGIC
end  
class Game::Party
  include IEI::ArtsSystem::MixIns::Party
end  
class Window::ArtsStatus < Window::SkillStatus 
end  
class Window::ArtsList < Window::Selectable
  def initialize(x,y,width=window_width,height=window_height)
    @list = []
    super(x,y,width,height)#window_width,window_height)
    select(0)
  end
  #def update_padding_bottom
  #end  
  attr_reader :actor
  def actor=(actor)
    return if @actor == actor
    @actor = actor
    refresh
    #select_last
  end
  def make_item_list()
    @list = @actor ? @actor.arts : []
  end  
  def refresh
    make_item_list
    create_contents
    draw_all_items
    call_update_help
  end 
  def item_max()
    @list.size
  end 
  def col_max
    2
  end  
  def spacing 
    0
  end  
  def window_width()
    adjust_w4window(col_max*96)
  end
  def window_height()
    adjust_h4window(item_height*4)
  end  
  def item_width()
    (self.width - (standard_padding*2)) / col_max
  end 
  def item_height()
    28#36
  end
  def item(index=self.index)
    @list[index]
  end  
  def draw_item(index)
    rect = item_rect(index).contract(2)
    open_artist.draw_art(@list[index],rect,true)
  end  
  def current_item=(item)
    return if @actor.nil?()
    @actor.equip_art(item,self.index)
    refresh()
  end
  #--------------------------------------------------------------------------
  # ● ヘルプテキスト更新
  #--------------------------------------------------------------------------
  def update_help
    @help_window.set_item(item)
  end
  def active_fading?()
    true
  end  
end  
class Window::ArtsCommand < Window::Command
  def window_width
    return 160
  end
  def make_command_list()
    add_command("Equip"   , :equip)
    add_command("Unequip" , :unequip)
    add_command("Clear"   , :clear)
    add_command("List"    , :list)
  end
  attr_reader :actor
  def actor=(actor)
    return if @actor == actor
    @actor = actor
    refresh
    #select_last
  end
end  
class Scene::Arts < Scene::MenuBase
  def start()
    super()
    create_all_windows()
    auto_add_windows()
  end 
  def create_all_windows()
    create_help_window()
    create_command_window()
    create_status_window()
    create_equip_window()    
    create_item_window()
    center_windows(true,false,[@equip_window,@item_window])
  end 
  def create_command_window()
    @command_window = Window::ArtsCommand.new(@help_window.x,@help_window.vy2)
    @command_window.help_window = @help_window
    @command_window.actor = @actor
    @command_window.set_handler(:equip   , method(:command_equip))
    @command_window.set_handler(:unequip , method(:command_unequip))
    @command_window.set_handler(:clear   , method(:command_clear))
    @command_window.set_handler(:list    , method(:command_list))
    @command_window.set_handler(:cancel  , method(:return_scene))
    @command_window.set_handler(:pagedown, method(:next_actor))
    @command_window.set_handler(:pageup  , method(:prev_actor))
  end  
  def create_status_window()
    @status_window = Window::ArtsStatus.new(@command_window.vx2,@help_window.vy2,@help_window.width-@command_window.width)
    @status_window.actor = @actor
  end  
  def create_equip_window()
    @equip_window = Window::ArtsList.new(@help_window.x,@status_window.vy2,@canvas.width/2)
    @equip_window.actor = @actor
    @equip_window.help_window = @help_window
  end  
  def create_item_window()
    @item_window = Window::ArtsList.new(@equip_window.vx2,@status_window.vy2,@equip_window.width)
    @item_window.actor = $game_party
    @item_window.help_window = @help_window
    @item_window.height = @canvas.vy2-@item_window.y
  end
  def command_equip()
    @item_window.set_handler(:ok, method(:equip_current_item))
    @item_window.set_handler(:cancel, method(:end_item_selection))
    #@item_window.set_handler(:pageup, method(:pred_element))
    #@item_window.set_handler(:pagedown, method(:succ_element))
    @equip_window.set_handler(:ok,method(:start_item_selection))
    @equip_window.set_handler(:cancel,method(:end_equip_selection))
    @equip_window.activate()
  end  
  def command_unequip()
    @equip_window.set_handler(:ok,method(:unequip_current_item))
    @equip_window.set_handler(:cancel,method(:end_equip_selection))
    @equip_window.activate()
  end  
  def command_clear()
    @actor.unequip_arts()
    @help_window.clear()
    @equip_window.refresh()
    @item_window.refresh()
    @status_window.refresh()
    @command_window.refresh()
    @command_window.activate()
  end
  def command_list()
    @item_window.set_handler(:ok, method(:show_item_full_info))
    @item_window.set_handler(:cancel, method(:end_item_list))
    @item_window.activate()
  end  
  def start_item_selection()
    @item_window.activate()
  end  
  def end_item_selection()
    @equip_window.activate()
  end  
  def equip_current_item()
    @equip_window.current_item = @item_window.item
    @equip_window.activate()
    @status_window.refresh()
  end  
  def unequip_current_item()
    @equip_window.current_item = nil
    @equip_window.activate()
    @status_window.refresh()
  end  
  def end_equip_selection()
    @command_window.activate()
  end  
  def show_item_full_info()
  end  
  def end_item_list()
    @command_window.activate
  end   
  def update()
    super()
  end
  def on_actor_change
    @command_window.actor = @actor
    @equip_window.actor = @status_window.actor = @actor
    @command_window.activate
  end
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
