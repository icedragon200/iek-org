# Mouse Addon
# // 01/24/2012
# // 01/24/2012 
if defined? Mouse
module Mixin::MouseSelectable
  def top_rect
    Rect.new(self.vx, self.vy, self.width, 32)
  end
  def bottom_rect
    Rect.new(self.vx, self.vy2-32, self.width, 32)
  end  
  #alias :pre_mouse_update :update 
  #def update
  #  pre_mouse_update()
  #  
  #end  
  def mouse_index?(i)
    MouseEx.in_area?(item_rect_to_screen(item_rect(i)).to_v4) && i.between?(0,item_max-1)
  end  
  def current_item_to_screen()
    item_rect_to_screen(item_rect(self.index))
  end  
  def item_rect_to_screen(rect)
    rect = rect.dup
    rect.x = self.x + standard_padding + rect.x - self.ox
    rect.y = self.y + standard_padding + rect.y - self.oy
    rect.height -= 2
    rect
  end
  def cursor_rect_to_screen()
    item_rect_to_screen(cursor_rect.to_rect)
  end  
  def screen_x2content_x(sx)
    sx - self.x - self.padding + self.ox
  end  
  def screen_y2content_y(sy)
    sy - self.y - self.padding + self.oy
  end
  def mouse_in_contents?
    MouseEx.in_area?(self.contents_rect)
  end  
  def process_cursor_move()
    return unless cursor_movable? && !win_busy?
    last_index = @index
    unless mouse_select_index()
      cursor_down (Input.trigger?(:DOWN))  if Input.repeat?(:DOWN)
      cursor_up   (Input.trigger?(:UP))    if Input.repeat?(:UP)
      cursor_right(Input.trigger?(:RIGHT)) if Input.repeat?(:RIGHT)
      cursor_left (Input.trigger?(:LEFT))  if Input.repeat?(:LEFT)
      cursor_pagedown   if !handle?(:pagedown) && Input.trigger?(:R)
      cursor_pageup     if !handle?(:pageup)   && Input.trigger?(:L)
    end  
    Sound.play_cursor if @index != last_index
  end
  def mouse_select_index()
    return false unless MouseEx.moved?
    return false unless mouse_in_contents?
    i = calc_col_index(MouseEx.x) + (calc_row_index(MouseEx.y) * col_max)
    i = i.clamp(0,(item_max-1).max(0)) 
    self.index = i if i != self.index
    # // LAGGY O_O But kinda works o-e
    #for i in (top_row*col_max)...((top_row*col_max)+page_item_max)
    #  if mouse_index?(i)
    #    self.index = i 
    #    return true
    #  end  
    #end 
    return true
  end
  def calc_col_index(x)
    (screen_x2content_x(x) / (item_width + spacing).to_f).floor    
  end  
  def calc_row_index(y)
    (screen_y2content_y(y) / (item_height).to_f).floor
  end
  def process_handling
    return unless open? && active && !win_busy?
    if mouse_in_contents?
      return process_ok       if ok_enabled?     && MouseEx.left_click?()
      return process_cancel   if cancel_enabled? && MouseEx.right_click?()
    end  
    return process_ok       if ok_enabled?        && Input.trigger?(:C)
    return process_cancel   if cancel_enabled?    && Input.trigger?(:B)
    return process_pagedown if handle?(:pagedown) && Input.trigger?(:R)
    return process_pageup   if handle?(:pageup)   && Input.trigger?(:L)
  end
end
class Container::WinSel #Window::MenuParty
  include Mixin::MouseSelectable  
end  
class Window::Selectable < Window::Base
  remove_method :process_handling
  remove_method :process_cursor_move
  include Mixin::MouseSelectable
end
module Mixin::WheelButtons
  def ex_init
    super()
    @buttons = []
    @buttons << GUIExt::Button_Base.new( nil, 0, 0, 13 )
    @buttons << GUIExt::Button_Base.new( nil, 0, 0, 14 )
    @buttons << GUIExt::Button_Base.new( nil, 0, 0, 15 )
    @buttons << GUIExt::Button_Base.new( nil, 0, 0, 12 )
    @buttons << GUIExt::Button_Base.new( nil, 0, 0, 4 )
    @buttons << GUIExt::Button_Base.new( nil, 0, 0, 5 )
    @buttons[0].set_handler(:click, method(:wheel_left))
    @buttons[1].set_handler(:click, method(:wheel_right))
    @buttons[2].set_handler(:click, method(:wheel_up))
    @buttons[3].set_handler(:click, method(:wheel_down))
    @buttons[4].set_handler(:click, method(:wheel_cancel))
    @buttons[5].set_handler(:click, method(:wheel_ok))
    @buttons[0].help_text = "Previous"
    @buttons[1].help_text = "Next"
    @buttons[2].help_text = "Previous Page"
    @buttons[3].help_text = "Next Page"
    @buttons[4].help_text = "Cancel"
    @buttons[5].help_text = "Ok"
    @buttons.each{|b|b.help_window = Mouse.tooltip_sprite}
  end
  def update()
    super()
  end  
  def refresh_x()
    super()
    @buttons[0].x = self.vx - 32
    @buttons[1].x = self.vx2 + 32
    @buttons[2].x = self.x #- 12
    @buttons[3].x = self.x #- 12
    @buttons[4].x = self.vx - 24
    @buttons[5].x = self.vx2 + 24
  end
  def refresh_y()
    super()
    @buttons[0].y = self.y #- 12 
    @buttons[1].y = self.y #- 12
    @buttons[2].y = self.vy - 52
    @buttons[3].y = self.vy2 + 32
    @buttons[4].y = self.vy2 + 40
    @buttons[5].y = self.vy2 + 40
  end
  def refresh_z()
    super()
    @buttons.each { |but| but.z = self.z + 4 }
  end 
  def refresh_visible()
    super()
    @buttons.each { |but| but.visible = self.visible }
  end  
  def refresh_active()
    super()
    @buttons.each { |but| but.active = self.active }
  end  
  def refresh_viewport()
    super()
    @buttons.each { |but| but.viewport = self.viewport }
  end 
end
#=begin
class Wheel_Command
  include Mixin::WheelButtons
  def process_cursor_move()
    return unless cursor_movable?
    last_index = self.index
    @buttons.each { |but| but.update }
    wheel_up() if Input.repeat?( :UP )
    wheel_down() if Input.repeat?( :DOWN )
    wheel_left() if Input.repeat?( :LEFT )
    wheel_right() if Input.repeat?( :RIGHT )
    Sound.play_cursor() if self.index != last_index
  end 
end  
=begin
class Game::RogueCursor
  def update_input()
    return if moving?
    #d = Input.dir4
    #return unless d > 0
    mx = (_map.display_x / 32) + (MouseEx.x / 32) #_map.round_x_with_direction(@x, d)
    my = (_map.display_y / 32) + (MouseEx.y / 32) #_map.round_y_with_direction(@y, d)
    if _map.valid?( mx, my )
      @x, @y = mx, my#[[mx, _map.width-1].min, 0].max, [[my, _map.height-1].min, 0].max
      @real_x = mx#_map.x_with_direction(@x, reverse_dir(d))
      @real_y = my#_map.y_with_direction(@y, reverse_dir(d))
      RogueManager.call_handler( :on_cursor_move )
    end  
  end
end  
=end
module MapScroll
  def self._map
    $game_system.rogue? ? $game_rogue : $game_map
  end 
  def self.response_size
    64
  end    
  def self.pressure_effect( sp, press )
    sp.flash(Pallete[:sys_green].xset(:alpha=>(255*press).clamp(0,255)), 60) if sp
  end  
  def self.mscroll_up(press)
    pressure_effect(@edge_sprites[0], press)
    _map.scroll_up(press)
  end
  def self.mscroll_down(press)
    pressure_effect(@edge_sprites[1], press)
    _map.scroll_down(press)
  end
  def self.mscroll_left(press)
    pressure_effect(@edge_sprites[2], press)
    _map.scroll_left(press)
  end
  def self.mscroll_right(press)
    pressure_effect(@edge_sprites[3], press)
    _map.scroll_right(press)
  end 
  def self.view_edge
    if @view_edge.nil?
      @view_edge = Handler::ViewEdge.new(Graphics.play_rect.clone)
      @view_edge.set_handler(:left, method(:mscroll_left))
      @view_edge.set_handler(:right, method(:mscroll_right))
      @view_edge.set_handler(:up, method(:mscroll_up))
      @view_edge.set_handler(:down, method(:mscroll_down))
    end
    @view_edge
  end
  def self.edge_responses
    self.view_edge.edge_responses
  end  
  def self.use_egde_sprites?
    false
  end  
  def self.make_edges
    @edge_sprites = []
    if use_egde_sprites?
      @edge_sprites = self.view_edge.make_edge_sprites 
    end  
    #@x32_cursor = Sprite.new()
    #@x32_cursor.bitmap = Bitmap.new(32,32)
    #@x32_cursor.bitmap.fill_rect(@x32_cursor.bitmap.rect.contract(2), Pallete[:white])
    #@x32_cursor.bitmap.blur
    #@x32_cursor.bitmap.blur
    #@x32_cursor.opacity = 128
    #@x32_cursor.z = 20
  end  
  def self.edge_sprites()
    @edge_sprites
  end  
  def self.x32_cursor
    @x32_cursor
  end  
  def self.init
    edge_responses()
    make_edges()
  end  
end
module SceneManager
  class << self
    alias :post_mouse_addon_run :run
    def run(*args)
      MapScroll.init
      post_mouse_addon_run(*args)
    end  
  end  
end  
class Game::Map  
  alias :pre_mouse_update :update 
  def update(*args)
    pre_mouse_update(*args)
    MapScroll.view_edge.update if @cursor.active if @cursor
    MapScroll.edge_sprites.each { |e| e.update() }
    #MapScroll.x32_cursor.update
    #MapScroll.x32_cursor.x, MapScroll.x32_cursor.y = *mouse_x32_screen_pos()    
  end  
  def mouse_x32_pos_f
    cx, cy = *MouseEx.pos 
    #cx -= 16
    #cy -= 16
    cx = (display_x + (cx / 32.0))#.round(0)
    cy = (display_y + (cy / 32.0))#.round(0)
    return cx, cy
  end
  def mouse_x32_pos
    cx, cy = *mouse_x32_pos_f
    cx = cx.floor#.round(0)
    cy = cy.floor#.round(0)
    return cx, cy
  end
  def mouse_x32_screen_pos()
    cx, cy = mouse_x32_pos()
    return adjust_x(cx) * 32, adjust_y(cy) * 32
  end  
end  
class Scene::Rogue < Scene::Base
  #--------------------------------------------------------------------------#
  # ● new-method :refresh_spriteset
  #--------------------------------------------------------------------------#
  def trigger_cursor_mode( forced_off=false )
    if $game_rogue.cursor.active || forced_off
      _map.cursor.moveto( @subject.x, @subject.y )
      _map.release_character_table
      _map.deactivate_cursor 
      _map.cursor.focus_off
      _map.clear_active_ranges()
      @subject.focus_on
    else  
      @subject.focus_off
      _map.cursor.moveto( *_map.mouse_x32_pos )#@subject.x, @subject.y )
      _map.static_character_table
      _map.activate_cursor
      _map.cursor.focus_on
      on_cursor_move()
      Mouse.set_pos(_map.cursor.screen_x, _map.cursor.screen_y)
      #rngs = [[0, 1], [0, -1], 
      # [1, 0], [-1, 0]].collect { |r| [r[0] + @subject.x, r[1] + @subject.y] }
      #_map.add_ranges( rngs, Color.new(0, 32, 168).to_flash )
    end  
  end 
end  
end
