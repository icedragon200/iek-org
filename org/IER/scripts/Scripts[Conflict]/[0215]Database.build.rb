# Database.build
#==============================================================================#
# ■ Database.build
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 01/31/2012
# // • Data Modified : 02/02/2012
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 01/31/2012 V1.0 
#         Started script
#     ♣ 02/02/2012 V1.0 
#         Labelled everything
#==============================================================================#
module Database
  #--------------------------------------------------------------------------#
  # ■|► module-method :mk_db_location
  #--------------------------------------------------------------------------#
  def self.mk_db_location()
    Dir.mkdir(DB_LOCATION) unless(FileTest.exist?(DB_LOCATION))
  end  
  #--------------------------------------------------------------------------#
  # ■|► module-method :save_all!
  #--------------------------------------------------------------------------#
  def self.save_all!()
    (BUILD_SET_SYMBOLS-[:temp]).each { |s| send("save_"+s.to_s) }
  end
  #--------------------------------------------------------------------------#
  # ■|► module-method :save_all
  #--------------------------------------------------------------------------#
  def self.save_all()
    (BUILD_SET_SYMBOLS-[:temp]).each { |s| send("save_"+s.to_s) if @build_set[s] }
  end  
  #--------------------------------------------------------------------------#
  # ■|► module-method :load_all!
  #--------------------------------------------------------------------------#
  def self.load_all!()
    (BUILD_SET_SYMBOLS-[:temp]).each { |s| send("load_"+s.to_s) }
  end
  #--------------------------------------------------------------------------#
  # ■|► module-method :load_* || save_*
  #--------------------------------------------------------------------------#
  BUILD_FILENAMES.each_pair do |s,v|
    module_eval(%Q(
      def self.save_#{s.to_s}
        save_data(@#{s.to_s}, DB_LOCATION+"#{v}")
      end
      def self.load_#{s.to_s}
        @#{s.to_s} = load_data_cin(DB_LOCATION+"#{v}") { [] }
      end  
    ))
  end    
  #--------------------------------------------------------------------------#
  # ■|► module-method :load_system
  #--------------------------------------------------------------------------#
  def self.load_system()
    @system = load_data_cin(DB_LOCATION+"System.rvdata2") { RPG::System.new }
  end 
  #--------------------------------------------------------------------------#
  # ■|► module-method :load_startup
  #--------------------------------------------------------------------------#
  def self.load_startup()
    @startup = load_data_cin(DB_LOCATION+"Startup.rvdata2") { RPG::Startup.new }
  end  
  #--------------------------------------------------------------------------#
  # ■|► module-method :load_temp
  #--------------------------------------------------------------------------#
  def self.load_temp()
    @temp = load_data_cin(DB_LOCATION+"Temp.rvdata2") { Temp.new() }# // D:
  end  
  #--------------------------------------------------------------------------#
  # ■|► module-method :load_header
  #--------------------------------------------------------------------------#
  def self.load_header()
    @db_header = load_data_cin(DB_LOCATION+"Header.rvdata2") { Hash.new() }
    @db_header[:version_numbers] ||= {}
    if @db_header[:version_numbers].empty?
      @build_set.merge!(Hash[BUILD_SET_SYMBOLS.collect{|s|[s,true]}])
    else  
      d = nil
      @db_header[:version_numbers].each_pair { |key, value|
        d = @current_db_verions[key]
        @build_set[key] = d.nil? ? true : d != value
      }
    end
    @build_set[:temp] = true
  end  
  #--------------------------------------------------------------------------#
  # ■|► module-method :save_header
  #--------------------------------------------------------------------------#
  def self.save_header()
    BUILD_SET_SYMBOLS.each { |k|
      @db_header[:version_numbers][k] = @current_db_verions[k]
    } 
    save_data(@db_header, DB_LOCATION+"Header.rvdata2")
  end 
  #--------------------------------------------------------------------------#
  # ■|► module-method :load
  #--------------------------------------------------------------------------#
  def self.load()
    mk_db_location()
    load_header()
    BUILD_SET_SYMBOLS.each { |key|
      value = @build_set[key]
      value == true ? self.send("build_"+key.to_s) : self.send("load_"+key.to_s)
    }
    after_load()
  end 
  #--------------------------------------------------------------------------#
  # ■|► module-method :save
  #--------------------------------------------------------------------------#
  def self.save()
    mk_db_location()
    before_save()
    save_all()
    save_header()
  end 
  #--------------------------------------------------------------------------#
  # ■|► module-method :load_and_save
  #--------------------------------------------------------------------------#
  def self.load_and_save()
    load()
    save()
  end  
  #--------------------------------------------------------------------------#
  # ■|► module-method :before_save
  #--------------------------------------------------------------------------#
  def self.before_save()
  end  
  #--------------------------------------------------------------------------#
  # ■|► module-method :after_load
  #--------------------------------------------------------------------------#
  def self.after_load
    load_recipe_list()
    #sks = @skills.select{|s|s && s.id > 20}.collect{|s|s.id} 
    #@classes.compact.each{|cls|
    #  sks.each {|s|cls.learnings << RPG::Class::Learning.new().set( 1, s, '' )}
    #}
  end  
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
