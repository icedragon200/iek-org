# Window::Selectable (Overwrites)
#==============================================================================#
# ♥ Window::Selectable (Overwrites)
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/13/2011
# // • Data Modified : 01/26/2012
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#==============================================================================#
class Window::Selectable < Window::Base 
  def pred_index(wrap=true)
    self.index = self.index.pred
    self.index = wrap ? self.index.modulo(item_max) : self.index.clamp(0,item_max-1)
  end
  def succ_index(wrap=true)
    self.index = self.index.succ
    self.index = wrap ? self.index.modulo(item_max) : self.index.clamp(0,item_max-1)
  end 
  
  def process_cursor_move()
    return unless cursor_movable? && !win_busy?
    last_index = @index
    cursor_down (Input.trigger?(:DOWN))  if Input.repeat?(:DOWN)
    cursor_up   (Input.trigger?(:UP))    if Input.repeat?(:UP)
    cursor_right(Input.trigger?(:RIGHT)) if Input.repeat?(:RIGHT)
    cursor_left (Input.trigger?(:LEFT))  if Input.repeat?(:LEFT)
    cursor_pagedown   if !handle?(:pagedown) && Input.trigger?(:R)
    cursor_pageup     if !handle?(:pageup)   && Input.trigger?(:L)
    Sound.play_cursor if @index != last_index
  end
  def process_handling()
    return unless open? && active && !win_busy?
    return process_ok       if ok_enabled?        && Input.trigger?(:C)
    return process_cancel   if cancel_enabled?    && Input.trigger?(:B)
    return process_pagedown if handle?(:pagedown) && Input.trigger?(:R)
    return process_pageup   if handle?(:pageup)   && Input.trigger?(:L)
  end
  def row_index()
    return index / col_max
  end  
  def cursor_easers()
    #[:elastic_out, :elastic_out, :sine_out, :sine_out]
    # // x, y, width, height 
    @cursor_easers ||= [:sine_inout, :sine_inout, :sine_out, :sine_out] 
    return @cursor_easers 
  end  
  def cursor_times()
    @cursor_times ||= [7, 7, 10, 10]
    return @cursor_times # // x, y, width, height 
  end 
  def cursor_exparams()
    @cursor_exparams ||= [[],[],[],[]]
    return @cursor_exparams
  end  
  def update_cursor()
    rect = @target_cursor_rect
    if @cursor_all
      rect.set(0, 0, contents.width, row_max * item_height)
      self.top_row = 0
    elsif @index < 0
      cursor_rect.empty
      rect.empty
    else
      ensure_cursor_visible
      rect.set(item_rect(@index))
    end
    #rect.width = active ? rect.width : rect.height.min(rect.width)
    if @_last_cursor != a = [@index, item_max, col_max, @cursor_all, active] 
      easers = cursor_easers()
      times  = cursor_times().collect { |t| Tween.frames_to_tt(t) }
      exp    = cursor_exparams()
      cra    = cursor_rect.to_a
      tcra   = @target_cursor_rect.to_a
      @_cursor_tweener.clear()
      for i in 0...4
        @_cursor_tweener.add_tween( cra[i],tcra[i],easers[i],times[i],exp[i] )
      end  
      @_last_cursor = a
    end  
  end
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
