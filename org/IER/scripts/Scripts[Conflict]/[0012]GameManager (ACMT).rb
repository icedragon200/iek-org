# GameManager (ACMT)
#==============================================================================#
# ■ GameManager (ACMT)
#==============================================================================#
# // • Created By    : IceDragon
# // • Data Created  : 12/27/2011
# // • Data Modified : 02/02/2012
# // • Version       : 1.0a
#==============================================================================#
# // This module handles the "Game's" achievements
#==============================================================================#
# ● Change Log
#     ♣ 12/27/2011 V1.0 
#==============================================================================#
module GameManager
  ACMT = true
  add_acmts( 
    make_acmt(1,"Tried Earthen","Thank you for trying Earthen Dawn of Smiths")
  )
  REPONSES = {}
  gname = "CodeJIFZ"
  REPONSES[gname] = {}
  REPONSES[gname][1] = "Good Job, we will add more gold to your party :3"
  REPONSES[gname][2] = "She was hard wasn't she, check for a new shield recipe"
  REPONSES[gname][3] = "Fenix is awesome, nuff said, have a fireball spell page"
  REPONSES[gname][4] = "Omegas didnt have his own way for long, gold for your troubles?"
  REPONSES[gname][5] = "Problem?, how about a Meoteor II spell page o3o?"
  def self.acmt_response( game_name, code )
    r = REPONSES[game_name]
    return nil unless r
    return r[code]
  end  
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
