# Sprite::MinimapCharacter
#==============================================================================#
# ♥ Sprite::MinimapCharacter
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/26/2011
# // • Data Modified : 12/27/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/26/2011 V1.0 
#
#==============================================================================#
class Sprite::MinimapCharacter < Sprite
  attr_reader :mm_width
  attr_reader :mm_height
  attr_reader :scale
  attr_accessor :character
  def initialize( viewport, character )
    super( viewport )
    @character = character
    self.bitmap = Cache.system("IsoCharacter")
    self.ox = 1
    self.oy = 8
    self.z  = 201
    @twidth = 16
    @theight= 16
    @mm_width  = 0
    @mm_height = 0
    self.scale = 1.0
    @bobbing_count = 0
  end  
  def mm_width=( new_mm_width )
    if @mm_width != new_mm_width
      @mm_width = new_mm_width
      refresh_scale()
    end  
  end  
  def mm_height=( new_mm_height )
    if @mm_height != new_mm_height
      @mm_height = new_mm_height
      refresh_scale()
    end
  end
  def scale=( new_scale )
    if @scale != new_scale
      @scale = new_scale
      refresh_scale()
    end
  end 
  def refresh_scale()
    sc = @scale / 1.0
    @mm_swidth  = @mm_width * sc
    @mm_sheight = @mm_height * sc
    @twidth     = 16 * sc
    @theight    = 16 * sc
    self.zoom_x = self.zoom_y = sc
  end  
  def update()
    super()
    if @character.mm_bobbing?()
      @bobbing_count = (@bobbing_count + 1) % 60
      @add_y = Math.sin( @bobbing_count / 60.0 * Math::PI ) * @character.mm_bob_amount
    else  
      @bobbing_count = 0
      @add_y = 0
    end  
    self.x, self.y = *Isometric.calc_screen_xy( 
        @character.mm_x, @character.mm_y, @twidth, @theight, @mm_swidth, @mm_sheight )
    self.z = 201 + @character.real_y   
    self.y -= @add_y
    if @character.is_a?( Game::RogueCursor )
      self.z += 100
      self.tone.set( -119, 0, -119, 0 )
    else  
      if @character.foe?
        self.tone.set( 0, -119, -220, 0 )
      elsif @character.friend?
        self.tone.set( -220, -119, 0, 0 )
      end 
      self.tone.average!( 0.3 ) if @character.dead? 
    end  
  end  
  def remove?
    @character.remove_sprite? #&& !effect?
  end    
end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
