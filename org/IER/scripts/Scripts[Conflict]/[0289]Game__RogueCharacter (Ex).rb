# Game::RogueCharacter (Ex)
class Game::RogueCharacter
  SPECIAL_EFFECT_XY_TRIG = 101
  SPECIAL_EFFECT_TAME    = 102
  SPECIAL_EFFECT_RANDWARP= 103
  def item_effect_special(user, item, effect)
    @result.success = true
    case effect.data_id
    when SPECIAL_EFFECT_ESCAPE
      escape
    when SPECIAL_EFFECT_XY_TRIG
      @result.set_xy_trig( true, self.x, self.y )
    when SPECIAL_EFFECT_TAME
      if enemy?
        item_effect_tame( user, item ) if user.alignment != self.alignment
      else
        @result.success = false
      end
    when SPECIAL_EFFECT_RANDWARP
      set_rwarp_action()
    end
    #@result.success = true
  end
  def item_effect_tame( user, item )
    chance = user.tme# - tme
    chance *= luk_effect_rate( user )
    @alignment = rand < chance ? user.alignment : self.alignment
    refresh_alignment()
  end
  # Tamer Points
  def tme()
    1.0 * hp_rate
  end
  # // 01/21/2012
  def start_map_event(x, y, triggers, normal)
    _map.events_xy(x, y).each do |event|
      if event.trigger_in?(triggers) && event.normal_priority? == normal
        event.start
      end
    end
  end 
  def check_event_trigger_here(triggers)
    start_map_event(@x, @y, triggers, false)
  end
  def check_event_trigger_there(triggers)
    x2 = _map.round_x_with_direction(@x, @direction)
    y2 = _map.round_y_with_direction(@y, @direction)
    start_map_event(x2, y2, triggers, true)
    return if _map.any_event_starting?
    return unless _map.counter?(x2, y2)
    x3 = _map.round_x_with_direction(x2, @direction)
    y3 = _map.round_y_with_direction(y2, @direction)
    start_map_event(x3, y3, triggers, true)
  end
  def check_event_trigger_touch(x, y)
    start_map_event(x, y, [1,2], true)
  end
  def check_touch_event
    return false if in_airship?
    check_event_trigger_here([1,2])
    _map.setup_starting_event
  end
  def check_action_event
    return false if in_airship?
    check_event_trigger_here([0])
    return true if _map.setup_starting_event
    check_event_trigger_there([0,1,2])
    _map.setup_starting_event
  end
  # // 03/07/2012
  # // Overwrite
  def item_apply(user, item)
    @result.clear
    @result.used = item_test(user, item)
    @result.missed = (@result.used && rand >= item_hit(user, item))
    @result.evaded = (!@result.missed && rand < item_eva(user, item))
    if @result.hit?
      unless item.damage.none?
        @result.critical = (rand < item_cri(user, item))
        make_damage_value(user, item)
        execute_damage(user,item)
      end
      item.effects.each {|effect| item_effect_apply(user, item, effect) }
      item_user_effect(user, item)
      on_item_hit(user, item)
    end
  end
  def execute_damage(user,item=nil)
    on_damage(@result.hp_damage,item) if @result.hp_damage > 0
    self.hp -= @result.hp_damage
    self.mp -= @result.mp_damage
    user.hp += @result.hp_drain
    user.mp += @result.mp_drain
  end
  def on_damage(value,item=nil)
    remove_states_by_damage
    charge_tp_by_damage(value.to_f / mhp)
  end
  def on_item_hit(user,item)
    # // . x . Nothing Here but us Whitespace
  end  
end  
