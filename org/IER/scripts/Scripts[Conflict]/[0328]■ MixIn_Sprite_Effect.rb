# ■ MixIn_Sprite_Effect
#==============================================================================#
# ■ Mixin::Sprite_Effect
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/26/2011
# // • Data Modified : 12/26/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/26/2011 V1.0 
#
#==============================================================================# 
module Mixin::Sprite_Effect
  def setup_new_effect
    if !@character_visible && @character.alive?
      start_effect(:appear)
    elsif @character_visible && @character.hidden?
      start_effect(:disappear)
    end
    if @character_visible && @character.sprite_effect_type
      start_effect(@character.sprite_effect_type)
      @character.set_sprite_effect_type( nil )
    end
    if !@balloon_sprite && @character.balloon_id > 0
      @balloon_id = @character.balloon_id
      start_balloon
    end
  end
  def start_effect(effect_type)
    @effect_type = effect_type
    case @effect_type
    when :appear
      @effect_duration = 16
      @character_visible = true
    when :disappear
      @effect_duration = 32
      @character_visible = false
    when :whiten
      @effect_duration = 16
      @character_visible = true
    when :blink
      @effect_duration = 20
      @character_visible = true
    when :collapse
      @effect_duration = 48
      @character_visible = false
    when :boss_collapse
      @effect_duration = bitmap.height
      @character_visible = false
    when :instant_collapse
      @effect_duration = 16
      @character_visible = false
    end
    revert_to_normal
  end
  def revert_to_normal
    self.blend_type = 0
    self.color.set(0, 0, 0, 0)
    self.opacity = 255
    self.ox = @cw / 2
    self.oy = @ch
    #self.src_rect.y = 0
  end
  def setup_new_animation
    if @character.animation_id > 0
      animation = $data_animations[@character.animation_id]
      mirror = @character.animation_mirror
      start_animation(animation, mirror)
      @character.animation_id = 0
    end
  end
  def effect?
    @effect_type != nil #|| @animation != nil || @balloon_sprite != nil
  end
  def update_effect
    if @effect_duration > 0
      @effect_duration -= 1
      case @effect_type
      when :whiten
        update_whiten
      when :blink
        update_blink
      when :appear
        update_appear
      when :disappear
        update_disappear
      when :collapse
        update_collapse
      when :boss_collapse
        update_boss_collapse
      when :instant_collapse
        update_instant_collapse
      end
      @effect_type = nil if @effect_duration == 0
    end 
  end
  def update_whiten
    self.color.set(255, 255, 255, 0)
    self.color.alpha = 128 - (16 - @effect_duration) * 10
  end
  def update_blink
    self.opacity = (@effect_duration % 10 < 5) ? 255 : 0
  end
  def update_appear
    self.opacity = (16 - @effect_duration) * 16
  end
  def update_disappear
    self.opacity = 256 - (32 - @effect_duration) * 10
  end
  def update_collapse
    self.blend_type = 1
    self.color.set(255, 128, 128, 128)
    self.opacity = 256 - (48 - @effect_duration) * 6
  end
  def update_boss_collapse
    alpha = @effect_duration * 120 / bitmap.height
    #self.ox = bitmap.width / 2 + @effect_duration % 2 * 4 - 2
    self.blend_type = 1
    self.color.set(255, 255, 255, 255 - alpha)
    self.opacity = alpha
    self.src_rect.y -= 1
    Sound.play_boss_collapse2 if @effect_duration % 20 == 19
  end
  def update_instant_collapse
    self.opacity = 0
  end
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
