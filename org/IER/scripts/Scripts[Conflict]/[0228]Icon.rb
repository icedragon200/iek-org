# Icon
#==============================================================================#
# ■ Icon
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/10/2011
# // • Data Modified : 12/10/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/10/2011 V1.0 
#
#==============================================================================#
module Icon 
  def self.atk()
    return 398
  end  
  def self.def()
    return 502
  end  
  def self.mat()
    return 414
  end
  def self.mdf()
    return 501
  end  
  def self.agi()
    return 471
  end  
  def self.luk()
    return 125
  end    
  def self.element(id)
    DB.element_icon_by_id(id)  
  end  
  def self.cost(type)
    DB.cost_icon(type)
  end  
  def self.rogue(rogue_id)
    DB.rogue_icon(rogue_id) 
  end 
end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
