# GUIExt
#==============================================================================#
# ■ GUIExt
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/28/2011
# // • Data Modified : 12/28/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/28/2011 V1.0
#
#==============================================================================#
module GUIExt
  def self.repeat_header_bmp_vert( info )
    bitmap        = info[:bitmap]
    dbmp          = info[:draw_bmp]
    x, y          = info[:x] || 0, info[:y] || 0
    width         = info[:width] || (bitmap ? bitmap.width : 24)
    height        = info[:height] || (bitmap ? bitmap.height : 48)
    startrect     = info[:startrect] || Rect.new( 0, 0, width, 32 )
    endrect       = info[:endrect] || Rect.new( 0, dbmp.height-32, width, 32 )
    midrect       = info[:midrect] || Rect.new( 0, 32, width, 32 )
    opacity       = info[:opacity] || 255
    bitmap.blt( x, y, dbmp, startrect, opacity )
    bitmap.blt( x, y+(height-endrect.height), dbmp, endrect, opacity )
    dx, dy = x, y+startrect.height
    dh = height - (startrect.height + endrect.height)
    bitmap.ext_repeat_bmp_vert( 
      :x => dx, :y => dy, :length => dh,
      :draw_bmp => dbmp, :rect => midrect, :opacity => opacity
    )
  end
  def self.repeat_header_bmp_horz( info )
    bitmap        = info[:bitmap]
    dbmp          = info[:draw_bmp]
    x, y          = info[:x] || 0, info[:y] || 0
    width         = info[:width] || (bitmap ? bitmap.width : 48)
    height        = info[:height] || (bitmap ? bitmap.height : 24)
    startrect     = info[:startrect] || Rect.new( 0 , 0, 32, height )
    endrect       = info[:endrect] || Rect.new( dbmp.width-32,  0, 32, height )
    midrect       = info[:midrect] || Rect.new( 32, 0, 32, height )
    opacity       = info[:opacity] || 255
    bitmap.blt( x, y, dbmp, startrect, opacity )
    bitmap.blt( x+(width-endrect.width), y, dbmp, endrect, opacity )
    dx, dy = x+startrect.width, y
    dw = width - (startrect.width + endrect.width)
    bitmap.ext_repeat_bmp_horz( 
      :x => dx, :y => dy, :length => dw, 
      :draw_bmp => dbmp, :rect => midrect, :opacity => opacity 
    )
  end  
  def self.draw_slider_base_vert( info )
    bitmap        = info[:bitmap]
    width         = info[:width] || (bitmap ? bitmap.width : 24)
    height        = info[:height] || (bitmap ? bitmap.height : 48)
    base_bmp      = info[:base_bmp] || Cache.system("Gui_Slider1")
    height = 8 if height < 8
    hg = height <= 48 ? height / 2 : 24
    repeat_header_bmp_vert( 
      :bitmap    => bitmap,
      :x => info[:x], :y => info[:y],
      :width     => width,
      :height    => height,
      :draw_bmp  => base_bmp,
      :startrect => Rect.new( 0,  0, width, hg ),
      :endrect   => Rect.new( 0, 72-hg, width, hg ),
      :midrect   => Rect.new( 0, 24, width, 24 )
    )
  end
  def self.draw_slider_base_horz( info )
    bitmap        = info[:bitmap]
    width         = info[:width] || (bitmap ? bitmap.width : 48)
    height        = info[:height] || (bitmap ? bitmap.height : 24)
    base_bmp      = info[:base_bmp] || Cache.system("Gui_Slider2")
    width = 8 if width < 8
    wd = width <= 48 ? width / 2 : 24
    repeat_header_bmp_horz( 
      :bitmap    => bitmap,
      :x => info[:x], :y => info[:y],
      :width     => width,
      :height    => height,
      :draw_bmp  => base_bmp,
      :startrect => Rect.new( 0 , 0, wd, height ),
      :endrect   => Rect.new( 72-wd,  0, wd, height ),
      :midrect   => Rect.new( 24, 0, 24, height )
    )
  end 
  def self.draw_header_base( info )
    bitmap        = info[:bitmap]
    width         = info[:width] || (bitmap ? bitmap.width : 128)
    height        = info[:height] || (bitmap ? bitmap.height : 14)
    base_bmp      = info[:base_bmp] || Cache.system("Header_Base(Window)")
    height = height.min(base_bmp.height)
    width  = base_bmp.width if width < base_bmp.width
    repeat_header_bmp_horz( 
      :bitmap    => bitmap,
      :x => info[:x], :y => info[:y],
      :width     => width,
      :height    => height,
      :draw_bmp  => base_bmp
    )
  end 
  def self.draw_equip_header_base( info )
    bitmap        = info[:bitmap]
    width         = info[:width] || (bitmap ? bitmap.width : 128)
    height        = info[:height] || (bitmap ? bitmap.height : 38)
    base_bmp      = info[:base_bmp] || Cache.system("Equipment_Tab(Window)")
    height = height.min(base_bmp.height)
    width  = base_bmp.width if width < base_bmp.width
    repeat_header_bmp_horz( 
      :bitmap    => bitmap,
      :x => info[:x], :y => info[:y],
      :width     => width,
      :height    => height,
      :draw_bmp  => base_bmp
    )
  end
  # // 02/03/2012
  def self.draw_help_header( info )
    bitmap        = info[:bitmap]
    width         = info[:width] || (bitmap ? bitmap.width : 128)
    height        = info[:height] || (bitmap ? bitmap.height : 24)
    base_bmp      = info[:base_bmp] || Cache.system("Header_Help(Sprite)")
    height = height.min(base_bmp.height)
    #width  = guibmp.width if width < guibmp.width
    repeat_header_bmp_horz( 
      :bitmap    => bitmap,
      :x => info[:x], :y => info[:y],
      :width     => width,
      :height    => height,
      :startrect => Rect.new( 0, 0, 8,base_bmp.height),
      :midrect   => Rect.new( 8, 0,80,base_bmp.height),
      :endrect   => Rect.new(96-8,0,8,base_bmp.height),
      :draw_bmp  => base_bmp
    )
  end  
  # // 02/06/2012
  def self.draw_option_header( info )
    bitmap        = info[:bitmap]
    width         = info[:width] || (bitmap ? bitmap.width : 172)
    height        = info[:height] || (bitmap ? bitmap.height : 14)
    base_bmp      = info[:base_bmp] || Cache.system("Header_Options(Window)")
    height = height.min(base_bmp.height)
    #width  = guibmp.width if width < guibmp.width
    repeat_header_bmp_horz( 
      :bitmap    => bitmap,
      :x => info[:x], :y => info[:y],
      :width     => width,
      :height    => height,
      :startrect => Rect.new( 0, 0,32,base_bmp.height),
      :midrect   => Rect.new(32, 0,32,base_bmp.height),
      :endrect   => Rect.new(64, 0,64,base_bmp.height),
      :draw_bmp  => base_bmp
    )
  end
  def self.draw_tab( info )
    bitmap        = info[:bitmap]
    width         = info[:width] || (bitmap ? bitmap.width : 96)
    height        = info[:height] || (bitmap ? bitmap.height : 24)
    base_bmp      = info[:base_bmp] || Cache.system("Header_Tab(Sprite)")
    height = height.min(base_bmp.height)
    #width  = guibmp.width if width < guibmp.width
    repeat_header_bmp_horz( 
      :bitmap    => bitmap,
      :x => info[:x], :y => info[:y],
      :width     => width,
      :height    => height,
      :draw_bmp  => base_bmp
    )
  end
  def self.draw_tail_base( info )
    bitmap        = info[:bitmap]
    width         = info[:width] || (bitmap ? bitmap.width : 128)
    height        = info[:height] || (bitmap ? bitmap.height : 14)
    base_bmp      = info[:base_bmp] || Cache.system("Tail_Base(Window)")
    height = height.min(base_bmp.height)
    width  = base_bmp.width if width < base_bmp.width
    repeat_header_bmp_horz( 
      :bitmap    => bitmap,
      :x => info[:x], :y => info[:y],
      :width     => width,
      :height    => height,
      :draw_bmp  => base_bmp
    )
  end
  # // 02/18/2012
  def self.draw_skill_border( info )
    bitmap        = info[:bitmap]
    width         = info[:width] || (bitmap ? bitmap.width : 172)
    height        = info[:height] || (bitmap ? bitmap.height : 24)
    base_bmp      = info[:base_bmp] || Cache.system("Skill_Borders(Window)")
    height = height.min(24)
    #width  = guibmp.width if width < guibmp.width
    dy = 24 * (info[:index] || 0)
    dh = 24
    repeat_header_bmp_horz( 
      :bitmap    => bitmap,
      :x => info[:x], :y => info[:y],
      :width     => width,
      :height    => height,
      :startrect => Rect.new( 0, dy,40,dh),
      :midrect   => Rect.new(40, dy,42,dh),
      :endrect   => Rect.new(82, dy,30,dh),
      :draw_bmp  => base_bmp,
      :opacity   => info[:opacity] || 255
    )
  end
  # // 02/25/2012
  def self.draw_art_border( info )
    bitmap        = info[:bitmap]
    width         = info[:width] || (bitmap ? bitmap.width : 96)
    height        = info[:height] || (bitmap ? bitmap.height : 24)
    base_bmp      = info[:base_bmp] || Cache.system("Art_Border(Window)")
    height = height.min(24)
    #width  = guibmp.width if width < guibmp.width
    dy = 0
    dh = 24
    repeat_header_bmp_horz( 
      :bitmap    => bitmap,
      :x => info[:x], :y => info[:y],
      :width     => width,
      :height    => height,
      :startrect => Rect.new( 0, dy,24,dh),
      :midrect   => Rect.new(24, dy,48,dh),
      :endrect   => Rect.new(72, dy,24,dh),
      :draw_bmp  => base_bmp,
      :opacity   => info[:opacity] || 255
    )
  end
end
#==============================================================================#
# ■ GUIExt::Include
#==============================================================================#
module GUIExt::Include
  def gui_draw_slider_base_vert( info={} )
    ::GUIExt.draw_slider_base_vert( _ext_set_bitmap( info ) )
  end
  def gui_draw_slider_base_horz( info={} )
    ::GUIExt.draw_slider_base_horz( _ext_set_bitmap( info ) )
  end
  def gui_draw_header_base( info={} )
    ::GUIExt.draw_header_base( _ext_set_bitmap( info ) )
  end  
  def gui_draw_equip_header_base( info={} )
    ::GUIExt.draw_equip_header_base( _ext_set_bitmap( info ) )
  end 
  # // 02/03/2012
  def gui_draw_help_header( info={} )
    ::GUIExt.draw_help_header( _ext_set_bitmap( info ) )
  end  
  # // 02/06/2012
  def gui_draw_option_header( info={} )
    ::GUIExt.draw_option_header( _ext_set_bitmap( info ) )
  end
  def gui_draw_tab( info={} )
    ::GUIExt.draw_tab( _ext_set_bitmap( info ) )
  end  
  def gui_draw_tail_base( info={} )
    ::GUIExt.draw_tail_base( _ext_set_bitmap( info ) )
  end
  # // 02/18/2012
  def gui_draw_skill_border( info={} )
    ::GUIExt.draw_skill_border( _ext_set_bitmap( info ) )
  end  
  # // 02/25/2012
  def gui_draw_art_border( info={} )
    ::GUIExt.draw_art_border( _ext_set_bitmap( info ) )
  end  
end  
#==============================================================================#
# ♥ Bitmap
#==============================================================================#
class Bitmap
  include GUIExt::Include
end
#==============================================================================#
# ♥ Artist
#==============================================================================#
class Artist
  include GUIExt::Include
end
#==============================================================================#
# ♥ Sprite::SliderHandle
#==============================================================================#
class Sprite::SliderHandle < Sprite
  attr_reader :type
  attr_reader :active
  def initialize( viewport, type=:vert, x=0, y=0 )
    super( viewport )
    self.x = x
    self.y = y
    @active = false
    self.type = type
  end  
  def active=( active )
    if @active != active
      @active = active
      update_type
    end
  end  
  def activate()
    self.active = true
  end  
  def deactivate()
    self.active = false
  end  
  def type=( nop )
    if @type != nop
      @type = nop
      update_type()
    end
  end  
  def update_type()
    case @type
    when :vert
      self.bitmap = Cache.system( "Gui_Slider1" )
      self.src_rect.set( 0, 24*3, 24, 24 )
      self.src_rect.y += 24 if self.active
    when :horz
      self.bitmap = Cache.system( "Gui_Slider2" )
      self.src_rect.set( 24*3, 0, 24, 24 )
      self.src_rect.x += 24 if self.active
    end
  end  
end 
# // 03/03/2012 [
class Sprite::SliderBase < Sprite
end  
class Sprite::SliderBar < Sprite
end
# // 03/03/2012 ]
# // 01/25/2012
# // 01/25/2012
#==============================================================================#
# ♥ GUIExt::GuiObject_Base
#==============================================================================#
class GUIExt::GuiObject_Base < Container
  attr_accessor :ax, :ay, :az
  def initialize()
    super(0,0,0,0)
    @handler = {}
    @help_text = ""
    @help_window = nil
    aset(0,0,0)
  end 
  def aset(ax,ay,az)
    @ax, @ay, @az = ax, ay, az
    self
  end 
  def set_handler(symbol, method)
    @handler[symbol] = method
  end
  def handle?(symbol)
    @handler.include?(symbol)
  end
  def call_handler(symbol)
    @handler[symbol].call if handle?(symbol)
  end  
    #_update_position()
    #_update_oposition()
    #_update_size()
    #_update_active()
    #_update_visible()
    #_update_viewport()
    #_update_opacity()
    #_update_openness()
    #_update_padding() 
  attr_reader :help_window
  def help_window=(help_window)
    @help_window = help_window
    call_update_help
  end 
  def call_update_help
    update_help if active && @help_window
  end
  def update_help # // No Text
    @help_window.clear
  end
  attr_reader :help_text
  def help_text=(n)
    if @help_text != n
      @help_text = n
      call_update_help()
    end  
  end  
  def update_help # // @help_text
    @help_window.set_text(@help_text)
  end
end 
# // 02/06/2012
class GUIExt::GuiObject_Base_ws < GUIExt::GuiObject_Base # << With Sprite
  def _objs
    super + [@sprite]
  end    
end  
#==============================================================================#
# ♥ GUIExt::Slider
#==============================================================================#
class GUIExt::Slider < GUIExt::GuiObject_Base
  attr_reader :length
  attr_reader :type
  attr_reader :rate
  attr_reader :bar_color
  attr_reader :max 
  attr_reader :min
  attr_accessor :allow_input
  attr_accessor :tick_size
  def initialize( viewport=nil, type=:horz, length=128, x=0, y=0, z=0 )
    @length       = length
    @type         = type
    @rate         = 0.0
    @bar_color    = Color.new( 0, 0, 0, 0 )
    @allow_input  = false
    @max          = 1.0
    @min          = 0.0
    @tick_size    = 0.01
    super()
    self.viewport = viewport
    self.x = x
    self.y = y
    self.z = z
    self.active  = false
    self.visible = true
    self.opacity = 255
    refresh()
  end
  def init_internal
    super()
    @base         = Sprite::SliderBase.new( @viewport )
    @bar          = Sprite::SliderBar.new( @viewport )
    @handle       = Sprite::SliderHandle.new( @viewport, type, 0, 0 )
  end  
  def dispose
    @base.dispose_all()
    @bar.dispose_all()
    @handle.dispose()
    super()
  end  
  def active_opacity
    255
  end  
  def inactive_opacity
    128
  end  
  def max=( n )
    if @max != n
      @max = n
      update_rate()
    end  
  end
  def min=( n )
    if @min != n
      @min = n
      update_rate()
    end  
  end
  def _update_position()
    super()
    @base.x = @bar.x = @handle.x = self.x
    @base.y = @bar.y = @handle.y = self.y
    @handle.z = 1 + @bar.z = 1 + @base.z = self.z
  end
  def _update_visible()
    super()
    @handle.visible = @bar.visible = @base.visible = self.visible
  end
  def _update_viewport()
    super()
    @base.viewport = @bar.viewport = @handle.viewport = self.viewport
  end 
  def _update_active
    super()
    _update_opacity()
    @handle.active = self.active
  end  
  def _update_opacity
    super()
    @bar.opacity = self.active ? active_opacity : inactive_opacity
  end  
  def type=( type )
    if @type != type
      @type = type
      refresh()
    end
  end  
  def rate=( rate )
    if @rate != rate
      @rate = rate
      update_rate()  
    end
  end   
  def length=( length )
    if @length != length
      @length = length
      refresh()
    end  
  end 
  def bar_color=( bar_color )
    if @bar_color != bar_color
      @bar_color = bar_color
      refresh_bar_color
    end
  end  
  def force_bar_color(bar_color)
    @bar_color = bar_color
    refresh_bar_color()
  end  
  def refresh_bar_color
    @bar.color = @bar_color
  end  
  def refresh
    @base.bitmap.dispose unless @base.bitmap.disposed? if @base.bitmap
    @bar.bitmap.dispose unless @bar.bitmap.disposed? if @bar.bitmap
    case @type
    when :vert
      wd, hg = 24, @length
      @base.bitmap = Bitmap.new( wd, hg )
      @bar.bitmap  = Bitmap.new( wd/2, hg-2 )
      @bar.ox      = -((@base.width - @bar.bitmap.width) / 2)
      @bar.oy      = -1
      @base.bitmap.gui_draw_slider_base_vert()
    when :horz  
      wd, hg = @length, 24
      @base.bitmap = Bitmap.new( wd, hg )
      @bar.bitmap  = Bitmap.new( wd-2, hg/2 )
      @bar.ox      = -1
      @bar.oy      = -((@base.height - @bar.bitmap.height) / 2)
      @base.bitmap.gui_draw_slider_base_horz()
    end 
    @bar.color = @bar_color
    redraw_bar()
    update_rate()
    _update_all()
  end  
  def redraw_bar()
    @bar.bitmap.clear()
    @bar.bitmap.fill_rect( @bar.bitmap.rect.contract(2), Color.new( 0, 0, 0, 255) )
    @bar.bitmap.blur
  end  
  def update()
    super()
    process_slider_move()
    process_handling()
  end 
  def rate_offset( n1, n2 )
    n1o = n1 < 0 ? n1.abs : (n1 > 0 ? -n1 : n1)
    n2o = n2 < 0 ? n2.abs : 0
    return n2 + n1o + n2o, n1o, n2o
  end  
  def fix_rate_offset( r, n1, n2 )
    n, n1o, n2o = *rate_offset( n1, n2 )
    return r + n1o, n
  end  
  def calc_rate( r, m, mx )
    r, v = *fix_rate_offset(r, m, mx)
    r / v
  end  
  def update_rate
    r = calc_rate( @rate, self.min, self.max )
    case @type
    when :vert
      @bar.src_rect.set( 0, 0, @bar.bitmap.width, @bar.bitmap.height * r )
      @handle.oy = -((@base.height * r) - (@handle.height / 2))
    when :horz  
      @bar.src_rect.set( 0, 0, @bar.bitmap.width * r, @bar.bitmap.height )
      @handle.ox = -((@base.width * r) - (@handle.width / 2))
    end
  end 
  def process_slider_move()
    return unless active && input_enabled?
    last_rate = self.rate
    case @type
    when :vert
      if Input.press?(:DOWN)
        self.rate = (self.rate + @tick_size).min(max)
      elsif Input.press?(:UP) 
        self.rate = (self.rate - @tick_size).max(min)
      end  
    when :horz
      if Input.press?(:RIGHT)
        self.rate = (self.rate + @tick_size).min(max)
      elsif Input.press?(:LEFT) 
        self.rate = (self.rate - @tick_size).max(min)
      end    
    end  
    if Input.trigger?(:R)
      self.rate = (self.rate + @tick_size * 10).min(max)
    elsif Input.trigger?(:L)
      self.rate = (self.rate - @tick_size * 10).max(min)
    end  
    if self.rate != last_rate
      Sound.play_cursor 
      call_handler( :on_change ) if handle?(:on_change)
    end  
  end  
  def process_handling
    return unless active && input_enabled?
    return process_ok       if ok_enabled?        && Input.trigger?(:C)
    return process_cancel   if cancel_enabled?    && Input.trigger?(:B)
  end
  def ok_enabled?
    handle?(:ok)
  end
  def cancel_enabled?
    handle?(:cancel)
  end
  def process_ok
    Sound.play_ok
    Input.update
    deactivate
    call_ok_handler
  end
  def call_ok_handler
    call_handler(:ok)
  end
  def process_cancel
    Sound.play_cancel
    Input.update
    deactivate
    call_cancel_handler
  end
  def call_cancel_handler
    call_handler(:cancel)
  end
  def input_enabled?
    return @allow_input
  end
  def enable_input
    self.allow_input = true
  end  
  def disable_input
    self.allow_input = false
  end  
end  
# // 01/25/2012
# // 01/25/2012
#==============================================================================#
# ♥ Sprite::Button
#==============================================================================#
class Sprite::Button < Sprite
  attr_reader :index
  attr_reader :button_width
  attr_reader :button_height
  def initialize(viewport, button_index=0)
    super(viewport)
    self.bitmap = Cache.system("Gui_Buttons")
    @index = -1
    @button_width  = 32 
    @button_height = 32 
    @button_col = 4
    self.index = button_index
  end
  def set(index=@index,width=@button_width,height=@button_height)
    @index = index
    @button_width  = width
    @button_height = height
    update_src_rect()
  end  
  def button_width=(n)
    if @button_width != n
      @button_width = n
      update_src_rect
    end  
  end  
  def button_height=(n)
    if @button_height != n
      @button_height = n
      update_src_rect
    end  
  end
  attr_reader :button_col
  def button_col=(n)
    if @button_col != n
      @button_col = n
      update_src_rect()
    end  
  end  
  def index=(n)
    if @index != n
      @index = n 
      update_src_rect
    end  
  end 
  def update_src_rect()
    self.src_rect.set(
      @button_width*(@index%@button_col), 
      @button_height*(@index/@button_col), 
      @button_width, 
      @button_height
    )
  end  
end  
# // 02/06/2012
# // 02/06/2012
#==============================================================================#
# ♥ GUIExt::MixIn_AOV
#==============================================================================#
module GUIExt::MixIn_AOV
  attr_reader :aov
  def init_aov
    @aov = Mouse::AreaOnEvent.new(self)
    @aov.set_handler(:over, method(:on_mouse_over))
    @aov.set_handler(:not_over, method(:on_mouse_not_over))
    @aov.set_handler(:left_click, method(:on_mouse_left_click))
  end  
  def on_mouse_over
    call_update_help()
  end  
  def on_mouse_not_over
  end  
  def on_mouse_left_click
    call_handler(:click)
  end
  def update()
    super()
    process_handling()  
  end  
  def process_handling()
    return unless self.active && self.visible
    @aov.update()
  end
end  
# // 01/25/2012
# // 01/25/2012
#==============================================================================#
# ♥ GUIExt::Button_Base
#==============================================================================#
class GUIExt::Button_Base < GUIExt::GuiObject_Base_ws
  include GUIExt::MixIn_AOV
  attr_reader :button_index
  def initialize( viewport=nil, x=0, y=0, bindex=3 )
    @button_index = bindex
    super()
    self.x = x
    self.y = y
    self.z = 0
    self.visible  = true
    self.viewport = viewport
    self.opacity = 255
    @over_color        = Pallete[:sys_blue2].xset(:alpha=>96)
    @not_over_color    = Pallete[:white].xset(:alpha=>0)
    @click_flash_color = Pallete[:white].xset(:alpha=>128)
    self.width = self.height = 32
    @flash_counter = 0
    init_aov()
  end 
  def init_internal()
    super()
    @sprite = Sprite::Button.new(nil, @button_index)
  end  
  def over_color
    @over_color 
  end  
  def not_over_color()
    @not_over_color
  end  
  def on_mouse_over
    @sprite.color.set(over_color)
    super()
  end  
  def on_mouse_not_over
    @sprite.color.set(not_over_color)
    super()
  end  
  def on_mouse_left_click
    @flash_counter = flash_time
    @sprite.flash( @click_flash_color, flash_time )
    super()
  end  
  def flash_time
    20
  end  
  def button_index=(n)
    if @button_index != n
      @button_index = n
      refresh_button_index 
    end
  end  
  def refresh_button_index
    @sprite.index = @button_index
  end   
  def update()
    @flash_counter = @flash_counter.pred.max(0) if @flash_counter > 0
    super()
  end  
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
