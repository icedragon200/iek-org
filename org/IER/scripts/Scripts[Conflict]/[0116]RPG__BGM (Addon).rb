# RPG::BGM (Addon)
class RPG::BGM < RPG::AudioFile
  #@@volrate = 1.0
  #@@fader = nil
  def play(pos = 0)
    if @name.empty?
      Audio.bgm_stop
      @@last = RPG::BGM.new
    else
      Audio.bgm_play('Audio/BGM/' + @name, self.volume, @pitch, pos) rescue nil
      @@last = self.clone
    end
  end
  def vol_rate
    $game_settings ? $game_settings.bgm_volume_rate : 1.0
  end  
  def volume
    return super * vol_rate #* @@volrate
  end  
=begin  
  def fadein(time)
    time /= 10
    @@fader = Thread.new() { 
      time.times { |i| @@volrate = 1.0 * i.to_f / time ; replay(); sleep(0.01) }
      @@volrate = 1.0
      @@fader = nil
    }
    self
  end 
  def crossover( time, bgm )
    time /= 10
    @@fader = Thread.new() { 
      time.downto(0) { |i| @@volrate = 1.0 * i.to_f / time ; replay(); sleep(0.01) }
      bgm.fadein( time )
    }
    self
  end 
  def self.fading?()
    !@@fader.nil?()
  end  
=end  
end

