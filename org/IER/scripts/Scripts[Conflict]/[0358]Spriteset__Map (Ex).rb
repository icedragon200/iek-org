# Spriteset::Map (Ex)
# // 02/18/2012
# // 02/18/2012
class Spriteset_Map
  def create_move_cursor
    @move_cursor = Sprite::MoveCursor.new(@viewport1) 
  end
  def dispose_move_cursor
    @move_cursor.dispose
  end
  def update_move_cursor
    @move_cursor.update
  end
  def animation?
    @character_sprites.any? { |sprite| sprite.animation?() }
  end
  def sanimation?
    @character_sprites.any? { |sprite| sprite.sanimation?() }
  end  
  def effect?
    @character_sprites.any? { |sprite| sprite.effect?() }
  end
  def tweening?()
    !@viewport_tween.done?
  end  
  
  def default_viewport_rect()
    Graphics.play_rect
  end  
  attr_reader :viewport_rect
  def reset_viewports()
    @viewport_rect  = default_viewport_rect()
    @viewport_tween = Tween.new()
    tween = @viewport_tween
    x, y = @viewport_rect.to_a
    w, h = @viewport_rect.xto_a(:width,:height)
    tween.set( [x, h/2, w, 0], [x, y, w, h], :sine_out, Tween.frames_to_tt(120) )
  end 
  def restore_viewport(time=nil, easer=nil)
    tween_viewports( 
      default_viewport_rect.xto_h(*Rect::SYM_ARGS).merge(:time => time, :easer=>easer)
    )
  end  
  def tween_viewports( *args )
    case args[0]
    when Hash
      x,y,w,h,easer,time = args[0].get_values(:x,:y,:width,:height,:easer,:time)      
    when Rect
      x,y,w,h = args[0].to_a
      easer   = args[1] 
      time    = args[2] 
    else # // Array
      x,y,w,h,easer,time = args[0..5]
    end  
    x     ||= @viewport_rect.x 
    y     ||= @viewport_rect.y
    w     ||= @viewport_rect.width
    h     ||= @viewport_rect.height
    easer ||= :sine_in
    time  ||= 10
    @viewport_tween.set_and_reset( 
      @viewport_rect.to_a, [x, y, w, h], easer, Tween.frames_to_tt(time) 
    )
  end 
    
  alias :pre_tv_cr_view :create_viewports
  def create_viewports()
    pre_tv_cr_view()
    reset_viewports()
  end
end  
