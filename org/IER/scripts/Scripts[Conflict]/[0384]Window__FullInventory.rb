# Window::FullInventory
#==============================================================================#
# ♥ Window::FullInventory
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 01/02/2012
# // • Data Modified : 01/02/2012
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 01/02/2012 V1.0
#
#==============================================================================#
class Window::FullInventory < Window::Inventory 
  AddonBin_WFI = WindowAddons::AddonBin[WindowAddons::ScrollBar]
  def addon_bin
    AddonBin_WFI
  end  
  def col_max
    4
  end  
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
