# [IEI]Elements
# // 02/24/2012
# // 02/24/2012
module IEI
  class Element < RPG::BaseItem # // >_> Nothing here <_< Come get going.. 
    WEAK_RATE        = 1.50 #2.0 # >: 2x was a little too much
    NORMAL_RATE      = 1.00 #1.0 # :p Now why would you change this
    RESITANT_RATE    = 0.75 #0.5 # (: Gotta balance this out too
    INEFFECTIVE_RATE = 0.00
    REGEN_RATE       =-0.50
    def self.[](id,weak=[],resistant=[],ineffective=[],regen=[])
      f = RPG::BaseItem::Feature
      ele = new()
      ele.initialize_add()
      ele.id          = id
      ele.icon_index  = Icon.element(ele.id)
      ele.name        = Vocab.element(ele.id)
      ele.description = ''
      ele.features  = []
      ele.features += weak.collect {|i|MkFeature.element_r(i,WEAK_RATE)}
      ele.features += resistant.collect {|i|MkFeature.element_r(i,RESITANT_RATE)}
      ele.features += ineffective.collect {|i|MkFeature.element_r(i,INEFFECTIVE_RATE)}
      ele.features += regen.collect {|i|MkFeature.element_r(i,REGEN_RATE)}
      ele # // . x . You get teh newly created element
    end
  end  
  module Elements
    @elements = []
    Element = IEI::Element
    def self.elements ; @elements ; end
    def self.element(n); elements[n]; end
    def self.element_id(n) ; DB.element_id(n) ; end
    def self.eid(*a,&b) ; element_id(*a,&b) ; end
    Feature = RPG::BaseItem::Feature # // >_>... You know the drill  
    module MixIns ; end
    # //         = Element[id,[weak_ids..],[resist_id..],[ineffective_id..],[regen_id..]]
    def self.mk_objs() # // >_> There is a good reason for this
      @elements[0] = Element[ 0,[ ],[ ],[ ],[ ]]
      # // . x . Your basic element Object only has weakness and resistant features
      # // o3o For best results try adding your own custom features to it
      # // . x . I use a REMAP (pretty much resets all the object's id based on there index)
      # // That way I dont have to set the Element's id yet until the end
      @elements[eid(:fire)]  = [ 0,[eid(:water)] ,[eid(:fire)]  ,[],[]]
      @elements[eid(:water)] = [ 0,[eid(:fire)]  ,[eid(:water)] ,[],[]]
      @elements[eid(:earth)] = [ 0,[eid(:wind)]  ,[eid(:earth)] ,[],[]]
      @elements[eid(:wind)]  = [ 0,[eid(:earth)] ,[eid(:wind)]  ,[],[]]
      @elements[eid(:light)] = [ 0,[eid(:dark)]  ,[eid(:light)] ,[],[]]
      @elements[eid(:dark)]  = [ 0,[eid(:light)] ,[eid(:dark)]  ,[],[]]
      # // REMAP
      for i in 0...@elements.size
        if @elements[i]
          # // Make the actual Element Object
          @elements[i] = Element[*@elements[i]] if @elements[i].is_a?(Array)
          @elements[i].id = i
        end  
      end 
      #File.open("SkillNames.txt", "w+"){|f|
      #  $data_skills.compact.each{|s|f.puts(format("%03d-%s", s.id, s.name))}
      #}
    end # // mk_objs
  end
end   
IEI::Core.on_data_load { IEI::Elements.mk_objs }
module IEI::Elements::MixIns::Battler
  # // 
  # // 02/13/2012
  # // 02/24/2012
  # // More Tactics Ogre Insiration . x . ELEMENTS XD
  attr_accessor :element_id
  def pre_init_iei()
    super() 
    @element_id = 0 # // No Element . x .
  end  
  def init_iei()
    super()
  end 
  def post_init_iei()
    super()
  end
  def element(id=@element_id) # // . x . Yes you retrieve the actual Element object
    IEI::Elements.element(id)
  end  
  def elements() # // >_> Yes not element, ELEMENTS, >=> Dont ask
    [element(@element_id)]
  end  
  def feature_objects() # // And then we add em all together
    super + elements() # // Other + Elements :3
  end
end
class Game::RogueCharacter2
  include IEI::Elements::MixIns::Battler
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
