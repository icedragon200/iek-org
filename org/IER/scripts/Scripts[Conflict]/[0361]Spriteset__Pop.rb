# Spriteset::Pop
# // 01/31/2012
# // 01/31/2012
class Spriteset_Pop
  def initialize
    create_viewports()
    create_pops()
  end  
  def create_viewports
    @viewport1 = Viewport.new
  end  
  def create_pops()
    @pops = []
  end  
  def dispose
    dispose_pops()
    dispose_viewports()
  end
  def dispose_viewports()
    @viewport1.dispose()
  end  
  def dispose_pops()
    @pops.each { |p| p.dispose }
  end  
  def update
    update_viewports()
    update_pops()
  end  
  def update_viewports()
    @viewport1.update
  end  
  def update_pops
    @pops.select! { |p| p.done? ? p.dispose : p.update ; !p.disposed? }
  end   
  def add_pop(hnd)
    @pops << Sprite::PopText.new(@viewport1, hnd)
    self
  end  
  def remove_pop(sp)
    @pops.delete(sp)
    self
  end  
  # // 02/01/2012
  def busy?
    return @pops.any? { |p| !p.done? } if @pops.size > 0
    return false
  end
  def clear_all
    dispose_pops()
  end  
  # // 02/09/2012
  def add_speechbub(x, y, text)
    @pops << SpeechBubble.new(@viewport1, x, y, text)
    self
  end 
  # // 02/24/2012
  def busy_wait_count()
    @pops.collect{|p|p.current_wait_count}.max || 0
  end  
end  
