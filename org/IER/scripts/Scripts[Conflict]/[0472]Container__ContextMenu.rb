# Container::ContextMenu
# // 02/14/2012
# // 02/14/2012  
class Container::ContextMenu < Container::WinSel
  include Container::Addons::Background_Simple
  def cont_cursor_base_color
    Pallete[:brown3].xset(:alpha=>98)
  end
  def cont_cursor_padding_color
    Pallete[:brown1]
  end  
  attr_accessor :parent
  def initialize(x,y)
    clear_command_list()
    make_command_list()
    super(x,y,window_width,window_height)
    @parent = nil
    select(0)
    refresh()
    activate()
  end  
  def window_width
    96
  end
  def window_height
    (item_max * line_height) + (standard_padding * 2)
  end  
  def standard_padding
    2
  end  
  def line_height
    16
  end 
  def split_here?(index)
    @split_index.include?(index)
  end    
  def item_max
    @list.size
  end
  def clear_command_list
    @split_index = []
    @list = []
  end
  def make_command_list
  end
  def add_command(name, symbol, enabled = true, ext = nil)
    @list.push({:name=>name, :symbol=>symbol, :enabled=>enabled, :ext=>ext})
  end
  def command_name(index)
    @list[index][:name]
  end
  def command_enabled?(index)
    @list[index][:enabled]
  end
  def current_data
    index >= 0 ? @list[index] : nil
  end
  def current_item_enabled?
    current_data ? current_data[:enabled] : false
  end
  def current_symbol
    current_data ? current_data[:symbol] : nil
  end
  def current_ext
    current_data ? current_data[:ext] : nil
  end
  def select_symbol(symbol)
    @list.each_index {|i| select(i) if @list[i][:symbol] == symbol }
  end
  def select_ext(ext)
    @list.each_index {|i| select(i) if @list[i][:ext] == ext }
  end
  def draw_item(index)
    rect = item_rect(index)
    contents.font.set_style(:window_header)
    change_color(contents.font.color, command_enabled?(index))
    draw_text(item_rect_for_text(index), command_name(index), alignment)
    draw_horz_line(rect.vy2) if split_here?(index)
  end 
  def alignment
    return 1
  end
  def ok_enabled?
    return true
  end
  def call_ok_handler
    if handle?(current_symbol)
      call_handler(current_symbol)
    elsif handle?(:ok)
      super
    else
      activate
    end
  end 
end  
