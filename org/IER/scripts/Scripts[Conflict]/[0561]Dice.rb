# Dice
#~ # // By IceDragon && Nelderson
#~ class RPG::UsableItem::Damage
#~   def dice(x,y)
#~     puts "Dice!"
#~     x + rand((x*y) + 1)
#~   end
#~ end
#~ module DataManager
#~   class << self
#~     alias :it_load_database :load_database
#~     def load_database(*args,&block)
#~       it_load_database(*args,&block)
#~       ($data_items+$data_skills).compact.each do |obj|
#~         s = obj.damage.formula
#~         s = s.gsub(/(\-?\d+)[d](\-?\d+)/i) do
#~           a,b = $1,$2
#~           "dice(#{a},#{b})"
#~         end
#~         obj.damage.formula = s
#~       end
#~     end
#~   end  
#~ end
