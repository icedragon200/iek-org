# Mouse::Handler
# // 01/24/2012
# // 01/24/2012
if defined? Mouse
class Mouse::Handler 
  include Mouse::Constants  
  module MouseStates
    LEFT_CLICK   = 1
    RIGHT_CLICK  = 2
    MIDDLE_CLICK = 3
    LEFT_PRESS   = 4
    RIGHT_PRESS  = 5
    MIDDLE_PRESS = 6
    MOVED  = 7 # // Moved for Client?
    RMOVED = 8 # // Moved at all?
    CLICKS  = [LEFT_CLICK, RIGHT_CLICK, MIDDLE_CLICK]
    PRESSES = [LEFT_PRESS, RIGHT_PRESS, MIDDLE_PRESS]
  end
  include MouseStates
  def initialize
    @events   = []
    @states   = Array.new(9, false)
    # // 02/14/2012
    @tcount   = Hash.new # // Click Counter, used for Double Clicks
    @tcool    = Hash.new # // Click Cooldown, used for Double Clicks
    @last_pos = [-1,-1] 
    @last_real_pos = [-1,-1]
  end  
  def init
    Mouse.init()
    release_states()
    @last_pos = [-1, -1]
    @last_real_pos = [-1,-1]
  end  
  def [](n)
    @states[n]
  end  
  def set_states
    BUTTONS.each_with_index do |b,i|
      @states[CLICKS[i]]  = Mouse.click?(b)
      @states[PRESSES[i]] = Mouse.press?(b)
    end
    @states[MOVED]        = Mouse.pos != @last_pos
    @states[RMOVED]       = Mouse.rpos != @last_rpos
  end
  def release_states
    @states.map! { false }
  end 
  def click_cool
    24
  end  
  def update()
    set_states()
    @last_pos = Mouse.pos
    @last_real_pos = Mouse.rpos
    update_tcount()
    update_visible()
  end 
  def update_visible()
    @visible = in_client?()
    if @last_visible != @visible 
      @last_visible = @visible 
      InputEx.ShowCursor(@visible ? 0 : 1)
    end  
  end  
  def update_graphics()
    Mouse.update_cursor_sprite()
    Mouse.update_tooltip_sprite()
  end  
  def update_tcount()
    # // @_@ Weird stuff happens when you delete an element while iterating
    @tcount.each_pair do |k,v|
      if(self[k]) # // If Click . x. 
        @tcount[k] = v+1
        @tcool[k] = click_cool
      else  
        @tcool[k] = @tcool[k].to_i.pred.max(0)
        if(@tcool[k] <= 0)
          @tcount.delete(k) 
          @tcool.delete(k)
        end  
      end  
      #puts [k, @tcount[k], @tcool[k]].inspect
    end  
  end  
  def multi_click?(c,n=1)
    return unless self[c]
    unless @tcount.has_key?(c)
      @tcount[c] = 1
      @tcool[c] = click_cool
    end  
    @tcount[c] % n == 0
  end  
  def dleft_click?() # // Double Left Click .x .
    multi_click?(LEFT_CLICK,2) 
  end 
  def dright_click?() # // Double Right Click . x.
    multi_click?(RIGHT_CLICK,2) 
  end
  def dmiddle_click?() # // Double Middle Click . x .
    multi_click?(MIDDLE_CLICK,2) 
  end
  def left_click?()
    self[LEFT_CLICK]
  end  
  def right_click?()
    self[RIGHT_CLICK]
  end
  def middle_click?()
    self[MIDDLE_CLICK]
  end
  def left_press?()
    self[LEFT_PRESS]
  end
  def right_press?()
    self[RIGHT_PRESS]
  end
  def middle_press?()
    self[MIDDLE_PRESS]
  end
  def moved?()
    self[MOVED]
  end 
  def rmoved?()
    self[RMOVED]
  end  
  def x
    @last_pos[0]
  end
  def y
    @last_pos[1]
  end  
  def rx
    @last_real_pos[0]
  end
  def ry
    @last_real_pos[1]
  end 
  def z
    999
  end  
  def pos
    return *@last_pos
  end 
  def in_client?() 
    #pos != [-1,-1]
    x_in_client? && y_in_client?
  end
  def x_in_client?()
    Mouse.x_in_client?()
  end  
  def y_in_client?()
    Mouse.y_in_client?()
  end
  def at_pos?( x, y )
    (@last_pos[0] == x && @last_pos[1] == y)
  end  
  def in_area?( obj )
    obj.in_area?( *self.pos() )
  end  
end 
end
