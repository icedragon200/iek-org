# Window::SkillList
#==============================================================================
# ■ Window::SkillList
#------------------------------------------------------------------------------
# 　スキル画面で、使用できるスキルの一覧を表示するウィンドウです。
#==============================================================================

class Window::SkillList < Window::Selectable
  #--------------------------------------------------------------------------
  # ● オブジェクト初期化
  #--------------------------------------------------------------------------
  def initialize(x, y, width=window_width, height=window_height)
    super
    @actor = nil
    @stype_id = 0
    @data = []
    @mode = 0
  end
  def window_width()
    adjust_w4window(col_max * ((96 + 72 + 4)+spacing))
  end
  def window_height()
    adjust_h4window(item_height*3)
  end  
  #def col_max
  #  return 4
  #end  
  def spacing 
    0
  end  
  def item_width
    super #(self.width - (spacing*col_max)) / col_max
  end  
  def item_height
    24 + 4
  end
  def line_height
    28
  end  
  def battler
    self.actor
  end  
  def battler=(n)
    self.actor = n
  end 
  attr_reader :actor
  #--------------------------------------------------------------------------
  # ● アクターの設定
  #--------------------------------------------------------------------------
  def actor=(actor)
    return if @actor == actor
    @actor = actor
    refresh
    self.index = @data.index( @actor.last_skill.object ).to_i
    self.oy = 0
  end
  #--------------------------------------------------------------------------
  # ● スキルタイプ ID の設定
  #--------------------------------------------------------------------------
  def stype_id=(stype_id)
    return if @stype_id == stype_id
    @stype_id = stype_id
    refresh
    self.oy = 0
  end
  #--------------------------------------------------------------------------
  # ● 桁数の取得
  #--------------------------------------------------------------------------
  def col_max
    return 2 #1
  end
  #--------------------------------------------------------------------------
  # ● 項目数の取得
  #--------------------------------------------------------------------------
  def item_max
    (@actor && mode == 0) ? @actor.equip_skill_size : (@data ? @data.size : 1)
  end
  #--------------------------------------------------------------------------
  # ● スキルの取得
  #--------------------------------------------------------------------------
  def item
    @data && index >= 0 ? @data[index] : nil
  end
  def current_item()
    return item
  end
  #--------------------------------------------------------------------------
  # ● 選択項目の有効状態を取得
  #--------------------------------------------------------------------------
  def current_item_enabled?
    enable?(@data[index])
  end
  #--------------------------------------------------------------------------
  # ● スキルをリストに含めるかどうか
  #--------------------------------------------------------------------------
  def include?(item)
    item && (@stype_id > 0 ? item.stype_id == @stype_id : true)
  end
  #--------------------------------------------------------------------------
  # ● スキルを許可状態で表示するかどうか
  #--------------------------------------------------------------------------
  def enable?(item)
    @actor && @actor.usable?(item)
  end
  #--------------------------------------------------------------------------
  # ● スキルリストの作成
  #--------------------------------------------------------------------------
  def make_item_list
    case mode
    when 0 # // Equipped  S-type
      @data = (@actor ? @actor.skills.select {|skill| include?(skill) } : [])
    when 1 # // All S-type
      @data = (@actor ? @actor.all_skills.select {|skill| include?(skill) } : [])  
    when 2 # // All 
      @data = (@actor ? @actor.all_skills : [])
    end  
  end
  #--------------------------------------------------------------------------
  # ● 前回の選択位置を復帰
  #--------------------------------------------------------------------------
  def select_last
    select(@data.index(@actor.last_skill.object) || 0)
  end
  #--------------------------------------------------------------------------
  # ● 項目の描画
  #--------------------------------------------------------------------------
  def draw_item(index)
    skill = @data[index]
    rect = item_rect(index).contract(2)
    set = @actor._hot_keys.obj_set?(skill)
    @artist.draw_skill_ex(skill,rect,(set && !skill.nil?()) ? 4 : 0,enable?(skill))
  end 
  # // Draw Skill Cost Goes Here
  #--------------------------------------------------------------------------
  # ● ヘルプテキスト更新
  #--------------------------------------------------------------------------
  def update_help
    @help_window.set_item(item)
  end
  #--------------------------------------------------------------------------
  # ● リフレッシュ
  #--------------------------------------------------------------------------
  def refresh
    make_item_list
    create_contents
    draw_all_items
    call_update_help
  end
  # // 02/23/2012
  attr_reader :mode
  def mode=(mode)
    @mode = mode
    refresh()
  end  
  def set_mode(n)
    @mode = n
  end  
end
