# Mixin::DrawTextEx
# // 01/31/2012
# // 01/31/2012
module Mixin::DrawTextEx  
  def windowskin
    Cache.system("Window")
  end  
  def line_height
    24
  end
  #--------------------------------------------------------------------------
  # ● テキストサイズの取得
  #--------------------------------------------------------------------------
  def text_size(str)
    contents.text_size(str)
  end
  #--------------------------------------------------------------------------
  # ● 文字色取得
  #     n : 文字色番号（0..31）
  #--------------------------------------------------------------------------
  def text_color(n)
    windowskin.get_pixel(64 + (n % 8) * 8, 96 + (n / 8) * 8)
  end
  #--------------------------------------------------------------------------
  # ● 各種文字色の取得
  #--------------------------------------------------------------------------
  def normal_color;      text_color(0);   end;    # 通常
  def system_color;      text_color(16);  end;    # システム
  def crisis_color;      text_color(17);  end;    # ピンチ
  def knockout_color;    text_color(18);  end;    # 戦闘不能
  def gauge_back_color;  text_color(19);  end;    # ゲージ背景
  def hp_gauge_color1;   text_color(20);  end;    # HP ゲージ 1
  def hp_gauge_color2;   text_color(21);  end;    # HP ゲージ 2
  def mp_gauge_color1;   text_color(22);  end;    # MP ゲージ 1
  def mp_gauge_color2;   text_color(23);  end;    # MP ゲージ 2
  def mp_cost_color;     text_color(23);  end;    # 消費 TP
  def power_up_color;    text_color(24);  end;    # 装備 パワーアップ
  def power_down_color;  text_color(25);  end;    # 装備 パワーダウン
  def tp_gauge_color1;   text_color(28);  end;    # TP ゲージ 1
  def tp_gauge_color2;   text_color(29);  end;    # TP ゲージ 2
  def tp_cost_color;     text_color(29);  end;    # 消費 TP
  #--------------------------------------------------------------------------
  # ● 保留項目の背景色を取得
  #--------------------------------------------------------------------------
  def pending_color
    windowskin.get_pixel(80, 80)
  end
  #--------------------------------------------------------------------------
  # ● 半透明描画用のアルファ値を取得
  #--------------------------------------------------------------------------
  def translucent_alpha
    return 160
  end
  #--------------------------------------------------------------------------
  # ● テキスト描画色の変更
  #     enabled : 有効フラグ。false のとき半透明で描画
  #--------------------------------------------------------------------------
  def change_color(color, enabled = true)
    contents.font.color.set(color)
    contents.font.color.alpha = translucent_alpha unless enabled
  end
  #--------------------------------------------------------------------------
  # ● テキストの描画
  #     args : Bitmap#draw_text と同じ
  #--------------------------------------------------------------------------
  def draw_text(*args)
    contents.draw_text(*args)
  end
  #--------------------------------------------------------------------------
  # ● 制御文字つきテキストの描画
  #--------------------------------------------------------------------------
  def draw_text_ex(x, y, text)
    reset_font_settings
    text = convert_escape_characters(text)
    pos = {:x => x, :y => y, :new_x => x, :height => calc_line_height(text)}
    process_character(text.slice!(0, 1), text, pos) until text.empty?
  end
  #--------------------------------------------------------------------------
  # ● フォント設定のリセット
  #--------------------------------------------------------------------------
  def reset_font_settings
    change_color(normal_color)
    contents.font.size = Font.default_size
    contents.font.bold = false
    contents.font.italic = false
  end
  #--------------------------------------------------------------------------
  # ● 制御文字の事前変換
  #    実際の描画を始める前に、原則として文字列に変わるものだけを置き換える。
  #    文字「\」はエスケープ文字（\e）に変換。
  #--------------------------------------------------------------------------
  def convert_escape_characters(text)
    result = text.to_s.clone
    result.gsub!(/\\/)            { "\e" }
    result.gsub!(/\e\e/)          { "\\" }
    result.gsub!(/\eV\[(\d+)\]/i) { $game_variables[$1.to_i] }
    result.gsub!(/\eV\[(\d+)\]/i) { $game_variables[$1.to_i] }
    result.gsub!(/\eN\[(\d+)\]/i) { actor_name($1.to_i) }
    result.gsub!(/\eP\[(\d+)\]/i) { party_member_name($1.to_i) }
    result.gsub!(/\eG/i)          { Vocab::currency_unit }
    result
  end
  #--------------------------------------------------------------------------
  # ● アクター n 番の名前を取得
  #--------------------------------------------------------------------------
  def actor_name(n)
    actor = n >= 1 ? $game_actors[n] : nil
    actor ? actor.name : ""
  end
  #--------------------------------------------------------------------------
  # ● パーティメンバー n 番の名前を取得
  #--------------------------------------------------------------------------
  def party_member_name(n)
    actor = n >= 1 ? $game_party.members[n - 1] : nil
    actor ? actor.name : ""
  end
  #--------------------------------------------------------------------------
  # ● 文字の処理
  #     c    : 文字
  #     text : 描画処理中の文字列バッファ（必要なら破壊的に変更）
  #     pos  : 描画位置 {:x, :y, :new_x, :height}
  #--------------------------------------------------------------------------
  def process_character(c, text, pos)
    case c
    when "\n"   # 改行
      process_new_line(text, pos)
    when "\f"   # 改ページ
      process_new_page(text, pos)
    when "\e"   # 制御文字
      process_escape_character(obtain_escape_code(text), text, pos)
    else        # 普通の文字
      process_normal_character(c, pos)
    end
  end
  #--------------------------------------------------------------------------
  # ● 通常文字の処理
  #--------------------------------------------------------------------------
  def process_normal_character(c, pos)
    text_width = text_size(c).width
    draw_text(pos[:x], pos[:y], text_width * 2, pos[:height], c)
    pos[:x] += text_width
  end
  #--------------------------------------------------------------------------
  # ● 改行文字の処理
  #--------------------------------------------------------------------------
  def process_new_line(text, pos)
    pos[:x] = pos[:new_x]
    pos[:y] += pos[:height]
    pos[:height] = calc_line_height(text)
  end
  #--------------------------------------------------------------------------
  # ● 改ページ文字の処理
  #--------------------------------------------------------------------------
  def process_new_page(text, pos)
  end
  #--------------------------------------------------------------------------
  # ● 制御文字の本体を破壊的に取得
  #--------------------------------------------------------------------------
  def obtain_escape_code(text)
    text.slice!(/^[\$\.\|\^!><\{\}\\]|^[A-Z]+/i)
  end
  #--------------------------------------------------------------------------
  # ● 制御文字の引数を破壊的に取得
  #--------------------------------------------------------------------------
  def obtain_escape_param(text)
    text.slice!(/^\[\d+\]/)[/\d+/].to_i rescue 0
  end
  #--------------------------------------------------------------------------
  # ● 制御文字の処理
  #     code : 制御文字の本体部分（「\C[1]」なら「C」）
  #--------------------------------------------------------------------------
  def process_escape_character(code, text, pos)
    case code.upcase
    when 'K'
      change_color(kcolor(obtain_escape_param(text)))
    when 'C'
      change_color(text_color(obtain_escape_param(text)))
    when 'I'
      process_draw_icon(obtain_escape_param(text), pos)
    when '{'
      make_font_bigger
    when '}'
      make_font_smaller
    end
  end
  #--------------------------------------------------------------------------
  # ● 制御文字によるアイコン描画の処理
  #--------------------------------------------------------------------------
  def process_draw_icon(icon_index, pos)
    draw_icon(icon_index, pos[:x], pos[:y])
    pos[:x] += 24
  end
  #--------------------------------------------------------------------------
  # ● フォントを大きくする
  #--------------------------------------------------------------------------
  def make_font_bigger
    contents.font.size += 8 if contents.font.size <= 64
  end
  #--------------------------------------------------------------------------
  # ● フォントを小さくする
  #--------------------------------------------------------------------------
  def make_font_smaller
    contents.font.size -= 8 if contents.font.size >= 16
  end
  #--------------------------------------------------------------------------
  # ● 行の高さを計算
  #     restore_font_size : 計算後にフォントサイズを元に戻す
  #--------------------------------------------------------------------------
  def calc_line_height(text, restore_font_size = true)
    result = [line_height, contents.font.size].max
    last_font_size = contents.font.size
    text.slice(/^.*$/).scan(/\e[\{\}]/).each do |esc|
      make_font_bigger  if esc == "\e{"
      make_font_smaller if esc == "\e}"
      result = [result, contents.font.size].max
    end
    contents.font.size = last_font_size if restore_font_size
    result
  end
  def strip_escape_codes(text)
    t = text.gsub(/\\\w\[\d+\]/i, "")
    t
  end  
end
