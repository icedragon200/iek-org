# Sprite::MouseTooltip
# // 02/03/2012
# // 02/03/2012
class Sprite::Tooltip < Sprite ; end
if defined? Mouse
class << Mouse
  alias :pre_tooltip_sprite_init :init
  def init(*args)
    pre_tooltip_sprite_init(*args)
    dispose_tooltip_sprite()
    create_tooltip_sprite()
  end  
  def create_tooltip_sprite()
    @tooltip_sprite = Sprite::MouseTooltip.new(nil,"")
  end  
  def dispose_tooltip_sprite
    @tooltip_sprite.dispose unless @tooltip_sprite.disposed? if @tooltip_sprite
    @tooltip_sprite = nil
  end
  def update_tooltip_sprite
    @tooltip_sprite.update if @tooltip_sprite
  end  
  def tooltip_sprite()
    @tooltip_sprite
  end  
end 
class Sprite::MouseTooltip < Sprite::Tooltip
  POLE_POS = Array.new(10)
  POLE_POS[7] = [-1,-1] ; POLE_POS[8] = [ 0,-1] ; POLE_POS[9] = [+1,-1]
  POLE_POS[4] = [-1, 0] ; POLE_POS[5] = [ 0, 0] ; POLE_POS[6] = [+1, 0]
  POLE_POS[1] = [-1,+1] ; POLE_POS[2] = [ 0,+1] ; POLE_POS[3] = [+1,+1]
  def initialize(*args,&block)
    super(*args,&block)
    @pp_tweener = Tween.new([0,0],[0,0],:linear,1.0)
  end  
  def xpole
    MouseEx.rx <=> Graphics.width / 2
  end  
  def ypole
    MouseEx.ry <=> Graphics.height / 2
  end  
  def pole_pos(xpole,ypole)
    case [xpole,ypole]
    when POLE_POS[3], POLE_POS[6], POLE_POS[9] # // X Right
      return -(width+4), 0
    when POLE_POS[2], POLE_POS[5], POLE_POS[8] # // X Center 
      return 22, 0
    when POLE_POS[1], POLE_POS[4], POLE_POS[7] # // X Left 
      return 22, 0
    end
  end  
  def update
    super()
    pp = pole_pos(xpole,ypole)
    if @pp_tweener.end_values != pp
      @pp_tweener.set_and_reset(@pp_tweener.values, pp, :back_out, Tween.f2tt(10))
    end  
    @pp_tweener.update
    self.x = MouseEx.rx + @pp_tweener.value(0)
    self.y = MouseEx.ry + @pp_tweener.value(1)
    self.z = MouseEx.z + 1
    #self.x = self.x.clamp(0,Graphics.width-width)
  end  
end  
end

    
