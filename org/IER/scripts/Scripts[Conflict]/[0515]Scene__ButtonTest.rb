# Scene::ButtonTest
# // 02/24/2012
# // 02/24/2012
class Scene::ButtonTest < Scene::Base
  def start()
    super()
    create_background()
    @button_window = Container::ButtonTest.new(0,0)
    @button_window.salign(1,1)
    @button_window.set_handler(:button_size_change,@button_window.method(:center_xy))
    @button_window.set_handler(:wb_close,method(:on_button_close))
    add_window(@button_window)
    @button_window.start_open()
  end  
  def post_start()
    super()
    @button_window.activate()
  end  
  def terminate
    dispose_background()
    super()
  end  
  def on_button_close()
    @button_window.start_close
    wait_for_window(@button_window)
    return_scene
  end  
  def update_basic()
    super()
    Keyboard.update()
  end  
  def update()
    update_background()
    super()
    on_button_close() if Keyboard.trigger?(:ESC)
  end  
end  
