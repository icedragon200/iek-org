# Game::HotKey
#==============================================================================#
# ♥ Game::HotKey
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/24/2011
# // • Data Modified : 12/24/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/24/2011 V1.0 
#
#==============================================================================# 
class Game::HotKey
  attr_reader :key
  attr_reader :code
  attr_reader :value
  NIL_CODE            = 0
  ATTACK_CODE         = 1
  GUARD_CODE          = 2
  SKIP_CODE           = 3
  SKILL_CODE          = 4
  ITEM_CODE           = 6
  MENU_OPEN_CODE      = 41
  CURSOR_MODE_CODE    = 51
  MINIMAP_TOGGLE_CODE = 52
  # // Menu Item
  MENU_OBJ               = RPG::BaseItem.new()
  MENU_OBJ.initialize_add()
  MENU_OBJ.name          = "Menu"
  MENU_OBJ.description   = "Button to enter menu"
  MENU_OBJ.icon_index    = 232
  # // Cursor Item
  CURSOR_OBJ             = RPG::BaseItem.new()
  CURSOR_OBJ.initialize_add()
  CURSOR_OBJ.name        = "Cursor Mode"
  CURSOR_OBJ.description = "Button to enter cursor mode"
  CURSOR_OBJ.icon_index  = 273
  # // Map Item
  MAP_OBJ                = RPG::BaseItem.new()
  MAP_OBJ.initialize_add()
  MAP_OBJ.name           = "Minimap Toggle"
  MAP_OBJ.description    = "Button to toggle minimap"
  MAP_OBJ.icon_index     = 231
  def initialize( key )
    @key = key
    @params = []
    set_key_as( 0, 0 )
  end  
  def description
    return @key
  end  
  def set_key_as( code=0, value=0 ) 
    @code  = code
    @value = value
  end  
  def set_obj( obj )
    case obj
    when RPG::Item
      set_key_as( ITEM_CODE, obj.id )
    when RPG::Skill
      set_key_as( SKILL_CODE, obj.id )
    else 
      set_key_as( NIL_CODE )
    end
    set_params( [] )
    return self
  end  
  def set_params( params ) 
    @params = params
    return self
  end  
  def add_param( ghkp ) # // Game::HotKey::Parameter
    @params << ghkp
    return self
  end  
  def new_param()
    p = Game::HotKey::Parameter.new()
    add_param( p )
    return p
  end  
  def object
    case @code
    when NIL_CODE       
      return nil
    when ATTACK_CODE
      return $data_skills[Database::DEF_ATTACK_SKILL_ID]
    when GUARD_CODE
      return $data_skills[Database::DEF_GUARD_SKILL_ID]    
    when SKIP_CODE
      return $data_skills[Database::DEF_SKIP_SKILL_ID]  
    when SKILL_CODE  
      return $data_skills[@value]
    when ITEM_CODE
      return $data_items[@value]
    when MENU_OPEN_CODE
      return MENU_OBJ
    when CURSOR_MODE_CODE
      return CURSOR_OBJ
    when MINIMAP_TOGGLE_CODE
      return MAP_OBJ
    else
      return nil
    end
  end  
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
