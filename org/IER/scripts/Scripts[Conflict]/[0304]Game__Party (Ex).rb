# Game::Party (Ex)
# // 02/29/2012
# // 02/29/2012
class Game::Party
  # // Inventory Wrapper . x .
  def inventory
    if @inventory.nil?
      @inventory = Game::Inventory.new(self)
      @inventory.no_limit!()
    end
    @inventory
  end  
  def items
    inventory.items
  end
  def weapons
    inventory.weapons
  end
  def armors
    inventory.armors
  end
  def item_container(*args,&block)
    inventory.item_container(*args,&block)
  end 
  def item_number(*args,&block)
    inventory.item_number(*args,&block)
  end  
  def max_item_number(*args,&block)
    inventory.max_item_number(*args,&block)
  end 
  def max_item_number?(*args,&block)
    inventory.max_item_number?(*args,&block)
  end  
  def gain_item(item, amount, include_equip = false)
    inventory.gain_item(item, amount) do |new_number|
      if include_equip && new_number < 0
        discard_members_equip(item, -new_number)
      end
      $game_map.need_refresh = true
    end  
  end
  # // Debug!
  def gain_all_this(items)
    items.compact.each{|i|gain_item(i,max_item_number(i)) if i.id > 0}
  end  
  def gain_all_items()
    gain_all_this($data_items)
  end
  def gain_all_weapons()
    gain_all_this($data_weapons)
  end
  def gain_all_armors()
    gain_all_this($data_armors)
  end  
  def gain_all_equips()
    gain_all_weapons()
    gain_all_armors()
  end  
  def gain_all_arts
    (1...40).each{|aid|$game_party.gain_art(aid,1)}
  end
  
  def setup_debug_party
    gain_all_items()
    gain_all_equips()
    gain_all_arts()
    members.each { |m| m.optimize_equipments() }
    artsys = IEI::ArtsSystem
    members[0].equip_arts( *(artsys.find_arts4groups(:element,:fire,:lv1) +
      artsys.find_arts4groups(:element,:light,:lv1)) ) 
    members[1].equip_arts( *(artsys.find_arts4groups(:element,:wind,:lv1) +
      artsys.find_arts4groups(:element,:dark ,:lv1)) ) 
    members[2].equip_arts( *(artsys.find_arts4groups(:element,:water,:lv1) +
      artsys.find_arts4groups(:element,:earth,:lv1)) ) 
    members.each{|m|m.equip_available_skills()}
  end  
end  
