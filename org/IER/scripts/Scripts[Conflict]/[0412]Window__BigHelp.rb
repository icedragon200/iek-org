# Window::BigHelp
# // 03/01/2012
# // 03/01/2012
class Window::BigHelp < Window::Help2
  def initialize(x,y,w,h)
    w ||= window_width
    h ||= window_height
    super(x,y,w,h)
  end
  alias :refresh_h2 :refresh
  def refresh()
    item    = @item
    exitem  = Ex_Database.ex_equip_item?(item)
    dx, dy  = 0, 0
    dw = dh = contents.height
    rect  = Rect.new(dx,dy,dw,dh)
    rect2 = rect.clone
    rect2.squeeze!(rect2.width,false,1);rect2.width=contents.width-rect2.x
    rect2.height *= 0.4;rect2.vy2 = contents.height
    rect3 = rect2.contract(4,1)
    rect4 = rect3.contract(24,1)
    rect5 = Rect.new(0,dy+14,dh,24) # // Just the basis
    if(exitem)
      rect5.width = DrawExt.adjust_size4bar3(rect5.width,10,1,1)
      rect5.x = contents.width - rect5.width
      rect6 = rect5.offset_vert()
      rect6.height = 6
      rect6.y = contents.height - (6 * rect6.height) # // 6 Elements * 6px
      rect7 = Rect.new(rect2.x,rect2.y-8,56,8)
      rect2.width -= rect6.width 
    end  
    art = @artist
    drawing_sandbox() do
      contents.clear()
      art.draw_box( *rect.to_a )
      art.draw_box( *rect2.to_a )
      if(item)
        art.draw_icon_ex_stretch(item.icon_index,rect,true,item.iconset_name)
        art.draw_item_name(item,rect3.x,dy,rect3.width,16)
        art.draw_horz_line(rect5.y,rect3.x,rect3.width)
        art.draw_item_description(item,rect3.x,rect3.y,rect3.width,14)
        art.draw_item_parameters(item,rect4.x,dy+16,4) 
        if(exitem)
          art.draw_item_exp(item,*rect5.to_a) 
          art.draw_item_element_res(item,*rect6.to_a)
          art.draw_item_durability(item,rect7)
          drawing_sandbox() do
            contents.font.set_style(:simple_black)
            art.draw_text(rect5.xpull(16,2).xset(:height=>16),"Experience",1)
            art.draw_text(rect6.xpull(16,2).xset(:height=>16),"Element Residue",1)
          end  
        end  
      end  
    end # // Sandbox 
    start_fadein_contents()
  end    
end  
