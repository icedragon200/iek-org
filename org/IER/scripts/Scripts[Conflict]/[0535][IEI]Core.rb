# [IEI]Core
# // 02/22/2012
# // 02/22/2012
# // Icy Engine Iliks
($imported||={})["IEI::Core"] = 0x10000
module IEI
  module Core
    @data_load_stack = []
    def self.on_data_load(&block)
      @data_load_stack << block
    end  
    def self.exc_load_stack
      @data_load_stack.each { |blck| blck.call }
    end  
    def self.do_obj_cache(obj)
      obj.note_eval if(Devi.enabled?("NoteEval"))
    end  
    def self.mk_notefolder_tags(str)
      return Regexp.new("<%1$s>(.*)<\/%1$s>" % str, "mi")
    end  
    def self.get_note_folders(tag,str)
      str.scan(tag)#.collect { |str| str.split('') }
    end  
  end  
  module Sprite
  end
  module Window
  end
  module Scene
  end
  class Tileset
    def initialize(columns,rows,cell_width,cell_height)
      @grid = IEI::Grid.new(columns,rows,cell_width,cell_height)
      @bitmap = Bitmap.new(columns*cell_width,rows*cell_height)
    end  
    attr_reader :bitmap
    def cell_r(*args,&block)
      @grid.cell_r(*args,&block)
    end  
    def columns
      @grid.columns
    end  
    def rows
      @grid.rows
    end  
    def width
      @bitmap.width
    end  
    def height
      @bitmap.height
    end  
    def cell_width
      @grid.cell_width
    end  
    def cell_height
      @grid.cell_height
    end
    def disposed?()
      @bitmap.nil?() || @bitmap.disposed?()
    end  
  end  
end  
class RPG::BaseItem
  # // 04/28/2012
  def get_note_folders(tag)
    IEI::Core.get_note_folders(tag,@note)
  end  
=begin  
  def get_note_folders(open_tag,close_tag)
    lines  = note.split(/[\r\n]+/i) 
    i,line,result,arra = 0, nil,[],[]
    while(i < lines.size)
      line = lines[i]
      if(line =~ open_tag)
        until(line =~ close_tag)
          i += 1
          line = lines[i]
          result << line
          raise "End of note reached!" if(i > lines.size)
        end  
        arra << result; result = []
      end  
      i += 1
    end
    arra
  end
=end  
end  
class << DataManager
  # // Because Element makes calls to the Vocab module
  # // .x. The main database has to be loaded BEFORE the elements are created
  alias :core_post_db_load :post_db_load
  def post_db_load
    core_post_db_load
    IEI::Core.exc_load_stack
  end  
end 
IEI::Core.on_data_load do
  [$data_actors,$data_items,$data_skills,$data_weapons,$data_armors,
   $data_states,$data_enemies,$data_classes].each do |dat|
    dat.compact.each { |o| IEI::Core.do_obj_cache(o) }
  end  
end
# // . x . REI Specific stuff
class Game::RogueCharacterBase
  #alias :iei_rcb_initialize :initialize
  #def initialize(*args,&block)
  #  pre_init_iei()  # // . x . Like, make arrays and stuff
  #  iei_rcb_initialize(*args,&block)
  #  init_iei()      # // @.@ Dirty Initializes and stuff
  #  post_init_iei() # // =w= Cleaning up
  #end  
  alias :iei_rcb_pre_init_members :pre_init_members
  def pre_init_members() 
    iei_rcb_pre_init_members()
    pre_init_iei()  # // . x . Like, make arrays and stuff
  end  
  alias :iei_rcb_init_members :init_members
  def init_members()
    iei_rcb_init_members()
    init_iei()      # // @.@ Dirty Initializes and stuff
  end  
  alias :iei_rcb_post_init_members :post_init_members
  def post_init_members()
    iei_rcb_post_init_members()
    post_init_iei() # // =w= Cleaning up
  end  
  def pre_init_iei()
    # // Nothing here but use Whitespace
  end  
  def init_iei()
    # // Nothing here but use Whitespace
  end  
  def post_init_iei()
    # // Nothing here but use Whitespace
  end  
end  
class Game::Unit
  alias :iei_gmp_initialize :initialize
  def initialize()
    pre_init_iei()
    iei_gmp_initialize()
    init_iei()
    post_init_iei()
  end  
  def pre_init_iei()
    # // Nothing here but use Whitespace
  end  
  def init_iei()
    # // Nothing here but use Whitespace
  end  
  def post_init_iei()
    # // Nothing here but use Whitespace
  end
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
