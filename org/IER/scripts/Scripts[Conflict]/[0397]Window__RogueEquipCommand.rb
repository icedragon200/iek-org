# Window::RogueEquipCommand
# // 03/05/2012
# // 03/05/2012
class Window::RogueEquipCommand < Window::RogueObjCommand
  class AddonBin_WREC < AddonBin_WRC
    def header_text
      return "#{Vocab.equip} Options"
    end
  end  
  def addon_bin
    AddonBin_WREC
  end
  def make_command_list()
    add_command(Vocab::equip2,   :equip)
    add_command("Unequip"    ,   :unequip)
    add_command(Vocab::optimize, :optimize)
    add_command(Vocab::clear,    :clear)
  end  
  def col_max 
    4
  end
  def item_width
    org_item_width
  end  
  def window_width
    Graphics.width
  end  
end  
