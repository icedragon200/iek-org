# Artist
# // 03/05/2012
# // 03/05/2012
# // Soooo what the hell is this for?
# // Why the hell doesn't the window draw_* functions work anymore?
# // :( Well due to some method flooding, and some serious lag
# // I migrated every single draw function to this class
# // >: Now you have to call artist.your_function 
# // The result?
# // Less lag, more coding >:, i'll dig the code over the lag
# // >: Window::Base had 370 instance methods >: scary...
class Artist
  # // Parent, commonly a window . x .
  attr_accessor :parent
  def initialize(parent)
    @parent = parent
  end
  # // . x. Canvas, the artist needs his canvas
  def canvas
    case(parent)
    when Bitmap ; parent
    when Window ; parent.contents    
    else        ; parent.bitmap
    end  
  end  
  # // . x . Just in case something needs all this
  def method_missing(sym,*args) 
    parent.send(sym,*args)
  end  
end  
