# Window::RogueEquip
#==============================================================================#
# ♥ Window::RogueEquip
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/30/2011
# // • Data Modified : 12/30/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/30/2011 V1.0
#
#==============================================================================#
class Window::RogueEquip < Window::Equip
  class AddonBin_WRE < WindowAddons::AddonBin[WindowAddons::Header]
    def header_text
      return "Equipment"
    end
    def header_cube
      (c=super).xset(self.x+(self.width-c.width)/2,self.open_y-(14))
    end 
  end  
  def addon_bin
    AddonBin_WRE
  end   
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
