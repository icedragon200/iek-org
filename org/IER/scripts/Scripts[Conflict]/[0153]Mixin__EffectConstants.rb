# Mixin::EffectConstants
# // 02/24/2012
module Mixin::EffectConstants
  EFFECT_RECOVER_HP     = 11              
  EFFECT_RECOVER_MP     = 12              
  EFFECT_GAIN_TP        = 13              
  EFFECT_ADD_STATE      = 21              
  EFFECT_REMOVE_STATE   = 22              
  EFFECT_ADD_BUFF       = 31              
  EFFECT_ADD_DEBUFF     = 32              
  EFFECT_REMOVE_BUFF    = 33              
  EFFECT_REMOVE_DEBUFF  = 34              
  EFFECT_SPECIAL        = 41              
  EFFECT_GROW           = 42              
  EFFECT_LEARN_SKILL    = 43              
  EFFECT_COMMON_EVENT   = 44              
  # // 
  SPECIAL_EFFECT_ESCAPE = 0               
end 
