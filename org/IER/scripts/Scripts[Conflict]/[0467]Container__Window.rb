# Container::Window
# // 02/06/2012
# // 02/06/2012  
class Container::Window < Container::Base
  include Container::Addons::OwnViewport
  include Container::Addons::Contents
  include Container::Addons::StandardDrawing
  def initialize(x,y=nil,width=nil,height=nil)
    super(x,y,width,height)
  end   
end  
