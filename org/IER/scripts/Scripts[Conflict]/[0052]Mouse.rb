# Mouse
# // 01/24/2012
# // 01/24/2012
# // Version : 0.3
if true == true
module Mouse  
  module Constants
    BUTTON_LEFT  = 0x01
    BUTTON_RIGHT = 0x02
    BUTTON_MIDDLE= 0x04
    WHEEL_UP     = nil # // Unused
    WHEEL_DOWN   = nil # // Unused
    BUTTONS = [BUTTON_LEFT, BUTTON_RIGHT, BUTTON_MIDDLE]
  end 
  include Constants
  GPS  = Win32API.new( "kernel32", "GetPrivateProfileStringA", "pppplp", "l")
  @@client = nil
  def self.init
    set_client()
    r = Graphics.rect
    set_pos( r.cx, r.cy )
  end  
  def self.set_client()
    game_name = "\0" * 256
    GPS.call( 'Game',' Title', '', game_name, 255, ".\\Game.ini" )
    game_name.delete!("\0")
    @@client = InputEx.FindWindowA('RGSS Player', game_name)
  end
  def self.hwnd()
    @@client
  end
  def self.global_pos()
    gpos = [0, 0].pack('l2')
    return (InputEx.GetCursorPos(gpos) != 0 ? gpos.unpack('l2') : nil)
  end
  def self.in_client?() 
    self.pos != [-1,-1]
  end
  def self.x_in_client?()
    x = self.rpos[0]
    return false if x < 0
    return false if x > client_rect.width
    return true
  end  
  def self.y_in_client?()
    y = self.rpos[1]
    return false if y < 0
    return false if y > client_rect.height
    return true
  end
  def self.rpos() # // Real Client Position
    self.screen_to_client(*self.global_pos) || [-1,-1]
  end  
  def self.client_rect
    Rect.new(0, 0, *self.client_size)
  end  
  def self.pos()
    x, y = *rpos()
    width, height = *self.client_size
    if (x and y)
      return x, y if x.between?( 0, width ) && y.between?( 0, height )
    end
    return -1, -1
  end
  def self.set_pos( x, y )
    a = client_to_screen(x, y)
    InputEx.SetCursorPos(*a) if a
  end  
  def self.client_to_screen( x, y )
    return nil unless (x and y)
    pos = [x, y].pack('l2')
    return (InputEx.ClientToScreen(hwnd, pos) != 0 ? pos.unpack('l2') : nil)
  end  
  def self.screen_to_client( x, y )
    return nil unless (x and y)
    pos = [x, y].pack('l2')
    return (InputEx.ScreenToClient(hwnd, pos) != 0 ? pos.unpack('l2') : nil)
  end
  def self.client_size()
    rect = [0, 0, 0, 0].pack('l4') ; InputEx.GetClientRect( hwnd, rect )
    return rect.unpack('l4')[2..3]
  end
  def self.click?( n )
    return InputEx.GetAsyncKeyState(n) & 0x01 == 0x01
  end  
  def self.press?( n )
    return InputEx.GetAsyncKeyState(n).abs & 0x8000 == 0x8000
  end  
  def self.left_click?()
    click?(BUTTON_LEFT)
  end
  def self.right_click?()
    click?(BUTTON_RIGHT)
  end
  def self.middle_click?()
    click?(BUTTON_MIDDLE)
  end
  def self.left_press?()
    press?(BUTTON_LEFT)
  end
  def self.right_press?()
    press?(BUTTON_RIGHT)
  end
  def self.middle_press?()
    press?(BUTTON_MIDDLE)
  end
end 
end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
