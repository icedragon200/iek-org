# WindowAddons (Sprites)
class Sprite::ScrollBar < Sprite
end  
class Sprite::Scroller < Sprite
  attr_accessor :method_value
  attr_accessor :method_max
  def initialize( viewport, parent )
    super( viewport )
    @parent       = parent
    self.method_value = parent.scroll_bar_value_method()
    self.method_max   = parent.scroll_bar_max_method()
    @last_value   = -1
    @last_max     = self.method_max.call
    @bar_sprite   = Sprite::ScrollBar.new( self.viewport )
    @bar_tween    = Tween.new( [0,0], [0,0] )
    self.type     = parent.scroll_bar_type()
  end  
  def viewport=( viewport )
    super( viewport )
    @bar_sprite.viewport = self.viewport
  end  
  def dispose()
    dispose_bitmap()
    @bar_sprite.dispose_all()
    super()
  end  
  attr_reader :type
  def type=( new_type )
    if @type != new_type
      @type = new_type
      refresh()
    end  
  end 
  def refresh()
    redraw_base_bar()
    redraw_scroll_bar()
    update_position()
    update_visible()
  end 
  def redraw_base_bar()
    cube = @parent.scroll_bar_cube
    dispose_bitmap()
    case @type
    when :vert
      self.bitmap = Bitmap.new(cube.width,cube.length)
    when :horz
      self.bitmap = Bitmap.new(cube.length,cube.width)
    end  
    @parent.scroll_base_draw(self.bitmap)    
  end  
  def redraw_scroll_bar()
    @bar_sprite.bitmap.dispose() if @bar_sprite.bitmap
    @last_max = [@method_max.call(), 1].max
    xpad, ypad = 0, 0
    case @type
    when :vert
      bw = self.width
      bh = self.height / @last_max.to_f
      xpad = 1
    when :horz
      bw = self.width / @last_max.to_f
      bh = self.height
      ypad = 1
    else
      raise "Unknown Type: #{@type}"
    end 
    bw, bh = bw.round(0).to_i, bh.round(0).to_i
    @bar_sprite.bitmap = Bitmap.new( bw, bh )
    @parent.scroll_bar_draw(@bar_sprite.bitmap,@type,[xpad,ypad])
  end  
  def update_position()
    self.x,self.y,self.z = @parent.scroll_bar_cube.xto_a(:x,:y,:z)
    @bar_sprite.x, @bar_sprite.y, @bar_sprite.z = self.x, self.y, self.z + 1
  end 
  def update_visible()
    @bar_sprite.visible = self.visible = @parent.scroll_bar_visible?
  end  
  def update()
    super()
    update_visible()
    return unless self.visible
    if @last_max != [@method_max.call(), 1].max
      redraw_scroll_bar()
    end  
    if n = @method_value.call and @last_value != n
      @last_value = n
      case @type
      when :vert
        tx = 0
        ty = -([@last_value, 0].max * (self.height / @last_max.to_f))
      when :horz # Unbounded Horizontal, Top Horizontal, Bottom Horizontal
        tx = -([@last_value, 0].max * (self.width / @last_max.to_f))
        ty = 0
      end
      @bar_tween.set_and_reset( 
        [@bar_sprite.ox,@bar_sprite.oy],[tx,ty],:expo_out,Tween.f2tt(10) 
      )
      #@bar_tween.scale_values( 100.0 )
    end  
    @bar_tween.update()
    @bar_sprite.ox, @bar_sprite.oy = *@bar_tween.values
  end  
end
class Sprite::WindowHeader < Sprite
  def initialize( viewport=nil, parent )
    super( viewport )
    @parent = parent
    update_position()
  end
  def dispose()
    dispose_bitmap()
    super()
  end  
  def refresh()
    dispose_bitmap()
    cube = @parent.header_cube
    sett = @parent.header_text_settings
    self.bitmap = Bitmap.new(cube.width,cube.height)
    self.bitmap.gui_draw_header_base(:base_bmp=>@parent.header_bitmap)
    self.bitmap.font = sett["font"]
    sw = ([0, 72, 36])[sett["align"]]
    self.bitmap.draw_text( 
      36, 1, 
      self.bitmap.width-sw, self.bitmap.height, 
      @parent.header_text, sett["align"]
    )
    update_position()
    update_visible()
  end  
  def update_position()
    cube = @parent.header_cube
    self.x, self.y, self.z = cube.x, cube.y, cube.z
  end
  def update_visible()
    self.visible = @parent.header_visible?()
  end  
end
# // 02/04/2012
class WinButton < GUIExt::Button_Base
  def initialize(viewport,bindex=0)
    aset(0,0,0)
    super(viewport,0,0,bindex)
    @sprite.bitmap        = CacheExt.bitmap("win_buttons")
    r = CacheExt.win_button_rect
    @sprite.set(bindex,*r.xto_a(:width,:height))
    self.width = r.width
    self.height = r.height
    @over_color = Pallete[:white].xset(:alpha=>0)
    @not_over_color = Pallete[:white].xset(:alpha=>0)
  end 
  attr_reader :org_button_index 
  def org_button_index=(n)
    @org_button_index = n
    self.button_index = @org_button_index
  end  
  def on_mouse_over()
    super()
    self.button_index = @org_button_index + 2
  end  
  def on_mouse_not_over()
    super()
    self.button_index = @org_button_index + 1
  end  
  def update
    super()
    return unless self.visible
    self.button_index = @org_button_index + 3 if @flash_counter > 0
    self.button_index = @org_button_index if !self.active
  end 
  def flash_time
    10
  end
end  
# // 02/06/2012
class Sprite::Tab < Sprite
  include Mixin::DrawTextEx
  def initialize(viewport=nil,text="",width=96,height=14)
    super(viewport)
    set(text,width,height)
  end
  def contents
    self.bitmap
  end 
  attr_reader :text
  def text=(n)
    if @text != n
      @text = n
      refresh()
    end  
  end  
  def line_height
    self.height || super
  end  
  def refresh()
    self.bitmap = Bitmap.new(@twidth, @theight)
    self.bitmap.gui_draw_tab()
    self.bitmap.font.set_style(:window_header2)
    draw_text(32,6,self.bitmap.width-32,12,@text,1)
  end  
  def set(text,width,height)
    @text    = text
    @twidth  = width
    @theight = height
    refresh()
  end  
  def twidth=(n)
    if @twidth != n
      @twidth = n
      refresh()
    end  
  end
  def theight=(n)
    if @theight != n
      @theight = n
      refresh()
    end  
  end  
end
class GUIExt::Tab < GUIExt::GuiObject_Base_ws
  include GUIExt::MixIn_AOV
  attr_reader :text
  attr_reader :parent
  def initialize(parent,viewport=nil,text="",width=96,height=24)
    super()
    @parent = parent
    self.width = width
    self.height = height
    self.viewport = viewport
    @text = text
    init_aov()
    refresh()
  end
  attr_accessor :tid
  def on_mouse_over
    @parent.on_mouse_over_tab(tid, self)
    super()
  end
  def on_mouse_not_over
    @parent.on_mouse_not_over_tab(tid, self)
    super()
  end
  def on_mouse_left_click
    @parent.on_mouse_left_click_tab(tid, self)
    super()
  end  
  def refresh
    @sprite = Sprite::Tab.new(self.viewport, @text, width, height)
  end  
  def text
    @sprite.text
  end  
  def text=(n)
    @sprite.text = n
  end 
  def update_sprite()
    super()
    @sprite.src_rect.set(0,0,*rect.xto_a(:width,:height))
  end  
end  
class Sprite::WindowTail < Sprite
  def initialize( viewport=nil, parent )
    super( viewport )
    @parent = parent
    update_position()
  end
  def dispose()
    dispose_bitmap()
    super()
  end  
  def refresh()
    dispose_bitmap()
    self.bitmap = Bitmap.new( @parent.tail_width, @parent.tail_height )
    self.bitmap.gui_draw_tail_base(:base_bmp=>@parent.footer_bitmap)
    update_position()
    update_visible()
  end  
  def update_position()
    self.x = @parent.tail_x
    self.y = @parent.tail_y
    self.z = @parent.tail_z
  end
  def update_visible()
    self.visible = @parent.tail_visible?()
  end  
  def update_opacity()
    self.opacity = @parent.tail_opacity()
  end  
  def update_viewport()
    self.viewport = @parent.viewport()
  end  
end
