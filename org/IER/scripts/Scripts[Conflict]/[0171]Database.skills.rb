# Database.skills
#==============================================================================#
# ■ Database.skills
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/07/2011
# // • Data Modified : 12/07/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/07/2011 V1.0 
#         Added:
#           Skill 0 (Dummy)
#           Skill 1 (Attack)
#
#==============================================================================#
module Database
  #@_ref_skills = load_data( "Skills.rvdata2" )
  #File.open("Skills.log", "w+") { |f|
  #  for i in 0...@_ref_skills.size
  #    f.puts @_ref_skills[i].inspect if @_ref_skills[i]
  #  end  
  #}
  Skill = RPG::Skill
  DEF_ATTACK_SKILL_ID = 1
  DEF_GUARD_SKILL_ID  = 6
  DEF_SKIP_SKILL_ID   = 9
  # // JUMPS
  # // (GENERAL_SKILLS)
  # // (FIRE_SKILLS)
  # // (WATER_SKILLS)
  # // (EARTH_SKILLS)
  # // (WIND_SKILLS)
  # // (LIGHT_SKILLS)
  # // (DARK_SKILLS)
def self.adjust_skills(items,element)  
  items.compact.each {|i|adjust_skill(i,element)}
  items
end  
def self.adjust_skill(item,element)  
  iof = @skill_id_off[element] # // ID offset 
  set_element_icon(element,item,:skill)
  item.id += iof
  item.element_id = element_id(element)
  item.stype_id          = @skill_type_id[element]
  item
end  
def self.add_skills(items)
  items.compact.each {|i|@skills[i.id]=i}
end  
#def self.set_skill_ids(skills,element)
#  skills.each_with_index{|s,i|@skill_id[(element.to_s+i.to_s).to_sym]=s.id if s}
#  skills
#end  
@skill_group = {}
def self.add_skill2groups(skill, *groups)
  groups.each { |g|
    @skill_group[g] ||= Set.new
    @skill_group[g].add(skill)
  }  
end  
def self.find_skills4groups(*groups)
  groups.inject(@skills){|r,g|r&@skill_group[g].to_a}
end  
def self.build_skills()     
  @skills = []
#==============================================================================#
# ◙ Skill 0 (Dummy)
#==============================================================================# 
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 0
  @skills[skill.id] = skill
# // (GENERAL_SKILLS)  
  add_skills mk_skills0()
# // (FIRE_SKILLS)  
  add_skills mk_skills1()    
# // (WATER_SKILLS)  
  add_skills mk_skills2()      
# // (EARTH_SKILLS)       
  add_skills mk_skills3()
# // (WIND_SKILLS)  
  add_skills mk_skills4() 
# // (LIGHT_SKILLS)  
  add_skills mk_skills5() 
# // (DARK_SKILLS)
  add_skills mk_skills6()   
#==============================================================================#
# ◙ REMAP
#==============================================================================#   
  for i in 0...@skills.size
    @skills[i].id = i if @skills[i]
  end  
end  
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
