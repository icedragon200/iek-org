# Ex_EquipItem
#==============================================================================#
# ♥ Ex_EquipItem
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 01/09/2012
# // • Data Modified : 01/09/2012
# // • Version       : 1.0
#==============================================================================#
class Ex_EquipItem < Ex_BaseItem
  include Mixin::EquipWrap
  attr_accessor :add_params  
  attr_accessor :sub_params 
  attr_accessor :add_features
  attr_accessor :sub_feature_codes
  attr_reader :level
  def initialize( base_object=nil )
    @item_id      = base_object ? base_object.id : 0
    @add_params   = Array.new(param_size, 0)
    @sub_params   = Array.new(param_size, 0)
    @add_features = []
    @sub_feature_codes = []
    init()
  end
  def init()
    init_durability()
    init_element_residue()
    init_level()
    init_exp()
    init_params()
  end  
  def base_object
  end 
  def base_params
    (0...8).to_a.collect { |i| base_object.level_params[i, level] }
  end  
  def rebuild_params
    @params = []
    for i in 0...param_size
      @params[i] = base_params[i] + add_params[i] - sub_params[i]
      @params[i] *= 0.1 if broken?() # // Reduce parameter to 10%
    end  
  end  
  def params
    rebuild_params unless @params
    @params
  end  
  def init_params()
    rebuild_params()
  end
  alias :base_features :features
  def rebuild_features
    @features = []
    @features += base_features
    @features.select! { |f| !sub_feature_codes.include?(f.code) }
    @features += add_features
  end  
  def features
    rebuild_features unless @features
    @features
  end  
  def is_weapon?
    false
  end  
  def is_armor?
    false
  end 
  alias :base_name :name
  def name
    @name ||= base_name + level_s()
    @name 
  end  
  # // Level System + Exp System . x .
  Ex::Handler::Level::WRAPS.each do |sym|
    module_eval("def #{sym}(*a,&b);@level_handler.#{sym}(*a,&b);end")
  end 
  def exp_for_level(level)
    base_object.exp_for_level(level)
  end 
  def on_exp_change()
    puts "#{name} - #{exp_rate.round(2) * 100} / 100"
    refresh()
  end  
  def on_level_change()
    reset4change()
  end  
  def init_level()
    @level_handler = Ex::Handler::Level.new(self,1,100)
  end 
  def init_exp()
    @level_handler.init_exp()
  end  
  # // Durability System
  attr_reader :durability
  def durability=(n)
    change = (@durability != n && n == 0)
    @durability = n.clamp(0,100).to_i
    reset4change() if change
  end  
  def durability_rate()
    durability / 100.0
  end  
  def broken?()
    @durability == 0
  end  
  def init_durability()
    self.durability = 100
  end  
  
  # // Element Residue System
  def element_residue(id=nil)
    id ? @element_residue[id] : @element_residue
  end  
  def element_residue_rate(id)
    element_residue(id) / 100.0
  end  
  def change_element_residue(id,n)
    @element_residue[id] = n.clamp(0,100).to_i
  end  
  def inc_element_residue(id,n)
    change_element_residue(id,element_residue(id)+n)
  end
  def dec_element_residue(id,n)
    inc_element_residue(id,-n)
  end  
  alias :inc_eleres :inc_element_residue
  alias :dec_eleres :dec_element_residue
  def init_element_residue()
    @element_residue = Array.new($data_system.elements.size,0)
  end  
  
  # // Element Spirit System
  def guardian_spirit
  end  
  
  def reset4change()
    @params   = nil
    @features = nil
    @name     = nil
  end
  def refresh()
    name()
    params()
    features()
  end  
  # // Private
  private :reset4change
  private :init
  private :init_level
  private :init_exp
  private :init_durability
  private :init_element_residue
  private :init_params
end  
class Game::RogueCharacter2
  def use_item(item)
    super(item)
    if(Ex_Database.skill?(item)) # // >_> We only got beef with teh skillz
      inc_weaponexp(calc_itemexp(item,:weapon)) if(item.id == attack_skill_id)
      inc_weaponres(calc_itemres(item,:weapon),item.element_id)
    end  
  end
  def on_item_hit(user,item=nil)
    super(user,item)
    if(item) # // ._. We dont want a nil item now do we?
      inc_armorexp(calc_itemexp(item,:armor))
      inc_armorres(calc_itemres(item,:armor),item.element_id)
    end  
  end  
  def ex_weapons()
    weapons.select{|w|Ex_Database.ex_weapon?(w)}
  end 
  def ex_armors()
    armors.select{|w|Ex_Database.ex_armor?(w)}
  end  
  def inc_weaponexp(n)
    ex_weapons.each{|w|w.inc_exp(n)}
  end  
  def inc_weaponres(n,id)
    ex_weapons.each{|w|w.inc_eleres(id,n)}
  end
  def inc_armorexp(n)
    ex_armors.each{|a|a.inc_exp(n)}
  end  
  def inc_armorres(n,id)
    ex_armors.each{|a|a.inc_eleres(id,n)}
  end 
  private
  def calc_itemexp(item,type=:all)
    1
  end  
  def calc_itemres(item,type=:all)
    1
  end  
end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
