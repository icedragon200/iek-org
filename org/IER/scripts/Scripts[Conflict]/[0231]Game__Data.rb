# Game::Data
#==============================================================================#
# ■ Game::Data
#==============================================================================#
# // • Created By    : IceDragon
# // • Data Created  : 12/06/2011
# // • Data Modified : 12/06/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/06/2011 V1.0  : Created Basic Header Info 
#==============================================================================#
module Game::Data 
  def self.get_actors()  
    return Database.actors()
  end
  def self.get_classes() 
    return Database.classes()
  end  
  def self.get_skills()  
    return Database.skills()
  end
  def self.get_items()   
    return Database.items()
  end
  def self.get_weapons() 
    return Database.weapons()
  end
  def self.get_armors()  
    return Database.armors()
  end
  def self.get_enemies() 
    return Database.enemies()
  end
  def self.get_troops() 
    return Database.troops()
  end
  def self.get_states()  
    return Database.states()
  end
  def self.get_animations()
    return Database.animations()
  end  
  def self.get_tilesets()
    return Database.tilesets()
  end
  def self.get_commonEvents
    return Database.commonEvents 
  end  
  def self.get_system()  
    return Database.system()
  end
  def self.get_mapinfos()
    return Database.mapinfos()  
  end  
  def self.get_startup()
    return Database.startup()
  end  
  def self.get_temp()
    return Database.temp()
  end 
  def self.get_traps()
    return Database.traps()
  end 
  def self.get_crafts()
    return Database.crafts()
  end  
  def self.get_spirit_classes()
    return Database.spirit_classes()
  end  
end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
