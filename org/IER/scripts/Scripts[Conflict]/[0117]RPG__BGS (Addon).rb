# RPG::BGS (Addon)
class RPG::BGS < RPG::AudioFile
  def play(pos = 0)
    if @name.empty?
      Audio.bgs_stop
      @@last = RPG::BGS.new
    else
      Audio.bgs_play('Audio/BGS/' + @name, self.volume, @pitch, pos) rescue nil
      @@last = self.clone
    end
  end
  def vol_rate
    $game_settings ? $game_settings.bgs_volume_rate : 1.0
  end  
  def volume
    return super * vol_rate
  end 
end

