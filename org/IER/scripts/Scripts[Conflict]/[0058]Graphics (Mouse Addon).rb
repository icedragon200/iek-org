# Graphics (Mouse Addon)
# // 01/25/2012
# // 01/25/2012
if defined? Mouse
#module Graphics
#  class << self
#    alias :pre_mouse_update :update
#    def update(*args)
#      pre_mouse_update(*args)
#    end  
#  end  
#end  
#end
module Main
  def self.pre_update()
    MouseEx.update()
  end
  def self.post_update()
    MouseEx.update_graphics()
  end  
end    
end
