# SpriteTest
#~ class Sprite::CursorEffect < Sprite
#~   attr_accessor :frame
#~   def initialize( viewport=nil )
#~     super( viewport )
#~     self.bitmap = Cache.system( "Cursor_Effect" )
#~     @frame = 0
#~     @time  = @time_max = 10
#~     update
#~   end
#~   def update()
#~     super()
#~     @time -= 1
#~     if @time <= 0
#~       @time = @time_max
#~       @frame = (@frame - 1) % 4
#~     end
#~     self.src_rect.set( 32 * @frame, 0, 32, 32 )
#~   end  
#~ end  

#~ sps = []
#~ for i in 0...18*14
#~   s = Sprite::CursorEffect.new( nil )
#~   s.x = (i % 18) * 32
#~   s.y = (i / 18) * 32
#~   s.frame = i % 4
#~   sps << s
#~ end  
#~ loop do
#~   Graphics.update
#~   Input.update
#~   sps.each do |s| s.update ; end
#~ end  
