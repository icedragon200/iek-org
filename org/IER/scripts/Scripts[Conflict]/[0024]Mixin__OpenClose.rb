# Mixin::OpenClose
#==============================================================================#
# ■ MixIn::OpenClose
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 01/04/2012
# // • Data Modified : 01/04/2012
# // • Version       : 1.0
#==============================================================================#
module Mixin::OpenClose
  def open_openness
    255
  end  
  def close_openness
    0
  end
  def open_time
    5
  end
  def close_time
    5
  end  
  def open?
    self.openness >= open_openness
  end  
  def close?
    self.openness <= close_openness
  end
  def open_easer
    :expo_out #:sine_inout
  end  
  def close_easer
    :expo_in #:sine_inout
  end
  def opening_or_closing?
    return @opening || @closing
  end
  def openness_tweener
    @openness_tweener
  end  
  def update_open
    #self.openness += 255 / open_time
    @openness_tweener.update
    self.openness = openness_tweener.value
    if openness_tweener.done?# && open?
      @opening = false 
      self.openness = open_openness
    end  
  end
  def update_close
    #self.openness -= 255 / close_time
    @openness_tweener.update
    self.openness = openness_tweener.value
    if openness_tweener.done?# && close?
      @closing = false 
      self.openness = close_openness
    end  
  end
  def set_to_open()
    self.openness = open_openness
    @opening = false
    @closing = false
    self
  end
  def set_to_close()
    self.openness = close_openness
    @opening = false
    @closing = false
    self
  end  
  def start_open()
    set_to_close()
    open()
  end
  def start_close()
    set_to_open()
    close()
  end
  def open
    Sound.play_window_open() # //
    @opening = true unless open?
    @closing = false
    @openness_tweener = Tween.new( 
      self.openness, open_openness, open_easer, Tween.frames_to_tt(open_time) )
    self
  end
  def close
    Sound.play_window_close() # //
    @closing = true unless close?
    @opening = false
    @openness_tweener = Tween.new( 
      self.openness, close_openness, close_easer, Tween.frames_to_tt(close_time) )
    self
  end
  def open_y
    open_y1
  end
  def openness_rate
    self.openness / open_openness.to_f
  end  
  def open_height
    openness_rate * height
  end
  def open_y1
    y + open_y1b()
  end  
  def open_y2
    y + open_y2b()
  end
  def open_y1b # // Bare
    (height/2.0) - (open_height/2.0)
  end  
  def open_y2b # // Bare
    height - open_y1b
  end
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
