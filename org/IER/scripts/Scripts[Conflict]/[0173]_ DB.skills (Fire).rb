#  | DB.skills (Fire)
# // 02/24/2012
# // 02/24/2012
module Database
def self.mk_skills1() # // Fire
  skills = []
  element = :fire
#==============================================================================#
# ◙ Skill (Cindle)
#==============================================================================#   
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 1
  skill.name              = "Cindle"
  skill.icon_index        = 96
  skill.description       = "Small flame, burn for glory"
  skill.features          = []
  skill.note              = ""
  skill.scope             = @scope[:one_enemy]
  skill.occasion          = @occasion[:battle] #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = @hit_type[:magical]
  skill.animation_id      = 31
  skill.stype_id          = @skill_type_id[element]
  skill.mp_cost           = 4
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = @damage_type[:hp_dam]
  skill.damage.element_id = element_id(element)
  skill.damage.formula    = "a.mat * 5 - b.mdf * 4"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skill.atk_range.range = 2
  skills[skill.id] = skill
  add_skill2groups(skill,element,:lv1)
#==============================================================================#
# ◙ Skill (Meteor)
#==============================================================================#   
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 2
  skill.name              = "Meteor"
  skill.icon_index        = 96
  skill.description       = "Whoosh, boooom, burn"
  skill.features          = []
  skill.note              = ""
  skill.scope             = @scope[:one_foe]
  skill.occasion          = @occasion[:battle] #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = @hit_type[:magical]
  skill.animation_id      = 3
  skill.stype_id          = @skill_type_id[element]
  skill.mp_cost           = 4
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = @damage_type[:hp_dam]
  skill.damage.element_id = element_id(element)
  skill.damage.formula    = "a.mat * 7 - b.mdf * 4"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skill.atk_range.range = 3
  skills[skill.id] = skill
  add_skill2groups(skill,element,:lv2)
#==============================================================================#
# ◙ Skill (Inflamite)
#==============================================================================#   
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 3
  skill.name              = "Inflamite"
  skill.icon_index        = 96
  skill.description       = "Flames of justice!"
  skill.features          = []
  skill.note              = ""
  skill.scope             = @scope[:one_enemy]
  skill.occasion          = @occasion[:battle] #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = @hit_type[:magical]
  skill.animation_id      = 2
  skill.stype_id          = @skill_type_id[element]
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = @damage_type[:hp_dam]
  skill.damage.element_id = element_id(element)
  skill.damage.formula    = "a.mat * 5 - b.mdf * 4"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skill.atk_range.range = 4
  skills[skill.id] = skill  
  add_skill2groups(skill,element,:lv3)
#==============================================================================#
# ◙ Skill (Blascite)
#==============================================================================#   
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 4
  skill.name              = "Blascite"
  skill.icon_index        = 96
  skill.description       = "A violent explosion"
  skill.features          = []
  skill.note              = ""
  skill.scope             = @scope[:one_enemy]
  skill.occasion          = @occasion[:battle] #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = @hit_type[:magical]
  skill.animation_id      = 14
  skill.stype_id          = @skill_type_id[element]
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = @damage_type[:hp_dam]
  skill.damage.element_id = element_id(element)
  skill.damage.formula    = "a.mat * 4 - b.mdf * 4"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skill.atk_range.range = 2
  skills[skill.id] = skill 
  add_skill2groups(skill,element,:lv2)
#==============================================================================#
# ◙ Skill (Burn)
#==============================================================================#   
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 5
  skill.name              = "Burn"
  skill.icon_index        = 96
  skill.description       = "Violent flames, engulfing anything"
  skill.features          = []
  skill.note              = ""
  skill.scope             = @scope[:one_enemy]
  skill.occasion          = @occasion[:battle] #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = @hit_type[:magical]
  skill.animation_id      = 33
  skill.stype_id          = @skill_type_id[element]
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = @damage_type[:hp_dam]
  skill.damage.element_id = element_id(element)
  skill.damage.formula    = "a.mat * 4 - b.mdf * 4"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skill.atk_range.range = 2
  skills[skill.id] = skill
  add_skill2groups(skill,element,:lv2)
#==============================================================================#
# ◙ Skill (Cinfer)
#==============================================================================#   
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 6
  skill.name              = "Cinfer"
  skill.icon_index        = 96
  skill.description       = "A soft flame, which calms the mind"
  skill.features          = []
  skill.note              = ""
  skill.scope             = @scope[:one_ally]
  skill.occasion          = @occasion[:battle] #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = @hit_type[:magical]
  skill.animation_id      = 30
  skill.stype_id          = @skill_type_id[element]
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = @damage_type[:none]
  skill.damage.element_id = 0
  skill.damage.formula    = "0"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skill.effects << MkEffect.add_buff(param_id(:mat), 5)
  skill.atk_range.range = 2
  skill.atk_range.minrange = 0
  skills[skill.id] = skill 
  add_skill2groups(skill,element,:lv1)
#==============================================================================#
# ◙ Skill (Inflamari)
#==============================================================================#   
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 7
  skill.name              = "Inflamari"
  skill.icon_index        = 96
  skill.description       = "Guardian Spirit Inflamari"
  skill.features          = []
  skill.note              = ""
  skill.scope             = @scope[:global]
  skill.occasion          = @occasion[:battle] #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = @hit_type[:magical]
  skill.animation_id      = 2
  skill.stype_id          = @skill_type_id[element]
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = @damage_type[:none]
  skill.damage.element_id = 0
  skill.damage.formula    = "0"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  #skill.effects << MkEffect.add_buff(param_id(:mat))
  skills[skill.id] = skill   
  add_skill2groups(skill,element,:lv4)
#==============================================================================#
# ◙ Skill (Xenolto.F)
#==============================================================================#   
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 8
  skill.name              = "Xenolto.F"
  skill.icon_index        = 96
  skill.description       = "Guardian Spirit Xenolto"
  skill.features          = []
  skill.note              = ""
  skill.scope             = @scope[:user_team]
  skill.occasion          = @occasion[:battle] #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = @hit_type[:magical]
  skill.animation_id      = 2
  skill.stype_id          = @skill_type_id[element]
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = @damage_type[:none]
  skill.damage.element_id = 0
  skill.damage.formula    = "0"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  #skill.effects << MkEffect.add_buff(param_id(:mat))
  skills[skill.id] = skill   
  add_skill2groups(skill,element,:lv4)
#==============================================================================#
# ◙ REMAP
#==============================================================================#   
  adjust_skills(skills,element)
end  
end  
