# MapManager
#==============================================================================#
# ■ MapManager
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/14/2011
# // • Data Modified : 12/21/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/14/2011 V1.0 
#     ♣ 12/20/2011 V1.0 
#         Added
#           generate_dungeon( info={} )
#
#==============================================================================#
module MapManager
  include Morph 
  @maps = {}
  def self.sym_tile( sym )
    return TileManager.sym_tile( sym )
  end  
  def self.get_map( map_id )
    return FLAT_MAP() if map_id == -1
    unless @maps[map_id]
      #@maps[map_id] = load_data(sprintf("Maps/Map%03d.rvdata2", map_id))
      @maps[map_id] = load_data(sprintf("Data/Map%03d.rvdata2", map_id))
      @maps[map_id].convert_to_rgss3_map! unless @maps[map_id].is_rgss3_map?
    end  
    return @maps[map_id]
  end  
  def self.calc_autotile(x, y, z, tile, data)
    # // Created By: Tomokay
    if data[x, y, z] == tile   # 床タイプのオートタイル
      n = 0
      n += 1 unless same_tile?(x - 1, y, z, tile, data)   # 左がない
      n += 1 unless same_tile?(x + 1, y, z, tile, data)   # 右がない
      n += 1 unless same_tile?(x, y - 1, z, tile, data)   # 上がない
      n += 1 unless same_tile?(x, y + 1, z, tile, data)   # 下がない
      if n == 0
        n += 1 unless same_tile?(x - 1, y - 1, z, tile, data)
        n += 2 unless same_tile?(x + 1, y - 1, z, tile, data)
        n += 4 unless same_tile?(x + 1, y + 1, z, tile, data)
        n += 8 unless same_tile?(x - 1, y + 1, z, tile, data)
      elsif n == 1
        if !same_tile?(x - 1, y, z, tile, data)      # 左がない
          n = 16
          n += 1 unless same_tile?(x + 1, y - 1, z, tile, data)   # 右上がない
          n += 2 unless same_tile?(x + 1, y + 1, z, tile, data)   # 右下がない
        elsif !same_tile?(x, y - 1, z, tile, data)   # 上がない
          n = 20
          n += 1 unless same_tile?(x + 1, y + 1, z, tile, data)   # 右下がない
          n += 2 unless same_tile?(x - 1, y + 1, z, tile, data)   # 左下がない
        elsif !same_tile?(x + 1, y, z, tile, data)   # 右がない
          n = 24
          n += 1 unless same_tile?(x - 1, y + 1, z, tile, data)   # 左下がない
          n += 2 unless same_tile?(x - 1, y - 1, z, tile, data)   # 左上がない
        else                                    # 下がない
          n = 28
          n += 1 unless same_tile?(x - 1, y - 1, z, tile, data)   # 左上がない
          n += 2 unless same_tile?(x + 1, y - 1, z, tile, data)   # 右上がない
        end
      elsif n == 2
        if !same_tile?(x - 1, y, z, tile, data)
          n = !same_tile?(x + 1, y, z, tile, data) ? 32 :
              same_tile?(x, y - 1, z, tile, data) ? 40 : 34
        else
          n = same_tile?(x + 1, y, z, tile, data) ? 33 :
              same_tile?(x, y - 1, z, tile, data) ? 38 : 36
        end
      elsif n == 3
        n = same_tile?(x, y + 1, z, tile, data) ? 42 :
            same_tile?(x + 1, y, z, tile, data) ? 43 :
            same_tile?(x, y - 1, z, tile, data) ? 44 : 45
      else
        n = 46
      end
      # // Corner Fix (IceDragon)
      n = 35 if n == 34 && !same_tile?( x + 1, y + 1, z, tile, data ) && same_tile?( x + 1, y, z, tile, data ) && same_tile?( x, y + 1, z, tile, data )
      n = 37 if n == 36 && !same_tile?( x - 1, y + 1, z, tile, data ) && same_tile?( x - 1, y, z, tile, data ) && same_tile?( x, y + 1, z, tile, data )
      n = 39 if n == 38 && !same_tile?( x - 1, y - 1, z, tile, data ) && same_tile?( x - 1, y, z, tile, data ) && same_tile?( x, y - 1, z, tile, data )
      n = 41 if n == 40 && !same_tile?( x + 1, y - 1, z, tile, data ) && same_tile?( x + 1, y, z, tile, data ) && same_tile?( x, y - 1, z, tile, data )
      data[x, y, z] = tile + n
    end
  end
  def self.calc_walltile( x, y, z, tile, data )
    if data[x, y, z] == tile
      n = 0
      n += 1 if !same_tile?( x-1, y, z, tile, data )
      n += 2 if !same_tile?( x, y-1, z, tile, data )
      n += 4 if !same_tile?( x+1, y, z, tile, data )
      n += 8 if !same_tile?( x, y+1, z, tile, data )
      data[x, y, z] += n
    end  
  end   
  def self.same_tile?( x, y, z, id, data )
    return true if x < 0   
    return true if y < 0 
    return true if x >= data.xsize 
    return true if y >= data.ysize 
    n = data[x, y, z].to_i
    return (n >= id and n < id + 48)
  end
  def self.nodes_placement( node_list, width, height )
    return node_list.collect() { |na| [na[0] * (width / 2), na[1] * (height / 2)] }
  end  
  def self.generate_room( info={} )
    width       = info[:width]  || 17
    height      = info[:height] || 17
    wall_height = info[:wall_height] || 1
    place_walls = info[:place_walls].nil?() ? true : info[:place_walls]
    rwidth      = width - 1 # Round Width
    rheight     = height - 1 # Round Height
    #==============================#
    # ● Room Types (16)
    # 0 - ╬ (4 Point)            
    # 1 - ╦ (3 Point)            
    # 2 - ╣ (3 Point)
    # 3 - ╠ (3 Point)
    # 4 - ╩ (3 Point)
    # 5 - ═ (2 Point)
    # 6 - ║ (2 Point)
    # 7 - ╗ (2 Point)
    # 8 - ╝ (2 Point)
    # 9 - ╔ (2 Point)
    # 10- ╚ (2 Point)
    # 11- ▲ (1 Point)
    # 12- ▼ (1 Point)
    # 13- ◄ (1 Point)
    # 14- ► (1 Point)
    # 15= ■ (0 Point)
    #==============================#
    room_type = info[:room_type] || 0
    nodes = []
    case room_type
    when 0
      nodes = nodes_placement( [[1, 0],[1, 2],[0, 1],[2, 1]], width, height )
    when 1
      nodes = nodes_placement( [[0, 1],[1, 2],[2, 1]], width, height )
    when 2
      nodes = nodes_placement( [[0, 1],[1, 2],[1, 0]], width, height )
    when 3
      nodes = nodes_placement( [[2, 1],[1, 2],[1, 0]], width, height )
    when 4
      nodes = nodes_placement( [[0, 1],[1, 0],[2, 1]], width, height )
    when 5
      nodes = nodes_placement( [[0, 1],[2, 1]], width, height )
    when 6
      nodes = nodes_placement( [[1, 0],[1, 2]], width, height )
    when 7
      nodes = nodes_placement( [[0, 1],[1, 2]], width, height )
    when 8
      nodes = nodes_placement( [[0, 1],[1, 0]], width, height )
    when 9
      nodes = nodes_placement( [[2, 1],[1, 2]], width, height )
    when 10  
      nodes = nodes_placement( [[2, 1],[1, 0]], width, height )
    when 11  
      nodes = nodes_placement( [[1, 2]], width, height )
    when 12  
      nodes = nodes_placement( [[1, 0]], width, height )
    when 13  
      nodes = nodes_placement( [[2, 1]], width, height )
    when 14  
      nodes = nodes_placement( [[0, 1]], width, height )
    when 15  
      nodes = []
    else
      raise "Weird Room Type: #{room_type}"
      exit
    end  
    ids = {
      "floor"=> 1,
      "ceil" => 2,
      "wall" => 3,
    }
    room = RPG::Dungeon::Room.new( 0, 0, width, height )
    nodes.each { |n| room.add_node( n[0], n[1] ) }
    # // Outline Room
    for i in 0...room.data.xsize
      room.data[i, 0, 0] = ids["ceil"]
      room.data[i, rheight, 0] = ids["ceil"]
    end  
    for i in 0...room.data.ysize 
      room.data[0, i, 0] = ids["ceil"]
      room.data[rwidth, i, 0] = ids["ceil"]
    end  
    for x in 1...room.data.xsize - 1
      for y in 1...room.data.ysize - 1
        room.data[x, y, 0] = ids["floor"]
      end
    end  
    offset_y = place_walls ? wall_height+1 : 1
    room.floor_rect.set( 1, offset_y, 
      room.data.xsize - 2, room.data.ysize - offset_y )
    room.nodes.each { |n| 
      for i in 0...offset_y
        room.data[n.x, n.y-i, 0] = ids["floor"] 
      end  
    }
    for x in 0...room.data.xsize
      for y in 0...room.data.ysize
        if room.data[x, y, 0] == ids["ceil"]
          if room.data[x, y+1, 0] == ids["floor"]
            for i in 1..wall_height
              room.data[x, y+i, 0] = ids["wall"]
            end  
          end  
        end  
      end
    end if place_walls
    return room
  end 
  def self.generate_dungeon( info={} )
    room_size  = info[:room_size] || [9, 9]
    room_count = info[:room_count] || [3, 3]
    padding    = info[:padding] || 6
    tiles      = info[:tiles] || {
      "floor" => 2048 + (48 * 16), 
      "ceil"  => 2048 + (48 * ((8*14)+4)),
      "wall"  => 2048 + (48 * ((8*15)+4)),
      "space" => 2048 + (48 * 20),
      "water" => 2048 + (48 * 8),
      "moss"  => 2048 + (48 * 39),
      "rug"   => 2048 + (48 * 43),
      "dark"  => 2048 + (48 * 18)
    }
    tpadding   = padding * 2
    dungeon = RPG::Dungeon.new( room_size[0] * room_count[0] + tpadding, 
      room_size[1] * room_count[1] + tpadding )
    dungeon.seed       = info[:seed] || dungeon.seed
    dungeon.padding    = [padding, padding]  
    dungeon.room_size  = room_size
    dungeon.room_count = room_count
    dungeon.tileset_id = 1
    dungeon.tiles      = tiles
    proper_room_type = proc { |rc, index|
      n = 0
      cx, cy = index % rc[0], index / rc[0]
      n = 1 if cx > 0 && cy == 0
      n = 2 if cx == rc[0] - 1 && cy > 0
      n = 3 if cx == 0 && cy > 0
      n = 4 if cx > 0 && cy == rc[1] - 1
      n = 9 if cx == 0 && cy == 0
      n = 7 if cx == rc[0] - 1 && cy == 0
      n = 10 if cx == 0 && cy == rc[1] - 1
      n = 8 if cx == rc[0] - 1 && cy == rc[1] - 1
      n
    }  
    seed_rand = Random.new( dungeon.seed )
    for rmi in 0...(room_count[0]*room_count[1])
      seed = seed_rand.rand(dungeon.seed + rmi + 
             room_count[0] + room_count[1] + 
             room_size[0] + room_size[1] + padding)
      width, height = *room_size
      room = generate_room( 
        {
          :room_type   => proper_room_type.call(room_count, rmi),
          :width       => width,
          :height      => height,
          :wall_height => 1,
          :place_walls => true
        }
      )
      room.seed = seed
      data = room.data
      data.iterate3() { |di, x, y, z|
        case di
        when 1
          tilid = tiles["floor"]
        when 2
          tilid = tiles["ceil"] 
        when 3
          tilid = tiles["wall"] 
        else
          tilid = di
        end 
        data[x, y, z] = tilid
      }
#=begin      
      room_rim = room.floor_rect.contract.to_a #[2, 4, width-3, height-3]
      if room.seed_rand.rand(3) == 0
        x, y = width / 2, height / 2
        tile_id = room.seed_rand.rand(2) == 0 ? tiles["space"] : tiles["water"]
        Growth.new.set_seed_gen(seed).grow_by(room_size[0] / 3).final_nodes.each { |n|
          fx, fy = x + n[0], y + n[1]
          if fx.between?( room_rim[0], room_rim[2] ) && 
             fy.between?( room_rim[1], room_rim[3] )
            data[fx, fy, 0] = tile_id if data[fx, fy, 0] == tiles["floor"]
          end  
        }
      end 
#=end      
      #if rand(2) == 0
        w, h = (width / 4), (height / 4)
        aligns = [[0, 0], [2, 0], [0, 2], [2, 2], [1, 1]]
        al = aligns[room.seed_rand.rand(aligns.size)]
        x, y = w + (w * al[0]), h + (h * al[1])
        tile_id = room.seed_rand.rand(2) == 0 ? tiles["moss"] : tiles["dark"]
        z = tile_id == tiles["moss"] ? 1 : 0
        Decimate.new(
          Growth.new.set_seed_gen(seed).grow_by(room_size[0] / 2).final_nodes
        ).set_seed_gen(seed).decimate_by((room_size[0] ** 1.3).to_i).final_nodes.each { |n|
          fx, fy = x + n[0], y + n[1]
          data[fx, fy, z] = tile_id if data[fx, fy, 0] == tiles["floor"]
        }
      #end 
      dungeon.rooms << room 
    end
    data = dungeon.remap_rooms.remap_data.data
    #data.iterate2( true ) { |x, y| 
    #  data[x, y, 0] = tiles["water"] if data[x, y, 0] == 0
    #}  
    for x in 0...data.xsize
      for y in 0...data.ysize
        calc_autotile( x, y, 0, tiles["floor"], data ) 
        calc_autotile( x, y, 0, tiles["ceil"] , data )
        calc_autotile( x, y, 0, tiles["space"], data )
        calc_autotile( x, y, 0, tiles["water"], data )
        calc_autotile( x, y, 1, tiles["moss"] , data )
        calc_autotile( x, y, 0, tiles["dark"] , data )
        calc_walltile( x, y, 0, tiles["wall"] , data )
      end
    end  
    return dungeon
  end  
  @tiles = { }
  @tiles["dirt"] = {
    "floor" => sym_tile("floor1"),
    "ceil"  => 2048 + (48 * ((8*14)+4)),
    "wall"  => 2048 + (48 * ((8*15)+4)),
    "space" => 2048 + (48 * 20),
    "water" => 2048 + (48 * 8),
    "moss"  => 2048 + (48 * 39),
    "dark"  => 2048 + (48 * 18)
  }
  @tiles["swamp"] = {
    "floor" => sym_tile("floor2"), 
    "ceil"  => 2048 + (48 * ((8*10)+4)),
    "wall"  => 2048 + (48 * ((8*11)+4)),
    "space" => 2048 + (48 * 21),
    "water" => 2048 + (48 * 6),
    "moss"  => 2048 + (48 * 31),
    "dark"  => 2048 + (48 * 19)
  }
  @tiles["stone"] = {
    "floor" => sym_tile("floor9"), 
    "ceil"  => 2048 + (48 * ((8*14)+5)),
    "wall"  => 2048 + (48 * ((8*15)+5)),
    "space" => 2048 + (48 * 28),
    "water" => 2048 + (48 * 10),
    "moss"  => 2048 + (48 * 39),
    "dark"  => 2048 + (48 * 26)
  }
  @tiles["ice"] = {
    "floor" => sym_tile("floor10"), 
    "ceil"  => 2048 + (48 * ((8*10)+5)),
    "wall"  => 2048 + (48 * ((8*11)+5)),
    "space" => 2048 + (48 * 29),
    "water" => 2048 + (48 * 12),
    "moss"  => 2048 + (48 * 31),
    "dark"  => 2048 + (48 * 27)
  }
  @tiles["lava"] = {
    "floor" => sym_tile("floor17"), 
    "ceil"  => 2048 + (48 * ((8*14)+6)),
    "wall"  => 2048 + (48 * ((8*15)+6)),
    "space" => 2048 + (48 * 36),
    "water" => 2048 + (48 * 4),
    "moss"  => 2048 + (48 * 31),
    "dark"  => 2048 + (48 * 34)
  }
  def self.FLAT_MAP
    m = generate_dungeon( 
      :room_size  => [9, 9+1],#[15, 15+1],#[9, 9+1],
      :room_count => [2, 2],
      :padding    => 9,
      :tiles      => @tiles["dirt"].clone,
      #:seed       => 2,
    )
    ev = EventManager.make_floor_up_ev()
    ev.id = 1
    m.tileset_id = 4
    m.events[1] = ev
    m.display_name = "Test Dungeon"
    m
  end  
  def self.rect_to_pos( rect )
    rect_array_to_pos( rect.to_a )
  end  
  def self.rect_array_to_pos( arra )
    result = []
    for x in arra[0]..arra[2]
      for y in arra[1]..arra[3]
        result << [x, y]
      end
    end  
    return result
  end  
  def self.text_based_minimap( objs, data, flags )
    result = []
    for y in 0...data.ysize
      for x in 0...data.xsize
        tids = [data[x, y, 0], data[x, y, 1], data[x, y, 2]]
        bit = 0x0001
        pass = tids.reverse.all? { |t| 
          flag = flags[t]
          if flag & 0x10 != 0            # [☆] : 通行に影響しない
            true
          elsif flag & bit == 0     # [○] : 通行可
            break true
          elsif flag & bit == bit   # [×] : 通行不可 
            break false 
          else
            break false
          end  
        }  
        result << (pass ? "░" : "█")
      end
    end 
    objs.each { |c|
      ind = c.x + (c.y * data.xsize)
      result[ind] = (c.friend? ? "☻" : "☺")
    }
    for y in 0...data.ysize
      result.insert(y * (data.xsize+1), "\n")
    end  
    return result.join("")
  end
  def self.text_based_minimap2(data)
    result = []
    for y in 0...data.ysize
      for x in 0...data.xsize
        result << (data[x,y]==1 ? "░" : "█")
      end
    end  
    for y in 0...data.ysize
      result.insert(y * (data.xsize+1), "\n")
    end  
    return result.join("")
  end  
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
