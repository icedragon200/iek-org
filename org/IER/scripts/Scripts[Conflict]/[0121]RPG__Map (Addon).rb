# RPG::Map (Addon)
# // 02/18/2012
# // 02/18/2012
class RPG::Map
  def is_rgss3_map?
    @display_name != nil
  end 
  def convert_to_rgss3_map
    clone.convert_to_rgss3_map!
  end  
  def convert_to_rgss3_map!
    @display_name ||= ''
    @tileset_id   ||= 0 # // 1
    @specify_battleback = false if @specify_battleback.nil?()
    @battleback_floor_name ||= ''
    @battleback_wall_name  ||= ''
    @autoplay_bgm = false if @autoplay_bgm.nil?()
    @bgm ||= RPG::BGM.new
    @autoplay_bgs = false if @autoplay_bgs.nil?()
    @bgs ||= RPG::BGS.new('', 80)
    @disable_dashing = false if @disable_dashing.nil?()
    @encounter_list ||= []
    @encounter_step ||= 30
    @parallax_name  ||= ''
    @parallax_loop_x = false if @parallax_loop_x.nil?()
    @parallax_loop_y = false if @parallax_loop_y.nil?()
    @parallax_sx ||= 0
    @parallax_sy ||= 0
    @parallax_show = false if @parallax_show.nil?()
    @note   ||= ''
    @data.resize(@data.xsize, @data.ysize, 4)
    @events ||= {}
  end  
end  
