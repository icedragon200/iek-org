# Game::Unit (Ex)
# // 02/19/2012
# // 02/19/2012
class Game::Unit
  #--------------------------------------------------------------------------
  # ● パーティメンバーの最も高いレベルの取得
  #--------------------------------------------------------------------------
  def highest_level
    lv = collect_levels().max()#members.collect {|actor| actor.level }.max
  end
  # // 02/19/2012
  def collect_levels()
    members.collect {|actor| actor.level }
  end  
  def lowest_level()
    lv = collect_levels().min()
  end  
  def average_level()
    members.inject(0) {|r,actor| r + actor.level }.div(members.size) 
  end  
  def random_level()
    collect_levels().sample()
  end
  # // 02/28/2012
  def index(battler)
    members.index(battler)
  end   
end  
