# GameManager (Setup)
#==============================================================================#
# ■ GameManager (Setup)
#==============================================================================#
# // • Created By    : IceDragon
# // • Data Created  : 12/27/2011
# // • Data Modified : 02/02/2012
# // • Version       : 1.0a
#==============================================================================#
# // Contains important information for the game.
# // This includes:
# //   Game ID
# //   Game Symbol
# //   Game Name
#==============================================================================#
# ● Change Log
#     ♣ 12/27/2011 V1.0 
#==============================================================================#
module GameManager
  SETUP = true # // >_> You can change the value to anything you like, just dont remove it
  GAME_NAME = "Earthen"    # // Used by save file
  GAME_SYM  = "EDOS1V0100" # // INITIALS+VERSION (Passed along with achivement string)
  GAME_ID   = "VX_TF_0001" # // Engine_TurtleFactory_ID (Used as achievement key)
  MAIN_FOLDER_NAME = "/TurtleFactory/"
end  
GMG = GameManager
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
