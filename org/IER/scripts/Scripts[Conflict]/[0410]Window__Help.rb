# Window::Help
#==============================================================================#
# ♥ Window::Help
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/31/2011
# // • Data Modified : 12/31/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/31/2011 V1.0 
#
#==============================================================================#
class Window::Help < Window::Base
  attr_reader :item
  def initialize( x=0, y=0, width=Graphics.width, height=line_height+(standard_padding*2) )
    super( x, y, width, height )
    @_def_align = 0
    self.windowskin = Cache.system("Window_Console")
  end   
  def standard_padding
    return 8
  end
  def set_text( text="", align=@_def_align, forced=false )
    if @text != text || @align != align || forced
      @text = text
      @align = align
      refresh()
    end  
  end 
  def clear
    set_text("")
  end
  def set_item(item)
    set_text(item ? item.description : "")
  end
  def refresh()
    contents.clear()
    contents.font.set_style( :default )
    @artist.draw_text( 0, 0, contents.width, contents.height, @text, @align ) 
  end  
  attr_reader :battler
  def battler=( new_battler )
    return if @battler == new_battler
    set_battler(new_battler)
    refresh()
  end 
  def set_battler(n)
    @battler = n
  end
  def actor
    battler
  end
  def actor=( new_actor )
    self.battler = new_actor
  end 
  def set_actor(n)
    set_battler(n)
  end  
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
