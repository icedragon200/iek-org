# RPG::SE (Addon)
class RPG::SE < RPG::AudioFile
  def play
    unless @name.empty?
      Audio.se_play('Audio/SE/' + @name, self.volume, @pitch) rescue nil
    end
  end
  def vol_rate
    $game_settings ? $game_settings.se_volume_rate : 1.0
  end 
  def volume
    return super * vol_rate
  end 
end

