# MathOp
#==============================================================================#
# ■ MathOp
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 01/16/2012
# // • Data Modified : 01/16/2012
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 01/16/2012 V1.0 
#==============================================================================# 
module MathOp
  module MathOp_Codes
    OP_EQUAL    = 0
    OP_ADD      = 1
    OP_SUBTRACT = 2
    OP_MULTIPLY = 3
    OP_DIVIDE   = 4
    OP_MODULO   = 5
    OP_MIN      = 11
    OP_MAX      = 12
  end  
  def self.arithmetic( value1, op_code, value2 )
    case(op_code)
    when OP_EQUAL # // Equal
      return value2
    when OP_ADD # // Addition
      return value1 + value2
    when OP_SUBTRACT # // Subtraction
      return value1 - value2
    when OP_MULTIPLY # // Multiplication
      return value1 * value2
    when OP_DIVIDE # // Division
      return value1 / value2.max(1) # // Protect
    when OP_MODULO # // Modulation
      return value1 % value2.max(1) # // Protect
    when OP_MIN # // Min
      return value1.min(value2)
    when OP_MAX # // Max
      return value1.max(value2)
    end  
    return value1
  end 
  module MathOp_Codes
    LOGIC_EQUAL   = 0 # // Equal to
    LOGIC_NEQUAL  = 1 # // Not Equal to
    LOGIC_GREAT   = 2 # // Greater than
    LOGIC_LESS    = 3 # // Less than
    LOGIC_GOREQ   = 4 # // Greater than or Equal
    LOGIC_LEREQ   = 5 # // Less than or Equal
  end  
  def self.logic( value1, logic_code, value2 )
    case(logic_code)
    when LOGIC_EQUAL
      return value1 == value2
    when LOGIC_NEQUAL
      return value1 != value2  
    when LOGIC_GREAT
      return value1 > value2
    when LOGIC_LESS
      return value1 < value2
    when LOGIC_GOREQ  
      return value1 >= value2
    when LOGIC_LEREQ
      return value1 <= value2
    end  
    return false
  end  
  include MathOp_Codes
end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
