# [IEI][MG]Minesweeper
# // 02/21/2012
# // 02/22/2012
class Game::Minesweeper
  attr_reader :result
  attr_reader :mine_table
  attr_reader :reveal_table
  attr_accessor :redraw
  attr_accessor :need_refresh
  ID_MINE = 9
  RESULT_WIN = 0
  RESULT_LOSE = 1
  def initialize
    @width = 13 # // 9
    @height = 9
    @mine_count = 10
  end  
  def new_game(width=13,height=9,mine_count=10)
    @width = width
    @height = height
    @mine_count = mine_count
    xys = []
    xy = nil
    @mine_table = Table.new(@width,@height)
    size = (@mine_table.xsize * @mine_table.ysize) 
    @reveal_table = Table.new(@mine_table.xsize,@mine_table.ysize,2)
    @mine_table.iterate2(true){|x,y|xys<<[x,y]}
    @mine_count = @mine_count.clamp(1,size-[@mine_table.xsize,@mine_table.ysize].max)
    @mines = Array.new(mine_count){xy=xys.pick!();@mine_table[xy[0],xy[1]]=ID_MINE;xy}
    @mine_table.iterate_map2(){|i,x,y|i==ID_MINE ? i : mines_sorrounding_count(x,y)}
    @revealed = []
    @redraw = [] 
    @need_refresh = true
    @mine_uncovered = nil
    @result = nil
    @total_reveals = size - @mines.size
    #puts 
    #for y in 0...@mine_table.ysize
    #  puts ""
    #  for x in 0...@mine_table.xsize
    #    print @mine_table[x,y] == 9 ? "#" : @mine_table[x,y]
    #  end  
    #end 
  end
  def check_conditions()
    if @mine_uncovered
      end_game(RESULT_LOSE)
      return true
    elsif @total_reveals == @revealed.size
      end_game(RESULT_WIN)
      return true
    end  
    return false
  end  
  def end_game(result)
    @result=result
    case @result
    when RESULT_WIN
      reveal_all()
    when RESULT_LOSE
      reveal_mines()
    end  
  end
  def mine_here?(x,y)
    @mine_table[x,y] == ID_MINE
  end
  def bad_flag?(x,y)
    @mine_table[x,y] != ID_MINE && @reveal_table[x,y,1] > 0
  end  
  def reveal_spaces(x,y)
    return if @mine_table.oor?(x,y)
    return @mine_uncovered=[x,y] if mine_here?(x,y)
    return reveal_space(x,y) if @mine_table[x,y] > 0
    nodes = []
    closed_nodes = Table.new(@mine_table.xsize, @mine_table.ysize)
    open_nodes = [[x,y]]
    node = nil
    maxit = closed_nodes.xsize * closed_nodes.ysize
    n = nil
    maxit.times() do
      node = open_nodes.pop
      break if node.nil?()
      unless closed_nodes[node[0],node[1]] == 1
        closed_nodes[node[0],node[1]] = 1
        nodes << [node[0],node[1]]
        [[node[0]+1,node[1]],
         [node[0]-1,node[1]],
         [node[0],node[1]+1],
         [node[0],node[1]-1],
         [node[0]+1,node[1]+1], # // Ex . x .
         [node[0]+1,node[1]-1],
         [node[0]-1,node[1]+1],
         [node[0]-1,node[1]-1]
        ].each do |n|
          if closed_nodes[*n] != 1 && !closed_nodes.oor?(*n)
            if !(@mine_table[*n] > 0)
              open_nodes << n
            else  
              reveal_space(*n)
            end
          end
        end
      end  
    end  
    nodes.each { |n| reveal_space(*n) }
  end 
  def reveal_all()
    @mine_table.iterate2(true){|x,y|reveal_space(x,y)}
    @need_refresh = true
  end  
  def reveal_mines()
    @mines.each {|n|reveal_space(*n)}
    @need_refresh = true
  end  
  def reveal_space(x,y)
    return if @mine_table.oor?(x,y)
    return if @reveal_table[x,y,0] == 1 || @reveal_table[x,y,1] > 0
    @reveal_table[x,y,0] = 1
    @revealed << [x, y]
    @redraw << [x,y]
  end
  def flag_space(x,y)
    return if @mine_table.oor?(x,y)
    @reveal_table[x,y,1] = @reveal_table[x,y,1].succ.modulo(3)
    @redraw << [x,y]
  end  
  def mines_sorrounding_count(x,y)
    result = 0
    for i in (-1..1)
      for i2 in (-1..1)
        result += 1 if(@mine_table[x+i,y+i2]==9) unless @mine_table.oor?(x+i,y+i2)
      end
    end  
    result
  end
end  
Minesweeper = Game::Minesweeper
class Spriteset::Minesweeper 
  @@tileset = nil
  @@tilesize = 32
  def self.tileset
    if(@@tileset.nil?() || @@tileset.disposed?())
      @@tileset = IEI::Tileset.new(7,1,@@tilesize,@@tilesize)
      [DrawExt::TRANS_BAR_COLORS, # // Tile
       DrawExt::RED_BAR_COLORS,   # // Mine  
       DrawExt::GREEN_BAR_COLORS, # // Flag
       DrawExt::BLUE_BAR_COLORS,  # // Hidden
       DrawExt::YELLOW_BAR_COLORS,# // ?
       DrawExt::WHITE_BAR_COLORS, # // (Unused)
       DrawExt::BLACK_BAR_COLORS  # // Bad Flag
      ].each_with_index { |c, i| 
        b = CacheExt.make_button("",c,@@tileset.cell_width,@@tileset.cell_height)
        @@tileset.bitmap.blt(@@tileset.cell_r(i).x,0,b,b.rect) ; b.dispose 
      }
    end
    @@tileset
  end  
  def initialize(viewport=nil)
    @base_layer = Sprite.new(viewport)
    @flag_layer = Sprite.new(viewport)
    @grid = IEI::Grid.new(8,1,tilesize,tilesize)
    @minesweeper = nil
    self.x, self.y, self.z = 0,0,0
  end
  def tilesize()
    @@tilesize
  end  
  attr_reader :minesweeper
  def minesweeper=(n)
    @minesweeper = n
    refresh()
  end  
  def data
    @minesweeper.mine_table
  end  
  def rdata
    @minesweeper.reveal_table
  end  
  attr_reader :x
  def x=(n)
    @flag_layer.x = @base_layer.x = @x = n
  end  
  attr_reader :y
  def y=(n)
    @flag_layer.y = @base_layer.y = @y = n
  end  
  attr_reader :z
  def z=(n)
    @flag_layer.z = 1 + @base_layer.z = @z = n
  end 
  def width
    @base_layer.width
  end
  def height
    @base_layer.height
  end
  def viewport=(n)
    @flag_layer.viewport = @base_layer.viewport = n
  end  
  def refresh()
    @base_layer.dispose_bitmap()
    @flag_layer.dispose_bitmap()
    @grid.columns = data.xsize
    @grid.rows    = data.ysize
    @base_layer.bitmap = Bitmap.new(data.xsize*tilesize,data.ysize*tilesize)
    @flag_layer.bitmap = Bitmap.new(*@base_layer.bitmap.rect.xto_a(:width,:height))
    redraw()
  end
  def redraw()
    data.iterate2(true){ |x,y| redraw_tile(x,y) }
  end
  def redraw_tile(x,y)
    id   =  data[x,y]
    rid  = rdata[x,y,0]
    rid2 = rdata[x,y,1]
    ts = self.class.tileset
    b  = ts.bitmap
    gr = @grid.cell_r(x,y)
    @base_layer.bitmap.clear_rect(gr)
    if(rid == 1)
      if(id != 9)
        text = id == 0 ? "" : id
        @base_layer.bitmap.blt(gr.x,gr.y,b,ts.cell_r(0))
        @base_layer.bitmap.font.size = tilesize
        @base_layer.bitmap.font.color = Pallete[:sys_orange]
        @base_layer.bitmap.draw_text(gr,text,1)      
      else
        if(@minesweeper.bad_flag?(x,y) && @minesweeper.result == 1)
          r = ts.cell_r(6)
        else
          r = ts.cell_r(rid2 > 0 ? 2 : 1)
        end  
        @base_layer.bitmap.blt(gr.x,gr.y,b,r)
      end
    #elsif rid == 2  
    else
      case(rid2)
      when 1 # // . x . Flag
        r = ts.cell_r(2)
      when 2 # // . x . ?
        r = ts.cell_r(4)
      else # // Normal Space
        r = ts.cell_r(3)
      end 
      r = ts.cell_r(6) if @minesweeper.bad_flag?(x,y) && @minesweeper.result == 1
      @base_layer.bitmap.blt(gr.x,gr.y,b,r)
    end  
  end  
  def dispose()
    @base_layer.dispose_all()
    @flag_layer.dispose_all()
    @disposed = true
  end  
  def update()
    if @minesweeper.need_refresh
      @minesweeper.need_refresh = false
      @minesweeper.redraw.clear()
      refresh()
    end  
    if @minesweeper.redraw.size > 0
      @minesweeper.redraw.each { |n| redraw_tile(*n) }
      @minesweeper.redraw.clear()
    end  
  end
end  
class Window::MinesweeperTimer < Window::Base
  attr_reader :time
  def initialize(x,y)
    super(x,y,window_width,window_height)
    contents.font.set_style(:window_header2)
    reset()
  end
  def line_height
    24
  end  
  def standard_padding
    4
  end  
  def window_width
    96
  end  
  def window_height
    32
  end  
  def reset()
    @counter = 0
    @time = 0
    refresh()
  end  
  def refresh()
    contents.clear
    @artist.draw_text(0,0,self.contents.width,line_height,"%03d" % @time, 1)
  end  
  def update()
    super()
    update_time() if self.active
  end  
  def update_time()
    @counter = @counter.pred.max(0)
    if @counter.eql?(0)
      @counter = Graphics.frame_rate
      @time += 1
      refresh()
    end  
  end  
end
class Scene::Minesweeper < Scene::Base
  def start()
    super()
    create_background()
    @minesweeper = Game::Minesweeper.new()
    @mspriteset = Spriteset::Minesweeper.new(@viewport)
    create_all_windows()
    new_game()
    @mspriteset.minesweeper = @minesweeper
  end
  def terminate()
    @mspriteset.dispose()
    super()
  end  
  def create_background
    super()
    @background_sprite.bitmap = SceneManager.background_bitmap
  end
  def check_conditions()
    at_end_game() if @minesweeper.check_conditions()
  end  
  def new_game()
    @minesweeper.new_game(Graphics.width/32,(Graphics.height-32)/32)
    @timer_window.reset()
    @timer_window.deactivate()
  end  
  def at_end_game()
    @timer_window.deactivate()
    case @minesweeper.result
    when Minesweeper::RESULT_WIN
      pop_quick_text_c("=w=d WIN")
      pop_quick_text_c("Time Taken: #{@timer_window.time} sec(s)")
    when Minesweeper::RESULT_LOSE
      pop_quick_text_c(".x.; LOSE")
    end  
    create_dim_background()
    pop_confirm_window( :text => "New Game?" ) { |win| win.salign(1,1) }
    dispose_dim_background()
    if(@confirm_result == 0)
      new_game() 
    elsif(@confirm_result == 1)  
      return_scene()
    end  
  end
  def create_all_windows()
    create_timer_window()
  end  
  def create_timer_window()
    @timer_window = Window::MinesweeperTimer.new(0,0)
    add_window(@timer_window)
  end    
  def update_basic()
    super()
    @mspriteset.update()
  end  
  def update
    super()
    @mspriteset.x = (Graphics.width - @mspriteset.width) / 2
    @mspriteset.y = 32
    @mspriteset.z = 1
    @mspriteset.update()
    x32,y32 = (MouseEx.x / 32).to_i, (MouseEx.y / 32).to_i
    ox32,oy32 = (@mspriteset.x / 32).to_i, (@mspriteset.y / 32).to_i
    x32 -= ox32
    y32 -= oy32
    if MouseEx.left_click?()
      @timer_window.activate()
      @minesweeper.reveal_spaces(x32,y32)
      check_conditions()
    elsif MouseEx.right_click?()
      @timer_window.activate()
      @minesweeper.flag_space(x32,y32)
    end  
  end  
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
