# GuiTest
#~ DataManager.init
#~ sliderv = GUIExt::Slider.new(nil,:vert,256,0,24,1)
#~ sliderv.min = -1.0
#~ sliderv.max = 1.0
#~ sliderv.enable_input
#~ sliderv.activate
#~ sliderv.bar_color = (Pallete[:sys_orange2]).xset(:alpha=>198)
#~ sliderh = GUIExt::Slider.new(nil,:horz,256,24,0,1)
#~ sliderh.min = -1.0
#~ sliderh.max = 1.0
#~ sliderh.enable_input
#~ sliderh.activate
#~ sliderh.bar_color = (Pallete[:sys_orange2]).xset(:alpha=>198)
#~ sliderv.tick_size = sliderh.tick_size = 0.1
#~ background_sprite = Sprite::WheelBackground.new()
#~ background_sprite.bitmap = Cache.system( "CommandWheel_Background" )
#~ background_sprite.center_oxy(32,32)
#~ background_sprite.salign(1,1)
#~ polygon = Circle.new(background_sprite.x,background_sprite.y,background_sprite.width/2)
#~ polygon.angle_offset = 180
#~ ball = Sprite.new()
#~ ball.bitmap = Cache.system("Ball")
#~ balla = Sprite.new()
#~ balla.bitmap = Cache.system("BallArrow")
#~ balla.ox = 16
#~ balla.oy = 16
#~ w, h = (198+32) / 2, (198+32) / 2
#~ balla.x, balla.y = w + 24 + (sliderh.rate * w), h + 24 + (sliderv.rate * h)
#~ def points_to_angle( x1, y1, x2, y2 )
#~   ((Math.sin(x1 - x2) * Math.cos(y1 - y2)) / Math::PI) * 180.0
#~   #opangle = Math::PI * (angle-@angle_offset) / 180.0
#~   #return @cx + @radius_x * Math.sin(opangle), @cy + @radius_y * Math.cos(opangle)
#~ end  
#~ loop do
#~   Graphics.update
#~   Input.update
#~   sliderv.update
#~   sliderh.update
#~   w, h = (198+32) / 2, (198+32) / 2
#~   ball.x, ball.y = w + 24 + (sliderh.rate * w), h + 24 + (sliderv.rate * h)
#~   balla.angle = points_to_angle( balla.x, balla.y, ball.x, ball.y )
#~ end  
