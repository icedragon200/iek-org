# SubScene::Base
# // 02/03/2012
# // 02/03/2012
class SubScene::Base < Scene::Base
  attr_reader :active 
  def main
    super
    perform_transition()
  end  
  def initialize()
    @end_ss   = false
    @active   = false
    @disposed = false
  end
  def disposed?()
    @disposed == true
  end  
  def dispose()
    pre_terminate()
    terminate()
  end  
  def terminate
    super
    @disposed = true
  end  
  def activate()
    self.active = true
  end  
  def deactivate()
    self.active = false
  end  
  def active=(n)
    if @active != n
      @active = n
      refresh_active()
    end  
  end  
  def refresh_active()
  end  
  def scene_changing?()
    @end_ss == true
  end  
  def end_subscene()
    @end_ss = true
  end
  def ss_end?()
    @end_ss == true
  end  
  def return_scene()
  end
  def update_basic()
    Fiber.yield
    update_all_windows()
  end
  def update()
    super()
    update_active() if @active
    update_inactive() if !@active
  end  
  def update_active()
  end
  def update_inactive()
  end  
end  
