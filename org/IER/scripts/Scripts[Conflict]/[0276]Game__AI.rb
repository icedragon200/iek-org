# Game::AI
#==============================================================================#
# ♥ Game::AI
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/21/2011
# // • Data Modified : 12/21/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/21/2011 V1.0 
#
#==============================================================================# 
class Game::AI
  TRAP_IQ          = 1
  SELF_PREV_IQ     = 2
  PATROL_IQ        = 3
  AGGRO_IQ         = 4
  PARTY_MOVE_IQ    = 5
  PARTY_SUPPORT_IQ = 6
  HEALING_IQ       = 7
  MOVE_IQ          = 8
  PICKUP_IQ        = 9
  class IQ
    attr_accessor :code, :value
    def initialize( code, value )
      set( code, value )
    end  
    def set( code, value )
      @code, @value = code, value
      return self
    end  
    def ==( obj )
      return obj.is_a?(IQ) ? self._hashcode == obj._hashcode : false
    end
    def _hashcode
      return [@code, @value]
    end  
  end  
  attr_accessor :priorities
  attr_accessor :search_range
  attr_reader :iqs
  attr_reader :subject
  PRIORITIES = [[0, :offense], [1, :defense], [2, :support]]
  def initialize( subject )
    @subject      = subject
    @priorities   = Table.new( PRIORITIES.size ) # Offensive, Defensive, Supportive
    @search_range = 15
    @iqs          = {}
  end
  def set_subject( subject )
    @subject = subject
    return self
  end  
  def priotity_scores
    PRIORITIES.collect { |pra| [@priorities[pra[0]], pra[1]] }
  end  
  def lowest_priority
    (priotity_scores.min { |a, b| a[0] <=> b[0] })[1]
  end  
  def highest_priority
    (priotity_scores.max { |a, b| a[0] <=> b[0] })[1]
  end  
  def get_iq( code )
    return @iqs[code]
  end
  def set_iq( code, value )
    @iqs[code] = (get_iq( code ) || IQ.new( 0, 0 )).set( code, value )
    return self
  end  
  def has_iq?( code )
    return !@iqs[code].nil?()
  end    
  def iq( code )
    i = get_iq( code )
    return i.nil?() ? -1 : i.value
  end   
  def clear_iqs()
    @iqs.clear
    return self
  end  
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
