# RPG::Craft
# // 01/03/2011
# // 01/03/2011
class RPG::Craft < RPG::BaseItem
  def initialize()
    super()
    initialize_add()
    @recipe_items = []
    @result_items = []
    @requirement  = 0 # // 0 - Any Order, 1 - Fixed Order
  end  
  attr_accessor :recipe_items
  attr_accessor :result_items
  attr_accessor :requirement
end  
