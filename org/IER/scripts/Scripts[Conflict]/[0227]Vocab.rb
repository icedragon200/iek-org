# Vocab
#==============================================================================#
# ■ Vocab
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/06/2011
# // • Data Modified : 12/07/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/06/2011 V1.0 
#==============================================================================#
module Vocab
  ExpNext  = 'EXP to next level'
  ExpTotal = 'Current EXP'
  
  UseItem         = '%s used %s!'
  
  # Critical Hit
  CriticalToEnemy = 'An excellent hit!!'
  CriticalToActor = 'A painful blow!!'

  # Results for Actions on Actors
  ActorDamage     = '%s took %s damage!'
  ActorLoss       = '%1$s lost %3$s %2$s!'
  ActorDrain      = '%1$s drained %3$s %2$s!'
  ActorNoDamage   = '%s took no damage!'
  ActorNoHit      = 'Miss! %s took no damage!'
  ActorRecovery   = '%1$s recovered %3$s %2$s!'

  # Results for Actions on Enemies
  EnemyDamage     = '%s took %s damage!'
  EnemyLoss       = '%1$s lost %3$s %2$s!'
  EnemyDrain      = '%1$s drained %3$s %2$s!'
  EnemyNoDamage   = '%s took no damage!'
  EnemyNoHit      = 'Miss! %s took no damage!'
  EnemyRecovery   = '%1$s recovered %3$s %2$s!'
  
  Evasion         = '%s evaded the attack!'
  MagicEvasion    = '%s evaded the spell!'
  MagicReflection = '%s reflected the spell!'
  CounterAttack   = '%s counter-attacks!'
  Substitute      = '%s substitutes!'

  BuffAdd         = '%s\'s %s increased!'#'%sの%sが上がった！'
  DebuffAdd       = '%s\'s %s decreased!' #'%sの%sが下がった！'
  BuffRemove      = '%s\'s %s returned to normal' #'%sの%sが元に戻った！'

  ActionFailure   = 'There was no effect on %s!'
  
  TrapSteppedOn   = '%s stepped on a %s'
  TrapFailed      = 'But nothing happened...'
  TrapTriggered   = '%s was triggered!'
  
  def self.new_game
    return 'Beginning'
  end
  def self.continue
    return 'Load'
  end
  def self.shutdown
    return 'Poop'
  end  
  def self.exp
    return 'EXP'
  end
  def self.exp_a
    return exp
  end
  def self.level
    return 'Level'
  end  
  def self.level_a
    return 'Lv'
  end 
  def self.hp
    return 'HP'
  end  
  def self.hp_a
    return hp
  end
  def self.mp
    return 'SP'
  end  
  def self.mp_a
    return mp
  end   
  def self.atk
    return 'Atk'
  end
  def self.def
    return 'Def'
  end
  def self.mat
    return 'Mat'
  end
  def self.mdf
    return 'Mdf'
  end
  def self.agi
    return 'Agi'
  end
  def self.luk
    return 'Luck'
  end  
  
  def self.rogue(rogue_id)
    $data_system.terms.rogue[rogue_id]
  end  
  def self.param(param_id)
    $data_system.terms.params[param_id]
  end
  def self.param_a(param_id)
    $data_system.terms.params_a[param_id]
  end
  def self.etype(etype_id)
    $data_system.terms.etypes[etype_id]
  end
  def self.element(element_id)
    $data_system.elements[element_id]
  end  
  
  # 基本ステータス
  def self.basic(basic_id)
    $data_system.terms.basic[basic_id]
  end
  # 能力値
  def self.param(param_id)
    $data_system.terms.params[param_id]
  end
  # 装備タイプ
  def self.etype(etype_id)
    $data_system.terms.etypes[etype_id]
  end
  # コマンド
  def self.command(command_id)
    $data_system.terms.commands[command_id]
  end
  # 通貨単位
  def self.currency_unit
    $data_system.currency_unit
  end

  #--------------------------------------------------------------------------
=begin
  def self.level;       basic(0);     end   # レベル
  def self.level_a;     basic(1);     end   # レベル (短)
  def self.hp;          basic(2);     end   # HP
  def self.hp_a;        basic(3);     end   # HP (短)
  def self.mp;          basic(4);     end   # MP
  def self.mp_a;        basic(5);     end   # MP (短)
  def self.tp;          basic(6);     end   # TP
  def self.tp_a;        basic(7);     end   # TP (短)
=end  
  def self.fight;       command(0);   end   # 戦う
  def self.escape;      command(1);   end   # 逃げる
  def self.attack;      command(2);   end   # 攻撃
  def self.guard;       command(3);   end   # 防御
  def self.item;        command(4);   end   # アイテム
  def self.skill;       command(5);   end   # スキル
  def self.equip;       command(6);   end   # 装備
  def self.status;      command(7);   end   # ステータス
  def self.formation;   command(8);   end   # 並び替え
  def self.save;        command(9);   end   # セーブ
  def self.game_end;    command(10);  end   # ゲーム終了
  def self.weapon;      command(12);  end   # 武器
  def self.armor;       command(13);  end   # 防具
  def self.key_item;    command(14);  end   # 大事なもの
  def self.equip2;      command(15);  end   # 装備変更
  def self.optimize;    command(16);  end   # 最強装備
  def self.clear;       command(17);  end   # 全て外す
  def self.new_game;    command(18);  end   # ニューゲーム
  def self.continue;    command(19);  end   # コンティニュー
  def self.shutdown;    command(20);  end   # シャットダウン
  def self.to_title;    command(21);  end   # タイトルへ
  def self.cancel;      command(22);  end   # やめる
end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
