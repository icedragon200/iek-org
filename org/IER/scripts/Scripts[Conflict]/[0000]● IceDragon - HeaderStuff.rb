# ● IceDragon - HeaderStuff
# 256.times { |i| puts sprintf( "%s : %s", i, [i].pack("I") ) }
# ☺☻♥♦♣♠•◘○◙♂♀♪♫☼►◄↕‼¶§▬↨↑↓→←∟↔▲▼⌂«»░▒▓▄█▀±¹³²■
#==============================================================================#
# ■ Script Header Information Template
#==============================================================================#
# // • Created By    : IceDragon
# // • Data Created  : 12/06/2011
# // • Data Modified : 12/06/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/06/2011 V1.0  : Started
#        Created Basic Header Info 
#==============================================================================#
#attr_accessor :name # ● Name of n
#==============================================================================#
# // [Configuration] //
#==============================================================================#
#==============================================================================#
# // [End Configuration] //
#==============================================================================#
#==============================================================================#
# ◙ Header/Constant/Property/Section
#==============================================================================#

#==============================================================================#
# ◙ Header/Constant/Property/Section
#/============================================================================\#
# ● Explanation
#   Cheesebread is delicious, so eat it.
#\============================================================================/#

#==============================================================================#
# ♥ Class
#==============================================================================#
#==============================================================================#
# ♥ Class
#/============================================================================\#
# ● Explanation
#   Cheesebread is delicious, so eat it.
#\============================================================================/#

#==============================================================================#
# ■ Module
#==============================================================================#
#==============================================================================#
# ■ Module
#/============================================================================\#
# ● Explanation
#   Cheesebread is delicious, so eat it.
#\============================================================================/#

  #--------------------------------------------------------------------------#
  # ♦ Included Module(s)
  #--------------------------------------------------------------------------#
  #--------------------------------------------------------------------------#
  # ♦ Constant(s)
  #--------------------------------------------------------------------------#
  #--------------------------------------------------------------------------#
  # ♦ Public Instance Variable(s)
  #--------------------------------------------------------------------------#

  #--------------------------------------------------------------------------#
  # ● new-method :n
  #--------------------------------------------------------------------------#
  #--------------------------------------------------------------------------#
  # ● overwrite-method :n
  #--------------------------------------------------------------------------#
  #--------------------------------------------------------------------------#
  # ● super-method :n
  #--------------------------------------------------------------------------#
  #--------------------------------------------------------------------------#
  # ► method :n
  #/------------------------------------------------------------------------\#
  # ● Parameters
  #     x          : X 
  #     y          : Y
  # ● Return
  #     String
  #\------------------------------------------------------------------------/#

  #--------------------------------------------------------------------------#
  # ♥|● new-class-method :n
  #--------------------------------------------------------------------------#
  #--------------------------------------------------------------------------#
  # ♥|● overwrite-class-method :n
  #--------------------------------------------------------------------------#
  #--------------------------------------------------------------------------#
  # ♥|► class-method :n
  #/------------------------------------------------------------------------\#
  # ● Parameters
  #     x          : X 
  #     y          : Y
  # ● Return
  #     String
  #\------------------------------------------------------------------------/#

  #--------------------------------------------------------------------------#
  # ■|● new-module-method :n
  #--------------------------------------------------------------------------#
  #--------------------------------------------------------------------------#
  # ■|● overwrite-module-method :n
  #--------------------------------------------------------------------------#
  #--------------------------------------------------------------------------#
  # ■|► module-method :n
  #/------------------------------------------------------------------------\#
  # ● Parameters
  #     x          : X 
  #     y          : Y
  # ● Return
  #     String
  #\------------------------------------------------------------------------/#
 
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
