# #Looper
=begin
#==============================================================================#
# ♥ Looper
#==============================================================================#
# // • Created By    : IceDragon
# // • Data Created  : 01/08/2012
# // • Data Modified : 01/08/2012
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 01/08/2012 V1.0 
#==============================================================================#
class Looper
  # // for (i=0; i < 10; i++) 
  # // {
  # //   Do stuffs D:
  # // }
  # // Looper( proc { 0 }, proc { |i| i < 10 }, proc { |i| i.succ } ) do |i|
  # //   Do stuff >,> Little bulky but what the hell...
  # // end  
  def initialize( start_proc, break_proc, next_proc )
    @start_proc = start_proc
    @break_proc = break_proc
    @next_proc  = next_proc
  end  
  def run()
    n = @start_proc.call
    while @break_proc.call( n ) do yield n ; n = @next_proc.call( n ) end
  end  
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
=end
