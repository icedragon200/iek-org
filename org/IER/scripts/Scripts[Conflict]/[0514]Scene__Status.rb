# Scene::Status
#==============================================================================
# ■ Scene::Status
#------------------------------------------------------------------------------
# 　ステータス画面の処理を行うクラスです。
#==============================================================================
class Window::Status2 < Window::RogueStatus
  def initialize(dactor)
    super(0,0)
    activate()
    set_actor(dactor)
    refresh()
  end
  def standard_draw_mode
    2
  end  
end  
class Scene::Status < Scene::MenuBase
  #--------------------------------------------------------------------------
  # ● 開始処理
  #--------------------------------------------------------------------------
  def start
    super
    @status_window = Window::Status2.new(@actor)# Window::Status.new(@actor)
    @status_window.set_handler(:cancel,   method(:return_scene))
    @status_window.set_handler(:pagedown, method(:next_actor))
    @status_window.set_handler(:pageup,   method(:prev_actor))
    @status_window.start_open()
    add_window(@status_window)
  end
  #--------------------------------------------------------------------------
  # ● アクターの切り替え
  #--------------------------------------------------------------------------
  def on_actor_change
    @status_window.actor = @actor
    @status_window.activate
  end
end
