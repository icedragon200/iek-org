# RPG::Dungeon
# // 12/19/2011
# // 12/21/2011
class RPG::Dungeon < RPG::Map
  def initialize( *args )
    super( *args )
    @rooms = []
    @room_table     = Table.new( 1, 1 ) # [x, y] = room_id (Actual Map Size)
    @room_schematic = Table.new( 1, 1 ) # [x, y] = room_id (Scaled, used for generation)
    @room_size  = [17, 17]
    @room_count = [3, 3]
    @tiles   = {}
    @padding = [0, 0]
    @seed = rand( 1000000 )
  end 
  def seed=( nseed )
    @seed = nseed
    set_seed_rand()
  end  
  def set_seed_rand()
    @seed_rand = Random.new( @seed )
  end
  def remap_data()
    @data = Table.new( @data.xsize, @data.ysize, @data.zsize )
    for i in 0...@rooms.size
      room = @rooms[i]
      room.data.iterate3() { |t, x, y, z| 
        @data[room.x1 + x, room.y1 + y, z] = t }
    end
    return self
  end  
  def random_room()
    @rooms[@seed_rand.rand(@rooms.size)]
  end  
  def remap_rooms()
    for i in 0...@rooms.size
      @rooms[i].id = i + 1 if @rooms[i]
    end
    reposition_rooms()
    rebuild_room_table()
    return self
  end
  def reposition_rooms()
    px, py = padding[0], padding[1]
    @room_schematic = Table.new( @room_count[0], @room_count[1] )
    for i in 0...@rooms.size
      rx = (i % @room_count[0])# * @room_size[0] 
      ry = (i / @room_count[0])# * @room_size[1]
      @room_schematic[rx, ry] = i
    end  
    @room_schematic.iterate2 { |i, x, y|
      rx = x * @room_size[0]
      ry = y * @room_size[1]
      @rooms[i].set_xy( px + rx, py + ry )
      @rooms[i].remap_nodes()
    }
  end  
  def rebuild_room_table()
    @room_table = Table.new( @data.xsize, @data.ysize )
    @rooms.each { |r|
      for x in r.x1...r.x2
        for y in r.y1...r.y2
          @room_table[x, y] = r.id
        end
      end  
    }
    return self
  end
  attr_accessor :rooms
  attr_accessor :room_table
  attr_accessor :room_schematic
  attr_accessor :room_size
  attr_accessor :room_count
  attr_accessor :padding
  attr_accessor :tiles
  attr_reader :seed
end  
