# Window::ChestItems
# // 01/29/2012
# // 01/29/2012
class Window::ChestItems < Window::Selectable
  class AddonBin_WCI < Window::QuickText::AddonBin_WQT ; end
  def addon_bin
    AddonBin_WCI
  end
  def header_text()
    @header_text 
  end 
  attr_reader :items
  def initialize( x, y, items=[], text="Chest Contained" )
    @items = items
    @header_text = text
    super( x, y, window_width, window_height )
    refresh()
  end
  def window_width
    192
  end
  def window_height
    ((@items.size * item_height) + standard_padding * 2).max(48)
  end
  def update_padding_bottom
  end
  def standard_padding
    8
  end
  def col_max
    1
  end  
  def item_max
    @items.size
  end    
  def draw_item( index )
    art = open_artist()
    rect = item_rect(index)
    contents.font.set_style(:default)
    art.draw_item_name(@items[index][0], rect.x, rect.y, contents.width)
    art.draw_text(item_rect_for_text(index), sprintf("x%2d", @items[index][1]), 2)
  end  
end
