# Scene::Title
#==============================================================================#
# ♥ Scene::Title
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/07/2011
# // • Data Modified : 12/07/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/07/2011 V1.0 
#
#==============================================================================#
# // 03/03/2012 [ 
class Sprite::Title < Sprite
  def initialize(viewport=nil)
    super(viewport)
    self.bitmap = Cache.system("EarthenTitle")
    self.salign(1,-1)
  end  
end  
class Container::About < Container::Console
  def update()
    super()
    call_handler(:cancel) if(Input.mtrigger?(:B))
  end  
end  
# // 03/03/2012 ]
class Scene::Title < Scene::Base
  #--------------------------------------------------------------------------#
  # ● super-method :start
  #--------------------------------------------------------------------------#
  def start()
    super()
    SceneManager.clear()
    create_background()
    create_all_windows()
    create_title_sprite()
    #@title_sprite.wave_amp    = 4
    #@title_sprite.wave_length = 256
    #@title_sprite.wave_speed  = 720
  end
  def create_all_windows()
    create_command_window()
    create_tail_window()
  end  
  #--------------------------------------------------------------------------#
  # ● new-method :create_command_window
  #--------------------------------------------------------------------------#
  def create_command_window()
    @command_window = Window::TitleCommand.new()
    @command_window.set_handler( :new_game   , method(:command_new_game) )
    @command_window.set_handler( :continue   , method(:command_continue) )
    @command_window.set_handler( :shutdown   , method(:command_shutdown) )
    @command_window.set_handler( :mapeditor  , method(:command_mapeditor) )
    @command_window.set_handler( :bestiary   , method(:command_bestiary) )
    @command_window.set_handler( :traptionary, method(:command_traptionary) )
    @command_window.set_handler( :itemary    , method(:command_itemary) )
    @command_window.set_handler( :skillary   , method(:command_skillary) )
    @command_window.set_handler( :about      , method(:command_about) ) # //02/19/2012
    add_window( @command_window )
  end  
  def create_tail_window()
    @tail_window = Container::Console.new(0,0,Graphics.width,24)
    @tail_window.salign(-1,2)
    @tail_window.redraw_background(0)
    @tail_window.draw_text(@tail_window.contents.rect,"Earthen V0.11",2)
    add_window( @tail_window )
  end
  def create_info_window()
    @info_window = Container::About.new(0,0,Graphics.width*0.95,Graphics.height*0.9)
    @info_window.salign(1,1)
    rect = Rect.new(0,0,@info_window.width,@info_window.line_height)
    (["Earthen: Dawn Of Smiths V0.11",:line]+Devi.list_la).each_with_index do |s,i|
      if(s == :line)
        @info_window.draw_horz_line(rect.vy2)
        rect.y += 2
      else
        rect.y += rect.height
        @info_window.draw_text(rect,s,1)
      end  
    end
    @info_window.z = 999
    @info_window.start_open()
    @info_window.set_handler(:cancel, method(:on_info_cancel))
    @info_window.activate()
    add_window( @info_window )
  end 
  def create_title_sprite()
    @title_sprite = Sprite::Title.new(@viewport)
  end
  def command_about()
    create_info_window()
    @command_window.hide.deactivate()
  end  
  def on_info_cancel()
    dispose_info_window() 
    @command_window.show.activate()
  end  
  #--------------------------------------------------------------------------#
  # ● super-method :terminate
  #--------------------------------------------------------------------------#
  def terminate()
    super()
    @title_sprite.dispose()
    dispose_background()
  end  
  def dispose_info_window()
    close_and_remove_window(@info_window)
  end  
  def update_basic()
    super()
    update_background() if @background_sprite
  end  
  #--------------------------------------------------------------------------#
  # ● super-method :update
  #--------------------------------------------------------------------------#
  def update()
    super()
    @title_sprite.update()
  end
  #--------------------------------------------------------------------------#
  # ● new-method :command_new_game
  #--------------------------------------------------------------------------#
  def command_new_game()
    $game_temp.other[0] = nil
    #SceneManager.goto( Scene::Map )
    SceneManager.goto( Scene::PreNewGame )
    SceneManager.call( Scene::Save )
  end  
  #--------------------------------------------------------------------------#
  # ● new-method :command_continue
  #--------------------------------------------------------------------------#
  def command_continue()
    SceneManager.call( Scene::Load )
  end  
  #--------------------------------------------------------------------------#
  # ● new-method :command_shutdown
  #--------------------------------------------------------------------------#
  def command_shutdown()
    SceneManager.exit()
  end 
  #--------------------------------------------------------------------------#
  # ● new-method :command_mapeditor
  #--------------------------------------------------------------------------#
  def command_mapeditor()
    SceneManager.call( Scene::MapEditor )
  end
  def command_bestiary()
    SceneManager.call( Scene::Bestiary )
  end  
  def command_traptionary()
    SceneManager.call( Scene::Traptionary )
  end  
  def command_itemary()
    SceneManager.call( Scene::Itemary )
  end  
  def command_skillary()
    SceneManager.call( Scene::Skillary )
  end  
end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
