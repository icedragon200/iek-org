# [IEI]NoteEval
Devi.add("NoteEval").set(
  :enabled => true,
  :name          => 'IEI::NoteEval',
  :author        => 'IceDragon',
  :date_created  => Devi::Date.new(4,28,2012),
  :date_modified => Devi::Date.new(4,30,2012),
  :version       => '0.10'
)
class RPG::BaseItem  
  def note_eval
    get_note_folders(IEI::Core.mk_notefolder_tags("note[_ ]?eval")).each{|a|eval(a.join("\n"))}
  end 
end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
