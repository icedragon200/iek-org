# Sprite::RogueItem
#==============================================================================#
# ♥ Sprite::RogueCharacter
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/06/2011
# // • Data Modified : 12/27/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/06/2011 V1.0 
#
#==============================================================================#
class Sprite::RogueItem < Sprite
  attr_reader :character
  def initialize( viewport, character )
    super( viewport )
    @character = character
    update()
  end  
  def update
    super()
    if @character
      update_bitmap
      update_position
      update_other
    end  
  end
  def update_bitmap
    if graphic_changed?
      @icon_index = @character.icon_index 
      set_icon_bitmap()
    end
  end
  def update_position()
    self.x = @character.screen_x
    self.y = @character.screen_y
    self.z = @character.screen_z
  end  
  def update_other
    self.opacity = @character.opacity
    self.visible = !@character.transparent
  end  
  def graphic_changed?
    @icon_index != @character.icon_index 
  end
  def set_icon_bitmap
    self.bitmap = Cache.system("Iconset")
    @cw = 24
    @ch = 24
    self.src_rect.set((@icon_index % 16) * @cw,(@icon_index / 16) * @cw,@cw,@ch)
    self.ox = @cw / 2
    self.oy = @ch
  end
  def remove?
    @character.remove_sprite?
  end  
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
