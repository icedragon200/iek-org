# Window::Files
# // 12/??/2011
class Window::Files < Window::Selectable
  class AddonBin_WF < WindowAddons::AddonBin[WindowAddons::Header,WindowAddons::ScrollBar]
    def header_text
      return "File"
    end
  end  
  def addon_bin
    AddonBin_WF
  end 
  def initialize( x, y )
    super( x, y, window_width, window_height )
    activate()
    refresh()
    self.index = 0
  end  
  def window_width()
    (item_width * 2) + standard_padding * 2
  end  
  def window_height()
    3 * item_height + standard_padding * 2
  end 
  def item_max
    DataManager.savefile_max
  end
  def col_max 
    2
  end
  def item_width
    256 + 4
  end  
  def item_height
    96 + 4
  end 
  def spacing
    0
  end  
  def draw_item( index )
    rect    = item_rect( index ).contract( 2 )
    brect   = Rect.new( 0, 0, 256, 96 )
    header  = DataManager.load_header(index)
    bi      = (header ? header[:save_background_index] : 0) || 0
    brect.y = bi * brect.height
    trect   = rect.dup
    trect.x+= 30
    open_artist(true) do |art|
      bmp = Cache.system("Save_Borders(Window)")
      contents.blt( rect.x, rect.y, bmp, brect, header ? 255 : art.translucent_alpha )
      contents.font.set_style( :window_header )
      art.draw_text( trect.x, trect.y, trect.width, 14, "File: #{index+1}")
      art.draw_party_characters(rect.x+42+16, rect.y+48+32, index )
      art.draw_playtime( rect.x+28, rect.y+22, 86, 2, index )
    end  
  end 
  def standard_artist
    Artist::Files  
  end  
end
class Artist::Files < Artist
  def draw_party_characters(x, y, index)
    header = DataManager.load_header(index)
    return unless header
    header[:characters].each_with_index do |data, i|
      draw_character(data[0], data[1], x + i * 33, y)
    end
  end
  def draw_playtime(x, y, width, align, index)
    header = DataManager.load_header(index)
    return unless header
    draw_text(x, y, width, 14, header[:playtime_s], align)
  end
end  
