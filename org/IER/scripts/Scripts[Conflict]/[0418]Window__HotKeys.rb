# Window::HotKeys
#==============================================================================#
# ♥ Window::HotKeys
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/24/2011
# // • Data Modified : 12/24/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/24/2011 V1.0 
#
#==============================================================================# 
class Window::HotKeys < Window::Selectable
  attr_reader :battler
  def initialize( x=0, y=0 )
    super( x, y, window_width, window_height )
    self.index = 0
  end
  def battler=( new_battler )
    if @battler != new_battler
      @battler = new_battler
      refresh
    end
  end  
  def window_width()
    (item_width * col_max) + (standard_padding * 2) + (spacing * col_max)
  end  
  def window_height()
    (item_height * (item_max / col_max)) + (standard_padding * 2) + 24
  end  
  def item_width
    32 + 4
  end
  def item_height
    36 + 4
  end  
  def item_max
    RogueManager::HOT_KEYS.size #@hot_keys.size
  end
  def col_max
    RogueManager::HOT_KEYS.size 
  end 
  def spacing
    2
  end  
  def contents_height
    height - standard_padding * 2
  end
  def update_padding_bottom
    
  end  
  def items
    @battler ? @battler._hot_keys : []
  end  
  def redraw_key( key )
    redraw_item( items.keys.index { |a| a == key } )
  end  
  def current_item()
    items[self.index]
  end
  def refresh()
    super()
    update_help()
  end  
  def locked?( key )
    return RogueManager.locked_key?(key)
  end  
  def current_key_locked?()
    return locked?( current_item.key )
  end  
  def draw_item( index )
    art = open_artist
    rect = item_rect( index )
    wd, hg = rect.width - 32, rect.height - 36
    rect.x      += wd / 2
    rect.y      += hg / 2
    rect.width  -= wd
    rect.height -= hg
    key = @battler._hot_keys[index]#.key
    bmp = Cache.system( locked?( key.key ) ? "HotKey_Border(Window)(Locked)" : "HotKey_Border(Window)" )
    contents.blt( rect.x, rect.y, bmp, bmp.rect )
    contents.font.set_style( :simple_black )
    art.draw_item_icon( key.object, rect.x+((rect.width-24)/2), rect.y+1 )
    art.draw_text( rect.x, rect.y+22, rect.width, 16, key.key, 1)
  end  
  def item_enabled?( item )
    return true
  end
  def update()
    super()
  end  
  def call_update_help
    update_help if active 
  end
  def update_help
    @artist.draw_item_help( 0, contents.height - 24, current_item() )
  end
  def select_available(item=nil)
    self.index = @battler._hot_keys.to_a.index(){|k|k.obj == nil}
  end  
  def equip_current(item)
    Sound.play_equip
    @battler._hot_keys[self.index].set_obj(item)
    refresh4change()
  end  
  def unequip_current()
    equip_current(nil)
  end 
  def refresh4change()
    refresh()
  end 
  def standard_artist
    Artist::HotKeys
  end  
end  
class Artist::HotKeys < Artist
  def draw_item_help( x, y, item )
    change_color( Color::Black )
    contents.font.size = Font.default_size
    contents.clear_rect( x, y, contents.width, 24 )
    return unless item
    obj = item.object
    dx = x
    dy = y
    draw_item_icon( obj, dx, dy )
    dx += 24
    contents.font.size = Font.default_size - 6
    draw_box( dx, dy+12, contents.width, 12 )
    draw_text( dx, dy, contents.width-(dx), 12, obj ? obj.name : "" )
    dy += 12
    contents.font.size = Font.default_size - 8
    draw_text( dx+4, dy, contents.width-(dx+4), 12, obj ? obj.description : "" )
  end 
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
