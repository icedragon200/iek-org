# Sprite::MouseCursor
# // 01/25/2012
# // 01/25/2012
if defined? Mouse
class << Mouse
  alias :pre_cursor_sprite_init :init
  def init(*args)
    pre_cursor_sprite_init(*args)
    dispose_cursor_sprite()
    create_cursor_sprite()
  end  
  def create_cursor_sprite()
    @cursor_sprite = Sprite::MouseCursor.new()
  end  
  def dispose_cursor_sprite
    @cursor_sprite.dispose unless @cursor_sprite.disposed? if @cursor_sprite
    @cursor_sprite = nil
  end
  def update_cursor_sprite
    @cursor_sprite.update if @cursor_sprite
  end  
  def cursor_sprite()
    @cursor_sprite
  end  
end  
class Sprite::MouseCursor < Sprite
  attr_reader :cursor_index
  def initialize(viewport=nil)
    super(viewport)
    self.bitmap = Cache.system("MouseCursors")
    self.cursor_index = 0
  end
  def dispose
    dispose_bitmap()
    super()
  end  
  def cursor_index=(n)
    @cursor_index = n
    self.src_rect.set(0,32*@cursor_index, 32, 32)
  end  
  def update()
    super()
    self.x = MouseEx.rx
    self.y = MouseEx.ry
    self.z = MouseEx.z
  end  
end  
end
