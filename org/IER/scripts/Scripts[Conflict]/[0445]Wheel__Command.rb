# Wheel::Command
#==============================================================================#
# ♥ Wheel::Command
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/11/2011
# // • Data Modified : 12/11/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/11/2011 V1.0 
#
#==============================================================================#
class Sprite::WheelBackground < Sprite
  def scale=( new_scale )
    if @scale != new_scale
      @scale = new_scale
      refresh_scale()
    end
  end 
  def refresh_scale()
    sc = @scale / 1.0
    self.zoom_x = self.zoom_y = sc
  end
end     
class Wheel::Command < Wheel::Base
  attr_accessor :windex
  def initialize( x, y, viewport=nil )
    super( x, y )
    @index    = 0
    @windex   = 0
    @index_tweener = Tween.new(0,0,:back_out,Tween.frames_to_tt(wheel_time))
    @sprites = []
    @viewport = nil
    @list     = []
    @handler  = {}
    @angle    = []
    @background_sprite = Sprite::WheelBackground.new()
    @background_sprite.bitmap = Cache.system( "CommandWheel_Background" )
    @background_sprite.ox = (@background_sprite.width-32) / 2
    @background_sprite.oy = (@background_sprite.height-32) / 2
    @cursor   = Sprite.new()
    @cursor.ox, @cursor.oy = -(32 - 24) / 2, -(32 - 24) / 2
    @cursor.bitmap = Bitmap.new( 24, 24 )
    @cursor.bitmap.ext_draw_box2( 
      {
        :x             => 0,
        :y             => 0,
        :width         => @cursor.bitmap.width,
        :height        => @cursor.bitmap.height,
        :padding       => 2,
        :footer_height => 4
      } 
    )
    @text_window = Window::SmallText.new( x, y, 56, 32 )
    @text_window.contents.font.set_style( :default )
    @wheel_direction = :static
    self.radius  = @close_radius
    self.visible = false
    self.active  = false
    self.x       = self.x
    self.y       = self.y
    self.z       = self.z
    
    update_sprites()
  end  
  def each_sprite()
    (@sprites + [@cursor, @background_sprite]).each { |spr| yield spr }
  end  
  def dispose()
    @cursor.bitmap.dispose()
    dispose_sprites()
    @cursor = nil ; @sprites.clear()
    super()
  end
  def dispose_sprites()
    each_sprite { |spr| spr.dispose() }
  end  
  def dispose_icon_sprites()
    @sprites.each { |spr| spr.dispose() }
  end 
  def select(index)
    self.index = index if index
  end
  def unselect
    self.index = -1
  end 
  def refresh_index()
    super()
    if @index_tweener.end_values[0] != @index
      @index_tweener.set_and_reset(self.windex,@index,
        :back_out,Tween.frames_to_tt(windex_time))
    end
    call_update_help
  end  
  def refresh_radius()
    super()
    refresh_xyz()
  end  
  def refresh_x()
    super()
    @text_window.x = self.vx + 16 + ((self.width - @text_window.width) / 2)
    @background_sprite.x = self.x
  end
  def refresh_y()
    super()
    @text_window.y = self.vy - radius + ((self.height - @text_window.height) / 2)
    @background_sprite.y = self.y
  end
  def refresh_z()
    super()
    @text_window.z = self.z + 3
    @background_sprite.z = self.z
    @cursor.z = self.z + 1
    @sprites.each { |spr| spr.z = self.z + 2 }
  end  
  def refresh_xyz()
    super()
    update_sprite_position()
  end  
  def refresh_visible()
    super()
    each_sprite { |spr| spr.visible = self.visible }
    @text_window.visible = self.visible
  end  
  def refresh_active()
    super()
    @text_window.active = self.active
    @text_window.update_padding()
  end  
  def refresh_viewport()
    super()
    each_sprite { |spr| spr.viewport = self.viewport }
    @text_window.viewport = self.viewport
  end  
  def add_command( icon, name, symbol, enabled = true, ext = nil )
    @list.push( 
      { 
        :icon => icon, 
        :name => name, 
        :symbol => symbol,
        :enabled => enabled, 
        :ext => ext
      }
    )
  end
  def command_name(index)
    @list[index][:name]
  end
  def command_enabled?(index)
    @list[index][:enabled]
  end
  def current_data
    index >= 0 ? @list[index] : nil
  end
  def current_item_enabled?
    current_data ? current_data[:enabled] : false
  end
  def current_symbol
    current_data ? current_data[:symbol] : nil
  end
  def select_symbol(symbol)
    @list.each_index {|i| select(i) if @list[i][:symbol] == symbol }
  end
  def select_ext(ext)
    @list.each_index {|i| select(i) if @list[i][:ext] == ext }
  end
  def set_handler(symbol, method)
    @handler[symbol] = method
  end
  def handle?(symbol)
    @handler.include?(symbol)
  end
  def call_handler(symbol)
    @handler[symbol].call if handle?(symbol)
  end 
  def refresh()
    super()
    dispose_icon_sprites()
    @sprites.clear()
    @angle.clear()
    for i in 0...item_max
      command = @list[i]
      @sprites[i] = Sprite::Icon.new( @viewport, command[:icon] )
      @sprites[i].ox = @cursor.ox
      @sprites[i].oy = @cursor.oy
      @angle[i] = 0
    end  
    @wheel_direction = :static
    update_sprites()
    refresh_state()
  end
  def item_max
    @list.size
  end  
  def get_item_xy( index )
    return @circle.get_angle_xy( -index * (wheel_slice / [item_max, 1].max) )
  end  
  def update()
    super()
    update_open if @opening
    update_close if @closing
    @index_tweener.update
    @windex = @index_tweener.value
    @text_window.update
    process_cursor_move()
    process_handling()
    update_sprites()
  end
  def update_sprite_position()
    unless @sprites.empty?()
      ang = wheel_slice / item_max
      for i in 0...item_max
        sprite = @sprites[i]
        target_angle = (((i - self.windex)) * ang) #% 360
        #@angle[i] = target_angle
        if target_angle > @angle[i]
          @angle[i] = (@angle[i] + (wheel_slice / wheel_time)).min(target_angle) 
        elsif target_angle < @angle[i]
          @angle[i] = (@angle[i] - (wheel_slice / wheel_time)).max(target_angle) 
        end  
        sprite.x, sprite.y = *@circle.get_angle_xy( -@angle[i] )
      end
    end  
    @cursor.x, @cursor.y = *get_item_xy( 0 )
    @background_sprite.scale = self.radius / open_radius.to_f
  end  
  def update_sprites()
    update_sprite_position()  
    each_sprite { |spr| spr.update }
  end  
  def process_cursor_move()
    return unless cursor_movable?
    last_index = self.index
    wheel_up() if Input.repeat?( :UP )
    wheel_down() if Input.repeat?( :DOWN )
    wheel_left() if Input.repeat?( :LEFT )
    wheel_right() if Input.repeat?( :RIGHT )
    Sound.play_cursor() if self.index != last_index
  end  
  def process_handling()
    return unless active
    return process_ok()       if ok_enabled?()        && Input.trigger?( :C )
    return process_cancel()   if cancel_enabled?()    && Input.mtrigger?( :B )
  end
  def wheel_ok()
    process_ok() if ok_enabled?()
  end
  def wheel_cancel()
    process_cancel() if cancel_enabled?()
  end  
  def cursor_movable?()
    active && item_max > 0
  end
  def ok_enabled?
    return true
  end
  def process_ok
    if current_item_enabled?()
      Sound.play_ok
      Input.update
      deactivate
      call_ok_handler
    else
      Sound.play_buzzer
    end
  end
  def call_ok_handler
    if handle?(current_symbol)
      call_handler(current_symbol)
    elsif handle?(:ok)
      call_handler(:ok)
    else
      activate
    end
  end
  def cancel_enabled?
    handle?(:cancel)
  end
  def process_cancel
    Sound.play_cancel
    Input.update
    deactivate
    call_cancel_handler
  end
  def call_cancel_handler
    call_handler(:cancel)
  end
  def wheel_up()
  end
  def wheel_down()
  end  
  def wheel_left( wrap = false )
    self.index = wrap ? (self.index - 1) % item_max : [(self.index - 1), 0].max
    @wheel_direction = :left
  end
  def wheel_right( wrap = false )
    self.index = wrap ? (self.index + 1) % item_max : [(self.index + 1), item_max-1].min
    @wheel_direction = :right
  end
  def call_update_help
    update_help if active #&& @help_window
  end
  def update_help
    @text_window.set_text( command_name( index ) ) if item_max > 0
    #@help_window.clear
  end
end 
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
