# Sprite::MoveCursor
# // 02/14/2012
# // 02/18/2012
class Sprite::MoveCursor < Sprite
  def initialize(viewport=nil)
    super(viewport)
    self.z       = 1
    self.bitmap  = Cache.character("!$MapCursors")
    @sequencer   = IEI::Sequencer.new([0,1,2,1], 5)
    #self.oy = 32
    @tx, @ty     = -1, -1
    update_src_rect()
    @target_tone = Pallete[:sys_red].to_tone() # // 02/19/2012
    @main_tone   = Pallete[:black].to_tone() 
    @tone_rate   = 0
  end  
  def _map
    $game_system._map
  end   
  def fadein()
    self.opacity += 255 / 10
  end
  def fadeout()
    self.opacity -= 255 / 30
  end
  def tone_rate_in
    20.0
  end  
  def tone_rate_out
    20.0
  end
  def update()
    super()
    update_src_rect()
    update_position()
    # // 02/19/2012
    unless $game_system.move_target.disabled?
      @last_tone_rate = @tone_rate
      if _map.impassable?(@tx, @ty)
        @tone_rate = (@tone_rate+(255/tone_rate_in())).clamp(0,255) if @tone_rate < 255
      else
        @tone_rate = (@tone_rate-(255/tone_rate_out())).clamp(0,255) if @tone_rate > 0
      end  
      self.tone.set(*@main_tone.transition_to(@target_tone,@tone_rate/255.0).to_a_na) if @last_tone_rate != @tone_rate
    end  
  end
  def update_src_rect()
    @sequencer.update()  
    self.src_rect.set(@sequencer.value*32,32,32,32)
  end  
  def update_position
    if $game_system.move_target.disabled?
      fadeout()
    else  
      @tx, @ty = $game_system.move_target.to_a 
      fadein()
    end  
    self.x = _map.adjust_x(@tx) * 32 #+ 16
    self.y = _map.adjust_y(@ty) * 32 #+ 32
  end  
end 
