# Mixin::ColorExpansion
#==============================================================================#
# ■ Mixin::Color
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/09/2011
# // • Data Modified : 12/19/2011
# // • Version       : 1.1
#==============================================================================#
# ● Change Log
#     ♣ 12/09/2011 V1.0 
#         Added (Methods)
#           darken( rate )
#           lighten( rate )
#
#     ♣ 12/09/2011 V1.0 
#         Added (Methods)
#           add( rate )
#           subtract( rate )
#           average( rate )
#           add_with( op_color )
#           subtract_with( op_color )
#           average_with( op_color )
#           darken_with( op_color )
#           lighten_with( op_color )
#
#     ♣ 12/19/2011 V1.0 
#         Added
#           to_flash()
#
#     ♣ 12/26/2011 V1.0 
#         Added (Methods)
#           to_a()
#           add!( rate )
#           subtract!( rate )
#           average!( rate )
#           darken!( rate )
#           lighten!( rate )
#           add_with!( op_color )
#           subtract_with!( op_color )
#           average_with!( op_color )
#           darken_with!( op_color )
#           lighten_with!( op_color )
#
#     ♣ 12/29/2011 V1.0 
#         Added (Methods)
#           transition_to( color, rate )
#           transition_to!( color, rate )
#           set_red( n )
#           set_green( n )
#           set_blue( n )
#           xset(:alpha=> n )
#==============================================================================#
module Mixin::ColorExpansion
  #--------------------------------------------------------------------------#
  # ♦ Constant(s)
  #--------------------------------------------------------------------------# 
  RED_SYM      = :red
  GREEN_SYM    = :green
  BLUE_SYM     = :blue
  ALPHA_SYM    = :alpha
  RED_SYM_S    = :red=
  GREEN_SYM_S  = :green=
  BLUE_SYM_S   = :blue=
  ALPHA_SYM_S  = :alpha=
  RGB_SYM      = [RED_SYM, GREEN_SYM, BLUE_SYM]
  RGBA_SYM     = RGB_SYM + [ALPHA_SYM]
  UP_LIMIT     = 255
  DOWN_LIMIT   = 0
  RESULT_OBJ = Color
  #--------------------------------------------------------------------------#
  # ● new-method :xset
  #/------------------------------------------------------------------------\#
  # ● Parameters
  #     n (Hash(key[:blue,:red,:green,:alpha],value(Integer)))
  # ● Return
  #     self (Instance)
  #\------------------------------------------------------------------------/#  
  def xset(hsh)
    self.red   = hsh[:red]   || self.red
    self.green = hsh[:green] || self.green
    self.blue  = hsh[:blue]  || self.blue
    self.send(alpha_sym_s,hsh[alpha_sym] || self.send(alpha_sym))
    self
  end  
  #--------------------------------------------------------------------------#
  # ● new-method :add
  #/------------------------------------------------------------------------\#
  # ● Parameters
  #     rate (Float)
  # ● Return
  #     Color
  #\------------------------------------------------------------------------/#  
  def add( rate=0.1 )
    col = _result_obj.new( *(rgb_sym.collect { |sym| c=self.send(sym);c+(c*rate) }) )
    col.xset(:alpha=> self.send(alpha_sym) )
    col
  end
  #--------------------------------------------------------------------------#
  # ● new-method :subtract
  #/------------------------------------------------------------------------\#
  # ● Parameters
  #     rate (Float)
  # ● Return
  #     Color
  #\------------------------------------------------------------------------/#
  def subtract( rate=0.1 )
    add( -rate )
  end  
  #--------------------------------------------------------------------------#
  # ● new-method :average
  #/------------------------------------------------------------------------\#
  # ● Parameters
  #     rate (Float)
  # ● Return
  #     Color
  #\------------------------------------------------------------------------/#
  def average( rate=0.1 )
    avg = (to_a_na.inject(0) { |r, i| r + i }) / 3.0
    col = _result_obj.new(*(to_a_na.collect { |c| c + (avg * rate) }))
    col.xset(:alpha=> self.send(alpha_sym) )
    col
  end
  #--------------------------------------------------------------------------#
  # ● new-method :lighten
  #/------------------------------------------------------------------------\#
  # ● Parameters
  #     rate (Float)
  # ● Return
  #     Color
  #\------------------------------------------------------------------------/#
  def lighten( rate=0.1 )
    col = _result_obj.new(*(rgb_sym.collect { |sym| c=self.send(sym);c+(255*rate) }))
    col.xset(:alpha=> self.send(alpha_sym) )
    col
  end
  #--------------------------------------------------------------------------#
  # ● new-method :darken
  #/------------------------------------------------------------------------\#
  # ● Parameters
  #     rate (Float)
  # ● Return
  #     Color
  #\------------------------------------------------------------------------/#
  def darken( rate=0.1 )
    lighten( -rate )
  end
=begin  
  #--------------------------------------------------------------------------#
  # ● new-method :add_with
  #/------------------------------------------------------------------------\#
  # ● Parameters
  #     op_color (Color) 
  # ● Return
  #     Color
  #\------------------------------------------------------------------------/#
  def add_with( op_color )
    _result_obj.new( *rgba_sym.collect { |c| 
      (self.send(c) + op_color.send(c)).clamp( _down_limit, _up_limit ) } ) 
  end  
  #--------------------------------------------------------------------------#
  # ● new-method :subtract_with
  #/------------------------------------------------------------------------\#
  # ● Parameters
  #     op_color (Color) 
  # ● Return
  #     Color
  #\------------------------------------------------------------------------/#
  def subtract_with( op_color )
    _result_obj.new( *rgba_sym.collect { |c| 
      (self.send(c) - op_color.send(c)).clamp( _down_limit, _up_limit ) } ) 
  end
  #--------------------------------------------------------------------------#
  # ● new-method :average_with
  #/------------------------------------------------------------------------\#
  # ● Parameters
  #     op_color (Color) 
  # ● Return
  #     Color
  #\------------------------------------------------------------------------/#    
  def average_with( op_color )
    return _result_obj.new( *rgba_sym.collect { |c| 
      (self.send(c) + op_color.send(c)) / 2 } ) 
  end 
  #--------------------------------------------------------------------------#
  # ● new-method :lighten_with
  #/------------------------------------------------------------------------\#
  # ● Parameters
  #     op_color (Color) 
  # ● Return
  #     Color
  #\------------------------------------------------------------------------/#    
  def lighten_with( op_color )
    rate = op_color.send(alpha_sym)
    return _result_obj.new( *rgb_sym.collect { |c| 
      sc = self.send(c)
      sc + (sc * op_color.send(c) / rate) } ) 
  end
  #--------------------------------------------------------------------------#
  # ● new-method :subtract_with
  #/------------------------------------------------------------------------\#
  # ● Parameters
  #     op_color (Color) 
  # ● Return
  #     Color
  #\------------------------------------------------------------------------/#    
  def darken_with( op_color )
    rate = op_color.send(alpha_sym)
    return _result_obj.new( *rgb_sym.collect { |c| 
      sc = self.send(c)
      sc - (sc * op_color.send(c) / rate) } 
    ) 
  end 
=end    
  #--------------------------------------------------------------------------#
  # ● new-method :add!
  #/------------------------------------------------------------------------\#
  # ● Parameters
  #     rate (Float)
  #\------------------------------------------------------------------------/#
  def add!( rate=0.1 )
    set( *add( rate ).to_a )
  end  
  #--------------------------------------------------------------------------#
  # ● new-method :subtract!
  #/------------------------------------------------------------------------\#
  # ● Parameters
  #     rate (Float)
  #\------------------------------------------------------------------------/#
  def subtract!( rate=0.1 )
    set( *subtract( rate ).to_a )
  end  
  #--------------------------------------------------------------------------#
  # ● new-method :average!
  #/------------------------------------------------------------------------\#
  # ● Parameters
  #     rate (Float)
  #\------------------------------------------------------------------------/#
  def average!( rate=0.1 )
    set( *average( rate ).to_a )
  end  
  #--------------------------------------------------------------------------#
  # ● new-method :darken!
  #/------------------------------------------------------------------------\#
  # ● Parameters
  #     rate (Float)
  #\------------------------------------------------------------------------/#
  def darken!( rate=0.1 )
    set( *darken( rate ).to_a )
  end  
  #--------------------------------------------------------------------------#
  # ● new-method :lighten!
  #/------------------------------------------------------------------------\#
  # ● Parameters
  #     rate (Float)
  #\------------------------------------------------------------------------/#
  def lighten!( rate=0.1 )
    set( *lighten( rate ).to_a )
  end 
=begin  
  #--------------------------------------------------------------------------#
  # ● new-method :add_with!
  #/------------------------------------------------------------------------\#
  # ● Parameters
  #     op_color (Color)
  #\------------------------------------------------------------------------/#
  def add_with!( op_color )
    set( *add_with( op_color ).to_a )
  end  
  #--------------------------------------------------------------------------#
  # ● new-method :subtract_with!
  #/------------------------------------------------------------------------\#
  # ● Parameters
  #     op_color (Color)
  #\------------------------------------------------------------------------/#
  def subtract_with!( op_color )
    set( *subtract_with( op_color ).to_a )
  end  
  #--------------------------------------------------------------------------#
  # ● new-method :average_with!
  #/------------------------------------------------------------------------\#
  # ● Parameters
  #     op_color (Color)
  #\------------------------------------------------------------------------/#
  def average_with!( op_color )
    set( *average_with( op_color ).to_a )
  end  
  #--------------------------------------------------------------------------#
  # ● new-method :darken_with!
  #/------------------------------------------------------------------------\#
  # ● Parameters
  #     op_color (Color)
  #\------------------------------------------------------------------------/#
  def darken_with!( op_color )
    set( *darken_with( op_color ).to_a )
  end 
  #--------------------------------------------------------------------------#
  # ● new-method :lighten_with!
  #/------------------------------------------------------------------------\#
  # ● Parameters
  #     op_color (Color)
  #\------------------------------------------------------------------------/#
  def lighten_with!( op_color )
    set( *lighten_with( op_color ).to_a )
  end 
  # // 02/27/2012 [
  def desaturate!(rate=1.0)
    cs = to_a_na
    avg = cs.inject(0){|r,i|r+i}/cs.size
    set(*cs.collect{|c|c-((c-avg)*rate)})
    self
  end  
  def desaturate(rate=1.0)
    dup.desaturate!( rate )
  end 
  def multiply_with!(op_color)
    set(*rgb_sym.collect{|sym|self.send(sym)*op_color.send(sym)/255.0})
    self
  end  
  def multiply_with(op_color)
    dup.multiply_with!(op_color)
  end  
=end  
  # // 02/27/2012 ]
  #--------------------------------------------------------------------------#
  # ● new-method :transition_to
  #/------------------------------------------------------------------------\#
  # ● Parameters
  #     color (Color)
  #     rate (Float)
  # ● Return
  #     Color
  #\------------------------------------------------------------------------/#
  def transition_to( color, rate )
    s, t, r = self.to_a, color.to_a, self.to_a # // Self, Target, Result
    for i in 0...s.size
      r[i] = (r[i]-((s[i]-t[i])*rate)).clamp(s[i].min(t[i]),s[i].max(t[i]))
    end
    return _result_obj.new(*r)
  end  
  #--------------------------------------------------------------------------#
  # ● new-method :transition_to!
  #/------------------------------------------------------------------------\#
  # ● Parameters
  #     color (Color)
  #     rate (Float)
  #\------------------------------------------------------------------------/#
  def transition_to!( color, rate )
    set( *transition_to( color, rate ).to_a )
  end  
  #--------------------------------------------------------------------------#
  # ● new-method :to_flash
  #/------------------------------------------------------------------------\#
  # ● Return
  #     Integer
  #\------------------------------------------------------------------------/#
  def to_flash()
    r,g,b = *rgb_sym.collect{|sym|(15*self.send(sym)/255.0).to_i}
    return b+(g<<4)+(r<<8)
    #return ("0x"+r.round.to_s(16)+g.round.to_s(16)+b.round.to_s(16)).hex
  end 
  #--------------------------------------------------------------------------#
  # ● new-method :to_a
  #/------------------------------------------------------------------------\#
  # ● Return
  #     Array[red, green, blue, alpha]
  #\------------------------------------------------------------------------/#
  def to_a()
    return rgba_sym.collect { |sym| self.send(sym) }
  end  
  #--------------------------------------------------------------------------#
  # ● new-method :to_a_na
  #/------------------------------------------------------------------------\#
  # ● Return
  #     Array[red, green, blue]
  #\------------------------------------------------------------------------/#
  def to_a_na()
    return rgb_sym.collect { |sym| self.send(sym) }
  end 
  #--------------------------------------------------------------------------#
  # ● new-method :to_hex
  #/------------------------------------------------------------------------\#
  # ● Return
  #     String(hex)
  #\------------------------------------------------------------------------/#
  def to_hex()
    to_a.collect{|i|"%02x"%i}.join('')
  end
  INC_EVAL = %Q(
  # // Get
  def red_sym       ; RED_SYM      ; end
  def green_sym     ; GREEN_SYM    ; end
  def blue_sym      ; BLUE_SYM     ; end  
  def alpha_sym     ; ALPHA_SYM    ; end 
  # // Set
  def red_sym_s     ; RED_SYM_S    ; end 
  def green_sym_s   ; GREEN_SYM_S  ; end 
  def blue_sym_s    ; BLUE_SYM_S   ; end  
  def alpha_sym_s   ; ALPHA_SYM_S  ; end  
  # // RGB Symbol Array  
  def rgb_sym       ; RGB_SYM      ; end  
  # // RGBA Symbol Array  
  def rgba_sym      ; RGBA_SYM     ; end
  # // Calculation Upper Limit    
  def _up_limit     ; UP_LIMIT     ; end
  # // Calculation Lower Limit  
  def _down_limit   ; DOWN_LIMIT   ; end
  # // Result object after calculations
  def _result_obj ; RESULT_OBJ ; end
  private :red_sym  , :blue_sym   , :green_sym  , :alpha_sym
  private :red_sym_s, :blue_sym_s , :green_sym_s, :alpha_sym_s
  private :rgb_sym  , :rgba_sym 
  private :_up_limit, :_down_limit, :_result_obj 
  alias inst_hash hash
  def hash;self.to_a.hash;end
  )
end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
