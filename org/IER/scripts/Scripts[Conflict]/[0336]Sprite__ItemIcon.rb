# Sprite::ItemIcon
# // 02/24/2012
# // 02/24/2012
class Sprite::ItemIcon < Sprite::Icon
  def initialize(viewport=nil, item=nil)
    super(viewport, 0)
    self.item = item
  end  
  def iconset()
    Cache.system(@item ? @item.iconset_name : "Iconset")
  end
  attr_reader :item
  def item=(n)
    return if @item == n
    @item = n
    self.icon_index = @item ? @item.icon_index : 0
  end
  def force_item(item)
    @item = item
    @icon_index = item.icon_index
    self.bitmap = iconset
    update_icon()
  end  
end  
