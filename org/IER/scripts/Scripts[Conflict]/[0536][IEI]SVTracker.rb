# [IEI]SVTracker
# // SV Tracker
# // Created By IceDragon
# // 03/30/2012
# // 03/30/2012
# // Icy Engine Iliks
# // Heaven knows, I could have meta programmed this...
class Game::Variables
  INIT_VALUES = {
    12 => 8
  }
  ALERT_PROCS = {
    # // variable_id => proc
  }
  alias iei_svt_initiailize initialize
  def initialize(*args,&block)
    iei_svt_initiailize(*args,&block)
    init_values()
  end  
  def init_values()
    INIT_VALUES.each_pair { |key,value| @data[key] = value }
  end  
  alias set_var :[]=
  def []=(id,value)
    ALERT_PROCS[id].call(id,value) if(ALERT_PROCS.has_key?(id)) if(@data[id]!=value)
    set_var(id,value)
  end
end
class Game::Switches
  ALERT_PROCS = {
    # // switch_id => proc
  }
  alias set_switch :[]=
  def []=(id,bool)
    ALERT_PROCS[id].call(id,bool) if(ALERT_PROCS.has_key?(id)) if(@data[id]!=bool)
    set_switch(id,bool)
  end
end
