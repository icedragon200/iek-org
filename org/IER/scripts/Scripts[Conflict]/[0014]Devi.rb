# Devi
# // 02/19/2012
# // 02/19/2012
# // . x. Replacement for ScriptManager
# // Version History
# // 0.1a - Started Script & Finished Main Idea
module Devi
  @headers = {}
  def self.warn?
    true
  end  
  def self.script?( name )
    return @headers.has_key?( name )
  end
  def self.add( name )
    warn "Script #{name} already installed" if @headers.has_key?(name) if warn?
    n = @headers[name] = ::Devi::Header.new(:name=>name)
    n
  end
  def self.get(name)
    @headers[name]
  end 
  def self.enabled?(name)
    script?(name) ? @headers[name].enabled? : false
  end  
  def self.disabled?(name)
    !enabled?(name)
  end  
  def self.sorted_headers_e # // Sorted Headers - Enabled
    @headers.keys.sort.select{|k|@headers[k].enabled?}.collect{|k|@headers[k]}
  end  
  def self.list_sa # // Sorted List of all enabled Script Header's short_s
    sorted_headers_e.collect{|v|v.short_s}
  end  
  def self.list_la # // Sorted List of all enabled Script Header's long_s
    sorted_headers_e.collect{|v|v.long_s}
  end 
  def self.import!()
    @headers.each_pair { |key,value| ($imported||={})[key] = value.enabled? }
  end  
end 
# // :3 Cheap Date class
class Devi::Date
  #           // dd/mm/yyyy
  DATE_FORMAT = '%1$02d/%2$02d/%3$04d'
  attr_accessor :day, :month, :year
  def initialize(d=1,m=1,y=1) # // >_> Like hell you could have a script that old
    @day = d
    @month = m
    @year = y
    # // >_> Now why the hell did I do that for >________>
  end  
  def to_s
    DATE_FORMAT % to_a
  end  
  def to_a
    [@day, @month, @year]
  end  
end  
# // All yer important info's and stuffz
class Devi::Header
  attr_accessor :version, :author, :enabled, :name
  attr_accessor :date_modified, :date_created
  def initialize(info={})
    set(info)
  end  
  def enabled?
    return @enabled == true
  end  
  def set(info={})
    @name          = info[:name] || ""
    @version       = info[:version] || ""
    @author        = info[:author] || ""
    @enabled       = info[:enabled].nil? ? true : info[:enabled]
    @date_created  = info[:date_created] || Devi::Date.new(1,1,1)
    @date_modified = info[:date_modified] || Devi::Date.new(1,1,1)
    self
  end  
  def short_s
    format("%020s - V%04s : %010s", @name, @version, @author)
  end  
  def long_s
    format("%020s - V%04s : %010s |DC[%s]|DM[%s]", @name, @version, @author, @date_created.to_s, @date_modified.to_s)
  end  
end  
# // Example + Actual Usage O: 
# // . x . You'll be placing this at the top of your scripts
# // @_@ But since Devi has to be evaluated first XD mine is at the bottom
Devi.add("Devi").set(
  # // User Config
  :enabled => true,
  # // Script Main Header
  :name          => 'Devi::ScriptManager',
  :author        => 'IceDragon',
  :date_created  => Devi::Date.new(2,19,2012),
  # // Changing Stuff, Stuff that changes frequently
  :version       => '0.10',
  :date_modified => Devi::Date.new(2,19,2012)
)
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
