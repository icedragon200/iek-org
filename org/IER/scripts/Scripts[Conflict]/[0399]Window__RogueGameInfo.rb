# Window::RogueGameInfo
# // 02/09/2012
# // 02/09/2012
class Window::RogueGameInfo < Window::Base
  def initialize(x,y,w,h)
    super(x,y,w,h)
    @drects = {}
    @drects[:playtime] = Rect.new(0,0,192,line_height)
    @drects[:floor]    = Rect.new(0,line_height*1,192,line_height)
    draw_floor
    refresh()
  end
  def line_height
    20
  end  
  def _map
    $game_rogue
  end  
  attr_accessor :update_handle
  def refresh
    contents.clear
    draw_playtime()
    draw_floor()
  end  
  def playtime()
    $game_system.playtime_s
  end  
  def playtime_rect()
    @drects[:playtime]
  end  
  def draw_playtime()
    open_artist.draw_text(rect=playtime_rect, "Playtime: "+(@playtime=playtime));rect
  end 
  def redraw_playtime()
    contents.clear_rect(playtime_rect)
    draw_playtime()
  end  
  def draw_floor()
    open_artist.draw_text_ex(*((rect=@drects[:floor]).xto_a(:x,:y)+[(_map.floor_s)]));rect
  end  
  def update()
    super
    redraw_playtime if(playtime) != @playtime
    call_update_handle()
  end  
  def call_update_handle
    @update_handle.call(self) if(@update_handle)
  end  
end  
