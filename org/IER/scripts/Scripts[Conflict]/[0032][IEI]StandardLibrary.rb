# [IEI]StandardLibrary
#==============================================================================#
# ♥ IEI - Standard Library
#==============================================================================#
# // • Created By    : IceDragon
# // • Data Created  : 12/06/2011
# // • Data Modified : 04/12/2012
# // • Version       : 1.0
#==============================================================================#
# ● Functions
#==============================================================================#
# ♥ - Class
# ■ - Module
#==============================================================================#
# ♥ Object
#     if_eql?(comp_obj,alt) { alt }
#     if_neql?(comp_obj,alt) { alt }
#     if_nil?(comp_obj,alt) { alt }
#     to_bool()
# ■ Kernel
#     load_data_cin(filename) { save_obj }
# ♥ Numeric
#     min(n)
#     max(n)
#     minmax(m,mx)
#     clamp(min,max)
#     pole()
#     pole_inv()
# ♥ Array
#     pick()
#     pick!()
#     pad()
#     pad!()
#     remove_this(obj,n=1)
#     uniq_arrays(group)
#     uniq_arrays!(group)
# ♥ Hash
#    get_values(*keys)
#==============================================================================#
# ● Change Log
#     ♣ 04/08/2012 V1.0 
#         Compiled all seperate scripts into 1
#==============================================================================#
if(HOME_LOAD)
  require 'StandardLibEx/Object_Ex'
  require 'StandardLibEx/Kernel_Ex'
  require 'StandardLibEx/Numeric_Ex'
  require 'StandardLibEx/Array_Ex'
  require 'StandardLibEx/Hash_Ex' 
else
  load_skpck('StandardLibEx')
  load_skrips(self.send(:binding),Skrip.get_skpck('StandardLibEx'))
end    
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
