# Geometry
#==============================================================================#
# ♥ Geometry
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/10/2011
# // • Data Modified : 01/28/2012
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/10/2011 V1.0 
#     ♣ 01/16/2012 V1.0 
#         Renamed script (Oval & Circle) to Shapes
#     ♣ 01/20/2012 V1.0 
#         Added
#           Polygon
#             .new(x, y, w, h, sides=6)
#             side_angle_size()
#             get_side_xy1(side=0) 
#             get_side_xy2(side=0, n=1)
#     ♣ 01/24/2012 V1.0 
#         Renamed script (Shapes) to Geometry
#     ♣ 01/24/2012 V1.0 
#         Added (Circle)
#           calc_angle( x2, y2 )
#==============================================================================#
class Oval
  include Mixin::Surface
  include Mixin::RectExpansion
  include RectOnly
  attr_accessor :cx,:cy
  attr_accessor :radius_x,:radius_y
  attr_accessor :angle_offset
  def initialize( x=0, y=0, radius_x=0, radius_y=0 )
    @cx, @cy, @radius_x, @radius_y = x, y, radius_x, radius_y 
    @angle_offset = 0 #90
  end
  def ao
    self.angle_offset
  end  
  def ao=(n)
    self.angle_offset = n
  end  
  def x
    cx - radius_x
  end  
  def y
    cy - radius_y
  end
  def x=(n)
    self.cx = n + radius_x
  end
  def y=(n)
    self.cy = n + radius_y
  end  
  def diameter_x
    @radius_x * 2
  end 
  def diameter_y
    @radius_y * 2
  end 
  alias rdx radius_x
  alias rdy radius_y
  def get_angle_xy( angle )
    opangle = Math::PI * (angle - @angle_offset) / 180.0
    return @cx + @radius_x * Math.cos(opangle), @cy + @radius_y * Math.sin(opangle)
  end  
  def get_xy_angle( x, y )
    calc_angle(x, y)
  end  
  def calc_angle(x2, y2)
    dx, dy  = Vector.xy_dist_from(x2, y2, @cx, @cy)
    m = dx.abs.max(dy.abs).max(1).to_f
    180 + (Math.atan2((dy)/m, (dx)/m) / Math::PI) * -180 rescue 0
  end
  def calc_circ_dist(x2, y2)
    dx, dy = Vector.xy_dist_from(@cx, @cy, x2, y2)
    m = dx.abs.max(dy.abs).max(1).to_f
    (dx * Math.cos(Math::PI * dx / m)).abs + (dy * Math.cos(Math::PI * dy / m)).abs
  end  
  def in_circ_area?(x,y)
    calc_circ_dist(x,y).abs <= calc_circ_dist(*get_angle_xy(calc_angle(x,y))).abs
  end 
  def set(*args)
    if args[0].is_a?(Oval)
      o = args[0]
      self.x, self.y, self.radius_x, self.radius_y = o.x, o.y, o.rdx, o.rdy
    else  
      self.x, self.y = args[0] || self.x, args[1] || self.y
      self.radius_x, self.radius_y = args[2] || self.rdx, args[3] || self.rdy
    end
    self
  end  
  def empty
    set( 0, 0, 0, 0 )
  end  
  def width
    @radius_x * 2
  end
  def height
    @radius_y * 2
  end  
  def width=(n)
    self.radius_x = n / 2
  end  
  def height=(n)
    self.radius_y = n / 2
  end  
  def cyc_360_angle(n=0,clockwise=true)
    clockwise ? n.next.modulo(360) : n.pred.modulo(360)
  end  
  def cycle_360(n=nil,clockwise=true)
    i = 0
    if n
      n.times { |c| yield i, c ; i = cyc_360_angle(i,clockwise) }
    else
      loop do 
        yield i ; i = cyc_360_angle(i,clockwise)
      end  
    end
  end
  def circumfrence
    2 * Math::PI * ((radius_x + radius_y) / 2)
  end  
end  
class Circle < Oval
  attr_reader :radius
  def initialize( x=0, y=0, radius=0 )
    super( x, y, 0, 0 )
    self.radius = radius 
  end 
  def diameter
    @radius * 2
  end  
  def radius=( n )
    self.radius_x = self.radius_y = @radius = n
  end
  def set( *args )
    args.push(args[2]) if args.size == 3
    super( *args )
  end
end
class Polygon < Oval
  attr_accessor :sides
  def initialize(sides, x=0, y=0, w=0, h=0)
    super(x, y, w, h)
    @sides = sides
    @angle_offset = 180
  end  
  def side_angle_size()
    360.0 / @sides
  end  
  def get_side_xy1(side=0) # // Exact Point
    get_angle_xy(side_angle_size*side)
  end  
  def get_side_xy2(side=0, n=1) # // Line Point
    get_angle_xy((side_angle_size*side)+(side_angle_size/2*n))
  end 
  alias :get_side_xy :get_side_xy1
  def next_side(n=0)
    n.next.modulo(@sides)
  end  
  def prev_side(n=0)
    n.pred.modulo(@sides)
  end 
  def cyc_side(n=0,aclockwise=false)
    aclockwise ? next_side(n) : prev_side(n)
  end  
  def cycle_sides(start_side=0, aclockwise=false, n=nil)
    i = start_side
    if n
      n.times { |c| yield i, c ; i = cyc_side(i,aclockwise) }
    else
      loop do 
        yield i ; i = cyc_side(i,aclockwise)
      end  
    end  
  end 
end  
class Square < Polygon
  def initialize( x, y, length )
    super(4,x,y,length,length)
  end
  def length
    return self.width
  end  
  def length=(n)
    self.width = self.height = n
  end  
  def set( *args )
    args.push(args[2]) if args.size == 3
    super( *args )
  end  
end  
class Cube
  attr_accessor :x, :y, :z
  attr_accessor :width, :length, :height
  def initialize(*args)
    set(*args)
  end
  def set(x=0,y=0,z=0,width=0,height=0,length=0)
    self.x, self.y, self.z = x, y, z
    self.width, self.length, self.height = width, length, height
    self
  end 
  def set2(x=0,y=0,z=0,width=0,length=0,height=0)
    self.x, self.y, self.z = x, y, z
    self.width, self.length, self.height = width, length, height
    self
  end 
  def xset(x=nil,y=nil,z=nil,w=nil,h=nil,l=nil)
    x,y,z,w,h,l = x.get_values(:x,:y,:Z,:width,:height,:length) if(x.is_a?(Hash))
    set(x||self.x,y||self.y,z||self.z,w||self.width,h||self.height,l||self.length)
    self
  end  
  def xto_a(*args,&block)
    return args.collect{|sym|self.send(sym)}
  end  
  def to_a
    return self.x, self.y, self.z, self.width, self.height, self.length
  end  
  def empty()
    set()
    self
  end  
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
