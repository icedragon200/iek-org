# Container::Console
# // 02/19/2012
# // 02/19/2012
class Container::Console < Container::Window
  include Container::Addons::HandlerBase
  include Container::Addons::Background_Simple
  def line_height
    contents.font.size + 2
  end 
  def line_color
    contents.font.color
  end  
  def create_contents()
    super()
    contents.font.set_style(:console1)
  end  
  def draw_sbackground
    redraw_background(1)
  end  
  def redraw_background(n=0)
    @sbackground_sprite.bitmap.clear()
    case n
    when 0
      @sbackground_sprite.bitmap.draw_console_tail()
    when 1  
      @sbackground_sprite.bitmap.draw_console_style_back(2)
    end  
  end  
  def standard_padding
    4
  end 
end  
class Bitmap
  def draw_console_style_back(border_size=1)
    drect = self.rect.clone
    fill_rect(drect, Color::ConsoleText)
    fill_rect(drect.contract(border_size), Color::ConsoleBack)
  end  
  def draw_console_tail
    drect = self.rect.clone
    drect2 = drect.clone
    drect2.height /= 4
    drect.squeeze!(drect2.height,false,2)
    gradient_fill_rect(drect2, Color::ConsoleBack.dup.xset(:alpha=>0), Color::ConsoleBack, true)
    fill_rect(drect, Color::ConsoleBack)
  end  
end  
