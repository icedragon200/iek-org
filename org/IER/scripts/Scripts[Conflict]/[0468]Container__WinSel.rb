# Container::WinSel
# // 02/07/2012
# // 02/07/2012  
class Container::WinSel < Container::Window
  include Container::Addons::SelectableBase
  include Container::Addons::CursorBase
  include Container::Addons::EaserCursor
  def initialize(*args,&block)
    super(*args,&block)
    select(0)
  end  
end  
