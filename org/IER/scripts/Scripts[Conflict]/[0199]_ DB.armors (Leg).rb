#  | DB.armors (Leg)
# // 02/29/2012
# // 02/29/2012
module Database
def self.mk_armors14()
  armors = []
  armor_sym = :leg
  equip_sym = :leg
#==============================================================================#
# ◙ Armor (Leg)(Leather Sandals)
#==============================================================================# 
  armor = Armor.new()
  armor.initialize_add()
  armor.id           = 1
  armor.name         = "Sandals"
  armor.icon_index   = 0
  armor.description  = 'Light weight sandals'
  armor.features     = []
  armor.note         = ""
  armor.etype_id     = 0 
  armor.params       = [0,0,0,1,0,0,1,0]
  armor.atype_id     = 0
  armor.features << MkFeature.def_r(1.02)
  armor.features << MkFeature.mdf_r(1.01)
  armor.features << MkFeature.agi_r(1.03)
  armors[armor.id] = armor  
#==============================================================================#
# ◙ Armor (Leg)(Leather Shoes)
#==============================================================================# 
  armor = Armor.new()
  armor.initialize_add()
  armor.id           = 2
  armor.name         = "Leather Shoes"
  armor.icon_index   = 0
  armor.description  = 'Durable, but not the best'
  armor.features     = []
  armor.note         = ""
  armor.etype_id     = 0 
  armor.params       = [0,0,0,2,0,1,2,0]
  armor.atype_id     = 0
  armor.features << MkFeature.def_r(1.03)
  armor.features << MkFeature.mdf_r(1.02)
  armor.features << MkFeature.agi_r(1.04)
  armors[armor.id] = armor   
#==============================================================================#
# ◙ REMAP
#==============================================================================# 
  adjust_armors(armors,armor_sym,equip_sym)   
end
end
