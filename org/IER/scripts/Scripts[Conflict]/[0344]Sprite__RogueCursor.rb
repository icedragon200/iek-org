# Sprite::RogueCursor
#==============================================================================#
# ♥ Sprite::RogueCursor
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/18/2011
# // • Data Modified : 12/18/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/18/2011 V1.0 
#
#==============================================================================#
class Sprite::RogueCursor < Sprite
  include Mixin::Sprite_Effect
  def initialize( viewport, cursor )
    super( viewport )
    @character = cursor
    @effect_duration = 0
    update
  end
  def update()
    super()
    self.x = @character.screen_x 
    self.y = @character.screen_y
    self.z = @character.screen_z
    self.visible = @character.visible #|| (@character_visible && self.opacity > 0)
    self.opacity = @character.opacity
    update_bitmap()
    setup_new_effect()
    update_effect()
  end  
  def update_bitmap
    if graphic_changed?
      @bitmap_name  = @character.bitmap_name
      @cursor_index = @character.cursor_index
      self.bitmap = Cache.system( @bitmap_name )
      self.src_rect.set( 0, 32 * @cursor_index, 32, 32 )
      self.ox = 32 / 2
      self.oy = 32
    end
  end
  def graphic_changed?
    return @bitmap_name != @character.bitmap_name ||
          @cursor_index != @character.cursor_index
  end
  def setup_new_effect
    if @character.sprite_effect_type
      start_effect(@character.sprite_effect_type)
      @character.set_sprite_effect_type( nil ) 
    end
  end  
  def revert_to_normal
    self.blend_type = 0
    self.color.set(0, 0, 0, 0)
    self.opacity = 255
    #self.ox = @cw / 2
    #self.oy = @ch
    #self.src_rect.y = 0
  end
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
