# Window::CraftInput
class Window::CraftInput < Window::Selectable
  class AddonBin_WCRI < WindowAddons::AddonBin[WindowAddons::Header]
    def header_text
      "Craft::Input"
    end
  end  
  def addon_bin
    AddonBin_WCRI
  end
  attr_reader :items
  def initialize( x, y )
    super( x, y, window_width, window_height )
    self.windowskin = Cache.system("Window_Craft")
    activate()
    @craft_block_type = [1, 0, 0, 2, 2, 3]
    @items = Array.new(6, nil)
    refresh()
    self.index = 0
  end  
  def item_width 
    36
  end 
  def item_height
    36
  end 
  def window_width
    (col_max * (item_width+spacing)) + standard_padding * 2
  end  
  def window_height
    (row_max * item_height) + standard_padding * 2
  end  
  def col_max
    item_max
  end  
  def item_max
    6
  end  
  def spacing 
    2
  end  
  def set_current_item(n)
    @items[@index] = n
    redraw_current_item()
  end  
  def current_item
    @items[@index]
  end  
  def item
    current_item
  end  
  def draw_item( index )
    rect = item_rect( index ).contract( 2 )
    bmp = Cache.system( "Craft_Borders(Window)" )
    item = @items[index]
    contents.blt( rect.x, rect.y, bmp, Rect.new( 0, 32*@craft_block_type[index], 32, 32 ) )
    return unless(item)
    @artist.draw_item_icon(item, rect.x+((32-24)/2), rect.y+((32-24)/2))
  end  
  def update_help
    @help_window.set_item(item)
  end
end  
