# Ruby Stuff!
# // OS Send
#puts `Cheese`
# // Heredoc
#doc = <<ENDDOC
#  Yup stuff like this :3
#ENDDOC
#puts doc
#doc = <<'ENDDOC'
#  Yup stuff like this :3
#ENDDOC
#puts doc
#for (a, b, c, d) in [[1, 2, 3, 4]*8]
#  puts a, b, c, d
#end  
#`cd #{Dir.pwd}`
#`dir`
#`mkdir ..\\Data(Backup)`
#%x[copy /Y ..\Data\ ..\Data(Backup)\ ]
# 
#puts $*
#def +@(val)
#  val+1
#end
#def -@(val)
#  val-1
#end  
# 1.+@() => 1
# 1.-@() => -1
#def cheese!(n)
#  n += 1
#rescue => ex
#  puts "Cheese Exploded..."
#else  
#  puts "Sending your cheese #{n} + 1"
#  n + 1
#ensure
#  puts "We require more Cheese..."
#  n = 0
#end  
#module Cheese
#  def cheese
#    "MORE CHEESE O:"
#  end  
#end  
#class SwissCheese
#  include Cheese
#end  
#class EuropeanCheese
#  extend Cheese
#end  
#puts SwissCheese.new.cheese
#puts EuropeanCheese.cheese
#puts cheese!(1)
#puts cheese!(nil)
#ObjectSpace.each_object(Class) { |o|
#  puts "Class: " + o.name 
#  puts o.ancestors
#}
# // Other way to make procs
# some_proc = -> { "Hello" }
# some_proc.call()
