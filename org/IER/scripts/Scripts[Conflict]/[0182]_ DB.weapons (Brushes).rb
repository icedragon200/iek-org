#  | DB.weapons (Brushes)
# // 02/29/2012
# // 02/29/2012
module Database
def self.mk_weapons2()
  weapons = []
  wep_sym = :brushes
#==============================================================================#
# ◙ Weapon (Brush)(Rumi)
#==============================================================================# 
  weapon              = Weapon.new()
  weapon.initialize_add()
  weapon.id           = 1
  weapon.name         = "Rumi"
  weapon.icon_index   = 0
  weapon.description  = 'A small artist brush'
  weapon.features     = []
  weapon.note         = ""
  weapon.price        = 0
  weapon.etype_id     = 0 
  weapon.params       = params_from_growth(:base_brushes)
  weapon.wtype_id     = 0
  weapon.animation_id = 0
  weapon.growths      = [:normal, :brushes] 
  weapons[weapon.id] = weapon   
#==============================================================================#
# ◙ Weapon (Brush)(Ruubri)
#==============================================================================# 
  weapon              = Weapon.new()
  weapon.initialize_add()
  weapon.id           = 2
  weapon.name         = "Ruubri"
  weapon.icon_index   = 0
  weapon.description  = ''
  weapon.features     = []
  weapon.note         = ""
  weapon.price        = 0
  weapon.etype_id     = 0 
  weapon.params       = params_from_growth(:base_brushes)
  weapon.wtype_id     = 0
  weapon.animation_id = 0
  weapon.growths      = [:magic, :brushes] 
  weapons[weapon.id] = weapon    
#==============================================================================#
# ◙ Weapon (Brush)(Sharoom)
#==============================================================================# 
  weapon              = Weapon.new()
  weapon.initialize_add()
  weapon.id           = 3
  weapon.name         = "Sharoom"
  weapon.icon_index   = 0
  weapon.description  = ''
  weapon.features     = []
  weapon.note         = ""
  weapon.price        = 0
  weapon.etype_id     = 0 
  weapon.params       = params_from_growth(:base_brushes)
  weapon.wtype_id     = 0
  weapon.animation_id = 0
  weapon.growths      = [:swift, :brushes] 
  weapons[weapon.id] = weapon    
#==============================================================================#
# ◙ Weapon (Brush)(Yeerust)
#==============================================================================# 
  weapon              = Weapon.new()
  weapon.initialize_add()
  weapon.id           = 4
  weapon.name         = "Yeerust"
  weapon.icon_index   = 0
  weapon.description  = ''
  weapon.features     = []
  weapon.note         = ""
  weapon.price        = 0
  weapon.etype_id     = 0 
  weapon.params       = params_from_growth(:base_brushes)
  weapon.wtype_id     = 0
  weapon.animation_id = 0
  weapon.growths      = [:heavy, :brushes] 
  weapons[weapon.id] = weapon 
#==============================================================================#
# ◙ Weapon (Brush)(Swush)
#==============================================================================# 
  weapon              = Weapon.new()
  weapon.initialize_add()
  weapon.id           = 5
  weapon.name         = "Swush"
  weapon.icon_index   = 0
  weapon.description  = ''
  weapon.features     = []
  weapon.note         = ""
  weapon.price        = 0
  weapon.etype_id     = 0 
  weapon.params       = params_from_growth(:base_brushes)
  weapon.wtype_id     = 0
  weapon.animation_id = 0
  weapon.growths      = [:dual, :brushes] 
  weapons[weapon.id] = weapon     
#==============================================================================#
# ◙ REMAP
#==============================================================================# 
  adjust_weapons(weapons,wep_sym)  
end  
end  
