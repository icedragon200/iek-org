# Game::RogueItem
#==============================================================================#
# ♥ Game::RogueItem
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/27/2011
# // • Data Modified : 12/27/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/27/2011 V1.0 
#
#==============================================================================# 
class Game::RogueItem < Game::Character
  attr_reader :name
  attr_reader :icon_index
  attr_reader :item_id
  attr_reader :item_symbol
  attr_accessor :item_number
  def initialize( x, y, item ) 
    super()
    moveto( x, y )
    @item_id     = item ? item.id : nil
    @item_symbol = item_symbol( item.class )
    @item_number = 1
    refresh_item()
    self.remove_this = true if item.nil?()
    @priority_type = 0
  end  
  def _map() 
    return $game_rogue
  end  
  def item_symbol( item_class )
    return :item   if item_class == RPG::Item
    return :weapon if item_class == RPG::Weapon
    return :armor  if item_class == RPG::Armor
    return :nil
  end
  def item()
    case @item_symbol
    when :item
      return $data_items[@item_id]
    when :weapon
      return $data_weapons[@item_id]
    when :armor
      return $data_armors[@item_id]
    else
      return nil
    end  
  end  
  def refresh_item()
    ite = item
    @icon_index = ite ? ite.icon_index : 0
    @name = ite ? ite.name : ""
  end  
  def screen_z
    return super + 2
  end
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
