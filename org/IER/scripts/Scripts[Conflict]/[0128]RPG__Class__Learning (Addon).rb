# RPG::Class::Learning (Addon)
class RPG::Class::Learning
  def set( level=@level, skill_id=@skill_id, note=@note )
    @level    = level
    @skill_id = skill_id
    @note     = note
    self
  end  
end  
