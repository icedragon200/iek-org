# Sprite::PopText
# // 01/31/2012
# // 01/31/2012
module Mixin::Font
  def set(*args)
    case args[0]
    when Hash
      hsh = args[0]
      nm, sz, cl, oc = hsh[:name], hsh[:size], hsh[:color], hsh[:out_color]
      o, b, i, s     = hsh[:outline], hsh[:bold], hsh[:italic], hsh[:shadow]
    when Font, Pop_Font
      fn = args[0]
      nm, sz, cl, oc = fn.name, fn.size, fn.color, fn.out_color
      o, b, i, s     = fn.outline, fn.bold, fn.italic, fn.shadow
      fn = nil
    else
      nm, sz, cl, oc, o, b, i, s = *args
    end  
    self.name      = (nm || Font.default_name).clone
    self.size      = (sz || Font.default_size)
    self.color     = Color.new(*(cl || Font.default_color).to_a)
    self.out_color = Color.new(*(oc || Font.default_out_color).to_a)
    self.outline   = (o.if_nil?() { Font.default_outline }).to_bool
    self.bold      = (b.if_nil?() { Font.default_bold }).to_bool
    self.italic    = (i.if_nil?() { Font.default_italic }).to_bool
    self.shadow    = (s.if_nil?() { Font.default_shadow }).to_bool
    self
  end
  def to_font
    f = Font.new
    f.set(self)
    f
  end
  def to_pfont
    Pop_Font.new(self)
  end  
end 
class Pop_Font
  include Mixin::FontStyles
  include Mixin::Font
  attr_accessor :name
  attr_accessor :size
  attr_accessor :color
  attr_accessor :out_color
  attr_accessor :outline
  attr_accessor :bold
  attr_accessor :italic
  attr_accessor :shadow
  def self.font2pfont(font)
    font.is_a?(self) ? font : self.new(font)
  end  
  def initialize(*args)
    set(*args)
  end
end  
class Font
  include Mixin::Font
  def to_font
    return self.clone
  end  
end  
class Pop
  def self.mk_hnd()
    Handler::Pop.new(Struct_Pop.new)
  end  
end  
class Struct_Pop
  attr_accessor :text
  attr_accessor :font
  def initialize(*args)
    set(*args)
  end  
  def set(text="", font=Font.new)
    @text = text
    @font = Pop_Font.font2pfont(font)
  end  
end  
class Handler::Pop
  attr_accessor :x, :y, :z
  attr_accessor :opacity
  attr_accessor :visible
  attr_reader :ax, :ay, :az
  attr_reader :pop
  include Physics
  def initialize(pop, x=0, y=0, z=999)
    @pop = pop
    @x, @y, @z = x, y, z
    @tweener = Tween::Seqeuncer.new()
    @tweener.add_tween( 
      [-32,0,0],
      [0,0,255],
      :elastic_out,
      Tween.frames_to_tt(20)
    )
    @tweener.add_tween( 
      [0,0,255],
      [16,0,255],
      :sine_in,
      Tween.frames_to_tt(40)
    )
    @tweener.add_tween( 
      [16,0,255],
      [16,32,0],
      :sine_in,
      Tween.frames_to_tt(20)
    )
    reset()
  end 
  def current_wait_count()
    t  = @tweener.tweeners.inject(0.0){|r,t|r+t.time}
    mt = @tweener.tweeners.inject(0.0){|r,t|r+t.maxtime}
    Tween.tt2f(mt-t)
  end  
  def done?
    @tweener.done?
  end    
  def reset
    @ax, @ay, @az = 0, 0, 0
    @asx, @asy = 0, 0
    @visible = true
    @opacity = 255
    @disposed = false
    @tweener.reset()
  end  
  def text
    @pop.text
  end  
  def font
    @pop.font
  end  
  def _map
    $game_system._map
  end 
  def real_x
    @x + @ax
  end
  def real_y
    @y + @ay
  end 
  def real_z
    @z + @az
  end 
  def screen_x
    _map.adjust_x(real_x) * 32 + 16 + @asx
  end
  def screen_y
    _map.adjust_y(real_y) * 32 + 32 + @asy
  end  
  def screen_z
    real_z
  end  
  def disposed?
    @disposed == true
  end  
  def dispose 
    @disposed = true
  end  
  def update
    return if done?
    @tweener.update
    @asx, @asy = *@tweener.values[0..1]
    @opacity = @tweener.value(2)
  end  
end  
class Sprite::PopText < Sprite
  include Mixin::DrawTextEx
  attr_reader :handler
  attr_reader :current_wait_count
  def initialize(viewport, handler)
    super(viewport)
    self.handler = handler
    update
  end  
  def contents
    self.bitmap
  end 
  def handler=(hnd)
    if @handler != hnd
      @handler = hnd
      refresh()
    end  
  end  
  def refresh()
    b = Bitmap.prep_test_bmp
    b.font.set(@handler.font.to_font)
    dispose_bitmap()
    tsz = @handler.text.split(/[\n\r]+/i).collect { |t| b.text_size(strip_escape_codes(t)) }
    rw = tsz.max { |a, b| a.width <=> b.width } || Rect.new(0,0,0,0)
    rh = tsz.max { |a, b| a.height <=> b.height } || Rect.new(0,0,0,0)
    self.bitmap = Bitmap.new(rw.width+32,rh.height*tsz.size+32)
    self.bitmap.font.set(@handler.font.to_font)
    draw_text_ex(16,16,@handler.text)
    self.ox = self.width / 2
    self.oy = self.height
  end  
  def done?
    @handler.done?
  end  
  def dispose
    dispose_bitmap()
    super()
  end  
  def update
    if @handler.disposed?
      self.visible = false
      return 
    end  
    super()
    @handler.update
    self.x = @handler.screen_x
    self.y = @handler.screen_y
    self.z = @handler.screen_z
    self.opacity = @handler.opacity
    self.visible = @handler.visible
    @current_wait_count = @handler.current_wait_count
  end  
end  
=begin
#b = Sprite.new()
#b.bitmap = Cache.system("Ball")
#b.center_oxy
#b.x += 32
MouseEx.init
pops = []
add_pop = proc { 
  hnd = Pop.mk_hnd()
  hnd.pop.text = mend('\c[',14,']') + "Damage " + mend('\c[',6,']') + "13"
  s = Sprite::PopText.new(nil, hnd) 
  hnd.y += (pops.size % 4) * 24 
  s.update
  pops << s
}
add_pop.call
loop do
  #b.y = Graphics.height/2+(Graphics.height/2*Math.cos(Math::PI*Graphics.frame_count/Graphics.frame_rate.to_f))
  Graphics.update
  Input.update
  pops = pops.select { |p|
    p.handler.done? ? p.dispose : p.update
    !p.disposed?
  }
  if Input.trigger?(:C)
    add_pop.call()
  end  
end  
#Marshal.dump(Pop_Font.new(Font.new()))
=end
