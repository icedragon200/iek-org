# RPG::Rogue::Trap
# // 12/18/2011
# // 12/18/2011
class RPG::Rogue::Trap < RPG::BaseItem
  def initialize()
    super()
    @character_name  = ""
    @character_index = ""
    @level           = 1
    @max_level       = 200
    @class_id        = 1
    @item_id         = 0
    @skill_id        = 0
    @trigger_limit   = -1
    @trigger_move_route = RPG::MoveRoute.new
    @finish_move_route  = RPG::MoveRoute.new
    @recover_on_trigger = true
  end  
  attr_accessor :character_name
  attr_accessor :character_index
  attr_accessor :level
  attr_accessor :max_level
  attr_accessor :class_id
  attr_accessor :item_id
  attr_accessor :skill_id
  attr_accessor :trigger_limit
  attr_accessor :trigger_move_route
  attr_accessor :finish_move_route
  attr_accessor :recover_on_trigger
end  
