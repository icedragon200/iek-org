# RPG::ME (Addon)
class RPG::ME < RPG::AudioFile
  def play
    if @name.empty?
      Audio.me_stop
    else
      Audio.me_play('Audio/ME/' + @name, self.volume, @pitch) rescue nil
    end
  end
  def vol_rate
    $game_settings ? $game_settings.me_volume_rate : 1.0
  end 
  def volume
    return super * vol_rate
  end 
end

