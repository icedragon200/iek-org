# Wheel::Base
#==============================================================================#
# ♥ Wheel::Base
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/11/2011
# // • Data Modified : 02/01/2012
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 02/01/2012 V1.0 
#
#==============================================================================#
module Wheel ; end
class Wheel::Base
  include Mixin::Surface
  attr_accessor :circle
  def initialize( x, y )
    @circle   = Circle.new( x, y, wheel_radius )
    @circle.angle_offset = 270
    @open_radius = wheel_radius
    @close_radius = 0
    @z = 0
    ex_init()
  end    
  def ex_init()
  end  
  def x
    return @circle.cx
  end
  def y
    return @circle.cy
  end
  def x=( new_x )
    @circle.cx = new_x
    refresh_x()
  end
  def y=( new_y )
    @circle.cy = new_y
    refresh_y()
  end
  attr_reader :z
  def z=( new_z )
    @z = new_z
    refresh_z()
  end
  def radius
    return @circle.radius
  end
  attr_reader :open_radius 
  attr_reader :close_radius 
  def radius=( new_radius )
    if @circle.radius != new_radius
      @circle.radius = new_radius
      refresh_radius()
    end  
  end
  attr_reader :viewport
  def viewport=( viewport )
    @viewport = viewport
    refresh_viewport()
  end  
  attr_reader :active
  def active=( active )
    @active = active
    refresh_active()
  end
  attr_reader :visible
  def visible=( new_visible )
    @visible = new_visible
    refresh_visible()
  end  
  attr_reader :index
  def index=( index )
    @index = index
    refresh_index()
  end  
  def vx()
    @circle.vx
  end
  def vy()
    @circle.vy
  end  
  def vx=(n)
    @circle.x = n
  end
  def vy=(n)
    @circle.y = n
  end 
  def width()
    radius * 2
  end
  def height()
    radius * 2
  end
  def show
    self.visible = true
    self
  end
  def hide
    self.visible = false
    self
  end
  def activate
    self.active = true
    self
  end
  def deactivate
    self.active = false
    self
  end
  def refresh_state()
    refresh_xyz()
    refresh_viewport()
    refresh_visible()
    refresh_active()
  end
  def refresh_xyz()
    refresh_x()
    refresh_y()
    refresh_z()
  end  
  def refresh_x
  end
  def refresh_y
  end
  def refresh_z
  end
  def refresh_visible
  end
  def refresh_viewport
  end
  def refresh_active
    call_update_help()
  end
  def refresh_index
  end
  def refresh_radius
  end  
  def refresh
  end  
  def update
  end
  def disposed?
    @disposed == true
  end  
  def dispose
    @disposed = true
  end  
  include Mixin::OpenClose # // open/close
  def openness
    self.radius
  end  
  def openness=( openness )
    self.radius = openness
  end  
  def open_openness
    open_radius
  end  
  def close_openness
    close_radius
  end  
  def open_time()
    5
  end 
  def close_time()
    5
  end
  def wheel_time()
    20
  end
  def windex_time()
    10
  end
  def wheel_radius()
    48
  end  
  def wheel_slice()
    180# - 20
  end 
end 
