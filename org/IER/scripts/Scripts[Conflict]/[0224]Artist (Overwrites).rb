# Artist (Overwrites)
# // 03/05/2012
# // 03/05/2012
# // Why is this here?
# // Well I overwrote some stuff for thw Window::Base
# // Just for sorting purposes this is here
class Artist
  def draw_actor_level(actor, x, y)
    change_color(system_color)
    draw_text(x, y, 32, line_height, Vocab::level_a)
    change_color(normal_color)
    draw_text(x + 8, y, 48, line_height, actor.level, 2)
  end

  def draw_item_name(item, x, y, enabled = true, width = 172)
    return unless item
    draw_item_icon( item, x, y, enabled )
    change_color(normal_color, enabled)
    old_font = contents.font.name
    #contents.font.name = ["Microsoft YaHei"]
    draw_text(x + 24, y, width, line_height, item.name)
    contents.font.name = old_font
  end
  def draw_actor_icons( actor, x, y, width = 96 )
    windex = width / 24
    icons = (actor.state_icons + actor.buff_icons)[0, windex]
    bmp = Cache.system("Status_StateBorder(Window)")
    for i in 0...windex
      contents.blt( x + 24 * i, y, bmp, bmp.rect )
    end  
    icons.each_with_index { |n, i|
      draw_icon(n, x + 24 * i, y) 
    }
    #bmp.dispose()
  end
  def draw_face(face_name, face_index, x, y, enabled = true)
    bitmap = Cache.face(face_name)
    rect = Rect.new(face_index % 4 * 96, face_index / 4 * 96, 96, 96)
    contents.blt(x, y, bitmap, rect, enabled ? 255 : translucent_alpha)
    bitmap.dispose
    #bitmap = Cache.picture("FaceBorder")
    #contents.blt( x, y, bitmap, bitmap.rect )
    #bitmap.dispose
  end
  def draw_actor_hp( actor, x, y, width = 124 )
    draw_battler_hp( 
      :battler => actor,
      :x => x,
      :y => y,
      :width => width
    )
  end
  #--------------------------------------------------------------------------
  # ● MP の描画
  #--------------------------------------------------------------------------
  def draw_actor_mp( actor, x, y, width = 124 )
    draw_battler_mp( 
      :battler => actor,
      :x => x,
      :y => y,
      :width => width
    )
  end 
end
