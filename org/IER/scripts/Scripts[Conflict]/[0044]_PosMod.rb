# #PosMod
# // 01/30/2012
# // 02/05/2012
=begin
module PosMod
  class Base
    attr_accessor :time
    attr_reader :value
    def initialize(d)
      @time = @maxtime = d
      reset()
    end  
    def start
    end  
    def reset()
      @time = @maxtime
      start()
    end  
    def update
      if @duration > 0 
        @value = value_at_time((@maxtime-@time)/@maxtime.to_f)
        @duration -= 1
      else
        @value = value_cap
      end
    end
    def value_cap
      0.0
    end  
  end  
  class Shake < Base
    def initialize(p=0.5,s=0.5,d=60)
      @power = p
      @speed = s
      super(d)
    end
    def value_at_time(d)
      Math.cos((@power*Math::PI*d)%Math::PI)*(@power*@speed)
    end  
  end 
  class Spring < Base
    def initialize(t,d=60.0)
      @tension  = t
      super(d)
    end  
    def value_at_time(d)
      (1.0-@tension) + Math.sin(Math::PI*@tension*d)#*0.8)
    end  
    def value_cap
      1.0
    end  
  end  
end  
=end
