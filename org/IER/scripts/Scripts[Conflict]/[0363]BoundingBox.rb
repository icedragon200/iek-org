# BoundingBox
# // 02/19/2012
# // 02/19/2012
class BoundingBox
  def initialize
    @sprites = Array.new(4) { Sprite.new }
    @sprites.each_with_index { |s, i|
      s.bitmap = Cache.system("BoundingBox")
      s.src_rect.set(32*(i%2),32*(i/2),32,32)
    }
    n = 4
    @sprites[0].set_oxy(n, n)
    @sprites[1].set_oxy(-n, n)
    @sprites[2].set_oxy(n, -n)
    @sprites[3].set_oxy(-n, -n)
    self.z = 0
  end  
  attr_reader :z
  def z=(n)
    @z = n
    @sprites.each { |s| s.z = @z }
  end  
  def dispose
    @sprites.each { |s| s.dispose }
  end
  def update
    @sprites.each { |s| s.update }
  end  
  def bound(*args)
    case r=args[0]
    when Rect
      x, y, width, height = *r.to_a()
    when Array
      x, y, width, height = *r
    when Hash
      x, y, width, height = *r.get_values(:x, :y, :width, :height)
    else
      x, y, width, height = *args
    end
    x, y, width, height = *Rect.new(x,y,width,height).to_v4a()
    @sprites[0].vx  = x
    @sprites[0].vy  = y
    @sprites[1].vx2 = width
    @sprites[1].vy  = y
    @sprites[2].vx  = x
    @sprites[2].vy2 = height
    @sprites[3].vx2 = width
    @sprites[3].vy2 = height
  end  
end  
