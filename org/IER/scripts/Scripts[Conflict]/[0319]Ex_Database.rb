# Ex_Database
#==============================================================================#
# ■ Ex_Database
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 01/09/2012
# // • Data Modified : 01/09/2012
# // • Version       : 1.0
#==============================================================================#
module Ex_Database
  def self.try_ex( obj )
    return can_ex?( obj ) ? to_ex( obj ) : nil
  end  
  def self.to_ex( obj )
    return make_weapon( obj ) if obj.is_a?(RPG::Weapon)
    return make_armor( obj ) if obj.is_a?(RPG::Armor)
    item
  end  
  def self.can_ex?( obj )
    return obj.is_a?(RPG::Weapon) || obj.is_a?(RPG::Armor) || ex?( obj )
  end  
  def self.ex_item?( obj )
    false
  end  
  def self.ex_skill?( obj )
    false
  end  
  def self.ex_weapon?( obj )
    obj.is_a?(Ex_Weapon)
  end  
  def self.ex_armor?( obj ) 
    obj.is_a?(Ex_Armor)
  end  
  def self.ex_equip_item?( obj )
    obj.is_a?(Ex_EquipItem)
  end  
  def self.ex?( obj )
    return ex_weapon?( obj ) || ex_armor?( obj )
  end  
  def self.make_weapon( obj )
    return Ex_Weapon.new( obj )
  end  
  def self.make_armor( obj )
    return Ex_Armor.new( obj )
  end
  def self.item?( obj )
    return Database.item?(obj) || ex_item?(obj)
  end  
  def self.skill?( obj )
    return Database.skill?(obj) || ex_skill?(obj)
  end 
  def self.weapon?( obj )
    return Database.weapon?(obj) || ex_weapon?(obj)
  end
  def self.armor?( obj )
    return Database.armor?(obj) || ex_armor?(obj)
  end
  def self.equip_item?( obj )
    return Database.equip_item?(obj) || ex_equip_item?( obj )
  end  
  def self.obj_symbol(item)
    return :weapon if weapon?(item)
    return :armor  if armor?(item)
    return :item   if item?(item)
    return :skill  if skill?(item)
    return :nil
  end  
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
