# Mouse::OnEvent
# // 01/24/2012
# // 01/24/2012
if defined? Mouse
class Mouse::OnEvent
  def initialize
    @handler = {}
  end  
  def set_handler( symbol, method )
    @handler[symbol] = method
    self
  end
  def clear_handlers()
    @handler.clear
    self
  end  
  def remove_handler( symbol )
    @handler.delete( symbol )
    self
  end  
  def update
    button_process()
    other_process()
  end 
  def button_process
    return unless valid_buttons?
    @handler[:left_click].call   if MouseEx.left_click? if valid_left_click?
    @handler[:right_click].call  if MouseEx.right_click? if valid_right_click?
    @handler[:middle_click].call if MouseEx.middle_click? if valid_middle_click?
    @handler[:left_press].call   if MouseEx.left_press? if valid_left_press?
    @handler[:right_press].call  if MouseEx.right_press? if valid_right_press? 
    @handler[:middle_press].call if MouseEx.middle_press? if valid_middle_press?
    true
  end  
  def other_process
    @handler[:moved].call        if MouseEx.moved? if valid_moved?
    true
  end  
  def valid_buttons?
    true
  end  
  def valid?(n)
    @handler.has_key?(n)
  end  
  def valid_left_click?()
    valid?(:left_click)
  end
  def valid_right_click?()
    valid?(:right_click)
  end 
  def valid_middle_click?()
    valid?(:middle_click)
  end  
  def valid_left_press?()
    valid?(:left_press)
  end
  def valid_right_press?()
    valid?(:right_press)
  end 
  def valid_middle_press?()
    valid?(:middle_press)
  end
  def valid_moved?()
    valid?(:moved)
  end  
end 
class Mouse::AreaOnEvent < Mouse::OnEvent
  attr_accessor :area
  def initialize(v4)
    @area = v4
    @in_area = false
    super()
  end  
  def rect
    @area
  end  
  def rect=(n)
    @area = n
  end  
  def x
    @area.x
  end  
  def y
    @area.y
  end  
  def x=(n)
    @area.x = n
  end  
  def y=(n)
    @area.y = n
  end    
  def in_area?
    @in_area
  end  
  def valid_buttons?
    super && in_area?
  end  
  def update
    @in_area = MouseEx.in_area?(@area)
    super 
  end  
  def other_process()
    super()
    @handler[:over].call if in_area? if valid_over?
    @handler[:not_over].call if !in_area? if valid_not_over?
  end  
  def valid_over?()
    valid?(:over)
  end  
  def valid_not_over?()
    valid?(:not_over)
  end  
end   
end
