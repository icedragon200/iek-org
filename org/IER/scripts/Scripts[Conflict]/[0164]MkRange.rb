# MkRange
# // 02/28/2012
# // 02/28/2012
module MkRange
  module_function()
  include RPG::BaseItem::Range::Constants
  def diamond(range, min_range=0)
    result = []
    for x in (-range)..range
      for y in (-range)..range
        next if x.abs+y.abs > range
        next if x.abs+y.abs < min_range
        result << [x,y]
      end
    end  
    result.uniq 
  end  
  def square(range, min_range)
    result = []
    for x in (-range)..range
      for y in (-range)..range
        next if((x.abs < min_range) || (y.abs < min_range))
        result << [x,y]
      end
    end  
    result.uniq 
  end  
  def line(range, min_range)
    result = []
    for i in min_range..range
      result << [ i, 0]
      result << [-i, 0]
      result << [ 0, i]
      result << [ 0,-i]
    end  
    result.uniq 
  end
  def by_id(id,*params)
    case id
    when RANGE_DIAMOND 
      diamond(*params)
    when RANGE_SQUARE
      square(*params)
    when RANGE_LINE
      line(*params)
    else
      []
    end  
  end  
end
