# Crosshair
#~ class Crosshair
#~   def initialize
#~     v = Viewport.new(0,0,3,Graphics.height)
#~     @vertical_line   = [Plane.new(v),v]
#~     @vertical_line[0].bitmap = Bitmap.new(v.rect.width,v.rect.height)
#~     @vertical_line[0].bitmap.fill_rect(0,0,v.rect.width,v.rect.height,Pallete[:white])
#~     v = Viewport.new(0,0,Graphics.width,3)
#~     @horizontal_line = [Plane.new(v),v]
#~     @horizontal_line[0].bitmap = Bitmap.new(v.rect.width,v.rect.height)
#~     @horizontal_line[0].bitmap.fill_rect(0,0,v.rect.width,v.rect.height,Pallete[:white])
#~   end  
#~   def xy
#~     return @vertical_line[1].rect.x, @horizontal_line[1].rect.y
#~   end  
#~   def moveto(x,y)
#~     @vertical_line[1].rect.x = x
#~     @horizontal_line[1].rect.y = y
#~   end  
#~   def update
#~     @vertical_line[1].update
#~     @horizontal_line[1].update
#~     #@vertical_line
#~     #@horizontal_line
#~   end  
#~ end
#~ ch = Crosshair.new
#~ tweener = Tween.new([0,0],[0,0],:linear,1.0)
#~ MouseEx.init
#~ loop do
#~   Main.update
#~   tweener.update
#~   if MouseEx.right_click?
#~     tweener.set_and_reset(ch.xy,[MouseEx.x,MouseEx.y],:quad_out,Tween.f2tt(30))
#~   end
#~   ch.moveto(*tweener.values)
#~ end  
