# Window::ElementSelect
# // 02/25/2012
# // 02/25/2012
class Window::ElementSelect < Window::Selectable
  def initialize(x,y,width=window_width,height=window_height)
    super(x,y,width,height)
    @sprite_cursor = Sprite::ElementCursor.new(self.viewport)
    @sprite_cursor.size = item_width * 1.5
    @sprite_cursor.oy = @sprite_cursor.ox = item_width
    select(0)
    refresh()
    update_sprite_cursor_pos()
  end  
  def x=(n)
    super(n)
    update_sprite_cursor_pos()
  end  
  def y=(n)
    super(n)
    update_sprite_cursor_pos()
  end  
  def z=(n)
    super(n)
    update_sprite_cursor_pos()
  end  
  def viewport=(n)
    super(n)
    @sprite_cursor.viewport = self.viewport
  end  
  def index=(n)
    super(n)
    @sprite_cursor.index = self.index
  end  
  def dispose()
    @sprite_cursor.dispose()
    super()
  end  
  def update
    super()
    @sprite_cursor.update()
    update_sprite_cursor_pos()
    @sprite_cursor.visible = !cursor_rect.empty?()
    @skill_window.stype_id = current_stype if @skill_window
    @sprite_cursor.maxcount = active2fade?() ? 3 : 6
  end  
  def update_sprite_cursor_pos()
    @sprite_cursor.x, @sprite_cursor.y = cursor_rect_to_screen.xto_a(:x,:y)
    @sprite_cursor.z = self.z + 1
  end  
  def active_fading?()
    true
  end
  def active2fade?()
    (@skill_window ? @skill_window.active : true)
  end
  def window_width()
    adjust_w4window(col_max * (item_width+spacing))
  end
  def window_height()
    adjust_h4window(item_height)
  end  
  def item_width()
    28#36
  end 
  def item_height()
    28#36
  end  
  def item_max()
    6
  end
  def col_max()
    6
  end  
  def standard_spacing
    0
  end  
  def spacing()
    8
  end  
  def item_x_off 
    (self.width - (col_max*(item_width+spacing)) - spacing) / 2
  end  
  def draw_item(index)
    rect = item_rect(index)
    rect = rect.contract((rect.width-24).max(0)/2)
    open_artist.draw_element_icon(index+1,rect.x,rect.y,true,false)
  end 
  #--------------------------------------------------------------------------
  # ● スキルウィンドウの設定
  #--------------------------------------------------------------------------
  def skill_window=(skill_window)
    @skill_window = skill_window
    update
  end
  def current_stype()
    DB.element2stype(self.index+1)
  end
end  
