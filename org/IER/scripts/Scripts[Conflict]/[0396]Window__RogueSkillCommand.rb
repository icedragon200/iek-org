# Window::RogueSkillCommand
#==============================================================================#
# ♥ Window::RogueSkillCommand
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/28/2011
# // • Data Modified : 12/28/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/28/2011 V1.0
#
#==============================================================================#
class Window::RogueSkillCommand < Window::RogueObjCommand
  class AddonBin_WRSC < AddonBin_WRC
    def header_text
      return "#{Vocab.skill} Options"
    end
  end  
  def addon_bin
    AddonBin_WRSC
  end

  def make_command_list()
    add_command( "Use"  , :use, item_can?( @item, :use ) )
    add_command( "Set"  , :set, item_can?( @item, :set ) )
  end  
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
