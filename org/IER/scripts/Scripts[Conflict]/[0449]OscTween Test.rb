# OscTween Test
# // 01/26/2012
# // 01/26/2012
class Tween::Osc
  def self.run_test
    @sprite = Sprite.new(nil)
    @sprite.bitmap = Cache.system("Ball")
    @sprite.center_oxy()
    size = @sprite.ox.abs * 2
    #ax = [:back_in, :sine_out, :back_out, :bee_in]
    #ay = [:sine_in, :bee_out]+2.times.collect{Tween::EASER_SYMBOLS.keys.pick}
    #xt = [48, 68, 128, 256]
    #yt = [60, 120, 30+rand(30), 54]
    tesk = Tween::EASER_SYMBOLS.keys-[:null_in,:null_out]
    ax = tesk.shuffle[0...16] #16.times.collect{tesk.pick}
    ay = tesk.shuffle[0...16]
    xt = 16.times.collect{90+(rand(2) ? rand(60) : -rand(60))}
    yt = 16.times.collect{90+(rand(2) ? rand(60) : -rand(60))}
    @tweenerx = self.new(
      [size],[Graphics.width-size],
      ax, xt.collect { |f| Tween.frames_to_tt(f) })
    @tweenery = self.new(
      [Graphics.height-size],[size],
      ay, yt.collect { |f| Tween.frames_to_tt(f) })  
    #@tweener.set_cycles(-1)  
    loop do  
      Graphics.update
      Input.update
      @tweenerx.update
      @tweenery.update
      @sprite.x, @sprite.y = @tweenerx.value.clamp(*Graphics.rect.xw_a), @tweenery.value.clamp(*Graphics.rect.xto_a(:y,:height))
    end  
  end  
end  
#Tween::Osc.run_test()
