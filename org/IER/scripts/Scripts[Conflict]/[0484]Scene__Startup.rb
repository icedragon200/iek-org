# Scene::Startup
#==============================================================================#
# ♥ Scene::Startup
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/07/2011
# // • Data Modified : 12/07/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/07/2011 V1.0 
#
#==============================================================================#
class Scene::Startup < Scene::Base
  SPLASH = false
  #--------------------------------------------------------------------------#
  # ● overwrite-method :main
  #--------------------------------------------------------------------------#
  def main()
    SceneManager.goto( Scene::Title ) #Scene::Craft )
    $data_temp.splash_id = 0
    ($data_startup.splash_screens.size).times { |i| 
      SceneManager.call( Scene::Splash )
    } if SPLASH 
  end
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
