# Game::EquipItem
#==============================================================================#
# ♥ Game::EquipItem
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 01/09/2012
# // • Data Modified : 01/09/2012
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 01/09/2012 V1.0
#==============================================================================#
class Game::EquipItem < Game::BaseItem
  def initialize()
    super()
    @object = nil
  end
  alias :base_is_weapon? :is_weapon?
  alias :base_is_armor? :is_armor?
  def is_weapon? ; super || @class == Ex_Weapon ; end
  def is_armor?  ; super || @class == Ex_Armor  ; end
  alias :base_object :object
  def object ; return @object ; end
  def object=( object ) 
    super( object )
    @object = object
  end  
  def set_equip( *args )
    super( *args )
    if base_is_weapon?
      self.object = Ex_Database.make_weapon( base_object )
    elsif base_is_armor?
      self.object = Ex_Database.make_armor( base_object )
    else
      self.object = nil
    end  
  end  
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=# 
