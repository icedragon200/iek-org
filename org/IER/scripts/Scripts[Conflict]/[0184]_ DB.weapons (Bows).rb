#  | DB.weapons (Bows)
# // 02/29/2012
# // 02/29/2012
module Database
def self.mk_weapons4()
  weapons = []
  wep_sym = :bows
#==============================================================================#
# ◙ Weapon (Bow)(Shimis)
#==============================================================================# 
  weapon              = Weapon.new()
  weapon.initialize_add()
  weapon.id           = 1
  weapon.name         = "Shimis"
  weapon.icon_index   = 0
  weapon.description  = ''
  weapon.features     = []
  weapon.note         = ""
  weapon.price        = 0
  weapon.etype_id     = 0 
  weapon.params       = params_from_growth(:base_bows)
  weapon.wtype_id     = 0
  weapon.animation_id = 0
  weapon.growths      = [:normal, :bows] 
  weapons[weapon.id] = weapon  
#==============================================================================#
# ◙ Weapon (Bow)(Spineyard)
#==============================================================================# 
  weapon              = Weapon.new()
  weapon.initialize_add()
  weapon.id           = 2
  weapon.name         = "Spineyard"
  weapon.icon_index   = 0
  weapon.description  = ''
  weapon.features     = []
  weapon.note         = ""
  weapon.price        = 0
  weapon.etype_id     = 0 
  weapon.params       = params_from_growth(:base_bows)
  weapon.wtype_id     = 0
  weapon.animation_id = 0
  weapon.growths      = [:magic, :bows]
  weapons[weapon.id] = weapon  
#==============================================================================#
# ◙ Weapon (Bow)(Archenel)
#==============================================================================# 
  weapon              = Weapon.new()
  weapon.initialize_add()
  weapon.id           = 3
  weapon.name         = "Archenel"
  weapon.icon_index   = 0
  weapon.description  = ''
  weapon.features     = []
  weapon.note         = ""
  weapon.price        = 0
  weapon.etype_id     = 0 
  weapon.params       = params_from_growth(:base_bows)
  weapon.wtype_id     = 0
  weapon.animation_id = 0
  weapon.growths      = [:swift, :bows] 
  weapons[weapon.id] = weapon  
#==============================================================================#
# ◙ Weapon (Bow)(Stadive)
#==============================================================================# 
  weapon              = Weapon.new()
  weapon.initialize_add()
  weapon.id           = 4
  weapon.name         = "Stadive"
  weapon.icon_index   = 0
  weapon.description  = ''
  weapon.features     = []
  weapon.note         = ""
  weapon.price        = 0
  weapon.etype_id     = 0 
  weapon.params       = params_from_growth(:base_bows)
  weapon.wtype_id     = 0
  weapon.animation_id = 0
  weapon.growths      = [:heavy, :bows]
  weapons[weapon.id] = weapon   
#==============================================================================#
# ◙ Weapon (Bow)(Acaed)
#==============================================================================# 
  weapon              = Weapon.new()
  weapon.initialize_add()
  weapon.id           = 5
  weapon.name         = "Acaed"
  weapon.icon_index   = 0
  weapon.description  = ''
  weapon.features     = []
  weapon.note         = ""
  weapon.price        = 0
  weapon.etype_id     = 0 
  weapon.params       = params_from_growth(:base_bows)
  weapon.wtype_id     = 0
  weapon.animation_id = 0
  weapon.growths      = [:dual, :bows]
  weapons[weapon.id] = weapon     
#==============================================================================#
# ◙ REMAP
#==============================================================================# 
  adjust_weapons(weapons,wep_sym)    
end
end
