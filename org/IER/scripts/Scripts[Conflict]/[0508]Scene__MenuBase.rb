# Scene::MenuBase
class Scene::MenuBase < Scene::Base
  #--------------------------------------------------------------------------
  # ● 開始処理
  #--------------------------------------------------------------------------
  def start
    super
    create_canvas()
    create_background
    @actor = $game_party.menu_actor
  end
  #--------------------------------------------------------------------------
  # ● 終了処理
  #--------------------------------------------------------------------------
  def terminate
    super
    dispose_background
  end
  #--------------------------------------------------------------------------
  # ● 背景の作成
  #--------------------------------------------------------------------------
  def create_background
    super()
    @background_sprite.bitmap = SceneManager.background_bitmap
  end
  #--------------------------------------------------------------------------
  # ● ヘルプウィンドウの作成
  #--------------------------------------------------------------------------
  #def create_help_window
  #  @help_window = Window::Help2.new(@canvas.x,@canvas.y,@canvas.width)
  #  @help_window.viewport = @viewport
  #end
  def create_help_window
    rect = Rect.new(0,0,0,0) # // . x . To gain access to surface stuff
    @help_window = Window::BigHelp.new(@canvas.x,@canvas.y,@canvas.width,rect.adjust_h4window(96,8))
    @help_window.viewport = @viewport
    @help_window.actor = @actor
  end
  #--------------------------------------------------------------------------
  # ● 次のアクターに切り替え
  #--------------------------------------------------------------------------
  def next_actor
    @actor = $game_party.menu_actor_next
    on_actor_change
  end
  #--------------------------------------------------------------------------
  # ● 前のアクターに切り替え
  #--------------------------------------------------------------------------
  def prev_actor
    @actor = $game_party.menu_actor_prev
    on_actor_change
  end
  #--------------------------------------------------------------------------
  # ● アクターの切り替え
  #--------------------------------------------------------------------------
  def on_actor_change
  end
end
