# SimpleBackgroundTest
=begin
class Container::Simple_Background_Test < Container::WinSel
  bin = WindowAddons::AddonBin[
    WindowAddons::Header,
    WindowAddons::ScrollBar,
    WindowAddons::Footer
  ]
  class AddonBin_SimpleTest < bin
    def header_text
      return "Test!"
    end
    def header_cube
      (c=super).xset(self.x,nil,nil,width,nil,nil)
    end  
    def header_bitmap
      CacheExt.bitmap("header_base")
    end 
    def header_text_settings
      (hsh=super)["font"].color = Pallete[:white]
      hsh
    end  
    def footer_bitmap
      CacheExt.bitmap("footer_base")
    end  
  end  
  def addon_bin
    AddonBin_SimpleTest
  end
  def item_max
    15
  end
  def col_max
    1
  end  
  include Container::Addons::Background_Simple  
end  
MouseEx.init
container = Container::Simple_Background_Test.new(Graphics.rect.contract(48))
container.activate
loop do
  Main.update
  container.update
end  
=end
