# Handler::Effect
#==============================================================================#
# ■ Handler::Effect
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 01/16/2012
# // • Data Modified : 01/20/2012
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 01/16/2012 V1.0   
#     ♣ 01/19/2012 V1.0  
#         Fiber-ized the command process
#     ♣ 01/20/2012 V1.0  
#         Optimized Cycle, Loop, If, Else, End
#==============================================================================#
class Game::Variables ; end
class Handler::Effect ; end
module Handler::Effect::EFF_Codes
  include MathOp::MathOp_Codes
  CMD_NULL         = 0   # // Placed at the end of all effect lists
  CMD_OPACITY      = 1   # // Opacity >_>
  CMD_OFFSET       = 2   # // OX, OY
  CMD_MOVE         = 3   # // X, Y, Z
  CMD_VISIBLE      = 4   # // Visible: Character, Overall
  CMD_COLOR        = 5   # // 
  CMD_WAIT         = 6   # //
  CMD_SCRIPT       = 7
  CMD_LOOP         = 101
  CMD_REPEAT       = 102
  CMD_CYCLE        = 103
  CMD_CYCLE_REPEAT = 104
  CMD_YIELD        = 110
  CMD_BREAK        = 120
  CMD_IF           = 151
  CMD_UNLESS       = 152 # // N/A
  CMD_ELSE         = 153
  CMD_END          = 201
  CMD_VARIABLE     = 301
  CMD_LABEL        = 401 
  CMD_JUMP         = 402 
  VARIABLE_GET= 0
  VARIABLE_OP = 1  
  COLOR_RED   = 0
  COLOR_GREEN = 1
  COLOR_BLUE  = 2
  COLOR_ALPHA = 3
  COLOR_RGB   = 4
  COLOR_RGBA  = 5
  PARAM_LOGIC = 0
  PARAM_SCRIPT= 1
  BREAKABLE_DEPTHS = [CMD_LOOP, CMD_CYCLE]
  CODE_STRING = []
  CODE_STRING[CMD_NULL]    = "Null"
  CODE_STRING[CMD_OPACITY] = "Opacity"
  CODE_STRING[CMD_OFFSET]  = "Offset"
  CODE_STRING[CMD_MOVE]    = "Move"
  CODE_STRING[CMD_VISIBLE] = "Visible"
  CODE_STRING[CMD_COLOR]   = "Color"
  CODE_STRING[CMD_WAIT]    = "Wait"
  CODE_STRING[CMD_LOOP]    = "Loop"
  CODE_STRING[CMD_CYCLE]   = "Cycle"
  CODE_STRING[CMD_YIELD]   = "Yield"
  CODE_STRING[CMD_BREAK]   = "Break"
  CODE_STRING[CMD_IF]      = "If"
  CODE_STRING[CMD_UNLESS]  = "Unless"
  CODE_STRING[CMD_ELSE]    = "Else"
  CODE_STRING[CMD_END]     = "End"
  CODE_STRING[CMD_VARIABLE]= "Variable"
  CODE_STRING[CMD_LABEL]   = "Label"
  CODE_STRING[CMD_JUMP]    = "Jump"
  CODE_STRING[CMD_REPEAT]  = "Repeat Above"
  CODE_STRING[CMD_CYCLE_REPEAT] = "Cycle Rpt.Above"
end  
class Handler::Effect
  include MathOp
  include Handler::Effect::EFF_Codes
  @@list = {}
  def self.list()
    @@list 
  end  
  def self.to_cmd_list( array )
    assign_indents( ([CMD_NULL]+array+[CMD_NULL]).collect do |a| 
      a = [a] unless a.is_a?(Array) 
      efc = EffectCommand.new(*a) 
      efc
    end )
  end  
  def self.assign_indents( list )
    opn_codes = [CMD_LOOP, CMD_CYCLE, CMD_IF]
    bck_codes = [CMD_ELSE]
    end_codes = [CMD_REPEAT, CMD_CYCLE_REPEAT, CMD_END]
    indent = 1
    for efc in list
      indent = indent.pred.max(0) if end_codes.include?( efc.code ) 
      unless bck_codes.include?(efc.code)
        efc.indent = indent
      else
        efc.indent = indent.pred.max(0)
      end  
      indent += 1 if opn_codes.include?( efc.code )
    end  
    list
  end  
  def self.print_list( list )
    for efc in list
      print "  " * efc.indent
      print format( "%s: ", CODE_STRING[efc.code])#, efc.code ) "%08s(%03d): "
      efc.params.each { |p| print format( "%s ", p ) }
      print "\n"
    end
  end  
  attr_accessor :x, :y, :z
  attr_accessor :ox, :oy
  attr_accessor :blend_type
  attr_accessor :opacity
  attr_accessor :visible
  attr_accessor :cvisible
  attr_accessor :color
  attr_reader :index
  class CMD_Variables < Game::Variables
    def on_change ; end
  end  
  def initialize( list=nil )
    clear() 
    reset()
    @list = list if list
    create_fiber if @list
  end  
  def clear
    @variables    = CMD_Variables.new()
    @x, @y, @z    = 0, 0, 0
    @ox, @oy      = 0, 0, 0
    @blend_type   = 0
    @color        = Color.new(0,0,0,0)
    @opacity      = 255
    @visible      = true
    @cvisible     = true
    @list         = []
  end
  def current_settings_s
    str = ""
    str+= "x: #{@x}\n"
    str+= "y: #{@y}\n"
    str+= "z: #{@z}\n"
    str+= "opacity: #{@opacity}\n"
    str+= "visible: #{@visible}\n"
    str+= "cvisible: #{@cvisible}\n"
    str
  end 
  def print_current_command
    #return puts "Nil Command" unless @list[@index]
    puts format( "Index: %d (%s: %s)", @index, CODE_STRING[@list[@index].code], @list[@index].params ) if @list[@index]
  end 
  def reset
    @index        = 0
    @indent       = 0
    @labels       = {}
    @cycles       = {}
    @branch       = {}
  end  
  def done?
    return @index == @list.size 
  end  
  def jump_to( index )
    @index = index
  end   
  def advance_index
    @index += 1
  end 
  def previous_indent()
    @indent.pred 
  end  
  def increase_indent()
    @indent += 1
  end  
  def decrease_indent()
    @indent = @indent.pred.max(0)   
  end  
  def clear_indents( dep )
    @indent_stack.select! { |a| a[0] < dep }
  end 
  def create_fiber
    @fiber = Fiber.new { run } if @list
  end
  def running?
    @fiber != nil
  end
  def run
    while @list[@index] do
      #print_current_command
      #puts current_settings_s
      Fiber.yield if process_command(@list[@index]) == 1
      advance_index
    end  
    Fiber.yield
    @fiber = nil
  end  
  def update
    @fiber.resume if @fiber
  end  
  def process_arithm( *args )
    MathOp.arithmetic( *args )
  end  
  def process_logic( *args )
    MathOp.logic( *args )
  end  
  def process_command( efc )
    return 1 if efc.nil?()
    @indent = efc.indent 
    case efc.code
    when CMD_NULL
      return 0
    when CMD_YIELD
      return 1
    when CMD_JUMP
      ind = find_label_index(efc.param(0))
      raise "Could not find label #{efc.param(0)}" unless ind 
      jump_to( ind )
    when CMD_OPACITY
      @opacity = process_arithm(@opacity, efc.param(0), convert_param(efc.param(1))) 
    when CMD_OFFSET
      case efc.param(0) 
      when 0
        @ox = process_arithm(@ox, efc.param(1), convert_param(efc.param(2))) 
      when 1  
        @oy = process_arithm(@oy, efc.param(1), convert_param(efc.param(2))) 
      end  
    when CMD_MOVE
      case efc.param(0) 
      when 0
        @x = process_arithm(@x, efc.param(1), convert_param(efc.param(2))) 
      when 1  
        @y = process_arithm(@y, efc.param(1), convert_param(efc.param(2))) 
      when 2
        @z = process_arithm(@z, efc.param(1), convert_param(efc.param(2))) 
      end
    when CMD_VISIBLE
      v = convert_param(efc.param(0)) == 0 
      case efc.param(0) 
      when 0
        @visible = v
      when 1
        @cvisible = v
      end  
    when CMD_COLOR
      command_color( *efc.params )
    when CMD_BREAK
      command_skip()
    when CMD_LOOP  
    when CMD_REPEAT
      command_repeat()
    when CMD_CYCLE
      @cycles[@indent] = convert_param( efc.param(0).to_i )
    when CMD_CYCLE_REPEAT
      @cycles[@indent] = @cycles[@indent].pred.max(0)
      command_repeat() unless @cycles[@indent] == 0 
    when CMD_IF
      command_if( *efc.params )
    when CMD_ELSE
      command_else( *efc.params ) 
    when CMD_END
      command_end( *efc.params ) 
    when CMD_VARIABLE
      command_variable( *efc.params )
    when CMD_SCRIPT
      eval( efc.param(0) )
    when CMD_WAIT
      convert_param( efc.param(0).to_i ).times { Fiber.yield }
    end  
    return 0
  end 
  def logic_params( params )
    params.collect { |prm| convert_param( prm ) }
  end  
  def logic_correct_by_code( code, bool )
    case code
    when CMD_IF
      return bool
    when CMD_UNLESS  
      return !bool
    end  
  end  
  def convert_param( param )
    return command_variable(*param[1, param.size]) if param.is_a?(Array) && param[0] == CMD_VARIABLE
    return @cycles[previous_indent] if param.is_a?(Array) && param[0] == CMD_CYCLE
    return param
  end 
  def eval_logic( code, values )
    case code
    when PARAM_LOGIC
      process_logic(*logic_params(values))
    when PARAM_SCRIPT
      eval(values)
    end
  end  
  def command_skip( *params )
    @index += 1 while @list[@index + 1].indent > @indent
  end 
  def command_color( *params )
    case params[0]
    when COLOR_RED   # // Red
      @color.xset(:red=>process_arithm(@color.red, params[1], convert_param(params[2])))
    when COLOR_GREEN # // Green
      @color.xset(:green=>process_arithm(@color.green, params[1], convert_param(params[2])))
    when COLOR_BLUE  # // Blue
      @color.xset(:blue=>process_arithm(@color.blue, params[1], convert_param(params[2])))
    when COLOR_ALPHA # // Alpha  
      @color.xset(:alpha=>process_arithm(@color.alpha, params[1], convert_param(params[2])))
    when COLOR_RGB   # // RGB
      @color.set( *@color.to_a_na.collect { |c| 
        process_arithm(c, params[1], convert_param(params[2])) } )
    when COLOR_RGBA  # // RGBA
      @color.set( *@color.to_a.collect { |c| 
        process_arithm(c, params[1], convert_param(params[2])) } )  
    end  
  end      
  def command_if( *params )
    @branch[@indent] = eval_logic(*params) ? 0 : 1
    command_skip if @branch[@indent] != 0
  end  
  def command_else( *params )
    command_skip if @branch[@indent] == 0
  end  
  def command_repeat( *params )
    begin
      @index -= 1
    end until @list[@index].indent == @indent
  end  
  def command_end( *params )
  end  
  def command_variable( variable_id, code, *params )
    case code
    when VARIABLE_OP 
      v = @variables[variable_id]
      @variables[variable_id] = process_arithm(v, params[0], convert_param(params[1]))      
    end  
    return @variables[variable_id]
  end  
  def find_command_index( eff_code, sindex, list, indent, sindex_limit=nil )
    for i in sindex...list.size
      return i if list[i].code == eff_code && list[i].indent == indent
      break if sindex == sindex_limit if sindex_limit
    end  
    return nil
  end  
  def find_end_index( sindex, list, indent )
    find_command_index( CMD_END, sindex, list, indent )
  end  
  def find_label_index( label )
    @list.index() { |a| a.code == CMD_LABEL && a.param(0) == label }
  end  
end  
class EffectCommand
  attr_accessor :code
  attr_accessor :params
  attr_accessor :indent
  def initialize( code, *params )
    @code   = code
    @params = params
    @indent  = 0
  end 
  def params_after()
    return @params.slice
  end  
  def param(id)
    @params[id]
  end 
end  
class Handler::Effect
  def self.from_list( n )
    l = @@list[n]  
    return nil unless l
    self.new( l )
  end  
end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
