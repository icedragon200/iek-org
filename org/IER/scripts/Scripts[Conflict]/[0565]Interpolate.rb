# Interpolate
#~ sp = Sprite.new
#~ sp.bitmap = Cache.system("Ball")
#~ sp.center_oxy()
#~ #sp_points = Array.new(4) do 
#~ #  tsp = Sprite.new; tsp.bitmap = Cache.system("Ball") 
#~ #  tsp.zoom_x = tsp.zoom_y = 0.5; tsp.tone = Pallete[:green].xset(:alpha=>0).to_tone
#~ #  tsp.center_oxy(32,32)
#~ #  tsp
#~ #end  
#~ MouseEx.init
#~ canvas = Bitmap.new(*Graphics.rect.xto_a(:width,:height))
#~ canvas_sprite = Sprite.new()
#~ canvas_sprite.bitmap = canvas
#~ class Bitmap
#~   # // Bresenham's line algorithm
#~   def draw_line(x1,y1,x2,y2,color)
#~     dx = x2 - x1
#~     dy = y2 - y1
#~     sx = x1 < x2 ? 1 : -1
#~     sy = y1 < y2 ? 1 : -1
#~     err = (dx-dy).to_f
#~     e2 = 0
#~     loop do
#~       contents.set_pixel(x1,x2,color)
#~       break if(x1 == x2 && y1 == y2)
#~       e2 = 2*err
#~       if e2 > -dy 
#~         err = err - dy
#~         x1  = x1 + sx  
#~       end
#~       if e2 < dx 
#~         err = err + dx
#~         y1  = y1 + sy 
#~       end
#~     end  
#~   end
#~ end  
#~ class BitmapEx
#~   def self.draw_line(bitmap, x1, y1, x2, y2, color, weight = 1)
#~     # Bresenham's line algorithm
#~     a = (y2 - y1).abs
#~     b = (x2 - x1).abs
#~     s = (a > b)
#~     dx = (x2 < x1) ? -1 : 1
#~     dy = (y2 < y1) ? -1 : 1
#~     if s
#~       c = a
#~       a = b
#~       b = c
#~     end
#~     df1 = ((b - a) << 1)
#~     df2 = -(a << 1)
#~     d = b - (a << 1)
#~     self.set_pxels(bitmap, x1, y1, color, weight) 
#~     if(s)
#~       while y1 != y2
#~         y1 += dy
#~         if d < 0
#~           x1 += dx
#~           d += df1
#~         else
#~           d += df2
#~         end
#~         self.set_pxels(bitmap, x1, y1, color, weight) 
#~       end
#~     else
#~       while x1 != x2
#~         x1 += dx
#~         if d < 0
#~           y1 += dy
#~           d += df1
#~         else
#~           d += df2
#~         end
#~         self.set_pxels(bitmap, x1, y1, color, weight) 
#~       end
#~     end
#~   end
#~   def self.set_pxels(bitmap, x, y, color, weight = 1)
#~     even = ((weight % 2) == 0) ? 1 : 0
#~     half = weight / 2
#~     for px in (x-half)..(x+half-even)
#~       for py in (y-half)..(y+half-even)
#~         bitmap.set_pixel(px, py, color) 
#~       end
#~     end
#~   end
#~ end  
#~ loop do
#~   canvas.clear
#~   points = []
#~   sz = 24#+rand(86)
#~   points = Array.new(sz) { Point[0,0] }
#~   points.each_with_index { |p,i| 
#~     p.x,p.y = Graphics.width*i/sz.to_f,rand(Graphics.height)
#~   }  
#~   #a = Point[Graphics.width*0.12,100]
#~   #b = Point[Graphics.width*0.38, 20]
#~   #c = Point[Graphics.width*0.72,180]
#~   #d = Point[Graphics.width*0.95,100]
#~   #points += [a, b, c, d]
#~   #points.each_with_index{|p,i|
#~   #  sp_points[i].x,sp_points[i].y = p.to_xy_a
#~   #}
#~   for i in 0...(points.size-1)
#~     p1, p2 = points[i], points[i+1]
#~     c = Pallete[:sys_orange].transition_to(Pallete[:sys_red],i/(points.size-1).to_f)
#~     BitmapEx.draw_line(canvas,p1.x.to_i,p1.y.to_i,p2.x.to_i,p2.y.to_i,c,2)
#~     Graphics.update
#~   end  
#~   last_point = nil
#~   for i in 0...256
#~     dest = Point[0,0]
#~     Interpolate.bezier(dest,i/256.0,*points)
#~     #puts dest.to_s
#~     sp.x, sp.y = dest.to_xy_a
#~     Main.update #if i % 10 == 0
#~     if last_point
#~       BitmapEx.draw_line(canvas,last_point.x.to_i,last_point.y.to_i,dest.x.to_i,dest.y.to_i,Pallete[:white])
#~     end  
#~     last_point = dest
#~   end  
#~ end
