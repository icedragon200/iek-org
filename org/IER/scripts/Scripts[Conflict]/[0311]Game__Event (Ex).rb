# Game::Event (Ex)
module Mixin::CommentCache
  def rebuild_comment_cache()
    @comments = []
    RPG::EventCommand.each_comment(@list||[]){|line|@comments.push(line)} 
  end  
  def read_comment_cache()
    comment_cache_pre()
    unless @comments
      rebuild_comment_cache()
    end  
    @comments.each{|line|comment_cache_line(line)}
    comment_cache_post()
  end  
  def comment_cache_pre()
  end
  def comment_cache_line( line )    
  end
  def comment_cache_post()
  end
end  
class Game::Event < Game::Character
  include Mixin::CommentCache
end  
