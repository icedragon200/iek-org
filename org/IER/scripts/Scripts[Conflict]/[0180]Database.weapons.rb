# Database.weapons
#==============================================================================#
# ■ Database.weapons
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/07/2011
# // • Data Modified : 12/07/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/07/2011 V1.0 
#         Added:
#           Weapon 0 (Dummy)
#           Weapon 1 (<unnamed>)
#
#==============================================================================#
module Database
  Weapon = RPG::Weapon
def self.adjust_weapons(items,weapon_sym)  
  items.compact.each {|i|adjust_weapon(i,weapon_sym)}
  items
end  
def self.adjust_weapon(item,weapon_sym)  
  iof = @weapon_id_off[weapon_sym] # // ID offset 
  item.icon_index = @weapon_icon_index[weapon_sym] + item.id
  item.id = iof + item.id
  item.wtype_id = @weapon_type_id[weapon_sym]
  item
end  
def self.add_weapons(items)
  items.compact.each {|i|@weapons[i.id]=i}
end  
def self.build_weapons()
  @_weapon_param_blocks = {}
  @weapons = []
#==============================================================================#
# ◙ Weapon 0 (Dummy)
#==============================================================================# 
  weapon              = Weapon.new()
  weapon.initialize_add()
  weapon.id           = 0
  weapon.name         = "Dummy Weapon"
  weapon.icon_index   = 0
  @weapons[weapon.id] = weapon
#==============================================================================#
# ◙ Spears  
#==============================================================================#
  add_weapons mk_weapons1()
#==============================================================================#
# ◙ Brushes
#==============================================================================#
  add_weapons mk_weapons2()
#==============================================================================#
# ◙ Swords
#==============================================================================#
  add_weapons mk_weapons3()
#==============================================================================#
# ◙ Bows
#==============================================================================#
  add_weapons mk_weapons4()
#==============================================================================#
# ◙ REMAP
#==============================================================================#   
  for i in 0...@weapons.size
    next unless @weapons[i]
    weapon = @weapons[i]
    weapon.id    = i
    weapon.rebuild_parameter_table( WEAPON_LEVELCAP ) do |p,l,o|
      weapon_parameter(o,p,l)
    end
    8.times{|i|weapon.params[i] = weapon.level_params[i,1]}
    weapon.iconset_name = "Iconset_Weapon"
    weapon.database_offset = 1000
  end   
#File.open("WeaponNames.txt", "w+"){|f|
#  @weapons.compact.each{|s|f.puts(format("%03d-%s", s.id, s.name))}
#}  
end  
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
