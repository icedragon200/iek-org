# DataManager
#==============================================================================#
# ■ DataManager
#==============================================================================#
# // • Created By    : Enterbrain
# // • Modified By   : IceDragon
# // • Data Created  : ??/??/2011
# // • Data Modified : 12/06/2011
# // • Version       : 1.0a
#==============================================================================#
# ● Change Log
#     ♣ 12/06/2011 V1.0a : 1 Change(s)
#         Removed (Loading) Data Objects for:
#           Actors  (replaced by Game::Data.get_actors())
#           Classes (replaced by Game::Data.get_classes())
#           Skills  (replaced by Game::Data.get_skills())
#           Items   (replaced by Game::Data.get_items())
#           Weapons (replaced by Game::Data.get_weapons())
#           Armors  (replaced by Game::Data.get_armors())
#           Enemies (replaced by Game::Data.get_enemies())
#           Troop   (Disabled)
#           States  (replaced by Game::Data.get_states())
#           System  (replaced by Game::Data.get_system())
#
#     ♣ 12/07/2011 V1.0a : 1 Change(s)
#         Removed (Loading) Data Objects for:
#           Animations (replaced by Game::Data.get_animations)
#
#     ♣ 12/14/2011 V1.0a : 1 Addition
#         Added (Game::Rogue) $game_rogue
#
#     ♣ 12/18/2011 V1.0a : 1 Addition
#         Added (Data) $data_traps (Game::Data.get_traps())
#
#     ♣ 12/30/2011 V1.0a : 1 Addition
#         Added (Game::Settings) $game_settings
#
#     ♣ 01/03/2012 V1.0a : 1 Addition
#         Added (Data) $data_crafts (Game::Data.get_crafts())
#
#     ♣ 02/02/2012 V1.0a : 1 Addition
#         Restored Troop
#         Common_Events  (replaced by Game::Data.get_commonEvents())        
#
#==============================================================================#
#------------------------------------------------------------------------------
# 　データベースとゲームオブジェクトを管理するモジュールです。ゲームで使用する
# ほぼ全てのグローバル変数はこのモジュールで初期化されます。
#==============================================================================
module DataManager
  include GameManager
  #--------------------------------------------------------------------------
  # ● モジュールのインスタンス変数
  #--------------------------------------------------------------------------
  @last_savefile_index = 0                # 最後にアクセスしたファイル
  #--------------------------------------------------------------------------
  # ● モジュール初期化
  #--------------------------------------------------------------------------
  def self.init
    @last_savefile_index = 0
    load_database
    create_game_objects
    setup_battle_test if $BTEST
  end
  #--------------------------------------------------------------------------
  # ● データベースのロード
  #--------------------------------------------------------------------------
  def self.load_database
    if $BTEST
      load_battle_test_database
    else
      load_normal_database
      check_player_location
    end
  end
  #--------------------------------------------------------------------------
  # ● 通常のデータベースをロード
  #--------------------------------------------------------------------------
  def self.load_normal_database
    Database.load_and_save()
    pre_db_load()
    $data_actors        = Game::Data.get_actors() 
    $data_classes       = Game::Data.get_classes()
    $data_skills        = Game::Data.get_skills() 
    $data_items         = Game::Data.get_items()  
    $data_weapons       = Game::Data.get_weapons()
    $data_armors        = Game::Data.get_armors() 
    $data_enemies       = Game::Data.get_enemies()
    $data_troops        = Game::Data.get_troops()
    $data_states        = Game::Data.get_states() 
    $data_animations    = Game::Data.get_animations()
    $data_tilesets      = Game::Data.get_tilesets()
    $data_common_events = Game::Data.get_commonEvents() 
    $data_system        = Game::Data.get_system()  
    $data_mapinfos      = Game::Data.get_mapinfos() 
    # //
    $data_startup       = Game::Data.get_startup()
    $data_temp          = Game::Data.get_temp()
    $data_traps         = Game::Data.get_traps()
    $data_crafts        = Game::Data.get_crafts()
    # // 
    $data_spirit_classes= Game::Data.get_spirit_classes()
    post_db_load()
  end
  #--------------------------------------------------------------------------
  # ● 戦闘テスト用のデータベースをロード
  #--------------------------------------------------------------------------
  def self.load_battle_test_database
    $data_actors        = load_data("Data/BT_Actors.rvdata2")
    $data_classes       = load_data("Data/BT_Classes.rvdata2")
    $data_skills        = load_data("Data/BT_Skills.rvdata2")
    $data_items         = load_data("Data/BT_Items.rvdata2")
    $data_weapons       = load_data("Data/BT_Weapons.rvdata2")
    $data_armors        = load_data("Data/BT_Armors.rvdata2")
    $data_enemies       = load_data("Data/BT_Enemies.rvdata2")
    $data_troops        = load_data("Data/BT_Troops.rvdata2")
    $data_states        = load_data("Data/BT_States.rvdata2")
    $data_animations    = load_data("Data/BT_Animations.rvdata2")
    $data_tilesets      = load_data("Data/BT_Tilesets.rvdata2")
    $data_common_events = load_data("Data/BT_CommonEvents.rvdata2")
    $data_system        = load_data("Data/BT_System.rvdata2")
  end
  def self.pre_db_load()
  end
  def self.post_db_load()
  end  
  #--------------------------------------------------------------------------
  # ● プレイヤーの初期位置存在チェック
  #--------------------------------------------------------------------------
  def self.check_player_location
    if $data_system.start_map_id == 0
      msgbox(Vocab::PlayerPosError)
      exit
    end
  end
  #--------------------------------------------------------------------------
  # ● 各種ゲームオブジェクトの作成
  #--------------------------------------------------------------------------
  def self.create_game_objects
    $game_temp          = Game::Temp.new
    $game_system        = Game::System.new
    $game_switches      = Game::Switches.new
    $game_variables     = Game::Variables.new
    $game_timer         = Game::Timer.new
    $game_message       = Game::Message.new
    $game_self_switches = Game::SelfSwitches.new
    $game_actors        = Game::Actors.new
    $game_party         = Game::Party.new
    $game_troop         = Game::Troop.new
    $game_map           = Game::Map.new
    $game_rogue         = Game::Rogue.new # // . x .
    $game_player        = Game::Player.new
    $game_settings      = Game::Settings.new
  end
  #--------------------------------------------------------------------------
  # ● ニューゲームのセットアップ
  #--------------------------------------------------------------------------
  def self.setup_new_game
    create_game_objects
    $game_party.setup_starting_members
    $game_map.setup($data_system.start_map_id)
    $game_player.moveto($data_system.start_x, $data_system.start_y)
    $game_player.refresh
    Graphics.frame_count = 0
  end
  #--------------------------------------------------------------------------
  # ● 戦闘テストのセットアップ
  #--------------------------------------------------------------------------
  def self.setup_battle_test
    $game_party.setup_battle_test
    BattleManager.setup($data_system.test_troop_id)
    BattleManager.play_battle_bgm
  end
  #--------------------------------------------------------------------------
  # ● セーブファイルの存在判定
  #--------------------------------------------------------------------------
  def self.save_file_exists?
    #!Dir.glob('Save*.rvdata2').empty?
    !Dir.glob(GAME_SAVE_GLB).empty?
  end
  #--------------------------------------------------------------------------
  # ● セーブファイルの最大数
  #--------------------------------------------------------------------------
  def self.savefile_max
    return 16
  end
  #--------------------------------------------------------------------------
  # ● ファイル名の作成
  #     index : ファイルインデックス
  #--------------------------------------------------------------------------
  def self.make_filename(index)
    #sprintf("Save%02d.rvdata2", index + 1)
    sprintf(GAME_SAVE_SPF, index + 1)
  end
  #--------------------------------------------------------------------------
  # ● セーブの実行
  #--------------------------------------------------------------------------
  def self.save_game(index)
    begin
      save_game_without_rescue(index)
    rescue => ex
      puts ex
      delete_save_file(index)
      false
    end
  end
  #--------------------------------------------------------------------------
  # ● ロードの実行
  #--------------------------------------------------------------------------
  def self.load_game(index)
    load_game_without_rescue(index) rescue false
  end
  #--------------------------------------------------------------------------
  # ● セーブヘッダのロード
  #--------------------------------------------------------------------------
  def self.load_header(index)
    load_header_without_rescue(index) rescue nil
  end
  #--------------------------------------------------------------------------
  # ● セーブの実行（例外処理なし）
  #--------------------------------------------------------------------------
  def self.save_game_without_rescue(index)
    File.open(make_filename(index), "wb") do |file|
      $game_system.on_before_save
      Marshal.dump(make_save_header, file)
      Marshal.dump(make_save_contents, file)
      @last_savefile_index = index
    end
    return true
  end
  #--------------------------------------------------------------------------
  # ● ロードの実行（例外処理なし）
  #--------------------------------------------------------------------------
  def self.load_game_without_rescue(index)
    File.open(make_filename(index), "rb") do |file|
      Marshal.load(file)
      extract_save_contents(Marshal.load(file))
      reload_map_if_updated
      @last_savefile_index = index
    end
    return true
  end
  #--------------------------------------------------------------------------
  # ● セーブヘッダのロード（例外処理なし）
  #--------------------------------------------------------------------------
  def self.load_header_without_rescue(index)
    File.open(make_filename(index), "rb") do |file|
      return Marshal.load(file)
    end
    return nil
  end
  #--------------------------------------------------------------------------
  # ● セーブファイルの削除
  #--------------------------------------------------------------------------
  def self.delete_save_file(index)
    File.delete(make_filename(index)) rescue nil
  end
  #--------------------------------------------------------------------------
  # ● セーブヘッダの作成
  #--------------------------------------------------------------------------   
  def self.make_save_header
    header = {}
    header[:characters] = $game_party.characters_for_savefile
    header[:playtime_s] = $game_system.playtime_s
    header[:save_background_index] = $game_system.save_background_index
    header
  end
  #--------------------------------------------------------------------------
  # ● セーブ内容の作成
  #--------------------------------------------------------------------------
  def self.make_save_contents
    contents = {}
    contents[:system]        = $game_system
    contents[:timer]         = $game_timer
    contents[:message]       = $game_message
    contents[:switches]      = $game_switches
    contents[:variables]     = $game_variables
    contents[:self_switches] = $game_self_switches
    contents[:actors]        = $game_actors
    contents[:party]         = $game_party
    contents[:troop]         = $game_troop
    contents[:map]           = $game_map
    contents[:rogue]         = $game_rogue # // . x .
    contents[:player]        = $game_player
    contents[:settings]      = $game_settings # // . x .
    contents
  end
  #--------------------------------------------------------------------------
  # ● セーブ内容の展開
  #--------------------------------------------------------------------------
  def self.extract_save_contents(contents)
    $game_system        = contents[:system]
    $game_timer         = contents[:timer]
    $game_message       = contents[:message]
    $game_switches      = contents[:switches]
    $game_variables     = contents[:variables]
    $game_self_switches = contents[:self_switches]
    $game_actors        = contents[:actors]
    $game_party         = contents[:party]
    $game_troop         = contents[:troop]
    $game_map           = contents[:map]
    $game_rogue         = contents[:rogue] # // . x .
    $game_player        = contents[:player]
    $game_settings      = contents[:settings] # // . x .
  end
  #--------------------------------------------------------------------------
  # ● データが更新されている場合はマップを再読み込み
  #--------------------------------------------------------------------------
  def self.reload_map_if_updated
    if $game_system.version_id != $data_system.version_id
      $game_map.setup($game_map.map_id)
      $game_player.center($game_player.x, $game_player.y)
      $game_player.make_encounter_count
    end
  end
  #--------------------------------------------------------------------------
  # ● セーブファイルの更新日時を取得
  #--------------------------------------------------------------------------
  def self.savefile_time_stamp(index)
    File.mtime(make_filename(index)) rescue Time.at(0)
  end
  #--------------------------------------------------------------------------
  # ● 更新日時が最新のファイルインデックスを取得
  #--------------------------------------------------------------------------
  def self.latest_savefile_index
    savefile_max.times.max_by {|i| savefile_time_stamp(i) }
  end
  #--------------------------------------------------------------------------
  # ● 最後にアクセスしたファイルのインデックスを取得
  #--------------------------------------------------------------------------
  def self.last_savefile_index
    @last_savefile_index
  end
end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
