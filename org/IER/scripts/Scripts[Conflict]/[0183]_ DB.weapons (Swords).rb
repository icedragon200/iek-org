#  | DB.weapons (Swords)
# // 02/29/2012
# // 02/29/2012
module Database
def self.mk_weapons3()
  weapons = []
  wep_sym = :swords
#==============================================================================#
# ◙ Weapon (Sword)(Lineage)
#==============================================================================# 
  weapon              = Weapon.new()
  weapon.initialize_add()
  weapon.id           = 1
  weapon.name         = "Lineage"
  weapon.icon_index   = 0
  weapon.description  = 'A mass produced sword, originally used by Emperor Linean'
  weapon.features     = []
  weapon.note         = ""
  weapon.price        = 0
  weapon.etype_id     = 0 
  weapon.params       = params_from_growth(:base_swords)
  weapon.wtype_id     = 0
  weapon.animation_id = 0
  weapon.growths      = [:normal, :swords]
  weapons[weapon.id] = weapon 
#==============================================================================#
# ◙ Weapon (Sword)(Sulrive)
#==============================================================================# 
  weapon              = Weapon.new()
  weapon.initialize_add()
  weapon.id           = 2
  weapon.name         = "Sulrive"
  weapon.icon_index   = 0
  weapon.description  = ''
  weapon.features     = []
  weapon.note         = ""
  weapon.price        = 0
  weapon.etype_id     = 0 
  weapon.params       = params_from_growth(:base_swords)
  weapon.wtype_id     = 0
  weapon.animation_id = 0
  weapon.growths      = [:magic, :swords] 
  weapons[weapon.id] = weapon    
#==============================================================================#
# ◙ Weapon (Sword)(Fenseer)
#==============================================================================# 
  weapon              = Weapon.new()
  weapon.initialize_add()
  weapon.id           = 3
  weapon.name         = "Fenseer"
  weapon.icon_index   = 0
  weapon.description  = ''
  weapon.features     = []
  weapon.note         = ""
  weapon.price        = 0
  weapon.etype_id     = 0 
  weapon.params       = params_from_growth(:base_swords)
  weapon.wtype_id     = 0
  weapon.animation_id = 0
  weapon.growths      = [:swift, :swords] 
  weapons[weapon.id] = weapon    
#==============================================================================#
# ◙ Weapon (Sword)(Emperor)
#==============================================================================# 
  weapon              = Weapon.new()
  weapon.initialize_add()
  weapon.id           = 4
  weapon.name         = "Emperor"
  weapon.icon_index   = 0
  weapon.description  = ''
  weapon.features     = []
  weapon.note         = ""
  weapon.price        = 0
  weapon.etype_id     = 0 
  weapon.params       = params_from_growth(:base_swords)
  weapon.wtype_id     = 0
  weapon.animation_id = 0
  weapon.growths      = [:heavy, :swords]
  weapons[weapon.id] = weapon  
#==============================================================================#
# ◙ Weapon (Sword)(Shorlas)
#==============================================================================# 
  weapon              = Weapon.new()
  weapon.initialize_add()
  weapon.id           = 5
  weapon.name         = "Shorlas"
  weapon.icon_index   = 0
  weapon.description  = ''
  weapon.features     = []
  weapon.note         = ""
  weapon.price        = 0
  weapon.etype_id     = 0 
  weapon.params       = params_from_growth(:base_swords)
  weapon.wtype_id     = 0
  weapon.animation_id = 0
  weapon.growths      = [:dual, :swords] 
  weapons[weapon.id] = weapon     
#==============================================================================#
# ◙ REMAP
#==============================================================================# 
  adjust_weapons(weapons,wep_sym)    
end
end
