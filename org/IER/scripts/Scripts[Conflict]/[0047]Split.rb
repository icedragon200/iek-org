# Split
#==============================================================================#
# ■ Split
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 01/15/2012
# // • Data Modified : 01/15/2012
# // • Version       : 1.0
#==============================================================================#
class Rect 
  def split_by( xs=1, ys=1, area=self )
    result = []
    rarea = area
    xs, ys = xs.max(1), ys.max(1)
    swd, shg = rarea.width / xs, rarea.height / ys
    for iy in 0...ys  
      for ix in 0...xs    
        result << self.class.new(rarea.x+(ix*swd),rarea.y+(iy*shg),swd,shg)
      end
    end  
    result
  end 
end
class Bitmap
  def split_by( xs=1, ys=1, a=rect )
    rect.split_by(xs,ys,a).collect{|r|
      b=Bitmap.new(*r.xto_a(:width,:height));b.blt(0,0,self,r);b
    }
  end  
end  
class Sprite
  def self.split_to_new( bitmap, xs, ys, src_rect=nil )
    src_rect = bitmap.rect unless src_rect
    xs, ys = xs.max(1), ys.max(1)
    bmps = bitmap.split_by(xs,ys,src_rect)
    bmps.collect { |b| 
      s              = Sprite.new(nil)
      s.bitmap       = b
      s
    }.each_with_index { |s, i|
      s.x += s.bitmap.width * (i % xs)
      s.y += s.bitmap.height * (i / xs)
    }
  end  
  def split_by( xs=1, ys=1 )
    sp = self
    xs, ys = xs.max(1), ys.max(1)
    Sprite.split_to_new(sp.bitmap,xs,ys,src_rect).each_with_index { |s, i|
      s.x           += sp.x
      s.y           += sp.y
      s.z           += sp.z
      s.ox, s.oy     = sp.ox, sp.oy
      s.viewport     = sp.viewport
      s.opacity      = sp.opacity
      s.bush_depth   = sp.bush_depth 
      s.bush_opacity = sp.bush_opacity
      s.color        = sp.color 
      s.tone         = sp.tone
    }
  end  
end
module Split
  def self.run_test
    s = Sprite.new()
    s.bitmap = Cache.system("Equipment_Tab(Window)")
    s.src_rect.contract!(3)
    s.salign(1,1)
    ss = s.split_by( 2, 2 )
    ss += Sprite.split_to_new(s.bitmap, 3, 3)
    ss.each { |sb| sb.src_rect.contract!(1) }
    s.opacity = 128
    s.src_rect.set( s.bitmap.rect )
    s.y -= s.height
    s.update
    loop do
      Graphics.update
      Input.update
      ss.each { |sb| sb.update }
    end
  end  
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
