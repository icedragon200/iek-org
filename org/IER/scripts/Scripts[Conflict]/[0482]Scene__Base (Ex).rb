# Scene::Base (Ex)
# // 01/13/2012
# // 02/01/2012
class Plane::Background < Plane
end
class Scene::Base
  def self.mk_dispose_window(*names)
    names.each do |n|
      var = "@#{n}"
      define_method("dispose_"+n.to_s) do 
        remove_window(instance_variable_get(var))
        instance_variable_set(var,nil)
      end
    end  
  end
  def self.mk_showhide_window(*names)
    names.each do |n|
      var = "@#{n}"
      define_method("show_"+n.to_s) do 
        open_and_activate_window(instance_variable_get(var))
      end
      define_method("hide_"+n.to_s) do 
        close_and_deactivate_window(instance_variable_get(var))
      end
    end  
  end
  
  def clamp_windows_to_space
    @windows.each { |w| w.clamp_to_space() }
  end  
  def create_background
    @background_sprite = Plane::Background.new(@viewport)
    @background_sprite.bitmap = Cache.system("MenuBackground2")
    @background_sprite.z = -10
  end
  def dispose_background()
    @background_sprite.dispose()
  end
  def update_background
    @background_sprite.ox += 1
  end
  def create_dim_background()
    @dim_background_sprite = Plane::Background.new(@viewport)
    @dim_background_sprite.bitmap = CacheExt.black_box
    @dim_background_sprite.opacity = 128
    @dim_background_sprite.z = -9
  end
  def dispose_dim_background()
    @dim_background_sprite.dispose()
  end
  def update_dim_background()
  end  
  def start
    create_main_viewport()
    create_windows()
  end  
  def windows_auto_z()
    @windows.each_with_index {|w,i| w.z = i }
  end  
  def create_windows
    @windows = []
  end  
  def auto_add_windows()
    instance_variables.each do |varname|
      ivar = instance_variable_get(varname)
      add_window(ivar) if ivar.is_a?(Window)
    end
  end  
  def update_all_windows
    @windows.each do |win| win.update() end
  end
  def dispose_all_windows
    @windows.each do |win| win.dispose() end
  end
  def add_window( window )
    window.viewport ||= @viewport
    @windows |= [window]
  end
  def add_windows( *windows )
    windows.each do |win| add_window( win ) end
  end  
  def remove_window( window, dsp=true )
    @windows.delete( window )
    window.dispose unless window.disposed? if window if dsp
  end 
  def remove_windows( dsp=false, *windows )
    windows.each do |win| remove_window( win, dsp ) end
  end  
  def open_and_activate_window( *windows )
    windows.each do |w|
      w.show.activate.start_open()
      wait_for_window( w )
    end
  end 
  def close_and_deactivate_window( *windows )
    windows.each do |w|
      w.deactivate.start_close()
      wait_for_window( w )
      w.hide()
    end
  end
  def close_and_remove_window( *windows )
    windows.each do |w|
      w.start_close()
      wait_for_window( w )
      remove_window( w )
    end
  end  
  def update_for_wait
    update_basic
  end
  def wait( duration )
    duration.times do |i| update_for_wait end
  end 
  def wait_ex( duration )
    duration.times do |i| update_for_wait ; yield i if block_given? end
  end  
  def wait_for_window( *windows )
    update_for_wait while windows.any? { |win| win.opening_or_closing? }
  end 
  def show_wait( wait_time, x=0, y=0, z=0, width=128, height=8 )
    sp = Sprite::Progress.new(@viewport, :horz, width, height)
    sp.x,sp.y,sp.z=x,y,z;wtf=wait_time.to_f # // xD LOL
    wait_ex(wait_time){|i|sp.rate=i/wtf ; break if yield i if block_given?}
    sp.dispose()
  end  
  RESULT_TRUE = 0
  RESULT_FALSE= 1
  def confirm_window_ok
    @confirm_result = RESULT_TRUE
  end  
  def confirm_window_cancel
    @confirm_result = RESULT_FALSE
  end
  def pop_confirm_window( *rargs )
    if rargs[0].is_a?(Hash)
      args = rargs[0] 
    else
      args = { 
        :text => rargs[0], 
        :x    => rargs[1], 
        :y    => rargs[2]
      }
    end  
    @confirm_result = nil
    x         = args[:x] || 0
    y         = args[:y] || 0
    window = Window::Confirm.new( x, y, args[:text] )
    window.set_handler( :ok, args[:yes_handler] || method(:confirm_window_ok) )
    window.set_handler( :cancel, args[:no_handler] || method(:confirm_window_cancel) )
    window.activate()
    yield window if block_given?()
    add_window( window )
    window.start_open
    wait_for_window( window )
    update_for_wait while window.active
    window.start_close
    wait_for_window( window )
    remove_window( window )
    @confirm_result
  end  
  def pop_chest_items( *rargs )
    if rargs[0].is_a?(Hash)
      args = rargs[0] 
    else
      args = { 
        :items => rargs[0], 
        :text  => rargs[1], 
        :x     => rargs[2], 
        :y     => rargs[3],
        :wait  => rargs[4]
      }
    end  
    x         = args[:x] || 0
    y         = args[:y] || 0
    wait_time = args[:wait] || 90 #   // [item, number]
    window = Window::ChestItems.new( x, y, args[:items]||[], args[:text] )
    #window.activate()
    yield window if block_given?()
    add_window( window )
    window.start_open
    wait_for_window( window )
    block = args[:break] ? proc { |i| Input.trigger_any?(:A, :B, :C) } : nil
    show_wait( wait_time, window.x, window.vy2, window.z, window.width, &block )
    window.start_close
    wait_for_window( window )
    remove_window( window )
  end  
  def pop_quick_text( *rargs )
    if rargs[0].is_a?(Hash)
      args = rargs[0] 
    else
      args = { 
        :text        => rargs[0], 
        :x           => rargs[1], 
        :y           => rargs[2],
        :width       => rargs[3], 
        :height      => rargs[4],
        :wait        => rargs[5],
        :header_text => rargs[6],
        :break       => rargs[7],
      }
    end
    x         = args[:x] || 0
    y         = args[:y] || 0
    width     = args[:width] || 172
    height    = args[:height] || 40
    texts     = args[:text] || [""]
    texts     = [texts] unless texts.is_a?(Enumerable)
    wait_time = args[:wait] || 90
    window    = Window::QuickText.new( x, y, width, height, nil, args[:header_text] )
    yield window if block_given?
    add_window( window )
    block = args[:break] ? proc { |i| Input.mtrigger_any?(:A, :B, :C) } : nil
    texts.each do |t|
      window.set_text( t )
      window.start_open
      wait_for_window( window )
      show_wait( wait_time, window.x, window.vy2, window.z, window.width, &block )
      window.start_close
      wait_for_window( window )
    end
    remove_window( window )
  end  
  def pop_quick_text_c(*args)
    pop_quick_text(*args) { |win| win.salign(1,1) }
  end  
  def center_windows(xc=true, yc=true,windows=@windows)
    winrect = Surface.area_rect( *windows )
    rect = Rect.center(Graphics.rect,winrect) 
    dif_x = xc ? winrect.x - rect.x : 0
    dif_y = yc ? winrect.y - rect.y : 0
    windows.each { |win| win.x -= dif_x; win.y -= dif_y }
  end  
  def dropdown_windows( *windows )
    or_window_pos = windows.collect do |w| w.to_rect ; end
    windows.each do |w| 
      #w.x, w.y = w.x, -w.height 
      #w.width = w.height = w.standard_padding*2
      rect = Rect.center(Graphics.rect,Surface.area_rect( w )) 
      rect.width = rect.height = w.standard_padding*2
      rect.y = -w.height
      w.move( *rect.to_a )
      w.x = w.x
      w.y = w.y
      #w.opacity = 0
    end
    for i in 0...windows.size
      win   = windows[i]
      orpos = or_window_pos[i]
      tween = Tween::Multi.new
      tween.clear()
      easer = :back_out
      frm = 10
      tween.add_tween( win.y, orpos.y, easer, Tween.frames_to_tt( frm ) )
      until tween.done?
        tween.update
        win.y = tween.values[0]
        update_for_wait 
      end  
      tween.clear()
      easer = :elastic_out
      frm = 10
      tween.add_tween( win.height, orpos.height, easer, Tween.frames_to_tt( frm ) )
      until tween.done?
        tween.update
        win.height = tween.values[0]
        update_for_wait 
      end
      tween.clear()
      frm = 30
      easer = :sine_out
      tween.add_tween( win.x, orpos.x, easer, Tween.frames_to_tt( frm ) )
      easer = :sine_in
      tween.add_tween( win.width, orpos.width, easer, Tween.frames_to_tt( frm ) )
      until tween.done?
        tween.update
        win.x     = tween.values[0]
        win.width = tween.values[1]
        update_for_wait 
      end
      #tween.clear()
      #easer = :elastic_out
      #frm = 10
      #tween.add_tween( win.x, orpos.x, easer, Tween.frames_to_tt( frm ) )
      #until tween.done?
      #  tween.update
      #  win.x = tween.values[0]
      #  #win.x, win.y, win.width, win.height = *tween.values
      #  update_for_wait 
      #end 
      #tween.clear()
      #easer = :sine_in
      #frm = 20
      #tween.add_tween( win.opacity, 255, easer, Tween.frames_to_tt( frm ) )
      #until tween.done?
      #  tween.update
      #  win.opacity = tween.value(0)
      #  update_for_wait 
      #end
      win.x, win.y, win.width, win.height = *orpos.to_a
    end  
  end
  # // 02/25/2012 
  def create_canvas() # . x . Just a guiding rect for window placement
    @canvas = Graphics.rect#.contract(4)
  end  
  # // 03/01/2012
  def pop_console_input()
    input = Container::InputConsole.new(0,0)
    input.z = 9999
    input.salign(-1,2)
    input.activate()
    eval_handler = proc do |text|
      begin
        intp = (respond_to?(:_map) && _map) ? _map.interpreter : nil
        eval(text) 
      rescue(Exception) => ex
        Input.update
        Keyboard.update
        puts("Eval failed: #{ex.message}")
        input.activate()
        #return
      else
        input.clear()
      end  
    end
    input.set_handler(:eval, eval_handler)
    Input.update()
    create_dim_background()
    @dim_background_sprite.z = 9899
    @dim_background_sprite.z = input.z - 100
    #eval_handler.call(gets)
    loop do
      Graphics.update()
      Keyboard.update()
      input.update()
      break if Keyboard.trigger?(:ESC)
    end  
    dispose_dim_background()
    Graphics.update()
    Input.update()
    Keyboard.update()
    input.dispose()
  end  
end  
