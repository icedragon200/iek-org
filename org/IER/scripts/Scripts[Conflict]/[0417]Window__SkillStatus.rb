# Window::SkillStatus
# // 02/23/2012
# // 02/25/2012
class Window::SkillStatus < Window::RogueStatus
  def standard_draw_mode()
    3
  end  
  def window_width()
    super - 160 - 64
  end  
end
