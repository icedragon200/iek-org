# SubScene::RogueMinimap
# // 02/03/2012
# // 02/03/2012
class SubScene::RogueMinimap < SubScene::Base
  def start()
    super()
    create_background()
    @background_sprite.opacity = 128
    create_minimap()
    @input_handler = InputHandler.new
    @input_handler.set_handler(:dir4, method(:scroll_minimap))
    @input_handler.set_handler(:mB, method(:close_minimap))
    @mouse_handler = Handler::ViewEdge.new(@viewport.rect.clone)
    @mouse_handler.set_handler(:down, method(:scroll_down))
    @mouse_handler.set_handler(:left, method(:scroll_left))
    @mouse_handler.set_handler(:right, method(:scroll_right))
    @mouse_handler.set_handler(:up, method(:scroll_up))   
    @mouse_handler.set_scroll_amp(10)
    open_minimap()
  end  
  def open_minimap()
    x,y,w,h = *@minimap_spriteset.viewport1.rect.to_a
    tweener = Tween.new([x,y,w,0],[x,y,w,h],:back_in,Tween.f2tt(20))
    until tweener.done?
      tweener.update
      @minimap_spriteset.viewport1.rect.set(@viewport.rect.set(*tweener.values))
      update_for_wait
    end
  end  
  def close_minimap()
    x,y,w,h = *@minimap_spriteset.viewport1.rect.to_a
    tweener = Tween.new([x,y,w,h],[x,y,w,0],:back_in,Tween.f2tt(20))
    until tweener.done?
      tweener.update
      @minimap_spriteset.viewport1.rect.set(@viewport.rect.set(*tweener.values))
      update_for_wait
    end
    end_subscene()
  end 
  def scroll_down(n)
    scroll_minimap(2,n)
  end
  def scroll_left(n)
    scroll_minimap(4,n)
  end
  def scroll_right(n)
    scroll_minimap(6,n)
  end
  def scroll_up(n)
    scroll_minimap(8,n)
  end  
  def scroll_minimap(d,n=1)
    @minimap_spriteset.scroll(d,n)  
  end  
  def create_minimap()
    @minimap_spriteset = Spriteset_Minimap.new()
    @minimap_spriteset.viewport1.z = @viewport.z + 1
    @minimap_spriteset.set_rect( *Graphics.rect.to_a )
    @minimap_spriteset.unlock
  end  
  def terminate()
    super()
    @input_handler = nil
    @mouse_handler = nil
    @minimap_spriteset.dispose()
    dispose_background()
  end  
  def update_basic()
    super()
    update_background()
    @minimap_spriteset.update
  end  
  def update
    super()
    @input_handler.update
    @mouse_handler.update
  end  
end  
