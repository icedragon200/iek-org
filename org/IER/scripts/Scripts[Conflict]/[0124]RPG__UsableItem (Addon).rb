# RPG::UsableItem (Addon)
class RPG::UsableItem
  def initialize_add
    super
    # // :miss, :evade, :hit, :attack
    @user_sprite_actions = []
    @target_sprite_actions = []
    @ai_tags = []
  end
  def count_name( n )
    if n == 0
      "No #{@name}s"
    elsif n == 1
      "#{n} #{@name}"
    elsif n > 1
      "#{n} #{@name}s"
    end  
  end  
  attr_accessor :user_sprite_actions
  attr_accessor :target_sprite_actions
  attr_accessor :ai_tags
  class SpriteAction
  end  
end 
