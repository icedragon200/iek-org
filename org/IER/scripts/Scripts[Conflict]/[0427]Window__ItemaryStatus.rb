# Window::ItemaryStatus
#==============================================================================#
# ♥ Window::ItemaryStatus
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 01/08/2012
# // • Data Modified : 01/08/2012
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 01/08/2012 V1.0 
#==============================================================================#
class Window::ItemaryStatus < Window::BookStatus
  attr_reader :item
  def init_members()
    super()
  end  
  def draw_block2( x, y )
    bmp = super( x, y )
    draw_item_icon( @item, ((bmp.width - 24) / 2), ((bmp.height - 24) - 4) + 28 )
  end  
  def refresh
    super
  end  
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
