# Window::Base (Ex)
# // 12/12/2011
# // 02/02/2012
class Window::Base
  def open_artist(dont_ex=false,&block)
    return dont_ex ? yield(@artist) : @artist.instance_exec(self,&block) if(block_given?())
    return @artist
  end   
  # // Contents Fading
  def contents_fadein_time
    20
  end
  def contents_fadeout_time
    20
  end
  # // 02/19/2012 [
  def fadein_contents()
    @fade_con_in  = true
    @fade_con_out = false
  end
  def fadeout_contents()
    @fade_con_in  = false 
    @fade_con_out = true 
  end
  # // . x.  Add this to your window's update method to enable contents fading
  def update_contents_fading()
    update_fadein_contents() if @fade_con_in
    update_fadeout_contents() if @fade_con_out
  end  
  def update_fadein_contents()
    self.contents_opacity = (self.contents_opacity+(255.0/contents_fadein_time)).min(255)
    @fade_con_in = false if self.contents_opacity == 255
  end
  def update_fadeout_contents()
    self.contents_opacity = (self.contents_opacity-(255.0/contents_fadeout_time)).max(0)
    @fade_con_out = false if self.contents_opacity == 0
  end
  # // Manual . x .
  def fadein_contents_c() 
    fadein_contents() if self.contents_opacity < 255
  end  
  def fadeout_contents_c() 
    fadeout_contents() if self.contents_opacity > 0
  end  
  def start_fadein_contents()
    self.contents_opacity = 0
    fadein_contents()
  end
  def start_fadeout_contents()
    self.contents_opacity = 255
    fadeout_contents()
  end
  # ] \\ 02/19/2012
  # // Window Addon Patch
  alias :winad_initialize :initialize
  def initialize(*args,&block)
    winad_initialize(*args,&block)
    init_addons()
  end
  alias :winad_dispose :dispose
  def dispose(*args,&block)
    dispose_addons()
    winad_dispose(*args,&block)
  end
  alias :winad_update :update
  def update(*args,&block)
    winad_update(*args,&block)
    update_addons()
  end  
  def contents_x
    self.x + standard_padding 
  end  
  def contents_y
    self.y + standard_padding
  end 
  def contents_rect
    Rect.new(contents_x, contents_y, 
     self.width-standard_padding,self.height-standard_padding-padding_bottom)
  end  
  def init_addons()   ; end unless method_defined?( :init_addons )
  def dispose_addons(); end unless method_defined?( :dispose_addons )
  def update_addons() ; end unless method_defined?( :update_addons )
  def win_busy?()
    false
  end unless method_defined?(:win_busy?)  
  def mouse_in_window?()
    MouseEx.in_area?(self.to_rect)
  end
  # // 02/27/2012 - Active Fading
  alias :actfade_update :update
  def update(*args,&block)
    actfade_update(*args,&block)
    update_active_fading() if active_fading?()
  end
  def update_active_fading()
    active2fade?() ? active_fadein() : active_fadeout()
  end  
  def active_fadein()
    self.opacity = (self.opacity+(255/active_fadein_time)).clamp(active_fade_min(),active_fade_max())
  end
  def active_fadeout()
    self.opacity = (self.opacity-(255/active_fadein_time)).clamp(active_fade_min(),active_fade_max())
  end  
  def active_fadein_time()
    20.0
  end
  def active_fadeout_time()
    20.0
  end  
  def active_fade_min()
    198
  end  
  def active_fade_max()
    255
  end  
  def active_fading?()
    false
  end
  def active2fade?()
    active
  end  
end  
