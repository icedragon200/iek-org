# Scene::Rogue (Debug)
class Scene::Rogue < Scene::Base
  def add_return( retproc )
    warn "Obosolete function, #{__method__} was called"
    @return_stack << retproc
    return self
  end  
  def pop_return()
    @return_stack.pop
  end  
  def do_return( code=0 )
    File.open("OFC.log","w+"){|f|f.puts caller(0)}
    warn "Obsolete Function Called check OFC.log"
    return 
    rt = pop_return()
    return rt ? rt.call( code ) : false
  end
  
  def draw_contracted_rooms()
    _map.rooms.each { |r|
      arra = MapManager.rect_array_to_pos( r.floor_rect_map.to_a )
     #_map.add_ranges( arra, Color.new( 198, 98, 0 ).to_flash )
      arra = MapManager.rect_array_to_pos( r.floor_rect_map.contract(1).to_a )
      _map.remove_ranges( arra )
      arra = (0..90).to_a.collect { |i| r.random_floor_pos( 1 ) }
      _map.add_ranges( arra, Color.new( 32, 32, 32 ).to_flash )
    }
  end  
  def draw_room_floor
    room = _map.room_at( @subject.x, @subject.y )
    unless room.nil?()
      arra = MapManager.rect_array_to_pos( room.floor_rect_map.to_a )
      _map.add_ranges( arra, Color.new( 198, 98, 0 ).to_flash )
    end
    wait(30)
  end 
end  
