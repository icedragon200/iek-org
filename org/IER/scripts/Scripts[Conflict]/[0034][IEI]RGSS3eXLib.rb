# [IEI]RGSS3eXLib
#==============================================================================#
# ♥ IEI - Xpansion Library
#==============================================================================#
# // • Created By    : IceDragon
# // • Data Created  : 12/06/2011
# // • Data Modified : 04/12/2012
# // • Version       : 1.0
#==============================================================================#
# ● Functions
#==============================================================================#
# ♥ - Class
# ■ - Module
#==============================================================================#
# ■ Graphics
# ■ Input
# ♥ Font
# ♥ Color
# ♥ Tone
#==============================================================================#
module Graphics
  #--------------------------------------------------------------------------#
  # ● new-module-method :rect
  #/------------------------------------------------------------------------\#
  # ● Return
  #     Rect
  #\------------------------------------------------------------------------/#
  def self.rect
    Rect.new( 0, 0, width, height )
  end unless method_defined? :rect 
  #--------------------------------------------------------------------------#
  # ● new-module-method :play_rect
  #/------------------------------------------------------------------------\#
  # ● Return
  #     Rect
  #\------------------------------------------------------------------------/#
  def self.play_rect
    Rect.new( 0, 0, playwidth, playheight )
  end unless method_defined? :play_rect
  #--------------------------------------------------------------------------#
  # ● new-module-method :frames_to_sec
  #/------------------------------------------------------------------------\#
  # ● Parameters
  #     frames (Numeric)
  # ● Return
  #     Float
  #\------------------------------------------------------------------------/#
  def self.frames_to_sec( frames )
    frames / frame_rate.to_f
  end    
end 
module Input
  def self.trigger_any?(*args)
    args.any? { |k| trigger?(k) }
  end  
  def self.trigger_all?(*args)
    args.all? { |k| trigger?(k) }
  end  
  def self.repeat_any?(*args)
    args.any? { |k| repeat?(k) }
  end  
  def self.repeat_all?(*args)
    args.all? { |k| repeat?(k) }
  end  
  def self.press_any?(*args)
    args.any? { |k| press?(k) }
  end  
  def self.press_all?(*args)
    args.all? { |k| press?(k) }
  end  
  # // Mouse Checks O:
  def self.mtrigger_any?(*args)
    args.any? { |k| mtrigger?(k) }
  end  
  def self.mtrigger_all?(*args)
    args.all? { |k| mtrigger?(k) }
  end  
  def self.mrepeat_any?(*args)
    args.any? { |k| mrepeat?(k) }
  end  
  def self.mrepeat_all?(*args)
    args.all? { |k| mrepeat?(k) }
  end  
  def self.mpress_any?(*args)
    args.any? { |k| mpress?(k) }
  end  
  def self.mpress_all?(*args)
    args.all? { |k| mpress?(k) }
  end 
end
# //
class Color
#==============================================================================#
# ◙ Colors
#==============================================================================#  
  Black = Pallete[:black]
  White = Pallete[:white]
  ConsoleBack = Black.dup
  ConsoleText = Color.new( 192, 192, 192 )
  include Mixin::ColorExpansion
  module_eval(INC_EVAL)
  def new_fs()
    Color.new.set(self)
  end 
  def to_l()
    return blue.to_i|(green.to_i<<8)|(red.to_i<<16)|(alpha.to_i<<24)
  end 
  # // 02/19/2012
  def to_tone()
    Tone.new(*self.to_a_na)
  end  
  ConsoleBack.xset(:alpha=>198)
end 
class Tone
  include Mixin::ColorExpansion
  #--------------------------------------------------------------------------#
  # ♦ Constant(s)
  #--------------------------------------------------------------------------#  
  ALPHA_SYM    = :gray
  ALPHA_SYM_S  = :gray=
  RGB_SYM      = [RED_SYM, GREEN_SYM, BLUE_SYM]
  RGBA_SYM     = RGB_SYM + [ALPHA_SYM]
  UP_LIMIT     = 255
  DOWN_LIMIT   = -255
  RESULT_OBJ   = Tone
  module_eval(INC_EVAL)
end
module Mixin::FontStyles
  @@styles = {}
  @@styles[:default] = {}
  @@styles[:simple_black] = {
    :color     => Color::Black,
    :shadow    => false,
    :outline   => false,
    :size      => Font.default_size - 8,
  }
  @@styles[:simple_brown1] = @@styles[:simple_black].merge( {
    :color     => Pallete[:brown1]
  } )
  @@styles[:big_number] = {
    :color     => Pallete[:brown3],
    :out_color => Pallete[:brown1],
    :shadow    => false,
    :outline   => true,
    :bold      => true,
    :size      => 20,
  }
  @@styles[:paper_text] = {
    :color     => Pallete[:paper3],
  }
  @@styles[:window_header] = {
    :color     => Pallete[:brown1],
    :shadow    => false,
    :outline   => false,
    :bold      => true,
    :size      => Font.default_size - 8,
  }
  @@styles[:window_header2] =  @@styles[:window_header].merge( {
    :color     => Pallete[:brown2]
  } )
  @@styles[:button_white] = {
    :color     => Pallete[:white],
    :shadow    => false,
    :italic    => false,
    :outline   => true,
    :bold      => true,
    :size      => Font.default_size - 8
  }
  @@styles[:console1] = {
    :color     => Color::ConsoleText,
    :shadow    => false,
    :italic    => false,
    :outline   => false,
    :bold      => false,
    :name      => ["ProggySmallTT","Arial"],
    :size      => 10
  }
  attr_reader :style
  def style=( style_data )
    @style = style_data
    refresh_style()
  end  
  def refresh_style()
    @style ||= STYLES[:default]
    self.name      = @style[:name].nil?()      ? Font.default_name() : @style[:name].clone
    self.size      = @style[:size].nil?()      ? Font.default_size() : @style[:size]
    self.bold      = @style[:bold].nil?()      ? Font.default_bold() : @style[:bold] 
    self.italic    = @style[:italic].nil?()    ? Font.default_italic() : @style[:italic] 
    self.shadow    = @style[:shadow].nil?()    ? Font.default_shadow() : @style[:shadow]
    self.outline   = @style[:outline].nil?()   ? Font.default_outline() : @style[:outline]
    self.color     = @style[:color].nil?()     ? Font.default_color() : @style[:color].clone
    self.out_color = @style[:out_color].nil?() ? Font.default_out_color() : @style[:out_color].clone
  end  
  def set_style( style_name )
    self.style = @@styles[style_name] || @@styles[:default]
    self
  end
  # // New From Self
  def nfs()
    f = Font.new()
    f.set(self)
    # // Clone all references . x . That way its UNIQUE XD
    f.name      = [].replace(f.name)
    f.color     = Color.new(*f.color.to_a)
    f.out_color = Color.new(*f.out_color.to_a)
    # // . x . Pass up the newly created Font
    f
  end  
end 
class Font
  include Mixin::FontStyles
end  
class Table
  include Mixin::TableExpansion
end  
class Sprite
  # // 01/20/2012
  def center_oxy(nx=0,ny=0)
    self.ox = (self.width - nx) / 2
    self.oy = (self.height - ny) / 2
  end  
  # // 01/30/2012
  def dispose_bitmap()
    self.bitmap.dispose unless self.bitmap.disposed? if self.bitmap
    self.bitmap = nil
  end
  # // 03/03/2012
  SPRITE_DEBUG = false
if SPRITE_DEBUG   
  @@sprites = [] 
  alias :spr_debug_initialize :initialize
  def initialize(*args,&block)  
    spr_debug_initialize(*args,&block)  
    puts "Created Sprite: #{self}"
    @@sprites.push(self)
  end
  alias :spr_debug_dispose :dispose
  def dispose(*args,&block)  
    puts "Disposed Sprite: #{self}"
    @@sprites.delete(self)
    puts "Remaining Sprites: #{@@sprites.size}"
    spr_debug_dispose(*args,&block)
  end
end  
end
class Bitmap
  @@__test_bmp = nil
  def self.prep_test_bmp
    @@__test_bmp = Bitmap.new(*Graphics.rect.xto_a(:width,:height)) if @@__test_bmp.nil? || @@__test_bmp.disposed?
    @@__test_bmp
  end
  # // 02/24/2012
  def blur_ex(n=1)
    n.times { blur() }
  end  
  # // 03/01/2012  
  BITMAP_DEBUG = false
if BITMAP_DEBUG  
  @@bitmaps = []
  alias :bmp_debug_initialize :initialize
  def initialize(*args,&block)  
    bmp_debug_initialize(*args,&block)  
    puts "Created Bitmap: #{self}"
    @@bitmaps.push(self)
  end
  alias :bmp_debug_dispose :dispose
  def dispose(*args,&block)  
    puts "Disposed Bitmap: #{self}" + (@creator_id ? "Created by: #{CID_STR[@creator_id]}" : "")
    @@bitmaps.delete(self)
    puts "Remaining Bitmaps: #{@@bitmaps.size}"
    bmp_debug_dispose(*args,&block)
  end
end  
  # // 04/15/2012
  def draw_circle(circle,color,fill=false)
    x, y = nil, nil
    mcircle = circle.clone
    for i in 0...360
      unless(fill)
        x, y = circle.get_angle_xy(i)
        set_pixel(x.ceil,y.ceil,color) 
      else  
        circle = mcircle.clone
        for r in 0...mcircle.radius
          circle.radius = r
          x, y = circle.get_angle_xy(i)
          set_pixel(x.ceil,y.ceil,color) 
        end  
      end  
    end
  end    
end 
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
