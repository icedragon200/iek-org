# [IEI]Gallery
require 'C:/Lib/IEI/lib/Gallery'
module IEI::Gallery
  # // CG[id,"CG-Name","filename"]
  # // CG[id,"CG-Name","filename",switch_id]
  # // CG[id,"CG-Name",["filename","filename","filename"],switch_id]
  do_false = proc { false }
  @sets["Dawn"]     = [
    CG[0,"Earthen: Dawn of Smiths","cg_earthen000"],
    CG[1,"Earthen","cg_earthen001",101,&do_false],
    CG[2,"Earthen","cg_earthen002",102,&do_false],
    CG[3,"Earthen","cg_earthen003",103,&do_false]
  ]
  @sets["Daybreak"] = [
  ]
  @sets["Dusk"]     = [
  ]
end
