# [IEI]BitmapEx
# // 04/15/2012
# // 04/15/2012
class Bitmap
  def rrecolor(f_color,t_color=nil)
    if(f_color.is_a?(Color) && t_color.is_a?(Color))
      hsh = { f_color => t_color }
    elsif(f_color.is_a?(Array) && t_color)
      arra = t_color.is_a?(Enumerable) ? t_color : [t_color]*f_color.size 
      hsh = {};f_color.each_with_index{|c,i|hsh[c]=arra[i]}
    else  
      hsh = f_color
    end  
    x,y,color = nil,nil,nil
    iterate_do { |x,y,color| hsh[color]||color }
  end  
  def palletize()
    pallete = Set.new();x,y,color = nil,nil,nil
    iterate_do(true) {|x,y,color|pallete<<color.to_a}
    pallete.to_a.sort.collect{|a|Color.new(*a)}
  end  
  def iterate_do(return_only=false)
    x, y = nil, nil
    if(return_only)
      for y in 0...height
        for x in 0...width
          yield(x,y,get_pixel(x,y))
        end
      end  
    else
      for y in 0...height
        for x in 0...width
          set_pixel(x,y,yield(x,y,get_pixel(x,y)))
        end
      end  
    end  
  end  
end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
=begin
sprites = Array.new(3) { Sprite.new }
sprites[0].bitmap = Cache.character("Baron_8D").clone
sprites[1].bitmap = sprites[0].bitmap.clone
pallete = sprites[0].bitmap.palletize()
sprites[2].bitmap = Bitmap.new(pallete.size,1)
sprites[2].zoom_x = sprites[2].zoom_y = 4.0
sprites[1].src_rect.set(sprites[0].src_rect.set(0,0,96,128))
sprites[0].salign(0,1)
sprites[1].salign(1,1)
sprites[2].salign(0,0)
pallete.each_with_index{|c,i|sprites[2].bitmap.set_pixel(i,0,c)}
=end
=begin
sprites = Array.new(3) { Sprite.new }
sprites[0].bitmap = Bitmap.new(*Graphics.rect.xto_a(:width,:height))
rects = sprites[0].bitmap.rect.split_by(2,2)
sprites[0].bitmap.fill_rect(rects[0], Pallete[:sys_red])
sprites[0].bitmap.fill_rect(rects[1], Pallete[:sys_green])
sprites[0].bitmap.fill_rect(rects[2], Pallete[:sys_blue])
sprites[0].bitmap.fill_rect(rects[3], Pallete[:sys_orange])
timethen = Time.now
pallete = sprites[0].bitmap.palletize()
puts "Pallete Time:"
timenow = Time.now
puts timethen
puts timenow
puts timenow - timethen
# // Draw
sprites[2].bitmap = Bitmap.new(pallete.size,1)
pallete.each_with_index{|c,i|sprites[2].bitmap.set_pixel(i,0,c)}

timethen = Time.now
sprites[0].bitmap.rrecolor(
  [Pallete[:sys_red],Pallete[:sys_green],Pallete[:sys_blue],Pallete[:sys_orange]],
  [Pallete[:white],Pallete[:grey16],Pallete[:brown1],Pallete[:paper2]]
)
puts "Recolor Time:"
timenow = Time.now
puts timethen
puts timenow
puts timenow - timethen
#Main.init
MouseEx.init
loop do
  Main.update
  sprites.each{|s|s.update}
end  
=end
