# Window::RogueStatus
#==============================================================================#
# ♥ Window::RogueStatus
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/10/2011
# // • Data Modified : 12/12/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/10/2011 V1.0 
#          Created Script
#
#     ♣ 12/12/2011 V1.0 
#
#==============================================================================#
class Window::RogueStatus < Window::Selectable
  attr_reader :battler
  attr_reader :draw_mode
  def initialize( x, y, w=nil, h=nil )
    @draw_mode = standard_draw_mode # // 0-normal, 1-mini, 2-full, 3-menu
    w ||= window_width ; h ||= window_height 
    super( x, y, w, h )
  end
  def standard_draw_mode()
    0
  end  
  def draw_mode=( new_draw_mode )
    if @draw_mode != new_draw_mode
      set_draw_mode(new_draw_mode)
      refresh_window_size()
      refresh()
    end  
  end 
  def set_draw_mode(n)
    @draw_mode = n
  end  
  def battler=( new_battler )
    if new_battler != @battler
      set_battler(new_battler)
      refresh()
    end  
  end 
  def set_battler(n)
    @battler = n
  end  
  def actor
    battler
  end
  def actor=( new_actor )
    self.battler = new_actor
  end 
  def set_actor(n)
    set_battler(n)
  end
  def refresh_window_size()
    self.width, self.height = window_width, window_height 
    create_contents()
  end  
  def window_width
    return Graphics.width 
  end
  def window_height
    case @draw_mode 
    when 0 ; (standard_padding * 2) + (24 * 2) 
    when 1 ; (standard_padding * 2) + 24
    when 2 ; Graphics.height
    when 3 ; fitting_height(4) 
    end
  end 
  def set_position( pos )
    if @position != pos
      @position = pos
      case @position
      when 0 # // Top
        self.y = 0
      when 1 # // Middle
        self.y = (Graphics.height - self.height) / 2
      when 2 # // Bottom
        self.y = Graphics.height - self.height
      end  
    end  
  end  
  def update
    super()
    update_contents_fading() # // 02/19/2012
  end  
  def refresh()
    contents.clear()
    self.send("refresh_mode#{@draw_mode}")
    start_fadein_contents()
  end  
  # // Battle Status
  def refresh_mode0
    return if @battler.nil?()
    art = open_artist()
    hw = contents.width / 2
    bw = contents.width / 4
    dx = 0
    dy = 0
    wd = art.draw_actor_element( @battler, dx, dy, true, false ).width
    contents.font.set_style( :default )
    art.draw_actor_name( @battler, dx + wd, dy )
    art.draw_actor_level( @battler, hw - 56, dy )
    art.draw_battler_exp( 
      :battler => @battler,
      :x => hw - (56 + 72),
      :y => dy + 4, 
      :height => 16,
      :width => 72 
    )
    art.draw_battler_hp( 
      :battler => @battler,
      :x => hw + (bw * 0), 
      :y => dy+4, 
      :height => 16, 
      :width => bw 
    )
    art.draw_battler_mp(
      :battler => @battler,
      :x => hw + (bw * 1), 
      :y => dy+4, 
      :height => 16, 
      :width => bw/2 
    )
    art.draw_battler_wt(
      :battler => @battler,
      :x => hw + (bw * 1) + (bw/2), 
      :y => dy+4, 
      :height => 16, 
      :width => bw/2 
    )
    dy += line_height
    art.draw_horz_line( dy )
    art.draw_actor_icons( @battler, dx, dy, contents.width / 4 ) 
    dx = (contents.width / 4)
    contents.font.size = Font.default_size
    for i in 0...6
      art.draw_parameter( @battler, dx+((i%8)*64), dy+((i/8)*12), 2+i )
    end  
  end  
  # // Short Status
  def refresh_mode1
    return if @battler.nil?()
    art = open_artist()
    bw = contents.width / 6
    dx, dy = 0, 0
    wd = art.draw_actor_element( @battler, dx, dy, true, false ).width
    art.draw_horz_line( dy + 20 )
    contents.font.size = Font.default_size
    art.draw_actor_name( @battler, dx+wd, dy )
    dx += wd + 96
    art.draw_battler_hp( 
      :battler        => @battler,
      :x              => dx, 
      :y              => dy, 
      :height         => 10, 
      :width          => bw,
      :dont_draw_text => true
    )
    dy += 10
    art.draw_battler_mp(
      :battler        => @battler,
      :x              => dx, 
      :y              => dy, 
      :height         => 10, 
      :width          => bw,
      :dont_draw_text => true
    )
  end 
  # // Full Status
  def refresh_mode2()
    return if @battler.nil?()
    art = open_artist()
    dx, dy = 0, 0
    cw2 = contents.width / 2
    cw4 = contents.width / 4
    cw8 = contents.width / 8
    drawing_sandbox() do  
      r = art.draw_actor_element( @battler, dx, dy )
      dx += 40
      #draw_actor_name( @battler, dx, dy )
      contents.font.set_style(:simple_black)
      clsname = format("%s%s: %s", Vocab.level_a,@battler.level,@battler.class.name)
      art.draw_fract(dx,dy,@battler.name,clsname,96,r.height/2)
      dx = cw2
      dx += art.draw_element_icon(DB.element_id(:hp),dx,dy).width
      dx += art.draw_fract(dx, dy, @battler.hp, @battler.mhp,32,r.height/2).width+4
      dx += art.draw_element_icon(DB.element_id(:mp),dx,dy).width
      dx += art.draw_fract(dx, dy, @battler.mp, @battler.mmp,32,r.height/2).width+4
      dx += art.draw_element_icon(DB.element_id(:wt),dx,dy).width
      w,h = art.draw_fract(dx, dy, @battler.wt, @battler.mwt,32,r.height/2).xto_a(:width,:height)
      dy = dy+h+2
      art.draw_horz_line(dy)
      dy += 2
      eqs = @battler.equips
      dx = 0
      for i in 0...eqs.size
        art.draw_actor_equip_at_ex(@battler,i,Rect.new(dx+((i%2)*cw4),dy+((i/2)*40),cw4-8,40))
      end 
      sks = @battler.skills
      skill, rect, set = nil, nil, nil
      dy2 = dy + (4 * 42)
      for i in 0...sks.size
        skill = sks[i]
        rect  = Rect.new(dx,dy2+(i*24),cw4-8,24)
        set   = @battler._hot_keys.obj_set?(skill)
        art.draw_skill_ex(skill,rect,(set && !skill.nil?()) ? 4 : 0)
      end 
      dx += cw4
      art.draw_actor_arts(@battler,Rect.new(dx,dy2,cw4-8,line_height))
      dx = cw2
      art.draw_actor_parameters_ex(@battler,dx,dy,cw8-8,14,1,2...8)
      dx, dy = dx + cw8, dy
      art.draw_actor_element_rates(@battler,dx,dy,32,160)#cw4+cw8-32) 
      dx, dy = 0, contents.height - 48
      art.draw_horz_line(dy)
    end # // drawing_sandbox()  
  end  
  # // Menu Status
  def refresh_mode3()
    return if @battler.nil?()
    art = open_artist()
    dx, dy = 0, 0
    drawing_sandbox do
      rect = art.draw_actor_element( @battler, dx, dy, true, true )
      contents.font.set_style(:simple_black)
      dspacing = 4
      dcols    = 3
      bw,bh= (contents.width-(dspacing*dcols))/dcols,20#24*5, 20
      art.draw_fract(rect.vx2,dy,@battler.name,@battler.class.name,bw-rect.width,rect.height/2)
      art.draw_tiny_actor(@battler,rect.vx2,dy+rect.height-24,24)
      art.draw_horz_line(dy+rect.height,0,bw)
      dy  += rect.height
      contents.font.set_style(:default)
      dy  += (contents.height - rect.height) - (bh*3)
      art.draw_battler_exp( 
        :battler => @battler,
        :x       => dx,
        :y       => dy, 
        :height  => bh,
        :width   => bw 
      )
      dy += bh
      art.draw_battler_hp( 
        :battler        => @battler,
        :x              => dx, 
        :y              => dy, 
        :height         => bh, 
        :width          => bw,
        :dont_draw_text => false
      )
      dy += bh
      art.draw_battler_mp(
        :battler        => @battler,
        :x              => dx, 
        :y              => dy, 
        :height         => bh, 
        :width          => bw,
        :dont_draw_text => false
      )
      dy  = 0
      dx  = bw + dspacing
      art.draw_actor_parameters_ex(@battler,dx,dy,bw/2,14,2,2...8)
      dy  = contents.height - 24
      art.draw_actor_icons(@battler,dx+((bw%24)/2),dy,bw) 
      dy  = 0
      dx  = (bw + dspacing) * 2
      art.draw_actor_arts(@battler,Rect.new(dx,dy,bw,line_height))
    end  
  end  
  def standard_artist
    Artist::RogueStatus
  end  
end
class Artist::RogueStatus < Artist
  # // 02/27/2012
  def draw_actor_parameters_ex(actor,x,y,w=64,h=14,col=1,params=0...8)
    ps = params.to_a
    for i in 0...ps.size
      draw_parameter(actor,x+((i%col)*w),y+((i/col)*h),ps[i])
    end  
  end  
  # // c*   = cell*
  # // data = {:rate=>Float(0.0..1.0),:color=>Color}
  def draw_table(data,x,y,cwidth,cheight,spacing=0,orn=0)
    dx, dy = nil,nil
    data.each_with_index do |hsh,i| 
      rate, color = hsh.get_values(:rate,:color)
      case(orn)
      when 0 # // Horz
        dx = x
        dy = y + ((cheight+spacing)*i) 
      when 1 # // Vert
        dx = x + ((cwidth+spacing)*i) 
        dy = y
      end      
      ext_draw_bar2( 
        {
          :x      => dx,
          :y      => dy,
          :width  => cwidth,
          :height => cheight,
          :rate   => rate.clamp(0.0,1.0),
          #:divisions => divs,
          #:spacing => spacing,
          :orn    => orn,
          :gaugem => orn
          #:padding => bpadding
        }.merge(DrawExt.quick_bar_colors(color))
      )
      yield dx, dy, cwidth, cheight, i, hsh if(block_given?())
    end  
  end  
  def draw_actor_element_rates(actor,x,y,width=128,height=30*6)
    data = []
    for i in 1..6
      rate  = actor.element_rate(i)
      mrate = (rate / 2.0)
      c     = element_color(i).add((mrate-1.0).clamp(0.0,1.0))
      hsh = {
        :rate  => mrate,
        :color => c
      }
      data << hsh
      #draw_actor_element_rate(actor,i,x,y+(32*(i-1)),width)
    end  
    draw_table(data,x,y,width,height,2,1) { 
      |zx,zy,zw,zh,i,hsh|
      draw_ruler(zx,zy,zw,zh,20,2,1)
      draw_element_icon(i+1,zx,zy+zh,true,zw>=32) if(zw >= 24)
    }
  end  
  def draw_actor_element_rate(actor,element_id,x,y,width=128,enabled=true)
    draw_element_rate(element_id,actor.element_rate(element_id),x,y,width,enabled)
  end  
  def draw_element_rate(element_id,rate,x,y,width=128,enabled=true)
    r = draw_element_icon(element_id,x,y,enabled,true)
    r.offset_horz!()
    r.width = width - r.width
    r2 = r.squeeze((r.height*0.50).to_i,false,2)
    r3 = r2.dup
    mrate = (rate / 2.0)
    c = element_color(element_id).add((mrate-1.0).clamp(0.0,1.0))
    divs = 20
    spacing = 1
    bpadding = 1 # // Bar Padding . x .
    #r3.height *= 0.3 # // 4 :get_bar4_bar 4win XD
    r3.height *= 0.70
    bw = DrawExt.adjust_size4bar3(r3.width,divs,spacing,bpadding)
    bh = DrawExt.adjust_size4bar4(r3.height,bpadding)
    r3.y -= bh - r3.height
    ext_draw_bar2( 
      {
      :x      => r3.x,
      :y      => r3.y,
      :width  => bw,
      :height => bh,
      :rate   => mrate.clamp(0.0,1.0),
      :divisions => divs,
      :spacing => spacing,
      :padding => bpadding,
      #:barseg_method => DrawExt.method(:get_bar1_bar)
      }.merge(DrawExt.quick_bar_colors(c))
    )
    draw_ruler(r3.x, r3.y, bw, bh*0.8, divs, 5)
    #contents.fill_rect(r4,element_rate_color(element_id,rate.ceil)) if mrate > 0
    text = format("Weakness: %.2fx",(rate.round(2))) # R: 1.00
    r4 = r2.offset_vert(-1); r4.width = bw
    drawing_sandbox do
      contents.font.set_style(:simple_black)
      contents.draw_text(r4,Vocab.element(element_id),0) #rescue nil
      contents.draw_text(r4,text,2) #rescue nil
    end
    r
  end  
  def draw_ruler(x,y,w,h,divs,sca=2,orn=0,c=Pallete[:black].xset(:alpha=>198))
    case(orn)
    when 0 ; sp = w / divs 
    when 1 ; sp = h / divs 
    end  
    dw, dx, dh, dy = [nil]*4
    for i in 0..divs
      long = (i % sca == 0)
      case(orn)
      when 0
        dh = long ? h : h * 0.6
        dy = long ? y : y + (h-dh)
        contents.fill_rect(x+(i*sp),dy,1,dh,c)
      when 1
        dw = long ? w : w * 0.6
        dx = long ? x : x + (w-dw)
        contents.fill_rect(dx,y+(i*sp),dw,1,c)  
      end  
    end  
  end   
  # // 
  # // 03/04/2012
  def draw_tiny_actor(actor,x,y,h=nil)
    draw_tiny_character( 
      :character_name  => actor.character_name,
      :character_index => actor.character_index, 
      :character_hue   => actor.character_hue,
      :x => x + 16, 
      :y => y + 32,  
      :height => h
    )
  end  
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
