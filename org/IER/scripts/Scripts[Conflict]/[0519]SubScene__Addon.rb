# SubScene::Addon
# // 02/03/2012
# // 02/03/2012
module SubScene::Addon # // Include in a normal Scene::* to enable SubScenes
  def initialize
    super()
    @sub_scene = nil
    @ss_fiber = nil
  end  
  def terminate()
    super()
    dispose_ss_fiber()
    dispose_subscene()
  end   
  def set_ss_and_uun(sc)
    set_subscene(sc)
    update_ss_uun()
  end  
  def set_subscene(sc) # // sc == SubScene::* class
    @sub_scene = sc.new()
    update_for_wait()
    @ss_fiber = Fiber.new { run_subscene }
  end  
  def run_subscene()
    @sub_scene.main()
    dispose_ss_fiber()
    dispose_subscene()
  end  
  def dispose_ss_fiber()
    @ss_fiber  = nil
  end  
  def dispose_subscene()
    @sub_scene.dispose() unless @sub_scene.disposed?() if @sub_scene
    @sub_scene = nil
  end  
  def update_subscene()
    @ss_fiber.resume() if @ss_fiber
  end  
  def update_ss_uun()
    Input.update
    until @ss_fiber.nil?()
      update_for_wait()
      update_subscene()
    end 
    Input.update
  end  
end  
