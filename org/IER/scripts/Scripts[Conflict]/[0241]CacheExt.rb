# CacheExt
# // 02/04/2012
# // 02/04/2012
module CacheExt
  def self.make_button(text,colors,width=16,height=16)
    bmp = Bitmap.new(width,height)
    bmp.ext_draw_bar2( { }.merge(colors) )
    bmp.font.set_style(:button_white)
    bmp.draw_text(0,0,width,height,text,1) unless text.empty?
    bmp
  end 
  def self.win_button_rect
    Rect.new(0,0,18,18) #Rect.new(0,0,14,14)
  end  
  def self.sys_button_rect
    Rect.new(0,0,48,16)
  end  
  def self.sys_buttons()
    if @sys_buttons.nil? || @sys_buttons.disposed?
      r = sys_button_rect.freeze
      @sys_buttons = Bitmap.new(r.width*4, r.height*3)
      [#[" " , [inactive_colors, active_colors, mouse_over]]
        ["[-Lock-]"  , [DrawExt::TRANS_BAR_COLORS, DrawExt::BLACK_BAR_COLORS, DrawExt::WHITE_BAR_COLORS, DrawExt::RUBY_BAR_COLORS]],
        ["[-Reset-]" , [DrawExt::TRANS_BAR_COLORS, DrawExt::BLACK_BAR_COLORS, DrawExt::WHITE_BAR_COLORS, DrawExt::BLUE_BAR_COLORS]],
      ].each_with_index do |n, i|
        yd = r.height * i
        n[1].each_with_index do |c,xi|
          xd = r.width * xi
          bmp = make_button(n[0],c,r.width,r.height)
          @sys_buttons.blt(xd,yd,bmp,bmp.rect)
          bmp.dispose() ; bmp = nil
        end  
      end 
    end
    @sys_buttons
  end
  # // 02/21/2012
  def self.black_box
    if @black_box.nil? || @black_box.disposed?
      @black_box = Bitmap.new(32,32)
      @black_box.fill_rect(@black_box.rect,Pallete[:black])
    end  
    @black_box
  end  
  @cache = {}
  @@bitmaps = {}
  @@bitmaps["header_base"] = proc do
    bmp = CacheExt["footer_base"].dup
    col = bmp.get_pixel(0,0)
    bmp.set_pixel(0,0,Pallete[:trans])
    bmp.set_pixel(bmp.width-1,0,Pallete[:trans])
    bmp.set_pixel(1,1,col)
    bmp.set_pixel(bmp.width-2,1,col)
    bmp
  end
  @@bitmaps["footer_base"] = proc do
    bmp = Bitmap.new(96,14)
    colors = [Pallete[:gray17],Pallete[:gray18]]
    bmp.gradient_fill_rect(bmp.rect,*colors,true)
    colors.collect! { |c| c.lighten(0.1) }
    bmp.gradient_fill_rect(bmp.rect.contract(1),*colors,true)
    bmp
  end  
  @@bitmaps["win_buttons"] = proc do
    r = win_button_rect.freeze
    target_bmp = Bitmap.new(r.width*4, r.height*3)
    [#[" " , [inactive_colors, active_colors, mouse_over]]
      ["-" , [DrawExt::TRANS_BAR_COLORS, DrawExt::BLACK_BAR_COLORS, DrawExt::WHITE_BAR_COLORS, DrawExt::BLUE_BAR_COLORS]],
      ["=" , [DrawExt::TRANS_BAR_COLORS, DrawExt::BLACK_BAR_COLORS, DrawExt::WHITE_BAR_COLORS, DrawExt::BLUE_BAR_COLORS]],
      ["x" , [DrawExt::TRANS_BAR_COLORS, DrawExt::BLACK_BAR_COLORS, DrawExt::RED_BAR_COLORS  , DrawExt::RUBY_BAR_COLORS]],
    ].each_with_index do |n, i|
      yd = r.height * i
      n[1].each_with_index do |c,xi|
        xd = r.width * xi
        bmp = make_button(n[0],c,r.width,r.height)
        target_bmp.blt(xd,yd,bmp,bmp.rect)
        bmp.dispose() ; bmp = nil
      end 
    end
    target_bmp
  end  
  def self.bitmap(name)
    name = name.downcase
    bmp = @cache[name]
    @cache[name] = nil if bmp.disposed? if bmp
    @cache[name] ||= @@bitmaps[name].call
  end  
  class << self
    alias :[] :bitmap
  end  
end  
