# ■ MixIn_Sprite_Effect (Ex)
#==============================================================================#
# ■ Mixin::Sprite_Effect (Ex)
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 01/16/2012
# // • Data Modified : 01/16/2012
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 01/16/2012 V1.0 
#
#==============================================================================# 
module Mixin::Sprite_Effect
  def start_effect(effect_type)
    @effect_type = effect_type
    @effect_handler = Handler::Effect.from_list( effect_type )
    if @effect_handler
      @effect_handler.visible = self.visible
    end  
    revert_to_normal
  end
  def update_effect
    if @effect_handler
      @effect_handler.update()
      self.color.set( *@effect_handler.color.to_a )
      self.opacity    = @effect_handler.opacity 
      self.blend_type = @effect_handler.blend_type
      self.visible    = @effect_handler.visible
      @character_visible = @effect_handler.cvisible
      self.x         += @effect_handler.x
      self.y         += @effect_handler.y
      self.z         += @effect_handler.z
      self.ox        += @effect_handler.ox
      self.oy        += @effect_handler.oy
      if @effect_handler.done?
        @effect_type = nil 
        @effect_handler = nil
      end  
    end      
  end
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
