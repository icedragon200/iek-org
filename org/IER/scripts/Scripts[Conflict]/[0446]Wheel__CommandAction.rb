# Wheel::CommandAction
#==============================================================================#
# ♥ Wheel::CommandAction
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/11/2011
# // • Data Modified : 12/11/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/11/2011 V1.0 
#
#==============================================================================#
class Wheel::CommandAction < Wheel::Command
  attr_reader :battler
  attr_reader :command_set_index
  def initialize( viewport=nil )
    super( 0, 0, viewport )
    self.x = (Graphics.width - radius) / 2
    self.y = (Graphics.height - radius) / 2
    @battler = nil
    @command_set_index = 0
    @command_sets = {
      :battle_commands => [:attack, :guard  , :nudge  , :skip  , :tame   ],#, :to_ai],
      :menu_commands   => [:skill , :item   , :equip  , :status, :hotkeys],
      :op_commands     => [:list  , :minimap, :options, :save            ],
    }
    @comset_a = [:battle_commands, :menu_commands, :op_commands]
    set_commands()
    refresh()
  end
  def command_set_index=( cindex )
    return if(@command_set_index == cindex)
    @command_set_index = cindex
    set_commands()
    self.index = 0
    refresh()
  end  
  def set_commands()
    @commands_name = @comset_a[@command_set_index]
    make_command_list()
  end  
  def index=( new_index )
    super( new_index )
    @battler.last_command = current_symbol if @battler
  end  
  def battler=( new_battler )
    return if(@battler == new_battler)
    @battler = new_battler
    @battler.last_command.nil?() ? self.index = 0 :
      select_symbol( @battler.last_command ) if @battler
  end  
  def make_command_list()
    @list = []
    rid = nil # // . x . Just declaring the variable
    @command_sets[@commands_name].each do |com|
      rid = DB.rogue_sym2id(com)
      add_command( Icon.rogue(rid), Vocab.rogue(rid), com )
    end
  end
  def wheel_up()
    Sound.play_cursor()
    self.command_set_index = (@command_set_index - 1) % @comset_a.size
  end
  def wheel_down()
    Sound.play_cursor()
    self.command_set_index = (@command_set_index + 1) % @comset_a.size
  end  
end 
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
