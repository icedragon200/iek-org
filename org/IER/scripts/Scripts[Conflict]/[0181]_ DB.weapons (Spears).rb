#  | DB.weapons (Spears)
# // 02/29/2012
# // 02/29/2012
module Database
def self.mk_weapons1()
  weapons = []
  wep_sym = :spears
#==============================================================================#
# ◙ Weapon (Spear)(Partisan)
#==============================================================================# 
  weapon              = Weapon.new()
  weapon.initialize_add()
  weapon.id           = 1
  weapon.name         = "Partisan"
  weapon.icon_index   = 0
  weapon.description  = 'A very difficult spear'
  weapon.features     = []
  weapon.note         = ""
  weapon.price        = 0
  weapon.etype_id     = 0 
  weapon.params       = params_from_growth(:base_spears)
  weapon.wtype_id     = 0
  weapon.animation_id = 0
  weapon.growths      = [:normal, :spears] 
  weapons[weapon.id] = weapon
#==============================================================================#
# ◙ Weapon (Spear)(Mermald)
#==============================================================================# 
  weapon              = Weapon.new()
  weapon.initialize_add()
  weapon.id           = 2
  weapon.name         = "Mermald"
  weapon.icon_index   = 0
  weapon.description  = ''
  weapon.features     = []
  weapon.note         = ""
  weapon.price        = 0
  weapon.etype_id     = 0 
  weapon.params       = params_from_growth(:base_spears)
  weapon.wtype_id     = 0
  weapon.animation_id = 0
  weapon.growths      = [:magic, :spears] 
  weapons[weapon.id] = weapon  
#==============================================================================#
# ◙ Weapon (Spear)(Hedgas)
#==============================================================================# 
  weapon              = Weapon.new()
  weapon.initialize_add()
  weapon.id           = 3
  weapon.name         = "Hedgas"
  weapon.icon_index   = 0
  weapon.description  = ''
  weapon.features     = []
  weapon.note         = ""
  weapon.price        = 0
  weapon.etype_id     = 0 
  weapon.params       = params_from_growth(:base_spears)
  weapon.wtype_id     = 0
  weapon.animation_id = 0
  weapon.growths      = [:swift, :spears] 
  weapons[weapon.id] = weapon  
#==============================================================================#
# ◙ Weapon (Spear)(Larzear)
#==============================================================================# 
  weapon              = Weapon.new()
  weapon.initialize_add()
  weapon.id           = 4
  weapon.name         = "Larzear"
  weapon.icon_index   = 0
  weapon.description  = ''
  weapon.features     = []
  weapon.note         = ""
  weapon.price        = 0
  weapon.etype_id     = 0 
  weapon.params       = params_from_growth(:base_spears)
  weapon.wtype_id     = 0
  weapon.animation_id = 0
  weapon.growths      = [:heavy, :spears] 
  weapons[weapon.id] = weapon  
#==============================================================================#
# ◙ Weapon (Spear)(Disperas)
#==============================================================================# 
  weapon              = Weapon.new()
  weapon.initialize_add()
  weapon.id           = 5
  weapon.name         = "Disperas"
  weapon.icon_index   = 0
  weapon.description  = ''
  weapon.features     = []
  weapon.note         = ""
  weapon.price        = 0
  weapon.etype_id     = 0 
  weapon.params       = params_from_growth(:base_spears)
  weapon.wtype_id     = 0
  weapon.animation_id = 0
  weapon.growths      = [:dual, :spears] 
  weapons[weapon.id] = weapon      
#==============================================================================#
# ◙ REMAP
#==============================================================================# 
  adjust_weapons(weapons,wep_sym)
  weapons.compact.each do |weapon|
    #weapon.atk_range.code     = MkRange::RANGE_LINE
    weapon.atk_range.range    = 6#2
    weapon.atk_range.minrange = 1
    weapon.effect_range.code  = MkRange::RANGE_LINE_TT
  end  
end  
end  
