#  | DB.skills (Water)
# // 02/24/2012
# // 02/24/2012
module Database
def self.mk_skills2() # // Water
  skills = []
  element = :water
#==============================================================================#
# ◙ Skill (Chillis)
#==============================================================================#   
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 1
  skill.name              = "Chillis"
  skill.icon_index        = 97
  skill.description       = "Basic Water Magic"
  skill.features          = []
  skill.note              = ""
  skill.scope             = @scope[:one_enemy]
  skill.occasion          = @occasion[:battle] #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = @hit_type[:magical]
  skill.animation_id      = 19
  skill.stype_id          = @skill_type_id[element]
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = @damage_type[:hp_dam]
  skill.damage.element_id = element_id(element)
  skill.damage.formula    = "3 * a.mat / 2"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skill.atk_range.range = 2
  skills[skill.id] = skill  
  add_skill2groups(skill,element,:lv1)
#==============================================================================#
# ◙ Skill (Artinite)
#==============================================================================#   
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 2
  skill.name              = "Artinite"
  skill.icon_index        = 97
  skill.description       = "Falling shards"
  skill.features          = []
  skill.note              = ""
  skill.scope             = @scope[:one_enemy]
  skill.occasion          = @occasion[:battle] #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = @hit_type[:magical]
  skill.animation_id      = 20
  skill.stype_id          = @skill_type_id[element]
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = @damage_type[:hp_dam]
  skill.damage.element_id = element_id(element)
  skill.damage.formula    = "3 * a.mat / 2"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skill.atk_range.range = 3
  skills[skill.id] = skill   
  add_skill2groups(skill,element,:lv2)
#==============================================================================#
# ◙ Skill (Frosten)
#==============================================================================#   
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 3
  skill.name              = "Frosten"
  skill.icon_index        = 97
  skill.description       = "Something just froze over"
  skill.features          = []
  skill.note              = ""
  skill.scope             = @scope[:one_enemy]
  skill.occasion          = @occasion[:battle] #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = @hit_type[:magical]
  skill.animation_id      = 21
  skill.stype_id          = @skill_type_id[element]
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = @damage_type[:hp_dam]
  skill.damage.element_id = element_id(element)
  skill.damage.formula    = "3 * a.mat / 2"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skill.atk_range.range = 4
  skills[skill.id] = skill 
  add_skill2groups(skill,element,:lv3)
#==============================================================================#
# ◙ Skill (Glacaos)
#==============================================================================#   
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 4
  skill.name              = "Glacaos"
  skill.icon_index        = 97
  skill.description       = "Sub Zero problems inbound"
  skill.features          = []
  skill.note              = ""
  skill.scope             = @scope[:one_enemy]
  skill.occasion          = @occasion[:battle] #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = @hit_type[:magical]
  skill.animation_id      = 22
  skill.stype_id          = @skill_type_id[element]
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = @damage_type[:hp_dam]
  skill.damage.element_id = element_id(element)
  skill.damage.formula    = "3 * a.mat / 2"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skill.atk_range.range = 3
  skills[skill.id] = skill  
  add_skill2groups(skill,element,:lv2)
#==============================================================================#
# ◙ Skill (Saear)
#==============================================================================#   
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 5
  skill.name              = "Saear"
  skill.icon_index        = 97
  skill.description       = "Sub Zero problems inbound"
  skill.features          = []
  skill.note              = ""
  skill.scope             = @scope[:one_enemy]
  skill.occasion          = @occasion[:battle] #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = @hit_type[:magical]
  skill.animation_id      = 2
  skill.stype_id          = @skill_type_id[element]
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = @damage_type[:hp_dam]
  skill.damage.element_id = element_id(element)
  skill.damage.formula    = "3 * a.mat / 2"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skill.atk_range.range = 3
  skills[skill.id] = skill 
  add_skill2groups(skill,element,:lv2)
#==============================================================================#
# ◙ Skill (Clear)
#==============================================================================#   
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 6
  skill.name              = "Clear"
  skill.icon_index        = 97
  skill.description       = "Cleaning Up"
  skill.features          = []
  skill.note              = ""
  skill.scope             = @scope[:one_ally]
  skill.occasion          = @occasion[:battle] #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = @hit_type[:magical]
  skill.animation_id      = 24
  skill.stype_id          = @skill_type_id[element]
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = @damage_type[:none]
  skill.damage.element_id = 0
  skill.damage.formula    = "0"
  skill.damage.variance   = 0
  skill.damage.critical   = false
  skill.effects << MkEffect.rem_state(2,1.0)
  skill.atk_range.range = 2
  skill.atk_range.minrange = 0
  skills[skill.id] = skill  
  add_skill2groups(skill,element,:lv1)
#==============================================================================#
# ◙ Skill (Flushin)
#==============================================================================#   
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 7
  skill.name              = "Flushin"
  skill.icon_index        = 97
  skill.description       = "Guardian Spirit Flushin"
  skill.features          = []
  skill.note              = ""
  skill.scope             = @scope[:global]
  skill.occasion          = @occasion[:battle] #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = @hit_type[:magical]
  skill.animation_id      = 2
  skill.stype_id          = @skill_type_id[element]
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = @damage_type[:none]
  skill.damage.element_id = 0
  skill.damage.formula    = "0"
  skill.damage.variance   = 0
  skill.damage.critical   = false
  skills[skill.id] = skill 
  add_skill2groups(skill,element,:lv4)
#==============================================================================#
# ◙ Skill (Valdine)
#==============================================================================#   
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 8
  skill.name              = "Valdine"
  skill.icon_index        = 97
  skill.description       = "Guardian Spirit Valdine"
  skill.features          = []
  skill.note              = ""
  skill.scope             = @scope[:user_team]
  skill.occasion          = @occasion[:battle] #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = @hit_type[:magical]
  skill.animation_id      = 2
  skill.stype_id          = @skill_type_id[element]
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = @damage_type[:hp_rec]
  skill.damage.element_id = 0
  skill.damage.formula    = "a.mat * 4"
  skill.damage.variance   = 0
  skill.damage.critical   = false
  skills[skill.id] = skill    
  add_skill2groups(skill,element,:lv4)
#==============================================================================#
# ◙ REMAP
#==============================================================================#   
  adjust_skills(skills,element)  
end
end
