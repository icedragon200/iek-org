# Container::Clock
# // 02/06/2012
# // 02/06/2012  
class Container::Clock < Container::Base
  include Container::Addons::OwnViewport
  include Container::Addons::Background_Glass
  include Container::Addons::Background_Menu
  #include Container::Addons::Background_Simple
  include Container::Addons::Contents
  #include Container::Addons::ContextMenu
  def context_menu_class
    Container::ClockMenu
  end  
  def set_cm_handlers
    @context_menu.set_handler(:color, method(:command_color))
    @context_menu.set_handler(:change, method(:command_change))
  end  
  def command_color()
    
  end
  def command_change()
    case @mode
    when :small
      change_mode(:big)
    when :big
      change_mode(:small)  
    end 
    @context_menu.hide()
    activate()
  end
  class AddonBin_CCLCK < WindowAddons::AddonBin[
                           WindowAddons::Header,
                           WindowAddons::Mouse_MoveWindow,
                           WindowAddons::Footer,
                           WindowAddons::Base_Tail]
    def header_text
      "Time"
    end
    def header_text_settings
      (hsh=super)["align"] = 1
      hsh
    end  
    def header_cube()
      super.xset(self.x,nil,nil,self.width)
    end 
  end  
  def addon_bin
    AddonBin_CCLCK
  end
  ClockMode = Struct.new(:width, :height)
  MODE_SETTINGS = {
    :small => ClockMode.new(192, 32),
    :big   => ClockMode.new(Graphics.width - 256, 72)
  }
  attr_reader :mode
  def initialize(x,y)
    @mode = standard_mode
    super(x,y,container_width,container_height)
    activate()
  end  
  def standard_mode
    :small
  end  
  def change_mode(n)
    @mode = n
    refresh_mode()
  end  
  def refresh_mode()
    set(x,y,container_width,container_height)
    _redraw!()
  end  
  def gbackground_tone
    Tone.new(*Pallete[:sys_red].to_a[0..2])#.set_gray(0)
  end  
  def container_width
    MODE_SETTINGS[@mode].width
  end
  def container_height
    MODE_SETTINGS[@mode].height
  end  
  def standard_padding
    0
  end  
  def update
    super()
    update_time() if self.active
  end  
  def update_time  
    t = Time.now
    timenow = [t.hour, t.min, t.sec]
    if @last_time != timenow
      @last_time = timenow
      contents.clear
      a = @last_time
      a[0] %= 12
      contents.font.size = self.height
      contents.draw_text(
        0,0,
        contents.width,contents.font.size+4,
        format("%02d:%02d:%02d", *a), 1
      )
    end  
  end  
end  
