# TweenTest
class Tween
  def self.run_test
    f = Sprite.new(nil)
    f.bitmap = Cache.system( "Ball" )
    @help_window = Window::SmallText.new( 0, 0, Graphics.playwidth, 32 )
    @index = 0
    @tweeners = []
    make_tweeners = proc { |sprite, easer|
      result = Tween.new( 
        [sprite.x, sprite.y],
        [sprite.width+rand(Graphics.width-(sprite.width * 2)), 
        sprite.height+rand(Graphics.height-(sprite.height * 2))],
        easer, Tween.frames_to_tt( 30 )  
      )
      #result << Tween.new( sprite.x, sprite.width+rand(544-(sprite.width * 2)), easer, 1 )
      #result << Tween.new( sprite.y, sprite.height+rand(416-(sprite.height * 2)), easer, 1 )
      result
    }
    loop {
      Graphics.update
      Input.update
      @tweener.update unless @tweener.done? if @tweener
      #@tweeners.each { |t| t.update }
      f.update 
      f.x, f.y = *@tweener.values if @tweener
      if Input.trigger?(:C)
        #@tweeners = make_tweeners.call( f, Tween::EASERS[@index].symbol )
        @tweener = make_tweeners.call( f, Tween::EASERS[@index].symbol )
      elsif Input.trigger?(:LEFT)
        @index -= @index - 1 < 0 ? -(Tween::EASERS.size - 1) : 1
      elsif Input.trigger?(:RIGHT)
        @index += @index + 1 == Tween::EASERS.size ? -@index : 1
      end
      @help_window.set_text(Tween::EASERS[@index].name, 1)
    }
  end
  def self.graph_test
    @graph = Sprite.new(nil)
    @graph.bitmap = Bitmap.new(*Graphics.rect.contract(32).xto_a(:width,:height))
    @graph.salign(1,1)
    @help_window = Window::SmallText.new( 0, 0, Graphics.playwidth, 32 )
    @indexes  = Array.new(2, 0)
    @tweeners = []
    @outsize = 48
    make_tweeners = proc { |sprite, easers|
      result = Tween::Multi.new()
      result.add_tween( [@outsize], [sprite.width-@outsize], easers[0], Tween.frames_to_tt(120) )
      result.add_tween( [@outsize], [sprite.height-@outsize], easers[1], Tween.frames_to_tt(120) )
      result
    }
    loop {
      Graphics.update
      Input.update
      unless @tweener.done? 
        @tweener.update 
        r = Rect.new(@tweener.value(0), @tweener.value(1), 2, 2)
        @graph.bitmap.fill_rect(r, Pallete[:white])
      end if @tweener
      if Input.trigger?(:C)
        @tweener = make_tweeners.call( @graph, @indexes.collect { |i| Tween::EASERS[i].symbol } )
        @graph.bitmap.fill_rect(@graph.bitmap.rect, Pallete[:sys_orange2])
        @graph.bitmap.fill_rect(@graph.bitmap.rect.contract(@outsize), Pallete[:sys_orange])
      elsif Input.trigger?(:UP)
        @indexes[0] = @indexes[0].pred.clamp(0,Tween::EASERS.size-1)
      elsif Input.trigger?(:DOWN)
        @indexes[0] = @indexes[0].succ.clamp(0,Tween::EASERS.size-1)  
      elsif Input.trigger?(:LEFT)
        @indexes[1] = @indexes[1].pred.clamp(0,Tween::EASERS.size-1)
      elsif Input.trigger?(:RIGHT)
        @indexes[1] = @indexes[1].succ.clamp(0,Tween::EASERS.size-1)  
      end
      t = format("Vertical : %s ---- Horizontal : %s", *@indexes.collect { |i| Tween::EASERS[i].name })
      @help_window.set_text(t, 1)
    }
  end  
end  
#Tween.graph_test
#Tween.run_test
