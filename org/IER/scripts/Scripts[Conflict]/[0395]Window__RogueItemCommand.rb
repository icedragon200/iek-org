# Window::RogueItemCommand
#==============================================================================#
# ♥ Window::RogueItemCommand
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/15/2011
# // • Data Modified : 12/15/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/15/2011 V1.0
#
#==============================================================================#
class Window::RogueItemCommand < Window::RogueObjCommand
  class AddonBin_WRIC < AddonBin_WRC
    def header_text
      return "#{Vocab.item} Options"
    end
  end  
  def addon_bin
    AddonBin_WRIC
  end
  def make_command_list()
    @list = []
    add_command( "Use"  , :use  , item_can?( @item, :use ) )
    add_command( "Throw", :throw, item_can?( @item, :throw ) )
    add_command( "Drop" , :drop , item_can?( @item, :drop ) )
    #add_command( "Equip", :equip, item_can?( @item, :equip ) )
    add_command( "Set"  , :set  , item_can?( @item, :set ) )
  end 
  def col_max 
    5
  end
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
