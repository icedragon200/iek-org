# Scene::File
#==============================================================================#
# ♥ Scene::File
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/31/2011
# // • Data Modified : 12/31/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/31/2011 V1.0 
#
#==============================================================================# 
class Scene::File < Scene::Base
  def start()
    super()
    create_background()
    create_savefiles_window()
    create_help_window()
    @help_window.set_text( help_window_text )
  end  
  def terminate()
    super()
    dispose_background()
  end  
  def update()
    super()
    update_background()
  end  
  def create_savefiles_window()
    @savefiles_window = Window::Files.new(0,0)
    @savefiles_window.salign(1,1)
    @savefiles_window.set_handler( :ok, method(:on_savefile_ok) )
    @savefiles_window.set_handler( :cancel, method(:on_savefile_cancel) )
    add_window( @savefiles_window )
  end  
  def create_help_window()
    @help_window = Window::Help.new( 
      @savefiles_window.x, @savefiles_window.vheight,
      @savefiles_window.width, 48
    )
    add_window(@help_window)   
  end  
  def init_selection
    @savefiles_window.index = first_savefile_index
  end
  def first_savefile_index
    return 0
  end  
  def help_window_text()
    return ""
  end 
  #--------------------------------------------------------------------------
  # ● セーブファイル［決定］
  #--------------------------------------------------------------------------
  def on_savefile_ok
  end
  #--------------------------------------------------------------------------
  # ● セーブファイル［キャンセル］
  #--------------------------------------------------------------------------
  def on_savefile_cancel
    Sound.play_cancel
    return_scene
  end
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
