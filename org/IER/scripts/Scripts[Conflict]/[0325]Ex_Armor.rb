# Ex_Armor
#==============================================================================#
# ♥ Ex_Armor
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 01/09/2012
# // • Data Modified : 01/09/2012
# // • Version       : 1.0
#==============================================================================#
class Ex_Armor < Ex_EquipItem
  def base_object
    $data_armors[@item_id]
  end 
  def is_armor?
    true
  end   
  def init_level
    super
    @max_level = DB::ARMOR_LEVELCAP
  end  
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
