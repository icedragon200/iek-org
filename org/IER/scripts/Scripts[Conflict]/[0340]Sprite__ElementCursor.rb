# Sprite::ElementCursor
# // 02/25/2012
# // 02/25/2012
class Sprite::ElementCursor < Sprite::LoopAnimation
  attr_reader :colors
  attr_reader :tones
  def initialize(viewport=nil,a=(1..6))
    super(viewport,"Heal5s",0...12,3)
    @colors = a.collect{|i|Pallete["element#{i}".to_sym].xset(:alpha=>255)}
    @tones  = @colors.collect{|c|c.dup.xset(:alpha=>255).lighten(0.2).to_tone.set_gray(0)}
    @lindex = @index  = 0
    @counter= 0
    self.size = 48.0
    self.blend_type = 1
  end 
  attr_reader :size
  def size=(n)
    @size = n.to_f
    self.zoom_x = self.zoom_y = @size / self.width
  end  
  def elements_size
    @colors.size
  end  
  attr_reader :index
  def index=(n)
    if @index != n
      @lindex = @index.modulo(elements_size)
      @index = n
      @counter = 0
    end  
  end  
  def pred_index(wrap=true)
    self.index = self.index.pred
    self.index = wrap ? self.index.modulo(elements_size) : self.index.clamp(0,elements_size-1)
  end
  def succ_index(wrap=true)
    self.index = self.index.succ
    self.index = wrap ? self.index.modulo(elements_size) : self.index.clamp(0,elements_size-1)
  end  
  def ltone
    @tones[@lindex]
  end
  def ttone
    @tones[@index]
  end
  def lcolor
    @colors[@lindex]
  end  
  def tcolor
    @colors[@index]
  end
  def update()
    super()
    @counter = (@counter + (255 / 20.0)).clamp(0,255)
    self.tone  = ltone.transition_to(ttone,@counter/255.0)
    self.color = lcolor.transition_to(tcolor,@counter/255.0)
  end  
end   
