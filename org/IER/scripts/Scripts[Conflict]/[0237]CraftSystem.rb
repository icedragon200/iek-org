# CraftSystem
# // 01/29/2012
# // 01/29/2012
module CraftSystem
  def self.craft()
  end 
  def self.item_symbol(item)
    return :ex_weapon if item.class == Ex_Weapon
    return :ex_armor if item.class == Ex_Armor
    return :item   if item.class == RPG::Item
    return :weapon if item.class == RPG::Weapon
    return :armor  if item.class == RPG::Armor
    return :nil
  end  
  def self.clean_item_symbol(sym)
    return :weapon if sym == :ex_weapon
    return :armor if sym == :ex_armor
    return sym
  end 
  def self.to_material(i)
    Database.item_to_material(i)
  end  
  def self.items_to_recipe(a)
    Database.mk_recipe(*(a.collect{|i|to_material(i)}))
  end  
  def self.recipe_match?(a)
    Database.craft_recipes.has_key?(a)
  end  
  def self.get_craft_from(a)
    recipe_match?(a) ? Database.craft_recipes[a] : nil
  end  
  # // Scene Craft
  def self.sc_get_craft_from(items)
    get_craft_from(items_to_recipe(items))
  end  
end  
