# Window::SmallText
#==============================================================================#
# ♥ Window::SmallText
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/12/2011
# // • Data Modified : 12/12/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/12/2011 V1.0 
#
#==============================================================================#
class Window::SmallText < Window::Base
  attr_reader :text
  attr_reader :align
  def initialize( x, y, width=96, height=32 )
    super( x, y, width, height )
    self.windowskin = Cache.system("Window_SmallBorder")
    @text = nil
    @align = nil
    @_def_align = 1
  end  
  def standard_padding
    4
  end
  def set_text( text="", align=@_def_align, forced=false )
    if @text != text  || @align != align || forced
      @text = text
      @align = align
      refresh()
    end  
  end 
  def refresh()
    contents.clear()
    contents.font.set_style( :simple_black )
    #contents.font.size = Font.default_size - 8
    open_artist.draw_text(0,0,contents.width,contents.height,@text,@align) 
  end  
end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
