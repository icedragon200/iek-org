# SystemButton
# // 02/04/2012
class Button_Toggle < GUIExt::Button_Base
  attr_reader :toggle_state
  def initialize(*args,&block)
    super(*args,&block)
    @toggle_state = false
  end  
  def on_mouse_left_click()
    super()
    toggle()
  end  
  def toggle
    @toggle_state = !@toggle_state
  end  
end  
class SystemButton < Button_Toggle
  def initialize(viewport,bindex=0)
    super(viewport,0,0,bindex)
    @org_button_index = @button_index
    @sprite.bitmap        = CacheExt.sys_buttons()
    r = CacheExt.sys_button_rect
    @sprite.set(bindex,*r.xto_a(:width,:height))
    self.width = r.width
    self.height = r.height
    #@over_color = Pallete[:white].xset(:alpha=>0)
    #@not_over_color = Pallete[:white].xset(:alpha=>0)
  end 
  attr_reader :org_button_index 
  def org_button_index=(n)
    @org_button_index = n
    self.button_index = @org_button_index
  end  
  def on_mouse_over()
    super()
    #self.button_index = @org_button_index + 2
  end  
  def on_mouse_not_over()
    super()
    #self.button_index = @org_button_index + 1
  end  
  def update
    super()
    #self.button_index = @org_button_index + 3 if @flash_counter > 0
    #self.button_index = @org_button_index if !self.active
    self.button_index = @org_button_index + (@toggle_state ? 3 : 2)
  end 
  def flash_time
    10
  end
end
