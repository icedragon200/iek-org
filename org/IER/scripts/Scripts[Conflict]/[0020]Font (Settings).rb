# Font (Settings)
#==============================================================================#
# ■ Font
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/08/2011
# // • Data Modified : 12/08/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/08/2011 V1.0 
#
#==============================================================================#
Font.default_name      = ["Verdana", "Arial", "Courier New"] #"Orator Std", "Mongolian Baiti" 
Font.default_size      = 20
Font.default_bold      = false
Font.default_italic    = false
Font.default_shadow    = false
Font.default_outline   = true
Font.default_color     = Color.new( 255, 255, 255, 255 )
Font.default_out_color = Color.new(  20,  20,  20, 128 )#96 )
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
