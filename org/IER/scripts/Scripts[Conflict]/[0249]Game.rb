# Game
# // 02/19/2012
# // 02/19/2012
class Game
  # // >.> Dont ask
  def initialize()
    newgame()
  end
  def newgame()
    reset()
  end
  def newgame_p() # // Newgame+
    @system.setup_newgame_p() # // >,> Not Implemented 
  end  
  def reset()
    @temp      = Game::Temp.new()
    @system    = Game::System.new()
    @switches  = Game::Switches.new()
    @variables = Game::Variables.new()
    @timer     = Game::Timer.new()
    @message   = Game::Message.new()
    @self_switches = Game::SelfSwitches.new()
    @actors    = Game::Actors.new()
    @party     = Game::Party.new()
    @troop     = Game::Troop.new()
    @map       = Game::Map.new()
    @rogue     = Game::Rogue.new()
    @player    = Game::Player.new()
    @settings  = Game::Settings.new()
  end  
  def pre_save()
    @temp = nil # // . x . You dont need to save the temp
  end  
  def post_save()
    @temp = Game::Temp.new() # // Remake temp on save end
  end  
  def pre_load() # // . x.  Kinda Impossible
  end  
  def post_load()
    @temp = Game::Temp.new() # // Remake temp on reload
  end  
  attr_reader :temp
  attr_reader :system
  attr_reader :switches
  attr_reader :variables
  attr_reader :timer
  attr_reader :message
  attr_reader :self_switches
  attr_reader :actors
  attr_reader :party
  attr_reader :troop
  attr_reader :map
  attr_reader :rogue
  attr_reader :player
  attr_reader :settings
end  
