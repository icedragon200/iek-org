# WindowAddons
#==============================================================================#
# ■ WindowAddons
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/15/2011
# // • Data Modified : 02/19/2012
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/15/2011 V1.0
#         Added
#           Header
#           ScrollBar
#
#     ♣ 12/15/2011 V1.0
#         Updated
#           Header
#
#     ♣ 12/15/2011 V1.0
#         Added
#           Devi::Header
#==============================================================================#
Devi.add("WindowAddons").set(
  # // User Config
  :enabled => true,
  # // Script Main Header
  :name          => 'Window Addons',
  :author        => 'IceDragon',
  :date_created  => Devi::Date.new(12,15,2011),
  # // Changing Stuff, Stuff that changes frequently
  :version       => '1.20',
  :date_modified => Devi::Date.new(6,5,2012)
)  
module WindowAddons ; end
class WindowAddons::AddonBin
  def self.[](*addons)
    ::Class.new(self) { addons.each { |a| include a } }
  end  
  attr_reader :parent
  def initialize(parent)
    @parent = parent
    init_addons()
  end  
  def update
    update_addons()
  end  
  def dispose()
    dispose_addons()
  end 
  def method_missing(sym,*args)
    @parent.send(sym,*args)
  end  
  def active_addons()
    []
  end
  def init_addons()
  end  
  def update_addons()
  end  
  def dispose_addons()
  end
  def addons_refresh_x()
  end
  def addons_refresh_y()
  end
  def addons_refresh_z()
  end
  def addons_refresh_opacity()
  end
  def addons_refresh_visible()
  end
  def addons_refresh_viewport()
  end
  def addons_refresh_openness()
  end
  def addons_refresh_active()
  end
  def header_space()
    14
  end  
  def tail_space()
    14
  end  
  HEADER_TAGS = [:header,:winbuttons,:tabs]
  TAIL_TAGS   = [:tail]
  # // Is there any form of header type addon installed? O_O;
  def header_addon?()
    (active_addons&HEADER_TAGS).empty?
  end  
  # // Is there any form of tailing type addon installed? @_@;
  def tail_addon?()
    (active_addons&TAIL_TAGS).empty?
  end
  
  attr_writer :can_move_window
  def can_move_window
    @can_move_window = false if @can_move_window.nil?
    @can_move_window
  end  
  def mouse_can_move_window?()
    @can_move_window
  end 
  
  def win_busy?()
    false
  end  
end  
module WindowAddons::Base 
  def addon_bin
    WindowAddons::AddonBin
  end  
  def init_addons()
    @addon_bin = addon_bin.new(self)
  end  
  def update_addons()
    @addon_bin.update
  end  
  def dispose_addons()
    @addon_bin.dispose()
  end
  def x=( x )
    super( x )
    @addon_bin.addons_refresh_x()
  end  
  def y=( y )
    super( y )
    @addon_bin.addons_refresh_y()
  end  
  def z=( z )
    super( z )
    @addon_bin.addons_refresh_z()
  end
  def opacity=( opacity )
    super( opacity )
    @addon_bin.addons_refresh_opacity()
  end  
  def visible=( visible )
    super( visible )
    @addon_bin.addons_refresh_visible()
  end  
  def openness=( openness )
    super( openness )
    @addon_bin.addons_refresh_openness()
  end  
  def viewport=( viewport )
    super( viewport )
    @addon_bin.addons_refresh_viewport()
  end
  def win_busy?()
    @addon_bin.win_busy?
  end  
  def can_move_window=(n)
    @addon_bin.can_move_window = n 
  end  
  def can_move_window
    @addon_bin.can_move_window()
  end  
  def mouse_can_move_window?()
    @addon_bin.mouse_can_move_window?() 
  end  
  
  def space_y()
    super + (@addon_bin.header_addon? ? 0 : @addon_bin.header_space)
  end  
  def space_height
    super + (@addon_bin.tail_addon? ? 0 : @addon_bin.tail_space)
  end  
end
module WindowAddons::Header
  def active_addons
    super + [:header] 
  end  
  def init_header
    @header_sprite = Sprite::WindowHeader.new( viewport, self )
    @header_sprite.refresh()
  end  
  def dispose_header()
    @header_sprite.dispose()
  end  
  def update_header()
    @header_sprite.update()
  end 
  def _redraw()
    super()
    @header_sprite.refresh()
  end
  def header_cube()
    Cube.new(self.x+2,self.open_y-14,self.z,96 + (self.width * 0.3).to_i,14,0)
  end   
  def header_bitmap()
    Cache.system("Header_Base(Window)")
  end  
  def header_text()
    ""
  end  
  def header_text_settings()
    fnt = Font.new.set_style(:window_header)
    {"align"=>0,"font"=>fnt}
  end 
  def header_visible?()
    return self.visible && !self.close?
  end  
  def addons_refresh_x
    super
    @header_sprite.update_position
  end  
  def addons_refresh_y
    super
    @header_sprite.update_position
  end  
  def addons_refresh_z
    super
    @header_sprite.update_position
  end
  def addons_refresh_visible
    super
    @header_sprite.update_visible
  end  
  def addons_refresh_openness
    super
    @header_sprite.update_visible
    @header_sprite.update_position
  end 
  def addons_refresh_viewport
    super
    @header_sprite.viewport = viewport
  end  
  def init_addons()
    super
    init_header()
  end  
  def update_addons()
    super()
    update_header()
  end
  def dispose_addons()
    super()
    dispose_header()
  end
end  
module WindowAddons::ScrollBar
  def active_addons
    super + [:scrollbar] 
  end
  def init_scroll_bar()
    @scroll_bar = Sprite::Scroller.new( self.viewport, self )
  end
  def dispose_scroll_bar()
    @scroll_bar.dispose()
  end  
  def update_scroll_bar()
    @scroll_bar.update()
  end 
  def _redraw()
    super()
    @scroll_bar.refresh()
  end 
  def scroll_bar_value_method
    @parent.method(:row_index)
  end
  def scroll_bar_max_method  
    @parent.method(:row_max)
  end 
  def scroll_bar_type()
    :vert
  end
  def scroll_bar_cube()
    length = self.height-(standard_padding*2)
    x,y = self.vx2-standard_padding,self.y+(self.height-length)/2
    Cube.new(x,y,self.z+1,8,0,length)
  end 
  def scroll_theme
    Themes[:modern_dark]
  end  
  def scroll_base_draw(bmp)
    rect = bmp.rect
    theme = scroll_theme
    bmp.fill_rect(rect,theme[:inline])
    bmp.fill_rect(rect.contract(1),theme[:outline])
    #bmp.fill_rect(rect,Pallete[:gray17])
    #bmp.fill_rect(rect.contract(1),Pallete[:gray15])
  end  
  def scroll_bar_draw(bmp,type,(xpad,ypad))
    theme = scroll_theme
    bmp.fill_rect(bmp.rect,theme[:outline])
    bmp.fill_rect(bmp.rect.contract(1),theme[:inline])
    bmp.fill_rect(bmp.rect.contract(2),theme[:base])
    grips = 3
    case type
    when :vert 
      #rect = Rect.new(xpad,bmp.height-(grips*2),bmp.width-(xpad * 2),1)
      rect = Rect.new(xpad,0,bmp.width-(xpad*2),1)
      orn = [1,2]
      rect.y = (bmp.height / 2.0).round(0).to_i - grips
    when :horz 
      rect = Rect.new(0,ypad,1,bmp.height-(ypad*2))
      orn = [2,1]
      rect.x = (bmp.width / 2.0).round(0).to_i - grips
    end          
    rect.contract!(1,orn[0])
    3.times do
      bmp.fill_rect(rect,theme[:light2]) 
      rect.xpush!(1,orn[1])
      bmp.fill_rect(rect.contract(1,orn[2]),theme[:inline]) 
      rect.xpush!(1,orn[1])
    end 
    #bmp.ext_draw_box1(
    #  :x             => xpad,
    #  :y             => ypad,
    #  :width         => bmp.width - (xpad * 2),
    #  :height        => bmp.height - (ypad * 2),
    #  :base_color    => Pallete[:paper4],
    #  :padding_color => Pallete[:brown5]
    #)
  end  
  def scroll_bar_visible?()
    return self.visible && self.open?
  end
  def addons_refresh_x
    super
    @scroll_bar.update_position
  end  
  def addons_refresh_y
    super
    @scroll_bar.update_position
  end  
  def addons_refresh_z
    super
    @scroll_bar.update_position
  end
  def addons_refresh_visible
    super
    @scroll_bar.update_visible
  end  
  def addons_refresh_openness
    super
    @scroll_bar.update_visible
  end  
  def addons_refresh_viewport
    super
    @scroll_bar.viewport = viewport
  end 
  def init_addons()
    super()
    init_scroll_bar()
  end  
  def update_addons()
    super()
    update_scroll_bar()
  end
  def dispose_addons()
    super() 
    dispose_scroll_bar()
  end  
end 
module WindowAddons::WinButtons
  def active_addons
    super + [:winbuttons] 
  end
  def init_win_buttons()
    @win_buttons = Collection_WinButtons.new(self)
    @win_buttons.set_handler(:minimize, method(:winbuttons_minimize))
    @win_buttons.set_handler(:maximize, method(:winbuttons_maximize))
    @win_buttons.set_handler(:close, method(:winbuttons_close))
  end
  def wb_openness 
    self.height
  end  
  def wb_openness=(n)
    self.height = n
  end  
  def wb_tminimize() # // Target Minimize
    standard_padding * 2
  end  
  def wb_tmaximize() # // Target Maximize
    winbuttons_orgsize.height
  end  
  def wb_do_minimize
    self.wb_openness = (self.wb_openness-(Graphics.height/10.0)).max(wb_tminimize)
    clamp_to_space()
    if self.wb_openness <= wb_tminimize
      @minimizing = false 
      self.wb_openness = wb_tminimize
    end  
  end  
  def wb_do_maximize
    self.wb_openness = (self.wb_openness+(Graphics.height/10.0)).min(wb_tmaximize)
    clamp_to_space()
    if self.wb_openness >= wb_tmaximize()
      @maximizing = false 
      self.wb_openness = wb_tmaximize()
    end  
  end 
  def wb_minimizing?
    self.wb_openness > wb_tminimize() && @minimizing
  end  
  def wb_maximizing?
    self.wb_openness < wb_tmaximize() && @maximizing
  end  
  def wb_mizing?
    wb_minimizing?() || wb_maximizing?()
  end  
  def win_busy?()
    super || wb_mizing?()
  end  
  def update_win_buttons()
    wb_do_minimize if wb_minimizing?
    wb_do_maximize if wb_maximizing?
    @win_buttons.update()
  end  
  def dispose_win_buttons()
    @win_buttons.dispose()
  end 
  def winbuttons_orgsize()
    if @orgsize.nil?
      @orgsize = Rect.new.set(self.to_rect)
      @orgsize.height = wb_openness
    end  
    @orgsize
  end  
  def winbuttons_minimize
    winbuttons_orgsize()
    @minimizing = true
    @maximizing = false
  end
  def winbuttons_maximize
    winbuttons_orgsize()
    @minimizing = false
    @maximizing = true
  end
  def winbuttons_close
  end  
  def winbuttons_x
    self.vx2 - (CacheExt.win_button_rect.width*3 + 4)#+ standard_padding)
  end
  def winbuttons_y
    self.open_y1 - CacheExt.win_button_rect.height  #+ standard_padding
  end
  def winbuttons_z
    self.z #+ 1
  end  
  def winbuttons_width
    (CacheExt.win_button_rect.width+1) * 3
  end  
  def winbuttons_actives
    [true, true, false]
  end  
  def addons_refresh_x
    super
    @win_buttons.update_position()
  end  
  def addons_refresh_y
    super
    @win_buttons.update_position()
  end  
  def addons_refresh_z
    super
    @win_buttons.update_position()
  end
  def addons_refresh_visible
    super
    @win_buttons.update_visible()
  end  
  def addons_refresh_openness
    super
    @win_buttons.update_openness()
  end  
  def addons_refresh_viewport
    super
    @win_buttons.update_viewport()
  end
  def init_addons()
    super()
    init_win_buttons()
  end  
  def update_addons
    super()
    update_win_buttons()
  end
  def dispose_addons
    super()
    dispose_win_buttons()
  end  
end  
module WindowAddons::Mouse_MoveWindow
  @@mouse_moving = nil
  def active_addons
    super + [:mouse_movewindow] 
  end
  def init_mmw()
    @@mouse_moving = nil if @@mouse_moving.disposed? if @@mouse_moving
    @can_move_window = true
  end  
  def update_mmw()
    if @@mouse_moving == self
      if MouseEx.rmoved?
        self.x = (MouseEx.rx-(self.width/2)) #if MouseEx.x_in_client? 
        self.y = (MouseEx.ry-(self.height/2)) #if MouseEx.y_in_client?
        clamp_to_space()
      end
      Mouse.tooltip_sprite.set_text("Moving...")
    end
    if mouse_can_move_window?
      if MouseEx.middle_click?() || (@@mouse_moving ? MouseEx.left_click?() : MouseEx.dleft_click?())
        if @@mouse_moving
          if @@mouse_moving == self || @@mouse_moving.disposed?
            @@mouse_moving = nil
          end  
        else
          @@mouse_moving = self
          Mouse.set_pos(self.cx, self.cy)
        end  
      end if mouse_in_window?()
    else
      @@mouse_moving = nil if @@mouse_moving == self
    end   
  end
  def win_busy?
    super || !@@mouse_moving.nil?
  end
  def init_addons()
    super()
    init_mmw()
  end 
  def update_addons
    super()
    update_mmw()
  end
end
module WindowAddons::Tabs
  def active_addons
    super + [:tabs] 
  end
  def init_tabs()
    @tab_collection = Collection_Tabs.new(self)
  end
  def dispose_tabs()
    @tab_collection.dispose
  end  
  def update_tabs()
    @tab_collection.update
  end 
  def _redraw()
    super()
    @tab_collection.refresh()
  end 
  def on_tab_change()
  end 
  def header_space
    tabs_y < self.y ? self.y - tabs_y : super()
  end  
  def tabs
    []
  end  
  def tabs_width
    tab_spacing * tabs.size
  end  
  def tabs_height
    tab_height
  end  
  def tab_width
    96
  end  
  def tab_height
    24
  end
  def tab_active_height
    tab_height()
  end  
  def tab_inactive_height
    16
  end 
  def tabs_x
    self.x
  end
  def tabs_y
    self.open_y1 - tabs_height
  end
  def tabs_z
    self.z
  end  
  def tab_spacing
    tab_width - 16
  end  
  def addons_refresh_x
    super
    @tab_collection.update_position
  end  
  def addons_refresh_y
    super
    @tab_collection.update_position
  end  
  def addons_refresh_z
    super
    @tab_collection.update_position
  end  
  def addons_refresh_openness
    super
    @tab_collection.update_position
  end 
  def addons_refresh_opacity
    super
    @tab_collection.update_opacity
  end  
  def addons_refresh_visible
    super
    @tab_collection.update_visible
  end  
  def addons_refresh_viewport
    super
    @tab_collection.update_viewport
  end  
  def init_addons
    super()
    init_tabs()
  end
  def dispose_addons
    super()
    dispose_tabs()
  end
  def update_addons
    super()
    update_tabs()
  end  
end
# // Opposite of header . x . Just a little sprite at the bottom of a window
module WindowAddons::Footer
  def active_addons
    super + [:tail] 
  end
  def init_tail
    @tail_sprite = Sprite::WindowTail.new(self.viewport, self)
    @tail_sprite.refresh()
  end  
  def dispose_tail
    @tail_sprite.dispose()
  end
  def update_tail
    @tail_sprite.update()
  end 
  def footer_bitmap
    Cache.system("tail")
  end  
  def _redraw()
    super()
    @tail_sprite.refresh()
  end  
  def tail_x
    self.x
  end  
  def tail_y
    self.open_y2
    #self.vy2
  end  
  def tail_z
    self.z
  end  
  def tail_width
    self.width
  end  
  def tail_height
    14
  end  
  def tail_visible?
    self.visible && !self.close?
  end  
  def tail_opacity
    255
  end  
  def addons_refresh_x
    super
    @tail_sprite.update_position
  end  
  def addons_refresh_y
    super
    @tail_sprite.update_position
  end  
  def addons_refresh_z
    super
    @tail_sprite.update_position
  end  
  def addons_refresh_openness
    super
    @tail_sprite.update_position
    @tail_sprite.update_visible
  end  
  def addons_refresh_opacity
    super
    @tail_sprite.update_opacity
  end  
  def addons_refresh_visible
    super
    @tail_sprite.update_visible
  end  
  def addons_refresh_viewport
    super
    @tail_sprite.update_viewport
  end
  def init_addons
    super()
    init_tail()
  end
  def dispose_addons
    super()
    dispose_tail()
  end
  def update_addons
    super()
    update_tail()
  end 
end  
# // 02/18/2012
module WindowAddons::OptionCursor
  def init_opt_cursor
    @opt_cursor = Sprite::OptionCursor.new(self.viewport)
    update_opt_cursor_pos()
    update_opt_cursor_visible()
  end  
  def update_opt_cursor()
    @opt_cursor.update
    update_opt_cursor_pos()
  end
  def dispose_opt_cursor()
    @opt_cursor.dispose
  end  
  def update_opt_cursor_pos()
    r = item_rect_to_screen(cursor_rect)
    @opt_cursor.x = r.x
    @opt_cursor.y = r.y
    @opt_cursor.z = self.z + 1
    @opt_cursor.oy = -(r.height-@opt_cursor.height)/2
    @opt_cursor.ox = @opt_cursor.width
  end  
  def update_opt_cursor_visible
    @opt_cursor.visible = self.visible && self.openness > 0
  end  
  def update_opt_cursor_active()
    @opt_cursor.active = self.active
  end  
  def update_opt_cursor_opacity()
    @opt_cursor.opacity = 255
  end  
  def update_opt_cursor_openness()
    update_opt_cursor_visible()
  end  
  def x=(n)
    super(n)
    update_opt_cursor_pos()
  end  
  def y=(n)
    super(n)
    update_opt_cursor_pos()
  end  
  def z=(n)
    super(n)
    update_opt_cursor_pos()
  end
  def active=(n)
    super(n)
    update_opt_cursor_active()
  end  
  def opacity=(n)
    super(n)
    update_opt_cursor_opacity()
  end 
  def openness=(n)
    super(n)
    update_opt_cursor_openness()
  end 
  def visible=(n)
    super(n)
    update_opt_cursor_visible()
  end  
  def viewport=(n)
    super(n)
    @opt_cursor.viewport = self.viewport
  end  
  def init_addons
    super()
    init_opt_cursor()
  end
  def update_addons
    super()
    update_opt_cursor()
  end
  def dispose_addons
    super()
    dispose_opt_cursor()
  end  
end
# // Small Antilag :D
module WindowAddons::Base_Tail
  def active_addons
    super + [:base_tail] 
  end
  alias :all_active_addons :active_addons
  def active_addons
    @active_addons ||= all_active_addons()
    @active_addons
  end 
end  
# // << >>
class Window::Base < Window
  include WindowAddons::Base 
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
