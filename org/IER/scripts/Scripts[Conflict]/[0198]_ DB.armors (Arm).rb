#  | DB.armors (Arm)
# // 02/29/2012
# // 02/29/2012
module Database
def self.mk_armors11()
  armors = []
  armor_sym = :arm
  equip_sym = :arm
#==============================================================================#
# ◙ Armor (Arm)(Leather Gloves)
#==============================================================================# 
  armor = Armor.new()
  armor.initialize_add()
  armor.id           = 1
  armor.name         = "Leather"
  armor.icon_index   = 486
  armor.description  = 'Durable, but kinda hard to grip stuff'
  armor.features     = []
  armor.note         = ""
  armor.etype_id     = 0 
  armor.params       = [0,0,0,1,0,1,0,0]
  armor.atype_id     = 0
  armor.features << MkFeature.def_r(1.03)
  armor.features << MkFeature.mdf_r(1.03)
  armors[armor.id] = armor    
#==============================================================================#
# ◙ REMAP
#==============================================================================# 
  adjust_armors(armors,armor_sym,equip_sym)   
end
end
