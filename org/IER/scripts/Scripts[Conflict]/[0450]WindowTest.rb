# WindowTest
module WindowTest
  def self.start_test( window_class, *params )
    @window = window_class.new( *params )
    @window.salign(1,1)
    @window.z = 200
    loop do
      Main.update()
      @window.update
      if block_given?
        break if yield @window
      end  
    end  
    @window.dispose()
    @window = nil
  end  
end  
class TestWindow < Window::Base
  def initialize()
    super( 0, 0, Graphics.width, Graphics.height )
    #contents.gui_draw_slider_base_horz( 
    #  {
    #    :width => 128,
    #    #:height => 12,
    #  }
    #)
    #contents.gui_draw_slider_base_vert( 
    #  {
    #    :y => 24,
    #    :height => 128,
    #    #:height => 12,
    #  }
    #)
    @sliders = []
    @sliders << GUIExt::Slider.new( nil, :horz, 128, 16, 16, 1 )
    @sliders << GUIExt::Slider.new( nil, :vert, 128, 16, 16+24, 1 )
    @sliders[0].bar_color = Color.new( 241, 194, 80 )
    @sliders.each { |s| s.z = 200 }
  end  
  def update()
    super()
    t = 120.0
    @sliders.each { |s|
      s.rate = (Math.sin( Graphics.frame_count / t * Math::PI ).abs)
    } 
    #@sliders[0].bar_color = Color.new(196, 196, 196).transition_to( Color.new( 241, 194, 80 ), @sliders[0].rate )
  end  
end  
#DataManager.init
#WindowTest.start_test(Window::CraftInput, 0, 0)
#DataManager.init
#MouseEx.init
#WindowTest.start_test(Window::ColorSelect, 0, 0)
def show_achievements(nm)
  gmid = GameManager.gm_game_ids[nm]
  GameManager.get_acmts_for( gmid ) { |hsh|
    next if hsh.nil?
  hsh.keys.sort.each { |i|
    ac1  = hsh ? hsh[i] : nil
    if ac1
      nxt = false
      GameManager.open_config { |cnf|
        cnf["Achievements"] ||= {}
        cnf["Achievements"][gmid] ||= {}
        nxt = cnf["Achievements"][gmid][ac1.code] == true
        cnf["Achievements"][gmid][ac1.code] = true
        cnf
      }
      next if nxt
      c = 0
      WindowTest.start_test( 
        Window::Achievement, 0, 0, ac1.name, ac1.description 
      ) { |win|
        c += 1
        if c == 180
          win.close
        end 
        win.close?
      }
      c = 0
      text = GameManager.acmt_response(nm, ac1.code).to_s
      WindowTest.start_test( Window::QuickText, 0, 0, Graphics.width / 1.5, 40, text ) { |win|
        c += 1
        if c == 180
          win.close
        end
        win.close?
      }
      Graphics.wait( 3 )
    end  
  }
  }
end 
["CodeJIFZ", "Necrodanse", "SARA"].each { |n| show_achievements(n) }
