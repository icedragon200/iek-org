# [IEI]LeMenu
class Window::OptionsBase < Window::Selectable
  def initialize(x,y,w,h)
    make_item_list
    super(x,y,w,h)
    init
    refresh
  end  
  def init
    @selected_index = nil # // nil - Unselected
  end  
  def draw_item(index)
    rect = item_rect(index)
    rect4text = item_rect_for_text(index)
    item = @data[index]
    if(item)
      draw_item_type(rect,item)
    end  
  end
  def draw_item_type(rect,item)
    @artist.draw_text(rect,item[:symbol].to_s.capitalize)
    type, parameters, function = item[:param]
    value = function.call(:value)
    case type
    when :list
    when :toggle, :cycle
      
    when :spinbox
      
    end  
  end  
  def make_item_list
    @data = []
  end  
  def update
    super
  end  
end 
class Window::TestOptions < Window::OptionsBase
  #def initialize
  #end
  def item_max
    @data.size
  end  
  def make_item_list
    super
    symbol = :test1
    func = proc { |type|
      case(type)
      when :value ; $temp_var ||= 0
      else        ;  nil
      end  
    }
    parameters = [:list,["Cheese","Bread","Apple"],func]
    function   = proc{|prev,current|puts("#{prev} changed to #{current}")}
    @data << {:symbol=>symbol,:param=>parameters,:function=>function}
  end  
end  

# // 
=begin
window = Window::TestOptions.new(*Graphics.rect)
window.show.activate.select(0)
MouseEx.init
loop do
  Main.update
  window.update
end
=end
