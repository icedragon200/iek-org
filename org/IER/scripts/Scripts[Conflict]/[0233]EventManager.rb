# EventManager
#==============================================================================#
# ■ EventManager
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 01/19/2012
# // • Data Modified : 01/19/2012
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 01/19/2012 V1.0 
#==============================================================================#
module EventManager
  include RPG
  include EventCommand::EvCCodes
  def self.evc_new(*params)
    EventCommand.new(*params)
  end  
  def self.evc_0(ind=0)
    evc_new(0, ind, [])
  end  
  def self.evc_show_text(ind, fn, fi, bt, pos)
    evc_new(SHOW_TEXT, ind, [fn, fi, bt, pos])
  end  
  def self.evc_show_text_line(ind, t)
    evc_new(SHOW_TEXT_LINE, ind, [t])
  end  
  def self.evc_show_text_a(ind, fn, fi, bt, pos, *txts)
    [evc_show_text(ind,fn,fi,bt,pos)]+txts.collect{|t|evc_show_text_line(ind,t)}
  end  
  def self.evc_show_choice(ind, cancel_op, *choices)
    evc_new(SHOW_CHOICE, ind, [choices, cancel_op])
  end  
  def self.evc_choice_branch(ind, branch, text)
    evc_new(SHOW_CHOICE_BRANCH, ind, [branch, text])
  end  
  def self.evc_choice_cancel(ind)
    evc_new(SHOW_CHOICE_CANCEL, ind, [])
  end
  def self.evc_choice_end(ind)
    evc_new(SHOW_CHOICE_END, ind, [])
  end  
  def self.evc_show_choice_a(ind, cancel_op, *choices)
    a = []
    a << evc_show_choice(ind, cancel_op, *choices)
    choices.each_with_index do |c, i|
      a << evc_choice_branch(ind,i,c)
      a += yield(i).each { |evc| evc.indent += ind+1 }
      a << evc_0(ind+1)
    end
    if cancel_op == 5
      a << evc_choice_cancel(ind)
      a += yield(-1).each { |evc| evc.indent += ind+1 }
      a << evc_0(ind+1)
    end  
    a << evc_choice_end(ind)
    a
  end  
  def self.evc_script(ind,scr)
    evc_new(SCRIPT, ind, [scr])
  end  
  def self.make_floor_up_ev(x=0,y=0)
    ev = Event.new(x,y)
    page = Event::Page.new()
    page.graphic.tile_id = 30
    page.list = []
    page.list += evc_show_text_a(0, "", 0, 0, 0, "Advance to the next floor?")
    page.list += evc_show_choice_a(0,2,"Yes","No"){|i|i==0 ? [evc_script(0,"_map.advance_floor")] : [] }
    page.list << evc_0(0)
    ev.pages[0] = page
    ev
  end 
  #File.open("Eventx.txt", "w+") { |f|
  #  make_floor_up_ev(0,0).pages[0].list.each { |evc|
  #    f.puts evc.to_s_exp()
  #  }  
  #}
  #map = load_data("Data/Map001.rvdata")
  #File.open("Event.txt", "w+") { |f|
  #  map.events[1].pages[0].list.each { |evc|
  #    f.puts evc.to_s_exp()
  #  }  
  #} 
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
