# SubScene::Menu
# // 02/03/2012
# // 02/03/2012 
class SubScene::Menu < SubScene::Base
  def start()
    super()
    @viewport.rect.set(Graphics.play_rect)
    create_all_windows()
  end  
  def create_all_windows()
    @command_window = Window::MenuCommand.new(0,0)
    @command_window.set_handler(:cancel, method(:command_cancel))
    add_window( @command_window )
  end  
  def command_cancel
    end_subscene()
  end  
end  
