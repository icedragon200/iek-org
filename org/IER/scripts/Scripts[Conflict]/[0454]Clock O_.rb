# Clock O:
class GUIExt::Clock
  def initialize
    @ticking_sounds = [RPG::SE.new("ClockTick01"), RPG::SE.new("ClockTick02")]
    @background_sprite = Sprite::WheelBackground.new()
    @background_sprite.bitmap = Cache.system( "CommandWheel_Background" )
    @background_sprite.center_oxy(32,32)
    @background_sprite.salign(1,1)
    @ball_sprites = Array.new(4) { Sprite.new }
    @polygons = Array.new(4) { Circle.new(
        @background_sprite.x, @background_sprite.y, @background_sprite.width/2
      )
    }  
    @polygons[3].y += @background_sprite.height/2
    rd = [1.2, 1.7, 2.2, 1.8]
    @polygons.each_with_index { |p, i|
      p.angle_offset = 90
      p.radius *= rd[i]
    }
    @hands = Array.new(4) { Sprite.new }  
    w = @background_sprite.width/2
    @hands[0].bitmap = Bitmap.new(9, w*0.6)
    @hands[1].bitmap = Bitmap.new(7, w*0.8)
    @hands[2].bitmap = Bitmap.new(5, w*1.0)
    @hands[3].bitmap = Bitmap.new(6, w*rd[3])
    [
     Pallete[:sys_red], 
     Pallete[:sys_blue], 
     Pallete[:sys_orange],
     Pallete[:sys_green]
    ].each_with_index { |c, i|
      @hands[i].bitmap.fill_rect(@hands[i].bitmap.rect.contract(1), c)
      @hands[i].bitmap.blur
      @hands[i].x = 16 + @polygons[i].cx
      @hands[i].y = 16 + @polygons[i].cy
      @hands[i].z = 1 + i
      @hands[i].ox = (@hands[i].width / 2) 
      @ball_sprites[i].bitmap = Cache.system( "Ball" )
      @ball_sprites[i].color.set(c).xset(:alpha=>128)
      @ball_sprites[i].center_oxy(32,32)
      @ball_sprites[i].z = 6 + i
    } 
    @hands[2].oy = 8 
    @digital_sprite = Sprite.new()
    @digital_sprite.bitmap = Bitmap.new(@background_sprite.width, 24)
    @digital_sprite.x, @digital_sprite.vy2 = @background_sprite.x, @background_sprite.y
    @digital_sprite.z = 36
    @digital_sprite.x -= @background_sprite.ox
    @digital_sprite.y -= @background_sprite.oy
    @time_tweens = Array.new(3) { Tween.new(0,0,:linear,1.0) }
    @tween_times  = [90, 50, 10]
    @tween_easers = [:bounce_out, :circ_in, :back_out]
    @last_time = time
    update
  end  
  def time
    t = (t=Time.now).to_f+t.gmt_offset#
    #t = Graphics.frame_count / Graphics.frame_rate.to_f 
    #sec, min, hour = t % 60, t / 60 % 60, t / 60 / 60 % 24
    sec, min, hour = t, t / 60, t / 60 / 60 
    return hour, min, sec
  end 
  def u_time(tna)
    time_a = tna.dup
    time_a.each_with_index { |t, i|
      if time_a[i] != @last_time[i]
        @time_tweens[i].set_and_reset(@last_time[i], time_a[i], 
          @tween_easers[i], Tween.frames_to_tt(@tween_times[i]))
      end 
      time_a[i] = @time_tweens[i].value #+ time_a[i] - 1 
    }
    time_a
  end  
  def time_i(t=time)
    hour, min, sec = *t
    return hour.to_i, min.to_i, sec.to_i
  end 
  def time_m(ta,th=false)
    return [ta[0] % (th ? 12 : 24), ta[1] % 60, ta[2] % 60]
  end  
  def time_changed?(time_a)
    @last_time != time_a
  end  
  def second_changed?(n)
    @last_time[2] != n
  end  
  def minute_changed?(n)
    @last_time[1] != n
  end  
  def hour_changed?(n)
    @last_time[0] != n
  end  
  def update()
    @time_tweens.each { |t| t.update }
    t = time
    time_now = time_i(t)
    hour_f, min_f, sec_f = time_m(t)
    hour, min, sec = *u_time(time_now)
    if time_changed?(time_now)
      @digital_sprite.bitmap.clear()
      tn = time_m(time_now,true)
      @digital_sprite.bitmap.draw_text(
        0,0,@digital_sprite.width,24,format("%02d:%02d:%02d",*tn),1
      ) 
      @ticking_sounds[sec.to_i%2].play  
    end    
    @digital_sprite.opacity = 128 + (128 * (sec_f % 1.0))
    @digital_sprite.color.set(@ball_sprites[0].color)
    @digital_sprite.color.alpha = 198 * (sec_f / 60.0)
    angs = [(hour * 30), (min * 6), (sec * 6), 180+(40 * Math.sin((sec_f%30.0)*Math::PI))]
    angs2 = [(hour_f * 30), (min_f * 6), (sec_f * 6), angs[3]]
    @polygons.each_with_index { |p, i|
      @ball_sprites[i].vset(*p.get_angle_xy(angs2[i]))
      @hands[i].angle = 180 - angs[i]
    }  
    @ball_sprites[3].color.set(
     Pallete[:sys_green].transition_to(
      Pallete[:sys_green2], Math.sin((sec_f % 1.0)*Math::PI)
     )
    ).xset(:alpha=>128)
    @last_time = time_now
  end  
end
=begin
DataManager.init
clock = GUIExt::Clock.new
loop do
  Graphics.update
  clock.update
end  
=end
=begin
DataManager.init
tweener = Tween::Osc.new(0, 1.0, [:sine_inout, :sine_inout], [120].collect{|f|Tween.f2tt(f)})
sl = GUIExt::Slider.new(nil, :horz, Graphics.width-64, 32, Graphics.rect.vy2-64, 1 )
sl.bar_color = Pallete[:sys_green2].xset(:alpha=>198)
sl.activate
clock = GUIExt::Clock.new
loop do
  Graphics.update
  Input.update
  clock.update
  tweener.update
  sl.rate = tweener.value
end  
=end
