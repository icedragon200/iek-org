# Window::Help2
#==============================================================================#
# ♥ Window::Help2
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/28/2011
# // • Data Modified : 12/28/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/28/2011 V1.0 
#
#==============================================================================#
class Artist::Help < Artist
  def draw_item_durability(item,rect)
    return unless Ex_Database.ex_equip_item?(item)
    rect, rate = rect, item.durability_rate
    colors = DrawExt.quick_bar_colors(Pallete[:sys_orange])
    ext_draw_bar1(rect.xto_h(*Rect::SYM_ARGS).merge(:rate=>rate).merge(colors))
  end  
  def draw_item_element_res(item,x,y,w=96,h=6)
    return unless Ex_Database.ex_equip_item?(item)
    rect, rate = nil, nil # // Declaration . x .
    for i in 1..6 # // . x . Only the 6 main elements
      rect = Rect.new(x,y+(h*(i-1)),w,h)
      rate = item.element_residue_rate(i)
      colors = DrawExt.quick_bar_colors(element_color(i))
      ext_draw_bar4(rect.xto_h(*Rect::SYM_ARGS).merge(:rate=>rate).merge(colors))
    end  
  end
  
  def draw_item_exp(item,x,y,w=128,h=16)
    draw_battler_exp(
      :battler => item,
      :x => x,
      :y => y,
      :width => w,
      :height=> h
    ) if Ex_Database.ex_equip_item?(item)
  end
  def draw_item_name(item,x,y,w=128,h=12)
    drawing_sandbox() do
      contents.font.set_style(:simple_black)
      contents.font.name = ["Microsoft YaHei"]
      contents.font.size += 4
      draw_text( x, y, w, h, item ? item.name : "" )
    end  
  end  
  def draw_item_description(item,x,y,w=contents.width,h=14)
    drawing_sandbox() do
      contents.font.set_style(:simple_black)
      contents.font.size = Font.default_size - 8
      draw_text( x, y, w, h, item ? item.description : "" )
    end  
  end
  def draw_item_parameters(item,x,y,col=4)
    return if item.nil?()
    return unless Ex_Database.equip_item?(item)
    params = item.params.clone
    if @battler
      params = @battler.mk_temp_self.mk_params_for(item)
    end  
    for i in 0...8
      draw_parameter_i(params[i], x + ((i % col) * 64), y + ((i / col) * 12), i)
    end 
  end      
end  
class Window::Help2 < Window::Help  
  def set_item( item )
    if @item != item
      @item = item
      refresh()
    end  
  end 
  def reset_font_settings()
    contents.font.set_style( :simple_black )
  end  
  def calc_line_height(text, restore_font_size = true)
    return 12
  end 
  def standard_artist
    Artist::Help
  end  
  alias :refresh_h1 :refresh
  def refresh()
    return refresh_h1 if @text
    contents.clear
    item = @item
    contents.font.set_style( :simple_black )
    dx, dy = 0, 0
    open_artist(true) do |art|
      bmp = Cache.system("Help_ItemBorder(Window)")
      contents.blt( dx, dy, bmp, bmp.rect )
      art.draw_item_icon( item, dx, dy+((contents.height-24)/2) )
      dx += 24
      contents.font.size = Font.default_size - 6
      if Ex_Database.equip_item?(item)
        art.draw_box( dx, dy+12, (contents.width-(4*64))-28, 12 )
      else  
        art.draw_box( dx, dy+12, contents.width-28, 12 )
      end  
      art.draw_item_name(item,dx,dy,contents.width-(dx))
      dy += 12
      art.draw_item_description(item,dx+4,dy,contents.width-28,14)
      dx = contents.width - (4*64)
      dy = 0
      art.draw_item_parameters(item,dx,dy,4)
      art.draw_item_exp(item,dx-130,dy,128,14) 
      #art.draw_text_ex( dx+4, dy, item ? item.description : "" )
    end  
    start_fadein_contents()
  end 
  def clear()
    @text = nil
    set_item(nil)
  end
  def update()
    super()
    update_contents_fading() # // 02/19/2012
  end  
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
