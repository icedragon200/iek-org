# [IEI]XpansionLibrary
#==============================================================================#
# ♥ IEI - Xpansion Library
#==============================================================================#
# // • Created By    : IceDragon
# // • Data Created  : 12/06/2011
# // • Data Modified : 04/12/2012
# // • Version       : 1.0
#==============================================================================#
# ● Functions
#==============================================================================#
# ♥ - Class
# ■ - Module
#==============================================================================#
# ■ IEI::Pallete (Referenced as Pallete)
# ♥ IEI::Grid
# ♥ IEI::Sequencer
# ♥ IEI::ArrayTable
#==============================================================================#
if(HOME_LOAD)
  require 'XpanLib/Pallete'
  require 'XpanLib/Grid' 
  require 'XpanLib/Sequencer' 
  require 'XpanLib/ArrayTable'
  require 'XpanLib/Pos'
  require 'XpanLib/Morph'
  require 'XpanLib/Interpolate'
  require 'XpanLib/Notetag'
  require 'XpanLib/Notebox'
  require 'XpanLib/Tween'
  
  require 'FunLib/LogicGates'
else
  load_skpck('XpanLib')
  load_skrips(self.send(:binding),Skrip.get_skpck('XpanLib'))
end  
#==============================================================================#
# // Color Symbols (Earthen Pallete)
# :trans, :black, :white
# :gray1..:gray18
# :brown1, :brown2, :brown3, :brown4, :brown5
# :paper1, :paper2, :paper3, :paper4, :paper5, :paper6
# :outline, :purple
# :sys_orange , :sys_blue , :sys_green , :sys_red
# :sys_orange2, :sys_blue2, :sys_green2, :sys_red2
# :element1..:element8
#==============================================================================#
module IEI::Pallete
  @sym_colors[:trans]   = 0
  @sym_colors[:black]   = 64
  @sym_colors[:white]   = 83
  for i in 0...18 ; @sym_colors["gray#{i+1}".to_sym] = 82 - i ; end
  @sym_colors[:brown1]  = 1
  @sym_colors[:brown2]  = 2
  @sym_colors[:brown3]  = 3
  @sym_colors[:brown4]  = 4
  @sym_colors[:paper1]  = 5
  @sym_colors[:paper2]  = 6
  @sym_colors[:paper3]  = 7
  
  @sym_colors[:outline] = 16
  @sym_colors[:brown5]  = 17
  @sym_colors[:purple]  = 18
  @sym_colors[:paper4]  = 19
  @sym_colors[:paper5]  = 20
  @sym_colors[:paper6]  = 21
  
  @sym_colors[:sys_orange]  = 36
  @sym_colors[:sys_blue]    = 37
  @sym_colors[:sys_green]   = 38
  @sym_colors[:sys_red]     = 39
  @sym_colors[:sys_orange2] = 40
  @sym_colors[:sys_blue2]   = 41
  @sym_colors[:sys_green2]  = 42
  @sym_colors[:sys_red2]    = 43
  
  for i in 0...8 ; @sym_colors["element#{i+1}".to_sym] = 48 + i ; end
end
Pallete = IEI::Pallete
