# Database.armors
#==============================================================================#
# ■ Database.armors
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/07/2011
# // • Data Modified : 12/07/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/07/2011 V1.0 
#         Added:
#           Armor 0 (Dummy)
#           Armor 1 (<unnamed>)
#
#==============================================================================#
module Database
  Armor = RPG::Armor
def self.adjust_armors(items,armor_sym,equip_sym)  
  items.compact.each {|i|adjust_armor(i,armor_sym,equip_sym)}
  items
end  
def self.adjust_armor(item,armor_sym,equip_sym)  
  iof = @armor_id_off[armor_sym] # // ID offset 
  item.icon_index = @armor_icon_index[armor_sym] + item.id
  item.id = iof + item.id
  item.atype_id = @armor_type_id[armor_sym]
  item.etype_id = @equip_type_id[equip_sym]
  item
end  
def self.add_armors(items)
  items.compact.each {|i|@armors[i.id]=i}
end   
def self.build_armors()
  @_armor_param_blocks = {}
  @armors = []
#==============================================================================#
# ◙ Armor 0 (Dummy)
#==============================================================================# 
  armor = Armor.new()
  armor.id = 0
  armor.initialize_add()
  #@_armor_param_blocks[armor] = proc do |parameter, level, obj|
  #  obj.params[parameter] + Integer(case parameter
  #  when 0 # // Max Hp
  #    level * 11 / 3.6
  #  when 1 # // Max Mp
  #    level * 3 / 2.8
  #  when 2 # // Attack 
  #    level * 3 / 2.4
  #  when 3 # // Defense
  #    level * 3 / 2.4
  #  when 4 # // Magic Attack  
  #    level * 3 / 2.4
  #  when 5 # // Magic Defense  
  #    level * 3 / 2.4
  #  when 6 # // Agility  
  #    level * 3 / 2.0
  #  when 7 # // Luck 
  #    level * 3 / 2.4
  #  else
  #    0
  #  end) 
  #end  
  @armors[armor.id] = armor
#==============================================================================#
# ◙ Armor (Shield)
#==============================================================================#  
  add_armors mk_armors1()
#==============================================================================#
# ◙ Armor (Helmet)
#==============================================================================#    
  add_armors mk_armors2()
  add_armors mk_armors3()
  add_armors mk_armors4()
#==============================================================================#
# ◙ Armor (Body)
#==============================================================================#    
  add_armors mk_armors5()
  add_armors mk_armors6()
  add_armors mk_armors7()
#==============================================================================#
# ◙ Armor (Accessory)
#==============================================================================#    
  add_armors mk_armors8()
  add_armors mk_armors9()
  add_armors mk_armors10()
#==============================================================================#
# ◙ Armor (Arm)
#==============================================================================#    
  add_armors mk_armors11()
  #add_armors mk_armors12()
  #add_armors mk_armors13()  
#==============================================================================#
# ◙ Armor (Leg)
#==============================================================================#    
  add_armors mk_armors14()
  #add_armors mk_armors12()
  #add_armors mk_armors13()    
#==============================================================================#
# ◙ Armor (Crest)
#==============================================================================#      
  add_armors mk_armors20()
#==============================================================================#
# ◙ REMAP
#==============================================================================#   
  for i in 0...@armors.size
    next unless @armors[i]
    armor = @armors[i]
    armor.id = i
    armor.iconset_name = "Iconset_Armor"
    armor.database_offset = 2000
    armor.rebuild_parameter_table(ARMOR_LEVELCAP,&@_armor_param_blocks[armor])
  end        
end  
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
