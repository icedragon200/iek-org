# Rect
#==============================================================================#
# ♥ Rect (Expansion)
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/20/2011
# // • Data Modified : 01/04/2012
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 01/04/2012 V1.0 
#         Refer to Mixin::RectExpansion Module for functions    
#==============================================================================#
class Rect
  include Mixin::RectExpansion
  include RectOnly
end
class Sprite
  include Mixin::RectExpansion
end  
class Window
  include Mixin::RectExpansion
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
