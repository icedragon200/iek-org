#  | Arts (Elements)
module IEI
  module ArtsSystem
    def self.skills2skadd(skills)
      skills.collect{|s|MkFeature.skill_add(s.id)}
    end  
class << self
  alias :element_mk_objs :mk_objs
  def mk_objs()  
    element_mk_objs()
    # // Element Arts
    # Increases Attack, ups Fire Effects, increases damage from Water, prevents Water Usage 
    # Increases Luck, ups Water Effects, increases damage from Fire, prevents Fire Usage 
    # Increases Defense, ups Earth Effects, increases damage from Wind, prevents Wind Usage 
    # Increases Agility, ups Wind Effects, increases damage from Earth, prevents Earth Usage 
    # Increases Magic Defense, ups Light Effects, increases damage from Dark, prevents Dark Usage 
    # Increases Magic Attack, ups Dark Effects, increases damage from Light, prevents Light Usage 
    parm = [:atk, :luk, :def, :agi, :mdf, :mat].collect{|s|DB.param_id(s)}
    prt=[1.05, 1.1, 1.2];rrt=[0.9, 0.825, 0.75];wrt=[1.1, 1.25, 1.5]
    spfs=["%s I", "%s II", "%s III"];lvs=[:lv1, :lv2, :lv3]
    eles=[[:fire,:water],[:water,:fire],[:earth,:wind],[:wind,:earth],[:light,:dark],[:dark,:light]]
    fi, ein = 0, 0, 0 # // Start Index, Full Index, Element Index
    elem, welem = nil, nil # // Element Symbol, Weak Element Symbol
    for ea in eles
      elem, welem = ea
      for i in 0...3
        @arts[21+fi] = Art[21+fi,0,format(spfs[i],Vocab.element(DB.element_id(elem))),'',
          MkFeature.param_r(   parm[ein] , prt[i]),
          MkFeature.element_r( eid(elem) , rrt[i]), 
          MkFeature.element_r( eid(welem), wrt[i]),
          MkFeature.stype_seal(DB.stype_id(welem)),
          *skills2skadd(DB.find_skills4groups(elem,lvs[i]))
        ]
        DB.set_element_icon(elem,@arts[21+fi],:art)
        add_art2groups(@arts[21+fi],:element,elem,lvs[i])
        fi += 1
      end  
      ein += 1
    end  
  end # // mk_objs
end # // class << self  
  end
end  
