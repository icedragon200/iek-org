# Database.actors
#==============================================================================#
# ■ Database.actors
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/06/2011
# // • Data Modified : 12/06/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/06/2011 V1.0  
#         Added:
#           Actor 0 (Dummy)
#           Actor 1 (Baron)
#           Actor 2 (Elli)
#           Actor 3 (Chika)
#           Actor 4 (Spyet)
#           Actor 5 (Icaria)
#           Actor 6 (Nekome)
#           Actor 7 (Len)
#           Actor 8 (Cyrus)
#
#==============================================================================#
module Database
  Actor = RPG::Actor
  def self.armor_features(n)
    fet = []
    fet << MkFeature.armor_type( @armor_type_id[:shield] )
    fet << MkFeature.armor_type( @armor_type_id[:helmet] )
    fet << MkFeature.armor_type( @armor_type_id[:body] )
    fet << MkFeature.armor_type( @armor_type_id[:accessory] )
    fet << MkFeature.armor_type( @armor_type_id[:arm] )
    fet << MkFeature.armor_type( @armor_type_id[:leg] )
    fet << MkFeature.armor_type( @armor_type_id[:crest] )
    case(n)
    when :male
      fet << MkFeature.armor_type( @armor_type_id[:helmetM] )
      fet << MkFeature.armor_type( @armor_type_id[:bodyM] )
      fet << MkFeature.armor_type( @armor_type_id[:accessoryM] )
    when :female  
      fet << MkFeature.armor_type( @armor_type_id[:helmetF] )
      fet << MkFeature.armor_type( @armor_type_id[:bodyF] )
      fet << MkFeature.armor_type( @armor_type_id[:accessoryF] )
    end
    fet
  end
def self.build_actors()  
  @actors = []  
#==============================================================================#
# ◙ Actor 0 (Dummy)
#==============================================================================# 
  act                 = Actor.new()
  act.initialize_add()
  act.id              = 0
  act.element_id      = 0
  @actors[act.id]     = act
#==============================================================================#
# ◙ Actor 1 (Baron)
#==============================================================================# 
  act                 = Actor.new()
  act.initialize_add()
  act.id              = 1
  act.icon_index      = 0
  act.description     = ''
  act.note            = ''
  act.name            = "Baron"
  act.nickname        = act.name
  act.class_id        = 1
  act.initial_level   = 1
  act.max_level       = LEVELCAP
  act.character_name  = "Baron_8D"
  act.character_index = 0
  act.face_name       = "Baron"
  act.face_index      = 0
  act.equips          = [  @weapon_id_off[:spears] + 1,  0,  0,  0,  0]
  act.features        = []
  act.element_id      = element_id(:light)
  act.features += armor_features(:male)
  @actors[act.id]     = act
#==============================================================================#
# ◙ Actor 2 (Elli)
#==============================================================================# 
  act                 = Actor.new()
  act.initialize_add()
  act.id              = 2
  act.icon_index      = 0
  act.description     = ''
  act.note            = ''
  act.name            = "Elli"
  act.nickname        = act.name
  act.class_id        = 2
  act.initial_level   = 1
  act.max_level       = LEVELCAP
  act.character_name  = "Elli"
  act.character_index = 0
  act.face_name       = "Elli"
  act.face_index      = 0
  act.equips          = [  @weapon_id_off[:brushes] + 1,  0,  0,  0,  0]
  act.features        = []
  act.element_id      = element_id(:dark)
  act.features += armor_features(:female)
  @actors[act.id]     = act  
#==============================================================================#
# ◙ Actor 3 (Chika)
#==============================================================================# 
  act                 = Actor.new()
  act.initialize_add()
  act.id              = 3
  act.icon_index      = 0
  act.description     = ''
  act.note            = ''
  act.name            = "Chika"
  act.nickname        = act.name
  act.class_id        = 3
  act.initial_level   = 1
  act.max_level       = LEVELCAP
  act.character_name  = "Chika"
  act.character_index = 0
  act.face_name       = "Chika"
  act.face_index      = 0
  act.equips          = [  @weapon_id_off[:swords] + 1,  0,  0,  0,  0]
  act.features        = []
  act.element_id      = element_id(:earth)
  act.features += armor_features(:female)
  @actors[act.id]     = act  
#==============================================================================#
# ◙ Actor 4 (Spyet)
#==============================================================================# 
  act                 = Actor.new()
  act.initialize_add()
  act.id              = 4
  act.icon_index      = 0
  act.description     = ''
  act.note            = ''
  act.name            = "Spyet"
  act.nickname        = act.name
  act.class_id        = 4
  act.initial_level   = 1
  act.max_level       = LEVELCAP
  act.character_name  = "Spyet_8D"
  act.character_index = 0
  act.face_name       = "Spyet"
  act.face_index      = 0
  act.equips          = [  0,  0,  0,  0,  0]
  act.features        = []
  act.element_id      = element_id(:wind)
  act.features += armor_features(:female)
  @actors[act.id]     = act  
#==============================================================================#
# ◙ Actor 5 (Icaria)
#==============================================================================# 
  act                 = Actor.new()
  act.initialize_add()
  act.id              = 5
  act.icon_index      = 0
  act.description     = ''
  act.note            = ''
  act.name            = "Icaria"
  act.nickname        = act.name
  act.class_id        = 5
  act.initial_level   = 1
  act.max_level       = LEVELCAP
  act.character_name  = "Icaria"
  act.character_index = 0
  act.face_name       = "Icaria"
  act.face_index      = 0
  act.equips          = [  0,  0,  0,  0,  0]
  act.features        = []
  act.element_id      = element_id(:fire)
  act.features += armor_features(:female)
  @actors[act.id]     = act  
#==============================================================================#
# ◙ Actor 6 (Nekome)
#==============================================================================# 
  act                 = Actor.new()
  act.initialize_add()
  act.id              = 6
  act.icon_index      = 0
  act.description     = ''
  act.note            = ''
  act.name            = "Nekome"
  act.nickname        = act.name
  act.class_id        = 6
  act.initial_level   = 1
  act.max_level       = LEVELCAP
  act.character_name  = "Nekome"
  act.character_index = 0
  act.face_name       = "Nekome"
  act.face_index      = 0
  act.equips          = [  0,  0,  0,  0,  0]
  act.features        = []
  act.element_id      = element_id(:water)
  act.features += armor_features(:female)
  @actors[act.id]     = act  
#==============================================================================#
# ◙ Actor 7 (Len)
#==============================================================================# 
  act                 = Actor.new()
  act.initialize_add()
  act.id              = 7
  act.icon_index      = 0
  act.description     = ''
  act.note            = ''
  act.name            = "Len"
  act.nickname        = act.name
  act.class_id        = 7
  act.initial_level   = 1
  act.max_level       = LEVELCAP
  act.character_name  = "Len"
  act.character_index = 0
  act.face_name       = "Len"
  act.face_index      = 0
  act.equips          = [  0,  0,  0,  0,  0]
  act.features        = []
  act.element_id      = element_id(:light)
  act.features += armor_features(:female)
  @actors[act.id]     = act  
#==============================================================================#
# ◙ Actor 8 (Cyrus)
#==============================================================================# 
  act                 = Actor.new()
  act.initialize_add()
  act.id              = 8
  act.icon_index      = 0
  act.description     = ''
  act.note            = ''
  act.name            = "Cyrus"
  act.nickname        = act.name
  act.class_id        = 8
  act.initial_level   = 1
  act.max_level       = LEVELCAP
  act.character_name  = "Cyrus"
  act.character_index = 0
  act.face_name       = "Cyrus"
  act.face_index      = 0
  act.equips          = [  0,  0,  0,  0,  0]
  act.features        = []
  act.element_id      = element_id(:wind)
  act.features += armor_features(:male)
  @actors[act.id]     = act   
#==============================================================================#
# ◙ Actor 9 (Vesper)
#==============================================================================# 
  act                 = Actor.new()
  act.initialize_add()
  act.id              = 9
  act.icon_index      = 0
  act.description     = ''
  act.note            = ''
  act.name            = "Vesper"
  act.nickname        = act.name
  act.class_id        = 9
  act.initial_level   = 1
  act.max_level       = LEVELCAP
  act.character_name  = "Vesper"
  act.character_index = 0
  act.face_name       = "Vesper"
  act.face_index      = 0
  act.equips          = [  0,  0,  0,  0,  0]
  act.features        = []
  act.element_id      = element_id(:fire)
  act.features += armor_features(:male)
  @actors[act.id]     = act  
#==============================================================================#
# ◙ REMAP
#==============================================================================#   
  for i in 0...@actors.size
    @actors[i].id = i
  end  
end  
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
