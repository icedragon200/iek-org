# Scene::Splash
#==============================================================================#
# ♥ Scene::Splash
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/07/2011
# // • Data Modified : 12/07/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/07/2011 V1.0 
#
#==============================================================================#
class Scene::Splash < Scene::Base
  include RPG::Splash::Command_Codes
  #--------------------------------------------------------------------------#
  # ● super-method :start
  #--------------------------------------------------------------------------#
  def start()
    super()
    @splash = $data_startup.splash_screens[$data_temp.splash_id]
    @sprites = Array.new(5) { s = Sprite.new ; s.viewport = @viewport ; s }
    @sequence               =  @splash.sequence
    @sequence_index         = 0
    @wait_count = 0
  end
  #--------------------------------------------------------------------------#
  # ● super-method :terminate
  #--------------------------------------------------------------------------#
  def terminate()
    super()
    @sprites.each { |s| s.dispose }
  end
  #--------------------------------------------------------------------------#
  # ● new-method :return_scene
  #--------------------------------------------------------------------------#
  def return_scene
    $data_temp.splash_id += 1
    SceneManager.return()
  end  
  #--------------------------------------------------------------------------#
  # ● super-method :update
  #--------------------------------------------------------------------------#
  def update()
    super()
    return_scene if @sequence_index > @sequence.size
    return_scene if Input.trigger?( :C )
    @sprites.each { |s| s.update }
    @wait_count = [@wait_count - 1, 0].max()
    return unless @wait_count == 0
    loop do
      break if @sequence_index > @sequence.size
      break if @wait_count > 0
      command = @sequence[@sequence_index]
      case command.code
      when WAIT_CODE
        @wait_count = command.params[0]
      when SHOW_PICTURE_CODE
        @sprites[command.params[0]].bitmap = Cache.picture( command.params[1] )
      when ALIGN_PICTURE_CODE
        sp = @sprites[command.params[0]]
        case command.params[1]
        when 0 # // x
          sp.x = command.params[2] == 0 ? 0 : 
           command.params[2] == 1 ? (Graphics.width - sp.width) / 2 :
           Graphics.width - sp.width
        when 1 # // y
          sp.y = command.params[2] == 0 ? 0 : 
           command.params[2] == 1 ? (Graphics.height - sp.height) / 2 :
           Graphics.height - sp.height
        end  
      when ERASE_PICTURE_CODE  
        sp = @sprites[command.params[0]]
        sp.bitmap.dispose unless sp.bitmap.disposed? if sp.bitmap
      when FLASH_PICTURE_CODE
        sp = @sprites[command.params[0]]
        sp.flash(
          Color.new(command.params[1], 
                    command.params[2], 
                    command.params[3], 
                    command.params[4]),
         command.params[5]           
        )
      when FREEZE_CODE  
        Graphics.freeze
      when TRANSITION_CODE
        Graphics.transition( command.params[0] )
      when SPLASH_END_CODE  
        return_scene
      end if command
      @sequence_index += 1
    end 
  end
  #--------------------------------------------------------------------------#
  # ● super-method :perform_transition
  #--------------------------------------------------------------------------#
  def perform_transition()
  end  
end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
