# DrawShare
# // 02/06/2012
# // 02/06/2012
module DrawShare
  def draw_parameter_text(x,y,param_id,enabled=true)
    drawing_sandbox() do  
      voc   = Vocab.param_a( param_id )
      contents.font.set_style( :simple_brown1 )
      #change_color_ex(contents.font.color,enabled)
      draw_text( x, y, 32, 12, voc, 0 )
    end  
    Rect.new(x,y,32,12)
  end  
  def draw_parameter_base( x, y, param_id, enabled=true )
    bmp = Cache.system("Status_StatBorder(Window)") 
    drawing_sandbox() do  
      contents.blt( x+32, y, bmp, bmp.rect, enabled ? 255 : translucent_alpha )
      draw_parameter_text(x,y,param_id,enabled)
    end  
    Rect.new(x+32,y,bmp.width,bmp.height)
  end  
  def draw_parameter( obj, x, y, param_id, enabled=true )
    draw_parameter_i(obj ? obj.param( param_id ) : 0, x, y, param_id, enabled )
  end
  def draw_parameter_i( value, x, y, param_id, enabled=true )
    drawing_sandbox() do  
      r = draw_parameter_base( x, y, param_id, enabled ).contract(1,1)
      contents.font.set_style( :simple_brown1 )
      change_color_ex(Pallete[:outline], enabled)
      block_given? ? yield(r, value) : draw_text( r.x, r.y, r.width, r.height, value, 2 )
    end # // drawing_sandbox()
  end
  def change_color_ex(color,enabled=true)
    contents.font.color = color.clone
    contents.font.color.alpha = translucent_alpha unless(enabled)
  end  
  def draw_item_name(item, x, y, enabled = true, width = 172)
    return unless item
    drawing_sandbox() do
      draw_item_icon( item, x, y, enabled )
      change_color(normal_color, enabled)
      contents.font.name = ["Microsoft YaHei"]
      draw_text(x + 24, y, width, line_height, item.name)
    end
  end
  def draw_actor_icons( actor, x, y, width = 96 )
    windex = width / 24
    icons = (actor.state_icons + actor.buff_icons)[0, windex]
    bmp = Cache.system("Status_StateBorder(Window)")
    for i in 0...windex
      contents.blt( x + 24 * i, y, bmp, bmp.rect )
    end  
    icons.each_with_index { |n, i|
      draw_icon(n, x + 24 * i, y) 
    }
    #bmp.dispose()
  end
  def draw_tiny_character( info )
    x, y = info[:x] || 0, info[:y] || 0
    character_name  = info[:character_name] || ""
    character_index = info[:character_index] || 0
    character_hue   = info[:character_hue] || 0
    return unless character_name
    bitmap = Cache.character(character_name, character_hue).clone
    sign = character_name[/^[\!\$]./]
    if sign && sign.include?('$')
      cw = bitmap.width / 3
      ch = bitmap.height / 4
    else
      cw = bitmap.width / 12
      ch = bitmap.height / 8
    end
    h = info[:height] || (ch * 0.8)
    n = character_index
    src_rect = Rect.new((n%4*3+1)*cw, (n/4*4)*ch, cw, h)
    contents.blt(x - cw / 2, y - ch, bitmap, src_rect)
    bitmap.dispose
    return src_rect
  end
  def draw_battler_hp( info )
    battler = info[:battler]
    rate = battler.hp_rate
    colors  = DrawExt::HP1_BAR_COLORS
    draw_gauge_ext( info.merge( 
        :colors  => colors,
        :value   => battler.hp, 
        :max     => battler.mhp 
      )
    )    
  end    
  def draw_battler_mp( info )
    battler = info[:battler]
    rate = battler.mp_rate 
    colors  = DrawExt::MP_BAR_COLORS
    draw_gauge_ext( info.merge( 
        {
          :colors  => colors,
          :value   => battler.mp, 
          :max     => battler.mmp 
        }
      )
    )
  end
  def draw_battler_wt( info )
    battler = info[:battler]
    rate = battler.wt_rate 
    colors  = DrawExt::WT_BAR_COLORS
    draw_gauge_ext( info.merge( 
        {
          :colors  => colors,
          :value   => battler.wt, 
          :max     => battler.mwt
        }
      )
    )
  end  
  def draw_gauge_ext( info )
    x, y       = info[:x] || 0, info[:y] || 0
    width      = info[:width] || 128
    height     = info[:height] || 20
    value, max = info[:value] || 0, info[:max] || 0
    rate       = value.to_f / max.max(1) 
    colors     = info[:colors] || DrawExt::DEF_BAR_COLORS
    bar_method = info[:bar_method] || method( :ext_draw_bar2 )
    bar_method.call( 
      {
        :x     => x,
        :y     => y,
        :width => width,
        :height=> height,
        :rate  => rate
      }.merge(colors)
    )  
    unless info[:dont_draw_text] 
      x += 4
      contents.font.size = Font.default_size - 4
      change_color( normal_color )
      draw_text( x, y, width-4, height, sprintf("%s/%s", value, max), 1 )
      contents.font.size = Font.default_size
    end
  end
  SIMPLE_EXP = true
  def draw_battler_exp( info )
    battler = info[:battler]
    x, y    = info[:x] || 0, info[:y] || 0
    width   = info[:width] || 128
    height  = info[:height] || 20
    exp  = battler.exp - battler.current_level_exp
    mexp = battler.next_level_exp - battler.current_level_exp
    exprate = exp / mexp.to_f
    if battler.level == battler.max_level
      exp = mexp ; info[:dont_draw_text] = true
    end  
    ext_draw_bar1( 
      {
        :x     => x,
        :y     => y,
        :width => width,
        :height=> height-6,
        :rate  => exprate,
      }.merge( DrawExt::RUBY_BAR_COLORS )
    )
    crexp, mlvexp = battler.exp, battler.max_level_exp
    crexp = crexp.min(mlvexp)
    ext_draw_bar4(
      {
        :x     => x,
        :y     => y+(height-6),
        :width => width,
        :height=> 6,
        :rate  => crexp / mlvexp.to_f,
      }.merge( DrawExt::RUBY_BAR_COLORS )
    )
    unless info[:dont_draw_text]
      drawing_sandbox do
        x += 4
        contents.font.size = Font.default_size - 4
        change_color( normal_color )
        text = SIMPLE_EXP ? format("%s/100", (exprate*100).to_i) : format("%s/%s", exp, mexp)
        draw_text( x, y, width, height, text, 1 )
      end  
    end  
  end
  # // Draw Icon 
  def draw_item_icon( item, x, y, enabled=true )
    return unless item
    draw_icon_ex( item.icon_index, x, y, enabled, item.iconset_name )
  end                
  def draw_icon_ex(icon_index, x, y, enabled=true, iconset_name="Iconset")
    bitmap = Cache.system(iconset_name || "")
    rect = Rect.new(icon_index % 16 * 24, icon_index / 16 * 24, 24, 24)
    contents.blt(x, y, bitmap, rect, enabled ? 255 : translucent_alpha)
    Rect.new(x,y,24,24)
  end
  # // 03/01/2012 [
  def draw_icon_ex_stretch(icon_index,trect,enabled,iconset_name="Iconset")
    bitmap = Cache.system(iconset_name || "")
    rect = Rect.new(icon_index % 16 * 24, icon_index / 16 * 24, 24, 24)
    contents.stretch_blt(trect, bitmap, rect, enabled ? 255 : translucent_alpha)
    trect
  end
  # // 03/01/2012 ]
  #--------------------------------------------------------------------------
  # ● アイコンの描画
  #     enabled : 有効フラグ。false のとき半透明で描画
  #--------------------------------------------------------------------------
  def draw_icon(icon_index, x, y, enabled = true)
    draw_icon_ex(icon_index, x, y, enabled, "Iconset")
  end
  # // 02/06/2012
  def draw_icon_sys( icon_index, x, y, enabled=true )
    bitmap = Cache.system( "SysIconset" )
    rect = Rect.new(icon_index % 16 * 8, icon_index / 16 * 8, 8, 8)
    contents.blt(x, y, bitmap, rect, enabled ? 255 : translucent_alpha)
  end
  def draw_horz_line(y,x=0,width=self.width-(padding*2),height=2,color=line_color)
    contents.fill_rect( x, y, width, height, color )
  end  
  def draw_box( x, y, width, height )
    ext_draw_box3( 
      :x             => x,
      :y             => y,
      :width         => width,
      :height        => height,
      :base_color    => line_color,
      :padding       => 2,
      :footer_height => 4
    )
  end
  def line_color()
    col = Pallete.sym_color( :brown1 )
    col.alpha = 48
    col
  end
  # // 02/13/2012
  def draw_actor_equip_at_ex(actor, index, rect, enabled=true)
    return unless actor
    item = actor ? actor.equips[index] : nil
    bmp = Bitmap.new(*rect.xto_a(:width,:height))
    bmp.gui_draw_equip_header_base(
      :x     => 0,
      :y     => 0,
      :width => rect.width
      #//:height=> rect.height
    )
    contents.blt(rect.x,rect.y,bmp,bmp.rect,enabled ? 255 : translucent_alpha)
    bmp.dispose()
    hrect = rect.clone
    hrect.contract!(12,1)#30)
    contents.font.set_style( :window_header )
    draw_text( hrect.x, hrect.y, hrect.width, 14, slot_name(index), 1 )
    return unless item
    drawing_sandbox() do 
      rect2 = rect.dup
      contents.font.set_style( :simple_black )
      rect2.x += 6
      rect2.y += 14
      draw_item_icon( item, rect2.x, rect2.y )
      rect2.x += 26 + 4
      rect2.y += 3
      contents.font.size = Font.default_size - 8
      draw_text( rect2.x, rect2.y, rect2.width, 14, item.name )
      draw_item_small_exp(item,rect2.x-4,rect.y+38-6,rect.width-64)
    end 
  end
  def draw_item_small_exp(item,x,y,width=24,height=6)
    return unless Ex_Database.ex_equip_item?(item)
    ext_draw_bar2(
      :x      => x,
      :y      => y,
      :width  => width,
      :height => height,
      :rate   => item.exp_rate,
    )
  end 
  def slot_name(index)
    bat = (self.actor||self.battler)
    bat ? Vocab::etype(bat.equip_slots[index]) : ""
  end
  # // 02/18/2012
  def draw_skill_ex(skill,rect,skbi=0,enabled=true)
    bmp = Bitmap.new(*rect.xto_a(:width,:height))
    bmp.gui_draw_skill_border(
      :x => 0,
      :y => 0,
      :height => 24,
      :width => rect.width,
      :index => skbi
    )
    contents.blt(rect.x,rect.y,bmp,bmp.rect,enabled ? 255 : translucent_alpha)
    bmp.dispose()
    drawing_sandbox do 
      return unless(skill && skill.id > 0)
      bat = (self.actor||self.battler)
      rect2 = rect.clone #rect.squeeze_horz(24+16)
      rect2.width -= 72 #+ 40
      rect2.x += 40
      if(enabled) # // Draw the skill's icon
        draw_item_icon(skill, rect.x, rect.y, true) 
      else # // Draw the disabled skill icon, the brown one . x .
        draw_icon_ex(16,rect.x,rect.y,true,DB.iconset_name(:elements)) 
      end  
      contents.font.set_style(:window_header)
      rect3 = rect2.dup ; rect3.width -= 24
      change_color(contents.font.color, enabled)
      draw_text(rect3, skill.name, 1)
      contents.font.outline = true
      contents.font.out_color = contents.font.color
      contents.font.size += 2
      draw_skill_cost(rect2, skill, bat, enabled)
      ind = Icon.cost(:mp)
      inm = Database.iconset_name(:costs)
      draw_icon_ex(ind,rect.x+rect.width-24-6,rect.y,enabled,inm)
      dy = 20 - h = 20 * bat.get_magiclearn_r(skill.id)
      contents.fill_rect(rect.x+25,rect.y+2+dy,3,h,Pallete[:sys_orange].xset(:alpha=>enabled ? 255 : translucent_alpha))
    end
  end 
  def draw_skill_cost(rect, skill,actor=(self.actor||self.battler),enabled=enable?(skill))
    if actor.skill_tp_cost(skill) > 0
      change_color(tp_cost_color, enabled)
      draw_text(rect, actor.skill_tp_cost(skill), 2)
    elsif actor.skill_mp_cost(skill) > 0
      change_color(mp_cost_color, enabled)
      draw_text(rect, actor.skill_mp_cost(skill), 2)
    elsif actor.skill_hp_cost(skill) > 0
      change_color(hp_cost_color, enabled)
      draw_text(rect, actor.skill_hp_cost(skill), 2)  
    end
  end
  # // 02/25/2012
  def draw_actor_element( actor, x, y, enabled=true, border=true )
    draw_element_icon(actor.element_id,x,y,enabled,border)
  end
  def draw_element_icon(element_id,x,y,enabled=true,border=true)
    ind = Icon.element(element_id)
    inm = Database.iconset_name(:elements)
    dx, dy = x, y
    if border
      dx += (32 - 24) / 2 
      dy += (32 - 24) / 2 
      bmp = Cache.system( "Craft_Borders(Window)" )
      contents.blt( x, y, bmp, Rect.new( 0, 32*5, 32, 32 ) )
    end   
    draw_icon_ex(ind,dx,dy,enabled,inm)
    border ? Rect.new(x,y,32,32) : Rect.new(x,y,24,24)
  end  
  def draw_fract(x, y, t1, t2, w=56,h=line_height)
    draw_text(x, y, w, h, t1, 2)
    contents.fill_rect( x, y+h, w, 2, line_color )
    draw_text(x, y+h, w, h, t2, 2)
    Rect.new(x,y,w,h*2)
  end
  # //
  def draw_art(item,rect,enabled=true)
    bmp = Bitmap.new(*rect.xto_a(:width,:height))
    bmp.gui_draw_art_border(
      :x     => 0,
      :y     => 0,
      :width => rect.width,
      :height=> rect.height
    )
    contents.blt(rect.x,rect.y,bmp,bmp.rect,enabled ? 255 : translucent_alpha)
    bmp.dispose()
    if item
      bat = (self.actor||self.battler)
      rect2 = rect.squeeze(24,false,1)
      draw_item_icon(item, rect.x, rect.y, enabled)
      contents.font.set_style(:window_header)
      rect3 = rect2.dup 
      change_color(contents.font.color, enabled)
      draw_text(rect3, item.name, 1)
    end
  end
  def draw_actor_arts(actor,rect)
    arts = actor.arts
    drect = rect.clone
    for i in 0...arts.size
      draw_art(arts[i],drect)
      drect.offset_vert!()
    end  
    drect
  end  
  # // 02/27/2012  
  def drawing_sandbox()
    oldfont = contents.font.nfs
    yield
    contents.font.set(oldfont)
  end
  # // 03/05/2012
  def element_color(element_id)
    Pallete["element#{element_id}".to_sym] || Pallete[:white]
  end 
  # // Bresenham's line algorithm
  def draw_line(x1,y1,x2,y2,color)
    dx = x2 - x1
    dy = y2 - y1
    sx = x1 < x2 ? 1 : -1
    sy = y1 < y2 ? 1 : -1
    err = (dx-dy).to_f
    e2 = 0
    loop do
      contents.set_pixel(x1,x2,color)
      break if(x1 == x2 && y1 == y2)
      e2 = 2*err
      if e2 > -dy 
        err = err - dy
        x1  = x1 + sx  
      end
      if e2 < dx 
        err = err + dx
        y1  = y1 + sy 
      end
    end  
  end
end  
class Artist
  include DrawShare
end  
class Window::Base
  def drawing_sandbox()
    oldfont = contents.font.nfs
    yield
    contents.font.set(oldfont)
  end
end  
=begin # // . x.  Refer to Artist class
class Window::Base
  include DrawShare
end  
=end
