# Mixin::AbilityConstants
# // 02/29/2012
# // 02/29/2012
module Mixin::AbilityConstants
  ABILITY_ENCOUNTER_HALF    = 0           
  ABILITY_ENCOUNTER_NONE    = 1           
  ABILITY_CANCEL_SURPRISE   = 2           
  ABILITY_RAISE_PREEMPTIVE  = 3           
  ABILITY_GOLD_DOUBLE       = 4           
  ABILITY_DROP_ITEM_DOUBLE  = 5           
end  
