# Container::Addons
# // 02/06/2012
# // 02/06/2012
module Container::Addons
end  
# // 02/06/2012
module Container::Addons::Base
  private
  def init_caddons()
  end  
  def dispose_caddons()
  end
  def update_caddons()
  end  
end  
class Sprite::Background < Sprite ; end # // 03/03/2012
# // 02/06/2012
module Container::Addons::Background_Glass
  private
  def init_gbackground()
    @gbackground_sprite = Sprite::Background.new(self.viewport)
    refresh_gbackground()
    update_gbackground_pos()
    update_gbackground_tone()
  end  
  def dispose_gbackground()
    @gbackground_sprite.dispose_all()
    @gbackground_sprite = nil
  end
  def update_gbackground()
    @gbackground_sprite.update()
  end
  def _redraw()
    super()
    refresh_gbackground()
  end
  def _update_position() # // Internal . x .
    super()
    update_gbackground_pos() 
  end    
  def _update_size()
    super()
    @gbackground_sprite.width = self.width #/ @orgsize.width.to_f
    @gbackground_sprite.height = self.height #/ @orgsize.height.to_f
  end  
  def _update_openness()
    super()
    @gbackground_sprite.zoom_y = open_height() / self.height.to_f
    update_gbackground_pos()
  end 
  def _update_visible()
    super()
    @gbackground_sprite.visible = self.visible
  end  
  def _update_viewport()
    super()
    @gbackground_sprite.viewport = self.viewport
  end
  def init_caddons()
    super()
    init_gbackground()
  end  
  def dispose_caddons()
    super()
    dispose_gbackground()
  end
  def update_caddons()
    super()
    update_gbackground()
  end
  public 
  def refresh_gbackground()
    @gbackground_sprite.dispose_bitmap() 
    @gbackground_sprite.bitmap = Bitmap.new( self.width, self.height )
    @gbackground_sprite.bitmap.ext_draw_bar2( { }.merge(DrawExt::TRANS_BAR_COLORS) )
    bmp = Cache.picture("Smuge01")
    @gbackground_sprite.bitmap.stretch_blt(
      @gbackground_sprite.bitmap.rect,
      bmp,
      bmp.rect,
      25
    )
  end   
  def update_gbackground_pos()
    @gbackground_sprite.x = self.x
    @gbackground_sprite.y = open_y1()
    @gbackground_sprite.z = self.z - 1
  end
  def gbackground_tone()
    Tone.new()
  end  
  def update_gbackground_tone()
    @gbackground_sprite.tone.set(gbackground_tone)
  end    
end
# // 02/06/2012
module Container::Addons::Background_Simple
  def init_sbackground()
    @sbackground_sprite = Sprite::Background.new(self.viewport)
    refresh_sbackground()
    update_sbackground_pos()
  end  
  def dispose_sbackground()
    @sbackground_sprite.dispose_all()
    @sbackground_sprite = nil
  end
  def update_sbackground()
    @sbackground_sprite.update()
  end
  def _redraw()
    super()
    refresh_sbackground()
  end 
  def refresh_sbackground()
    @sbackground_sprite.dispose_bitmap() 
    @sbackground_sprite.bitmap = Bitmap.new( self.width, self.height )
    draw_sbackground()
  end  
  def sbackground_theme
    Themes[:modern_dark]
  end  
  def draw_sbackground()
=begin    
    @sbackground_sprite.bitmap.fill_rect(
      @sbackground_sprite.bitmap.rect, Pallete[:brown1]
    )
    @sbackground_sprite.bitmap.fill_rect(
      @sbackground_sprite.bitmap.rect.contract(1), Pallete[:paper1]
    )
    r = @sbackground_sprite.bitmap.rect.contract(2)
    bmp=@sbackground_sprite.bitmap.ext_draw_box3(
      #:x             => r.x,
      #:y             => r.y,
      :width         => r.width,
      :height        => r.height,
      :base_color    => Pallete[:brown1],
      :padding       => 2,
      :footer_height => (@sbackground_sprite.bitmap.height * 0.21).to_i,
      :return_only   => true
    )
    @sbackground_sprite.bitmap.blt(r.x,r.y,bmp,bmp.rect,128)
    bmp.dispose
=end
    theme = sbackground_theme
    bmp = @sbackground_sprite.bitmap 
    bmp.fill_rect(bmp.rect,theme[:outline])
    bmp.fill_rect(bmp.rect.contract(1),theme[:base])
    bmp.fill_rect(bmp.rect.contract(standard_padding),theme[:inline])
    bmp.fill_rect(bmp.rect.contract(standard_padding+1),theme[:base])
    rect = bmp.rect.xset(:height=>1).contract(standard_padding/2,1)
    rect.xpush!(2,2)
    bmp.fill_rect(rect,theme[:inline])
    rect.xpush!(2,2)
    bmp.fill_rect(rect,theme[:inline])
  end  
  def update_sbackground_pos()
    @sbackground_sprite.x = self.x
    @sbackground_sprite.y = open_y1()
    @sbackground_sprite.z = self.z - 1
  end
  def _update_position() # // Internal . x .
    super()
    update_sbackground_pos 
  end
  def _update_size()
    super()
    @sbackground_sprite.width = self.width #/ @orgsize.width.to_f
    @sbackground_sprite.height = self.height #/ @orgsize.height.to_f
  end  
  def _update_openness()
    super()
    @sbackground_sprite.zoom_y = open_height() / self.height.to_f
    update_sbackground_pos()
  end 
  def _update_visible()
    super()
    @sbackground_sprite.visible = self.visible
  end 
  def _update_viewport()
    super()
    @sbackground_sprite.viewport = self.viewport
  end
  def sbackground_opacity
    @sbackground_sprite.opacity
  end  
  def sbackground_opacity=(n)
    @sbackground_sprite.opacity = n
  end  
  def init_caddons()
    super()
    init_sbackground()
  end  
  def dispose_caddons()
    super()
    dispose_sbackground()
  end
  def update_caddons()
    super()
    update_sbackground()
  end  
end
# // 02/06/2012
module Container::Addons::Background_Menu
  def init_mbackground()
    @mbackground = Plane.new(self.ownviewport)
    @mbackground.z = -1
    @mbackground.bitmap = Cache.system("MenuBackground2")
    @mbackground.tone.gray = 255
    @mbackground.opacity = mbackground_opacity
    @mbackground.blend_type = 2
  end  
  def mbackground_opacity()
    255 * 0.4
  end  
  def dispose_mbackground()
    @mbackground.dispose
  end  
  def update_mbackground
    #@mbackground.update # >,> Plane's cant be updated...
    @mbackground.oy -= 1 if self.active
    unless self.active
      @mbackground.opacity = (@mbackground.opacity - (255 / 60.0)).clamp(0,mbackground_opacity) 
    else  
      @mbackground.opacity = (@mbackground.opacity + (255 / 60.0)).clamp(0,mbackground_opacity)
    end  
  end  
  def refresh_mbackground()
  end  
  def _redraw()
    super()
    refresh_mbackground()
  end 
  def visible=(n)
    super(n)
    @mbackground_sprite.visible = self.visible
  end 
  def init_caddons()
    super()
    init_mbackground()
  end  
  def dispose_caddons()
    super()
    dispose_mbackground()
  end
  def update_caddons()
    super()
    update_mbackground()
  end  
end  
# // 02/06/2012
module Container::Addons::OwnViewport
  attr_reader :ownviewport
  def init_ownviewport
    @ownviewport = Viewport.new(oviewport_x, oviewport_y, oviewport_width, oviewport_height)
    update_oviewport_all()
  end
  def dispose_ownviewport
    @ownviewport.dispose()
  end  
  def update_ownviewport
    @ownviewport.update()
  end  
  def _redraw()
    super()
    update_oviewport_all()
  end 
  def oviewport_x
    self.x + (self.viewport ? self.viewport.rect.x : 0)
  end  
  def oviewport_y
    self.open_y1 + (self.viewport ? self.viewport.rect.y : 0)
  end
  def oviewport_z
    self.z + (self.viewport ? self.viewport.z : 0)
  end
  def oviewport_width
    self.width
  end  
  def oviewport_height
    self.open_height
  end 
  def update_oviewport_all()
    update_oviewport_pos()
    update_oviewport_size()
  end
  def update_oviewport_pos()
    @ownviewport.rect.x = oviewport_x()
    @ownviewport.rect.y = oviewport_y()
    @ownviewport.z = oviewport_z()
  end
  def update_oviewport_size()
    @ownviewport.rect.width = oviewport_width
    @ownviewport.rect.height = oviewport_height
  end  
  def update_oviewport_visible()
    @ownviewport.visible = self.visible
  end
  
  def _update_position() # // Internal . x .
    super()
    update_oviewport_pos()
  end    
  def _update_size()
    super()
    update_oviewport_size()
  end  
  def _update_visible()
    super()
    update_oviewport_visible() 
  end  
  def _update_openness()
    super()
    return unless @ownviewport
    update_oviewport_pos()
    update_oviewport_size() 
  end  
  def init_caddons()
    super()
    init_ownviewport()
  end  
  def dispose_caddons()
    super()
    dispose_ownviewport()
  end
  def update_caddons()
    super()
    update_ownviewport()
  end 
end  
# // 02/06/2012
class Sprite::Content < Sprite ; end
module Container::Addons::Contents
  def standard_artist
    Artist
  end  
  def artist
    @artist
  end
  def open_artist(&block)
    if block_given?()
      @artist.instance_ex(self,&block) 
    else
      return @artist
    end  
  end
  attr_accessor :contents
  attr_reader :content_sprite
  def init_contents()
    @content_sprite = Sprite::Content.new(self.ownviewport)
    create_contents()
    @artist = standard_artist.new(self)
  end 
  def dispose_all_contents()
    self.contents.dispose() unless self.contents.disposed? if self.contents
    @content_sprite.dispose()
  end
  def update_contents()
    @content_sprite.update
  end  
  def _redraw()
    super()
    create_contents()
  end 
  def _update_position()
    super()
    update_content_pos()
  end  
  def _update_size()
    super()
    return unless @content_sprite
    @content_sprite.src_rect.width = self.width
    @content_sprite.src_rect.height = self.height
  end  
    
  def contents_x
    self.x + standard_padding 
  end  
  def contents_y
    self.y + standard_padding
  end 
  def contents_rect
    #@content_sprite.rect
    Rect.new(contents_x, contents_y, 
     self.width-(standard_padding*2),self.height-(standard_padding*2)-padding_bottom)
  end
  def ox
    @content_sprite.ox
  end  
  def oy
    @content_sprite.oy
  end 
  def ox=(n)
    @content_sprite.ox = n
  end  
  def oy=(n)
    @content_sprite.oy = n
  end
  def update_content_pos()
    @content_sprite.x = standard_padding
    @content_sprite.y = standard_padding
    @content_sprite.z = self.z
  end  
  def create_contents()
    self.contents.dispose() unless self.contents.disposed? if self.contents
    wd = self.width - (standard_padding*2)
    hg = self.height - (standard_padding*2)
    wd = wd.max(1)
    hg = hg.max(1)
    self.contents = Bitmap.new(wd, hg)
    @content_sprite.bitmap = self.contents
  end   
  def _update_visible()
    super()
    @content_sprite.visible = self.visible 
  end  
  def _update_viewport()
    super()
    @content_sprite.viewport = self.ownviewport 
  end  
  def contents_opacity
    @content_sprite.opacity
  end  
  def contents_opacity=(n)
    @content_sprite.opacity = n
  end
  def init_caddons()
    super()
    init_contents()
  end  
  def dispose_caddons()
    super()
    dispose_all_contents() # // . x .
  end
  def update_caddons()
    super()
    update_contents()
  end
end  
# // 02/06/2012
module Container::Addons::StandardDrawing
  include DrawExt::Include
  def _ext_drawing_bitmap()
    contents
  end  
  include Mixin::DrawTextEx
  include DrawShare
  def draw_actor_name(actor, x, y, width = 112)
    #change_color(hp_color(actor))
    draw_text(x, y, width, line_height, actor.name)
  end
  def draw_actor_level(actor, x, y)
    change_color(system_color)
    draw_text(x, y, 32, line_height, Vocab::level_a)
    change_color(normal_color)
    draw_text(x + 8, y, 48, line_height, actor.level, 2)
  end
end  
# // 02/06/2012
module Container::Addons::HandlerBase
  def init_handler()
    @handler = {}
  end  
  def set_handler(symbol, method)
    @handler[symbol] = method
  end
  def remove_handler( symbol )
    @handler.delete( symbol )
  end  
  def handle?(symbol)
    @handler.include?(symbol)
  end
  def call_handler(symbol)
    @handler[symbol].call if handle?(symbol)
  end
  def init_caddons
    super()
    init_handler()
  end  
end  
# // 02/06/2012
class Alerting_Rect < Rect
  def initialize(afunction,*args,&block)
    @afunction = afunction # // Alerting Function
    super(*args,&block)
  end  
  def set(*args,&block)
    super(*args,&block)
    @afunction.call(:set)
  end 
  def empty(*args,&block)
    super(*args,&block)
    @afunction.call(:empty)
  end  
  def x=(n)
    super(n)
    @afunction.call(:x)
  end  
  def y=(n)
    super(n)
    @afunction.call(:y)
  end  
  def width=(n)
    super(n)
    @afunction.call(:width)
  end  
  def height=(n)
    super(n)
    @afunction.call(:height)
  end  
end  
# // 02/06/2012
module Container::Addons::SelectableBase
  include Container::Addons::HandlerBase
  def col_max
    1
  end
  def spacing
    32
  end
  def item_max
    0
  end
  def item_width
    (width - standard_padding * 2 + spacing) / col_max - spacing
  end
  def item_height
    line_height
  end
  def row_max
    ((item_max + col_max - 1) / col_max).max(1)
  end
  def row
    index / col_max
  end
  def top_row
    oy / item_height
  end
  def top_row=(row)
    row = 0 if row < 0
    row = row_max - 1 if row > row_max - 1
    self.oy = row * item_height
  end
  def page_row_max
    (height - padding - padding_bottom) / item_height
  end
  def page_item_max
    page_row_max * col_max
  end
  def item_rect(index)
    rect = Rect.new
    rect.width = item_width
    rect.height = item_height
    rect.x = index % col_max * (item_width + spacing)
    rect.y = index / col_max * item_height
    rect
  end
  def item_rect_for_text(index)
    rect = item_rect(index)
    rect.x += 4
    rect.width -= 8
    rect
  end
  attr_reader :help_window
  def help_window=(help_window)
    @help_window = help_window
    call_update_help
  end
  def active=(active)
    super(active)
    update_cursor
    call_update_help
  end
  attr_reader :index
  def index=(index)
    @index = index
    update_cursor
    call_update_help
  end
  def pred_index(wrap=true)
    self.index = self.index.pred
    self.index = wrap ? self.index.modulo(item_max) : self.index.clamp(0,item_max-1)
  end
  def succ_index(wrap=true)
    self.index = self.index.succ
    self.index = wrap ? self.index.modulo(item_max) : self.index.clamp(0,item_max-1)
  end
  def call_update_help
    update_help if active && @help_window
  end
  def update_help
    @help_window.clear
  end
  def select(index)
    self.index = index if index
  end
  def unselect
    self.index = -1
  end  
  def draw_all_items
    item_max.times {|i| draw_item(i) }
  end
  def draw_item(index)
  end
  def clear_item(index)
    contents.clear_rect(item_rect(index))
  end
  def redraw_item(index)
    clear_item(index) if index >= 0
    draw_item(index)  if index >= 0
  end
  def redraw_current_item
    redraw_item(@index)
  end
  def refresh
    contents.clear
    draw_all_items
  end
end  
# // . x . Cursor stuffz
# // 02/06/2012
class Sprite::ContainerCursor < Sprite ; end
module Container::Addons::CursorBase  
  def on_cursor_rect_change(reason=:nil)
    update_cursor_sprite()
  end  
  def update_cursor_sprite()  
    @cursor_sprite.zoom_x = (@cursor_rect.width.to_f / @cursor_rect2.width).clamp(0.0,1.0)
    @cursor_sprite.zoom_y = (@cursor_rect.height.to_f / @cursor_rect2.height).clamp(0.0,1.0)
    @cursor_sprite.x = self.padding + @cursor_rect.x - ((@cursor_sprite.width-@cursor_rect.width)/2)
    @cursor_sprite.y = self.padding + @cursor_rect.y - ((@cursor_sprite.height-@cursor_rect.height)/2)
    @cursor_sprite.z = cursor_z
  end 
  def x=(n)
    super(n)
    update_cursor_sprite()
  end  
  def y=(n)
    super(n)
    update_cursor_sprite()
  end  
  def z=(n)
    super(n)
    update_cursor_sprite()
  end  
  def viewport=(n)
    super(n)
    @cursor_sprite.viewport = self.ownviewport
  end  
  def cursor_z
    self.z + 12
  end  
  def cont_cursor_base_color
    Pallete[:grey12].xset(:alpha=>98)#.lighten(0.4)
  end
  def cont_cursor_padding_color
    Pallete[:grey9]
  end  
  def init_cont_cursor()
    @index = 0
    @cursor_counter = 0
    @cursor_sprite = Sprite::ContainerCursor.new(self.ownviewport)
    @cursor_sprite.bitmap = Bitmap.new(item_width+4, item_height+4)
    r = @cursor_sprite.bitmap.rect.contract(2)
    @cursor_sprite.bitmap.ext_draw_box1(
      :x             => r.x,
      :y             => r.y,
      :width         => r.width,
      :height        => r.height,
      :base_color    => cont_cursor_base_color,
      :padding_color => cont_cursor_padding_color
    )
    @cursor_sprite.bitmap.blur
    @cursor_rect = Alerting_Rect.new(method(:on_cursor_rect_change),0,0,0,0)
    @cursor_rect2= Rect.new(0,0,item_width,item_height)
  end  
  def dispose_cont_cursor()
    @cursor_sprite.dispose_all()
    @cursor_sprite = nil
  end  
  def update_cont_cursor()
    process_cursor_move()
    process_handling()
    @cursor_counter = active ? @cursor_counter.succ.modulo(60) : 0
    @cursor_sprite.update
    @cursor_sprite.opacity = 55+(200*(Math.sin(Math::PI*@cursor_counter/60.0)).abs)
  end 
  def refresh_cont_cursor()
    dispose_cont_cursor()
    init_cont_cursor()
  end  
  def _redraw()
    super()
    refresh_cont_cursor()
  end  
  attr_reader :cursor_rect
  def update_cursor
    if @cursor_all
      cursor_rect.set(0, 0, contents.width, row_max * item_height)
      self.top_row = 0
    elsif @index < 0
      cursor_rect.empty
    else
      ensure_cursor_visible
      cursor_rect.set(item_rect(@index))
    end
  end
  def ensure_cursor_visible
    #self.top_row = row if row < top_row
    #self.bottom_row = row if row > bottom_row
  end
  def cursor_movable?
    active && open? && !@cursor_fix && !@cursor_all && item_max > 0
  end
  def cursor_down(wrap = false)
    if index < item_max - col_max || (wrap && col_max == 1)
      select((index + col_max) % item_max)
    end
  end
  def cursor_up(wrap = false)
    if index >= col_max || (wrap && col_max == 1)
      select((index - col_max + item_max) % item_max)
    end
  end
  def cursor_right(wrap = false)
    if col_max >= 2 && (index < item_max - 1 || (wrap && horizontal?))
      select((index + 1) % item_max)
    end
  end
  def cursor_left(wrap = false)
    if col_max >= 2 && (index > 0 || (wrap && horizontal?))
      select((index - 1 + item_max) % item_max)
    end
  end
  def cursor_pagedown
    if top_row + page_row_max < row_max
      self.top_row += page_row_max
      select([@index + page_item_max, item_max - 1].min)
    end
  end
  def cursor_pageup
    if top_row > 0
      self.top_row -= page_row_max
      select([@index - page_item_max, 0].max)
    end
  end
  def process_cursor_move
    return unless cursor_movable? && !win_busy?
    last_index = @index
    cursor_down (Input.trigger?(:DOWN))  if Input.repeat?(:DOWN)
    cursor_up   (Input.trigger?(:UP))    if Input.repeat?(:UP)
    cursor_right(Input.trigger?(:RIGHT)) if Input.repeat?(:RIGHT)
    cursor_left (Input.trigger?(:LEFT))  if Input.repeat?(:LEFT)
    cursor_pagedown   if !handle?(:pagedown) && Input.trigger?(:R)
    cursor_pageup     if !handle?(:pageup)   && Input.trigger?(:L)
    Sound.play_cursor if @index != last_index
  end
  def process_handling
    return unless open? && active && !win_busy?
    return process_ok       if ok_enabled?        && Input.trigger?(:C)
    return process_cancel   if cancel_enabled?    && Input.trigger?(:B)
    return process_pagedown if handle?(:pagedown) && Input.trigger?(:R)
    return process_pageup   if handle?(:pageup)   && Input.trigger?(:L)
  end
  def ok_enabled?
    handle?(:ok)
  end
  def cancel_enabled?
    handle?(:cancel)
  end
  def current_item_enabled?
    return true
  end
  def process_ok
    if current_item_enabled?
      Sound.play_ok
      Input.update
      deactivate
      call_ok_handler
    else
      Sound.play_buzzer
    end
  end
  def call_ok_handler
    call_handler(:ok)
  end
  def process_cancel
    Sound.play_cancel
    Input.update
    deactivate
    call_cancel_handler
  end
  def call_cancel_handler
    call_handler(:cancel)
  end
  def process_pageup
    Sound.play_cursor
    Input.update
    deactivate
    call_handler(:pageup)
  end
  def process_pagedown
    Sound.play_cursor
    Input.update
    deactivate
    call_handler(:pagedown)
  end
  def row_index
    return index / col_max
  end
  
  def init_caddons()
    super()
    init_cont_cursor()
  end  
  def dispose_caddons()
    super()
    dispose_cont_cursor()
  end
  def update_caddons()
    super()
    update_cont_cursor()
  end
end  
# // 02/06/2012
module Container::Addons::EaserCursor
  def cursor_easers
    #[:elastic_out, :elastic_out, :sine_out, :sine_out]
    # // x, y, width, height 
    @cursor_easers ||= [:sine_inout, :sine_inout, :sine_out, :sine_out] 
    return @cursor_easers 
  end  
  def cursor_times
    @cursor_times ||= [7, 7, 10, 10]
    return @cursor_times # // x, y, width, height 
  end 
  def cursor_exparams
    @cursor_exparams ||= [[],[],[],[]]
    return @cursor_exparams
  end  
  def update_cursor
    rect = @target_cursor_rect
    if @cursor_all
      rect.set(0, 0, contents.width, row_max * item_height)
      self.top_row = 0
    elsif @index < 0
      cursor_rect.empty
      rect.empty
    else
      ensure_cursor_visible
      rect.set(item_rect(@index))
    end
    if @_last_cursor != [@index, item_max, col_max, @cursor_all] 
      easers = cursor_easers
      times  = cursor_times.collect { |t| Tween.frames_to_tt(t) }
      exp    = cursor_exparams
      cra    = cursor_rect.to_a
      tcra   = @target_cursor_rect.to_a
      @_cursor_tweener.clear()
      for i in 0...4
        @_cursor_tweener.add_tween( cra[i],tcra[i],easers[i],times[i],exp[i] )
      end  
      @_last_cursor = [@index, item_max, col_max, @cursor_all]
    end  
  end
  def init_easer_cursor
    @_cursor_tweener = Tween::Multi.new()
    @target_cursor_rect = Rect.new(0,0,0,0)
  end  
  def update_easer_cursor
    @_cursor_tweener.update()
    cursor_rect.set(*@_cursor_tweener.values)
  end  
  def init_caddons
    super()
    init_easer_cursor()
  end 
  def update_caddons
    super()
    update_easer_cursor()
  end  
end  
# // 02/14/2012
# // 02/14/2012 
module Container::Addons::ContextMenu
  attr_reader :context_menu
  def init_context_menu()
    @context_menu = context_menu_class.new(context_menu_x, context_menu_y)
    @context_menu.parent = self
    @context_menu.visible = false
    @context_menu.set_handler(:cancel, method(:cm_cancel))
    update_cm_pos()
    set_cm_handlers()
  end  
  def set_cm_handlers()
  end  
  def cm_cancel()
    activate()
    @context_menu.deactivate
    @context_menu.hide
  end  
  def update_cm_pos()
    @context_menu.x = context_menu_x
    @context_menu.y = context_menu_y
    @context_menu.z = context_menu_z
  end  
  def update_context_menu()
    if MouseEx.right_click? && mouse_in_window?()
      deactivate
      @context_menu.activate
      @context_menu.show
      update_cm_pos()
      @context_menu.x = MouseEx.x
      @context_menu.y = MouseEx.y
    end if self.active 
    @context_menu.update
  end
  def dispose_context_menu()
    @context_menu.dispose
  end  
  def context_menu_x
    self.x
  end
  def context_menu_y
    self.y
  end  
  def context_menu_z
    self.z + 100 + (self.viewport ? self.viewport.z : 0)
  end  
  def context_menu_class
    Container::ContextMenu
  end  
  def viewport=(n)
    super(n)
    #@context_menu.viewport = n
  end  
  def init_caddons()
    super()
    init_context_menu()
  end
  def update_caddons()
    super()
    update_context_menu()
  end
  def dispose_caddons()
    super()
    dispose_context_menu()
  end
end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
