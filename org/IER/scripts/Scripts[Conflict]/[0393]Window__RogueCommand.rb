# Window::RogueCommand
#==============================================================================#
# ♥ Window::RogueCommand
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/28/2011
# // • Data Modified : 12/30/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/30/2011 V1.0
#
#==============================================================================#
class Window::RogueCommand < Window::Command
  class AddonBin_WRC < WindowAddons::AddonBin[WindowAddons::Header]
    def header_cube
      (c=super).xset(self.x+(self.width-c.width)/2)
    end 
  end  
  def addon_bin
    AddonBin_WRC
  end 
  def spacing
    2
  end  
  def col_max 
    3
  end 
  alias :org_item_width :item_width
  def item_width
    82
  end  
  def window_height
    hgc = (item_max / col_max) + (item_max % col_max > 0 ? 1 : 0)
    return (item_height * hgc) + standard_padding * 2
  end
  def window_width
    return ((item_width + spacing) * col_max) + (standard_padding * 2)
  end
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
