# Matrix Movement
circ = Circle.new(0,0,1)
ang = 0
x_press, y_press = 0.0,0.0
cube = Cube.new(*[0]*6)
cube.x = Graphics.width / 2 
cube.y = Graphics.height / 2
MouseEx.init
sp = Sprite.new()
sp.bitmap = Bitmap.new(32,32)
sp.bitmap.ext_draw_box3(
      :base_color    => Pallete[:sys_orange],
      :padding       => 4,
      :footer_height => 8
    )
sp.center_oxy#(32,32)  
velo_sp = Sprite.new()
velo_sp.bitmap = Bitmap.new(128,14)
velo_sp.bitmap.ext_draw_bar1(
  {
    #:x => 0,
    #:y => 0,
    #:width => 128,
    #:height => 14
  }.merge(DrawExt.quick_bar_colors(Pallete[:sys_blue]))
)
turn_sp = [Sprite.new(),Sprite.new()]
turn_sp[0].bitmap = Bitmap.new(64,14)
turn_sp[0].bitmap.ext_draw_bar1(
  {
    #:x => 0,
    #:y => 0,
    #:width => 128,
    #:height => 14
  }.merge(DrawExt.quick_bar_colors(Pallete[:sys_green]))
)
turn_sp[0].offset_vert!(1.0)
# // 
turn_sp[1].bitmap = Bitmap.new(64,14)
turn_sp[1].bitmap.ext_draw_bar1(
  {
    #:x => 0,
    #:y => 0,
    #:width => 128,
    #:height => 14
  }.merge(DrawExt.quick_bar_colors(Pallete[:sys_red]))
)
turn_sp[1].offset_vert!(1.0).offset_horz!(1.0)
# // 
velo = 0.0
accel = 0.0
turn_accel = 1.0
turn_velo = 5.0
max_velo = 5.0
max_develo = -3.0
max_accel = 1.0
max_deccel = -1.0
max_turn_accel = 1.0
#recoil_vel = 0
# // 
loop do
  Main.update
  if Input.press?(:LEFT)
    ang += turn_accel
  elsif Input.press?(:RIGHT)  
    ang -= turn_accel
  end
  ang %= 360
  if Input.press_any?(:UP,:DOWN)
    y_press, x_press = circ.get_angle_xy(ang)
    if Input.press?(:UP)
      accel = (accel+0.1).clamp(max_deccel,max_accel)
    elsif Input.press?(:DOWN)
      accel = (accel-0.1).clamp(max_deccel,max_accel)
    end  
  else
    accel = (accel+0.1).clamp(max_deccel,0) if accel < 0
    accel = (accel-0.1).clamp(0,max_accel) if accel > 0
  end  
  velo = (velo+accel).clamp(max_develo,max_velo)
  velo = (velo-0.1).clamp(0,max_velo) if velo > 0
  velo = (velo+0.1).clamp(max_develo,0) if velo < 0
  #recoil_vel = (recoil_vel+0.1).clamp(0,max_velo+2) if recoil_vel > 0
  #recoil_vel = (recoil_vel-0.1).clamp(-max_develo-2,0) if recoil_vel < 0
  cube.x += x_press * (velo) #+ recoil_vel)
  cube.y += y_press * (velo) #+ recoil_vel)
  #if cube.x > Graphics.width-sp.ox
  #  recoil_vel = max_develo
  #elsif cube.x < sp.ox
  #  recoil_vel = max_velo
  #end  
  #if cube.y > Graphics.height-sp.oy
  #  recoil_vel = max_develo
  #elsif cube.y < sp.oy
  #  recoil_vel = max_velo
  #end  
  cube.x = cube.x.clamp(sp.ox,Graphics.width-sp.ox)
  cube.y = cube.y.clamp(sp.oy,Graphics.height-sp.oy)
  sp.angle = ang
  sp.x,sp.y = cube.x, cube.y
  # // 
  velo_sp.src_rect.width = velo_sp.bitmap.width * velo.abs / max_velo
  if ang > 180
    turn_sp[0].src_rect.width = turn_sp[0].bitmap.width * turn_accel.abs / max_turn_accel
  else  
    turn_sp[1].src_rect.width = turn_sp[1].bitmap.width * turn_accel.abs / max_turn_accel
  end  
end  
