# Database (Shared Ref)
# // 02/02/2012
# // 02/02/2012
module Database
  WEAPON_LEVELCAP = 200
  ARMOR_LEVELCAP  = 200
  LEVELCAP        = 200
  TRAP_LEVELCAP   = 200
  BUILD_SET_SYMBOLS = [:actors, :classes, :skills, :items, :weapons, :armors, 
                       :enemies, :troops, :states, :animations, :tilesets, 
                       :commonEvents, :system, :mapinfos, 
                       :startup, :temp, :traps, :crafts, :spirit_classes]
  BUILD_FILENAMES = Hash[BUILD_SET_SYMBOLS.collect{|s|[s,s.to_s.capitalize+".rvdata2"]}]                    
  # // .__. House keeping...
  @current_db_verions = {
    # // Core
    :stringTable   => "0.001a",
    # // Default
    :actors        => "0.001c",
    :classes       => "0.001a",
    :skills        => "0.002a",
    :items         => "0.001a",
    :weapons       => "0.001a",
    :armors        => "0.001a",
    :enemies       => "0.001b",
    :troops        => "0.001a",
    :states        => "0.001a",
    :animations    => "0.002a",
    :tilesets      => "0.001a",
    :commonEvents  => "0.001a",
    :system        => "0.001a",
    # // EX
    :startup       => "0.001a",
    :temp          => "0.001a",
    :traps         => "0.001a",
    :crafts        => "0.001a",
    
    :spirit_classes=> "0.001a"
  }  
  # // >_> Just remake the entire database every startup
  @current_db_verions.keys.each { |k| @current_db_verions[k] = nil }
  @db_header = {}
  @build_set = {}
  include Mixin::RouteCommands
  
  Exp_Parameters = proc { |bs, cr, infa, infb| [bs || 10, cr || 7, infa || 10, infb || 10] }
  
  @class_name = {}
  @class_name[1] = "Fighter"  # // (8< YEAH DO SUMTHING @_@
  # // Envoy      , Templar   # // X-Classes
  @class_name[2] = "Witch"    # // 8) Ahhhhh 0:
  # // Archwitch , >,>
  @class_name[3] = "Squiress" # // :) Sounds cool? D8<
  # // Swordstress, >,>
  @class_name[4] = "Archer"   # // O: Simple
  @class_name[5] = "Aracaness"# // . x . It likes Arcane + ess (well it is XD)
  # // Siren, >,>
  @class_name[6] = "Ninja"    # // @_@ Couldn't find a replacement...
  # // Shinobi, >,>
  @class_name[7] = "Pandera"  # // o3o Original
  # // @__@ No idea
  @class_name[8] = "Technic"  # // o3o ORIGINAL
  # // @__@ Once again
  @class_name[9] = "Reaper"   # // Change >_>....
  # // @__@ Once again
  
  @iconset_name = {}
  @iconset_name[:materials]= "Iconset"#"Iconset_Ingridients"
  @iconset_name[:elements] = "Iconset_Elements"
  @iconset_name[:costs]    = "Iconset_Elements"
  
  @element_icon_index = {}
  @element_icon_index[:fire]   = 1
  @element_icon_index[:water]  = 2
  @element_icon_index[:earth]  = 3
  @element_icon_index[:wind]   = 4 
  @element_icon_index[:light]  = 5
  @element_icon_index[:dark]   = 6
  @element_icon_index[:hp]     = 7
  @element_icon_index[:mp]     = 8
  @element_icon_index[:wt]     = 9
  
  @element_id = @element_icon_index.clone
  
  @element_sym= @element_id.invert
  
  @element_icon_index2 = (0..9).to_a # // Array Map . x .
  @element_icon_type_offset = {}
  @element_icon_type_offset[:symbol] = 0
  @element_icon_type_offset[:skill]  = 16
  @element_icon_type_offset[:cost]   = 32
  @element_icon_type_offset[:art]    = 48
  
  @cost_icons = {}
  @cost_icons[:hp] = @element_icon_type_offset[:cost] + 7
  @cost_icons[:mp] = @element_icon_type_offset[:cost] + 8
  @cost_icons[:wt] = @element_icon_type_offset[:cost] + 9
  
  @skill_type_id = {}
  @skill_type_id[:normal] = 1
  @skill_type_id[:fire]   = 2
  @skill_type_id[:water]  = 3
  @skill_type_id[:earth]  = 4
  @skill_type_id[:wind]   = 5
  @skill_type_id[:light]  = 6
  @skill_type_id[:dark]   = 7
  # // o3o
  @skill_type_id[:hp] = 8
  @skill_type_id[:mp] = 9
  @skill_type_id[:wt] = 10
  def self.stype_id(sym)
    @skill_type_id[sym]
  end  
  
  @param_id = {}
  @param_id[:mhp] = 0
  @param_id[:mmp] = 1
  @param_id[:atk] = 2
  @param_id[:def] = 3
  @param_id[:mat] = 4
  @param_id[:mdf] = 5
  @param_id[:agi] = 6
  @param_id[:luk] = 7
  def self.param_id(sym)
    @param_id[sym]
  end
  
  def self.cost_icon(type)
    return @cost_icons[type]
  end 
  def self.element_id(sym)
    @element_id[sym]
  end  
  def self.element_icon_by_id(n)
    @element_icon_index2[n]
  end  
  def self.set_element_icon(n, obj=nil, type=:symbol)
    return @iconset_name[:elements], @element_icon_index[n] if(obj.nil?())
    obj.iconset_name = @iconset_name[:elements]
    obj.icon_index = @element_icon_index[n] + @element_icon_type_offset[type]
    obj
  end  
  def self.element2stype(id)
    @skill_type_id[@element_sym[id]]
  end    
  #def self.skill_id(sym)
  #  @skill_id[sym]
  #end  
  def self.iconset_name(name)
    @iconset_name[name]
  end 
  
  @scope = {}
  @scope[:none] = 0
  @scope[:one_foe] = @scope[:one_enemy] = 1
  @scope[:one_friend] = @scope[:one_ally] = 7
  @scope[:one_friend_dead] = @scope[:one_ally_dead] = 
   @scope[:one_friend_koed] = @scope[:one_ally_koed] = 9
  @scope[:user] = @scope[:self] = 11
  # // NYI
  @scope[:global]         = 20
  @scope[:foe_team]       = @scope[:enemy_team]       = 21
  @scope[:foe_team_alive] = @scope[:enemy_team_alive] = 22
  @scope[:foe_team_dead]  = @scope[:enemy_team_dead]  = 23
  @scope[:ally_team]      = @scope[:user_team]        = 31
  @scope[:ally_team_alive]= @scope[:user_team_alive]  = 32
  @scope[:ally_team_dead] = @scope[:user_team_dead]   = 33
  @scope[:everyone]       = 40
  @scope[:everyone_alive] = 41
  @scope[:everyone_dead]  = 42
  def self.scope(n);@scope[n];end
    
  @rogue = {}
  @rogue[:attack] = 1
  @rogue[:guard]  = 2
  @rogue[:nudge]  = 3
  @rogue[:skip]   = 4
  @rogue[:tame]   = 5
  @rogue[:skill]  = 10
  @rogue[:item]   = 11
  @rogue[:status] = 12
  @rogue[:hotkeys]= 13
  @rogue[:equip]  = 14
  @rogue[:list]   = 20
  @rogue[:minimap]= 21
  @rogue[:options]= 22
  @rogue[:save]   = 23
  def self.rogue_sym2id(sym);@rogue[sym];end
  
  @rogue_icon = {}
  @rogue_icon[@rogue[:attack]]  = 134
  @rogue_icon[@rogue[:guard]]   = 139
  @rogue_icon[@rogue[:skip]]    = 448
  @rogue_icon[@rogue[:nudge]]   = 11
  @rogue_icon[@rogue[:tame]]    = 122
  @rogue_icon[@rogue[:item]]    = 261
  @rogue_icon[@rogue[:skill]]   = 136
  @rogue_icon[@rogue[:status]]  = 233
  @rogue_icon[@rogue[:equip]]   = 170
  @rogue_icon[@rogue[:options]] = 337
  @rogue_icon[@rogue[:hotkeys]] = 143
  @rogue_icon[@rogue[:save]]    = 372
  @rogue_icon[@rogue[:minimap]] = 231
  @rogue_icon[@rogue[:list]]    = 234
  def self.rogue_icon(id);@rogue_icon[id];end

  @occasion = {}
  @occasion[:always] = 0
  @occasion[:battle] = 1
  @occasion[:menu]   = 2
  @occasion[:never]  = 3
  
  @hit_type = {}
  @hit_type[:certain]  = 0
  @hit_type[:physical] = 1
  @hit_type[:magical]  = 2
  
  @damage_type = {}
  @damage_type[:none]   = 0
  @damage_type[:hp_dam] = 1
  @damage_type[:mp_dam] = 2
  @damage_type[:hp_rec] = 3
  @damage_type[:mp_rec] = 4
  @damage_type[:hp_abs] = 5
  @damage_type[:mp_abs] = 6
  
  @item_id = {}
  @item_id[:materials] = 100
  
  @skill_id_off = {}
  @skill_id_off[:fire]  = 20
  @skill_id_off[:water] = 40
  @skill_id_off[:earth] = 60
  @skill_id_off[:wind]  = 80
  @skill_id_off[:light] = 100
  @skill_id_off[:dark]  = 120
  
  @weapon_id_off = {}
  @weapon_id_off[:spears]  = 0
  @weapon_id_off[:brushes] = 10
  @weapon_id_off[:swords]  = 20
  @weapon_id_off[:bows]    = 30
  @weapon_id_off[:fans]    = 40
  @weapon_id_off[:chains]  = 50
  @weapon_id_off[:hammers] = 60
  @weapon_id_off[:guns]    = 70
  @weapon_id_off[:scythes] = 80
  
  @weapon_icon_index = {}
  @weapon_icon_index[:spears]  = 0
  @weapon_icon_index[:brushes] = 16
  @weapon_icon_index[:swords]  = 32
  @weapon_icon_index[:bows]    = 48
  @weapon_icon_index[:staves]  = 0
  
  @weapon_type_id = {}
  @weapon_type_id[:spears]  = 1
  @weapon_type_id[:brushes] = 2
  @weapon_type_id[:swords]  = 3
  @weapon_type_id[:bows]    = 4
  @weapon_type_id[:staves]  = 16
  
  @armor_id_off = {}
  @armor_id_off[:shield]     = 0  # // Shield
  @armor_id_off[:helmet]     = 10 # // Genderless
  @armor_id_off[:helmetM]    = 20 # // Males only
  @armor_id_off[:helmetF]    = 30 # // Females only
  @armor_id_off[:body]       = 50 # // Genderless
  @armor_id_off[:bodyM]      = 60 # // Males only
  @armor_id_off[:bodyF]      = 70 # // Females only
  @armor_id_off[:accessory]  = 100 # // Genderless
  @armor_id_off[:accessoryM] = 110 # // Males only
  @armor_id_off[:accessoryF] = 120 # // Females only
  @armor_id_off[:arm]        = 150 # // Genderless
  @armor_id_off[:leg]        = 160 # // Genderless
  @armor_id_off[:crest]      = 180 # // Genderless
  
  @armor_icon_index = {}
  @armor_icon_index[:shield]     = (16*1)-1
  @armor_icon_index[:helmet]     = (16*5)-1
  @armor_icon_index[:helmetM]    = (16*6)-1
  @armor_icon_index[:helmetF]    = (16*7)-1
  @armor_icon_index[:body]       = (16*9)-1
  @armor_icon_index[:bodyM]      = (16*10)-1
  @armor_icon_index[:bodyF]      = (16*11)-1
  @armor_icon_index[:accessory]  = (16*13)-1
  @armor_icon_index[:accessoryM] = (16*14)-1
  @armor_icon_index[:accessoryF] = (16*15)-1
  @armor_icon_index[:arm]        = (16*17)-1
  @armor_icon_index[:armM]       = (16*18)-1
  @armor_icon_index[:armF]       = (16*19)-1
  @armor_icon_index[:leg]        = (16*21)-1
  @armor_icon_index[:legM]       = (16*22)-1
  @armor_icon_index[:legF]       = (16*23)-1
  @armor_icon_index[:crest]      = (16*25)-1
  
  @armor_type_id = {}
  @armor_type_id[:shield]    = 1
  @armor_type_id[:helmet]    = 2
  @armor_type_id[:helmetM]   = 3
  @armor_type_id[:helmetF]   = 4
  @armor_type_id[:body]      = 5
  @armor_type_id[:bodyM]     = 6
  @armor_type_id[:bodyF]     = 7
  @armor_type_id[:accessory] = 8
  @armor_type_id[:accessoryM]= 9
  @armor_type_id[:accessoryF]= 10
  @armor_type_id[:arm]       = 11
  @armor_type_id[:leg]       = 12
  @armor_type_id[:crest]     = 13
  
  @equip_type_id = {}
  @equip_type_id[:weapon]    = 0
  @equip_type_id[:shield]    = 1
  @equip_type_id[:helmet]    = 2
  @equip_type_id[:body]      = 3
  @equip_type_id[:accessory] = 4
  @equip_type_id[:arm]       = 5
  @equip_type_id[:leg]       = 6
  @equip_type_id[:crest]     = 7
  def self.etype_id(sym);@equip_type_id[sym];end
    
  @growth_class = {}
  #                            //[mhp,mmp,atk,def,mat,mdf,agi,lck]
  @growth_class[:normal]       = [  0,  0,  2,  0,  2,  0,  0,  0]
  @growth_class[:magic]        = [  0,  0,  1,  0,  3,  0,  0,  0]
  @growth_class[:swift]        = [  0,  0,  1,  0,  1,  0,  2,  0]
  @growth_class[:heavy]        = [  0,  0,  3,  0,  1,  0,  0,  0]
  @growth_class[:dual]         = [  0,  0,  2,  0,  1,  0,  1,  0]   
  
  # // Limit (New-8) (Old-15)
  @growth_class[:base_spears]  = [  0,  0,  9,  0,  1,  0, -2,  0]#[  0,  0,  8,  1,  1,  1,  3,  1]
  @growth_class[:base_brushes] = [  0,  0,  2,  0,  5,  0,  0,  1]#[  0,  0,  3,  0,  5,  1,  2,  4]
  @growth_class[:base_swords]  = [  0,  0,  6,  0,  2,  0,  0,  0]#[  0,  0,  5,  2,  2,  2,  2,  2]
  @growth_class[:base_bows]    = [  0,  0,  8,  0,  0,  0,  0,  0]#[  0,  0, 10, -1,  1,  2,  2,  1]
  @growth_class[:base_fans]    = [  0,  0,  4,  0,  4,  0,  0,  0]#[  0,  0,  5,  2,  2,  1,  1,  1]
  @growth_class[:base_chains]  = [  0,  0,  5,  0,  3,  0,  0,  0]#[  0,  0,  3,  2,  2,  3,  3,  2]
  @growth_class[:base_hammers] = [  0,  0, 12,  0,  1,  0, -5,  0]#[  0,  0, 12,  3,  1, -3,  1,  1]
  @growth_class[:base_guns]    = [  0,  0, 15, -2,  0, -2, -1, -2]#[  0,  0, 11,  0,  0,  0,  4,  0]
  @growth_class[:base_scythes] = [  0,  0, 10,  0,  0, -2,  0,  0]#[  0,  0, 10,  0,	3, 	0,	0, 	2]      

  @growth_class[:base_bombs]   = [  0,  0, 12,  0,  0,  0,  0,  0]#[  0,  0,  6,  2,  2,  2,  5, -2]
  @growth_class[:base_staves]  = [  0,  2,  4, -4,  8,  0, -2,  0]#[  0,  0, -3,  1, 10,  1,  1,  5]
  @growth_class[:base_claws]   = [  0,  0,  4,  0,  1,  0,  1,  0]#[  0,  0,  4, -2,  1,  6,  5,  1]
  @growth_class[:base_knives]  = [  0,  0,  4,  0,  2,  0,  2,  0]#[  0,  0,  3,  3,  1,  4,  3,  1]

  @growth_class[:spears]       = [  0,  0,  2,  0,  1,  0, -1,  0]#[  0,  0,  2,  1,  0,  1,  2,  0]
  @growth_class[:brushes]      = [  0,  0,  0,  0,  1,  0,  0,  1]#[  0,  0,  1,  0,  2,  0,  1,  2]
  @growth_class[:swords]       = [  0,  0,  1,  0,  1,  0,  0,  0]#[  0,  0,  1,  1,  1,  1,  1,  1]
  @growth_class[:bows]         = [  0,  0,  1,  0,  0,  0,  0,  0]#[  0,  0,  2,  0,  0,  2,  2,  0]
  @growth_class[:fans]         = [  0,  0,  1,  0,  1,  0,  0,  0]#[  0,  0,  1,  2,  1,  1,  0,  1]
  @growth_class[:chains]       = [  0,  0,  1,  0,  1,  0,  0,  0]#[  0,  0,  1,  1,  1,  1,  1,  1]
  @growth_class[:hammers]      = [  0,  0,  2,  0,  0,  0, -1,  0]#[  0,  0,  3,  2,  0,  0,  1,  0]      
  @growth_class[:guns]         = [  0,  0,  2,  0,  0,  0, -1,  0]#[  0,  0,  2,  1,  0,  1,  2,  0]
  @growth_class[:scythes]      = [  0,  0,  2,  0,  0,  0,  0,  0]#[  0,  0,  2,  0,  2,  0,  0,  2]      

  @growth_class[:bombs]        = [  0,  0,  2,  0,  0,  0,  0,  0]#[  0,  0,  1,  1,  0,  0,  3,  1]            
  @growth_class[:staves]       = [  0,  0,  0,  0,  2,  0,  0,  0]#[  0,  0,  1,  0,  3,  0,  0,  2]                            
  @growth_class[:claws]        = [  0,  0,  1,  0,  0,  0,  1,  0]#[  0,  0,  2,  0,  1,  1,  1,  1]                                  
  @growth_class[:knives]       = [  0,  0,  1,  0,  0,  0,  1,  0]#[  0,  0,  1,  0,  0,  3,  2,  0]                                        
                                                
  def self.params_from_growth( *growths )
    result = [0]*8 
    (growths.collect { |gn| @growth_class[gn] }).each do |g|
      g.each_with_index { |s, i| result[i] += s } 
    end
    result
  end  
  def self.weapon_parameter( weapon, parameter, level )
    n = params_from_growth(*weapon.growths)[parameter]
    n = weapon.params[parameter] + (level * n * 1.0)
    n.to_i
  end  
=begin 
  # OLD                        //[mhp,mmp,atk,def,spi,agi,dex,res]
  @growth_class[:base_swords]  = [  0,  0,  5,  2,  2,  2,  2,  2]
  @growth_class[:base_spears]  = [  0,  0,  8,  1,  1,  1,  3,  1]
  @growth_class[:base_scythes] = [  0,  0, 10,  0,	3, 	0,	0, 	2]      
  @growth_class[:base_brushes] = [  0,  0,  3,  0,  5,  1,  2,  4]
  @growth_class[:base_hammers] = [  0,  0, 12,  3,  1, -3,  1,  1]      
  @growth_class[:base_bombs]   = [  0,  0,  6,  2,  2,  2,  5, -2]
  @growth_class[:base_fans]    = [  0,  0,  5,  2,  2,  1,  1,  1]
  @growth_class[:base_bows]    = [  0,  0, 10, -1,  1,  2,  2,  1]
  @growth_class[:base_staves]  = [  0,  0, -3,  1, 10,  1,  1,  5]
  @growth_class[:base_claws]   = [  0,  0,  4, -2,  1,  6,  5,  1]
  @growth_class[:base_knives]  = [  0,  0,  3,  3,  1,  4,  3,  1]
  @growth_class[:base_guns]    = [  0,  0, 11,  0,  0,  0,  4,  0]
  @growth_class[:base_chains]  = [  0,  0,  3,  2,  2,  3,  3,  2]
  
  @growth_class[:swords]       = [  0,  0,  1,  1,  1,  1,  1,  1]
  @growth_class[:spears]       = [  0,  0,  2,  1,  0,  1,  2,  0]
  @growth_class[:scythes]      = [  0,  0,  2,  0,  2,  0,  0,  2]      
  @growth_class[:brushes]      = [  0,  0,  1,  0,  2,  0,  1,  2]
  @growth_class[:hammers]      = [  0,  0,  3,  2,  0,  0,  1,  0]      
  @growth_class[:bombs]        = [  0,  0,  1,  1,  0,  0,  3,  1]            
  @growth_class[:fans]         = [  0,  0,  1,  2,  1,  1,  0,  1]                
  @growth_class[:bows]         = [  0,  0,  2,  0,  0,  2,  2,  0]                      
  @growth_class[:staves]       = [  0,  0,  1,  0,  3,  0,  0,  2]                            
  @growth_class[:claws]        = [  0,  0,  2,  0,  1,  1,  1,  1]                                  
  @growth_class[:knives]       = [  0,  0,  1,  0,  0,  3,  2,  0]                                        
  @growth_class[:guns]         = [  0,  0,  2,  1,  0,  1,  2,  0]                                              
  @growth_class[:chains]       = [  0,  0,  1,  1,  1,  1,  1,  1]                                                    
=end      
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
