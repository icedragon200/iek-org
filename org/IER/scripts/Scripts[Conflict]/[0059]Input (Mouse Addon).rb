# Input (Mouse Addon)
# // 01/24/2012
# // 01/25/2012
if defined? Mouse
class << Input
  alias :pre_mouse_update :update 
  #alias :pre_mouse_trigger? :trigger?
  #alias :pre_mouse_press? :press?
  #alias :pre_mouse_repeat? :repeat?
  #def update(*args,&block)
  #  pre_mouse_update(*args,&block)
  #  MouseEx.update()
  #end   
  def mtrigger?(*n,&b)
    return true if n[0] == :C && MouseEx.left_click?
    return true if n[0] == :B && MouseEx.right_click?
    #pre_mouse_trigger?(*n)
    trigger?(*n,&b)
  end
  def mpress?(*n,&b)
    return true if n[0] == :C && MouseEx.left_press?
    return true if n[0] == :B && MouseEx.right_press?
    #pre_mouse_press?(*n)
    press?(*n,&b)
  end  
  def mrepeat?(*n,&b)
    return true if n[0] == :C && MouseEx.left_press?
    return true if n[0] == :B && MouseEx.right_press?
    #pre_mouse_repeat?(*n)  
    repeat?(*n,&b)
  end  
end # // Input
else
class << Input
  def mtrigger?(*args,&block) ; trigger?(*args,&block) ; end
  def mpress?(*args,&block) ; press?(*args,&block) ; end
  def mrepeat?(*args,&block) ; repeat?(*args,&block) ; end
end  
end # // Mouse
