# Handler::ViewEdge
# // 02/03/2012
# // 02/03/2012
class Handler::ViewEdge
  attr_reader :edge_responses
  def initialize(*args)
    @handler = {}
    @scroll_amp = 1.0
    set(*args)
  end  
  def set_handler(sym, meth)
    @handler[sym] = meth
  end  
  def response_size
    64
  end  
  def set_scroll_amp(n)
    @scroll_amp = n
  end  
  def scroll_amp( n )
    n * @scroll_amp #* (10 / (response_size / 32.0))
  end   
  def mscroll_up
    press = scroll_amp @edge_responses[0].rect.calc_pressure_vert(MouseEx.y, true) 
    @handler[:up].call(press) if @handler[:up]
  end
  def mscroll_down
    press = scroll_amp @edge_responses[1].rect.calc_pressure_vert(MouseEx.y) 
    @handler[:down].call(press) if @handler[:down]
  end
  def mscroll_left
    press = scroll_amp @edge_responses[2].rect.calc_pressure_horz(MouseEx.x, true) 
    @handler[:left].call(press) if @handler[:left]
  end
  def mscroll_right
    press = scroll_amp @edge_responses[3].rect.calc_pressure_horz(MouseEx.x) 
    @handler[:right].call(press) if @handler[:right]
  end 
  def set(*args)
    case args[0]
    when Rect
      rect = args[0]
    when Array
      rect = Rect.new(*args[0])
    when Hash
      rect = Rect.new(*args[0].get_values(:x,:y,:width,:height))
    else  
      rect = Rect.new(*args)
    end  
    size = response_size
    @edge_responses = []
    r = rect.clone
    r.height = size
    @edge_responses << Mouse::AreaOnEvent.new(r.dup)
    r.y = rect.height - r.height
    @edge_responses << Mouse::AreaOnEvent.new(r.dup)
    r = rect.clone
    r.width = size
    @edge_responses << Mouse::AreaOnEvent.new(r.dup)
    r.x = rect.width - r.width
    @edge_responses << Mouse::AreaOnEvent.new(r.dup)
    @edge_responses[0].set_handler(:over, method(:mscroll_up))
    @edge_responses[1].set_handler(:over, method(:mscroll_down))
    @edge_responses[2].set_handler(:over, method(:mscroll_left))
    @edge_responses[3].set_handler(:over, method(:mscroll_right))
  end  
  def update
    @edge_responses.each { |r| r.update }
  end  
  
  def make_edge_sprites
    a = Array.new(4) { Sprite.new() }
    edge_responses.each_with_index { |e, i| 
      a[i].bitmap = Bitmap.new(*e.rect.xto_a(:width,:height))
      a[i].bitmap.fill_rect(a[i].bitmap.rect, Pallete[:white])
      a[i].x, a[i].y = *e.rect.xto_a(:x,:y)
      a[i].z = 1000
      a[i].opacity = 128
    }
    a
  end  
end  
