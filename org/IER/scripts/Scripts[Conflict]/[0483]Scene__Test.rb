# Scene::Test
#==============================================================================#
# ♥ Scene::Test
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/08/2011
# // • Data Modified : 12/08/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/08/2011 V1.0 
#
#==============================================================================#  
class Scene::Test < Scene::Base
  def start()
    super()
    SceneManager.clear()
    @sprite = Sprite.new()
    @sprite.bitmap = Bitmap.new( @viewport.rect.width, @viewport.rect.height )
    bh = 16
    bc = @sprite.bitmap.height / bh
    bw = @sprite.bitmap.width / 5
    for i in 0..bc
      @sprite.bitmap.ext_draw_bar1( 
        {
          :bitmap => @sprite.bitmap,
          :x      => bw*0,
          :y      => i * bh,
          :width  => bw,
          :height => bh,
          :rate   => 1.0 * i / (bc-1),
        }.merge(DrawExt::BLACK_BAR_COLORS)#MP_BAR_COLORS) 
      )  
      val = sprintf("%s/%s", 100 * i / (bc-1), 100)
      @sprite.bitmap.font.size = 16
      @sprite.bitmap.draw_text( bw*0, i * bh, bw, 20, val, 1 )
      @sprite.bitmap.ext_draw_bar2( 
        {
          :bitmap => @sprite.bitmap,
          :x      => bw*1,
          :y      => i * bh,
          :width  => bw,
          :height => bh,
          :rate   => 1.0 * i / (bc-1),
        }.merge(DrawExt::WHITE_BAR_COLORS)#HP1_BAR_COLORS) 
      )
      @sprite.bitmap.draw_text( bw*1, i * bh, bw, 20, val, 1 )
      #DrawExt.draw_diamond(
      #  {
      #    :bitmap => @sprite.bitmap,
      #    :x      => bw * 2,
      #    :y      => i * 64,
      #    :width  => 64,
      #    :height => 64,
      #    :color  => Color.new( 200, 200, 200 )
      #  }
      #)      
      @sprite.bitmap.ext_draw_bar3( 
        {
          :bitmap => @sprite.bitmap,
          :x      => bw*2,
          :y      => i * bh,
          :width  => bw,
          :height => bh,
          :rate   => 1.0 * i.to_f / (bc-1),
          :divisions => 20,
        }.merge(DrawExt::HP2_BAR_COLORS) 
      )
      @sprite.bitmap.ext_draw_bar4( 
        {
          :bitmap => @sprite.bitmap,
          :x      => bw*3,
          :y      => i * bh,
          :width  => bw,
          :height => 6,
          :rate   => 1.0 * i / (bc-1),
        }.merge(DrawExt::HP1_BAR_COLORS) 
      )
      @sprite.bitmap.ext_draw_bar4( 
        {
          :bitmap => @sprite.bitmap,
          :x      => bw*3,
          :y      => (i * bh)+6,
          :width  => bw,
          :height => 6,
          :rate   => 1.0 * i / (bc-1),
        }.merge(DrawExt::HP2_BAR_COLORS) 
      )
      @sprite.bitmap.ext_draw_bar4( 
        {
          :bitmap => @sprite.bitmap,
          :x      => bw*3,
          :y      => (i * bh)+12,
          :width  => bw,
          :height => 6,
          :rate   => 1.0 * i / (bc-1),
        }.merge(DrawExt::HP3_BAR_COLORS) 
      )
      @sprite.bitmap.ext_draw_bar2( 
        {
          :bitmap => @sprite.bitmap,
          :x      => bw*4,
          :y      => i * bh,
          :width  => bw,
          :height => bh,
          :rate   => 1.0 * i / (bc-1),
        }.merge(DrawExt::RUBY_BAR_COLORS)
      )
    end
  end
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
