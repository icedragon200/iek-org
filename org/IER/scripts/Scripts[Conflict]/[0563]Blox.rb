# Blox
#~ =begin
#~ "FFFF".hex
#~ ("F"*4).hex
#~ 0xFFFF
#~ 2**16
#~ =
#~ 65535
#~ =end 
#~ def Color.random
#~   new(*3.times.collect{rand(256)})
#~ end  
#~ #(0xFFFF * 0.05).to_i
#~ block_size = 12
#~ page_count = 32
#~ screen_w2  = (Graphics.width - 32) / block_size / 2
#~ total_size = ((Graphics.width-32) / block_size) * (Graphics.height / block_size) # // Screen size
#~ total_size+= Graphics.width % block_size
#~ total_size*= page_count
#~ loop do
#~ colors = [
#~   #Pallete["gray#{i+1}".to_sym]}
#~   #Pallete[:sys_red],Pallete[:sys_green],Pallete[:sys_blue],Pallete[:sys_orange] 
#~   *128.times.collect{|i|Color.random}
#~ ].sort_by{|c|(c.red+1024)+(c.green+512)+c.blue}.collect{ |c|
#~   bmp = Bitmap.new(block_size,block_size)
#~   bmp.ext_draw_bar2( { }.merge(DrawExt.quick_bar_colors(c)) )
#~   bmp
#~ }
#~ total_size += total_size % colors.size
#~ arrasize = total_size / colors.size
#~ colors_ids = colors.size.times.inject([]) { |r,i|
#~   r + Array.new(arrasize,i)
#~ }  
#~ random_ids = Array.new(total_size) { 
#~   colors_ids.pick
#~ }
#~ random_ids.compact!
#~ sorted_ids = random_ids.sort.reverse
#~ viewport = Viewport.new
#~ viewport.rect.height -= 32
#~ sp = Sprite.new(viewport)
#~ hg = (random_ids.size / ((Graphics.height-32)/block_size)) 
#~ sp.bitmap = Bitmap.new(Graphics.width,hg * block_size)
#~ block = Rect.new(0,0,Graphics.width/block_size,hg)
#~ redraw_block = proc do |index,opacity=255|
#~   id = random_ids[index]
#~   x, y = index % block.width * block_size, index / block.width * block_size
#~   sp.bitmap.clear_rect(x,y,block_size,block_size)
#~   sp.bitmap.blt(x,y,colors[id],colors[id].rect,opacity)
#~ end
#~ for i in 0...random_ids.size ; redraw_block.call(i) ; end
#~ progress_bar = Sprite.new()  
#~ progress_bar.bitmap = Bitmap.new(Graphics.width, 32)
#~ progress_bar.bitmap.ext_draw_bar2(DrawExt.quick_bar_colors(Color.new(0,178,212)))
#~ progress_bar.salign(0,2)
#~ wd = progress_bar.bitmap.width
#~ tween = Tween.new(0,0,:back_out,1.0)
#~ last_oy = 0
#~ fibers = []
#~ # //
#~ MouseEx.init
#~ speed = 1
#~ begin
#~   eids = (sorted_ids-[sorted_ids.last])
#~   size = eids.size
#~ eids.each_with_index do |n,i|
#~   speed.times do 
#~     if Input.repeat?(:LEFT)
#~       speed = (speed - 1).max(1)
#~       puts "speed is now /#{speed}"
#~     elsif Input.repeat?(:RIGHT)
#~       speed = (speed + 1).min(30)
#~       puts "speed is now /#{speed}"
#~     end  
#~     Main.update
#~     oy = (((i / block.width) - screen_w2).max(0) * block_size)
#~     if last_oy != oy
#~       tween = Tween.new(last_oy,oy,:back_out,Tween.f2tt(40))
#~       last_oy = oy
#~     end  
#~     tween.update
#~     sp.oy = tween.value
#~     fibers.select! do |f|
#~       f[0] += 1
#~       redraw_block.call(f[1],(255/30)*f[0])
#~       redraw_block.call(f[2],(255/30)*f[0])
#~       if f[0] < 30
#~         true
#~       else
#~         redraw_block.call(f[1])
#~         redraw_block.call(f[2])
#~         false
#~       end  
#~     end 
#~   end
#~   ind = random_ids.reverse_index(n)
#~   random_ids[ind], random_ids[i] = random_ids[i], random_ids[ind]
#~   fibers << [0,ind,i] if random_ids[ind] != random_ids[i]
#~   #sp.update
#~   progress_bar.src_rect.width = wd * i / size.to_f
#~ end
#~ sleep 4.0
#~ sp.dispose_all
#~ progress_bar.dispose_all
#~ colors.each{|b|b.dispose}
#~ end
#~ end
