# Mouse::Test
# // 01/24/2012
# // 01/24/2012
if defined? Mouse
module Mouse
  class TestAreaOnEvent < AreaOnEvent
    def initialize( sp, v4 )
      super(v4)
      @sprite = sp
      set_handler(:over, proc { @sprite.opacity = 255 } )
      set_handler(:not_over, proc { @sprite.opacity = 128 } )
      set_handler(:left_click, proc { 
        Sound.play_ok
        @sprite.flash( Color.new(32, 32, 198), 30 )
      } )
      set_handler(:right_click, proc { 
        Sound.play_ok
        @sprite.flash( Color.new(198, 32, 32), 30 )
      } )
      set_handler(:middle_click, proc { 
        Sound.play_ok
        @sprite.flash( Color.new(32, 198, 32), 30 ) 
      } )
      set_handler(:left_press, proc { @sprite.flash( Color.new(32, 32, 198), 30 ) } )
      set_handler(:right_press, proc { @sprite.flash( Color.new(198, 32, 32), 30 ) } )
      set_handler(:middle_press, proc { @sprite.flash( Color.new(32, 198, 32), 30 ) } )
    end  
  end  
  def self.run_diagnose    
    MouseEx.init()
    sq = Rect.new(0,0,64,64)
    w = Graphics.width / sq.width
    h = Graphics.height / sq.height
    testcursors = []
    testsprites = []
    testbitmap = Bitmap.new( sq.width, sq.height )
    testbitmap.fill_rect( 2, 2, sq.width-4, sq.height-4, Color.new( 255, 255, 255 ) )
    for x in 0...w
      for y in 0...h
        testsprites << Sprite.new()
        testsprites[-1].bitmap = testbitmap
        testsprites[-1].x = x * sq.width
        testsprites[-1].y = y * sq.height
        sq2 = sq.clone ; sq2.width -= 1 ; sq2.height -= 1
        testcursors << TestAreaOnEvent.new(testsprites[-1], sq2)
        testcursors[-1].x = x * sq.width
        testcursors[-1].y = y * sq.height
      end  
    end  
    loop do     
      Graphics.update()
      Input.update()
      MouseEx.update()  
      for i in 0...testcursors.size
        testcursors[i].update()
        testsprites[i].update
      end
    end  
  end
end 
end
