# Themes
module Themes
end  
class << Themes  
  def init
    @themes = {}
    @themes[:modern_dark] = {
      :outline => Pallete[:gray18],
      :inline  => Pallete[:gray17],
      :base    => Pallete[:gray16],
      :light1  => Pallete[:gray13],
      :light2  => Pallete[:gray11],
      :light3  => Pallete[:gray9 ]
    }
  end
  def theme(sym)
    init unless(@themes)
    @themes[sym]
  end
  alias :[] :theme
end  
