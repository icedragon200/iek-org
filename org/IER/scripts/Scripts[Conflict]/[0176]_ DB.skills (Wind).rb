#  | DB.skills (Wind)
# // 02/24/2012
# // 02/24/2012
module Database
def self.mk_skills4() # // Wind
  skills = []
  element = :wind
#==============================================================================#
# ◙ Skill (Bolt)
#==============================================================================#   
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 1
  skill.name              = "Bolt"
  skill.icon_index        = 98
  skill.description       = "Basic Wind Magic"
  skill.features          = []
  skill.note              = ""
  skill.scope             = @scope[:one_enemy]
  skill.occasion          = @occasion[:battle] #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = @hit_type[:magical]
  skill.animation_id      = 2
  skill.stype_id          = @skill_type_id[element]
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = @damage_type[:hp_dam]
  skill.damage.element_id = element_id(element)
  skill.damage.formula    = "3 * a.mat / 2"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skill.atk_range.range = 2
  skills[skill.id] = skill 
  add_skill2groups(skill,element,:lv1)
#==============================================================================#
# ◙ Skill (Storm)
#==============================================================================#   
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 2
  skill.name              = "Storm"
  skill.icon_index        = 98
  skill.description       = "Whip up a lightning storm!"
  skill.features          = []
  skill.note              = ""
  skill.scope             = @scope[:one_enemy]
  skill.occasion          = @occasion[:battle] #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = @hit_type[:magical]
  skill.animation_id      = 2
  skill.stype_id          = @skill_type_id[element]
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = @damage_type[:hp_dam]
  skill.damage.element_id = element_id(element)
  skill.damage.formula    = "3 * a.mat / 2"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skill.atk_range.range = 3
  skills[skill.id] = skill
  add_skill2groups(skill,element,:lv2)
#==============================================================================#
# ◙ Skill (Xenite)
#==============================================================================#   
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 3
  skill.name              = "Xenite"
  skill.icon_index        = 98
  skill.description       = "Summon Xenolto, multiple bolts!"
  skill.features          = []
  skill.note              = ""
  skill.scope             = @scope[:one_enemy]
  skill.occasion          = @occasion[:battle] #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = @hit_type[:magical]
  skill.animation_id      = 2
  skill.stype_id          = @skill_type_id[element]
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = @damage_type[:hp_dam]
  skill.damage.element_id = element_id(element)
  skill.damage.formula    = "3 * a.mat / 2"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skill.atk_range.range = 4
  skills[skill.id] = skill  
  add_skill2groups(skill,element,:lv3)
#==============================================================================#
# ◙ Skill (Shoas)
#==============================================================================#   
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 4
  skill.name              = "Shoas"
  skill.icon_index        = 98
  skill.description       = "Lighting from the sky, stun my enemies"
  skill.features          = []
  skill.note              = ""
  skill.scope             = @scope[:one_enemy]
  skill.occasion          = @occasion[:battle] #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = @hit_type[:magical]
  skill.animation_id      = 2
  skill.stype_id          = @skill_type_id[element]
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = @damage_type[:hp_dam]
  skill.damage.element_id = element_id(element)
  skill.damage.formula    = "3 * a.mat / 2"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skill.atk_range.range = 3
  skills[skill.id] = skill 
  add_skill2groups(skill,element,:lv2)
#==============================================================================#
# ◙ Skill (Coilorm)
#==============================================================================#   
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 5
  skill.name              = "Coilorm"
  skill.icon_index        = 98
  skill.description       = "Lighting from the sky, stun my enemies"
  skill.features          = []
  skill.note              = ""
  skill.scope             = @scope[:one_enemy]
  skill.occasion          = @occasion[:battle] #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = @hit_type[:magical]
  skill.animation_id      = 2
  skill.stype_id          = @skill_type_id[element]
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = @damage_type[:hp_dam]
  skill.damage.element_id = element_id(element)
  skill.damage.formula    = "3 * a.mat / 2"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skill.atk_range.range = 3
  skills[skill.id] = skill 
  add_skill2groups(skill,element,:lv2)
#==============================================================================#
# ◙ Skill (Irie)
#==============================================================================#   
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 6
  skill.name              = "Irie"
  skill.icon_index        = 98
  skill.description       = "The great Irie winds carry you"
  skill.features          = []
  skill.note              = ""
  skill.scope             = @scope[:one_ally]
  skill.occasion          = @occasion[:battle] #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = @hit_type[:magical]
  skill.animation_id      = 2
  skill.stype_id          = @skill_type_id[element]
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = @damage_type[:none]
  skill.damage.element_id = 0
  skill.damage.formula    = "0"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skill.effects << MkEffect.add_buff(param_id(:agi), 5)
  skill.atk_range.range = 2
  skills[skill.id] = skill 
  add_skill2groups(skill,element,:lv1)
#==============================================================================#
# ◙ Skill (Xenolto)
#==============================================================================#   
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 7
  skill.name              = "Xenolto"
  skill.icon_index        = 98
  skill.description       = "Guardian Spirit Xenolto"
  skill.features          = []
  skill.note              = ""
  skill.scope             = @scope[:global]
  skill.occasion          = @occasion[:battle] #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = @hit_type[:magical]
  skill.animation_id      = 2
  skill.stype_id          = @skill_type_id[element]
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = @damage_type[:none]
  skill.damage.element_id = 0
  skill.damage.formula    = "0"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skills[skill.id] = skill  
  add_skill2groups(skill,element,:lv4)
#==============================================================================#
# ◙ Skill (Airia)
#==============================================================================#   
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 8
  skill.name              = "Airia"
  skill.icon_index        = 98
  skill.description       = "Guardian Spirit Airia"
  skill.features          = []
  skill.note              = ""
  skill.scope             = @scope[:user_team]
  skill.occasion          = @occasion[:battle] #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = @hit_type[:magical]
  skill.animation_id      = 2
  skill.stype_id          = @skill_type_id[element]
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = @damage_type[:none]
  skill.damage.element_id = 0
  skill.damage.formula    = "0"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skills[skill.id] = skill  
  add_skill2groups(skill,element,:lv4)
#==============================================================================#
# ◙ REMAP
#==============================================================================#   
  adjust_skills(skills,element)  
end
end
