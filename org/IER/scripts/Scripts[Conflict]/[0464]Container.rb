# Container
#==============================================================================#
# ♥ Container
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 02/06/2012
# // • Data Modified : 02/06/2012
# // • Version       : 1.0
#==============================================================================#
# ● Explanation
#   A very bare class used as a lightweight window
#   This class also includes methods from:
#     Surface
#     RectExpansion (Standard only)
#     OpenClose 
# ● Author Comments
#   >_> Hell its just a cheap ass stripped down window, that happens to 
#   be clone-able. Anyway I use Containers when making fancy sprite based menus
#   >_> They can also be used with most of the WindowAddons (Naise O:)
#   >_>
#   <_<
#   >_< WUT!?
#==============================================================================#
# ● Change Log
#     ♣ 02/06/2012 V1.0 
#
#==============================================================================#
class Container
  include Mixin::Surface
  include Mixin::RectExpansion
  include Mixin::OpenClose
  def initialize(*args,&block)
    @cube = Cube.new(0,0,0,0,0,0) # // Making use of that cube class now XD
    @ox, @oy = 0, 0
    @viewport = nil
    @active   = false
    @opacity  = 255
    @visible  = true
    @openness = 255
    @padding  = 0
    @padding_bottom = 0
    _set(*args,&block)
    init_internal()
    post_init()
  end   
  public
  def set(*args,&block)
    _set(*args,&block)
    _update_position()
    _update_size()
  end
  def move(x=self.x,y=self.y,width=self.width,height=self.height)
    @cube.x = x
    @cube.y = y
    @cube.width = width
    @cube.height = height
    _update_position()
    _update_size()
  end    
  def cube # // Returns a duplicate of the internal @cube (Cube)
    @cube.dup
  end  
  def x
    @cube.x
  end
  def y
    @cube.y
  end
  def z
    @cube.z
  end  
  def width
    @cube.width
  end  
  def height
    @cube.height
  end 
  def x=(n)
    @cube.x = n 
    _update_position()
  end  
  def y=(n)
    @cube.y = n 
    _update_position()
  end  
  def z=(n)
    @cube.z = n 
    _update_position()
  end  
  def width=(n)
    @cube.width = n 
    _update_size()
  end  
  def height=(n)
    @cube.height = n 
    _update_size()
  end  
  attr_reader :ox
  def ox=(n)  
    @ox = n
    _update_oposition()
  end  
  attr_reader :oy
  def oy=(n)  
    @oy = n
    _update_oposition()
  end  
  attr_reader :viewport
  def viewport=(n)
    @viewport = n
    #_update_viewport()
    _update_all() # // . x . Dont make it a habit to reassign Viewports
  end  
  attr_reader :active
  def active=(n)
    @active = n
    _update_active()
  end  
  attr_reader :visible
  def visible=(n)
    @visible = n
    _update_visible()
  end  
  attr_reader :opacity
  def opacity=(n)
    @opacity = n.clamp(0,255).to_i
    _update_opacity()
  end  
  attr_reader :openness
  def openness=(n)
    @openness = n.clamp(0,255).to_i
    _update_openness()
  end    
  attr_reader :padding
  def padding=(n)
    @padding = n
    _update_padding()
  end  
  attr_accessor :padding_bottom
  def show
    self.visible = true
    self
  end
  def hide
    self.visible = false
    self
  end
  def activate
    self.active = true
    self
  end
  def deactivate
    self.active = false
    self
  end
  def disposed?
    @disposed == true
  end  
  def dispose()
    raise RGSSError.new("Object Already Disposed") if disposed?
    _dispose_objs()
    @disposed = true
  end
  def update
    _update_objs()
  end
  private
  def init_internal()
  end  
  def post_init()
    _update_all()
  end  
  def _update_all()
    _update_position()
    _update_oposition()
    _update_size()
    _update_active()
    _update_visible()
    _update_viewport()
    _update_opacity()
    _update_openness()
    _update_padding()
  end  
  def _set(*args,&block)
    x,y,z,w,h = 0,0,0,0,0
    case args[0]
    when Container
      x,y,z,w,h,l = *args[0].cube.to_a
    when Rect, Polygon#, Window
      x,y,w,h = *args[0].to_rect.to_a
    when Hash  
      x,y,z,w,h = args[0].get_values(:x,:y,:z,:width,:height)
    else
      x,y,w,h = *args
    end 
    @cube.x      = x || 0
    @cube.y      = y || 0
    @cube.z      = z || 0
    @cube.width  = w || 0
    @cube.height = h || 0
  end  
  def _objs()
    []
  end 
  def _obj_enabled?(n)
    true
  end  
  def _dispose_objs()
    _each_obj(){|o|o.dispose unless o.disposed? if o}
  end  
  def _update_objs()
    _each_obj(){|o|o.update}
  end  
  def _each_obj()
    _objs.each { |o| yield o }
  end 
  def _update_position()
    begin
      _each_obj { |o| o.x, o.y, o.z = self.x, self.y, self.z } if _obj_enabled?(:position) 
    rescue => ex
      puts self
      raise ex
    end  
    # // You add stuff :D
  end 
  def _update_oposition()
    _each_obj { |o| o.ox, o.oy = self.ox, self.oy } if _obj_enabled?(:oposition)
    # // You add stuff :D
  end  
  def _update_size()
    _each_obj { |o| o.width, o.height = self.width, self.height } if _obj_enabled?(:size)
    # // You add stuff :D
  end  
  def _update_active()
    _each_obj { |o| o.active = self.active if o.respond_to?(:active=) } if _obj_enabled?(:active)
    # // You add stuff :D
  end  
  def _update_visible()
    _each_obj { |o| o.visible = self.visible } if _obj_enabled?(:visible)
    # // You add stuff :D
  end  
  def _update_opacity()
    _each_obj { |o| o.opacity = self.opacity } if _obj_enabled?(:opacity)
    # // You add stuff :D
  end  
  def _update_viewport()
    _each_obj { |o| o.viewport = self.viewport } if _obj_enabled?(:viewport)
    # // You add stuff :D
  end
  def _update_openness()
    # // You add stuff :D
  end
  def _update_padding()
    # // You add stuff :D
  end  
  def _redraw() # // Redraw everything in the container . x . super stuff
  end  
  def _redraw!() # // Redraw && Update All
    _update_all() # // Initialial Update
    _redraw()
    _update_all() # // Final Update
  end
end
#==============================================================================#
# ♥ Container::Base
#==============================================================================#
class Container::Base < Container
end  
#==============================================================================#
# // >_> Your wondering why the hell that(^Container::Base) is empty aren't you?
# // <_< Basically you use the Container::Base instead of the main class for
# // the WindowAddons.
# // Why?
# // >_> Its an inheritance thing...
# // If you should take a look in the WindowAddons script 
# // You'll notice super in the x=, y=, z= etc..etc methods in the Base module
# // >_>; <_<; >_<"
# // Anyway, back to the inheritance
# // . x . WindowAddons works like this:
# // My_Window < (Addons go here) < Window::Base < WindowAddon::Base < Window
# // 'include' places the module before the current class in the hierachy
# // Mentioned before; Window has x=, y=, z=, etc...etc..
# // So if you placed the AddonBase BEFORE Window... . x . bad things would happen
# // . x . You couldn't call (x=, y=, z=)'s super since they didn't exist then.
# // super calls the parent's method >.> So when a Addon calls say x='s super
# // If there was a addon before it, it calls that one, and it chains all the way
# // up till it reaches the Window's x= method. o3o Here is an example
# // Window::Cheese (Has Header, ScrollBar, WinButtons installed)
# // .x. You do something like (window.x = 3)
# // What happens?
# //   x=(n) 
# //     (Window::Cheese)x=(n)>> To the next
# //     (WinButtons)   x=(n) >> again
# //     (ScrollBar)    x=(n) >> and again...
# //     (Header)       x=(n) >> yet again...
# //     (WindowBase)   x=(n) >> again...... 
# //     (WindowAddons::Base)x=(n) >> again...
# //     (Window)       x=(n)  And your journey ends here
# // O_O Yup shit like that happens, it goes back.... WAY BACK 
# // So anyway...
# // Just remember, you need a base class to work with the WindowAddons
# // >_>
# // <_<
# // >_<
# // >.> And so there is teh reason for the empty class
# // Oh one more thing . x.  If you overwrite the methods in the child class
# // And you dont call super, the entire super line gets broken .x.
# // So x= would be a useless thingy . x .
# // Lesson: Dont overwrite important methods.
# // <.<; >.>; >,<;.. I wrote too much....
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
