# Window::Confirm
# // 01/25/2012
# // 01/25/2012
class Window::Confirm < Window::Command
  class AddonBin_WC < Window::QuickText::AddonBin_WQT
    def header_cube()
      (c=super).xset(self.x+(self.width-c.width)/2)
    end  
  end  
  def addon_bin
    AddonBin_WC
  end 
  def header_text()
    @header_text
  end 
  def initialize( x, y, header_text=nil )
    @header_text = header_text
    super( x, y )
  end  
  def make_command_list
    add_command( "Yes", :ok    , true )
    add_command( "No" , :cancel, true )
  end
  def col_max
    2
  end  
  def alignment()
    1
  end 
  def spacing 
    2
  end  
  def update_padding_bottom
    #surplus = (height - standard_padding * 2) % item_height
    #self.padding_bottom = padding + surplus
  end
  def draw_item(index)
    open_artist() do |p|
      draw_box(*p.item_rect(index).contract(2).to_a)
      change_color(normal_color, p.command_enabled?(index))
      draw_text(p.item_rect_for_text(index), p.command_name(index), p.alignment)
    end  
  end 
end  
