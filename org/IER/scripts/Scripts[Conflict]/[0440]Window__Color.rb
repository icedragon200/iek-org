# Window::Color
# // 02/01/2012
# // 02/01/2012
class Window::ColorSelect < Window::Selectable
  def initialize(x=0,y=0)
    super(x,y,window_width,window_height)
    @sliders = []
    @colors = Array.new(4) { Color.new(0,0,0,255) }
    @color_assign = [
      [1,0,0,1],
      [0,1,0,1],
      [0,0,1,1],
      [0,0,0,1]
    ]
    @sprite_color = Sprite.new()
    @sprite_color.bitmap = Bitmap.new(196, 32)
    @sprite_color.bitmap.fill_rect(@sprite_color.bitmap.rect.contract(2), Color::Black)
    @sprite_color.bitmap.blur
    @sprite_tone = Sprite.new()
    @sprite_tone.bitmap = Bitmap.new(196, 32)
    @sprite_tone.bitmap.fill_rect(@sprite_tone.bitmap.rect.contract(2), Color::Black)
    @sprite_tone.bitmap.blur
    refresh()
    activate()
    select(0)
  end
  def x=(x)
    super(x)
    @sprite_color.x = self.x + standard_padding + 96
    @sprite_tone.x = self.x + standard_padding + 96
    @sliders.each_with_index { |s,i|
      s.x = item_rect(i+3).x + self.x + standard_padding + 96
    }
  end  
  def y=(y)
    super(y)
    @sprite_color.y = self.y + standard_padding + 32
    @sprite_tone.y = self.y + standard_padding + 64
    @sliders.each_with_index { |s,i|
      s.y = item_rect(i+3).y + self.y + standard_padding + 4
    }
  end
  def z=(z)
    super(z)
    @sprite_color.z = self.z + 1
    @sprite_tone.z = self.z + 1
    @sliders.each { |s| s.z = self.z + 1 }
  end 
  def viewport=(viewport)
    super(viewport)
    @sprite_color.viewport = self.viewport
    @sprite_tone.viewport = self.viewport
    @sliders.each { |s| s.viewport = self.viewport }
  end  
  def get_color
    Color.new(@colors[0].red, @colors[1].green, @colors[2].blue, @colors[3].alpha)
  end  
  def window_width
    96 + 196 + standard_padding * 2
  end
  def window_height
    (32*item_max) + standard_padding * 2
  end  
  def item_max
    7
  end  
  def item_height
    32
  end  
  def update_padding_bottom
  end  
  def draw_item(index)
    rect = item_rect(index)
    trect = item_rect_for_text(index)
    trect.width = 96
    drect = rect.clone
    drect.x += trect.width
    drect.width -= trect.width
    case index
    when 0
      @artist.draw_text(trect, "Color",1)
      contents.fill_rect(drect.contract(2), get_color)
    when 1  
      @artist.draw_text(trect, "Sprite Color",1)
      @sprite_color.color.set(*get_color.to_a)
    when 2  
      @artist.draw_text(trect, "Sprite Tone",1)
      @sprite_tone.tone.set(*get_color.to_a)
    when 3
      @artist.draw_text(trect, "Red",1)
    when 4
      @artist.draw_text(trect, "Green",1)
    when 5
      @artist.draw_text(trect, "Blue",1)
    when 6
      @artist.draw_text(trect, "Alpha",1)  
    end  
    if index > 2
      @sliders[index-3].dispose() if @sliders[index-3]
      @sliders[index-3] = GUIExt::Slider.new( 
        self.viewport, :horz, 196, 
        rect.x+standard_padding, rect.y+standard_padding, self.z + 1 )
      @sliders[index-3].enable_input()  
    end    
  end    
  def update()
    super() 
    @sliders.each_with_index { |s,i|
      (i == @index-3 ? s.activate : s.deactivate) #if @index > 2
      s.update
      lsc = get_color
      @colors[i].set( *@color_assign[i].collect{|i|255*i*s.rate} )
      3.times{|i|redraw_item(i)} if lsc.to_a != get_color.to_a
      s.force_bar_color(@colors[i])
    }
  end  
end  
