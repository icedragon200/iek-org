# InputEx
# // 02/07/2012
# // 02/07/2012
module InputEx  
  # // Win32API functions // Depreceated >_____> just great...
  #GKAS = Win32API.new( "user32", "GetAsyncKeyState", "i", "i" )
  #GKS  = Win32API.new( "user32", "GetKeyState", "i", "i" )
  #KBDE = Win32API.new( "user32", 'keybd_event', "iill", "i" )
  #SNDI = Win32API.new( "user32", "SendInput", "ipi", "i" ) 
  #GCP  = Win32API.new( "user32", "GetCursorPos"    , "p" , "i" )
  #SCP  = Win32API.new( "user32", "SetCursorPos"    , "ll", "i" )
  #STC  = Win32API.new( "user32", "ScreenToClient"  , "lp", "i" )
  #CTS  = Win32API.new( "user32", "ClientToScreen"  , "lp", "i" ) 
  #GCR  = Win32API.new( "user32", "GetClientRect"   , "lp", "i" )
  #FW   = Win32API.new( "user32", "FindWindowA"     , "pp", "l" )
  # // DL... >,> Its torture I tellz you, (its a learning experience) >_>... OTL
  include DL # // O_O DAMN IT ALL, DL::TYPE_YOUR_MOM
  # // Open yer dll  
  class InputEx_Error < DLError
  end  
  def self.dll_user32()
    @dll_user32 ||= DL.dlopen("user32")
  end  
  def InputEx.p2l(str)
    (n,=[str].pack("p").unpack("l!*"));n
  end  
  def InputEx.do_p2l(n)
    n.is_a?(Fixnum) ? n : p2l(n)
  end 
  @@funcs = {} 
  def self.mk_func(sym,func,export=TYPE_LONG,t=:stdcall)
    sym=sym.to_s
    module_eval(%Q(
      def InputEx.#{func}(*args,&block)
        args.collect!{|n|InputEx.do_p2l(n)}
        @@funcs[:#{sym}]||=CFunc.new(dll_user32["#{func}"],#{export},"#{func}",:#{t})
        ret, = @@funcs[:#{sym}].call(args,&block)
        return ret || 0
      rescue => ex
        raise InputEx_Error, ex.message, ex.backtrace
      end)  
    )
  end
  # // Keyboard <_<
  mk_func :gkas, "GetAsyncKeyState"
  mk_func :gks , "GetKeyState"
  mk_func :kbde, "keybd_event"
  mk_func :sndi, "SendInput"
  # // Mouse >_>
  mk_func :gcp , "GetCursorPos"
  mk_func :scp , "SetCursorPos"
  # // Remeber to convert the strings(pointer) to a long using InputEx.p2l . x .
  mk_func :stc , "ScreenToClient"
  mk_func :cts , "ClientToScreen"
  mk_func :gcr , "GetClientRect"
  mk_func :fw  , "FindWindowA"  
  mk_func :shc , "ShowCursor"
end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
