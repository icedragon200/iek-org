# [IEI]KeyNValue
Devi.add("KeyNValue").set(
  :enabled => true,
  :name          => 'IEI::KeyNValue',
  :author        => 'IceDragon',
  :date_created  => Devi::Date.new(4,28,2012),
  :date_modified => Devi::Date.new(4,30,2012),
  :version       => '0.10'
)
class RPG::BaseItem
  # // Key and Value
  def knv
    unless(@knv)
      str = "note[_ ]?knv"
      @knv = get_note_folders(*IEI::Core.mk_notefolder_tags(str)).inject({}) do |r,a|
        a.each{|s|mtch = s.match(/(.*)=(.*)/i);r[s[1].downcase]=s[2] if(mtch)};r
      end
    end  
    @knv
  end
  attr_writer :knv
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
