# Sprite::Minimap
#==============================================================================#
# ♥ Sprite::Minimap
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/26/2011
# // • Data Modified : 12/27/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/26/2011 V1.0 
#
#==============================================================================#  
class Sprite::Minimap < Sprite
  attr_reader :data
  def initialize( viewport=nil )
    super( viewport )
  end 
  def dispose()
    dispose_bitmap()
    super()
  end 
  def data=( new_data )
    if @data != new_data
      @data = new_data
      refresh()
    end
  end  
  def refresh()
    dispose_bitmap()
    ws = (@data.xsize * 16 + @data.ysize * 16) / 2
    hs = (@data.xsize * 8 + @data.ysize * 8) / 2
    self.bitmap = Bitmap.new( ws, hs+8 )
    #self.bitmap.fill_rect( self.bitmap.rect, Pallete[:brown2] )
    tbmp = Cache.system( "Minimap_Tiles" )
    for ry in 0...@data.ysize
      for rx in 0...@data.xsize
        x, y = @data.xsize - 1 - rx, ry
        xo, yo = *Isometric.calc_screen_xy( x, y, 16, 16, self.bitmap.width, self.bitmap.height )
        index = @data[x, y]
        yo += 3 if @data[x, y] == 0
        #yo -= 16
        self.bitmap.blt( xo, yo, tbmp, 
          Rect.new( index % 8 * 16, index / 8 * 16, 16, 16 )
        )
      end
    end  
  end  
  def scale=( new_scale )
    if @scale != new_scale
      @scale = new_scale
      refresh_scale()
    end
  end 
  def refresh_scale()
    sc = @scale / 1.0
    self.zoom_x = self.zoom_y = sc
  end
end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
