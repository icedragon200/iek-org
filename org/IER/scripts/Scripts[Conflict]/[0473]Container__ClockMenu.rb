# Container::ClockMenu
# // 02/14/2012
# // 02/14/2012 
class Container::ClockMenu < Container::ContextMenu
  def make_command_list()
    add_command("Color", :color)
    add_command("Change", :change)
  end   
end  
  
