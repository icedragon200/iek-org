# Scene::Equip
# // 02/29/2012
class Window::EquipSlot < Window::Equip
  def initialize(*args,&block)
    super(*args,&block)
    select(-1)
  end  
end  
class Window::EquipStatus < Window::RogueStatus
  def standard_draw_mode()
    0
  end   
  #--------------------------------------------------------------------------
  # ● 装備変更後の一時アクター設定
  #--------------------------------------------------------------------------
  def set_temp_actor(temp_actor)
    return if @temp_actor == temp_actor
    @temp_actor = temp_actor
    refresh
  end  
end  
#==============================================================================
# ■ Scene::Equip
#------------------------------------------------------------------------------
# 　装備画面の処理を行うクラスです。
#==============================================================================

class Scene::Equip < Scene::MenuBase
  #--------------------------------------------------------------------------
  # ● 開始処理
  #--------------------------------------------------------------------------
  def start
    super
    create_help_window
    create_status_window
    create_command_window
    create_slot_window
    create_item_window
    #@big_help_window = Window::BigHelp.new(@help_window.x,@item_window.vy2,@help_window.width,@canvas.height-@item_window.vy2)
    #@item_window.help_window = @big_help_window
    auto_add_windows()
  end
  def create_help_window
    rect = Rect.new(0,0,0,0) # // . x . To gain access to surface stuff
    @help_window = Window::SkillHelp.new(@canvas.x,@canvas.y,@canvas.width,rect.adjust_h4window(96,8))
    @help_window.viewport = @viewport
    @help_window.actor = @actor
  end
  #--------------------------------------------------------------------------
  # ● ステータスウィンドウの作成
  #--------------------------------------------------------------------------
  def create_status_window
    @status_window = Window::EquipStatus.new(@help_window.x, @help_window.vy2, @help_window.width)
    @status_window.viewport = @viewport
    @status_window.actor = @actor
  end
  #--------------------------------------------------------------------------
  # ● コマンドウィンドウの作成
  #--------------------------------------------------------------------------
  def create_command_window
    wx = @status_window.x
    wy = @status_window.vy2
    ww = @status_window.width
    @command_window = Window::EquipCommand.new(wx, wy, ww)
    @command_window.viewport = @viewport
    @command_window.help_window = @help_window
    @command_window.set_handler(:equip,    method(:command_equip))
    @command_window.set_handler(:optimize, method(:command_optimize))
    @command_window.set_handler(:clear,    method(:command_clear))
    @command_window.set_handler(:cancel,   method(:return_scene))
    @command_window.set_handler(:pagedown, method(:next_actor))
    @command_window.set_handler(:pageup,   method(:prev_actor))
  end
  #--------------------------------------------------------------------------
  # ● スロットウィンドウの作成
  #--------------------------------------------------------------------------
  def create_slot_window
    wx = @help_window.x
    wy = @command_window.vy2
    ww = nil #@help_window.width#Graphics.width - @status_window.width
    @slot_window = Window::EquipSlot.new(wx, wy, ww)
    @slot_window.viewport = @viewport
    @slot_window.help_window = @help_window
    @slot_window.status_window = @status_window
    @slot_window.actor = @actor
    @slot_window.set_handler(:ok,       method(:on_slot_ok))
    @slot_window.set_handler(:cancel,   method(:on_slot_cancel))
  end
  #--------------------------------------------------------------------------
  # ● アイテムウィンドウの作成
  #--------------------------------------------------------------------------
  def create_item_window
    wx = @slot_window.vx2
    wy = @slot_window.y
    ww = @canvas.width - @slot_window.width
    wh = @slot_window.height
    @item_window = Window::EquipItem.new(wx,wy,ww,wh)#Window::EquipItem.new(wx, wy, ww, wh)
    @item_window.viewport = @viewport
    @item_window.help_window = @help_window
    @item_window.status_window = @status_window
    @item_window.actor = @actor
    @item_window.set_handler(:ok,     method(:on_item_ok))
    @item_window.set_handler(:cancel, method(:on_item_cancel))
    @slot_window.item_window = @item_window
  end
  #--------------------------------------------------------------------------
  # ● コマンド［装備変更］
  #--------------------------------------------------------------------------
  def command_equip
    @slot_window.activate
    @slot_window.select(0)
  end
  #--------------------------------------------------------------------------
  # ● コマンド［最強装備］
  #--------------------------------------------------------------------------
  def command_optimize
    Sound.play_equip
    @actor.optimize_equipments
    @status_window.refresh
    @slot_window.refresh
    @command_window.activate
  end
  #--------------------------------------------------------------------------
  # ● コマンド［全て外す］
  #--------------------------------------------------------------------------
  def command_clear
    Sound.play_equip
    @actor.clear_equipments
    @status_window.refresh
    @slot_window.refresh
    @command_window.activate
  end
  #--------------------------------------------------------------------------
  # ● スロット［決定］
  #--------------------------------------------------------------------------
  def on_slot_ok
    @item_window.activate
    @item_window.select(0)
  end
  #--------------------------------------------------------------------------
  # ● スロット［キャンセル］
  #--------------------------------------------------------------------------
  def on_slot_cancel
    @slot_window.unselect
    @command_window.activate
  end
  #--------------------------------------------------------------------------
  # ● アイテム［決定］
  #--------------------------------------------------------------------------
  def on_item_ok
    Sound.play_equip
    @actor.change_equip(@slot_window.index, @item_window.item)
    @slot_window.activate
    @slot_window.refresh
    @item_window.unselect
    @item_window.refresh
    @status_window.refresh()
  end
  #--------------------------------------------------------------------------
  # ● アイテム［キャンセル］
  #--------------------------------------------------------------------------
  def on_item_cancel
    @slot_window.activate
    @item_window.unselect
  end
  #--------------------------------------------------------------------------
  # ● アクターの切り替え
  #--------------------------------------------------------------------------
  def on_actor_change
    @help_window.actor = @actor
    @status_window.actor = @actor
    @slot_window.actor = @actor
    @item_window.actor = @actor
    @command_window.activate
  end
end
