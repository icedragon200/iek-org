#  | DB.armors (Body)
# // 02/29/2012
# // 02/29/2012
module Database
def self.mk_armors5()
  armors = []
  armor_sym = :body
  equip_sym = :body
#==============================================================================#
# ◙ Armor (Body)(Simple)
#==============================================================================# 
  armor = Armor.new()
  armor.initialize_add()
  armor.id           = 1
  armor.name         = "Cotton"
  armor.icon_index   = 0
  armor.description  = 'Simple shirt'
  armor.features     = []
  armor.note         = ""
  armor.etype_id     = 0 
  armor.params       = [0,0,0,3,0,1,0,0]
  armor.atype_id     = 0
  armor.features << MkFeature.def_r(1.06)
  armor.features << MkFeature.mdf_r(1.03)
  armors[armor.id] = armor  
#==============================================================================#
# ◙ Armor (Body)(Simple)
#==============================================================================# 
  armor = Armor.new()
  armor.initialize_add()
  armor.id           = 2
  armor.name         = "Quilted"
  armor.icon_index   = 0
  armor.description  = 'Its a bit puffy, but what the heck'
  armor.features     = []
  armor.note         = ""
  armor.etype_id     = 0 
  armor.params       = [0,0,0,4,0,1,0,0]
  armor.atype_id     = 0
  armor.features << MkFeature.def_r(1.07)
  armor.features << MkFeature.mdf_r(1.04)
  armors[armor.id] = armor  
#==============================================================================#
# ◙ REMAP
#==============================================================================# 
  adjust_armors(armors,armor_sym,equip_sym)  
end
end
