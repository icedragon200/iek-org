# Window::UpdateDummy
# // 01/20/2012
# // 01/20/2012
class Window::UpdateDummy < Window::Base
  attr_accessor :update_function
  def initialize(x, y, w, h, update_function)
    super(x,y,w,h)
    @update_function = update_function
    @update_function.call(self) if @update_function
  end  
  def update()
    super()
    @update_function.call(self) if @update_function
  end  
end  
