# RPG::Class (Addon)
class RPG::Class < RPG::BaseItem
  def rebuild_parameter_table( maxlevel=100 ) 
    @params = Table.new( 8, maxlevel )
    if block_given?()
      (1..maxlevel).each do |i|
        for j in 0...8
          # Parameter, Level = block { |parameter, level| integer } 
          @params[j,i] = yield j, i, self
        end  
      end
    else  
      (1..maxlevel).each do |i|
        @params[0,i] = 400+i*50
        @params[1,i] = 80+i*10
        (2..5).each {|j| @params[j,i] = 15+i*5/4 }
        (6..7).each {|j| @params[j,i] = 30+i*5/2 }
      end
    end  
  end  
end  
