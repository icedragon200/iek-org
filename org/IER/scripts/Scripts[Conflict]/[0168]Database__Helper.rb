# Database::Helper
=begin
# // 02/29/2012
# // 02/29/2012
# // . x . So what is the DB::Helper ?
# // . x . Its a collection of methods used to read objects from the database
# // o3o Say you want all the parameter related features for n item
# // It can retrieve all of them for you.
# // >.> Well hell you could do it yourself too
# // Anyway I'll add more stuff to it later
module Database::Helper
  include Mixin::FeatureConstants
  FORMAT_PARAM_PP = "\\k[2]+%03d\\k[0]%" # // Param Plus 
  FORMAT_PARAM_PM = "\\k[1]-%03d\\k[0]%" # // Param Minus
  FORMAT_PARAM_PC = "\\k[3]+%03d\\k[0]%" # // Param Constant
  def self.get_param_r_a(item)
    item.features.select{|f|f.code==FEATURE_PARAM}
  end 
  def self.r2i(n) # // Converts a Rate to a Percent . x .
    (n * 100).to_i
  end  
  def self.pr2i(n) # // Converts a Param Rate to a Percent . x .
    r2i(n-1.0)
  end  
  def self.param_r2i(feature)
    pr2i(feature.value)
  end  
  def self.r2s(n) # // Rate to Formatted Param String . x .
    i2ps(pr2i(n))
  end 
  def self.i2ps(i) # // Integer to Param String . x .
    format(((i>0)?FORMAT_PARAM_PP : ((i==0)?FORMAT_PARAM_PC : FORMAT_PARAM_PM)),i.abs)
  end  
  def self.param_r2s(feature)
    i2ps(param_r2i(feature))
  end  
  def self.item2param_texts(item)
    rates = [1.0]*8
    get_param_r_a(item).each do |pf|
      rates[pf.data_id] *= pf.value
    end 
    rates.collect{|f|[r2s(f),f]} # // [[Param String, Rate]...]
  end  
end   
=end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
