# Main
# // 02/18/2012
# // 02/18/2012
module Main  
  def self.pre_update()
  end
  def self.post_update()
  end  
  def self.update_basic()
    Graphics.update
    Input.update
  end  
  def self.update()
    pre_update()
    update_basic()
    post_update()
  end  
end
