# Sprite::Icon
#==============================================================================#
# ♥ Sprite::Icon
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/12/2011
# // • Data Modified : 12/12/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/12/2011 V1.0 
#
#==============================================================================# 
class Sprite::Icon < Sprite::Base
  attr_reader :icon_index
  def initialize( viewport=nil, icon_index=0 )
    super( viewport )
    @icon_index = icon_index
    self.bitmap = iconset
    update_icon()
  end  
  def iconset
    Cache.system( "Iconset" )
  end  
  def icon_index=( icon_index )
    @icon_index = icon_index
    self.bitmap = iconset
    update_icon()
  end  
  def update_icon()
    self.src_rect.set( @icon_index % 16 * 24, @icon_index / 16 * 24, 24, 24 )
  end  
end 
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
