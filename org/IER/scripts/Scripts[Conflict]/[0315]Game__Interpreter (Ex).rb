# Game::Interpreter (Ex)
# // 02/24/2012
class Game::Interpreter
  def pop_speechbub(text,id=@event_id)
    m  = get_character(id)
    ps = SceneManager.scene.pop_spriteset
    ps.add_speechbub( m.screen_x, m.screen_y, text )
    Fiber.yield while ps.busy?
  end  
end  
