# Scene::Rogue (Create/Dispose)
# // 03/05/2012
# // 03/05/2012
class Scene::Rogue < Scene::Base
  def create_all_windows()
    create_message_window()
    create_command_window()
    #create_item_window()  # // Anti Lag . x . Created only when needed
    #create_skill_window() # // Anti Lag . x . Created only when needed
    create_status_window()
    #create_info_window()  # // Unused 
    create_log_window()
    create_ginfo_window() # // Game Info Window
    create_rscommand_window()
  end    
  # // Create
  def create_message_window()
    @message_window = Window::Message.new()
    add_window(@message_window)
  end
  def create_command_window()
    @command_window = Wheel::CommandAction.new( @viewport )
    @command_window.set_handler( :attack , method( :command_attack ) )
    @command_window.set_handler( :guard  , method( :command_guard ) )
    @command_window.set_handler( :skip   , method( :command_skip ) )
    @command_window.set_handler( :nudge  , method( :command_nudge ) )
    @command_window.set_handler( :tame   , method( :command_tame ) )
  
    @command_window.set_handler( :skill  , method( :command_skill ) )
    @command_window.set_handler( :item   , method( :command_item ) )
    @command_window.set_handler( :status , method( :command_status ) )
    @command_window.set_handler( :equip  , method( :command_equip ) )
    @command_window.set_handler( :hotkeys, method( :command_hotkeys ) )
    
    @command_window.set_handler( :list   , method( :command_list ) )
    @command_window.set_handler( :options, method( :command_options ) )
    @command_window.set_handler( :save   , method( :command_save ) ) 
    @command_window.set_handler( :minimap, method( :command_minimap ) ) 
    
    @command_window.set_handler( :cancel , method( :hide_command_window ) )
    add_window(@command_window)
  end 
  def create_help_window(rect=Rect.new(0,0,196,40),big=false)
    refrect        = rect.to_rect # // . x . Make a dup
    if big
      refrect.height = 104
    else
      refrect.height = 40
    end  
    @help_window         = (big ? Window::BigHelp : Window::Help2).new( *refrect.to_a )
    @help_window.vy2     = @help_window.y
    @help_window.battler = @status_window.battler
    @help_window.set_to_close()
    add_window(@help_window)
  end  
  def create_item_window()
    @item_window = Window::RogueInventory.new( 0, 40+16 )
    @item_window.set_handler( :ok,     method(:on_item_ok) )
    @item_window.set_handler( :cancel, method(:on_item_cancel) )
    @item_window.salign(1,-1)
    @item_window.help_window = @help_window
    add_window(@item_window)
  end  
  def create_item_command()
    @item_command = Window::RogueItemCommand.new( @item_window.x, @item_window.vheight+16 )
    @item_command.item_window = @item_window
    @item_command.set_handler( :use   , method(:on_item_command_use) )
    @item_command.set_handler( :throw , method(:on_item_command_throw) )
    @item_command.set_handler( :drop  , method(:on_item_command_drop) )
    @item_command.set_handler( :equip , method(:on_item_command_equip) )
    @item_command.set_handler( :set   , method(:on_item_command_set) )
    @item_command.set_handler( :cancel, method(:on_item_command_cancel) )
    @item_command.salign(1,-1)
    add_window(@item_command)
  end  
  def create_skill_window()
    @skill_window = Window::RogueSkillList.new( 0, 40+16 )
    @skill_window.set_handler( :ok,     method(:on_skill_ok) )
    @skill_window.set_handler( :cancel, method(:on_skill_cancel) )
    @skill_window.salign(1,-1)
    @skill_window.help_window = @help_window
    add_window(@skill_window)
  end  
  def create_skill_command()
    @skill_command = Window::RogueSkillCommand.new( @skill_window.x, @skill_window.vy2+16 )
    @skill_command.item_window = @skill_window
    @skill_command.set_handler( :use   , method(:on_skill_command_use) )
    @skill_command.set_handler( :set   , method(:on_skill_command_set) )
    @skill_command.set_handler( :cancel, method(:on_skill_command_cancel) )
    add_window(@skill_command)
  end  
  def create_status_window()
    @status_window = Window::RogueStatus.new( 0, 0 )
    @status_window.battler = nil
    @status_window.y = Graphics.height - @status_window.height
    @status_window.set_handler( :cancel, method( :on_status_cancel ) )
    add_window(@status_window)
  end    
  def create_options_window()
    @options_window = Window::Options.new()
    @options_window.set_handler( :cancel, method( :on_options_cancel ) )
    @options_window.help_window = @help_window
    @options_window.salign(1,1)
    add_window(@options_window)
  end  
  def create_equip_window()
    @equip_window = Window::RogueEquip.new( 0, 120 )
    @equip_window.status_window = @status_window
    @equip_window.set_handler( :ok    , method( :on_equip_ok ) )
    @equip_window.set_handler( :cancel, method( :on_equip_cancel ) )
    @equip_window.help_window = @help_window
    add_window(@equip_window)
  end   
  def create_equip_command()
    @equip_command = Window::RogueEquipCommand.new( 0, 0 )
    @equip_command.set_handler(:equip   , method(:on_equip_command_equip))
    @equip_command.set_handler(:unequip , method(:on_equip_command_unequip))
    @equip_command.set_handler(:optimize, method(:on_equip_command_optimize))
    @equip_command.set_handler(:clear   , method(:on_equip_command_clear))
    @equip_command.set_handler(:cancel  , method(:on_equip_command_cancel))
    #@equip_command.equip_window = @equip_window
    #@equip_command.item_window  = @item_window
    @equip_command.help_window = @help_window
    add_window(@equip_command)
  end
  def create_eitem_window()
    wx = @equip_window.vx2
    wy = @equip_window.y
    ww = @equip_window.width
    wh = @equip_window.height
    @eitem_window = Window::EquipItem.new(wx,wy,ww,wh)
    @eitem_window.help_window   = @help_window
    @eitem_window.status_window = @status_window
    @eitem_window.actor = @status_window.battler
    @eitem_window.set_handler(:ok,     method(:on_eitem_ok))
    @eitem_window.set_handler(:cancel, method(:on_eitem_cancel))
    @equip_window.item_window = @item_window
    add_window(@eitem_window)
  end  
  def create_hotkey_window
    @hotkey_window = Window::HotKeys.new()
    @hotkey_window.set_handler(:cancel,method(:on_hotkey_cancel))
    @hotkey_window.set_handler(:return_ok,method(:on_hotkey_return_ok))
    add_window(@hotkey_window)
  end 
  def create_hotkey_command()
    @hotkey_command = Window::RogueHotkeyCommand.new(@hotkey_window.x,@hotkey_window.vy2+16)
    @hotkey_command.set_handler(:change, method(:on_hotkey_command_change))
    @hotkey_command.set_handler(:remove, method(:on_hotkey_command_remove))
    @hotkey_command.set_handler(:cancel, method(:on_hotkey_command_cancel))
    @hotkey_command.salign(1,-1)
    add_window(@hotkey_command)
  end  
  def create_info_window
    @info_window = Window::CursorInfo.new( 0, 0, 96, 32 )
    @info_window.openness = 0
    add_window(@info_window)
  end  
  def create_log_window
    @log_window = Window::RogueLog.new()
    @log_window.method_wait = method(:wait)
    @log_window.method_wait_for_effect = method(:wait_for_effect)
    add_window(@log_window)
  end 
  def create_ginfo_window
    @ginfo_window = Window::RogueGameInfo.new( 0, 0, Graphics.playwidth, 64 )
    @ginfo_window.update_handle = proc {|w|w.vy2=@spriteset.viewport_rect.y}
    add_window(@ginfo_window)
  end  
  def create_rscommand_window()
    @rscommand_window = Container::RogueStatusCommand.new(@status_window)
    @rscommand_window.set_handler(:status , method(:rscommand_status))
    @rscommand_window.set_handler(:control, method(:rscommand_control))
    @rscommand_window.set_handler(:cancel , method(:on_rscommand_cancel))
    @rscommand_window.height = 0
    @rscommand_window.update()
    add_window(@rscommand_window)
  end
  # // Dispose
  # // Refer to Scene::Base (Ex)  
  mk_dispose_window :help_window 
  mk_dispose_window :skill_window , :skill_command
  mk_dispose_window :item_window  , :item_command
  mk_dispose_window :equip_window , :equip_command, :eitem_window
  mk_dispose_window :hotkey_window, :hotkey_command
  mk_dispose_window :status_window
  mk_dispose_window :options_window
  mk_dispose_window :log_window
  mk_dispose_window :rscommand_window
  mk_dispose_window :info_window

end  
