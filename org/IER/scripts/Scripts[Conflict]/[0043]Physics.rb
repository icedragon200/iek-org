# Physics
# // 01/28/2012
# // 01/28/2012
module Physics
  GRAVITY = 0.98
  module_function()
  def speed( distance, time )
    distance / time
  end  
  def acceleration( speed, time )
    speed / time
  end  
  def weight( mass, gravity )
    mass * gravity
  end  
  def acceleration_by_force( force, mass )
    force / mass
  end  
  def acceleration_by_gravity( mass )
    GRAVITY / mass
  end  
  def work( force, distance )
    force * distance
  end  
  def power( work, time )
    work / time
  end  
  def kinetic_energy( mass, speed )
    0.5 * mass / (speed ** 2)
  end  
  def potential_energy( mass, force, distance )
    mass * force * distance
  end  
  def p_gravity_energy( mass, distance )
    mass * GRAVITY * distance
  end  
  def accel( speed, time )
    acceleration( speed, time )
  end  
  def accel_d2_force( force, mass )
    acceleration_by_force( force, mass )
  end  
  def accel_d2_grav( mass )
    acceleration_by_gravity( mass )
  end  
end  
class Vector
  def self.dist_from(p1,p2)
    (p2-p1)
  end  
  def self.xy_dist_from(x1,y1,x2,y2)
    return dist_from(x1,x2), dist_from(y1,y2)
  end  
end
