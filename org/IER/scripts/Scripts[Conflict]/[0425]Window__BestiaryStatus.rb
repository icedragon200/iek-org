# Window::BestiaryStatus
#==============================================================================#
# ♥ Window::BestiaryStatus
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 01/08/2012
# // • Data Modified : 01/08/2012
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 01/08/2012 V1.0 
#==============================================================================#
class Bestiary_Character < Game::RogueCharacter2
  attr_accessor :sx, :sy
  def initialize()
    @sx, @sy = 0, 0
    super()
  end
  def screen_x
    return @sx + 16
  end
  def screen_y
    return @sy + 32
  end
  def screen_z
    return 100
  end  
end  
class Window::BestiaryStatus < Window::BookStatus
  def init_members()
    super()
    @character   = Bestiary_Character.new
    @character.instance_variable_set( "@step_anime", true )
    @character.instance_variable_set( "@move_speed", 5 )
    @sprite      = Sprite::RogueCharacter.new( nil, @character )
    @directions  = [2, 6, 8, 4]
    @dir_index   = 0
    @dir_counter = 0
    @enemies     = []
  end  
  def dispose
    @sprite.dispose 
    super()
  end
  def viewport=(v)
    super(v)
    @sprite.viewport = v
  end  
  def on_set_item( old_enemy, new_enemy )
    @enemies[new_enemy.id] ||= Game::RogueEnemy.new( new_enemy.id )
  end  
  def refresh()
    contents.clear()
    bmp = draw_book_border( 0, 28 )
    if @item
      genemy = @enemies[@item.id]
      dy = 0
      draw_block1( 0, 0 )
      dy += 24
      @character.set_graphic( genemy.character_name, genemy.character_index, "", 0 )
      contents.font.set_style( :simple_black )
      draw_text( 7, dy+7, bmp.width-14, 16, genemy.name, 1 )
      contents.font.set_style( :default )
      draw_text( bmp.width+8, dy, contents.width-bmp.width-8, 20, "Class:", 0 )
      contents.font.set_style( :paper_text )
      contents.font.size = Font.default_size - 2
      contents.font.bold = true
      draw_text( bmp.width+64, dy+2, contents.width-bmp.width-64, 20, genemy.class.name, 0 )
      contents.font.set_style( :default )
      draw_text( bmp.width+8, dy+24, contents.width-bmp.width-8, 20, "Level:", 0 )
      contents.font.set_style( :default )
      contents.font.size = Font.default_size - 2
      contents.font.bold = true
      draw_text( bmp.width+64, dy+26, contents.width-bmp.width-64, 20, genemy.level, 0 )
      dy += 102
      draw_horz_line(dy)
      dy += 2
      contents.font.set_style( :default )
      draw_parameters( genemy, 0, dy )
      dy += (24 * 8)
      draw_horz_line(dy)
      dy += 2
      contents.font.set_style( :simple_black )
      #contents.font.size = Font.default_size - 8
      text = convert_escape_characters(@item.description)
      pos = {:x => 0, :y => dy, :new_x => 0, 
        :width => contents.width, :height => 20 }#calc_line_height(text)}
      process_character(text.slice!(0, 1), text, pos) until text.empty?
      #draw_text_ex( 0, dy, @item.description )
    end  
    @character.sx = self.x + padding + ((bmp.width - 32) / 2)
    @character.sy = self.y + padding + ((bmp.height - 32) - 4) + 28
    bmp.dispose
    @dir_index = 0
    @dir_counter = 0
    @character.set_direction( @directions[@dir_index] ) 
    @character.set_sprite_effect_type( :appear )
  end  
  def draw_parameters(enemy, x, y)
    8.times {|i| draw_actor_param(enemy, x, y + line_height * i, i) }
  end
  def draw_actor_param( actor, x, y, param_id )
    contents.font.set_style( :paper_text )
    draw_text(x, y, contents.width, line_height, Vocab::param(param_id))
    contents.font.set_style( :default )
    draw_text(x + (contents.width-48), y, 36, line_height, actor.param(param_id), 2)
  end
  def update()
    super()
    @character.update()
    @dir_counter = (@dir_counter + 1) % 120
    if @dir_counter % 120 == 0
      @dir_index = (@dir_index + 1) % @directions.size
      @character.set_direction( @directions[@dir_index] ) 
    end  
    @sprite.update()
  end  
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
