# Game::Action (Ex)
# // 02/28/2012
# // 02/28/2012
class Game::Action
  class ParameterCode
    attr_accessor :code
    attr_accessor :value1
    attr_accessor :value2
    def initialize( *args )
      set( *args )
    end
    def set( code=0, value1=0, value2=0 ) 
      @code = code
      @value1 = value1
      @value2 = value2
    end  
  end  
  class EffectArea
    attr_reader :object
    attr_accessor :direction
    def initialize(subject,object)
      @subject = subject
      @direction = 5
      self.object = object
    end  
    def _map()
      @subject._map
    end  
    def object=(n)
      @object = n
    end  
    # // Start x, y + Target x, y
    def make_effect_area(x,y,tx,ty)
      range = @subject.get_effect_range(@object)
      range.mk_range_a2(x,y,tx,ty)
    end  
    def mk_range_a(x,y,tx,ty)
      # // ._. Instead of finding the target from the start
      # // The target finds the start .__. @______@
      _map.adjust_range(make_effect_area(tx,ty,x,y),tx,ty)
    end  
    def mk_range_table(x,y,tx,ty)
      RPG::BaseItem::Range.mk_range_table(mk_range_a(x,y,tx,ty),_map.width,_map.height)
    end      
  end  
  def effect_select_targets(targets)
    #return targets unless effect_pos?()
    table = @effect_area.mk_range_table(@subject.x,@subject.y,*effect_pos)
    targets.select{|t|table[t.x, t.y] == 1}
  end  
  def select_single_targets(targets)
    targets # // Just return it, no need to remove anyone . 3 .
  end  
  #--------------------------------------------------------------------------
  # ● スキルを設定
  #--------------------------------------------------------------------------
  def set_skill(skill_id)
    set_obj($data_skills[skill_id])
    self
  end
  #--------------------------------------------------------------------------
  # ● アイテムを設定
  #--------------------------------------------------------------------------
  def set_item(item_id)
    set_obj($data_items[item_id])
    self
  end
  def set_obj(item)
    @effect_area.object = @item.object = item
    self
  end  
  alias :prm_clear :clear
  def clear()
    prm_clear()
    @parameter_code = ParameterCode.new(0, 0, 0)
    @effect_area    = EffectArea.new(@subject, nil)
    @effect_x       = nil
    @effect_y       = nil
    @ov_targets     = nil
    self
  end
  attr_accessor :effect_x, :effect_y
  def set_effect_pos(x,y)
    @effect_x, @effect_y = x, y
    self
  end  
  def effect_pos()
    return effect_pos? ? [@effect_x, @effect_y] : [@subject.x, @subject.y] 
  end 
  def effect_pos?()
    !@effect_x.nil?()
  end  
  def mk_effect_a()
    @effect_area.mk_range_a(@subject.x, @subject.y, *effect_pos)
  end  
  def parameter_code
    return @parameter_code
  end  
  def set_skip()
    set_skill(subject.skip_skill_id)
    self
  end  
  def set_move()
    set_skill(subject.move_skill_id)
    self
  end 
  def set_targets( targets )
    @ov_targets = targets
    self
  end 
  def skip?()
    item == $data_skills[subject.skip_skill_id]
  end
  def confusion_target()
    case subject.confusion_level
    when 1
      subject.get_alive_enemies_sorrounding.sample
    when 2
      if rand(2) == 0
        subject.get_alive_enemies_sorrounding.sample
      else
        subject.get_alive_allies_sorrounding.sample
      end
    else
      subject.get_alive_allies_sorrounding.sample
    end
  end
end  
