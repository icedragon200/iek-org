# Mixin::EquipWrap
#==============================================================================#
# ■ Mixin::EquipWrap
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 01/09/2012
# // • Data Modified : 01/09/2012
# // • Version       : 1.0
#==============================================================================#
module Mixin::EquipWrap
  def param_size
    8
  end  
  @wraps  = [:id, :name, :icon_index, :description, :features, :note] # RPG::BaseItem
  @wraps += [:price, :etype_id, :params] # RPG::EquipItem
  @wraps += [:wtype_id, :animation_id]   # RPG::Weapon
  @wraps += [:atype_id]                  # RPG::Armor
  @wraps += [:performance]               # RPG::Weapon && RPG::Armor
  # RPG::BaseItem (Addon)
  @wraps += [:iconset_name, :db_id, :effect_range, :atk_range, :element_id]      
  @wraps.each { |s| module_eval( %Q(def #{s.to_s} ; base_object.#{s.to_s} ; end) ) }
  def param( param_id )
    params[param_id]
  end    
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
