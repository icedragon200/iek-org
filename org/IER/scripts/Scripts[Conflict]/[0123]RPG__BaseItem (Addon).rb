# RPG::BaseItem (Addon)
class RPG::BaseItem  
  class Range # // 02/28/2012
    attr_accessor :code
    attr_accessor :range, :minrange
    def initialize(code, range=0, minrange=0)
      @code = code
      @range, @minrange = range, minrange
    end
    def mk_range_a()
      MkRange.by_id(@code, @range, @minrange)
    end
    # // Special Calculations . x .
    def mk_range_a2(x,y,tx,ty)
      case(@code)
      # // LTT ignores range, but works with minrange O_O...
      when RANGE_LINE_TT
        tab = Table.new(20,20)
        nodes = []
        # // 0 all the target data ._. Much easier to handle and convert later
        cn    = [0,0]
        tn    = [tx-x,ty-y]
        dx,dy = nil, nil
        while(cn != tn) # // While your still not at your target
          nodes << cn unless((cn[0]+cn[1]).abs < @minrange) # // Add the current node
          dx,dy = cn[0] - tn[0], cn[1] - tn[1]
          cn = [cn[0]+dx.pole_inv,cn[1]+dy.pole_inv]  
          tab[cn[0]+10,cn[1]+10] = 1
          puts MapManager.text_based_minimap2(tab)
        end 
        nodes
      else # // Make regular Range 
        mk_range_a()
      end  
    end  
    def too_close?(sx,sy,tx,ty)
      [(sx - tx), (sy - ty)].any?{|s|!s.abs.between?(@minrange,@range)}
    end  
    def self.mk_range_table(data,width,height)
      table = Table.new(width,height)
      data.each{|r|table[*r]=1}
      table
    end  
    module Constants  
      RANGE_DIAMOND = 1
      RANGE_SQUARE  = 2
      RANGE_LINE    = 3
      RANGE_LINE_TT = 4 # // Line to Target
    end  
    include Constants 
  end
  def initialize_add
    @iconset_name = "Iconset"
    @database_offset = 0
    # // 02/13/2012
    @element_id = 0
    # // 02/28/2012
    @effect_range = Range.new(Range::RANGE_DIAMOND, 0, 0)
    @atk_range    = Range.new(Range::RANGE_DIAMOND, 1, 1)
  end  
  def db_id()
    @database_offset + @id
  end  
  attr_accessor :iconset_name
  attr_accessor :database_offset 
  # // 02/13/2012
  attr_accessor :element_id
  # // 02/28/2012
  attr_accessor :effect_range
  attr_accessor :atk_range
end    
