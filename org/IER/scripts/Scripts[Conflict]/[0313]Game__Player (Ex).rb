# Game::Player (Ex)
# // 02/14/2012
# // 02/18/2012
class Game::Character
  def force_path(tx,ty)
    path = find_path(
      :tx => tx,
      :ty => ty,
      :route => true,
      :diagnol => false
    )
    # The path retrieved is actually backwards, so it must be reversed
    path.reverse!
    # Add an end ccommand
    path.push(RPG::MoveCommand.new(0))
    move_route = RPG::MoveRoute.new
    move_route.list = path
    move_route.repeat = false
    force_move_route(move_route)
    return !path.empty?
  end  
end  
class Game::Player
  def encounter
    return
  end  
  def move_by_input
    return if _map.interpreter.running?
    return if $move_disabled
    if MouseEx.left_press?
      #force_path(@target_x, @target_y)
      $game_system.move_target.set(*_map.mouse_x32_pos)
      #return
    end 
    unless moving? 
      move_toward_character($game_system.move_target) unless $game_system.move_target.disabled?()
      null_target_xy() if $game_system.move_target.obj_pos_match?(self)
    end  
    return if !movable? 
    if Input.dir4 > 0
      move_straight(Input.dir4) ; null_target_xy()
    end  
  end
  def null_target_xy()
    $game_system.move_target.set(nil,nil)
  end  
end 
# // 02/18/2012
class Game::System
  Point = Struct.new(:x, :y)
  class Point
    def set(x,y) 
      self.x, self.y = x, y
    end  
    def disabled?
      self.x == nil
    end 
    def match?(x, y)
      return self.x == x && self.y == y
    end  
    def obj_pos_match?(obj)
      match?(obj.x,obj.y)
    end 
    def to_a
      return self.x, self.y
    end  
  end  
  def move_target
    @move_target ||= Point.new(nil,nil)
    @move_target
  end  
end  
