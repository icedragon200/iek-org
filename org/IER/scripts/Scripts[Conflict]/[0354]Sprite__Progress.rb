# Sprite::Progress
# // 02/03/2012
# // 02/03/2012
class Sprite::Progress < Sprite
  def initialize(viewport=nil,type=:horz,length=128,size=12)
    super(viewport)
    @type   = type
    @length = length
    @size   = size 
    @rate   = 0.0
    @bar    = Sprite.new()
    @bar.ox = -1
    @bar.oy = -1
    refresh()
    self.x = 0
    self.y = 0
    self.z = 0
    @bar.viewport = self.viewport
  end  
  def dispose()
    dispose_bitmap()
    @bar.dispose_bitmap()
    @bar.dispose()
    super()
  end  
  attr_reader :rate
  def rate=(rate)
    return unless(@rate != rate)
    @rate = rate
    refresh_rate
  end  
  def refresh()
    dispose_bitmap()
    @bar.dispose_bitmap()
    case @type
    when :horz
      self.bitmap = DrawExt.get_bar1_base(
        :width  => @length, 
        :height => @size,
      )
      @bar.bitmap  = DrawExt.get_bar1_bar(
        {:width  => @length-2, 
        :height => @size-2}.merge(DrawExt::METAL2_BAR_COLORS)#YELLOW_BAR_COLORS)
      )
    when :vert
      self.bitmap = Bitmap.new(@size, @length)
      @bar.bitmap  = Bitmap.new(@size-2, @length-2)
    end  
    refresh_rate()
    self.x = self.x
    self.y = self.y
    self.z = self.z
  end  
  def viewport=(n)
    super(n)
    @bar.viewport = self.viewport
  end  
  def x=(n)
    super(n)
    @bar.x = self.x
  end
  def y=(n)
    super(n)
    @bar.y = self.y
  end
  def z=(n)
    super(n)
    @bar.z = self.z + 1
  end  
  def refresh_rate
    case @type
    when :horz
      @bar.src_rect.set(0,0,@bar.bitmap.width*@rate,@bar.bitmap.height)
    when :vert
      @bar.src_rect.set(0,0,@bar.bitmap.width,@bar.bitmap.height*@rate)
    end  
  end  
end  
