# Container::Base
# // 02/06/2012
# // 02/06/2012
class Container::Base < Container
  include WindowAddons::Base
  include Container::Addons::Base
  # // Window Addon Patch + Container Addon Patch O:
  def init_internal(*args,&block)
    super(*args,&block)
    self.padding = standard_padding
    init_addons()
    init_caddons()
  end
  def dispose(*args,&block)
    dispose_addons()
    dispose_caddons()
    super(*args,&block)
  end
  def update(*args,&block)
    super(*args,&block)
    update_addons()
    update_caddons()
    update_open if @opening
    update_close if @closing
  end
  def standard_padding
    12
  end  
  # // Window Addons :D - Unlike windows .x . 
  def init_addons()     ; end unless method_defined?( :init_addons )
  def dispose_addons()  ; end unless method_defined?( :dispose_addons )
  def update_addons()   ; end unless method_defined?( :update_addons )
  # // Container Addons :D
  def init_caddons()    ; end unless method_defined?( :init_caddons )
  def dispose_caddons() ; end unless method_defined?( :dispose_caddons )
  def update_caddons()  ; end unless method_defined?( :update_caddons )
  # // Window Addon Win Busy method patch :D
  def win_busy?()
    false
  end unless method_defined?(:win_busy?)  
  def mouse_in_window?()
    MouseEx.in_area?(self.to_rect)
  end
  # // 03/03/2012
  def line_height
    24
  end 
  def fitting_height(line_number)
    line_number * line_height + standard_padding * 2
  end
end  
