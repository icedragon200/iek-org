# Sprite::LoopAnimation
# // 02/25/2012
# // 02/25/2012
class Sprite::LoopAnimation < Sprite
  attr_accessor :active
  def initialize(viewport=nil,filename="",pattern=[0,1,2,3],time=10)
    @hue = 0
    @sequencer = IEI::Sequencer.new(pattern, time)
    super(viewport)
    self.filename = filename
    self.active = true
    update_src_rect()
  end
  attr_reader :filename
  def filename=(n)
    self.bitmap = Cache.animation(@filename=n,@hue)
  end  
  def maxcount();@sequencer.maxcount;end
  def maxcount=(n);@sequencer.maxcount=n;end
  attr_reader :hue
  def hue=(n)
    if @hue != n
      @hue = n
      self.bitmap.dispose
      self.filename = @filename
    end
  end  
  def update_src_rect()
    @sequencer.update() if self.active
    self.src_rect.set((@sequencer.value%5)*192,(@sequencer.value/5)*192,192,192)
  end  
  def update()
    super()
    update_src_rect()
  end  
end    
