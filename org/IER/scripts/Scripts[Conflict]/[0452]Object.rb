# Object
#~ class SomeParentObject ; end
#~ class SomeObject < SomeParentObject
#~   def <<
#~   end
#~   def >>
#~   end
#~   def +@(val)
#~     val+1
#~   end
#~   def -@(val)
#~     val-1
#~   end  
#~   def <(val)
#~     @val = val
#~     puts "< #{@val}"
#~     self
#~   end  
#~   def >
#~     puts "> #{@val}"
#~     self
#~   end
#~   def []
#~     puts "[]"
#~     self
#~   end  
#~   def ^
#~     puts "^ :3" 
#~     self
#~   end  
#~   def /
#~     puts "/"
#~     self
#~   end  
#~   def ~
#~     puts "~ Accent Stuff"
#~     self
#~   end  
#~   def !
#~     puts "Exclamation!"
#~     self
#~   end  
#~   def &
#~     puts "&"
#~     self
#~   end  
#~   def _
#~     puts "Underscore_"
#~     self
#~   end  
#~ end 
#~ obj = SomeObject.new()
#~ ((obj < 2).>.[].^./.~).!.&._
#Looper( proc { 0 }, proc { |n| n < 1000 }, proc { |n| n.next } ) { |n| puts n }
