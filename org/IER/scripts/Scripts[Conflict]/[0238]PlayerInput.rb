# PlayerInput
# // 12/25/2011
# // 01/31/2012
class InputHandler
  OTHER_KEYS = (
   #%w(DOWN LEFT RIGHT UP)+
   %w(A B C X Y Z L R)#+
   #%w(SHIFT CTRL ALT)+
   #%w(F5 F6 F7 F8 F9)
  ).collect{|s|s.to_sym}
  attr_reader :handler
  def initialize
    @handler = {}
  end  
  def clear_handler()
    @handler.clear()
  end  
  def set_handler( symbol, method )
    @handler[symbol] = method
  end  
  def update()
    return @handler[:dir4].call( Input.dir4 ) if Input.dir4() > 0 if @handler[:dir4]  
    return @handler[:LR].call() if Input.press_all?( :L, :R ) if @handler[:LR]
    return @handler[:LX].call() if Input.press_all?( :L, :X ) if @handler[:LX]
    return @handler[:LY].call() if Input.press_all?( :L, :Y ) if @handler[:LY]
    return @handler[:LZ].call() if Input.press_all?( :L, :Z ) if @handler[:LZ] 
    return @handler[:RX].call() if Input.press_all?( :R, :X ) if @handler[:RX] 
    return @handler[:RY].call() if Input.press_all?( :R, :Y ) if @handler[:RY] 
    return @handler[:RZ].call() if Input.press_all?( :R, :Z ) if @handler[:RZ] 
    return @handler[:mC].call() if Input.mtrigger?( :C ) if @handler[:mC]
    return @handler[:mB].call() if Input.mtrigger?( :B ) if @handler[:mB]
    OTHER_KEYS.each { |k|
      return @handler[k].call() if Input.trigger?( k ) if @handler[k] 
    }  
  end  
end  
PlayerInput = InputHandler.new()
