# Scene::PreNewGame
#==============================================================================#
# ♥ Scene::PreNewGame
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/31/2011
# // • Data Modified : 12/31/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/31/2011 V1.0 
#
#==============================================================================#
class Scene::PreNewGame < Scene::Base
  #--------------------------------------------------------------------------#
  # ● overwrite-method :main
  #--------------------------------------------------------------------------#
  def main()
    unless $game_temp.other[0]
      SceneManager.clear()  
      return SceneManager.goto( Scene::Title )
    end  
    $game_party.add_actor( 1 )
    $game_party.add_actor( 2 )
    $game_party.add_actor( 3 )
    $game_system.savefile_index = $game_temp.other[0]
    $game_system.save_background_index = 1
    #RogueManager.setup_map( -1 ).start_dungeon()
    $game_map.setup(2)
    $game_player.make_encounter_count
    $game_player.moveto(6,6)
    Graphics.frame_count = 0
    DataManager.save_game( $game_system.savefile_index )
    SceneManager.goto(Scene::Map)
  end
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
