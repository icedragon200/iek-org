# Window::Inventory
#==============================================================================#
# ♥ Window::Inventory
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/11/2011
# // • Data Modified : 12/11/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/11/2011 V1.0
#
#==============================================================================#
class Window::Inventory < Window::Selectable
  def initialize(x,y,w=nil,h=nil)
    @items = []
    w ||= window_width
    h ||= window_height
    super(x,y,w,h)
    select(0)
  end
  attr_reader :battler
  def battler=( new_battler )
    if @battler != new_battler
      @battler = new_battler
      refresh()
    end  
  end  
  def actor
    battler
  end
  def actor=( new_actor )
    self.battler = new_actor
  end 
  def set_actor(n)
    set_battler(n)
  end 
  def window_width
    return ((item_width+spacing) * col_max) + standard_padding * 2
  end  
  def window_height
    return (item_height * 3) + (standard_padding * 2)
  end  
  def update_pos
    update_height
  end  
  def item_max
    @items.size
    #@battler.inventory.size
  end
  def col_max
    return 4
  end  
  def item_width
    return 28+32+4
  end  
  def item_height
    return 28
  end  
  def spacing
    return 2
  end  
  def current_item()
    return @items[self.index]
  end  
  def item()
    return current_item()
  end
  def inventory
    @battler.inventory
  end  
  def item_number( item )
    inventory.item_number( item )
  end  
  def make_item_list
    @items = (@battler ? inventory.all_items : []).select{|i|include?(i)}
  end  
  def refresh()
    make_item_list()
    create_contents()
    draw_all_items()
    call_update_help()
  end  
  def draw_item( index )
    art = open_artist
    item        = @items[index]
    enabled     = item_enabled?( item )
    rect        = item_rect( index ).contract(2)
    wd, hd      = 24 - 24, rect.height - 24
    icon_rect   = Rect.new( rect.x, rect.y, 24, 24 )
    number_rect = Rect.new( rect.x+28, rect.y+4, 32, 20 )
    set         = ((@battler ? @battler._hot_keys.obj_set?(item) : false) && !item.nil?())
    equip       = ((@battler ? @battler.equips.include?(item) : false) && !item.nil?())
    bmp         = Cache.system("Item_Borders(Window)")
    brect       = Rect.new( 0, 0, bmp.width, 24 )
    brect.y     = 24 * 4 if set 
    brect.y     = 24 * (Ex_Database.ex?( item ) ? 1 : 2) if Ex_Database.can_ex?( item )
    brect.y     = 24 * 3 if equip
    drawing_sandbox() do
      contents.blt( icon_rect.x, icon_rect.y, bmp, brect )
      contents.font.size = Font.default_size - 4
      art.draw_item_icon( item, icon_rect.x + (wd / 2), icon_rect.y + (hd / 2) )
      contents.font.set_style(:simple_black)
      if Ex_Database.ex_weapon?( item )
        art.draw_item_small_exp(item,number_rect.x+2,number_rect.y+12,number_rect.width-4,6) 
        contents.font.size = Font.default_size - 8
        number_rect.y -= 4
        art.draw_text( number_rect, item.level_s, 1 )
      elsif Ex_Database.ex_armor?( item )
      else
        art.draw_text( number_rect, "x#{item_number(item)}", 1 ) if item
      end  
    end  
  end
  def draw_item_help( x, y, item )
    contents.font.set_style(:simple_black)
    contents.font.size = Font.default_size
    contents.clear_rect( x, y, contents.width, 24 )
    dx = x
    dy = y
    draw_item_icon( item, dx, dy )
    dx += 24
    contents.font.size = Font.default_size - 6
    draw_box( dx, dy+12, contents.width, 12 )
    draw_text( dx, dy, contents.width-(dx), 12, item ? item.name : "" )
    dy += 12
    contents.font.size = Font.default_size - 8
    draw_text( dx+4, dy, contents.width-(dx+4), 12, item ? item.description : "" )
  end   
  def include?(i)
    true
  end  
  def item_enabled?( item )
    return true
  end 
  def redraw_current_item()
    super()
    call_update_help()
  end  
  #--------------------------------------------------------------------------
  # ● ステータスウィンドウの設定
  #--------------------------------------------------------------------------
  def status_window=(status_window)
    @status_window = status_window
    call_update_help
  end
  #--------------------------------------------------------------------------
  # ● アイテムウィンドウの設定
  #--------------------------------------------------------------------------
  def item_window=(item_window)
    @item_window = item_window
    update
  end
  #--------------------------------------------------------------------------
  # ● ヘルプテキスト更新
  #--------------------------------------------------------------------------
  def update_help()
    super()
    @help_window.set_item(item) if @help_window
    #@status_window.set_temp_actor(nil) if @status_window
  end
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
