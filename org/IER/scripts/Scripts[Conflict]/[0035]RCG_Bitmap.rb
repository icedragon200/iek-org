# RCG_Bitmap
require 'C:/Lib/iExRuby/RCG.rb'
class Bitmap_RCG < Bitmap
  def initialize(rcg_struct)
    @rcg_struct = rcg_struct
    super(@rcg_struct.width,@rcg_struct.height)
    rcg_redraw()
  end
  def rcg_redraw()
    colors = @rcg_struct.colors.collect{|a|a ? Color.new(*a) : Pallete[:trans]}
    @rcg_struct.data.each_with_index { |n,i|
      set_pixel(i%width,i/width,colors[n])
    }
  end  
end
