# Container::ButtonTest
# // 02/24/2012
# // 02/24/2012
class Sprite::ButtonTest < Sprite
  attr_accessor :button
  attr_accessor :active
  def initialize(viewport=nil,button=nil)
    super(viewport)
    @button = button
    @counter = 0
    @xcounter = 0
    self.color = Pallete[:white]
    self.color.alpha = 0
    self.opacity = 0
  end
  def dispose()
    dispose_bitmap()
    super()
  end  
  def update()
    super()
    @counter = @counter.pred.max(0)
    @xcounter = @xcounter.pred.max(0)
    if Input.trigger?(@button)
      @counter = 10
      @xcounter = 0
      self.color.alpha = 0
    elsif Input.press?(@button)  
      @counter += 1
      @xcounter += 1
    end if @button && self.active
    self.opacity += 255 / 5.0 if @counter > 0
    self.opacity -= 255 / 5.0 if @counter == 0
    self.opacity = self.opacity.clamp(0,198)
    self.color.alpha += 255 / 60.0 if @xcounter > 0
    self.color.alpha -= 255 / 60.0 if @xcounter == 0
  end  
end  
class Container::ButtonTest < Container::Window
  def center_xy()
    salign(1,1)
  end  
  include Container::Addons::Background_Glass
  #def gbackground_tone()
  #  Tone.new(*Color::ConsoleText.to_tone).subtract(0.5)
  #end  
  wad = WindowAddons
  include Container::Addons::HandlerBase
  class AddonBin_CBT < wad::AddonBin[wad::Header,wad::WinButtons,wad::Mouse_MoveWindow]
    def header_text
      "Button Test"
    end  
    def winbuttons_minimize
      Sound.play_option_change
      parent.size_index = parent.size_index.pred.clamp(0,parent.sizes.size-1)
      parent.refresh_button_size()
    end  
    def winbuttons_maximize
      Sound.play_option_change
      parent.size_index = parent.size_index.succ.clamp(0,parent.sizes.size-1)
      parent.refresh_button_size()
    end  
    def winbuttons_close
      Sound.play_cancel
      parent.call_handler(:wb_close)
    end  
    def winbuttons_actives
      [true,true,true]
    end  
  end
  def addon_bin
    AddonBin_CBT
  end  
  attr_accessor :size_index, :sizes, :button_size
  def initialize(x,y)
    @button_size = 1
    @sizes = [32,48,56,64,72]
    @size_index = @sizes.size / 2
    super(x,y,window_width,window_height)
  end  
  def refresh_button_size()
    self.button_size = @sizes[@size_index]
  end
  def init_internal()
    super()
    @sprites = []
    refresh_button_size()
  end
  def _obj_enabled?(n)
    return false if n == :oposition
    return false if n == :size
    return false if n == :opacity
    super
  end  
  def _redraw()
    super()
    refresh()
  end   
  attr_reader :button_size
  def button_size=(n)
    if(@button_size != n)
      @button_size = n
      self.width = window_width
      self.height = window_height
      _redraw!()
      call_handler(:button_size_change)
    end  
  end  
  def _objs
    super + @sprites
  end  
  def window_width
    (button_size*7)
  end
  def window_height
    (button_size*5)+(standard_padding * 2)
  end 
  def refresh()
    _dispose_objs()
    @sprites.clear()
    # // . x . dir4 ; Buttons
    #make_dir4(0,button_size*1.0)
    #make_standard6((contents.width-(button_size*3)),button_size*1.0)
    #make_funcs((contents.width-(button_size*5))/2,0)
    #make_other(0,button_size*4.0)
    # // . x . Buttons ; dir4
    make_dir4((contents.width-(button_size*3)),button_size*1.0)
    make_standard6(0,button_size*1.0)
    make_funcs((contents.width-(button_size*5))/2,0)
    make_other((contents.width-(button_size*3)),button_size*4.0)
    #make_l_and_r()
  end
  def make_dir4(x,y)
    bx,by = adjust_x4contents(x), adjust_y4contents(y)
    @sprites[0] = Sprite::ButtonTest.new(nil, :UP   ).set_oxy(-(bx+(button_size)),-(by))
    @sprites[1] = Sprite::ButtonTest.new(nil, :LEFT ).set_oxy(-(bx)   ,-(by+(button_size)))
    @sprites[2] = Sprite::ButtonTest.new(nil, :RIGHT).set_oxy(-(bx+(button_size*2)),-(by+button_size))
    @sprites[3] = Sprite::ButtonTest.new(nil, :DOWN ).set_oxy(-(bx+button_size),-(by+(button_size*2)))
    contents.ext_draw_box3(
      :x             => x,
      :y             => y,
      :width         => button_size*3,
      :height        => button_size*3,
      :base_color    => Pallete[:sys_orange],
      :padding       => 2,
      :footer_height => 2
    )
    for i in 0...4
      sp = @sprites[i]
      sp.bitmap = Bitmap.new(button_size,button_size)
      sp.bitmap.fill_rect(sp.bitmap.rect.contract(2),Pallete[:sys_orange])
      sp.bitmap.blur_ex(2)
      draw_button(x-(sp.ox+bx),y-(sp.oy+by),sp.button.to_s)
    end  
  end  
  def make_standard6(x,y)
    bx,by = adjust_x4contents(x), adjust_y4contents(y)
    @sprites[ 4] = Sprite::ButtonTest.new(nil, :A).set_oxy(-(bx)                ,-(by+(button_size)))
    @sprites[ 5] = Sprite::ButtonTest.new(nil, :B).set_oxy(-(bx+(button_size))  ,-(by+(button_size*0.5)))
    @sprites[ 6] = Sprite::ButtonTest.new(nil, :C).set_oxy(-(bx+(button_size*2)),-(by))
    @sprites[ 7] = Sprite::ButtonTest.new(nil, :X).set_oxy(-(bx)                ,-(by+(button_size*2)))
    @sprites[ 8] = Sprite::ButtonTest.new(nil, :Y).set_oxy(-(bx+(button_size))  ,-(by+(button_size*1.5)))
    @sprites[ 9] = Sprite::ButtonTest.new(nil, :Z).set_oxy(-(bx+(button_size*2)),-(by+(button_size)))
    # // . x .
    @sprites[10] = Sprite::ButtonTest.new(nil, :L).set_oxy(-(bx+(button_size))  ,-(by+(button_size*3)))
    @sprites[11] = Sprite::ButtonTest.new(nil, :R).set_oxy(-(bx+(button_size*2)),-(by+(button_size*3)))
    contents.ext_draw_box3(
      :x             => x,
      :y             => y,
      :width         => button_size*3,
      :height        => button_size*4,
      :base_color    => Pallete[:sys_green],
      :padding       => 2,
      :footer_height => 2
    )
    for i in 4...12
      sp = @sprites[i]
      sp.bitmap = Bitmap.new((button_size),(button_size))
      sp.bitmap.fill_rect(sp.bitmap.rect.contract(2),Pallete[:sys_green])
      sp.bitmap.blur_ex(2)
      draw_button(x-(sp.ox+bx),y-(sp.oy+by),sp.button.to_s)
    end
  end
  def make_funcs(x,y)
    bx,by = adjust_x4contents(x), adjust_y4contents(y)
    5.times{|i|@sprites[12+i]=Sprite::ButtonTest.new(nil,"F#{5+i}".to_sym).set_oxy(-(bx+(button_size*i)),-(by))}
    contents.ext_draw_box3(
      :x             => x,
      :y             => y,
      :width         => button_size*5,
      :height        => button_size*1,
      :base_color    => Pallete[:sys_blue],
      :padding       => 2,
      :footer_height => 2
    )
    for i in 12...(12+5)
      sp = @sprites[i]
      sp.bitmap = Bitmap.new((button_size),(button_size))
      sp.bitmap.fill_rect(sp.bitmap.rect.contract(2),Pallete[:sys_blue])
      sp.bitmap.blur_ex(2)
      draw_button(x-(sp.ox+bx),y-(sp.oy+by),sp.button.to_s)
    end
  end
  def make_other(x,y)
    bx,by = adjust_x4contents(x), adjust_y4contents(y)
    @sprites[17] = Sprite::ButtonTest.new(nil,:SHIFT).set_oxy(-(bx+(button_size*0)),-(by))
    @sprites[18] = Sprite::ButtonTest.new(nil,:CTRL).set_oxy(-(bx+(button_size*1)),-(by))
    @sprites[19] = Sprite::ButtonTest.new(nil,:ALT).set_oxy(-(bx+(button_size*2)),-(by))
    contents.ext_draw_box3(
      :x             => x,
      :y             => y,
      :width         => button_size*3,
      :height        => button_size*1,
      :base_color    => Pallete[:sys_red],
      :padding       => 2,
      :footer_height => 2
    )
    for i in 17...20
      sp = @sprites[i]
      sp.bitmap = Bitmap.new((button_size),(button_size))
      sp.bitmap.fill_rect(sp.bitmap.rect.contract(2),Pallete[:sys_red])
      sp.bitmap.blur_ex(2)
      draw_button(x-(sp.ox+bx),y-(sp.oy+by),sp.button.to_s)
    end
  end  
  def draw_button(x,y,text)
    width, height = (button_size), (button_size)
    contents.ext_draw_box3(
      :x             => x,
      :y             => y,
      :width         => width,
      :height        => height,
      :base_color    => Pallete[:black],
      :padding       => 2,
      :footer_height => 1
    )
    contents.font.set_style(:window_header2)
    contents.font.size = button_size / 3
    contents.font.color = Pallete[:white]
    contents.draw_text(x,y,width,height,text,1)
  end  
  def dispose()
    
    super()
  end  
  def update()
    super()
    
  end  
end  
