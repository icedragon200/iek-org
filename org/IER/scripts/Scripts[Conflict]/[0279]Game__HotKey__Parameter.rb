# Game::HotKey::Parameter
#==============================================================================#
# ♥ Game::HotKey::Parameter
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/30/2011
# // • Data Modified : 12/30/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/30/2011 V1.0 
#
#==============================================================================# 
class Game::HotKey::Parameter
  NIL_CODE             = 0
  USE_OBJ_CODE         = 1
  THROW_OBJ_CODE       = 5
  DROP_OBJ_CODE        = 8
  QUICK_SETTINGS = {
    :nil      => [NIL_CODE      , 0],
    :throw    => [THROW_OBJ_CODE, 0],
    :drop     => [DROP_OBJ_CODE , 0],
    :use_auto => [USE_OBJ_CODE  , 0],
    :use_self => [USE_OBJ_CODE  , 1],
    :use_inf  => [USE_OBJ_CODE  , 2],
    :use_ask  => [USE_OBJ_CODE  , 3]
  }
  attr_accessor :code
  attr_accessor :value
  def initialize( code=0, value=0 )
    set( code, value )
  end  
  def set( code=0, value=0 )
    @code, @value = code, value
    return self
  end 
  def sym_set( sym )
    set( *QUICK_SETTINGS[sym] )
  end  
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
