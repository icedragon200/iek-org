# Bitmap-EX
#~ #unless SARA::DEBUG
#~ # ===========================================================================
#~ # ★★ WF-RGSS Scripts ★★
#~ #   Bitmap-EX ビットマップスクリプト
#~ # バージョン   ： rev-6.4(2010-12-19)
#~ # 作者         ： A Crying Minister (WHITE-FLUTE)
#~ # サポート先URI： http://www.whiteflute.org/wfrgss/
#~ # ---------------------------------------------------------------------------
#~ # 機能：
#~ # ・ビットマップに対して様々なエフェクトをかけることが出来るようになります。
#~ # ・手軽に高速なゲージを導入することができます。
#~ # ・ビットマップをpngファイルに書き出すことができます。
#~ # ・アニメーションGIFをサポートします。暗号化アーカイブからでも読み込めます。
#~ # ---------------------------------------------------------------------------
#~ # 設置場所     ：共通スクリプト より下、Mainより上
#~ # 必要スクリプト：
#~ # ・共通スクリプト、共通実行スクリプト
#~ # 必要DLL： wfbitmap.dll
#~ # 注意事項：
#~ # ▽メソッドによっては時間の掛かる処理があります。
#~ # 著作権表示：
#~ # libpng version 1.2.42
#~ # * Copyright (c) 1998-2010 Glenn Randers-Pehrson
#~ # * (Version 0.96 Copyright (c) 1996, 1997 Andreas Dilger)
#~ # * (Version 0.88 Copyright (c) 1995, 1996 Guy Eric Schalnat, Group 42, Inc.)
#~ #
#~ # The GIFLIB distribution is Copyright (c) 1997  Eric S. Raymond
#~ #==============================================================================
#~ module Kernel
#~   def rpgvx?
#~     true
#~   end  
#~ end  
#~ module WFRGSS_BitmapEX
#~   #----------------------------------------------------------------------------
#~   # 高速テキスト描画に置き換えるか

#~   USE_DRAW_TEXT = false

#~ end

#~ #==============================================================================
#~ # RGSS組み込みクラス Font
#~ #------------------------------------------------------------------------------
#~ class Font
#~   def exist_font
#~     if self.name.is_a?(Array)
#~       for font in self.name
#~         return font if Font.exist?(font)
#~       end
#~       return ""
#~     else
#~       return self.name
#~     end
#~   end
#~ end

#~ #==============================================================================
#~ # RGSS組み込みクラス Bitmap
#~ #------------------------------------------------------------------------------
#~ class Bitmap
#~   #--------------------------------------------------------------------------
#~   # ● クラス変数
#~   #--------------------------------------------------------------------------
#~   begin
#~     @@mosaic = Win32API.new('wfbitmap','mosaic',%w(l l l l l),'l').freeze
#~     @@reversing = Win32API.new('wfbitmap','Reversing',%w(l l l),'l').freeze
#~     @@diffusion = Win32API.new('wfbitmap','Diffusion',%w(l l l l),'l').freeze
#~     @@blur = Win32API.new('wfbitmap','blur',%w(l l l l),'l').freeze
#~     @@darkrndimg = Win32API.new('wfbitmap','DarkRndImg',%w(l l l),'l').freeze
#~     @@lightrndimg = Win32API.new('wfbitmap','lightRndImg',%w(l l l),'l').freeze
#~     @@radialblur = Win32API.new('wfbitmap','radialBlur',%w(l l l l),'l').freeze
#~     @@rotationblur = Win32API.new('wfbitmap','rotationBlur',%w(l l l l),'l').freeze
#~     @@whirlblur = Win32API.new('wfbitmap','WhirlBlur',%w(l l l l l),'l').freeze
#~     @@postaraiz = Win32API.new('wfbitmap','Postaraiz',%w(l l l),'l').freeze
#~     @@guage = Win32API.new('wfbitmap','drawGuage',%w(l l l p p p l l l l l l),'l').freeze
#~     @@png = Win32API.new('wfbitmap','pngWrite',%w(l p i i),'l').freeze
#~     @@blend = Win32API.new('wfbitmap','blend_blt',
#~                            %w(l l l l l l i i l l l l l l),'l').freeze
#~     @@fontdraw = Win32API.new('wfbitmap','drawText',%w(p p l p p),'i').freeze
#~     @@fontsize = Win32API.new('wfbitmap','getTextSize',%w(p p l p p),'i').freeze
#~     @@dispose = Win32API.new('wfbitmap','dispose','v','v').freeze
#~   rescue Exception
#~     raise if debug?
#~     raise(LoadError,"cannot read modules.(wfbitmap.dll)")
#~   end
#~   #--------------------------------------------------------------------------
#~   # ● 解放
#~   #--------------------------------------------------------------------------
#~   def self.dispose
#~     @@dispose.call()
#~   end
#~   #--------------------------------------------------------------------------
#~   # ● モザイク
#~   #--------------------------------------------------------------------------
#~   def mosaic( msx = 2, msy = 2)
#~     raise(RGSSError,"disposed bitmap.") if self.disposed?
#~     b = self.dup
#~     @@mosaic.call( b.object_id , msx , msy , b.width , b.height )
#~     b
#~   end
#~   #--------------------------------------------------------------------------
#~   # ● モザイク
#~   #--------------------------------------------------------------------------
#~   def mosaic!( msx = 2, msy = 2)
#~     raise(RGSSError,"disposed bitmap.") if self.disposed?
#~     @@mosaic.call( object_id , msx , msy , self.width , self.height )
#~     self
#~   end
#~   #--------------------------------------------------------------------------
#~   # ● 色反転
#~   #--------------------------------------------------------------------------
#~   def reversing
#~     raise(RGSSError,"disposed bitmap.") if self.disposed?
#~     b = self.dup
#~     @@reversing.call( b.object_id , b.width , b.height )
#~     b
#~   end
#~   #--------------------------------------------------------------------------
#~   # ● 色反転
#~   #--------------------------------------------------------------------------
#~   def reversing!
#~     raise(RGSSError,"disposed bitmap.") if self.disposed?
#~     @@reversing.call( object_id , self.width , self.height )
#~     self
#~   end
#~   #--------------------------------------------------------------------------
#~   # ● 拡散
#~   #--------------------------------------------------------------------------
#~   def diffusion( noise = 2 )
#~     raise(RGSSError,"disposed bitmap.") if self.disposed?
#~     b = self.dup
#~     @@diffusion.call( b.object_id , noise , b.width , b.height )
#~     b
#~   end
#~   #--------------------------------------------------------------------------
#~   # ● 拡散
#~   #--------------------------------------------------------------------------
#~   def diffusion!( noise = 2 )
#~     raise(RGSSError,"disposed bitmap.") if self.disposed?
#~     @@diffusion.call( object_id , noise , self.width , self.height )
#~     self
#~   end
#~   unless rpgvx?
#~     #--------------------------------------------------------------------------
#~     # ● ぼかし(※時間がかかります。) ※ XPのみ VXでは標準のものを使う
#~     #--------------------------------------------------------------------------
#~     def blur( sm = 5 )
#~       raise(RGSSError,"disposed bitmap.") if self.disposed?
#~       b = self.dup
#~       @@blur.call(b.object_id , sm , b.width , b.height )
#~       b
#~     end
#~   else
#~     #--------------------------------------------------------------------------
#~     # ● ぼかし(※時間がかかります。) ※ VXのみ
#~     #--------------------------------------------------------------------------
#~     def blur2( sm = 5 )
#~       raise(RGSSError,"disposed bitmap.") if self.disposed?
#~       b = self.dup
#~       @@blur.call(b.object_id , sm , b.width , b.height )
#~       b
#~     end
#~   end  
#~   #--------------------------------------------------------------------------
#~   # ● ぼかし(※時間がかかります。)
#~   #--------------------------------------------------------------------------
#~   def blur!( sm = 5 )
#~     raise(RGSSError,"disposed bitmap.") if self.disposed?
#~     @@blur.call(object_id , sm , self.width , self.height )
#~     self
#~   end
#~   #--------------------------------------------------------------------------
#~   # ● ランダム色(暗色)
#~   #--------------------------------------------------------------------------
#~   def darkrndimg
#~     raise(RGSSError,"disposed bitmap.") if self.disposed?
#~     b = self.dup
#~     @@darkrndimg.call( b.object_id , b.width , b.height)
#~     b
#~   end
#~   #--------------------------------------------------------------------------
#~   # ● ランダム色(暗色)
#~   #--------------------------------------------------------------------------
#~   def darkrndimg!
#~     raise(RGSSError,"disposed bitmap.") if self.disposed?
#~     @@darkrndimg.call( object_id , self.width , self.height)
#~     self
#~   end
#~   #--------------------------------------------------------------------------
#~   # ● ランダム色(明色)
#~   #--------------------------------------------------------------------------
#~   def lightrndimg
#~     raise(RGSSError,"disposed bitmap.") if self.disposed?
#~     b = self.dup
#~     @@lightrndimg.call( b.object_id , b.width , b.height)
#~     b
#~   end
#~   #--------------------------------------------------------------------------
#~   # ● ランダム色(明色)
#~   #--------------------------------------------------------------------------
#~   def lightrndimg!
#~     raise(RGSSError,"disposed bitmap.") if self.disposed?
#~     @@lightrndimg.call( object_id , self.width , self.height)
#~     self
#~   end
#~   #--------------------------------------------------------------------------
#~   # ● 放射状ブラー（※時間がかかります。)
#~   #--------------------------------------------------------------------------
#~   def radialblur( ef = 10 )
#~     raise(RGSSError,"disposed bitmap.") if self.disposed?
#~     b = self.dup
#~     @@radialblur.call( b.object_id , b.width , b.height , ef )
#~     b
#~   end
#~   #--------------------------------------------------------------------------
#~   # ● 放射状ブラー（※時間がかかります。)
#~   #--------------------------------------------------------------------------
#~   def radialblur!( ef = 10 )
#~     raise(RGSSError,"disposed bitmap.") if self.disposed?
#~     @@radialblur.call( object_id , self.width , self.height , ef )
#~     self
#~   end
#~   #--------------------------------------------------------------------------
#~   # ● 回転ブラー（※時間がかかります。)
#~   #--------------------------------------------------------------------------
#~   def rotationblur( ef = 10 )
#~     raise(RGSSError,"disposed bitmap.") if self.disposed?
#~     b = self.dup
#~     @@rotationblur.call( b.object_id , b.width , b.height , ef )
#~     b
#~   end
#~   #--------------------------------------------------------------------------
#~   # ● 回転ブラー（※時間がかかります。)
#~   #--------------------------------------------------------------------------
#~   def rotationblur!( ef = 10 )
#~     raise(RGSSError,"disposed bitmap.") if self.disposed?
#~     @@rotationblur.call( object_id , self.width , self.height , ef )
#~     self
#~   end
#~   #--------------------------------------------------------------------------
#~   # ● 渦巻き（※時間がかかります。)
#~   #--------------------------------------------------------------------------
#~   def whirlblur( ef = 10 , r = 1 )
#~     raise(RGSSError,"disposed bitmap.") if self.disposed?
#~     _fixnum_range_check( r , -12 , 12 )
#~     b = self.dup
#~     @@whirlblur.call( b.object_id , b.width , b.height , ef , r )
#~     b
#~   end
#~   #--------------------------------------------------------------------------
#~   # ● 渦巻き（※時間がかかります。)
#~   #--------------------------------------------------------------------------
#~   def whirlblur!( ef = 10 , r = 1 )
#~     raise(RGSSError,"disposed bitmap.") if self.disposed?
#~     _fixnum_range_check( r , -12 , 12 )
#~     @@whirlblur.call( object_id , self.width , self.height , ef , r )
#~     self
#~   end
#~   #--------------------------------------------------------------------------
#~   # ● ポスタライズ
#~   #--------------------------------------------------------------------------
#~   def postaraiz
#~     raise(RGSSError,"disposed bitmap.") if self.disposed?
#~     b = self.dup
#~     @@postaraiz.call( b.object_id , b.width , b.height )
#~     b
#~   end
#~   #--------------------------------------------------------------------------
#~   # ● ポスタライズ
#~   #--------------------------------------------------------------------------
#~   def postaraiz!
#~     raise(RGSSError,"disposed bitmap.") if self.disposed?
#~     @@postaraiz.call( object_id , self.width , self.height )
#~     self
#~   end
#~   #--------------------------------------------------------------------------
#~   # ● 横ゲージ描画
#~   #--------------------------------------------------------------------------
#~   def horizontal_gauge( x, y, width, height, color1, color2, basecolor,life )
#~     raise(RGSSError,"disposed bitmap.") if self.disposed?
#~     c1 = color_convert( color1 )
#~     c2 = color_convert( color2 )
#~     bc = color_convert( basecolor )
#~     @@guage.call( object_id , self.width , self.height , c1 , c2 , bc ,
#~                   x , y + height, width , height , life , 0 )
#~     self
#~   end
#~   #--------------------------------------------------------------------------
#~   # ● 縦ゲージ描画
#~   #--------------------------------------------------------------------------
#~   def vertical_gauge( x, y, width, height, color1 , color2 , basecolor , life )
#~     raise(RGSSError,"disposed bitmap.") if self.disposed?
#~     c1 = color_convert( color1 )
#~     c2 = color_convert( color2 )
#~     bc = color_convert( basecolor )
#~     @@guage.call( object_id , self.width , self.height , c1 , c2 , bc ,
#~                   x , y + height, width , height , life , 1 )
#~     self
#~   end
#~   #--------------------------------------------------------------------------
#~   # ● 横ゲージ描画2 ▽△
#~   #--------------------------------------------------------------------------
#~   def horizontal_gauge2( x, y, width, height, color1, color2, basecolor,life )
#~     raise(RGSSError,"disposed bitmap.") if self.disposed?
#~     c1 = color_convert( color1 )
#~     c2 = color_convert( color2 )
#~     bc = color_convert( basecolor )
#~     @@guage.call( object_id , self.width , self.height , c1 , c2 , bc ,
#~                   x + height, y + height, width , height , life , 2 )
#~     self
#~   end
#~   #--------------------------------------------------------------------------
#~   # ● 横ゲージ描画3 △▽
#~   #--------------------------------------------------------------------------
#~   def horizontal_gauge3( x, y, width, height, color1, color2, basecolor,life )
#~     raise(RGSSError,"disposed bitmap.") if self.disposed?
#~     c1 = color_convert( color1 )
#~     c2 = color_convert( color2 )
#~     bc = color_convert( basecolor )
#~     @@guage.call( object_id , self.width , self.height , c1 , c2 , bc ,
#~                   x, y + height, width , height , life , 3 )
#~     self
#~   end
#~   #--------------------------------------------------------------------------
#~   # ● PNG書き出し
#~   #--------------------------------------------------------------------------
#~   def write_png( filename )
#~     raise(RGSSError,"disposed bitmap.") if self.disposed?
#~     @@png.call( object_id , filename , self.width , self.height )
#~   end
#~   #--------------------------------------------------------------------------
#~   # ● ブレンディング
#~   #--------------------------------------------------------------------------
#~   def blend_blt( x, y , src, rect, blend_type = 0 , opacity = 255 )
#~     raise(RGSSError,"disposed bitmap.") if self.disposed? 
#~     _fixnum_range_check( blend_type , 0 , 10 )
#~     @@blend.call( self.object_id , src.object_id , x , y , rect.x , rect.y ,
#~                   blend_type , opacity , src.width , src.height ,
#~                   self.width , self.height , rect.width , rect.height)
#~     self
#~   end
#~   #--------------------------------------------------------------------------
#~   # ● 文字描画
#~   #--------------------------------------------------------------------------
#~   def draw_text_s(*args)
#~     raise(RGSSError,"disposed bitmap.") if self.disposed?
#~     x, y, width, height, text, align, mode = get_text_args_s(args)
#~     w2 = text_size(text).width
#~     ry = height - self.font.size
#~     rect = [x-4, y + 2 + ( ry >> 1), width+8 , height+4 ,
#~             self.width , self.height, w2].pack("s*")
#~     data = "\x00" * 9
#~     data[0] = Integer( self.font.color.red )
#~     data[1] = Integer( self.font.color.green )
#~     data[2] = Integer( self.font.color.blue )
#~     data[3] = Integer( self.font.color.alpha )
#~     data[4] = self.font.size - 3 #4
#~     data[5] = mode & 0x1
#~     data[6] = (self.font.italic ? 2 : 0) | (self.font.bold ? 1 : 0)
#~     data[7] = ((mode & 0x2) >> 1)
#~     data[8] = align
#~     @@fontdraw.call(text.dup,self.font.exist_font.dup,
#~                      self.object_id,rect,data) == 1
#~   end
#~   private
#~   #--------------------------------------------------------------------------
#~   # ◆(内部専用)◆ 描画引数
#~   #--------------------------------------------------------------------------
#~   def get_text_args_s(args)
#~     if args.at(0).is_a?(Rect)
#~       if args.size.between?(2, 5)
#~         rect = args.at(0)
#~         x, y = rect.x, rect.y
#~         width, height = rect.width, rect.height
#~         text  = args.at(1).to_s
#~         align = (args.at(2).equal?(nil) ? 0 : args.at(2))
#~         mode = (args.at(3).equal?(nil) ? 1 : args.at(3)) # // 3
#~       else
#~         as = args.size < 2 ? 2 : 5
#~         raise(ArgumentError,
#~         "wrong number of arguments(#{args.size} of #{as})")
#~       end
#~     elsif args.size.between?(5, 8)
#~       x, y, width, height = args
#~       text  = args.at(4).to_s
#~       align = (args.at(5).equal?(nil) ? 0 : args.at(5))
#~       mode = (args.at(6) ? args.at(6) : 1 )
#~     else
#~       as = args.size < 5 ? 5 : 8
#~       raise(ArgumentError,
#~         "wrong number of arguments(#{args.size} of #{as})")
#~     end
#~     return [x, y, width, height, text, align, mode]
#~   end
#~   #--------------------------------------------------------------------------
#~   # ◆(内部専用)◆ カラーコンバート ( bgra の順になっていることに注意する)
#~   #--------------------------------------------------------------------------
#~   def color_convert(color)
#~     str = "\x00" * 4
#~     str[2] = color.red.to_i
#~     str[1] = color.green.to_i
#~     str[0] = color.blue.to_i 
#~     str[3] = color.alpha.to_i
#~     str
#~   end

#~   unless method_defined?(:original_draw_text)
#~     alias original_draw_text draw_text unless $@
#~     if WFRGSS_BitmapEX::USE_DRAW_TEXT
#~       alias draw_text draw_text_s unless $@
#~     end
#~   end
#~ end

#~ #==============================================================================
#~ # GIFFile クラス
#~ #------------------------------------------------------------------------------

#~ class GIFFile
#~   #--------------------------------------------------------------------------
#~   # ● クラス変数
#~   #--------------------------------------------------------------------------
#~   begin
#~     @@loader = Win32API.new('wfbitmap','gifLoader',%w(p p p p p),'l').freeze
#~     @@load = Win32API.new('wfbitmap','gifLoadImage',%w(l i),'l').freeze
#~     @@count = Win32API.new('wfbitmap','gifImageCount','v','l').freeze
#~     @@close = Win32API.new('wfbitmap','GifClose','v','v').freeze
#~   rescue Exception
#~     raise if debug?
#~     raise(LoadError,"cannot read modules.(wfbitmap.dll)")
#~   end
#~   #--------------------------------------------------------------------------
#~   # ● 定数
#~   #--------------------------------------------------------------------------
#~   RGSSA_FILE = rpgvx? ? "Game.rgss2a" : "Game.rgssad"
#~   #--------------------------------------------------------------------------
#~   # ● 公開インスタンス変数
#~   #--------------------------------------------------------------------------
#~   attr_reader :bitmap
#~   attr_reader :count
#~   #--------------------------------------------------------------------------
#~   # ● オブジェクト初期化
#~   #--------------------------------------------------------------------------
#~   def initialize( giffile )
#~     w = "\x00" * 2
#~     h = "\x00" * 2
#~     @loaded = false
#~     begin
#~       infile = giffile.gsub(/\//){ "\\" }
#~       i = @@loader.call(RGSSA_FILE,infile,String.utf82ansi(infile), w , h )
#~       raise(Errno::ENOENT,infile) if i < 0
#~       @loaded = true
#~       @count = @@count.call()
#~       width = w.unpack("S").first
#~       height = h.unpack("S").first
#~       @bitmap = Array.new( @count ){ Bitmap.new( width , height ) }
#~       i = 0
#~       for bitmap in @bitmap
#~         @@load.call( bitmap.object_id , i )
#~         i += 1
#~       end
#~     ensure
#~       @@close.call() if @loaded
#~     end
#~   end
#~   #--------------------------------------------------------------------------
#~   # ● 解放
#~   #--------------------------------------------------------------------------
#~   def dispose
#~     for bitmap in @bitmap
#~       bitmap.dispose
#~     end
#~   end
#~ end

#~ #==============================================================================
#~ # GifSprite クラス
#~ #------------------------------------------------------------------------------

#~ class GifSprite < Sprite
#~   #--------------------------------------------------------------------------
#~   # ● オブジェクト初期化
#~   #--------------------------------------------------------------------------
#~   def initialize( giffile , wait = 1 , viewport = nil)
#~     super(viewport)
#~     @gif = GIFFile.new( giffile )
#~     @wait = wait
#~     self.bitmap = @gif.bitmap.at(0)
#~     @_count = 0
#~     @_index = 0
#~   end
#~   #--------------------------------------------------------------------------
#~   # ● 解放
#~   #--------------------------------------------------------------------------
#~   def dispose
#~     @gif.dispose
#~     super
#~   end
#~   #--------------------------------------------------------------------------
#~   # ● フレーム更新
#~   #--------------------------------------------------------------------------
#~   def update
#~     super
#~     @_count += 1
#~     if @_count >= @wait
#~       @_index += 1
#~       @_index %= @gif.count
#~       self.bitmap = @gif.bitmap.at(@_index)
#~       @_count = 0 
#~     end
#~   end
#~ end

#~ #end # // unless SARA::DEBUG
