# Window::Equip
#==============================================================================#
# ♥ Window::Equip
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/30/2011
# // • Data Modified : 12/30/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/30/2011 V1.0
#
#==============================================================================#
class Window::Equip < Window::Selectable
  def initialize( x, y, w=nil, h=nil )
    w ||= window_width
    h ||= window_height
    super(x,y,w,h)
    select(0)
  end  
  attr_reader :battler
  def battler=( new_battler )
    return if @battler == new_battler
    set_battler(new_battler)
    refresh()
  end 
  def set_battler(n)
    @battler = n
  end
  def actor
    battler
  end
  def actor=( new_actor )
    self.battler = new_actor
  end 
  def set_actor(n)
    set_battler(n)
  end
  def col_max
    2
  end  
  def spacing 
    return 2
  end  
  def item_width
    return 96+24+4
  end  
  def item_height
    return 38+4
  end 
  def window_height
    adjust_h4window(item_height * 4)
  end
  def window_width
    return ((item_width) * col_max) + (standard_padding * 2) + (spacing * 2)
  end
  def draw_item( index )
    rect = item_rect( index ).contract( 2 )
    @artist.draw_actor_equip_at_ex(@battler, index, rect, enable?(index))
  end 
  def item(index=self.index)
    @battler ? @battler.equips[index] : nil
  end  
  #--------------------------------------------------------------------------
  # ● フレーム更新
  #--------------------------------------------------------------------------
  def update
    super
    @item_window.slot_id = index if @item_window
  end
  #--------------------------------------------------------------------------
  # ● 項目数の取得
  #--------------------------------------------------------------------------
  def item_max
    @battler ? @battler.equip_slots.size : 0
  end
  #--------------------------------------------------------------------------
  # ● 装備スロットを許可状態で表示するかどうか
  #--------------------------------------------------------------------------
  def enable?(index)
    @battler ? @battler.equip_change_ok?(index) : false
  end
  #--------------------------------------------------------------------------
  # ● 選択項目の有効状態を取得
  #--------------------------------------------------------------------------
  def current_item_enabled?
    enable?(index)
  end
  #--------------------------------------------------------------------------
  # ● ステータスウィンドウの設定
  #--------------------------------------------------------------------------
  def status_window=(status_window)
    @status_window = status_window
    call_update_help
  end
  #--------------------------------------------------------------------------
  # ● アイテムウィンドウの設定
  #--------------------------------------------------------------------------
  def item_window=(item_window)
    @item_window = item_window
    update
  end
  #--------------------------------------------------------------------------
  # ● ヘルプテキスト更新
  #--------------------------------------------------------------------------
  def update_help
    super
    @help_window.set_item(item) 
  end
  def equip_current(item)
    Sound.play_equip
    @battler.change_equip(self.index,item)
    refresh4change()
  end  
  def unequip_current()
    equip_current(nil)
  end  
  def optimize_equipments()
    Sound.play_equip
    @battler.optimize_equipments()
    refresh4change()
  end  
  def clear_equipments()
    @battler.clear_equipments()
    refresh4change()
  end  
  def refresh4change()
    refresh()
    @status_window.refresh if @status_window
    @item_window.refresh() if @item_window
    call_update_help()
  end  
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
