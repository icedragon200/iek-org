=begin
 ♥ IER - Map Merge
 ────────────────────────────────────────────────────────────────────────────── 
 • Created By    : IceDragon
 • Data Created  : 06/02/2012
 • Data Modified : 06/03/2012
 • Version       : 0x10000
 ──────────────────────────────────────────────────────────────────────────────
 ─┐ ● Requirements ┌───────────────────────────────────────────────────────────
  └────────────────┘ 
  This script requires the RGSS3-MACL
 
 ─┐ ● Introduction ┌───────────────────────────────────────────────────────────
  └────────────────┘ 
  Welcome to IER - Map Merge.
  If you are familiar with the original IEX version for RMVX, then you probably
  dont need to read this introduction.
  Map Merge, is a designed to take multiple maps, and merge/paint them unto
  another map.
  While reading the instructions you will encounter certain words, here
  are there meanings in this context:
    painting/merging
      This process of replacing data on a map using another
    canvas map 
      This refers to the map you are currently working with.
    paint map
      This refers to the map that will be painted/merged with the canvas  
  Happy merging!
      
 ─┐ ● Instruction Manual ┌─────────────────────────────────────────────────────
  └──────────────────────┘ 
  Read the Reference Manual.
  Place the notetags in their appropriate places.
  Due to the nature of map merging, a map will show changes, but
  the map may appear jumbled, or dioriented.
  You will have to call 'reset_merge_map' after a switch
  change for the proper effects to take place.
  NOTE:
    Events will not be copied from the paint maps.
 ─┐ ● Reference Manual ┌───────────────────────────────────────────────────────
  └────────────────────┘ 
  functions (Game_Interpreter)
    reset_merge_map
  
  Notetags 
    Map
      <mmp> or <map merge>
        merge: id, id, id
          This is an essential tag for the mmp.
          This designates the maps that will be merged with canvas map.
          Note: paint maps will also undergo canvas merging
            this will allow you to multi merge.
            WARNING:
              DO NOT LOOP MERGE: Do not have maps that merge their parent. 
        switch: id-state
          A switch associated with the current merge map set.
          You can use multiple switches to control the map.
        layers: x, x, x
          Enables x layer.
          When this tag is used, you can select certain layers of the
          mentioned merge maps to paint unto the current.
          By default, all layers are painted.
        xy: x, y
          You can set a location on the canvas map to start painting.
          If the map ends up bigger than the canvas, it will wrap around.
      </mmp> or </map merge>
    EG:
      <mmp>
        merge: 2
        switch: 1-ON
        layers: 3, 4
        xy: 10, 10  
      </mmp>
  
 ─┐ ● Change Log ┌─────────────────────────────────────────────────────────────
  └──────────────┘ 
  (MM/DD/YYYY)
   06/03/2012 - 0x10000 
     Ported IEX - Map Merge
 ──────────────────────────────────────────────────────────────────────────────
=end 
($imported||={})['IER::MapMerge']=0x10000
raise "This script requires the RGSS3-MACL" unless $imported["RGSS3-MACL"]
raise "The current RGSS3-MACL is incompatible with this script" if $imported["RGSS3-MACL"] < 0x10000
# ╒╕ ■                                                        IER::MapMerge ╒╕
# └┴────────────────────────────────────────────────────────────────────────┴┘
module IER
  module MapMerge
    # // Ignore these tiles during merge
    DEFAULT_VOID_TILES = [0,1544]
    # // Disable all Merging, set to nil to ignore switch
    DISABLE_MERGE_SWITCH = 10
  end
  module Regexp
    module MapMerge
      MERGE  = /merge:\s*(\d+(?:\s*,\s*\d+)*)/i
      SWITCH = /switch:\s*(\d+)-(.*)/i
      XY     = /(?:pos|xy):\s*(\d+)\s*,\s*(\d+)/i
      LAYERS = /layers:\s*([1-4](?:\s*,\s*[1-4]){0,3})/i
    end
  end
  module MapMerge
    NOTE_PARSER = Chitat.new "(?:mpm|map[ _]?merge)" do |chi|
      chi.set_tag :merge , IER::Regexp::MapMerge::MERGE
      chi.set_tag :switch, IER::Regexp::MapMerge::SWITCH
      chi.set_tag :xy    , IER::Regexp::MapMerge::XY
      chi.set_tag :layers, IER::Regexp::MapMerge::LAYERS
    end  
  end
end
# ╒╕ ♥                                                             RPG::Map ╒╕
# └┴────────────────────────────────────────────────────────────────────────┴┘
class RPG::Map
# ╒╕ ♥                                                      Struct_MergeMap ╒╕
# └┴────────────────────────────────────────────────────────────────────────┴┘
  class Struct_MergeMap 
    attr_reader :switches, :map_ids, :layers, :xy
    def initialize
      @switches   = {}
      @map_ids    = []
      @layers     = [0,1,2,3]
      @xy = Point.new(0,0)
    end
    def parsers
      MACL::Parsers
    end
    def add_map_ids *ids
      @map_ids |= ids
    end
    def set_switch id, bool
      @switches[id] = bool
    end
    def set_layers *ids
      @layers = []
      ids.each do |id| @layers << id end
      @layers.uniq!
      @layers.sort!
      self
    end
    def set_xy x=0, y=0
      @xy.set x,y
    end
    def active_layers
      @layers
    end
    def read_chitat_tags *tags
      tags.each do |tag|
        case tag.sym
        when :merge 
          add_map_ids *parsers.str2int_a(tag.param(1))
        when :switch 
          set_switch parsers.str2int(tag.param(1)), parsers.str2bool(tag.param(2))
        when :xy
          set_xy *parsers.str2int(tag.params[1,2])
        when :layers
          set_layers *parsers.str2int_a(tag.param(1)).map!(&:pred)
        end
      end
    end
    def conditions_met?
      @switches.all? do |(key,value)| $game_switches[key] == value end
    end
  end
  alias :ier_mpm_note_scan :note_scan
  def note_scan
    ier_mpm_note_scan
    merge_maps
  end
  def merge_maps
    unless @merge_maps
      stacks = IER::MapMerge::NOTE_PARSER.parse_str4tags(@note)
      @merge_maps = stacks.collect do |tag_stack|
        mpm = Struct_MergeMap.new 
        mpm.read_chitat_tags *tag_stack
        mpm
      end
    end
    @merge_maps  
  end
  def merge_maps_active
    merge_maps.select &:conditions_met?
  end
  def do_merge_map
    mpms = merge_maps_active
    #puts "Merging %s" % mpms.inspect
    mpms.each do |mpm|
      ids = mpm.map_ids
      ids.each do |id|
        #puts "Merging Map %d" % id
        merge_map mpm.xy, MapManager.load_map(id).do_merge_map, mpm.active_layers
      end  
    end
    self
  end
  def merge_map point, map, active_layers=[0,1,2,3] 
    map = MapManager.load_map(map) if map.is_a?(Integer)
    ix,iy = point.to_a # // Insert X, Y
    w,h = map.width,map.height
    # // 3 Dimensional Loop
    x,y,z,tid = [0]*4
    voids = IER::MapMerge::DEFAULT_VOID_TILES
    for y in 0...h
      for x in 0...w
        for z in active_layers
          tid = map.data[x,y,z]
          next if voids.include? tid
          @data[ix+x,iy+y,z] = tid
        end  
      end
    end
  end
end
# ╒╕ ♥                                                             Game_Map ╒╕
# └┴────────────────────────────────────────────────────────────────────────┴┘
class Game_Map
  alias :ier_mpm_post_load_map :post_load_map
  def post_load_map
    ier_mpm_post_load_map
    remerge_maps
  end
  def remerge_maps
    @map_merged_st = false
    @map.data = MapManager.load_map(@map_id).data
    swid = IER::MapMerge::DISABLE_MERGE_SWITCH
    do_merge_map unless swid and $game_switches.on?(swid)
  end
  def do_merge_map
    return if @map_merged_st
    @map.do_merge_map
    @map_merged_st = true
  end
end
# ╒╕ ♥                                                     Game_Interpreter ╒╕
# └┴────────────────────────────────────────────────────────────────────────┴┘
class Game_Interpreter
  def reset_map_merge(time=60)
    Graphics.freeze
    $game_map.remerge_maps
    SceneManager.recall
  end
end
# ┌┬────────────────────────────────────────────────────────────────────────┬┐
# ╘╛ ● End of File ●                                                        ╘╛