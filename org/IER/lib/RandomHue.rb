=begin
 ♥ IER - Random Hue
 ────────────────────────────────────────────────────────────────────────────── 
 • Created By    : IceDragon
 • Data Created  : 06/02/2012
 • Data Modified : 06/03/2012
 • Version       : 0x10000
 ──────────────────────────────────────────────────────────────────────────────
 ─┐ ● Requirements ┌───────────────────────────────────────────────────────────
  └────────────────┘ 
  This script requires the RGSS3-MACL
 
 ─┐ ● Introduction ┌───────────────────────────────────────────────────────────
  └────────────────┘ 
  Welcome to IER - Random Hue.
  Introducing a rather old script, the ICY/IEX - Random Hue.
  Most users would remember this for GTBS.
  This script allows you to set a variable/random hue
  for battlers ingame, that way you can have the
  same enemy, but different hues (color,shades)
 ─┐ ● Instruction Manual ┌─────────────────────────────────────────────────────
  └──────────────────────┘ 
  Place all notetags in their appropriate noteboxes.
  NOTE:
    hueset names are case sensitive.
    'default24' is not the same as 'Default24'
  NOTE:
    Even if you call set_random_hue during battle, it wont
    make any changes, since the sprite has to reload
    the bitmap for changes to take place.
 ─┐ ● Reference Manual ┌───────────────────────────────────────────────────────
  └────────────────────┘ 
  functions (Game_Enemy)
    set_random_hue
  
  Notetags   
    Enemy
      <randomhueset: n>, <random hueset: n> <random_hueset: n>
      EG:
        <random hueset: default24>
        
 ─┐ ● Change Log ┌─────────────────────────────────────────────────────────────
  └──────────────┘ 
  (MM/DD/YYYY)
   06/14/2012 - 0x10000 
     Ported IEX - Random Enemy Hue
 ──────────────────────────────────────────────────────────────────────────────
=end 
($imported||={})['IER::RandomHue']=0x10000
raise "This script requires the RGSS3-MACL" unless $imported["RGSS3-MACL"]
raise "The current RGSS3-MACL is incompatible with this script" if $imported["RGSS3-MACL"] < 0x10000
# ╒╕ ■                                                       IER::RandomHue ╒╕
# └┴────────────────────────────────────────────────────────────────────────┴┘
module IER
  module RandomHue
    HUES = {}
    HUES['none']      = [] # // Do not change this
    # // 16 Hues
    HUES['default16'] = 16.times.collect do |i| 256 / 16 * i end
    # // 24 Hues
    HUES['default24'] = 24.times.collect do |i| 256 / 24 * i end
    # // 32 Hues
    HUES['default32'] = 32.times.collect do |i| 256 / 32 * i end
    # // 64 Hues
    HUES['default64'] = 64.times.collect do |i| 256 / 64 * i end
    # // Missing Hues
    HUES.default = HUES['none']
  end
end
# ╒╕ ■                                                           Game_Enemy ╒╕
# └┴────────────────────────────────────────────────────────────────────────┴┘
class RPG::Enemy
  def hue_set_name
    @hue_set_name ||= (@note.match(/<random[\s_]?hueset:\s(.+)>/i)||[nil,'none'])[1]
  end
  def hue_set
    IER::RandomHue::HUES[@hue_set_name]
  end
  def random_hue?
    !hue_set.empty?
  end
end
# ╒╕ ■                                                           Game_Enemy ╒╕
# └┴────────────────────────────────────────────────────────────────────────┴┘
class Game_Enemy
  alias ier_rndhue_initialize initialize
  def initialize *args,&block
    ier_rndhue_initialize *args,&block
    set_random_hue
  end
  def set_random_hue
    return unless enemy.random_hue?
    @battler_hue = enemy.hue_set.sample
  end
end
# ┌┬────────────────────────────────────────────────────────────────────────┬┐
# ╘╛ ● End of File ●                                                        ╘╛