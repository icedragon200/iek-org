#-unlessdef MACLREQ_VER
#-define MACLREQ_VER#=0x10000
#-endif
raise "This script requires the RGSS3-MACL" unless $imported["RGSS3-MACL"]
raise "The current RGSS3-MACL is incompatible with this script" if $imported["RGSS3-MACL"] < MACLREQ_VER