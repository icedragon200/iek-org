# ~ICY_Recruitment-System
#------------------------------------------------------------------------------#
#Recruitment System V 1.2a
#Created by : IceDragon
#Script-Status : GO!
#Date Started : 3/12/2010
#Date Last Modified : 7/17/2010
#Date Completed : 3/24/2010
#Date Released : 3/24/2010
#Difficulty : Medium
#------------------------------------------------------------------------------#
=begin

REALEASE LOG--------------
--------
7/17/2010 = V1.2a Dumped all my Window Scripts into it
3/24/2010 = V1.0 Finished Script >_<

In order to use the script you must configure it below!
To call the script
USE
$scene = Scene_Recruit.new
=end
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
#-#-#-#-#START CUSTOMIZATION#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
#THIS IS NOT JUST SOME PLUG 'n' PLAY or COPY 'n' PASTE
#Configure 'IT'
module ICY 
  module RCS 

#Character limit for naming
NAME_CHAR_LIMIT = 9

#Opacity for inactive windows
OPAC = 160

#Basic Recruit Cost
RECCOST = 1000

#There can also be sub charges such as Type, Gender or Class Costs
#Enable other costs, such as class cost
EXTRACOSTS = true 
#Type Cost enabled
T_Cost = true
#Gender Cost enabled
G_Cost = true
#Class Cost enabled
C_Cost = true

#These are the recruits. An array of actor IDS.
    RECRUITS = [2, 3, 4, 5, 6, 7, 8]

    
#These are sub catergories fo sorting purposes   
     TYPES  = ["Human"]              
    GENDERS = ["Male","Female"]
    
#Icons for types     
    TYPESCON = {
    "Human"  => 137,
    }#Don't touch this
    
#Type Costs $
#Money makes this system go round if you enable this feature
    TYPESCOST = {
    "Human"  => 100,
    }#Don't touch this
    
#Icons for Genders   
    GENDERSCON = {
    "Male"   => 2,
    "Female" => 9,
    }#Don't touch this

#Icons for Genders 
#Money makes this system go round if you enable this feature
    GENDERSCOST = {
    "Male"   => 200,
    "Female" => 200,
    }#Don't touch this
    
#Recruit info follows    
#Class_ID =>[type, gender]
#OR
#Class_ID =>[gender, type]
#It doesn't really matter how you put in the array just don't have it twice!
    RECRUIT_INFO = {
    0 => ["Unknown","Unknown"],#NULL ACTOR
    2 => ["Human","Female"],
    3 => ["Human","Male"],
    4 => ["Human","Female"],
    5 => ["Human","Male"],
    6 => ["Human","Male"],
    7 => ["Human","Female"],
    8 => ["Human","Male"],
            }#Don't touch this
               
#Recruitment class type IDS
#This will be used for the selectable classes
#These are the classes that can be assigned to the selected recruit
#Note that the Recruits will "NOT" be filtered by classes. 
    REC_CLASSES = [1, 2, 3, 4, 5, 6, 7, 8]
    
#Class icons
 REC_CLASS_CONS = {
   0  =>  176,  # Nil Class
   1  =>  32,   # Paladin(Knight)
   2  =>  6,    # Warrior
   3  =>  128,  # Priest
   4  =>  119,  # Magician
   5  =>  3,    # Knight(Paladin)
   6  =>  136,  # Dark Knight
   7  =>  132,  # Grappler
   8  =>  39,   # Thief
    }#Don't touch this 
    
#Class Costs $ 
#Money makes this system go round if you enable this feature
 REC_CLASS_COST = {
 0  => "Free", #NO Cost#Don't touch this 
 1 => 300,
 2 => 600,
 3 => 500,
 4 => 500,
 5 => 600,
 6 => 800,
 7 => 500,
 8 => 300,

 }#Don't touch this 
 
#Menu commands icon
#These are the icons used by the commands inside the recruitment window
#Recruit Icon
RECUCON  = 152
#Setup Icon
SETUPCON = 155
#Leave Icon
LEAVECON = 157
#Tab Switch SE
TABSND = "Book"

end#END RCS-DON'T TOUCH

  module RCS_HELPS#DON'T TOUCH
    
    #Texts used for help
    #Help for types\races
    RACEHELP = {
    "Human" => "Simple everday human",
    }#Don't touch this
    #Help for Genders
    GENDERHELP = {
    "Male"   => "Male",
    "Female" => "Female",
    }#Don't touch this 
    #Help for Classes
    CLASSHELP = {
    0 => "Unknown",
    1 => "Holy Knights",
    2 => "High Attack Class",
    3 => "Holy Healing Class",
    4 => "Magic Users",
    5 => "Guards of justice",
    6 => "Dark users",
    7 => "Masters of Barehand",
    8 => "Stealth Class"
    }#Don't touch this 
    
  end#END RCS_HELPS-DON'T TOUCH
  
  
  module RCS_Vocab#DON'T TOUCH
    
    #Text used for recruitment system
    #Text to be used when setting up recruit
    SETTEXT = "Set_Up"
    
    #Text to be used when recruiting
    RECTEXT = "Recruit"
    
    #Text to be used when cancelling recruiting
    CANCELTEXT = "Quit Recruiting"
    
    #Text to be used for the types
    TYPEWIN = "Race"
    
    #Text to be used for genders
    GENWIN = "Gender"
    
    #Text to be used for classes
    CLSWIN = "Class"
    
    #Text used in confirmation window
    CONFIRM = "Recruit!"
    
    #Icon for Confirm
    CONFCON = 1468
    
    #Text used in confirmation window
    CANCEL = "....."
    
    #Icon for Cancel
    CANCCON = 115
    
    #Text used for total cost
    TOTACOST = "Total Fee"
    
    #Text used for recruit cost
    RECCOSTTEXT = "Recruit Fee"
    
  end#END RCS_Vocab-DON'T TOUCH
  
 module RCS_Fonts#DON'T TOUCH
   
   #Font Control
   #Default Font Size
   FONTSIZE = 16
   
   #Font used for list
   DEFAULTFONT = "Agency FB"
   
   #List Font Size
   SELFONTSIZE = 24
   
   #Font used for list headers
   HEADERFONT = "Agency FB"
   
   #Header Font Size
   HEADERFONTSIZE = 24
   
 end#END RCS_Fonts-DON'T TOUCH
end#END ICY-DON'T TOUCH
################################################################################
#------------------------------------------------------------------------------#
#END\\\END\\\END\\\END\\\END\\\END\\\END///END///END///END///END///END///END///#
#------------------------------------------------------------------------------#
################################################################################
#==============================================================================
# ** ICY_Windows
#------------------------------------------------------------------------------
#  This Script houses all my Base Window scripts
#==============================================================================
=begin
Script 'O' Contents
ICY_W1.0  ICY_Window_Base
ICY_W1.1  ICY_Window_Selectable
ICY_W1.2  ICY_Window_Command
ICY_W1.3  ICY_Window_Help
ICY_W1.4  ICY_Window_NameEdit
ICY_W1.5  ICY_Window_NameInput
ICY_W1.6  ICY_Window_Item
ICY_W1.7  ICY_Window_Current_Items
ICY_W1.8  ICY_Window_Gold
ICY_W1.9  ICY_Window_ShopBuy
ICY_W1.10 ICY_Window_SaveFile
ICY_W1.11 ICY_Window_NameingActor
=end
#==============================================================================
# ** ICY_Gold
#------------------------------------------------------------------------------
#  This is the customizi part.
#==============================================================================
module ICY
  module ISS
    #This is the Icon used in my gold window by default it is 205
    Goldcon = 205
  end
end
$imported = {} if $imported == nil
$imported["ICY_Window_Systems"] = true
#ICY_W1.0
#==============================================================================
# ** ICY_Window_Base
#------------------------------------------------------------------------------
#  This is a class for custom windows.
#==============================================================================

class ICY_Window_Base < Window_Base
  #--------------------------------------------------------------------------
  # * Object Initialization
  #     x      : window x-coordinate
  #     y      : window y-coordinate
  #     width  : window width
  #     height : window height
  #   windfold : use a window folder?
  #   cuswin   : use a custom window?
  #--------------------------------------------------------------------------
  def initialize(x, y, width, height, fontsize = 21)
     super(x,y, width, height)
    self.z = 100
    self.back_opacity = 200
    self.openness = 255
    @fontsize = fontsize
    create_contents
    @opening = false
    @closing = false
  end
  
def icy_draw_text(tx, ty, twidth, theight, string, align = 0, font = "Arial Black")
  self.contents.font.name = font
  self.contents.font.size = @fontsize
  self.contents.draw_text(tx, ty, twidth, theight, string, align)
end

end
#ICY_W1.1
#==============================================================================
# ** ICY_Window_Selectable
#------------------------------------------------------------------------------
#  This window contains cursor movement and scroll functions.
#==============================================================================

class ICY_Window_Selectable < Window_Selectable
  #--------------------------------------------------------------------------
  # * Public Instance Variables
  #--------------------------------------------------------------------------
  attr_reader   :item_max                 # item count
  attr_reader   :column_max               # digit count
  attr_reader   :index                    # cursor position
  attr_reader   :help_window              # help window
  #--------------------------------------------------------------------------
  # * Object Initialization
  #     x       : window X coordinate
  #     y       : window Y coordinate
  #     width   : window width
  #     height  : window height
  #     spacing : width of empty space when items are arranged horizontally
  #--------------------------------------------------------------------------
  def initialize(x, y, width, height, columns, spacing = 32)
    super(x, y, width, height, spacing)
    @column_max = columns
  end
  
end
#ICY_W1.2
#==============================================================================
# ** ICY_Window_Command
#------------------------------------------------------------------------------
#  This window deals with general command choices.
#==============================================================================

class ICY_Window_Command < Window_Selectable
  #--------------------------------------------------------------------------
  # * Public Instance Variables
  #--------------------------------------------------------------------------
  attr_reader   :commands                 # command
  #--------------------------------------------------------------------------
  # * Object Initialization
  #     width      : window width
  #     commands   : command string array
  #     commandcons: command icon array
  #     column_max : digit count (if 2 or more, horizontal selection)
  #     row_max    : row count (0: match command count)
  #     spacing    : blank space when items are arrange horizontally
  #--------------------------------------------------------------------------
  def initialize(width, commands, commandcons, fontsize = 18, column_max = 1, row_max = 0, spacing = 32)
    if row_max == 0
      row_max = (commands.size + column_max - 1) / column_max
    end
    super(0, 0, width, row_max * WLH + 32, spacing)
    @commands = commands
    @item_max = commands.size
    @column_max = column_max
    @commandcons = commandcons
    @fontsize = fontsize
    refresh
    self.index = 0
  end
  #--------------------------------------------------------------------------
  # * Refresh
  #--------------------------------------------------------------------------
  def refresh
   self.contents.clear
    for i in 0...@item_max
      command = @commands[i]
      icon = @commandcons[command]        
       icy_recdraw_item(i, icon)
    end
  end
  #--------------------------------------------------------------------------
  # * Draw Item
  #     index   : item number
  #     enabled : enabled flag. When false, draw semi-transparently.
  #--------------------------------------------------------------------------
  def icy_recdraw_item(index, icon_index, enabled = true)
    rect = item_rect(index)
    rect.x += 24
    rect.width -= 8
    
    if @column_max == 1
    draw_icon(icon_index, x, rect.y, enabled)
  else
    draw_icon(icon_index, (rect.x - 24), y, enabled)
  end
  
    self.contents.clear_rect(rect)
    self.contents.font.size = @fontsize
    self.contents.font.color = normal_color
    self.contents.font.color.alpha = enabled ? 255 : 128
    self.contents.draw_text(rect, @commands[index])
  end
end

#ICY_W1.3
#==============================================================================
# ** ICY_Window_Help
#------------------------------------------------------------------------------
#  This window shows explanations.
#==============================================================================

class ICY_Window_Help < Window_Base
  #--------------------------------------------------------------------------
  # * Object Initialization
  #--------------------------------------------------------------------------
  def initialize(x, y, width, height, fontsize = 18)
    super(x, y, width, height)
    @fontsize = fontsize
  end
  #--------------------------------------------------------------------------
  # * Set Text
  #  text  : character string displayed in window
  #  align : alignment (0..flush left, 1..center, 2..flush right)
  #--------------------------------------------------------------------------
  def set_text(text, align = 0)
    if text != @text or align != @align
      self.contents.clear
      self.contents.font.size = @fontsize
      self.contents.font.color = normal_color
      self.contents.draw_text(4, 0, self.width - 40, WLH, text, align)
      @text = text
      @align = align
    end
  end
end

#ICY_W1.4
#==============================================================================
# ** ICY_Window_NameEdit
#------------------------------------------------------------------------------
#  This window is used to edit an actor's name on the name input screen.
#==============================================================================

class ICY_Window_NameEdit < Window_Base
  #--------------------------------------------------------------------------
  # * Public Instance Variables
  #--------------------------------------------------------------------------
  attr_reader   :name                     # name
  attr_reader   :index                    # cursor position
  attr_reader   :max_char                 # maximum number of characters
  #--------------------------------------------------------------------------
  # * Object Initialization
  #     actor    : actor
  #     max_char : maximum number of characters
  #--------------------------------------------------------------------------
  def initialize(actor, max_char)
    super(20, 20, 480, 128)
    @actor = actor
    @name = actor.name
    @max_char = max_char
    name_array = @name.split(//)[0...@max_char]   # Fit within max length
    @name = ""
    for i in 0...name_array.size
      @name += name_array[i]
    end
    @default_name = @name
    @index = name_array.size
    self.active = false
    refresh
    update_cursor
  end
  #--------------------------------------------------------------------------
  # * Return to Default Name
  #--------------------------------------------------------------------------
  def restore_default
    @name = @default_name
    @index = @name.split(//).size
    refresh
    update_cursor
  end
  #--------------------------------------------------------------------------
  # * Add Text Character
  #     character : text character to be added
  #--------------------------------------------------------------------------
  def add(character)
    if @index < @max_char and character != ""
      @name += character
      @index += 1
      refresh
      update_cursor
    end
  end
  #--------------------------------------------------------------------------
  # * Delete Text Character
  #--------------------------------------------------------------------------
  def back
    if @index > 0
      name_array = @name.split(//)          # Delete one character
      @name = ""
      for i in 0...name_array.size-1
        @name += name_array[i]
      end
      @index -= 1
      refresh
      update_cursor
    end
  end
  #--------------------------------------------------------------------------
  # * Get rectangle for displaying items
  #     index : item number
  #--------------------------------------------------------------------------
  def item_rect(index)
    rect = Rect.new(0, 0, 0, 0)
    rect.x = 220 - (@max_char + 1) * 12 + index * 24
    rect.y = 36
    rect.width = 24
    rect.height = WLH
    return rect
  end
  #--------------------------------------------------------------------------
  # * Refresh
  #--------------------------------------------------------------------------
  def refresh
    self.contents.clear
    draw_actor_face(@actor, 0, 0)
    name_array = @name.split(//)
    for i in 0...@max_char
      c = name_array[i]
      c = '_' if c == nil
      self.contents.draw_text(item_rect(i), c, 1)
    end
  end
  #--------------------------------------------------------------------------
  # * Update cursor
  #--------------------------------------------------------------------------
  def update_cursor
    self.cursor_rect = item_rect(@index)
  end
  #--------------------------------------------------------------------------
  # * Frame Update
  #--------------------------------------------------------------------------
  def update
    super
    update_cursor
  end
end

#ICY_W1.5
#==============================================================================
# ** ICY_Window_NameInput
#------------------------------------------------------------------------------
#  This window is used to select text characters on the name input screen.
#==============================================================================

class ICY_Window_NameInput < Window_Base
  #--------------------------------------------------------------------------
  # * Text Character Table
  #--------------------------------------------------------------------------
  ENGLISH = [ 'A','B','C','D','E',  'F','G','H','I','J',
              'K','L','M','N','O',  'P','Q','R','S','T',
              'U','V','W','X','Y',  'Z',' ',' ',' ',' ',
              'a','b','c','d','e',  'f','g','h','i','j',
              'k','l','m','n','o',  'p','q','r','s','t',
              'u','v','w','x','y',  'z','_','-',' ',' ',
              ' ',' ',' ',' ',' ',  ' ',' ',' ',' ',' ',
              '1','2','3','4','5',  ' ',' ',' ',' ',' ',
              '6','7','8','9','0',  ' ',' ',' ',' ','OK']
  TABLE = [ENGLISH]
  #--------------------------------------------------------------------------
  # * Object Initialization
  #     mode : Defeault input mode (always 0 in English version)
  #--------------------------------------------------------------------------
  def initialize(mode = 0)
    super(20, 148, 386, 248)
    @mode = mode
    @index = 0
    refresh
    update_cursor
  end
  #--------------------------------------------------------------------------
  # * Text Character Acquisition
  #--------------------------------------------------------------------------
  def character
    if @index < 88
      return TABLE[@mode][@index]
    else
      return ""
    end
  end
  #--------------------------------------------------------------------------
  # * Determine Cursor Position: Mode Switch
  #--------------------------------------------------------------------------
  def is_mode_change
    return (@index == 88)
  end
  #--------------------------------------------------------------------------
  # * Determine Cursor Location: Confirmation
  #--------------------------------------------------------------------------
  def is_decision
    return (@index == 89)
  end
  #--------------------------------------------------------------------------
  # * Get rectangle for displaying items
  #     index : item number
  #--------------------------------------------------------------------------
  def item_rect(index)
    rect = Rect.new(0, 0, 0, 0)
    rect.x = index % 10 * 32 + index % 10 / 5 * 16
    rect.y = index / 10 * WLH
    rect.width = 32
    rect.height = WLH
    return rect
  end
  #--------------------------------------------------------------------------
  # * Refresh
  #--------------------------------------------------------------------------
  def refresh
    self.contents.clear
    for i in 0..89
      rect = item_rect(i)
      rect.x += 2
      rect.width -= 4
      self.contents.draw_text(rect, TABLE[@mode][i], 1)
    end
  end
  #--------------------------------------------------------------------------
  # * Update cursor
  #--------------------------------------------------------------------------
  def update_cursor
    self.cursor_rect = item_rect(@index)
  end
  #--------------------------------------------------------------------------
  # * Move cursor down
  #     wrap : Wraparound allowed
  #--------------------------------------------------------------------------
  def cursor_down(wrap)
    if @index < 80
      @index += 10
    elsif wrap
      @index -= 80
    end
  end
  #--------------------------------------------------------------------------
  # * Move cursor up
  #     wrap : Wraparound allowed
  #--------------------------------------------------------------------------
  def cursor_up(wrap)
    if @index >= 10
      @index -= 10
    elsif wrap
      @index += 80
    end
  end
  #--------------------------------------------------------------------------
  # * Move cursor right
  #     wrap : Wraparound allowed
  #--------------------------------------------------------------------------
  def cursor_right(wrap)
    if @index % 10 < 9
      @index += 1
    elsif wrap
      @index -= 9
    end
  end
  #--------------------------------------------------------------------------
  # * Move cursor left
  #     wrap : Wraparound allowed
  #--------------------------------------------------------------------------
  def cursor_left(wrap)
    if @index % 10 > 0
      @index -= 1
    elsif wrap
      @index += 9
    end
  end
  #--------------------------------------------------------------------------
  # * Move Cursor to [OK]
  #--------------------------------------------------------------------------
  def cursor_to_decision
    @index = 89
  end
  #--------------------------------------------------------------------------
  # * Move to Next Page
  #--------------------------------------------------------------------------
  def cursor_pagedown
    @mode = (@mode + 1) % TABLE.size
    refresh
  end
  #--------------------------------------------------------------------------
  # * Move to Previous Page
  #--------------------------------------------------------------------------
  def cursor_pageup
    @mode = (@mode + TABLE.size - 1) % TABLE.size
    refresh
  end
  #--------------------------------------------------------------------------
  # * Frame Update
  #--------------------------------------------------------------------------
  def update
    super
    last_mode = @mode
    last_index = @index
    if Input.repeat?(Input::DOWN)
      cursor_down(Input.trigger?(Input::DOWN))
    end
    if Input.repeat?(Input::UP)
      cursor_up(Input.trigger?(Input::UP))
    end
    if Input.repeat?(Input::RIGHT)
      cursor_right(Input.trigger?(Input::RIGHT))
    end
    if Input.repeat?(Input::LEFT)
      cursor_left(Input.trigger?(Input::LEFT))
    end
    if Input.trigger?(Input::A)
      cursor_to_decision
    end
    if Input.trigger?(Input::R)
      cursor_pagedown
    end
    if Input.trigger?(Input::L)
      cursor_pageup
    end
    if Input.trigger?(Input::C) and is_mode_change
      cursor_pagedown
    end
    if @index != last_index or @mode != last_mode
      Sound.play_cursor
    end
    update_cursor
  end
end

#ICY_W1.6
#==============================================================================
# ** ICY_Window_Item
#------------------------------------------------------------------------------
#  This window displays a list of inventory items for the item screen, etc.
#==============================================================================

class ICY_Window_Item < Window_Selectable
  #--------------------------------------------------------------------------
  # * Object Initialization
  #     x      : window x-coordinate
  #     y      : window y-coordinate
  #     width  : window width
  #     height : window height
  #--------------------------------------------------------------------------
  def initialize(x, y, width, height)
    super(x, y, width, height)
    @column_max = 1
    self.index = 0
    refresh
  end
  #--------------------------------------------------------------------------
  # * Get Item
  #--------------------------------------------------------------------------
  def item
    return @data[self.index]
  end
  #--------------------------------------------------------------------------
  # * Whether or not to include in item list
  #     item : item
  #--------------------------------------------------------------------------
  def include?(item)
    return false if item == nil
    if $game_temp.in_battle
      return false unless item.is_a?(RPG::Item)
    end
    return true
  end
  #--------------------------------------------------------------------------
  # * Whether or not to display in enabled state
  #     item : item
  #--------------------------------------------------------------------------
  def enable?(item)
    return $game_party.item_can_use?(item)
  end
  #--------------------------------------------------------------------------
  # * Refresh
  #--------------------------------------------------------------------------
  def refresh
    @data = []
    for item in $game_party.items
      next unless include?(item)
      @data.push(item)
      if item.is_a?(RPG::Item) and item.id == $game_party.last_item_id
        self.index = @data.size - 1
      end
    end
    @data.push(nil) if include?(nil)
    @item_max = @data.size
    create_contents
    for i in 0...@item_max
      draw_item(i)
    end
  end
  #--------------------------------------------------------------------------
  # * Draw Item
  #     index : item number
  #--------------------------------------------------------------------------
  def draw_item(index)
    rect = item_rect(index)
    self.contents.clear_rect(rect)
    item = @data[index]
    if item != nil
      number = $game_party.item_number(item)
      enabled = enable?(item)
      rect.width -= 4
      draw_item_name(item, rect.x, rect.y, enabled)
      self.contents.draw_text(rect, sprintf(":%2d", number), 2)
    end
  end
  #--------------------------------------------------------------------------
  # * Update Help Text
  #--------------------------------------------------------------------------
  def update_help
    @help_window.set_text(item == nil ? "" : item.description)
  end
end

#ICY_W1.7
#==============================================================================
# ** ICY_Window_Current_Items
#------------------------------------------------------------------------------
#  This window displays items in possession for selling on the shop screen.
#==============================================================================

class ICY_Window_CurrentItems < ICY_Window_Item
  #--------------------------------------------------------------------------
  # * Object Initialization
  #     x      : window x-coordinate
  #     y      : window y-coordinate
  #     width  : window width
  #     height : window height
  #--------------------------------------------------------------------------
  def initialize(x, y, width, height)
    super(x, y, width, height)
  end
  #--------------------------------------------------------------------------
  # * Whether or not to include in item list
  #     item : item
  #--------------------------------------------------------------------------
  def include?(item)
    return item != nil
  end
  #--------------------------------------------------------------------------
  # * Whether or not to display in enabled state
  #     item : item
  #--------------------------------------------------------------------------
  def enable?(item)
    return (item.price > 0)
  end
end

#ICY_W1.8

#==============================================================================
# ** ICY_Window_Gold
#------------------------------------------------------------------------------
#  This window displays the amount of gold.
#==============================================================================

class ICY_Window_Gold < Window_Base
  #--------------------------------------------------------------------------
  # * Object Initialization
  #     x : window X coordinate
  #     y : window Y coordinate
  #--------------------------------------------------------------------------
  def initialize(x, y)
    super(x, y, 160, WLH + 32)
    refresh
  end
  #--------------------------------------------------------------------------
  # * Refresh
  #--------------------------------------------------------------------------
  def refresh
    self.contents.clear
    draw_currency_value($game_party.gold, 4, 0, 120)
    draw_icon(ICY::ISS::Goldcon, 0, 0, true)
  end
end

#ICY_W1.9
#==============================================================================
# ** ICY_Window_ShopBuy
#------------------------------------------------------------------------------
#  This window displays buyable goods on the shop screen.
#==============================================================================

class ICY_Window_ShopBuy < ICY_Window_Selectable
  #--------------------------------------------------------------------------
  # * Object Initialization
  #     x : window X coordinate
  #     y : window Y coordinate
  #--------------------------------------------------------------------------
  def initialize(x, y, width, height, columns = 1, spacing = 32)
    super(x, y, width, height, columns)
    @shop_goods = $game_temp.shop_goods
    @columns = columns
    refresh
    self.index = 0
  end
  #--------------------------------------------------------------------------
  # * Get Item
  #--------------------------------------------------------------------------
  def item
    return @data[self.index]
  end
  #--------------------------------------------------------------------------
  # * Refresh
  #--------------------------------------------------------------------------
  def refresh
    @data = []
    for goods_item in @shop_goods
      case goods_item[0]
      when 0
        item = $data_items[goods_item[1]]
      when 1
        item = $data_weapons[goods_item[1]]
      when 2
        item = $data_armors[goods_item[1]]
      end
      if item != nil
        @data.push(item)
      end
    end
    @item_max = @data.size
    create_contents
    for i in 0...@item_max
      draw_item(i)
    end
  end
  #--------------------------------------------------------------------------
  # * Draw Item
  #     index : item number
  #--------------------------------------------------------------------------
  def draw_item(index)
    item = @data[index]
    number = $game_party.item_number(item)
    enabled = (item.price <= $game_party.gold and number < 99)
    rect = item_rect(index)
    self.contents.clear_rect(rect)
    draw_item_name(item, rect.x, rect.y, enabled)
    rect.width -= 4
    self.contents.draw_text(rect.x - 24, rect.y, rect.width, rect.height, item.price, 2)
    if @columns > 1
     draw_icon (ICY::ISS::Goldcon, rect.x + (rect.width - 24),rect.y, enabled)
     else 
    self.draw_icon (ICY::ISS::Goldcon, rect.width - 24, rect.y, enabled)
    end
  end
  #--------------------------------------------------------------------------
  # * Help Text Update
  #--------------------------------------------------------------------------
  def update_help
    @help_window.set_text(item == nil ? "" : item.description)
  end
end

#ICY_W1.10
#==============================================================================
# ** ICY_Window_SaveFile
#------------------------------------------------------------------------------
#  This window displays save files on the save and load screens.
#==============================================================================

class ICY_Window_SaveFile < Window_Base
  #--------------------------------------------------------------------------
  # * Public Instance Variables
  #--------------------------------------------------------------------------
  attr_reader   :filename                 # filename
  attr_reader   :file_exist               # file existence flag
  attr_reader   :time_stamp               # timestamp
  attr_reader   :selected                 # selected
  #--------------------------------------------------------------------------
  # * Object Initialization
  #     file_index : save file index (0-3)
  #     filename   : filename
  #--------------------------------------------------------------------------
  def initialize(file_index, filename)
    super(20, 124, 504, 286)
    @file_index = file_index
    @filename = filename
    load_gamedata
    refresh
    @selected = false
  end
  #--------------------------------------------------------------------------
  # * Load Partial Game Data
  #    By default, switches and variables are not used (for expansion use,
  #    such as displaying place names)
  #--------------------------------------------------------------------------
  def load_gamedata
    @time_stamp = Time.at(0)
    @file_exist = FileTest.exist?(@filename)
    if @file_exist
      file = File.open(@filename, "r")
      @time_stamp = file.mtime
      begin
        @characters     = Marshal.load(file)
        @frame_count    = Marshal.load(file)
        @last_bgm       = Marshal.load(file)
        @last_bgs       = Marshal.load(file)
        @game_system    = Marshal.load(file)
        @game_message   = Marshal.load(file)
        @game_switches  = Marshal.load(file)
        @game_variables = Marshal.load(file)
        
        @game_self_switches  = Marshal.load(file)
        @game_actors         = Marshal.load(file)
        @game_party          = Marshal.load(file)
        @game_troop          = Marshal.load(file)
        @game_map            = Marshal.load(file)
        @game_player         = Marshal.load(file)
    
        @total_sec = @frame_count / Graphics.frame_rate
        
        map_infos = load_data("Data/MapInfos.rvdata")
        mname = map_infos[@game_map.map_id].name.clone
        mname.gsub!(/\\N\[([0-9]+)\]/i) { $game_actors[$1.to_i].name }
        mname.gsub!(/\\PN\[([0-9]+)\]/i) { $game_party.members[$1.to_i].name }
        mname.gsub!(/\\V\[([0-9]+)\]/i) { $game_variables[$1.to_i] } 
        @mapp_name = mname
     rescue
        @file_exist = false
      ensure
        file.close
      end
    end
  end
  #--------------------------------------------------------------------------
  # * Refresh
  #--------------------------------------------------------------------------
  def refresh
    self.contents.clear
    self.contents.font.color = normal_color
    self.contents.font.size = 18
    name = Vocab::File + " #{@file_index + 1}"
    self.contents.draw_text(28, 0, 200, WLH, name)
    
    @name_width = contents.text_size(name).width + 24
    if @file_exist
      draw_icon(100,0, 0, true)
      draw_party_characters(24, 58)
      draw_playtime(0, 34, contents.width - 4, 2)
      draw_money(32, self.height - 64)
      draw_map_name(32, self.height - 128)
   if $imported["ICY_Clan_System"] == true
      draw_party_name(246, self.height - 96)
     end
    else
      draw_icon(98,0, 0, true)
      self.contents.font.size = 64
      self.contents.draw_text((self.width / 3), (self.height / 3), 300, 64, "NO DATA", 0)
    end
  end
  
  def draw_party_name(x, y)
    if $imported["ICY_Clan_System"] == true
    icon = @game_party.get_party_icon
    icon = icon.to_i
    draw_icon(icon, x - 24, y, true)    
    self.contents.draw_text(x, y, 200, WLH, "Party Name : #{@game_party.get_party_name}")
    end
  end
  
  def draw_path_chosen(x, y)
    self.contents.font.size = 18
    self.contents.font.color = normal_color
    draw_icon(149,x - 24, y, true)
    if @game_switches[511]
     self.contents.draw_text(x, y, 200, WLH, "Story : 'Male Story'") 
    elsif @game_switches[512]
      self.contents.draw_text(x, y, 200, WLH, "Story : 'Female Story'")
      else
      self.contents.draw_text(x, y, 200, WLH, "Story : 'Unknown'")
    end
    end
    
  def draw_map_name(x, y)
    draw_icon(2296, x - 24, y, true)
    self.contents.draw_text(x, y, 200, WLH, "Location : #{@mapp_name}")
  end
  
  def draw_money(x, y)
    draw_currency_value(@game_party.gold, x, y, 120)
    draw_icon(ICY::ISS::Goldcon, x - 24, y, true)
  end
  #--------------------------------------------------------------------------
  # * Draw Party Characters
  #     x : Draw spot X coordinate
  #     y : Draw spot Y coordinate
  #--------------------------------------------------------------------------
  def draw_party_characters(x, y)
    y_move = y 
    chars_count = 0
    i2 = 0
    iset = 0
    for i in 0...@characters.size
      i2 = i 
      i2 -= iset
      name = @characters[i][0]
      index = @characters[i][1]
      draw_character(name, index, x + i2 * 32, y_move)
      chars_count += 1
      if chars_count >= 5
      iset = i2 + 1
      y_move += 32
      end
    end
  end
  #--------------------------------------------------------------------------
  # * Draw Play Time
  #     x : Draw spot X coordinate
  #     y : Draw spot Y coordinate
  #     width : Width
  #     align : Alignment
  #--------------------------------------------------------------------------
  def draw_playtime(x, y, width, align)
    hour = @total_sec / 60 / 60
    min = @total_sec / 60 % 60
    sec = @total_sec % 60
    time_string = sprintf("%02d:%02d:%02d", hour, min, sec)
    self.contents.draw_text(x, y - 24 , width, WLH, "Play Time", 2)
    self.contents.font.color = ICY::Colors::LightGreen
    self.contents.draw_text(x, y, width, WLH, time_string, 2)
  end
  #--------------------------------------------------------------------------
  # * Set Selected
  #     selected : new selected (true = selected, false = unselected)
  #--------------------------------------------------------------------------
  def selected=(selected)
    @selected = selected
    update_cursor
  end
  #--------------------------------------------------------------------------
  # * Update cursor
  #--------------------------------------------------------------------------
  def update_cursor
    if @selected
      self.cursor_rect.set(0, 0, @name_width + 8, WLH)
    else
      self.cursor_rect.empty
    end
  end
end

class ICY_Window_Total_Saves < Window_Base
  
  #--------------------------------------------------------------------------
  # * Object Initialization
  #--------------------------------------------------------------------------
  def initialize(x, y, width, height, fontsize = 18)
    super(x, y, width, height)
    @fontsize = fontsize
  end
  #--------------------------------------------------------------------------
  # * Set Text
  #  text  : character string displayed in window
  #  align : alignment (0..flush left, 1..center, 2..flush right)
  #--------------------------------------------------------------------------
  def set_text(indexy, maxitw)
    if indexy != @indexy 
      self.contents.clear
      self.contents.font.size = @fontsize
      self.contents.font.color = normal_color
      self.contents.draw_text(4, 0, self.width - 40, WLH, "Save #{indexy + 1} out of #{maxitw}", 1)
      @indexy = indexy
    end
  end
  
end

#ICY_W1.11
#==============================================================================
# ** ICY_Window_NameingActor
#------------------------------------------------------------------------------
#  This window is used to show the actor currently being named.
#==============================================================================
class ICY_Window_NM_Actor < Window_Base
 
  def initialize
    super(406, 148, 128, 248)
    @actor = $game_actors[$game_temp.name_actor_id]
    @index = 0
    refresh
  end
  
  def refresh
    self.contents.clear
    self.contents.draw_text(5, 50, 64, 32,"Class:", 0)
    self.contents.draw_text(5, 82, 96, 32,@actor.class.name, 0)
    self.draw_character(@actor.character_name, @actor.character_index, 32, 48)
  end
  
end 

class ICY_Window_HelpEX < ICY_Window_Help
    
  def basic_setup
    set = $icy_rec["reclist"].retrieve
    @e1 = set.at(0)#Actor
    @e2 = set.at(1)#Type/Race
    @e3 = set.at(2)#Gender
    @e4 = set.at(3)#Class
    @e5 = set.at(4)#Is actor present
    @cost_calcu = 0
    
  if ICY::RCS::EXTRACOSTS
    if ICY::RCS::T_Cost == true
    type_costs  = ICY::RCS::TYPESCOST
    @type_cost  = type_costs[@e2]
    end
    if ICY::RCS::G_Cost == true
    gen_costs   = ICY::RCS::GENDERSCOST
     @gen_cost   = gen_costs[@e3]
     end
    if ICY::RCS::C_Cost == true
    class_costs = ICY::RCS::REC_CLASS_COST
    class_id    = $icy_rec["classwin"].class_grab
    @class_cost = class_costs[class_id.id]
    end    
  end
end

  def set_rec_text
    basic_setup
    main_y = 18
    main_x = 100
    
     self.contents.clear
     self.contents.font.size = (@fontsize + 1)
     self.contents.font.color = normal_color
  if @e5 
     self.contents.draw_text(0, 4, self.width - 40, WLH, @e1.name)
     self.contents.draw_text(main_x, main_y, self.width - 40, WLH, "#{ICY::RCS_Vocab::TYPEWIN}: #{@e2}")
     self.contents.draw_text(main_x, (main_y * 2), self.width - 40, WLH, "#{ICY::RCS_Vocab::GENWIN}: #{@e3}")
     self.contents.draw_text(main_x, (main_y * 3), self.width - 40, WLH, "#{ICY::RCS_Vocab::CLSWIN}: #{@e4}")
     self.contents.draw_text(main_x, (main_y * 4), self.width - 40, WLH, ICY::RCS_Vocab::RECCOSTTEXT)
     self.contents.font.color = ICY::Colors::LightCoral
     self.contents.draw_text(main_x, (main_y * 5), self.width - 40, WLH, ICY::RCS_Vocab::TOTACOST)
     self.contents.font.color = normal_color
     
     if ICY::RCS::EXTRACOSTS
       if ICY::RCS::T_Cost
         draw_currency_value(@type_cost, main_x * 2, main_y, 120)
         @cost_calcu += @type_cost
       end
       if ICY::RCS::G_Cost
        draw_currency_value(@gen_cost, main_x * 2, (main_y * 2), 120)
        @cost_calcu += @gen_cost
       end
       if ICY::RCS::C_Cost
        draw_currency_value(@class_cost, main_x * 2, (main_y * 3), 120)
        @cost_calcu += @class_cost
       end
      end
       draw_currency_value(ICY::RCS::RECCOST, main_x * 2, (main_y * 4), 120)
       @cost_calcu += ICY::RCS::RECCOST  
       draw_currency_value(@cost_calcu, main_x * 2, (main_y * 5), 120)
       
     self.draw_character(@e1.character_name, @e1.character_index, 32, 64)
     self.draw_icon(101, 0, (main_y * 4), true)
     self.contents.font.color = ICY::Colors::LightGreen
     self.contents.draw_text(24, (main_y * 4), self.width - 40, WLH, "LV : #{@e1.level}")
   else
     self.contents.font.color = ICY::Colors::Red
    self.contents.draw_text(0, 4, self.width - 40, WLH, "No One")
  end
 end
     
end

#==============================================================================
# ** Scene_Recruit
#------------------------------------------------------------------------------
#  This class brings up the recruitment scene.
#==============================================================================
class Scene_Recruit < Scene_Base
#------------------------------------------------------------------------------
# ** Perform_Transition
#------------------------------------------------------------------------------  
  def perform_transition
    Graphics.transition(30)
  end
#------------------------------------------------------------------------------
# ** Start
#------------------------------------------------------------------------------
  def start
    super
    create_menu_background
    create_systemstuff
    @gold_window = ICY_Window_Gold.new(346, 6)
    @help_window = ICY_Window_Help.new(272, 266, 254, 64)
    @recruit_window = Window_Base.new(0, 0, 320, 64)
    @recruit_window.contents.draw_text(25, 0, (@recruit_window.width / 2), 32,"Recruitment!",1)
    @recruit_window.draw_icon(2, 0, 0, true)
    create_list_windows
    @icy_win["statshelp"] = ICY_Window_HelpEX.new(180, @ji_y, (@jiwidth * 2), @jiheight)
    @help_window.set_text("......")
    create_headers
    create_confirms
    create_main_commands
    create_recruiting_windows
    set_visibility
    set_active
    set_help_windows
  end
  
  def create_systemstuff
    @switchinsnd = RPG::SE.new(ICY::RCS::TABSND)
    @win_slides = false
    @icy_wins = false
    @icy_stat = false
    @icy_rec = {}
    $icy_rec = {}
    @icy_header = {}
    @icy_win = {}
    @jiwidth = 180
    @jiheight = 160
    @ji_y = 96
    @windowgrap = ""
  end
  
  def create_list_windows
    @icy_rec["racewin"] = ICY_Recruit_ListWins.new(0, @ji_y, @jiwidth, @jiheight, ICY::RCS::TYPES, ICY::RCS_HELPS::RACEHELP, ICY::RCS::TYPESCON)
    @icy_rec["genwin"]  = ICY_Recruit_ListWins.new(180, @ji_y, @jiwidth, @jiheight,ICY::RCS::GENDERS, ICY::RCS_HELPS::GENDERHELP, ICY::RCS::GENDERSCON)
    $icy_rec["classwin"]= ICY_Rec_Classwin.new(360, @ji_y, @jiwidth, @jiheight)
    $icy_rec["reclist"] = ICY_Rec_List.new(0, @ji_y, @jiwidth, @jiheight)
  end
  
#------------------------------------------------------------------------------
# ** Set_Windows
#------------------------------------------------------------------------------  
  def set_help_windows
    $icy_rec["classwin"].help_window = @help_window
    @icy_rec["genwin"].help_window = @help_window
    @icy_rec["racewin"].help_window = @help_window 
    $icy_rec["reclist"].help_window = @icy_win["statshelp"]
  end
#------------------------------------------------------------------------------
# ** Set_Active_Windows
#------------------------------------------------------------------------------  
  def set_active
    #Active
    @icy_rec["racewin"].active = false 
    @icy_rec["genwin"].active = false
    $icy_rec["classwin"].active = false
    $icy_rec["reclist"].active = false
  end
#------------------------------------------------------------------------------
# ** Set_Visibility
#------------------------------------------------------------------------------  
  def set_visibility
    #Visible
    @icy_rec["racewin"].visible = false 
    @icy_rec["genwin"].visible = false
    $icy_rec["classwin"].visible = false
    $icy_rec["reclist"].visible = false
    @icy_header["race"].visible = false 
    @icy_header["gender"].visible = false  
    @icy_header["class"].visible = false
    @icy_win["statshelp"].visible = false
  end
#------------------------------------------------------------------------------
# ** Create_Recruit Commands
#------------------------------------------------------------------------------  
def create_recruiting_windows
   y1 = ICY::RCS_Vocab::CONFCON
   y2 = ICY::RCS_Vocab::CANCCON
   z1 = ICY::RCS_Vocab::CONFIRM
   z2 = ICY::RCS_Vocab::CANCEL
   concoso = {z1 => y1, z2 => y2}
   @icy_win["crecruit"] = ICY_Window_Command.new(254 , [z1, z2], concoso, ICY::RCS_Fonts::FONTSIZE, 2)
   @icy_win["crecruit"].x = 266
   @icy_win["crecruit"].y = 352
   @icy_win["crecruit"].active = false
   @icy_win["crecruit"].visible = false  
 end
 
#------------------------------------------------------------------------------
#** Main command window
#------------------------------------------------------------------------------
 def create_main_commands
   i1 = ICY::RCS::SETUPCON
   i2 = ICY::RCS::RECUCON
   i3 = ICY::RCS::LEAVECON
   s1 = ICY::RCS_Vocab::SETTEXT
   s2 = ICY::RCS_Vocab::RECTEXT
   s3 = ICY::RCS_Vocab::CANCELTEXT
   iconso = {s1 => i1, s2 => i2, s3 => i3}
   @storage_commands = ICY_Window_Command.new(254, [s1, s2, s3], iconso, ICY::RCS_Fonts::FONTSIZE)
   @storage_commands.x = 0
   @storage_commands.y = 266
   @storage_commands.help_window = @help_window
 end
#------------------------------------------------------------------------------
# ** Create_Confirm_Window
#------------------------------------------------------------------------------ 
 def create_confirms
   cp1 = "Yes recruit this"
   cp2 = "No don't recruit"
   @icy_win["confirm"] = Window_Command.new(382, [cp1, cp2], 2)
   @icy_win["confirm"].x = ((544 / 2)- (@icy_win["confirm"].width / 2))
   @icy_win["confirm"].y = (416 / 2)
   @icy_win["confirm"].active = false
   @icy_win["confirm"].visible = false
 end
#------------------------------------------------------------------------------
# ** Create_Headers
#------------------------------------------------------------------------------ 
  def create_headers
    @icy_header["race"] = ICY_Window_Base.new(16 , (@ji_y -48) , (@jiwidth -32), 64, ICY::RCS_Fonts::HEADERFONTSIZE)
    @icy_header["gender"] = ICY_Window_Base.new(196 , (@ji_y -48) , (@jiwidth -32), 64, ICY::RCS_Fonts::HEADERFONTSIZE)
    @icy_header["class"] = ICY_Window_Base.new(376 , (@ji_y -48) , (@jiwidth -32), 64, ICY::RCS_Fonts::HEADERFONTSIZE)
    @icy_header["race"].icy_draw_text(24 , 0, @jiwidth, 24, ICY::RCS_Vocab::TYPEWIN, 0, ICY::RCS_Fonts::HEADERFONT)
    @icy_header["gender"].icy_draw_text(24 , 0, @jiwidth, 24, ICY::RCS_Vocab::GENWIN, 0, ICY::RCS_Fonts::HEADERFONT)
    @icy_header["class"].icy_draw_text(24 , 0, @jiwidth, 24, ICY::RCS_Vocab::CLSWIN, 0, ICY::RCS_Fonts::HEADERFONT)
    @icy_header["race"].opacity = 0
    @icy_header["gender"].opacity = 0
    @icy_header["class"].opacity = 0 
  end
#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
# ** Update_Help
#------------------------------------------------------------------------------
 def update_help
   
   case @storage_commands.index
   when 0
      @help_window.set_text("Set up")
   when 1
      @help_window.set_text("Recruit")
   when 2
      @help_window.set_text(ICY::RCS_Vocab::CANCELTEXT)
      end
    end
#------------------------------------------------------------------------------
# ** Update Imagery
#------------------------------------------------------------------------------ 
  def update_imagery
     @icy_win["statshelp"].set_rec_text
   end
   
#------------------------------------------------------------------------------
# ** Update Help
#------------------------------------------------------------------------------  
  def update_confirmhelp
    case @icy_win["crecruit"].index
   when 0
      @help_window.set_text("Recruit this person")
   when 1
      @help_window.set_text("Quit Recruiting")
    end
  end
#------------------------------------------------------------------------------
# ** Command_Setup
#------------------------------------------------------------------------------ 
  def command_setup
    Sound.play_decision
    @storage_commands.opacity = ICY::RCS::OPAC 
    @storage_commands.active = false
    @icy_rec["racewin"].active = true 
    @icy_rec["genwin"].active = false
    $icy_rec["classwin"].active = false
    @icy_rec["racewin"].visible = true 
    @icy_rec["genwin"].visible = true
    $icy_rec["classwin"].visible = true
    @icy_header["race"].visible = true 
    @icy_header["gender"].visible = true  
    @icy_header["class"].visible = true
    @icy_wins = true
  end
#------------------------------------------------------------------------------
# ** Command_Recruit
#------------------------------------------------------------------------------
def command_recruit
  @icy_stat = true
  @storage_commands.opacity = ICY::RCS::OPAC 
  drace = @icy_rec["racewin"].rtc
  dgen  = @icy_rec["genwin"].rtc
  dclas = $icy_rec["classwin"].rtc
  $icy_rec["reclist"].getrecs(drace, dgen, dclas)
  $icy_rec["reclist"].refresh
  @storage_commands.active = false
  $icy_rec["reclist"].index = 0
  $icy_rec["reclist"].active = true
  $icy_rec["reclist"].visible = true
  @icy_win["crecruit"].active = true
  @icy_win["crecruit"].visible = true
  @icy_win["statshelp"].contents.clear
  @icy_win["statshelp"].set_rec_text
  @icy_win["statshelp"].visible = true
  @icy_win["confirm"].active = false
  @icy_win["confirm"].visible = false
end

#------------------------------------------------------------------------------
# ** Cancel Recruit
#------------------------------------------------------------------------------
 def no_rec
   Sound.play_cancel
   @icy_win["confirm"].active = false
   @icy_win["confirm"].visible = false
   @icy_win["crecruit"].active = true
   @icy_win["crecruit"].opacity = 255
  end
#------------------------------------------------------------------------------
# ** Recruit Go back
#------------------------------------------------------------------------------
def rec_go_back
    Sound.play_cancel
    @storage_commands.opacity = 255
    @icy_wins = false
    @icy_rec["racewin"].active = false 
    @icy_rec["genwin"].active = false
    $icy_rec["classwin"].active = false
    @icy_rec["racewin"].visible = false 
    @icy_rec["genwin"].visible = false
    $icy_rec["classwin"].visible = false
    @icy_header["race"].visible = false 
    @icy_header["gender"].visible = false  
    @icy_header["class"].visible = false
    @storage_commands.active = true
  end
  
#------------------------------------------------------------------------------
# ** Status Go back
#------------------------------------------------------------------------------
def stat_go_back
    Sound.play_cancel
    @storage_commands.opacity = 255
    @icy_stat = false
    $icy_rec["reclist"].active = false
    $icy_rec["reclist"].visible = false
    @icy_win["crecruit"].active = false
    @icy_win["crecruit"].visible = false
    @icy_win["statshelp"].visible = false
    @icy_win["confirm"].visible = false
    @icy_win["confirm"].active = false
    @storage_commands.active = true
end
#------------------------------------------------------------------------------
# ** Confirm Recruit
#------------------------------------------------------------------------------  
def command_takeon
   @icy_win["confirm"].active = true
   @icy_win["confirm"].visible = true
   @icy_win["crecruit"].active = false
   @icy_win["crecruit"].opacity = ICY::RCS::OPAC
end
#------------------------------------------------------------------------------
# ** Close_down
#------------------------------------------------------------------------------
def close_down
   Sound.play_cancel
   Graphics.freeze
   @gold_window.dispose
   @help_window.dispose
   @recruit_window.dispose
   @storage_commands.dispose
   $icy_rec["reclist"].dispose
   @icy_rec["racewin"].dispose 
   @icy_rec["genwin"].dispose
   $icy_rec["classwin"].dispose
   @icy_header["race"].dispose 
   @icy_header["gender"].dispose  
   @icy_header["class"].dispose
   @icy_win["crecruit"].dispose
   @icy_win["statshelp"].dispose
   @icy_win["confirm"].dispose
   $game_player.refresh
   $game_map.refresh
   $scene = Scene_Map.new
 end
 
#------------------------------------------------------------------------------
#------------------------------------------------------------------------------ 
#------------------------------------------------------------------------------
# ** Update
#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
def update
  super
  @icy_win["statshelp"].update
  @icy_rec["racewin"].update 
  $icy_rec["classwin"].update 
  @icy_rec["genwin"].update
  @storage_commands.update
  @icy_win["crecruit"].update
  @icy_win["confirm"].update
  $icy_rec["reclist"].update
#----------------------------------------------------------------------------
# ** Confirm
#----------------------------------------------------------------------------
  if @icy_win["confirm"].active and not @storage_commands.active
   update_help
    if Input.trigger?(Input::C) 
      case @icy_win["confirm"].index
      when 0
        final_setup
      when 1
        no_rec
      end
    end
	end
#----------------------------------------------------------------------------
# ** Main Command Window
#----------------------------------------------------------------------------
  if @storage_commands.active and not @icy_win["confirm"].active == true or not @icy_stat == true
   update_help
    if Input.trigger?(Input::C) 
      case @storage_commands.index
      when 0
        command_setup
      when 1
        command_recruit
      when 2
       close_down
      end
    end
#----------------------------------------------------------------------------
# ** Recruit Command Window
#----------------------------------------------------------------------------  
elsif @icy_win["crecruit"].active == true and not @storage_commands.active or not @icy_stat == true 
   if Input.trigger?(Input::C) 
    case @icy_win["crecruit"].index
      when 0
        command_takeon
      when 1
        stat_go_back
      end
    end
  end
  
#------------------------------------------------------------------------------
# ** Update Cancelling
#------------------------------------------------------------------------------
    if Input.trigger?(Input::B)
      if @icy_stat == true and not @icy_win["confirm"].active
        stat_go_back
      elsif @icy_wins == true and not @icy_win["confirm"].active
        rec_go_back
      elsif @icy_win["confirm"].active == true and not @storage_commands.active
        no_rec
      else
        close_down 
     end
   end
#------------------------------------------------------------------------------
# ** Update Commands
#------------------------------------------------------------------------------ 
  if @icy_stat
    if Input.repeat?(Input::UP) or Input.repeat?(Input::DOWN)
    update_imagery
    end
    update_confirmhelp
  end
  
  if @icy_wins
   if Input.trigger?(Input::RIGHT)
    @switchinsnd.play
    if @icy_rec["racewin"].active 
      @icy_rec["genwin"].active = true
      @icy_rec["racewin"].active = false   
    
  elsif @icy_rec["genwin"].active
     $icy_rec["classwin"].active = true
    @icy_rec["genwin"].active = false
  
  elsif $icy_rec["classwin"].active
    @icy_rec["racewin"].active = true
    $icy_rec["classwin"].active = false
    end
  end

  if Input.trigger?(Input::LEFT)
    @switchinsnd.play
    if @icy_rec["racewin"].active 
      $icy_rec["classwin"].active = true
      @icy_rec["racewin"].active = false   
    
  elsif @icy_rec["genwin"].active
     @icy_rec["racewin"].active = true
    @icy_rec["genwin"].active = false
  
  elsif $icy_rec["classwin"].active
    @icy_rec["genwin"].active = true
    $icy_rec["classwin"].active = false
    end
  end
  @help_window.update
end
  
end


#------------------------------------------------------------------------------
# ** Command_Recruit
#------------------------------------------------------------------------------
def cost_calculate
   set = $icy_rec["reclist"].retrieve
    @e1 = set.at(0)#Actor
    @e2 = set.at(1)#Type/Race
    @e3 = set.at(2)#Gender
    @e4 = set.at(3)#Class
    @e5 = set.at(4)#Is actor present
    @cost_calcu = 0
    
   if ICY::RCS::EXTRACOSTS
    type_costs  = ICY::RCS::TYPESCOST
    gen_costs   = ICY::RCS::GENDERSCOST
    class_costs = ICY::RCS::REC_CLASS_COST
    class_id    = $icy_rec["classwin"].class_grab
    @type_cost  = type_costs[@e2]
    @gen_cost   = gen_costs[@e3]
    @class_cost = class_costs[class_id.id]
       if ICY::RCS::T_Cost
        @cost_calcu += @type_cost
       end
       if ICY::RCS::G_Cost
        @cost_calcu += @gen_cost
       end
       if ICY::RCS::C_Cost
        @cost_calcu += @class_cost
      end
      
    end
      @cost_calcu += ICY::RCS::RECCOST 
      return @cost_calcu
end

def final_setup
  rec_cost = cost_calculate
  if rec_cost > $game_party.gold 
    Sound.play_buzzer
  else
  $game_party.lose_gold(@cost_calcu)
  set = $icy_rec["reclist"].retrieve
  actor_id = set.at(0)
  actor_id = actor_id.id
  class_id = $icy_rec["classwin"].class_grab
  class_id = class_id.id
  add_actor(actor_id) 
  change_class(actor_id, class_id)
  name_recruit(actor_id)
  end
end

def add_actor(actor_id) 
    $game_party.add_actor(actor_id)  
    $game_player.refresh
end
  
def change_class(actor_id, dclass_id)
  actor = $game_actors[actor_id]
  Sound.play_equip
  actor.class_id = dclass_id
end  

def name_recruit(actor_id)
   $game_temp.next_scene = "name"
   $game_temp.name_actor_id = actor_id
   $game_temp.name_max_char = ICY::RCS::NAME_CHAR_LIMIT
   $game_temp.next_scene = nil
   $scene = Scene_ICY_Name.new
end
      
def icy_change_level(actor_id)
    actor = $game_actors[actor_id]
    actor.change_level(actor.level + value, false)
end
   
end#End Main Class
################################################################################
#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
#==============================================================================
# ** ICY_Recruit_ListWins
#------------------------------------------------------------------------------
#  This window displays a list items formed from an array. It has icons and help.
#==============================================================================
class ICY_Recruit_ListWins < Window_Selectable
  
 def initialize(x, y, width, height, mainarray, helparray, iconarray)
    super(x, y, width, height)
    @back = Sprite.new
    self.index = 0
    self.contents = Bitmap.new(width-32, height-32)
    @column_max = 1
    @mainarray = mainarray
    @helparray = helparray
    @iconij = iconarray
    @item_max = @mainarray.size 
    refresh
  end
#------------------------------------------------------------------------------
# ** Refresh
#------------------------------------------------------------------------------   
  def refresh
    create_contents
    self.contents.clear
    
      @commands = @mainarray
      
      for i in 0...@commands.size
        command = @mainarray[i]
        @icon = @iconij[command]
        if @icon == nil
          @icon = @iconij[0]
        end
       icy_draw_item_name(i, @icon, 0)
        end
       @item_max = @commands.size
    end
    
     def icy_draw_item_name(index, icon_index, x, enabled = true)
      rect = item_rect(index)
      rect.x += 24
      rect.width -= 8
      draw_icon(icon_index, x, rect.y, enabled)
      self.contents.clear_rect(rect)
      self.contents.font.name = ICY::RCS_Fonts::DEFAULTFONT
      self.contents.font.size = ICY::RCS_Fonts::SELFONTSIZE
      self.contents.font.color = normal_color
      self.contents.font.color.alpha = enabled ? 255 : 128
      self.contents.draw_text(rect, @commands[index])
    end
    
    #Retrive current command 
  def rtc
    hindex = self.index
    rtcmd = @commands[hindex]
    return rtcmd
  end
  
      def update_help
       hindex = self.index
       nume = @mainarray[hindex]
       help = @helparray[nume]
       @help_window.set_text(help)
      end
end
################################################################################
#==============================================================================
# ** ICY_Rec_Classwin
#------------------------------------------------------------------------------
#  This window displays the classes.
#==============================================================================
class ICY_Rec_Classwin < Window_Selectable
  
 def initialize(x, y, width, height)
    super(x, y, width, height)
    @back = Sprite.new
    self.index = 0
    self.contents = Bitmap.new(width-32, height-32)
    @column_max = 1
    @classes = []
    @classnums = []
    @class = ICY::RCS::REC_CLASSES
    @classhelp = ICY::RCS_HELPS::CLASSHELP
    @item_max = @class.size 
    refresh
  end  
#------------------------------------------------------------------------------
# ** Refresh
#------------------------------------------------------------------------------   
  def refresh
    create_contents
    self.contents.clear
    
    for c in 0...@class.size
      @recla = @class.at(c)
      @iclas = $data_classes[@recla]
      @classnums[c] = @iclas
      @classes[c] = @iclas.name
    end
      @commands = @classes
      self.contents.clear
      @iconij = ICY::RCS::REC_CLASS_CONS
      
    for i in 0...@commands.size
        command = @class[i]
        @icon = @iconij[command]
        if @icon == nil
          @icon = @iconij[0]
        end
       icy_draw_item_name(i, @icon, 0)
        end
       @item_max = @commands.size
    end
    
     def icy_draw_item_name(index, icon_index, x, enabled = true)
      rect = item_rect(index)
      rect.x += 24
      rect.width -= 8
      draw_icon(icon_index, x, rect.y, enabled)
      self.contents.clear_rect(rect)
      self.contents.font.name = ICY::RCS_Fonts::DEFAULTFONT
      self.contents.font.size = ICY::RCS_Fonts::SELFONTSIZE
      self.contents.font.color = normal_color
      self.contents.font.color.alpha = enabled ? 255 : 128
      self.contents.draw_text(rect, @commands[index])
    end
    
    #Retrive current command
  def rtc
    hindex = self.index
    rtcmd = @commands[hindex]
    return rtcmd
  end
  
  def class_grab
    clsgrb = @classnums[self.index]
    return clsgrb
  end
  
      def update_help
       hindex = self.index
       declass = @class[hindex]
       help = @classhelp[declass]
       @help_window.set_text(help)
      end
    end
    
#==============================================================================
# ** ICY_Recruit_List
#------------------------------------------------------------------------------
#  This window displays a list of recruits.
#==============================================================================
class ICY_Rec_List < Window_Selectable
  #--------------------------------------------------------------------------
  # * Object Initialization
  #     x      : window x-coordinate
  #     y      : window y-coordinate
  #     width  : window width
  #     height : window height
  #--------------------------------------------------------------------------
  def initialize(x, y, width, height)
    super(x, y, width, height)
    @back = Sprite.new
    self.index = 0
    self.contents = Bitmap.new(width-32, height-32)
    @column_max = 1
    @stuff = ICY::RCS::RECRUITS
    @recinfo = ICY::RCS::RECRUIT_INFO
    @item_max = @stuff.size
    @actors = $game_party.members
  end
   
  def getrecs(drace, dgender ,dclass)
    @currentrace = drace
    @currentgender = dgender
    @assignclass = dclass
  end
#------------------------------------------------------------------------------
# ** Refresh
#------------------------------------------------------------------------------   
def refresh
    create_contents
    self.contents.clear
    item = []
    @commands = []
    @names = []
    @repcru = []
    @stuff = @stuff.compact
       
    for c in 0...@stuff.size
      recru = @stuff.at(c)
      arecru = $game_actors[recru]
      if @recinfo[recru].include?(@currentrace) and @recinfo[recru].include?(@currentgender) and not @actors.include?(arecru)
      @recui = @stuff.at(c)
      @inpi = $game_actors[@recui]
      @repcru[c] = @inpi
      @names[c] = @inpi.name
     end
   end
    if @names == nil
      @names["None"]
    end
    @names = @names.compact
    @repcru = @repcru.compact
    @item_max = @names.size
    @commands = @names
    self.contents = Bitmap.new(width - 32, height - 32) 
      self.contents.clear
      create_contents
      
      for i in 0...@commands.size
        command = @commands[i]
       icy_draw_item_name(i, 101, 0)
      end
      @item_max = @commands.size
    end
  
  def icy_draw_item_name(index, icon_index, x, enabled = true)
      rect = item_rect(index)
      rect.x += 24
      rect.width -= 8
      draw_icon(icon_index, x, rect.y, enabled)
      self.contents.clear_rect(rect)
      self.contents.font.name = ICY::RCS_Fonts::DEFAULTFONT
      self.contents.font.size = ICY::RCS_Fonts::SELFONTSIZE
      self.contents.font.color = normal_color
      self.contents.font.color.alpha = enabled ? 255 : 128
      self.contents.draw_text(rect, @commands[index])
    end
  
    def retrieve
     nui = @repcru.at(self.index)
    if nui != nil
      goed = true
    else
      goed = false
    end
     set = [nui, @currentrace, @currentgender, @assignclass, goed]
     set = set.compact
     return set
   end
   
end
################################################################################
#==============================================================================
# ** Scene_Name
#------------------------------------------------------------------------------
#  This class performs name input screen processing.
#==============================================================================

class Scene_ICY_Name < Scene_Base
  #--------------------------------------------------------------------------
  # * Start processing
  #--------------------------------------------------------------------------
  def start
    super
    @actor = $game_actors[$game_temp.name_actor_id]
    @edit_window = ICY_Window_NameEdit.new(@actor, $game_temp.name_max_char)
    @input_window = ICY_Window_NameInput.new
    @actor_window = ICY_Window_NM_Actor.new
  end
  #--------------------------------------------------------------------------
  # * Termination Processing
  #--------------------------------------------------------------------------
  def terminate
    super
    @edit_window.dispose
    @input_window.dispose
    @actor_window.dispose
  end
  
  #--------------------------------------------------------------------------
  # * Return to Original Screen
  #--------------------------------------------------------------------------
  def return_scene
   $scene = Scene_Recruit.new
  end
  #--------------------------------------------------------------------------
  # * Frame Update
  #--------------------------------------------------------------------------
  def update
    super
    update_menu_background
    @edit_window.update
    @input_window.update
    if Input.repeat?(Input::B)
      if @edit_window.index > 0             # Not at the left edge
        Sound.play_cancel
        @edit_window.back
      end
    elsif Input.trigger?(Input::C)
      if @input_window.is_decision          # If cursor is positioned on [OK]
        if @edit_window.name == ""          # If name is empty
          @edit_window.restore_default      # Return to default name
          if @edit_window.name == ""
            Sound.play_buzzer
          else
            Sound.play_decision
          end
        else
          Sound.play_decision
          @actor.name = @edit_window.name   # Change actor name
          return_scene
        end
      elsif @input_window.character != ""   # If text characters are not empty
        if @edit_window.index == @edit_window.max_char    # at the right edge
          Sound.play_buzzer
        else
          Sound.play_decision
          @edit_window.add(@input_window.character)       # Add text character
        end
      end
    end
  end
end  
################################################################################
#------------------------------------------------------------------------------#
#END\\\END\\\END\\\END\\\END\\\END\\\END///END///END///END///END///END///END///#
#------------------------------------------------------------------------------#
################################################################################
