# ~ICY_Variable Bars
#------------------------------------------------------------------------------#
#Event Variable Bars
#Created by : IceDragon
#Script-Status : Snippet
#Date Created : 4/24/2010
#Date Modified :
#Reference : Vampyr_HpBars, Modern Algebra_Hover Graphics
#------------------------------------------------------------------------------#
#How to use?
#In the event put this into a script...
#      icy_set_varbar(char_index, active, var, maxvalue, varbarname, mainbarz)
#char_index = 
#   -1 for player
#    0 for current event
#    1 and above for other events
#
#active = 
#this either true or false. If true the bar will be showed, if false 
#the bar is disposed
#
#var = 
# The Variable used for the bar...Put the ID of the variable
#
#maxvalue = 
# This is the maximum value of the bar
#
#varbarname =
# this is the name of the bar placed over the base bar..
# To get rid of the bars
#    icy_set_varbar(char_index, false)
#
#mainbarz = 
# this is the base bar placed behind the value bar(varbarname)
#
#
#EG icy_set_varbar(0, true, 2, 15, "SmlMainBarHP", "SmlMainBar")
#This would create a bar "SmlMainBar" with the value bar "SmlMainBarHP"
#It would be over the event it was called from
#Its maxvalue is 15
#And the variable ID would be 2


#Start customization
module ICY
  module EVENT_BARS
    #Default Bar Name
    MAINBAR = "SmlMainBar"
    #The actual bar thats placed over the main one..
    THEBAR = "SmlMainBarHP"
  end
end
#End customization
################################################################################
#------------------------------------------------------------------------------#
#END\\\END\\\END\\\END\\\END\\\END\\\END///END///END///END///END///END///END///#
#------------------------------------------------------------------------------#
################################################################################
class Game_Character
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # * Public Instance Variable
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  attr_accessor :icy_bar      # The base bar name
  attr_accessor :icy_hpbar    # The actual bar
  attr_accessor :icy_maxvar   # Max value
  attr_accessor :icy_varval   # Variable Value
  attr_accessor :icy_active
end
#==============================================================================
# ** Game Interpreter
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#==============================================================================

class Game_Interpreter
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # * Set Variable Bar
  #    char_index   : the index of the character
  #    hpbarname    : the base bar
  #    hpvar        : variable for hp.....
  #    maxvalue     : the maximum value of the bar
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  def icy_set_varbar(char_index = 0, active = false, hpvar = 0, maxvalue = 0, 
    varbarname = ICY::EVENT_BARS::THEBAR, mainbarz = ICY::EVENT_BARS::MAINBAR)
    # Get Character
    char_index = @event_id if char_index == 0
    target = char_index == -1 ? $game_player : $game_map.events[char_index]
    return if target == nil
    target.icy_bar = mainbarz
    target.icy_hpbar = varbarname
    target.icy_maxvar = maxvalue
    target.icy_varval = hpvar
    target.icy_active = active
  end
end
  
#==============================================================================
# ** Sprite Character
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#==============================================================================

class Sprite_Character
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # * Dispose
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  alias icy_hp_bar897_dispose dispose
  def dispose
    # Dispose hp_bar
    icy_dispose_hp_bar
    # Run Original Method
    icy_hp_bar897_dispose
  end
  
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # * Frame Update
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  alias icy_hp_event_bars980_update update
  def update
    # Run Original Method
    icy_hp_event_bars980_update
    # Update hp barz
    if @hover_sprite == nil && @character.icy_active != false
      icy_start_hp_bar(@character.icy_bar, @character.icy_hpbar,
                      @character.icy_varval, @character.icy_maxvar) 
    end
    icy_update_hp_bars
  end
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # * Start Hover Graphic
  #    picture_name : the name of the picture to float
  #    float        : boolean on whether picture should float
  #    colour       : the colour of the text
  #    size         : the size of the text
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  def icy_start_hp_bar (main_barname = nil, thebarname = nil, variablez = 0, maxvaluez = 0)
    return if main_barname == nil or thebarname == nil or variablez == nil or  maxvaluez == nil
    icy_dispose_hp_bar
    numvar = variablez    
    if numvar == nil
      numvar = 0
    end
    icyvarval = $game_variables[numvar]
    @hover_sprite = ::Sprite.new(viewport)
    begin
    @base = Cache.picture(main_barname)
    @hover_sprite.bitmap = Bitmap.new(@base.width, @base.height)
    bar = Cache.picture(@character.icy_hpbar)
    
    @hover_sprite.bitmap.blt(0, 0, @base, @base.rect)
    rect = Rect.new(0, 0, bar.width * icyvarval / maxvaluez, bar.height)
    @hover_sprite.bitmap.blt(0, 0, bar, rect)
    end
    @hover_sprite.x = self.x - (@hover_sprite.bitmap.width / 2)
    @hover_sprite.y = self.y - self.height - @hover_sprite.bitmap.height
    @hover_sprite.z = z + 200
    @icy_last_xy = [self.x, self.y] 
    @presentvalue = icyvarval
  end
  
  def icy_refresh
    return if @character.icy_active == false
    @hover_sprite.bitmap.clear
    if @character.icy_varval != nil
    newicyvarval = $game_variables[@character.icy_varval]
     else
    newicyvarval = 0
    end
    bar = Cache.picture(@character.icy_hpbar)
    @hover_sprite.bitmap.blt(0, 0, @base, @base.rect)
    rect = Rect.new(0, 0, bar.width * newicyvarval / @character.icy_maxvar, bar.height)
    @hover_sprite.bitmap.blt(0, 0, bar, rect)
    @hover_sprite.x = self.x - (@hover_sprite.bitmap.width / 2)
    @hover_sprite.y = self.y - self.height - @hover_sprite.bitmap.height
    @hover_sprite.z = z + 200
    @icy_last_xy = [self.x, self.y] 
  end
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # * Update Hover Graphic
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  def icy_update_hp_bars
    return if @hover_sprite == nil
    icy_refresh if something_changed?
    icy_dispose_hp_bar if @character.icy_active == false
    
  end
  
  def something_changed?
    return true if $game_variables[@character.icy_varval] != @presentvalue
    return true if @icy_last_xy != [self.x, self.y]
    return false
  end
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # * Dispose Hover Graphic
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  def icy_dispose_hp_bar
    if @hover_sprite != nil
      @hover_sprite.dispose
      @hover_sprite = nil
      @icy_last_xy = nil
    end
  end
end
################################################################################
#------------------------------------------------------------------------------#
#END\\\END\\\END\\\END\\\END\\\END\\\END///END///END///END///END///END///END///#
#------------------------------------------------------------------------------#
################################################################################
