# ~ICY_Scene_Skill
#------------------------------------------------------------------------------#
# ** ICY_Scene_Skill ReWrite
# ** Created by : IceDragon
# ** Script-Status : ReWrite
# ** Date Created : 3/31/2010
# ** Date Modified : 7/17/2010
#------------------------------------------------------------------------------#
$imported = {} if $imported == nil
$imported["ICY_HM_Style_Skill_Window"] = true
#------------------------------------------------------------------------------#
# ** Start Customization
#------------------------------------------------------------------------------#
module ICY
  module Skill_Window
    SKILL_WINDOW_SIZES = { # Don't touch this
#Don't touch this side < edit the array to the > [x, y, width, height, opacity],
    "Help" => [0, 112, 544, 86, 255],
    "Skills" => [0, 198, 544, 154, 255],
    "Header" => [0, 0, 544, 56, 255],
    "PartyStrip" => [0, 296, 544, 64, 255],
    } # Don't touch this
    MP_ICON = 100
    RECT_SIZE = 32
    BORDER_SIZE = 4
    ITEM_SQ_SPACING = 48
    SELECTION_SIZE = 48
    USE_BORDER_RECT = true
    NUM_OF_COLUMNS = 10
    SKILL_ICON_POS = [4, 4]
    MP_COST_POS = [0, 28]
    MP_COST_ALIGN = 0
  end
end
#------------------------------------------------------------------------------#
# ** End Customization
#------------------------------------------------------------------------------#
#==============================================================================
# ** ICY_Window_PartyStrip
#------------------------------------------------------------------------------
#  This window displays the party members in a strip
#==============================================================================
class ICY_Window_PartyStrip < Window_Base
  
  def initialize(x, y, width, height, actor)
    super(x, y, width, height)
    self.contents.clear
    draw_lead_actor(actor)
    draw_other_actors(actor)
  end
  
  def draw_character(character_name, character_index, x, y, enabled = true)
    return if character_name == nil
    bitmap = Cache.character(character_name)
    sign = character_name[/^[\!\$]./]
    if sign != nil and sign.include?('$')
      cw = bitmap.width / 3
      ch = bitmap.height / 4
    else
      cw = bitmap.width / 12
      ch = bitmap.height / 8
    end
    n = character_index
    src_rect = Rect.new((n%4*3+1)*cw, (n/4*4)*ch, cw, ch)
    self.contents.blt(x - cw / 2, y - ch, bitmap, src_rect, enabled ? 255 : 128)
  end
  
  def draw_lead_actor(lead)
    if lead != nil
      leadx = 32
      leady = 32
    draw_character(lead.character_name, lead.character_index, leadx, leady)
    end
  end
  
  def draw_other_actors(voider)
    if voider != nil
      pos_x = 96
      for actor in $game_party.members
        if actor.id != voider.id
        draw_character(actor.character_name, actor.character_index, pos_x, 32, false)
        pos_x += 32
         else
        next
      end
    end
  end
end
  
end

#==============================================================================
# ** Window_Skill
#------------------------------------------------------------------------------
#  This window displays a list of usable skills on the skill screen, etc.
#==============================================================================
class Window_Skill < Window_Selectable
  include ICY::Skill_Window
    attr_accessor :column_max
  #--------------------------------------------------------------------------
  # * Object Initialization
  #     x      : window x-coordinate
  #     y      : window y-coordinate
  #     width  : window width
  #     height : window height
  #     actor  : actor
  #--------------------------------------------------------------------------
  def initialize(x, y, width, height, actor)
    super(x, y, width, height)
    @actor = actor
    self.index = 0
    @column_max = NUM_OF_COLUMNS
    refresh
  end
  #--------------------------------------------------------------------------
  # * Get Skill
  #--------------------------------------------------------------------------
  def skill
    return @data[self.index]
  end
  #--------------------------------------------------------------------------
  # * Create Window Contents
  #--------------------------------------------------------------------------
  def create_contents
    self.contents.dispose
    self.contents = Bitmap.new(width - 32, [height - 32, row_max * ITEM_SQ_SPACING].max)
  end
  #--------------------------------------------------------------------------
  # * Refresh
  #--------------------------------------------------------------------------
  def refresh
    @data = []
    for skill in @actor.skills
      @data.push(skill)
      if skill.id == @actor.last_skill_id
        self.index = @data.size - 1
      end
    end
    @item_max = @data.size
    create_contents
    @reset = 0
    @nw_x = @reset
    @nw_y = 0
    @coun = 0
    self.contents.clear
    for i in 0..@item_max
    draw_item(i)
    end
  end
 
  #--------------------------------------------------------------------------
  # * Draw Item
  #     index : item number
  #--------------------------------------------------------------------------
  def draw_item(itemz)
    skill = @data[itemz]  
      if skill != nil        
    outline = Rect.new(0, 0, RECT_SIZE, RECT_SIZE)
    sub_rect = Rect.new((BORDER_SIZE / 2), (BORDER_SIZE / 2), 
       (RECT_SIZE - BORDER_SIZE), (RECT_SIZE - BORDER_SIZE))
      usable = @actor.skill_can_use?(skill)    
    if USE_BORDER_RECT  
      srect_color = system_color
      outline_sprite = Bitmap.new(RECT_SIZE, RECT_SIZE)
      outline_sprite.fill_rect(outline, srect_color)
      outline_sprite.clear_rect(sub_rect)
      self.contents.blt(@nw_x, @nw_y, outline_sprite, outline, usable ? 255 : 128)
    end
  
    draw_icon(skill.icon_index, @nw_x + 4, @nw_y + 4, usable ? 255 : 128)
    mp_cost = @actor.calc_mp_cost(skill)
    mp_cost_text = sprintf("#{mp_cost}:#{Vocab.mp}")
    old_font_size = self.contents.font.size
    self.contents.font.size = 16
     if $imported["ICY_Mp_Percent"]
       if ICY::MP_Percentage::USE_Mp_Percentage and ICY::MP_Percentage::Skills.include?(skill.id)
          icost = ICY::MP_Percentage::SkillPercents[skill.id]
          self.contents.draw_text(@nw_x + MP_COST_POS[0], @nw_y + MP_COST_POS[1],
          30, WLH, "#{icost}#{ICY::MP_Percentage::PerSym}", MP_COST_ALIGN)
        else
          self.contents.draw_text(@nw_x + MP_COST_POS[0], @nw_y + MP_COST_POS[1],
          48, WLH, mp_cost_text, MP_COST_ALIGN)
        end
         else
         self.contents.draw_text(@nw_x + MP_COST_POS[0], @nw_y + MP_COST_POS[1],
         48, WLH, mp_cost_text, MP_COST_ALIGN)
      end
    @coun += 1
    @nw_x += ITEM_SQ_SPACING
      end
    if @coun == @column_max
      @coun = 0
      @nw_x = @reset
      @nw_y += ITEM_SQ_SPACING 
    end
end

  #--------------------------------------------------------------------------
  # * Get Top Row
  #--------------------------------------------------------------------------
  def top_row
    return self.oy / ITEM_SQ_SPACING#WLH
  end
  #--------------------------------------------------------------------------
  # * Set Top Row
  #     row : row shown on top
  #--------------------------------------------------------------------------
  def top_row=(row)
    row = 0 if row < 0
    row = row_max - 1 if row > row_max - 1
    self.oy = row * ITEM_SQ_SPACING#WLH
  end
  #--------------------------------------------------------------------------
  # * Get Number of Rows Displayable on 1 Page
  #--------------------------------------------------------------------------
  def page_row_max
    return (self.height / ITEM_SQ_SPACING) 
  end
  #--------------------------------------------------------------------------
  # * Get Number of Items Displayable on 1 Page
  #--------------------------------------------------------------------------
  def page_item_max
    return page_row_max * @column_max #+ ITEM_SQ_SPACING
  end
  #--------------------------------------------------------------------------
  # * Move cursor one page down
  #--------------------------------------------------------------------------
  def cursor_pagedown
    if top_row + page_row_max < row_max
      @index = [@index + page_item_max, @item_max - 1].min
      self.top_row += page_row_max - ITEM_SQ_SPACING 
    end
  end
  #--------------------------------------------------------------------------
  # * Move cursor one page up
  #--------------------------------------------------------------------------
  def cursor_pageup
    if top_row > 0
      @index = [@index - page_item_max, 0].max
      self.top_row -= page_row_max - ITEM_SQ_SPACING 
    end
  end
  #--------------------------------------------------------------------------
  # * Update cursor
  #--------------------------------------------------------------------------
  def update_cursor
    if @index < 0                   # If the cursor position is less than 0
      self.cursor_rect.empty        # Empty cursor
    else                            # If the cursor position is 0 or more
      row = @index / @column_max    # Get current row
      if row < top_row              # If before the currently displayed
        self.top_row = row          # Scroll up
      end
      if row > bottom_row           # If after the currently displayed
        self.bottom_row = row       # Scroll down
      end
       y_l = @index / @column_max
       y_pos = (ITEM_SQ_SPACING * y_l)- self.oy
       x_l = self.index - (@column_max * y_l)
       x_pos = x_l * ITEM_SQ_SPACING
       if SELECTION_SIZE > RECT_SIZE
         subitive = (SELECTION_SIZE - RECT_SIZE) / 2
         self.cursor_rect.set(x_pos.to_i - subitive, y_pos.to_i - subitive, SELECTION_SIZE, SELECTION_SIZE)
       elsif SELECTION_SIZE < RECT_SIZE
         additive = (RECT_SIZE - SELECTION_SIZE) / 2
         self.cursor_rect.set(x_pos.to_i + additive, y_pos.to_i + additive, SELECTION_SIZE, SELECTION_SIZE)
        else
        self.cursor_rect.set(x_pos.to_i, y_pos.to_i, SELECTION_SIZE, SELECTION_SIZE)
       end
    end
  end
  #--------------------------------------------------------------------------
  # * Update Help Text
  #--------------------------------------------------------------------------
  def update_help
    @help_window.set_item(@data[self.index], @actor)
  end
  
end

#==============================================================================
# ** Window_Help
#------------------------------------------------------------------------------
#  This window shows skill and item explanations along with actor status.
#==============================================================================
class Window_Help < Window_Base
 include ICY::Skill_Window
    def set_item(skill, actor)
    @actor = actor
    if skill != nil
    self.contents.clear  
    outline = Rect.new(0, 0, RECT_SIZE, RECT_SIZE)
    sub_rect = Rect.new((BORDER_SIZE / 2), (BORDER_SIZE / 2), 
       (RECT_SIZE - BORDER_SIZE), (RECT_SIZE - BORDER_SIZE))
       
    if USE_BORDER_RECT 
      srect_color = system_color
      outline_sprite = Bitmap.new(RECT_SIZE, RECT_SIZE)
      outline_sprite.fill_rect(outline, srect_color)
      outline_sprite.clear_rect(sub_rect)
      self.contents.blt(0, 0, outline_sprite, outline)
    end
    text_x = 4
    draw_icon(skill.icon_index, SKILL_ICON_POS[0], SKILL_ICON_POS[1]) 
    # Writes Item Name
    self.contents.draw_text(text_x + 32, 0, self.width - 16, WLH, skill.name, 0)
    self.contents.font.color = normal_color
    # Writes Item Description
    old_font_size = self.contents.font.size
    self.contents.font.color = normal_color
    self.contents.font.size = 18
    self.contents.draw_text(text_x + 16, 32, self.width - 16, WLH, skill.description, 0)
    self.contents.font.size = old_font_size
     mp_cost = @actor.calc_mp_cost(skill)
     mp_cost_text = sprintf("#{mp_cost}:#{Vocab.mp}")
     x_poz = self.width - 128
     draw_icon(MP_ICON, x_poz - 32, 4)
      if $imported["ICY_Mp_Percent"]
       if ICY::MP_Percentage::USE_Mp_Percentage and ICY::MP_Percentage::Skills.include?(skill.id)
          icost = ICY::MP_Percentage::SkillPercents[skill.id]
          self.contents.draw_text(x_poz, 4, 30, WLH, "#{icost}#{ICY::MP_Percentage::PerSym}", 0)
        else
          self.contents.draw_text(x_poz, 4, 48, WLH, mp_cost_text, 0)
        end
         else
          self.contents.draw_text(x_poz, 4, 48, WLH, mp_cost_text, 0)
       end
       
    end
  end
 
end
#==============================================================================
# ** ICY_Item_Window_Help
#------------------------------------------------------------------------------
#  This window shows skill and item explanations along with actor status.
#==============================================================================

class ICY_Skill_Window_Help < Window_Base
  include ICY::Skill_Window
  #--------------------------------------------------------------------------
  # * Object Initialization
  #--------------------------------------------------------------------------
  def initialize(x, y, width, height)
    super(x, y, width, height)
    @item = nil
    @align = nil
  end
  #--------------------------------------------------------------------------
  # * Set Text
  #  text  : character string displayed in window
  #  align : alignment (0..flush left, 1..center, 2..flush right)
  #--------------------------------------------------------------------------
  def set_text(text, align = 0)
    if text != @text or align != @align
      self.contents.clear
      old_font_size = self.contents.font.size
      self.contents.font.color = normal_color
      self.contents.font.size = 16
      self.contents.draw_text(4, 24, self.width - 16, self.contents.font.size, text, align)
      self.contents.font.size = old_font_size
      @text = text
      @align = align
    end
  end
  
  def set_item(skill, actor)
    @actor = actor
    if skill != nil
    self.contents.clear  
    outline = Rect.new(0, 0, RECT_SIZE, RECT_SIZE)
    sub_rect = Rect.new((BORDER_SIZE / 2), (BORDER_SIZE / 2), 
       (RECT_SIZE - BORDER_SIZE), (RECT_SIZE - BORDER_SIZE))
       
    if USE_BORDER_RECT 
      srect_color = system_color
      outline_sprite = Bitmap.new(RECT_SIZE, RECT_SIZE)
      outline_sprite.fill_rect(outline, srect_color)
      outline_sprite.clear_rect(sub_rect)
      self.contents.blt(0, 0, outline_sprite, outline)
    end
    text_x = 4
    draw_icon(skill.icon_index, SKILL_ICON_POS[0], SKILL_ICON_POS[1]) 
    # Writes Item Name
    self.contents.draw_text(text_x + 32, 0, self.width - 16, WLH, skill.name, 0)
    self.contents.font.color = normal_color
    # Writes Item Description
    old_font_size = self.contents.font.size
    self.contents.font.color = normal_color
    self.contents.font.size = 18
    self.contents.draw_text(text_x + 16, 32, self.width - 16, WLH, skill.description, 0)
    self.contents.font.size = old_font_size
     mp_cost = @actor.calc_mp_cost(skill)
     mp_cost_text = sprintf("#{mp_cost}:#{Vocab.mp}")
     x_poz = self.width - 128
     draw_icon(MP_ICON, x_poz - 32, 4)
      if $imported["ICY_Mp_Percent"]
       if ICY::MP_Percentage::USE_Mp_Percentage and ICY::MP_Percentage::Skills.include?(skill.id)
          icost = ICY::MP_Percentage::SkillPercents[skill.id]
          self.contents.draw_text(x_poz, 4, 30, WLH, "#{icost}#{ICY::MP_Percentage::PerSym}", 0)
        else
          self.contents.draw_text(x_poz, 4, 48, WLH, mp_cost_text, 0)
        end
         else
          self.contents.draw_text(x_poz, 4, 48, WLH, mp_cost_text, 0)
       end
       
    end
  end
  
end


#==============================================================================
# ** ICY_Header_Window
#------------------------------------------------------------------------------
#  This window displays a header.
#==============================================================================
class ICY_Header_Window < Window_Base
  attr_accessor :font_size
  #--------------------------------------------------------------------------
  # * Object Initialization
  #     x      : window x-coordinate
  #     y      : window y-coordinate
  #     width  : window width
  #     height : window height
  #-------------------------------------------------------------------------- 
  def initialize(x, y, width, height)
    super(x, y, width, height)
    @font_size = 26
  end
  #--------------------------------------------------------------------------
  # ** Reset Font Size
  #   Resets the font size to 26
  #--------------------------------------------------------------------------
  def reset_font_size
    @font_size = 26
  end
  #--------------------------------------------------------------------------
  # ** Set Header
  #     text   : text
  #     icon   : icon_index
  #     align  : align
  #-------------------------------------------------------------------------- 
  def set_header(text = "" , icon = nil, align = 0)
    icon_offset = 32
    if icon == nil
      x = 0
    else
      x = icon_offset
      draw_icon(icon, 0, 0)
    end
     y = 0
     old_font_size = self.contents.font.size
     self.contents.font.size = @font_size
     self.contents.font.color = system_color
     self.contents.draw_text(x, y, (self.width - x) - 48, WLH, text, align)
     self.contents.font.color = normal_color
     self.contents.font.size = old_font_size
  end
  
end

#==============================================================================
# ** Scene_Skill
#------------------------------------------------------------------------------
#  This class performs the skill screen processing.
#==============================================================================

class Scene_Skill < Scene_Base
  #--------------------------------------------------------------------------
  # * Object Initialization
  #     actor_index : actor index
  #--------------------------------------------------------------------------
  def initialize(actor_index = 0, equip_index = 0)
    @actor_index = actor_index
  end
  #--------------------------------------------------------------------------
  # * Start processing
  #--------------------------------------------------------------------------
  def start
    super
    window_sizes = ICY::Skill_Window::SKILL_WINDOW_SIZES
    create_menu_background
    @actor = $game_party.members[@actor_index]
    
    party_strip_size = window_sizes["PartyStrip"]
    @actor_strip = ICY_Window_PartyStrip.new(party_strip_size[0], 
    party_strip_size[1], party_strip_size[2], party_strip_size[3], @actor)
    
    @viewport = Viewport.new(0, 0, 544, 416)
    
    header_size = window_sizes["Header"]
    @header_window = ICY_Header_Window.new(header_size[0], header_size[1], header_size[2], header_size[3])
    if $imported["MainMenuZealous"] 
     icon = YEZ::MENU::MENU_ICONS[:skill]
     else
     icon = nil
    end
    @header_window.set_header(Vocab.skill, icon, 0)
    @header_window.opacity = header_size[4]
    
    help_window_size = window_sizes["Help"]
    @help_window = ICY_Skill_Window_Help.new(help_window_size[0], help_window_size[1],
    help_window_size[2], help_window_size[3])
    @help_window.opacity = help_window_size[4]
    @help_window.viewport = @viewport
    
    @status_window = Window_SkillStatus.new(0, 56, @actor)
    @status_window.viewport = @viewport
    
    skill_window_size = window_sizes["Skills"]
    @skill_window = Window_Skill.new(skill_window_size[0], skill_window_size[1],
    skill_window_size[2], skill_window_size[3], @actor)
    @skill_window.opacity = skill_window_size[4]
    @skill_window.viewport = @viewport
    @skill_window.help_window = @help_window
    @skill_window.refresh
    
    @target_window = Window_MenuStatus.new(0, 0)
    if $imported["ICY_HM_Style_Menu_Status"]
      @target_win_help = ICY_Window_Menu_Status_Help.new(@target_window.x, @target_window.height)
      @target_window.help_window = @target_win_help
      @target_win_help.visible = false
    end
    hide_target_window
  end
  #--------------------------------------------------------------------------
  # * Termination Processing
  #--------------------------------------------------------------------------
  def terminate
    super
    dispose_menu_background
    @header_window.dispose
    @help_window.dispose
    @status_window.dispose
    @skill_window.dispose
    @target_window.dispose
    @actor_strip.dispose
    if $imported["ICY_HM_Style_Menu_Status"]
      @target_win_help.dispose
    end
  end
  #--------------------------------------------------------------------------
  # * Return to Original Screen
  #--------------------------------------------------------------------------
  def return_scene
    $scene = Scene_Menu.new(1)
  end
  #--------------------------------------------------------------------------
  # * Switch to Next Actor Screen
  #--------------------------------------------------------------------------
  def next_actor
    @actor_index += 1
    @actor_index %= $game_party.members.size
    $scene = Scene_Skill.new(@actor_index)
  end
  #--------------------------------------------------------------------------
  # * Switch to Previous Actor Screen
  #--------------------------------------------------------------------------
  def prev_actor
    @actor_index += $game_party.members.size - 1
    @actor_index %= $game_party.members.size
    $scene = Scene_Skill.new(@actor_index)
  end
  #--------------------------------------------------------------------------
  # * Frame Update
  #--------------------------------------------------------------------------
  def update
    super
    update_menu_background
    @help_window.update
    @status_window.update
    @skill_window.update
    @target_window.update
    if @skill_window.active
      update_skill_selection
    elsif @target_window.active
      update_target_selection
    end
  end
  #--------------------------------------------------------------------------
  # * Update Skill Selection
  #--------------------------------------------------------------------------
  def update_skill_selection
    if Input.trigger?(Input::B)
      Sound.play_cancel
      return_scene
    elsif Input.trigger?(Input::R)
      Sound.play_cursor
      next_actor
    elsif Input.trigger?(Input::L)
      Sound.play_cursor
      prev_actor
    elsif Input.trigger?(Input::C)
      @skill = @skill_window.skill
      if @skill != nil
        @actor.last_skill_id = @skill.id
      end
      if @actor.skill_can_use?(@skill)
        Sound.play_decision
        determine_skill
      else
        Sound.play_buzzer
      end
    end
  end
  #--------------------------------------------------------------------------
  # * Confirm Skill
  #--------------------------------------------------------------------------
  def determine_skill
    if @skill.for_friend?
      show_target_window(@skill_window.index % 2 == 0)
      if @skill.for_all?
        @target_window.index = 99
      elsif @skill.for_user?
        @target_window.index = @actor_index + 100
      else
        if $game_party.last_target_index < @target_window.item_max
          @target_window.index = $game_party.last_target_index
        else
          @target_window.index = 0
        end
      end
    else
      use_skill_nontarget
    end
  end
  #--------------------------------------------------------------------------
  # * Update Target Selection
  #--------------------------------------------------------------------------
  def update_target_selection
    if $imported["ICY_HM_Style_Menu_Status"]
      @target_win_help.update
    end
    if Input.trigger?(Input::B)
      Sound.play_cancel
      hide_target_window
    elsif Input.trigger?(Input::C)
      if not @actor.skill_can_use?(@skill)
        Sound.play_buzzer
      else
        determine_target
      end
    end
  end
  #--------------------------------------------------------------------------
  # * Confirm Target
  #    If there is no effect (such as using a potion on an incapacitated
  #    character), play a buzzer SE.
  #--------------------------------------------------------------------------
  def determine_target
    used = false
    if @skill.for_all?
      for target in $game_party.members
        target.skill_effect(@actor, @skill)
        used = true unless target.skipped
      end
    elsif @skill.for_user?
      target = $game_party.members[@target_window.index - 100]
      target.skill_effect(@actor, @skill)
      used = true unless target.skipped
    else
      $game_party.last_target_index = @target_window.index
      target = $game_party.members[@target_window.index]
      target.skill_effect(@actor, @skill)
      used = true unless target.skipped
    end
    if used
      use_skill_nontarget
    else
      Sound.play_buzzer
    end
  end
  #--------------------------------------------------------------------------
  # * Show Target Window
  #     right : Right justification flag (if false, left justification)
  #--------------------------------------------------------------------------
  alias icy_hm_skill_show_target_window show_target_window unless $@
  def show_target_window(*args)
    icy_hm_skill_show_target_window(*args)
    if $imported["ICY_HM_Style_Menu_Status"]
      @target_win_help.x = @target_window.x
      @target_win_help.y = @target_window.height
      @target_win_help.visible = true
    end
  end
  #--------------------------------------------------------------------------
  # * Hide Target Window
  #--------------------------------------------------------------------------
  alias icy_hm_skill_hide_target_window hide_target_window unless $@
  def hide_target_window(*args)
    icy_hm_skill_hide_target_window(*args)
    if $imported["ICY_HM_Style_Menu_Status"]
      @target_win_help.visible = false
    end
  end
  #--------------------------------------------------------------------------
  # * Use Skill (apply effects to non-ally targets)
  #--------------------------------------------------------------------------
  def use_skill_nontarget
    Sound.play_use_skill
    @actor.mp -= @actor.calc_mp_cost(@skill)
    @status_window.refresh
    @skill_window.refresh
    @target_window.refresh
    if $game_party.all_dead?
      $scene = Scene_Gameover.new
    elsif @skill.common_event_id > 0
      $game_temp.common_event_id = @skill.common_event_id
      $scene = Scene_Map.new
    end
  end
end

#===============================================================================
# return scene alias
#===============================================================================
if $imported["SceneStatusReDux"]
class Scene_Skill < Scene_Base
  
  #--------------------------------------------------------------------------
  # alias return scene
  #--------------------------------------------------------------------------
  alias skill_return_scene_statusrd return_scene unless $@
  def return_scene
    if $game_temp.status_menu_flag
      $scene = Scene_Status.new(@actor_index, $game_temp.status_menu_index)
    else
      skill_return_scene_statusrd
    end
  end
  
end
end
