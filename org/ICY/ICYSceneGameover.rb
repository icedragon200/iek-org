# ~ICY_Scene_Gameover
#------------------------------------------------------------------------------#
# ** ICY Scene Gameover
# ** Created by : IceDragon
# ** Script-Status : Addon
# ** Date Created : 3/21/2010
# ** Date Modified : 7/16/2010
#------------------------------------------------------------------------------#
$imported = {} if $imported == nil
$imported["ICY_Scene_Gameover"] = true
#------------------------------------------------------------------------------#
# ** Start Customization
#------------------------------------------------------------------------------#
module ICY
  module GAMEOVER
    GAMEOVER_BGM = "020-Field03" 
    #Text used for return to title
    RETTI = "To Title"
    RETTI_ICON = 210
    
    SHOW_WINDOW = true
    X_POS = 0
    Y_POS = 120
    #This is the amount of columns in the window
    #I reccommend 1 or 3
    COLUMNS = 3
    #If your using 1 column then 172 will do else use something above 350
    WIDTH = 420
  end
end
#------------------------------------------------------------------------------#
# ** End Customization
#------------------------------------------------------------------------------#

#ICY_W1.2
#==============================================================================
# ** ICY_Window_Command
#------------------------------------------------------------------------------
#  This window deals with general command choices.
#==============================================================================

class ICY_Window_Command < Window_Selectable
  #--------------------------------------------------------------------------
  # * Public Instance Variables
  #--------------------------------------------------------------------------
  attr_reader   :commands                 # command
  #--------------------------------------------------------------------------
  # * Object Initialization
  #     width      : window width
  #     commands   : command string array
  #     commandcons: command icon array
  #     column_max : digit count (if 2 or more, horizontal selection)
  #     row_max    : row count (0: match command count)
  #     spacing    : blank space when items are arrange horizontally
  #--------------------------------------------------------------------------
  def initialize(width, commands, commandcons, fontsize = 18, column_max = 1, row_max = 0, spacing = 32)
    if row_max == 0
      row_max = (commands.size + column_max - 1) / column_max
    end
    super(0, 0, width, row_max * WLH + 32, spacing)
    @commands = commands
    @item_max = commands.size
    @column_max = column_max
    @commandcons = commandcons
    @fontsize = fontsize
    refresh
    self.index = 0
  end
  #--------------------------------------------------------------------------
  # * Refresh
  #--------------------------------------------------------------------------
  def refresh
   self.contents.clear
    for i in 0...@item_max
      command = @commands[i]
      icon = @commandcons[command]        
       icy_recdraw_item(i, icon)
    end
  end
  #--------------------------------------------------------------------------
  # * Draw Item
  #     index   : item number
  #     enabled : enabled flag. When false, draw semi-transparently.
  #--------------------------------------------------------------------------
  def icy_recdraw_item(index, icon_index, enabled = true)
    rect = item_rect(index)
    rect.x += 24
    rect.width -= 8
    
    if @column_max == 1
    draw_icon(icon_index, x, rect.y, enabled)
  else
    draw_icon(icon_index, (rect.x - 24), y, enabled)
  end
  
    self.contents.clear_rect(rect)
    self.contents.font.size = @fontsize
    self.contents.font.color = normal_color
    self.contents.font.color.alpha = enabled ? 255 : 128
    self.contents.draw_text(rect, @commands[index])
  end
end

#==============================================================================
# ** Scene_Gameover
#------------------------------------------------------------------------------
#  This class performs game over screen processing.
#==============================================================================

class Scene_Gameover < Scene_Base
  #--------------------------------------------------------------------------
  # * Start processing
  #--------------------------------------------------------------------------
  def initialize(skip = false)
    @skip = skip
  end
  
  def start
    super
    @bgm = RPG::BGM.new(ICY::GAMEOVER::GAMEOVER_BGM, 80)
    if @skip == false
    RPG::BGM.stop
    RPG::BGS.stop
    $data_system.gameover_me.play
    Graphics.transition(120)
    Graphics.freeze
    end
    @bgm.play
    create_gameover_graphic
    create_winds
    @commanding.active = true
    @commanding.visible = true
  end
  
  def create_winds
    if $imported["ICY_Scene_File"] == true
    @continue_enabled = (Dir.glob("#{ICY::Saving_Scene::Save_Data_Name}*.rvdata").size > 0) 
      else
    @continue_enabled = (Dir.glob('Save*.rvdata').size > 0)
    end
    
    i1 = ICY::GAMEOVER::RETTI_ICON
    i2 = ICY::TScreen::CON
    i3 = ICY::TScreen::SHTDWN
    
    c1 = ICY::GAMEOVER::RETTI
    c2 = Vocab::continue
    c3 = Vocab::shutdown
    
    iconp = {c1 => i1, c2 => i2, c3 => i3}
    @commanding = ICY_Window_Command.new(ICY::GAMEOVER::WIDTH, [c1, c2, c3], iconp, 16,ICY::GAMEOVER::COLUMNS)
    @commanding.active = false
    @commanding.visible = false
    if ICY::GAMEOVER::SHOW_WINDOW != true
      @commanding.opacity = 0
    end
    
    if @continue_enabled                    # If continue is enabled
      @commanding.index = 1             # Move cursor over command
    else                                    # If disabled
      @commanding.icy_recdraw_item(1, i2, false)   # Make command semi-transparent
    end
  end
  
  #--------------------------------------------------------------------------
  # * Termination Processing
  #--------------------------------------------------------------------------
  def terminate
    super
    dispose_gameover_graphic
    $scene = nil if $BTEST
  end  
  #--------------------------------------------------------------------------
  # * Frame Update
  #--------------------------------------------------------------------------
  def update
    super
    if Input.trigger?(Input::C) and @commanding.active
      case @commanding.index
      when 0
        command_to_title
      when 1
        command_continue
      when 2
        command_shutdown
      end
      
    end
    
     @commanding.update
   end
   
   def command_to_title
      Sound.play_decision
      @commanding.visible = false
      @commanding.active = false
      $scene = Scene_Title.new
      Graphics.fadeout(120)
      RPG::BGM.fade(800)
      RPG::BGS.fade(800)
      RPG::ME.fade(800)
    end
  #--------------------------------------------------------------------------
  # * Load Database
  #--------------------------------------------------------------------------
  def icy_load_database
    $data_actors        = load_data("Data/Actors.rvdata")
    $data_classes       = load_data("Data/Classes.rvdata")
    $data_skills        = load_data("Data/Skills.rvdata")
    $data_items         = load_data("Data/Items.rvdata")
    $data_weapons       = load_data("Data/Weapons.rvdata")
    $data_armors        = load_data("Data/Armors.rvdata")
    $data_enemies       = load_data("Data/Enemies.rvdata")
    $data_troops        = load_data("Data/Troops.rvdata")
    $data_states        = load_data("Data/States.rvdata")
    $data_animations    = load_data("Data/Animations.rvdata")
    $data_common_events = load_data("Data/CommonEvents.rvdata")
    $data_system        = load_data("Data/System.rvdata")
    $data_areas         = load_data("Data/Areas.rvdata")
  end
  
    def command_continue
      if @continue_enabled
      Sound.play_decision
      Graphics.freeze
      @commanding.visible = false
      @commanding.active = false
      icy_load_database
      $scene = Scene_File.new(false, false, false, true)
      $game_map.refresh
    else
      Sound.play_buzzer
    end
    
  end
  
  def command_shutdown
    Sound.play_decision
    Graphics.fadeout(120)
    RPG::BGM.fade(800)
    RPG::BGS.fade(800)
    RPG::ME.fade(800)
    @commanding.visible = false
    $scene = nil
  end
  
  #--------------------------------------------------------------------------
  # * Execute Transition
  #--------------------------------------------------------------------------
  def perform_transition
    Graphics.transition(150)
  end
  #--------------------------------------------------------------------------
  # * Create Game Over Graphic
  #--------------------------------------------------------------------------
  def create_gameover_graphic
    @sprite = Sprite.new
    @sprite.bitmap = Cache.system("GameOver")
  end
  #--------------------------------------------------------------------------
  # * Dispose of Game Over Graphic
  #--------------------------------------------------------------------------
  def dispose_gameover_graphic
    @sprite.bitmap.dispose
    @sprite.dispose
  end
end

class Scene_File < Scene_Base
  
def initialize(saving, from_title, from_event, from_gameover = false)
    @saving = saving
    @from_title = from_title
    @from_event = from_event
    @from_gameover = from_gameover
  end
  
def return_scene
    if @from_title
      $scene = Scene_Title.new
    elsif @from_event
      $scene = Scene_Map.new
    elsif @from_gameover
      $scene = Scene_Gameover.new(true)
     else
      $scene = Scene_Menu.new(4) 
    end
  end
  
  #--------------------------------------------------------------------------
  # * Execute Transition
  #--------------------------------------------------------------------------
  def perform_transition
    if @from_gameover
    Graphics.transition(60)
    else
    Graphics.transition(10)
  end
  
  
  end
  
end
