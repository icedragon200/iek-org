# ~ICY_Scene_Chapter
#------------------------------------------------------------------------------#
# ** Icy Chapter Scene
# ** Created by :IceDragon 
# ** Script-Status : Addon
# ** Date Modified : 5/20/2010
# ** Version 1.0
#------------------------------------------------------------------------------#
# This is for the poor players, who don't have a clue, what point in the 
# game they are.
# It simply shows a scene with the chapter name, and number..
# The current chapter is stored in the $game_system and an in-game Variable
#
# This is so simple..
# To use this
# Call a script
# new_chapter(chapter_number, xtra)
# chapter_number is pretty straight forward..
# xtra Is this chapter and Extra chapter or not. Put either True or False
#------------------------------------------------------------------------------#
# End Desription
#------------------------------------------------------------------------------#
$imported = {} if $imported == nil
$imported["ICY_Chapter_Scene"] = true
#------------------------------------------------------------------------------#
# ** Start Customization
#------------------------------------------------------------------------------#
module ICY
  module Chapters
  
    Chapters = { # Don't mess with this
#Chapter number => "Chapter_Name", The comma is very important
    0 => "Who is Icy",
    1 => "Checking the stuff",                # Intro Chapter
    2 => "Whats good for something",
    } # AND Don't mess with this
    Chapter_Variable = 15 # In game variable used for chapters
    Fade_Out_Time = 240 # 1s = 60 Frames
    Chapter_Back = "TitleBack" # Image to be used as chapter background. Don't leave blank.
    Chapter_Tag = "-Chapter-" # Word displayed for chapter.. Like -Chapter- 1, or -Story- 1
    Extra_Chapter_Tag = "-EX-" # Tag used for XTRA Chapters.. Its actually a normal Chapter
    Chapter_BGM = RPG::BGM.new("Scene2", 90, 100) # BGM used for chapter
    Chapter_Icon = 149  #Used in Save File
  end
end
#------------------------------------------------------------------------------#
# ** End Customization
#------------------------------------------------------------------------------#
#==============================================================================
# ** Game_Interpreter
#------------------------------------------------------------------------------
#  An interpreter for executing event commands. This class is used within the
# Game_Map, Game_Troop, and Game_Event classes.
#==============================================================================
# ** Added
# -- new_chapter
#==============================================================================
class Game_Interpreter
  #----------------------------------------------------------------------------
  # ** New_Chapter = Used to change the current chapter
  #----------------------------------------------------------------------------
  def new_chapter(chapter, ex = false)
    $game_system.current_chapter = chapter
    $game_variables[ICY::Chapters::Chapter_Variable] = chapter
    $scene = Scene_Chapter.new(chapter, ex)
  end
  
end

#==============================================================================
# ** Game_System
#------------------------------------------------------------------------------
#  This class handles system-related data. Also manages vehicles and BGM, etc.
# The instance of this class is referenced by $game_system.
#==============================================================================
# ** Alias
# -- initialize
#
# ** Attribute Accessor
# -- current_chapter
#==============================================================================
class Game_System
  attr_accessor :current_chapter #for some super EASY access
  #----------------------------------------------------------------------------
  # ** alias initialize 
  #----------------------------------------------------------------------------
  alias chapter_system_initialize initialize
  def initialize(*args)
    chapter_system_initialize(*args)
    @current_chapter = 0
  end
  
end

#==============================================================================
# ** Scene_Chapter
#------------------------------------------------------------------------------
#  This class performs chapter screen processing.
#==============================================================================
class Scene_Chapter < Scene_Base
  
  #----------------------------------------------------------------------------
  # ** Initialize 
  #----------------------------------------------------------------------------
  def initialize(chapter = $game_system.current_chapter, ex = false)
    @ex = ex
    @chapter_track = ICY::Chapters::Chapter_BGM
    $game_message.visible = false 
    @chapter_number = chapter.to_i
  end
  
  #----------------------------------------------------------------------------
  # ** Start 
  #----------------------------------------------------------------------------
  def start
    super
    RPG::BGM.stop
    RPG::BGS.stop
    @chapter_track.play
    create_menu_background
    @viewport = Viewport.new(0,0, Graphics.width, Graphics.height)
    @viewport.z = 200
    @chapter_sprite = Chapter_Sprite.new(@viewport, @ex)
    @chapter_sprite.create_chapter_graphics(@chapter_number)
    @prepared = true
  end
  
  #----------------------------------------------------------------------------
  # ** Terminate .... Its empty!? 
  #----------------------------------------------------------------------------
  def terminate
    super
  end
  
  #----------------------------------------------------------------------------
  # ** Update 
  #----------------------------------------------------------------------------
  def update
    super
    if @prepared
    @chapter_sprite.update
    end
  
  if @chapter_sprite.finished
    dispose_sprites
  end
  
   if Input.trigger?(Input::A) or Input.trigger?(Input::B) or Input.trigger?(Input::C)
      dispose_sprites
    end
  end
  
  #----------------------------------------------------------------------------
  # ** Dispose Sprites 
  #----------------------------------------------------------------------------
  def dispose_sprites
    dispose_menu_background
    @chapter_sprite.dispose
    $scene = Scene_Map.new
    $game_map.autoplay
  end
  
end

#==============================================================================
# ** Chapter_Sprite
#------------------------------------------------------------------------------
#  This sprite is used to display the chapter. 
#==============================================================================
class Chapter_Sprite < Sprite_Base
  
  attr_accessor :finished
  include ICY::Chapters # yes because its annoying have to call ICY::Chapters::WHATEVER_I_NEED!
  #----------------------------------------------------------------------------
  # ** Initialize 
  #----------------------------------------------------------------------------  
  def initialize(viewport, ex = false)
    super(viewport)
    @extra = ex
    @vol = 100
    @fade_out_time = Fade_Out_Time
    @finished = false
    @start_fade = false
  end
  
  #----------------------------------------------------------------------------
  # ** Create_Chapter_Graphics 
  #----------------------------------------------------------------------------   
  def create_chapter_graphics(chap_num)
    chapternumber = chap_num
    chapter = Chapters[chap_num]
    self.bitmap = Bitmap.new(Graphics.width, Graphics.height)
    self.opacity = 0
    a = 0
    b = 1
    c = 2
    d = 3
    @sprites = []
    @sprites[a] = ::Sprite.new(self.viewport)
    @sprites[a].bitmap = Cache.system(Chapter_Back)
    @sprites[a].blend_type = 1
    @sprites[a].opacity = 0
    @sprites[a].x = (Graphics.width - @sprites[a].width) / 2
    @sprites[a].y = (Graphics.height - @sprites[a].height) / 2
    @sprites[b] = Bitmap.new(260, 64)
    @sprites[b].font.size = 56
    @sprites[b].font.bold = true
    @sprites[b].font.italic = true
    @sprites[b].font.shadow = true
    @sprites[b].font.color.set(173, 216, 230)
    @sprites[b].draw_text(0, 0, 220, 80, Chapter_Tag) 
    @sprites[c] = Bitmap.new(160, 160)
    @sprites[c].font.size = 48
    @sprites[c].font.bold = true
    @sprites[c].font.italic = true
    @sprites[c].font.shadow = true
    @sprites[c].draw_text(0, 0, 220, 80, chapternumber) if @extra == false
    @sprites[c].draw_text(0, 0, 220, 80, Extra_Chapter_Tag) if @extra 
    @sprites[d] = Bitmap.new(320, 160)
    @sprites[d].font.size = 28
    @sprites[d].font.bold = true
    @sprites[d].font.italic = true
    @sprites[d].font.shadow = true
    @sprites[d].draw_text(0, 0, 320, 80, "-#{chapter}-")
    icenter_x = (Graphics.width / 4)
    icenter_y = (Graphics.height / 3)
    self.bitmap.blt(icenter_x, icenter_y, @sprites[b], @sprites[b].rect)
    self.bitmap.blt((icenter_x + @sprites[b].width), icenter_y,@sprites[c], @sprites[c].rect)
    self.bitmap.blt(icenter_x, (icenter_y + @sprites[b].height), @sprites[d], @sprites[d].rect)
  end
  
  #----------------------------------------------------------------------------
  # ** Dispose 
  #---------------------------------------------------------------------------- 
  def dispose
    for i in 0..@sprites.size
      if @sprites[i] != nil
       @sprites[i].dispose
      end
    end
    super
  end
  
  #----------------------------------------------------------------------------
  # ** Update 
  #---------------------------------------------------------------------------- 
  def update
    if self.opacity < 255 and @start_fade == false
      @sprites[0].opacity += 1 unless @sprites[0].opacity >= 180
      self.opacity += 1
      #Graphics.update
    end
    if self.opacity == 255
      @start_fade = true
      @fade_out_time -= 1
    end
    if @start_fade == true and @fade_out_time == 0 and not self.opacity == 0
      @sprites[0].opacity -= 1 unless @sprites[0].opacity == 0
      self.opacity -= 1
      #Graphics.update
    end
    if @start_fade == true and self.opacity == 0 and @fade_out_time == 0
      @finished = true
    end
    super  
  end
  
end
################################################################################
#------------------------------------------------------------------------------#
#END\\\END\\\END\\\END\\\END\\\END\\\END///END///END///END///END///END///END///#
#------------------------------------------------------------------------------#
################################################################################
