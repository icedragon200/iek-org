# ~ICY_Map_JumpTo
#==============================================================================#
#------------------------------------------------------------------------------#
#= ** Icy Map Jumps
#= ** Created by : IceDragon
#= ** Script-Status : Snippet
#= ** Date Created : (I don't remember!)
#= ** Date Modified : 7/10/2010
#= ** Version : 0.5
#------------------------------------------------------------------------------#
#==============================================================================#
# THERE ARE NO CUSTOMIZATIONS FOR THIS!!!
# How to use?
# In an event..
# Use a Script with one of the following in it

#------------------------------------------------------------------------------#
# map_jump_to(fades, x, y)
# This will center the screen on the given X, Y coords
# You can disregard the fades(just put false)I still need to fix it, if you put 
# true nothing will happen anyway..
# eg. map_jump_to(false, 12, 27)
# The screen will jump to and center on the (x 12, y 27)
# To have it center back on the player just call
# map_jump_to
# This will center the screen back on the player
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
# map_jump_to_event(event, fades)
# This will jump to a event and center the screen on it.
# You can disregard the fades(just put false)I still need to fix it, if you put 
# true nothing will happen anyway..
#   -1 for player (basically works the same as calling map_jump_to)
#    0 for current event
#    1 and above for other events
# eg. map_jump_to_event(99, false)
# This will center on event 99
#
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
# map_event_scroll(active, event)
# This is still weird...
# The idea was to have the screen scroll with an event, similar to how it scrolls
# with the player, the problem is I can't get the scrolling right...
# This works but it looks crude
# My advice, DON"T USE IT!!(Still nothing bad will happen)
# active is a switch while true it will scroll with the event
# while false the screen will remain in its current position
#   -1 for player (basically works the same as calling map_jump_to)
#    0 for current event
#    1 and above for other events 
# eg. map_event_scroll(true, 99)
# This will scroll with event 99
#------------------------------------------------------------------------------#
#
# The module is junk!
# You can disregard it...
# If you can fix something in this script..
# Please do so and let me know!
module ICY
  module Map_Jump_Ops
    
    def self.fade_sys(fadeam, inout)
      if inout == "in"
       Graphics.fadein(fadeam)
       Graphics.wait(fadeam)
      elsif inout == "out"
       Graphics.fadeout(fadeam)
       Graphics.wait(fadeam)
       return true
      elsif inout == "frz"
       Graphics.freeze
      elsif inout == "trns"
       Graphics.transition(fadeam)
      end
    end
  
  end
end

#==============================================================================
# ** Game_Interpreter
#------------------------------------------------------------------------------
#  An interpreter for executing event commands. This class is used within the
# Game_Map, Game_Troop, and Game_Event classes.
#==============================================================================
class Game_Interpreter
  
  #Jump to point on map
  def map_jump_to(fadez = false, max = nil, may = nil)
    $game_player.icy_map_jump_to(max, may, fadez)
  end
  
  #Jump to an event
  def map_jump_to_event(targetevent, fadezz = false)
    $game_player.icy_center_on_target(targetevent, fadezz)
  end
  
  #Scroll with event
  def map_event_scroll(active, targetevent)
    $game_player.icy_scroll_target_setup(active, targetevent)
  end
  
end

#==============================================================================
# ** Game_Player
#------------------------------------------------------------------------------
#  This class handles maps. It includes event starting determinants and map
# scrolling functions. The instance of this class is referenced by $game_map.
#==============================================================================

class Game_Player < Game_Character
  #--------------------------------------------------------------------------
  # * Alias Initialize
  #--------------------------------------------------------------------------
  alias setup_event_target_scroll_initialize initialize unless $@
  def initialize
    setup_event_target_scroll_initialize
    @event_target_scroll = nil
    @active = false
  end
  
  #--------------------------------------------------------------------------
  # * Icy Jump to event on map
  #--------------------------------------------------------------------------
  def icy_center_on_target(targetevent, fadez)
    emax = $game_map.events[targetevent].x
    emay = $game_map.events[targetevent].y
    center(emax, emay)
    $game_map.refresh
    $game_map.update
    refresh
    update
  end
  
  #--------------------------------------------------------------------------
  # * Icy Jump to point on map
  #--------------------------------------------------------------------------
  def icy_map_jump_to(max, may, fadez)
    if max == nil or may == nil
      max = self.x
      may = self.y
    end
    center(max, may)
    $game_map.refresh
    $game_map.update
    refresh
    update
  end
  #--------------------------------------------------------------------------
  # * Icy Setup Target Scroll
  #--------------------------------------------------------------------------
  def icy_scroll_target_setup(active = false, tagert = nil)
    if tagert != nil and active != false
      @event_target_scroll = $game_map.events[tagert]
      @active = true
      center(@event_target_scroll.x, @event_target_scroll.y)
    $game_map.refresh
    $game_map.update
     elsif active == false
      @event_target_scroll = nil
     elsif tagert != nil and active == true
      @active = false
    end
  end
  #--------------------------------------------------------------------------
  # * Alias Update Method
  #--------------------------------------------------------------------------
  
  alias icy_lock_event_scroll_update update unless $@
   def update
    icy_lock_event_scroll_update
    if @event_target_scroll != nil
    icyold_real_x = @event_target_scroll.x
    icyold_real_y = @event_target_scroll.y 
    icy_update_scroll(icyold_real_x, icyold_real_y)
  end
end

  #--------------------------------------------------------------------------
  # * ICY Update Scroll
  #--------------------------------------------------------------------------
  def icy_update_scroll(ilast_real_x, ilast_real_y)
    center(ilast_real_x, ilast_real_y)
    $game_map.refresh
    $game_map.update
  end
   
end

################################################################################
#------------------------------------------------------------------------------#
#END\\\END\\\END\\\END\\\END\\\END\\\END///END///END///END///END///END///END///#
#------------------------------------------------------------------------------#
################################################################################

