# ~ICY_Scene_Item
#------------------------------------------------------------------------------#
# ** ICY Harvest Moon Style Inventory
# ** Created by : IceDragon
# ** Script-Status : ReWrite
# ** Date Created : 7/16/2010
# ** Date Modified : 7/17/2010
# ** Version : 1.0
#------------------------------------------------------------------------------#
# INTRODUCTION
# While going around the Resource Showcase. I stumbled upon a siggy which read.
# "Give us a Harvest Moon Inventory", I decided to click it, and check it out.
# And I made this!
# The only problem the script request was for a LIMITED INVENTORY!
# This is UNLIMITED!!
# But is still nice as it is.
# This is a Fully Customizable Item Scene (Unless I missed something!)
#
#------------------------------------------------------------------------------#
# PLACEMENT
# Above 
#   Main
#   KGC_Catergorize_Items
#
# Below
#  Materials
#------------------------------------------------------------------------------#
# CHANGES
# This script overwrites the Scene_Item.
# The Window_Item remains intact
# The Window_Help remains intact
# Instead Custom Classes were created for those.
# This may cause Incompatability with other item scripts.
#
#------------------------------------------------------------------------------#
# COMPATABILTIES
# Patch for KGC_Catergorize_Items
# If YERD_Custom_Menu_System is Installed it will get the :item Icon for the 
# Header
#------------------------------------------------------------------------------#
# KNOWN ISSUES
# Non at the moment.
#
#------------------------------------------------------------------------------#
# CHANGE LOG
# 7/17/2010 Finished Script V_1.0
#
#------------------------------------------------------------------------------#
# Importing script for compatabilities (So I know if the script is there or not)
$imported = {} if $imported == nil
$imported["ICY_HM_Style_Inventory"] = true
#------------------------------------------------------------------------------#
# ** Start Customization
#------------------------------------------------------------------------------#
module ICY
  module HM_Style_Items
    KGC_Catergorize = true #Are you using KGC_Catergorize_Item. false if you don't have the script. 
#---------------------------------------------------------------#
# ** Window Setup
#---------------------------------------------------------------#
    WINDOW_SIZES = { # Don't touch this
#Don't touch this side < edit the array to the > [x, y, width, height, opacity],
    "Help" => [0, 280, 544, 80, 255], # This is the window which shows the description
    "Item" => [0, 56, 544, 280, 255], # This is the window which shows the items
    "Header" => [0, 0, 544, 56, 255], # This is the header window which shows the name
    } # Don't touch this
    NUM_OF_COLUMN = 10 # Number of Columns in Item Window
    ITEM_SQ_SPACING = 48 # Item Spacing
    SELECTION_SIZE = 40 # Selection Box size
#---------------------------------------------------------------#
# ** Rect 
#---------------------------------------------------------------#
    RECT_SIZE = 32 # The Box's size Reccomend 32 and above anything less causes scrolling problems
    
    USE_BORDER_RECT = true # Should a box be shown around the items
    # If use_border_rect = false Disregard everything below
    RECT_IN_HELP = true # Should the box be shown around the item in the help.
    RECT_COLOR = ICY::Colors::Gold # Default Box Color
    BORDER_SIZE = 4 # Use Even Number PLEASE!!
    # Custom Box Colors
    USE_RECT_ITEM_COLOR = true # Use specific colors for item types? If false uses RECT_COLOR
    RECT_ITEM_COLORS = { # Don't touch this
#Don't touch this side < edit the > or Color.new(RED, GREEN, BLUE, ALPHA)
    "Default"=> RECT_COLOR,
    "Usable" => ICY::Colors::LightGreen,
    "Weapon" => ICY::Colors::LightCoral,
    "Armor"  => ICY::Colors::LightBlue,
    } # Don't touch this
# Specific Item Colors
    RECT_VIP_ITEMS = [10] # Item Ids go here
    RECT_VIP_ITEM_COLORS = { # Don't touch this
    10 => ICY::Colors::Red,
    } # Don't touch this
    
    RECT_VIP_WEAPONS = [] # Weapon Ids go here
    RECT_VIP_WEAPON_COLORS = { # Don't touch this
    
    } # Don't touch this
    
    RECT_VIP_ARMORS = [] # Amror Ids Go here
    RECT_VIP_ARMOR_COLORS = { # Don't touch this
    
    } # Don't touch this
#----------------------------------------------------------------------#
# ** Images
#----------------------------------------------------------------------#
# Images must be the same size as the RECT_SIZE to get desisred results
#
    USE_IMAGES = true
    USE_WINDOW_IMAGES = false
    WINDOW_IMAGES = { # Don't touch this
    "Help" => "",
    "Item" => "",
    "Header" => "",
    } # Don't touch this
    
    USE_IMG_RECT = false # Should and Image Rect be used
    IMG_RECT = "Rect_Test_Img" # Default Image used.
    
    USE_SPECIFIC_IMG_RECTS = false
    IMG_SPECIFIC_RECTS = {
    "Default"=> "",
    "Usable" => "",
    "Weapon" => "",
    "Armor"  => "",
    }
    # Specific Rect Images
    IMG_RECT_VIP_ITEMS = [10] # Item Ids go here
    IMG_RECT_VIP_ITEM_IMAGES = { # Don't touch this
    10 => "",
    } # Don't touch this
    
    IMG_RECT_VIP_WEAPONS = [] # Weapon Ids go here
    IMG_RECT_VIP_WEAPON_IMAGES = { # Don't touch this
    
    } # Don't touch this
    
    IMG_RECT_VIP_ARMORS = [] # Amror Ids Go here
    IMG_RECT_VIP_ARMOR_IMAGES = { # Don't touch this
    
    } # Don't touch this
#---------------------------------------------------------------#
# ** Draw_Item 
#---------------------------------------------------------------#    
    DRAW_ITEM_ICON_IN_HELP = true # Should the items Icon be drawn in the help?
    DRAW_AMOUNT_FOR_1 = false # If there is only one of the item should it write 1
    UNUSABLE_OPACITY = 180 # The opacity for unusable items
    
    ITEM_NUM_POS = [16, 16] # [x, y] Position of Item Number 
    ITEM_NUM_ALIGN = 2 # 0 - Flush Left, 1 - Center, 2 - Flush Right
    ITEM_NUM_IN_HELP = false # Should the items amount be shown in help.
    ITEM_NUM_FORMAT = "x%2d" # Format "yourtext%2d" used when showing amount
    ITEM_ICON_POS = [4, 4] # Items Icon Position 
#---------------------------------------------------------------#
# ** Mini_Icons 
#---------------------------------------------------------------#    
    USE_MINI_ICONS = true #These mini icons represent the item type
    MINI_ICON_SET = "Mini_UseIcons" # This is in the System Folder
    MINI_ICON_SIZE = 8 # Thats in Pixels for one Icon
    MINI_ICON_COLUMNS = 4 # How many coloumns are in the image file
    MINI_ICON_POS = [0, (RECT_SIZE - MINI_ICON_SIZE)] # Mini Icon position
    MINI_ICON_IN_HELP = false # Should the mini Icon be shown in the help?
    
    MINI_ICON_ITEMS = { # Don't touch this
#Don't touch this side < edit the > MiniIcon_ID,
    "Default"=> 5,
    "Usable" => 3,
    "Weapon" => 4,
    "Armor"  => 2,
    }
    
# This is similar to the RECT BORDERS No explanation will be given.
    MINI_VIP_ITEMS = [10]
    MINI_VIP_ITEM_ICONS = {
    10 => 6,
    }
    MINI_VIP_WEAPONS = []
    MINI_VIP_WEAPON_ICONS = {
    }
    MINI_VIP_ARMORS = []
    MINI_VIP_ARMOR_ICONS = {
    }
    
#------------------------------------------------------------------------------#
# ** End Customization
#------------------------------------------------------------------------------#
# DON'T TOUCH ANYTHING BEYOND THIS POINT!!!!
# UNLESS YOU KNOW WHAT YOUR DOING!
#------------------------------------------------------------------------------# 
  #--------------------------------------------------------------------------
  # * Prep Mini Icon
  #     icon_index : Icon number
  #   This doesn't really draw the icon, it just sends the image and rect back.
  #--------------------------------------------------------------------------
  def self.prep_mini_icon(icon_index)
    bitmap = Cache.system(MINI_ICON_SET)
    rect = Rect.new(icon_index % MINI_ICON_COLUMNS * MINI_ICON_SIZE, icon_index / MINI_ICON_COLUMNS * MINI_ICON_SIZE, MINI_ICON_SIZE, MINI_ICON_SIZE)
    return [bitmap, rect]
  end
  #--------------------------------------------------------------------------
  # * Get Rect Image
  #     item : Item
  #   This is used to get the color for the border rect.
  #--------------------------------------------------------------------------
  def self.get_rect_image(item)
      if USE_SPECIFIC_IMG_RECTS
         if item.is_a?(RPG::UsableItem)
           if IMG_RECT_VIP_ITEMS.include?(item.id)
             srect_image = IMG_RECT_VIP_ITEM_IMAGES[item.id]
              else
             srect_image = IMG_SPECIFIC_RECTS["Usable"]
          end
        elsif item.is_a?(RPG::Weapon)
           if IMG_RECT_VIP_WEAPONS.include?(item.id)
             srect_image = IMG_RECT_VIP_WEAPON_IMAGES[item.id]
              else
             srect_image = IMG_SPECIFIC_RECTS["Weapon"]
          end
        elsif item.is_a?(RPG::Armor)
           if IMG_RECT_VIP_ARMORS.include?(item.id)
             srect_image = IMG_RECT_VIP_ARMOR_IMAGES[item.id]
              else
             srect_image = IMG_SPECIFIC_RECTS["Armor"]
          end
        else
          if IMG_RECT_VIP_ITEMS.include?(item.id)
           srect_image = IMG_RECT_VIP_ITEM_IMAGES[item.id]
            else
           srect_image = IMG_SPECIFIC_RECTS["Default"]
          end
         end
        else
         srect_image = IMG_RECT
       end
       return srect_image
     end
  #--------------------------------------------------------------------------
  # * Get Rect Color
  #     item : Item
  #   This is used to get the color for the border rect.
  #--------------------------------------------------------------------------
  def self.get_rect_color(item)
      if USE_RECT_ITEM_COLOR
         if item.is_a?(RPG::UsableItem)
           if RECT_VIP_ITEMS.include?(item.id)
             srect_color = RECT_VIP_ITEM_COLORS[item.id]
              else
             srect_color = RECT_ITEM_COLORS["Usable"]
          end
        elsif item.is_a?(RPG::Weapon)
           if RECT_VIP_WEAPONS.include?(item.id)
             srect_color = RECT_VIP_WEAPON_COLORS[item.id]
              else
             srect_color = RECT_ITEM_COLORS["Weapon"]
          end
        elsif item.is_a?(RPG::Armor)
           if RECT_VIP_ARMORS.include?(item.id)
             srect_color = RECT_VIP_ARMOR_COLORS[item.id]
              else
             srect_color = RECT_ITEM_COLORS["Armor"]
          end
        else
          if RECT_VIP_ITEMS.include?(item.id)
           srect_color = RECT_VIP_ITEM_COLORS[item.id]
            else
           srect_color = RECT_ITEM_COLORS["Default"]
          end
         end
        else
         srect_color = RECT_COLOR
       end
       return srect_color
     end
  #--------------------------------------------------------------------------
  # * Get Mini Icon
  #     item : Item
  #   This is used to get the mini icon displayed.
  #--------------------------------------------------------------------------    
     def self.get_mini_icon(item)
       if item.is_a?(RPG::UsableItem)
        if MINI_VIP_ITEMS.include?(item.id)
           prep_minis = ICY::HM_Style_Items.prep_mini_icon(MINI_VIP_ITEM_ICONS[item.id])
          else
           prep_minis = ICY::HM_Style_Items.prep_mini_icon(MINI_ICON_ITEMS["Usable"])  
        end
      elsif item.is_a?(RPG::Weapon)
        if MINI_VIP_WEAPONS.include?(item.id)
           prep_minis = ICY::HM_Style_Items.prep_mini_icon(MINI_VIP_WEAPON_ICONS[item.id])
          else
           prep_minis = ICY::HM_Style_Items.prep_mini_icon(MINI_ICON_ITEMS["Weapon"])
        end
      elsif item.is_a?(RPG::Armor)
        if MINI_VIP_ARMORS.include?(item.id)
           prep_minis = ICY::HM_Style_Items.prep_mini_icon(MINI_VIP_ARMOR_ICONS[item.id])
          else
           prep_minis = ICY::HM_Style_Items.prep_mini_icon(MINI_ICON_ITEMS["Armor"])
        end
      else
        if MINI_VIP_ITEMS.include?(item.id)
           prep_minis = ICY::HM_Style_Items.prep_mini_icon(MINI_VIP_ITEM_ICONS[item.id])
          else
           prep_minis = ICY::HM_Style_Items.prep_mini_icon(MINI_ICON_ITEMS["Default"]) 
        end
       end
        return prep_minis
    end
      
  end
end

#==============================================================================
# ** ICY_Item_Window_Help
#------------------------------------------------------------------------------
#  This a custom help window used with the HM_Style_Inventory
#==============================================================================

class ICY_Item_Window_Help < Window_Base
  include ICY::HM_Style_Items
  #--------------------------------------------------------------------------
  # * Object Initialization
  #--------------------------------------------------------------------------
  def initialize(x, y, width, height)
    super(x, y, width, height)
    @back_sprite = nil
    if USE_WINDOW_IMAGES
      if WINDOW_IMAGES["Help"] != nil or ""
        self.opacity = 0
        bitmap = Cache.system(WINDOW_IMAGES["Help"])
        @back_sprite = Sprite.new
        @back_sprite.bitmap = bitmap 
      end
    end
    @item = nil
    @align = nil
  end
  
  def dispose
    super
   if USE_WINDOW_IMAGES 
     if WINDOW_IMAGES["Help"] != nil or ""
       @back_sprite.dispose if @back_sprite != nil
     end
   end
  end
  #--------------------------------------------------------------------------
  # * Set Text
  #  text  : character string displayed in window
  #  align : alignment (0..flush left, 1..center, 2..flush right)
  #--------------------------------------------------------------------------
  def set_text(text, align = 0)
    if text != @text or align != @align
      self.contents.clear
      self.contents.font.color = normal_color
      self.contents.draw_text(48, 24, self.width - 90, WLH, text, align)
      @text = text
      @align = align
    end
  end
  
  def set_item(item)
    if item != nil
    self.contents.clear  
    numberbox = Rect.new(ITEM_NUM_POS[0], ITEM_NUM_POS[1], RECT_SIZE, RECT_SIZE)
    outline = Rect.new(0, 0, RECT_SIZE, RECT_SIZE)
    sub_rect = Rect.new((BORDER_SIZE / 2), (BORDER_SIZE / 2), 
       (RECT_SIZE - BORDER_SIZE), (RECT_SIZE - BORDER_SIZE))
    
    if USE_IMAGES and USE_IMG_RECT
      srect_image = ICY::HM_Style_Items.get_rect_image(item)
      outline_sprite = Cache.system(srect_image)
      self.contents.blt(0, 0, outline_sprite, outline)
    end
    
    if USE_BORDER_RECT and RECT_IN_HELP 
      srect_color = ICY::HM_Style_Items.get_rect_color(item)
      outline_sprite = Bitmap.new(RECT_SIZE, RECT_SIZE)
      outline_sprite.fill_rect(outline, srect_color)
      outline_sprite.clear_rect(sub_rect)
      self.contents.blt(0, 0, outline_sprite, outline)
    end
    
    if USE_MINI_ICONS and MINI_ICON_IN_HELP
      prep_minis = ICY::HM_Style_Items.get_mini_icon(item)
      self.contents.blt(MINI_ICON_POS[0], MINI_ICON_POS[1], prep_minis[0], prep_minis[1])
    end
    
    draw_icon(item.icon_index, ITEM_ICON_POS[0], ITEM_ICON_POS[1]) if DRAW_ITEM_ICON_IN_HELP
    number = $game_party.item_number(item)
    self.contents.font.color = system_color
    if DRAW_ITEM_ICON_IN_HELP
      text_x = 48
    else
      text_x = 4
    end
    # Writes Item Name
    self.contents.draw_text(text_x, 0, self.width - 90, WLH, item.name, 0)
    self.contents.font.color = normal_color
    # Writes Item Amount
    old_font_size = self.contents.font.size
    self.contents.font.size = 16
    if DRAW_AMOUNT_FOR_1
      self.contents.draw_text(numberbox, sprintf(ITEM_NUM_FORMAT, number), ITEM_NUM_ALIGN) if ITEM_NUM_IN_HELP
    elsif DRAW_AMOUNT_FOR_1 == false and number > 1 
      self.contents.draw_text(numberbox, sprintf(ITEM_NUM_FORMAT, number), ITEM_NUM_ALIGN) if ITEM_NUM_IN_HELP
    end
    self.contents.font.size = old_font_size
    # Writes Item Description
    old_font_size = self.contents.font.size
    self.contents.font.color = normal_color
    self.contents.font.size = 18
    self.contents.draw_text(text_x + 16, 24, self.width - 90, WLH, item.description, 0)
    self.contents.font.size = old_font_size
    end
  end
  
end

#==============================================================================
# ** ICY_Header_Window
#------------------------------------------------------------------------------
#  This window displays a header.
#==============================================================================
class ICY_Item_Header_Window < Window_Base
  include ICY::HM_Style_Items
  attr_accessor :font_size
  #--------------------------------------------------------------------------
  # * Object Initialization
  #     x      : window x-coordinate
  #     y      : window y-coordinate
  #     width  : window width
  #     height : window height
  #-------------------------------------------------------------------------- 
  def initialize(x, y, width, height)
    super(x, y, width, height)
    @back_sprite = nil
    if USE_WINDOW_IMAGES
      if WINDOW_IMAGES["Header"] != nil or ""
        self.opacity = 0
        bitmap = Cache.system(WINDOW_IMAGES["Header"])
        @back_sprite = Sprite.new
        @back_sprite.bitmap = bitmap 
      end
    end
  end
  #--------------------------------------------------------------------------
  # ** Reset Font Size
  #   Resets the font size to 26
  #--------------------------------------------------------------------------
  def reset_font_size
    @font_size = 26
  end
  #--------------------------------------------------------------------------
  # ** Dispose
  #--------------------------------------------------------------------------
  def dispose
    super
    if USE_WINDOW_IMAGES
      if WINDOW_IMAGES["Header"] != nil or ""
        @back_sprite.dispose if @back_sprite != nil
      end
    end
  end
  #--------------------------------------------------------------------------
  # ** Set Header
  #     text   : text
  #     icon   : icon_index
  #     align  : align
  #-------------------------------------------------------------------------- 
  def set_header(text = "", icon = nil, align = 0)
    icon_offset = 32
    if icon == nil
      x = 0
    else
      x = icon_offset
      draw_icon(icon, 0, 0)
    end
     y = 0
     old_font_size = self.contents.font.size
     self.contents.font.size = 26
     self.contents.font.color = system_color
     self.contents.draw_text(x, y, (self.width - x) - 48, WLH, text, align)
     self.contents.font.color = normal_color
     self.contents.font.size = old_font_size
  end
  
end

#==============================================================================
# ** ICY_SC_Window_Item
#------------------------------------------------------------------------------
#  This is a custom window used with the HM_Style_Inventory
#==============================================================================

class ICY_SC_Window_Item < Window_Selectable
  include ICY::HM_Style_Items
  attr_accessor :column_max
  #--------------------------------------------------------------------------
  # * Object Initialization
  #     x      : window x-coordinate
  #     y      : window y-coordinate
  #     width  : window width
  #     height : window height
  #--------------------------------------------------------------------------
  def initialize(x, y, width, height)
    super(x, y, width, height)
    @back_sprite = nil
    if USE_WINDOW_IMAGES
      if WINDOW_IMAGES["Item"] != nil or ""
        self.opacity = 0
        bitmap = Cache.system(WINDOW_IMAGES["Item"])
        @back_sprite = Sprite.new
        @back_sprite.bitmap = bitmap 
      end
    end
    @column_max = NUM_OF_COLUMN
    self.index = 0
    @currentstore = 0 
    refresh
  end
  
  def dispose
    super
   if USE_WINDOW_IMAGES 
     if WINDOW_IMAGES["Item"] != nil or ""
       @back_sprite.dispose if @back_sprite != nil
     end
   end
  end
  #--------------------------------------------------------------------------
  # * Get Item
  #--------------------------------------------------------------------------
  def item
    return @data[self.index]
  end
  #--------------------------------------------------------------------------
  # * Whether or not to include in item list
  #     item : item
  #--------------------------------------------------------------------------
  def include?(item)
    return false if item == nil
    if $game_temp.in_battle
      return false unless item.is_a?(RPG::Item)
    end
    return true
  end
  #--------------------------------------------------------------------------
  # * Whether or not to display in enabled state
  #     item : item
  #--------------------------------------------------------------------------
  def enable?(item)
    return $game_party.item_can_use?(item)
  end
  #--------------------------------------------------------------------------
  # * Create Window Contents
  #--------------------------------------------------------------------------
  def create_contents
    self.contents.dispose
    self.contents = Bitmap.new(width - 32, [height - 32, row_max * ITEM_SQ_SPACING].max)
  end
  
  def off_x
    return (((self.width - 32) - (ITEM_SQ_SPACING * @column_max)) / 2)
  end
  
  def off_y
    return 0
  end
  #--------------------------------------------------------------------------
  # * Refresh
  #--------------------------------------------------------------------------
  def refresh
    @data = []
    for item in $game_party.items
      next unless include?(item)
      @data.push(item)
      if item.is_a?(RPG::Item) and item.id == $game_party.last_item_id
        self.index = @data.size - 1
      end
    end
    @data.push(nil) if include?(nil)
    @item_max = @data.size
    create_contents
    @reset = off_x
    @nw_x = @reset
    @nw_y = off_y
    @coun = 0
    self.contents.clear
    for i in 0..@item_max
    draw_item(i)
    end
  end
  #--------------------------------------------------------------------------
  # * Draw Icon
  #     icon_index : Icon number
  #     x     : draw spot x-coordinate
  #     y     : draw spot y-coordinate
  #     enabled    : Enabled flag. When false, draw semi-transparently.
  #--------------------------------------------------------------------------
  def draw_icon(icon_index, x, y, enabled = true)
    bitmap = Cache.system("Iconset")
    rect = Rect.new(icon_index % 16 * 24, icon_index / 16 * 24, 24, 24)
    self.contents.blt(x, y, bitmap, rect, enabled ? 255 : UNUSABLE_OPACITY)
  end
  #--------------------------------------------------------------------------
  # * Draw Item
  #     index : item number
  #--------------------------------------------------------------------------
  def draw_item(itemz)
    self.contents.font.color = normal_color 
    item = @data[itemz] 
    if item != nil 
    numberbox = Rect.new(@nw_x + ITEM_NUM_POS[0],@nw_y + ITEM_NUM_POS[1], RECT_SIZE, RECT_SIZE)
    outline = Rect.new(0, 0, RECT_SIZE, RECT_SIZE)
    sub_rect = Rect.new((BORDER_SIZE / 2), (BORDER_SIZE / 2), 
       (RECT_SIZE - BORDER_SIZE), (RECT_SIZE - BORDER_SIZE))
      usable = $game_party.item_can_use?(item)   
    if USE_IMAGES and USE_IMG_RECT
      srect_image = ICY::HM_Style_Items.get_rect_image(item)
      outline_sprite = Cache.system(srect_image)
      self.contents.blt(@nw_x, @nw_y, outline_sprite, outline, usable ? 255 : UNUSABLE_OPACITY)
    end
       
    if USE_BORDER_RECT  
      srect_color = ICY::HM_Style_Items.get_rect_color(item)
      outline_sprite = Bitmap.new(RECT_SIZE, RECT_SIZE)
      outline_sprite.fill_rect(outline, srect_color)
      outline_sprite.clear_rect(sub_rect)
      self.contents.blt(@nw_x, @nw_y, outline_sprite, outline, usable ? 255 : UNUSABLE_OPACITY)
    end
    
    if USE_MINI_ICONS
      prep_minis = ICY::HM_Style_Items.get_mini_icon(item)
      self.contents.blt(@nw_x + MINI_ICON_POS[0], @nw_y + MINI_ICON_POS[1], prep_minis[0], prep_minis[1], usable ? 255 : UNUSABLE_OPACITY)
    end
    
    draw_icon(item.icon_index, @nw_x + ITEM_ICON_POS[0], @nw_y + ITEM_ICON_POS[1], usable) unless @data[itemz] == nil
    number = $game_party.item_number(item)
    # Writes Item Amount
    old_font_size = self.contents.font.size
    self.contents.font.size = 16
    if DRAW_AMOUNT_FOR_1
      self.contents.draw_text(numberbox, sprintf(ITEM_NUM_FORMAT, number), ITEM_NUM_ALIGN) 
    elsif DRAW_AMOUNT_FOR_1 == false and number > 1 
      self.contents.draw_text(numberbox, sprintf(ITEM_NUM_FORMAT, number), ITEM_NUM_ALIGN) 
    end
    self.contents.font.size = old_font_size
    
    @coun += 1
    @nw_x += ITEM_SQ_SPACING
    end
    if @coun == @column_max
      @coun = 0
      @nw_x = @reset
      @nw_y += ITEM_SQ_SPACING
    end
  end
  
  #--------------------------------------------------------------------------
  # * Get Top Row
  #--------------------------------------------------------------------------
  def top_row
    return self.oy / ITEM_SQ_SPACING#WLH
  end
  #--------------------------------------------------------------------------
  # * Set Top Row
  #     row : row shown on top
  #--------------------------------------------------------------------------
  def top_row=(row)
    row = 0 if row < 0
    row = row_max - 1 if row > row_max - 1
    self.oy = row * ITEM_SQ_SPACING#WLH
  end
  #--------------------------------------------------------------------------
  # * Get Number of Rows Displayable on 1 Page
  #--------------------------------------------------------------------------
  def page_row_max
    return (self.height / ITEM_SQ_SPACING) 
  end
  #--------------------------------------------------------------------------
  # * Get Number of Items Displayable on 1 Page
  #--------------------------------------------------------------------------
  def page_item_max
    return page_row_max * @column_max #+ ITEM_SQ_SPACING
  end
  #--------------------------------------------------------------------------
  # * Move cursor one page down
  #--------------------------------------------------------------------------
  def cursor_pagedown
    if top_row + page_row_max < row_max
      @index = [@index + page_item_max, @item_max - 1].min
      self.top_row += page_row_max - ITEM_SQ_SPACING 
    end
  end
  #--------------------------------------------------------------------------
  # * Move cursor one page up
  #--------------------------------------------------------------------------
  def cursor_pageup
    if top_row > 0
      @index = [@index - page_item_max, 0].max
      self.top_row -= page_row_max - ITEM_SQ_SPACING 
    end
  end
  #--------------------------------------------------------------------------
  # * Update cursor
  #--------------------------------------------------------------------------
  def update_cursor
    if @index < 0                   # If the cursor position is less than 0
      self.cursor_rect.empty        # Empty cursor
    else                            # If the cursor position is 0 or more
      row = @index / @column_max    # Get current row
      if row < top_row              # If before the currently displayed
        self.top_row = row          # Scroll up
      end
      if row > bottom_row           # If after the currently displayed
        self.bottom_row = row       # Scroll down
      end
       y_l = @index / @column_max
       y_pos = ((ITEM_SQ_SPACING * y_l)- self.oy) + off_y
       x_l = self.index - (@column_max * y_l)
       x_pos = (x_l * ITEM_SQ_SPACING) + off_x
       if SELECTION_SIZE > RECT_SIZE
         subitive = (SELECTION_SIZE - RECT_SIZE) / 2
         self.cursor_rect.set(x_pos.to_i - subitive, y_pos.to_i - subitive, SELECTION_SIZE, SELECTION_SIZE)
       elsif SELECTION_SIZE < RECT_SIZE
         additive = (RECT_SIZE - SELECTION_SIZE) / 2
         self.cursor_rect.set(x_pos.to_i + additive, y_pos.to_i + additive, SELECTION_SIZE, SELECTION_SIZE)
        else
        self.cursor_rect.set(x_pos.to_i, y_pos.to_i, SELECTION_SIZE, SELECTION_SIZE)
       end
    end
  end
  #--------------------------------------------------------------------------
  # * Update Help Text
  #--------------------------------------------------------------------------
  def update_help
    @help_window.set_text(item == nil ? "" : item.description)
    @help_window.set_item(item) 
  end
end

#==============================================================================
# ** Scene_Item
#------------------------------------------------------------------------------
#  This class performs the item screen processing.
#==============================================================================

class Scene_Item < Scene_Base
  #--------------------------------------------------------------------------
  # * Start processing
  # ** Overwritten
  #--------------------------------------------------------------------------
  def start
    super
    window_sizes = ICY::HM_Style_Items::WINDOW_SIZES
    create_menu_background
    @viewport = Viewport.new(0, 0, 544, 416)
    header_size = window_sizes["Header"]
    @header_window = ICY_Item_Header_Window.new(header_size[0], header_size[1], header_size[2], header_size[3])
    @header_window.opacity = header_size[4]
    if $imported["MainMenuZealous"] 
     icon = YEZ::MENU::MENU_ICONS[:items]
     else
     icon = nil
    end
    @header_window.set_header(Vocab.item, icon, 0)
    help_size = window_sizes["Help"]
    @help_window = ICY_Item_Window_Help.new(help_size[0], help_size[1], help_size[2], help_size[3])
    @help_window.viewport = @viewport
    @help_window.opacity = help_size[4]
    item_win_size = window_sizes["Item"]
    @item_window = ICY_SC_Window_Item.new(item_win_size[0], item_win_size[1], item_win_size[2], item_win_size[3])
    @item_window.viewport = @viewport
    @item_window.opacity = item_win_size[4]
    @item_window.help_window = @help_window
    @item_window.active = false
    @target_window = Window_MenuStatus.new(0, 0)
    if $imported["ICY_HM_Style_Menu_Status"]
      @target_win_help = ICY_Window_Menu_Status_Help.new(@target_window.x, @target_window.height)
      @target_window.help_window = @target_win_help
      @target_win_help.visible = false
    end
    hide_target_window
  end
  #--------------------------------------------------------------------------
  # * Termination Processing
  # ** Aliased
  #--------------------------------------------------------------------------
  alias icy_hm_style_inve_terminate terminate unless $@
  def terminate
    icy_hm_style_inve_terminate
    @header_window.dispose
    if $imported["ICY_HM_Style_Menu_Status"]
      @target_win_help.dispose
    end
  end
  #--------------------------------------------------------------------------
  # * Show Target Window
  #     right : Right justification flag (if false, left justification)
  #--------------------------------------------------------------------------
  alias icy_hm_item_show_target_window show_target_window unless $@
  def show_target_window(*args)
    icy_hm_item_show_target_window(*args)
    if $imported["ICY_HM_Style_Menu_Status"]
      @target_win_help.x = @target_window.x
      @target_win_help.y = @target_window.height
      @target_win_help.visible = true
    end
  end
  
  #--------------------------------------------------------------------------
  # * Hide Target Window
  #--------------------------------------------------------------------------
  alias icy_hm_item_hide_target_window hide_target_window unless $@
  def hide_target_window(*args)
    icy_hm_item_hide_target_window(*args)
    if $imported["ICY_HM_Style_Menu_Status"]
      @target_win_help.visible = false
    end
  end
  #--------------------------------------------------------------------------
  # * Update Target Selection
  #--------------------------------------------------------------------------
  alias icy_hm_item_update_target_selection update_target_selection unless $@  
  def update_target_selection(*args)
    if $imported["ICY_HM_Style_Menu_Status"]
      @target_win_help.update
    end
    icy_hm_item_update_target_selection(*args)
  end
  #--------------------------------------------------------------------------
  # * Update Item Selection
  #--------------------------------------------------------------------------
  def update_item_selection
    if Input.trigger?(Input::B)
      Sound.play_cancel
      return_scene
    elsif Input.trigger?(Input::C)
      @item = @item_window.item
      if @item != nil
        $game_party.last_item_id = @item.id
      end
      if $game_party.item_can_use?(@item)
        Sound.play_decision
        determine_item
      else
        Sound.play_buzzer
      end
    end
  end

  #--------------------------------------------------------------------------
  # * Use Item (apply effects to non-ally targets)
  # ** Editted!
  #--------------------------------------------------------------------------
  def use_item_nontarget
    Sound.play_use_item
    $game_party.consume_item(@item)
    @item_window.refresh #draw_item(@item_window.index)
    @target_window.refresh
    if $game_party.all_dead?
      $scene = Scene_Gameover.new
    elsif @item.common_event_id > 0
      $game_temp.common_event_id = @item.common_event_id
      $scene = Scene_Map.new
    end
  end
end

#------------------------------------------------------------------------------#
# ** KGC_Catergorize_Item Patch
#------------------------------------------------------------------------------#
if ICY::HM_Style_Items::KGC_Catergorize
#==============================================================================
# ■ Window_Item
#==============================================================================

class ICY_SC_Window_Item < Window_Selectable # Made it apart of my Window_Item
  #--------------------------------------------------------------------------
  # ● 公開インスタンス変数
  #--------------------------------------------------------------------------
  attr_reader   :category                 # カテゴリ
  #--------------------------------------------------------------------------
  # ● オブジェクト初期化
  #     x      : ウィンドウの X 座標
  #     y      : ウィンドウの Y 座標
  #     width  : ウィンドウの幅
  #     height : ウィンドウの高さ
  #--------------------------------------------------------------------------
  alias initialize_KGC_CategorizeItem initialize
  def initialize(x, y, width, height)
    @category = 0

    initialize_KGC_CategorizeItem(x, y, width, height)
    refresh
  end
  #--------------------------------------------------------------------------
  # ○ カテゴリ設定
  #--------------------------------------------------------------------------
  def category=(value)
    @category = value
    self.index = 0 # Added this
    refresh
  end
  #--------------------------------------------------------------------------
  # ● アイテムをリストに含めるかどうか
  #     item : アイテム
  #--------------------------------------------------------------------------
  alias include_KGC_CategorizeItem? include?
  def include?(item)
    return false if item == nil

    # 「全種」なら無条件で含める
    if @category == KGC::CategorizeItem::RESERVED_CATEGORY_INDEX["All Items"]
      return true
    end

    result = include_KGC_CategorizeItem?(item)

    unless result
      # 使用可能なら追加候補とする
      if $imported["UsableEquipment"] && $game_party.item_can_use?(item)
        result = true
      end
    end
    # 戦闘外ならカテゴリ一致判定
    unless $game_temp.in_battle
      result &= (item.item_category.include?(@category))
    end

    return result
  end
end
end
################################################################################
#------------------------------------------------------------------------------#
#END\\\END\\\END\\\END\\\END\\\END\\\END///END///END///END///END///END///END///#
#------------------------------------------------------------------------------#
################################################################################
