# ~ICY_Window_MenuStatus
#------------------------------------------------------------------------------#
# ** Icy Menu Status
# ** Created by :IceDragon 
# ** Script-Status : ReWrite
# ** Date Modified : 7/20/2010
# ** Version 1.0
#------------------------------------------------------------------------------#
#------------------------------------------------------------------------------#
# ** This here is a Amped up version of the menu status to match with my item window
#------------------------------------------------------------------------------#
# INTRODUCTION
# So after I kinda played around with my new HM_Style_Items.
# I decided, "Why not mess with the other windows too!"
# And so the birth of this came along!
#------------------------------------------------------------------------------#
# PLACEMENT
# Above 
#   Main
#
# Below
#   Materials
#   Custom Item Windows
#   Custom Skill Windows
#   Custom Menu Systems
#------------------------------------------------------------------------------#
# CHANGES
# This script overwrites a section of the Window_Menu_Status.
#
#------------------------------------------------------------------------------#
# COMPATABILTIES
# Works nicely with ICY_Scene_Skill(HM_Styled) and the ICY_Scene_Item
#------------------------------------------------------------------------------#
# KNOWN ISSUES
# Non at the moment.
#
#------------------------------------------------------------------------------#
# CHANGE LOG
# 7/17/2010 Finished Script V_1.0
#
#------------------------------------------------------------------------------#
$imported = {} if $imported == nil
$imported["ICY_HM_Style_Menu_Status"] = true
#------------------------------------------------------------------------------#
# ** Start Customization
#------------------------------------------------------------------------------#
module ICY
  module Menu_Status
    RECT_SIZE = 72
    BORDER_SIZE = 4
    ITEM_SQ_SPACING = 86
    SELECTION_SIZE = 72
    NUMBER_COLUMNS = 4
  end
end
#------------------------------------------------------------------------------#
# ** End Customization
#------------------------------------------------------------------------------#
# DON'T TOUCH ANYTHING BEYOND THIS POINT!!!!
# UNLESS YOU KNOW WHAT YOUR DOING!
#------------------------------------------------------------------------------#

#==============================================================================
# ** Icy_Window_Menu_Status_Help
#------------------------------------------------------------------------------
#  Exclusive Icy_Menu_Status Script!
#==============================================================================
class ICY_Window_Menu_Status_Help < Window_Base
  include ICY::Menu_Status
  def initialize(x, y)
    super(x, y, 384, 416 / 2)
    @stored_actor = nil
  end
  #--------------------------------------------------------------------------
  # * Draw Sprite Actor = This a X, Y accurate version of the draw_actor_graphic
  #   actor   : actor 
  #     x     : draw spot x-coordinate
  #     y     : draw spot y-coordinate 
  #--------------------------------------------------------------------------
  def draw_sprite_actor(actor, x, y)
    character_name = actor.character_name
    character_index = actor.character_index
    return if character_name == nil
    trim_bitmap = Bitmap.new(32, 32)
    bitmap = Cache.character(character_name)
    sign = character_name[/^[\!\$]./]
    if sign != nil and sign.include?('$')
      cw = bitmap.width / 3
      ch = bitmap.height / 4
    else
      cw = bitmap.width / 12
      ch = bitmap.height / 8
    end
    n = character_index
    src_rect = Rect.new((n%4*3+1)*cw, (n/4*4)*ch, cw, ch)
    trim_bitmap.blt(0, 0, bitmap, src_rect)
    self.contents.blt(x, y, trim_bitmap, trim_bitmap.rect)
  end
  #------------------------------------------------------------------------
  #  * Set Item / Draw_Item
  #------------------------------------------------------------------------
  def set_item(actor)
    return if @stored_actor == actor
    self.contents.clear
    @reset = 0 
    @nw_x = @reset
    @nw_y = 0
    @coun = 0
    outline = Rect.new(0, 0, RECT_SIZE, RECT_SIZE)
    sub_rect = Rect.new((BORDER_SIZE / 2), (BORDER_SIZE / 2), 
    (RECT_SIZE - BORDER_SIZE), (RECT_SIZE - BORDER_SIZE))
       
    srect_color = system_color
    outline_sprite = Bitmap.new(RECT_SIZE, RECT_SIZE)
    outline_sprite.fill_rect(outline, srect_color)
    outline_sprite.clear_rect(sub_rect)
    self.contents.blt(@nw_x, @nw_y, outline_sprite, outline)
    old_font_size = self.contents.font.size
    self.contents.font.size = 21
    self.contents.draw_text(@nw_x + RECT_SIZE + 8, @nw_y, 128, WLH, actor.name)
    self.contents.font.size = old_font_size
    draw_actor_hp(actor, (@nw_x + RECT_SIZE) + 32, @nw_y + 16, 128)
    draw_actor_mp(actor, (@nw_x + RECT_SIZE) + 32, @nw_y + 48, 128)
    
    draw_sprite_actor(actor, @nw_x + (RECT_SIZE / 4), @nw_y + (RECT_SIZE / 4))
    dx = @nw_x
    if $imported["SubclassSelectionSystem"]
      if YE::SUBCLASS::CLASS_ICONS.include?(actor.class.id)
        icon = YE::SUBCLASS::CLASS_ICONS[actor.class.id]
      else
        icon = YE::SUBCLASS::CLASS_ICONS[0]
      end
      draw_icon(icon, dx, (@nw_y + RECT_SIZE) + 4)
      dx += 24
      if actor.subclass != nil
        if YE::SUBCLASS::CLASS_ICONS.include?(actor.subclass.id)
          icon = YE::SUBCLASS::CLASS_ICONS[actor.subclass.id]
        else
          icon = YE::SUBCLASS::CLASS_ICONS[0]
        end
        draw_icon(icon, dx, (@nw_y + RECT_SIZE) + 4)
        dx += 24
      end
    end
    
    old_font_size = self.contents.font.size
    self.contents.font.size = 18
    self.contents.font.color = system_color
    self.contents.draw_text(@nw_x + 8, (@nw_y + RECT_SIZE) - 28, RECT_SIZE, WLH, "#{Vocab::level_a}:#{actor.level}")
    self.contents.font.size = 21
    draw_actor_class(actor, dx, (@nw_y + RECT_SIZE) + 4)
    self.contents.font.color = normal_color
    self.contents.font.size = old_font_size
    draw_equipment(@nw_x, (@nw_y + RECT_SIZE) + 32, actor)
    @stored_actor = actor
  end

  #-------------------------------------------------------------------------
  # * Draw Equipment
  #-------------------------------------------------------------------------
  def draw_equipment(x, y, actor)
    srect_color = system_color
    outline = Rect.new(0, 0, 32, 32)
    sub_rect = Rect.new((4 / 2), (4 / 2),(32 - 4), (32 - 4))
    outline_sprite = Bitmap.new(32, 32)
    outline_sprite.fill_rect(outline, srect_color)
    outline_sprite.clear_rect(sub_rect)
    if $imported["MainMenuZealous"] == true
      draw_icon(YEZ::MENU::MENU_ICONS[:equip], x, y)
    end
    self.contents.draw_text(x + 32, y, self.width, WLH, "#{Vocab.equip}")
    e_x = x
    e_y = y + 32
    for i in actor.equips
      self.contents.blt(e_x, e_y, outline_sprite, outline)
      if i != nil
      draw_icon(i.icon_index, e_x + 4, e_y + 4)
      end
      e_x += 36
    end
  end
  
end

#==============================================================================
# ** Window_MenuStatus
#------------------------------------------------------------------------------
#  This window displays party member status on the menu screen.
#==============================================================================

class Window_MenuStatus < Window_Selectable
  include ICY::Menu_Status
  #--------------------------------------------------------------------------
  # * Object Initialization
  #     x : window X coordinate
  #     y : window Y coordinate
  #--------------------------------------------------------------------------
  def initialize(x, y)
    super(x, y, 384, 416 / 2)
    @column_max = NUMBER_COLUMNS
    self.active = false
    self.index = -1
    refresh
  end
  #--------------------------------------------------------------------------
  # * Draw Sprite Actor = This a X, Y accurate version of the draw_actor_graphic
  #   actor   : actor 
  #     x     : draw spot x-coordinate
  #     y     : draw spot y-coordinate 
  #--------------------------------------------------------------------------
  def draw_sprite_actor(actor, x, y)
    character_name = actor.character_name
    character_index = actor.character_index
    return if character_name == nil
    trim_bitmap = Bitmap.new(32, 32)
    bitmap = Cache.character(character_name)
    sign = character_name[/^[\!\$]./]
    if sign != nil and sign.include?('$')
      cw = bitmap.width / 3
      ch = bitmap.height / 4
    else
      cw = bitmap.width / 12
      ch = bitmap.height / 8
    end
    n = character_index
    src_rect = Rect.new((n%4*3+1)*cw, (n/4*4)*ch, cw, ch)
    trim_bitmap.blt(0, 0, bitmap, src_rect)
    self.contents.blt(x, y, trim_bitmap, trim_bitmap.rect)
  end
  
  #--------------------------------------------------------------------------
  # * Refresh
  #--------------------------------------------------------------------------
  def refresh
    self.contents.clear
    @reset = 0 
    @nw_x = @reset
    @nw_y = 0
    @coun = 0
    @item_max = $game_party.members.size
    @commands = $game_party.members
    for actor in $game_party.members
    draw_item(actor)
  end
end
  #--------------------------------------------------------------------------
  # * Draw Item
  #--------------------------------------------------------------------------
   def draw_item(actor)
    outline = Rect.new(0, 0, RECT_SIZE, RECT_SIZE)
    sub_rect = Rect.new((BORDER_SIZE / 2), (BORDER_SIZE / 2), 
    (RECT_SIZE - BORDER_SIZE), (RECT_SIZE - BORDER_SIZE))
       
    srect_color = system_color
    outline_sprite = Bitmap.new(RECT_SIZE, RECT_SIZE)
    outline_sprite.fill_rect(outline, srect_color)
    outline_sprite.clear_rect(sub_rect)
    self.contents.blt(@nw_x, @nw_y, outline_sprite, outline)
    old_font_size = self.contents.font.size
    self.contents.font.size = 16
    self.contents.draw_text(@nw_x + 8, (@nw_y + RECT_SIZE) - WLH, RECT_SIZE, WLH, actor.name)
    self.contents.font.size = old_font_size
    draw_actor_hp_gauge(actor, (@nw_x + 40), @nw_y - 4, (RECT_SIZE / 2), true)
    draw_actor_mp_gauge(actor, (@nw_x + 52), @nw_y - 4, (RECT_SIZE / 2), true)
    draw_sprite_actor(actor, @nw_x, @nw_y)
    old_font_size = self.contents.font.size
    self.contents.font.size = 14
    self.contents.font.color = system_color
    self.contents.draw_text(@nw_x + 8, @nw_y + (RECT_SIZE / 2), 32, WLH, "#{Vocab::level_a}:#{actor.level}")
    self.contents.font.color = normal_color
    self.contents.font.size = old_font_size
    
    @coun += 1
    @nw_x += ITEM_SQ_SPACING
    if @coun == @column_max
      @coun = 0
      @nw_x = @reset
      @nw_y += ITEM_SQ_SPACING
    end
  end
  
  #--------------------------------------------------------------------------
  # * Get Top Row
  #--------------------------------------------------------------------------
  def top_row
    return self.oy / ITEM_SQ_SPACING#WLH
  end
  #--------------------------------------------------------------------------
  # * Set Top Row
  #     row : row shown on top
  #--------------------------------------------------------------------------
  def top_row=(row)
    row = 0 if row < 0
    row = row_max - 1 if row > row_max - 1
    self.oy = row * ITEM_SQ_SPACING#WLH
  end
  #--------------------------------------------------------------------------
  # * Get Number of Rows Displayable on 1 Page
  #--------------------------------------------------------------------------
  def page_row_max
    return (self.height / ITEM_SQ_SPACING) 
  end
  #--------------------------------------------------------------------------
  # * Get Number of Items Displayable on 1 Page
  #--------------------------------------------------------------------------
  def page_item_max
    return page_row_max * @column_max #+ ITEM_SQ_SPACING
  end
  #--------------------------------------------------------------------------
  # * Move cursor one page down
  #--------------------------------------------------------------------------
  def cursor_pagedown
    if top_row + page_row_max < row_max
      @index = [@index + page_item_max, @item_max - 1].min
      self.top_row += page_row_max - ITEM_SQ_SPACING 
    end
  end
  #--------------------------------------------------------------------------
  # * Move cursor one page up
  #--------------------------------------------------------------------------
  def cursor_pageup
    if top_row > 0
      @index = [@index - page_item_max, 0].max
      self.top_row -= page_row_max - ITEM_SQ_SPACING 
    end
  end
  #--------------------------------------------------------------------------
  # * Update cursor
  #--------------------------------------------------------------------------
  def update_cursor
    if @index < 0                   # If the cursor position is less than 0
      self.cursor_rect.empty        # Empty cursor
    else                            # If the cursor position is 0 or more
      row = @index / @column_max    # Get current row
      if row < top_row              # If before the currently displayed
        self.top_row = row          # Scroll up
      end
      if row > bottom_row           # If after the currently displayed
        self.bottom_row = row       # Scroll down
      end
       y_l = @index / @column_max
       y_pos = (ITEM_SQ_SPACING * y_l)- self.oy
       x_l = self.index - (@column_max * y_l)
       x_pos = x_l * ITEM_SQ_SPACING
       if SELECTION_SIZE > RECT_SIZE
         subitive = (SELECTION_SIZE - RECT_SIZE) / 2
         self.cursor_rect.set(x_pos.to_i - subitive, y_pos.to_i - subitive, SELECTION_SIZE, SELECTION_SIZE)
       elsif SELECTION_SIZE < RECT_SIZE
         additive = (RECT_SIZE - SELECTION_SIZE) / 2
         self.cursor_rect.set(x_pos.to_i + additive, y_pos.to_i + additive, SELECTION_SIZE, SELECTION_SIZE)
        else
        self.cursor_rect.set(x_pos.to_i, y_pos.to_i, SELECTION_SIZE, SELECTION_SIZE)
       end
    end
  end
  
  #--------------------------------------------------------------------------
  # * Update Help Text
  #--------------------------------------------------------------------------
  def update_help
    if @help_window != nil
    @help_window.set_item(@commands[self.index]) 
    end
  end
  
end

#==============================================================================
# ** Scene_Menu
#------------------------------------------------------------------------------
#  This class performs the menu screen processing.
#==============================================================================
class Scene_Menu < Scene_Base
  
  alias icy_hm_style_menstat_start start unless $@
  def start
    icy_hm_style_menstat_start
    @help_window = ICY_Window_Menu_Status_Help.new(0, 416 / 2)
    @status_window.help_window = @help_window
    @help_window.visible = false
  end
  
  alias icy_hm_style_menstat_terminate terminate unless $@
  def terminate
    icy_hm_style_menstat_terminate
    if @help_window != nil
    @help_window.dispose
    end
  end
  #--------------------------------------------------------------------------
  # * Start Actor Selection
  #--------------------------------------------------------------------------
  alias icy_hm_style_menstat_start_actor_selection start_actor_selection unless $@
  def start_actor_selection
    icy_hm_style_menstat_start_actor_selection
    @help_window.visible = true unless @help_window == nil
  end
  #--------------------------------------------------------------------------
  # * End Actor Selection
  #--------------------------------------------------------------------------
  alias icy_hm_style_menstat_end_actor_selection end_actor_selection unless $@
  def end_actor_selection
    icy_hm_style_menstat_end_actor_selection
    @help_window.visible = false unless @help_window == nil
  end
  
  #--------------------------------------------------------------------------
  # * Frame Update
  #--------------------------------------------------------------------------
  alias icy_hm_style_menstat_update update unless $@
  def update
    icy_hm_style_menstat_update
    if @status_window.active
      @status_window.update_help
    end
  end
  
end

#==============================================================================
# ** Scene_Item
#------------------------------------------------------------------------------
#  This class performs the item screen processing.
#==============================================================================
unless $imported["ICY_HM_Style_Inventory"]
class Scene_Item < Scene_Base
  
  alias icy_new_item_menu_status_start start unless $@
  def start(*args)
    icy_new_item_menu_status_start(*args)
     if $imported["ICY_HM_Style_Menu_Status"]
      @target_win_help = ICY_Window_Menu_Status_Help.new(@target_window.x, @target_window.height)
      @target_window.help_window = @target_win_help
      @target_win_help.visible = false
    end
    hide_target_window
  end
  #--------------------------------------------------------------------------
  # * Termination Processing
  # ** Aliased
  #--------------------------------------------------------------------------
  alias icy_hm_style_inve_terminate terminate unless $@
  def terminate
    icy_hm_style_inve_terminate
    if $imported["ICY_HM_Style_Menu_Status"]
      @target_win_help.dispose if @target_win_help != nil
    end
  end
  #--------------------------------------------------------------------------
  # * Show Target Window
  #     right : Right justification flag (if false, left justification)
  #--------------------------------------------------------------------------
  alias icy_hm_item_show_target_window show_target_window unless $@
  def show_target_window(*args)
    icy_hm_item_show_target_window(*args)
    if $imported["ICY_HM_Style_Menu_Status"]
      @target_win_help.x = @target_window.x
      @target_win_help.y = @target_window.height
      @target_win_help.visible = true 
    end
  end
  
  #--------------------------------------------------------------------------
  # * Hide Target Window
  #--------------------------------------------------------------------------
  alias icy_hm_item_hide_target_window hide_target_window unless $@
  def hide_target_window(*args)
    icy_hm_item_hide_target_window(*args)
    if $imported["ICY_HM_Style_Menu_Status"]
      @target_win_help.visible = false if @target_win_help != nil
    end
  end
  #--------------------------------------------------------------------------
  # * Update Target Selection
  #--------------------------------------------------------------------------
  alias icy_hm_item_update_target_selection update_target_selection unless $@  
  def update_target_selection(*args)
    if $imported["ICY_HM_Style_Menu_Status"]
      @target_win_help.update
    end
    icy_hm_item_update_target_selection(*args)
  end
end
end

#==============================================================================
# ** Scene_Skill
#------------------------------------------------------------------------------
#  This class performs the skill screen processing.
#==============================================================================
unless $imported["ICY_HM_Style_Skill_Window"] 
class Scene_Skill < Scene_Base
  
  alias icy_new_skill_menu_status_start start unless $@
  def start(*args)
    icy_new_skill_menu_status_start(*args)
    if $imported["ICY_HM_Style_Menu_Status"]
      @target_win_help = ICY_Window_Menu_Status_Help.new(@target_window.x, @target_window.height)
      @target_window.help_window = @target_win_help
      @target_win_help.visible = false
    end
    hide_target_window
  end
  #--------------------------------------------------------------------------
  # * Termination Processing
  # ** Aliased
  #--------------------------------------------------------------------------
  alias icy_hm_style_skill_terminate terminate unless $@
  def terminate
    icy_hm_style_skill_terminate
    if $imported["ICY_HM_Style_Menu_Status"]
      @target_win_help.dispose if @target_win_help != nil
    end
  end
  #--------------------------------------------------------------------------
  # * Show Target Window
  #     right : Right justification flag (if false, left justification)
  #--------------------------------------------------------------------------
  alias icy_hm_skill_show_target_window show_target_window unless $@
  def show_target_window(*args)
    icy_hm_skill_show_target_window(*args)
    if $imported["ICY_HM_Style_Menu_Status"]
      @target_win_help.x = @target_window.x
      @target_win_help.y = @target_window.height
      @target_win_help.visible = true 
    end
  end
  
  #--------------------------------------------------------------------------
  # * Hide Target Window
  #--------------------------------------------------------------------------
  alias icy_hm_skill_hide_target_window hide_target_window unless $@
  def hide_target_window(*args)
    icy_hm_skill_hide_target_window(*args)
    if $imported["ICY_HM_Style_Menu_Status"]
      @target_win_help.visible = false if @target_win_help != nil
    end
  end
  #--------------------------------------------------------------------------
  # * Update Target Selection
  #--------------------------------------------------------------------------
  alias icy_hm_skill_update_target_selection update_target_selection unless $@  
  def update_target_selection(*args)
    if $imported["ICY_HM_Style_Menu_Status"]
      @target_win_help.update
    end
    icy_hm_skill_update_target_selection(*args)
  end
  
end
end

