# ICY_Random_Hues
#==============================================================================
# ** ICY_Random_Hues
#------------------------------------------------------------------------------
#Created by : IceDragon
#Script-Status : Addon
#Date Created : 4/29/2010
#------------------------------------------------------------------------------
#  This Script applies random hues for enemies specified..
#------------------------------------------------------------------------------
#==============================================================================
#Hues are from 0 - 255
#Well 0 and 255 come to the same thing
#If you go above 255....
#Only the computer would know what would happen...
module ICY
  module Enemy_Rand_Hues
    HuesSet = {
    #GeneralSet
    0 => [0, 50, 100, 150, 200],
    1 => [0, 10, 20, 30, 40, 50],
    2 => [60, 70, 80, 90, 100],
    3 => [110, 120, 130, 140, 150],
    #This is about 16 colors.
    4 => [0, 16, 32, 48, 64, 80, 96, 112, 128, 144, 160, 176, 192, 208, 224, 240]
    }
    Enemies = {
    1 => 4
    }
  end
end

################################################################################
#------------------------------------------------------------------------------#
#END\\\END\\\END\\\END\\\END\\\END\\\END///END///END///END///END///END///END///#
#------------------------------------------------------------------------------#
################################################################################
#==============================================================================
# ** Game_Enemy
#------------------------------------------------------------------------------
#  This class handles enemy characters. It's used within the Game_Troop class
# ($game_troop).
#==============================================================================
class Game_Enemy
  #--------------------------------------------------------------------------
  # * Object Initialization
  #     index    : index in troop
  #     enemy_id : enemy ID
  #--------------------------------------------------------------------------
  alias icy_rand_hues_initialize initialize
  def initialize(index, enemy_id) 
    icy_rand_hues_initialize(index, enemy_id)
    set_icy_hue
  end
  
  def set_icy_hue
    enemy = $data_enemies[@enemy_id]
      @battler_hue = enemy.battler_hue
    @icy_huesset = ICY::Enemy_Rand_Hues::HuesSet
    @icy_hueenemies = ICY::Enemy_Rand_Hues::Enemies
    if @icy_hueenemies.include?(@enemy_id)
      huenum = @icy_hueenemies[@enemy_id]
      hueset = @icy_huesset[huenum]
      huerand = rand(hueset.size)
      @battler_hue = hueset[huerand]
    end
    return @battler_hue
  end
  
end

class Sprite_Battler
  
  alias icy_rand_hues_update_battler_bitmap update_battler_bitmap
  def update_battler_bitmap
    icy_rand_hues_update_battler_bitmap
    @battler_hue = @battler.set_icy_hue
  end
end

if defined?(Sprite_Battler_GTBS) 
class Sprite_Battler_GTBS 
  
  #----------------------------------------------------------------------------
  # Update Bitmap - Updates the battler info and sprite info
  #----------------------------------------------------------------------------
  alias icy_rand_hues_update_bitmap update_bitmap unless $@
  def update_bitmap
    icy_rand_hues_update_bitmap
    if @battler.actor? != true
    @battler_hue = @battler.set_icy_hue
    end
  end
end
end
################################################################################
#------------------------------------------------------------------------------#
#END\\\END\\\END\\\END\\\END\\\END\\\END///END///END///END///END///END///END///#
#------------------------------------------------------------------------------#
################################################################################
