# ~ICY_Scene_Skill_BattleFix
module ICY
  module Battle_HM_Style_Skills
    WINDOW_SIZES = {
    "Help" => [0, 164, 544, 128, 255], # This is the window which shows the description
    "Skills" => [0, 0, 544, 164, 255], # This is the window which shows the items
    }
    COLUMN_MAX = 10
  end
end

class Scene_Battle < Scene_Base
  if $imported["ICY_HM_Style_Skill_Window"] 
  #--------------------------------------------------------------------------
  # * Start Skill Selection
  #--------------------------------------------------------------------------
  def start_skill_selection
    window_sizes = ICY::Battle_HM_Style_Skills::WINDOW_SIZES
    help_size = window_sizes["Help"]
    @help_window = ICY_Skill_Window_Help.new(help_size[0], help_size[1], help_size[2], help_size[3])
    @help_window.opacity = help_size[4]
    skill_window_size = window_sizes["Skills"]
    @skill_window = Window_Skill.new(skill_window_size[0], skill_window_size[1],
    skill_window_size[2], skill_window_size[3], @active_battler)
    @skill_window.opacity = skill_window_size[4]
    @skill_window.column_max = ICY::Battle_HM_Style_Skills::COLUMN_MAX
    @skill_window.refresh
    @skill_window.help_window = @help_window
    @actor_command_window.active = false
  end
  end
end
