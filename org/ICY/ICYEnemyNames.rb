# ICY_EnemyNames
#==============================================================================
# ** ICY_Random_EnemyNames
#------------------------------------------------------------------------------
#Created by : IceDragon
#Script-Status : Addon
#Date Created : 4/25/2010
#------------------------------------------------------------------------------
#  This Script applies random names to enemies
#------------------------------------------------------------------------------
#==============================================================================
#Everyone loves a little randomness in life...
#We get tired of the same old boring stuff...
#This script helps lower that by applying some random names to your enemies!
#This is a perfect addon for TBS games!
module ICY
  module Random_E_Names
    FemaleNames = ['Silene', 'Arisa', 'Kyla', 'Lyn', 'Laren', 'Elisa', 'El', 
                   'Caren', 'Carlene'] 
    MaleNames   = ['Markin', 'Josh', 'Phoros', 'Craig', 'Dermin', 'Lyri', 'Fagan',
                   'Germion']
    Males = [26]
    Females = [25, 27]
    #These are the names used for other enemies not stated in the Males or Females Array
    MonsterNames = ['Grygos', 'Gripio', 'Leios', 'Marras', 'Leerio', 'Luse', 'Mordin',
                    'Lezwert']
    #These are the enemies whose name shouldn't change
    Voids  = [29, 30]
  end
end
################################################################################
#------------------------------------------------------------------------------#
#END\\\END\\\END\\\END\\\END\\\END\\\END///END///END///END///END///END///END///#
#------------------------------------------------------------------------------#
################################################################################
#==============================================================================
# ** Game_Enemy
#------------------------------------------------------------------------------
#  This class handles enemy characters. It's used within the Game_Troop class
# ($game_troop).
#==============================================================================
class Game_Enemy
  
  def icy_enemy_id
    return @enemy_id
    end
  
  def icy_ran_name=(radname)
   @set_rad_name = radname 
 end
 
  def icy_ran_name    
    return @set_rad_name
  end
  #--------------------------------------------------------------------------
  # * Get Display Name
  #--------------------------------------------------------------------------
  def name
    if @plural
      if ICY::Random_E_Names::Voids.include?(@enemy_id)
        return @original_name + letter
      else
        return icy_ran_name 
      end
    else
      if ICY::Random_E_Names::Voids.include?(@enemy_id)
        return @original_name
      else
        return icy_ran_name 
      end
    end
  end
end
################################################################################
#------------------------------------------------------------------------------#
#END\\\END\\\END\\\END\\\END\\\END\\\END///END///END///END///END///END///END///#
#------------------------------------------------------------------------------#
################################################################################
#==============================================================================
# ** Game_Troop
#------------------------------------------------------------------------------
#  This class handles enemy groups and battle-related data. Also performs
# battle events. The instance of this class is referenced by $game_troop.
#==============================================================================
class Game_Troop < Game_Unit
  #--------------------------------------------------------------------------
  # * Generates name from given array
  #--------------------------------------------------------------------------
  def generate_name(sarray)
    newnamepos = rand(sarray.size)
    gname = sarray.at(newnamepos)
    return gname
  end
  #--------------------------------------------------------------------------
  # * Find Duplicates of the name
  #--------------------------------------------------------------------------
  def find_duplicate_names(setname, fullarray, setarray)
    agname = setname
    for i in 0...setarray.size
      if fullarray.include?(setarray)
        return 'Nameless'
       elsif fullarray.include?(agname)
       agname = setarray.at(i)
      else   
       return agname
     end 
    end
  end
  
  #--------------------------------------------------------------------------
  # * Creates the name for the enemies
  #--------------------------------------------------------------------------
  alias icy_make_unique_more_names make_unique_names
  def make_unique_names
    list_o_names = []
    mnamearray = ICY::Random_E_Names::MaleNames.compact
    fnamearray = ICY::Random_E_Names::FemaleNames.compact
    males = ICY::Random_E_Names::Males.compact
    females = ICY::Random_E_Names::Females.compact
    allnames = ICY::Random_E_Names::MonsterNames.compact
    #runs original
    icy_make_unique_more_names
    for enemy in members   
      #Males
    if males.include?(enemy.icy_enemy_id)
      gname = generate_name(mnamearray)
      enemy.icy_ran_name = find_duplicate_names(gname, list_o_names, mnamearray)
      if enemy.icy_ran_name != 'Nameless' or list_o_names.include?(enemy.icy_ran_name)
      list_o_names.push(enemy.icy_ran_name) 
    end
    
      #Females
      elsif females.include?(enemy.icy_enemy_id)
      gname = generate_name(fnamearray)
      enemy.icy_ran_name = find_duplicate_names(gname, list_o_names, fnamearray)
      if enemy.icy_ran_name != 'Nameless' or list_o_names.include?(enemy.icy_ran_name)
      list_o_names.push(enemy.icy_ran_name) 
       end
      
      #Others
      else
      gname = generate_name(allnames)
      enemy.icy_ran_name = find_duplicate_names(gname, list_o_names, allnames)
      if enemy.icy_ran_name != 'Nameless' or list_o_names.include?(enemy.icy_ran_name)
      list_o_names.push(enemy.icy_ran_name) 
       end
      end
    end

  end
  
end
################################################################################
#------------------------------------------------------------------------------#
#END\\\END\\\END\\\END\\\END\\\END\\\END///END///END///END///END///END///END///#
#------------------------------------------------------------------------------#
################################################################################
