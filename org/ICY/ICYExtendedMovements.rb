# ~ICY_Extended_Movements
#==============================================================================#
#=----------------------------------------------------------------------------=#
#= **ICY Extended Movements **                                                =#
#= **Created by : IceDragon **                                                =#
#= **Script-Status : Addon  **                                                =#
#= **Date Created : 5/17/2010 **                                              =#
#= **Date Modified : 7/14/2010 **                                             =#
#= **Version : 0.3 **                                                         =#
#=----------------------------------------------------------------------------=#
#==============================================================================#
# I think there are plenty of better scripts out there that do the same functions
# as this.
# But here's a list of stuff
#
# set_pattern_to    This is a pattern change (Or a frame setter)
# restore_pattern   This restores the pattern to its original one
#
# move_toward_event     Pretty straight forward
# move_away_from_event
#
# jump_forward    Jump forward 2 spaces, if 2nd is impassable jumps only one.
#                 If totally impassable, no jump..
#
# dist2_down       Moves a given distance all of these can be used for 
# dist2_up         normal movement, as it considers imapassability.
# dist2_left       eg. dist_up(12) moves 12 tiles up
# dist2_right      eg. dist_left(4, true) this will move 4 tiles left with
# dist2_forward    direction fix
# dist2_backward
#
# dist_down       Moves a given distance all of these are NOT to be used for 
# dist_up         normal movement, as it disregards imapassability.
# dist_left       eg. dist_up(12) moves 12 tiles up
# dist_right      eg. dist_left(4, true) this will move 4 tiles left with
# dist_forward    direction fix
# dist_backward
#
# half_down       Moves half of a tile
# half_up         eg. half_down moves half tile down
# half_left       eg. half_up(true) moves half tile up with direction fix
# half_right
#
# thrust_down     Doesn't really move at all.
# thrust_up       Does a half_tile in the direction and then the 
# thrust_left     half_tile's oppostite direction, to return to its original pos.
# thrust_right
# thrust_forward
# thrust_backward
#
#==============================================================================#
# To use these
# call a script inside a move route
#==============================================================================#
#
#
# Thou shalt not touch beyond this point unless you know what your doing...
#==============================================================================#
# **Game_Character
#==============================================================================#
class Game_Character
  
  def restore_pattern
    @original_pattern = 1
    @pattern = @original_pattern
  end
  
  def set_pattern_to(sett = @original_pattern)
    settpa = sett
    @pattern = settpa
    @original_pattern = settpa #this is to avoid restoring
    update_animation
  end
  #--------------------------------------------------------------------------
  # * Jump Forward
  #--------------------------------------------------------------------------
  def jump_forward
    case @direction
    when 2
      if $game_map.passable?(@x, @y + 2)
      jump(0, 2)
    elsif $game_map.passable?(@x, @y + 1)
      jump(0, 1)
    end      
    when 4
    if $game_map.passable?(@x - 2, @y)
      jump(-2, 0)
    elsif $game_map.passable?(@x - 1, @y)
      jump(-1, 0)
    end 
    when 6
     if $game_map.passable?(@x + 2, @y)
      jump(2, 0)
    elsif $game_map.passable?(@x + 1, @y)
      jump(1, 0)
    end 
    when 8
     if $game_map.passable?(@x, @y - 2)
      jump(0, -2)
    elsif $game_map.passable?(@x, @y - 1)
      jump(0, -1)
    end 
    end
  end
  
  def dist_down(dist, turn_ok = true)
      turn_down if turn_ok
      @y = $game_map.round_y(@y + dist)
      @real_y = ((@y - dist)*256)
      @move_failed = false
  end

  def dist_left(dist, turn_ok = true)
      turn_left if turn_ok
      @x = $game_map.round_x(@x - dist)
      @real_x = ((@x + dist)*256)
      @move_failed = false
  end

  def dist_right(dist, turn_ok = true)
      turn_right if turn_ok
      @x = $game_map.round_x(@x + dist)
      @real_x = ((@x - dist)*256)
      @move_failed = false
  end

  def dist_up(dist, turn_ok = true)
      turn_up if turn_ok
      @y = $game_map.round_y(@y - dist)
      @real_y = ((@y + dist)*256)
      @move_failed = false
  end
    
  def half_down(turn_ok = true)
      turn_down if turn_ok
      @y = $game_map.round_y(@y + 0.5)
      @real_y = ((@y - 0.5)*256)
      @move_failed = false
  end

  def half_left(turn_ok = true)
      turn_left if turn_ok
      @x = $game_map.round_x(@x-0.5)
      @real_x = ((@x+0.5)*256)
      @move_failed = false
  end

  def half_right(turn_ok = true)
      turn_right if turn_ok
      @x = $game_map.round_x(@x+0.5)
      @real_x = ((@x-0.5)*256)
      @move_failed = false
  end

  def half_up(turn_ok = true)
      turn_up if turn_ok
      @y = $game_map.round_y(@y-0.5)
      @real_y = ((@y+0.5)*256)
      @move_failed = false
  end
    
  def thrust_wait
    @wait_count = 30
    return true
  end
  #--------------------------------------------------------------------------
  # * Thrust Down
  #     turn_ok : Allows change of direction on the spot
  #-------------------------------------------------------------------------- 
  def thrust_down
    dudspeed = @move_speed
    @move_speed = 6
    half_down
    thrust_wait
    half_up(false)
    @move_speed = dudspeed
  end
  #--------------------------------------------------------------------------
  # * Thrust up
  #     turn_ok : Allows change of direction on the spot
  #--------------------------------------------------------------------------  
  def thrust_up
    dudspeed = @move_speed
    @move_speed = 6
    half_up
    thrust_wait
    half_down(false)
    @move_speed = dudspeed
  end
  #--------------------------------------------------------------------------
  # * Thrust Left
  #     turn_ok : Allows change of direction on the spot
  #-------------------------------------------------------------------------- 
  def thrust_left
    dudspeed = @move_speed
    @move_speed = 6
    half_left
    thrust_wait
    half_right(false)
    @move_speed = dudspeed
  end
  #--------------------------------------------------------------------------
  # * Thrust Right
  #     turn_ok : Allows change of direction on the spot
  #--------------------------------------------------------------------------
  def thrust_right
    dudspeed = @move_speed
    @move_speed = 6
    half_right
    thrust_wait
    half_left(false)
    @move_speed = dudspeed
  end
  #--------------------------------------------------------------------------
  # * Thrust Forward
  #--------------------------------------------------------------------------
  def thrust_forward
    case @direction
    when 2;  thrust_down
    when 4;  thrust_left
    when 6;  thrust_right
    when 8;  thrust_up
    end
  end
  #--------------------------------------------------------------------------
  # * Thrust Backward
  #--------------------------------------------------------------------------
  def thrust_backward
    last_direction_fix = @direction_fix
    @direction_fix = true
    case @direction
    when 2;  thrust_up
    when 4;  thrust_right
    when 6;  thrust_left
    when 8;  thrust_down
    end
    @direction_fix = last_direction_fix
  end
  
  #--------------------------------------------------------------------------
  # * Distance Forward
  #--------------------------------------------------------------------------
  def dist_forward(dist)
    case @direction
    when 2;  dist_down(dist)
    when 4;  dist_left(dist)
    when 6;  dist_right(dist)
    when 8;  dist_up(dist)
    end
  end
  #--------------------------------------------------------------------------
  # * Distance Backward
  #--------------------------------------------------------------------------
  def dist_backward(dist)
    last_direction_fix = @direction_fix
    @direction_fix = true
    case @direction
    when 2;  dist_up(dist)
    when 4;  dist_right(dist)
    when 6;  dist_left(dist)
    when 8;  dist_down(dist)
    end
    @direction_fix = last_direction_fix
  end
  
  #--------------------------------------------------------------------------
  # * Distance 2 SET!
  #--------------------------------------------------------------------------
                                 #X, Y, total dist, backward
    def icy_pass_test(x_move, y_move, dist, back = false)
      move_x = x_move
      move_y = y_move
      distanc = dist
      backward = back
      movedist = 0
   if move_x 
   if backward == false
    for i in 1...distanc
       if passable?(@x + i, @y)
         movedist = i
        elsif passable?(@x + i, @y) == false 
         return movedist
       end
     end
   else 
      for i in 1...distanc
       if passable?(@x - i, @y)
         movedist = i
        elsif passable?(@x - i, @y) == false
         return movedist
       end
     end
    end
   elsif move_y 
    if backward == false
      for i in 1...distanc
      if passable?(@x, @y + i)
         movedist = i
        elsif passable?(@x, @y + i) == false
         return movedist
       end
     end
   else
      for i in 1...distanc
       if passable?(@x, @y - i)
         movedist = i
        elsif passable?(@x, @y - i) == false
         return movedist
       end
     end
   end
  end
     return movedist
   end
   
  def dist2_down(dist, turn_ok = true)
      movedist = icy_pass_test(false, true, dist)
      turn_down if turn_ok
      @y = $game_map.round_y(@y + movedist)
      @real_y = ((@y - movedist)*256)
      @move_failed = false
      increase_steps 
  end

  def dist2_left(dist, turn_ok = true)
      movedist = icy_pass_test(true, false, dist, true)
      turn_left if turn_ok
      @x = $game_map.round_x(@x - movedist)
      @real_x = ((@x + movedist)*256)
      @move_failed = false
      increase_steps 
  end

  def dist2_right(dist, turn_ok = true)
     movedist = icy_pass_test(true, false, dist)
      turn_right if turn_ok
      @x = $game_map.round_x(@x + movedist)
      @real_x = ((@x - movedist)*256)
      @move_failed = false
      increase_steps 
  end

  def dist2_up(dist, turn_ok = true)
     movedist = icy_pass_test(false, true, dist, true)
      turn_up if turn_ok
      @y = $game_map.round_y(@y - movedist)
      @real_y = ((@y + movedist)*256)
      @move_failed = false
      increase_steps 
    end
   
  #--------------------------------------------------------------------------
  # * Distance 2 Forward
  #--------------------------------------------------------------------------
  def dist2_forward(dist)
    case @direction
    when 2;  dist2_down(dist)
    when 4;  dist2_left(dist)
    when 6;  dist2_right(dist)
    when 8;  dist2_up(dist)
    end
  end
  #--------------------------------------------------------------------------
  # * Distance 2 Backward
  #--------------------------------------------------------------------------
  def dist2_backward(dist)
    last_direction_fix = @direction_fix
    @direction_fix = true
    case @direction
    when 2;  dist2_up(dist)
    when 4;  dist2_right(dist)
    when 6;  dist2_left(dist)
    when 8;  dist2_down(dist)
    end
    @direction_fix = last_direction_fix
  end
  #--------------------------------------------------------------------------
  # * Calculate X Distance From Event
  #--------------------------------------------------------------------------
  def distance_x_from_event(event)
    sx = @x - $game_map.events[event].x
    if $game_map.loop_horizontal?         # When looping horizontally
      if sx.abs > $game_map.width / 2     # Larger than half the map width?
        sx -= $game_map.width             # Subtract map width
      end
    end
    return sx
  end
  #--------------------------------------------------------------------------
  # * Calculate Y Distance From Event
  #--------------------------------------------------------------------------
  def distance_y_from_event(event)
    sy = @y - $game_map.events[event].y
    if $game_map.loop_vertical?           # When looping vertically
      if sy.abs > $game_map.height / 2    # Larger than half the map height?
        sy -= $game_map.height            # Subtract map height
      end
    end
    return sy
  end
  #--------------------------------------------------------------------------
  # * find_best_path
  #--------------------------------------------------------------------------
  def find_best_path(sx, sy)
     if sx != 0 or sy != 0
      if sx.abs > sy.abs                  # Horizontal distance is longer
        
        if passable?(sx + 1, sy)
          sx > 0 ? move_left : move_right
         elsif passable?(sx, sy + 1)
           sy > 0 ? move_up : move_down
         else
           sx > 0 ? move_left : move_right   # Prioritize left-right
         end
         
        if @move_failed and sy != 0
          sy > 0 ? move_up : move_down
        end
      else                                # Vertical distance is longer
        
        if passable?(sx + 1, sy)
          sy > 0 ? move_up : move_down
         elsif passable?(sx, sy + 1)
           sx > 0 ? move_left : move_right
         else
           sy > 0 ? move_up : move_down      # Prioritize up-down
         end
         
        if @move_failed and sx != 0
          sx > 0 ? move_left : move_right
        end
      end
    end
  end
  #--------------------------------------------------------------------------
  # * Move toward Event
  #--------------------------------------------------------------------------
  def move_toward_event(event = 0)
    sx = distance_x_from_event(event)
    sy = distance_y_from_event(event)
    if sx != 0 or sy != 0
      if sx.abs > sy.abs                  # Horizontal distance is longer
        sx > 0 ? move_left : move_right   # Prioritize left-right
        if @move_failed and sy != 0
          sy > 0 ? move_up : move_down
        end
      else                                # Vertical distance is longer
        sy > 0 ? move_up : move_down      # Prioritize up-down
        if @move_failed and sx != 0
          sx > 0 ? move_left : move_right
        end
      end
    end
  end
  #--------------------------------------------------------------------------
  # * Move away from Event
  #--------------------------------------------------------------------------
  def move_away_from_event(event = 0)
    sx = distance_x_from_event(event)
    sy = distance_x_from_event(event)
    if sx != 0 or sy != 0
      if sx.abs > sy.abs                  # Horizontal distance is longer
        sx > 0 ? move_right : move_left   # Prioritize left-right
        if @move_failed and sy != 0
          sy > 0 ? move_down : move_up
        end
      else                                # Vertical distance is longer
        sy > 0 ? move_down : move_up      # Prioritize up-down
        if @move_failed and sx != 0
          sx > 0 ? move_right : move_left
        end
      end
    end
  end
  
end
################################################################################
#------------------------------------------------------------------------------#
#END\\\END\\\END\\\END\\\END\\\END\\\END///END///END///END///END///END///END///#
#------------------------------------------------------------------------------#
################################################################################
