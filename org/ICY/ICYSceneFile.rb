# ~ICY_Scene_File
#------------------------------------------------------------------------------#
# ** ICY_Scene_File ReWrite
# ** Created by : IceDragon
# ** Script-Status : ReWrite
# ** Date Created : 3/31/2010
# ** Date Modified : 7/19/2010
# ** Vesrion : 2.0
#------------------------------------------------------------------------------#
# ~INTRODUCTION
# ** This is a ReWrite of the Scene_File (The save screen)
# ** There are a couple customiztions for this
# ** This is now a Stand_Alone Script!
#
#------------------------------------------------------------------------------#
# ~CHANGES
# ** This script overwrites the Scene_File.
#
#------------------------------------------------------------------------------#
# ~COMPATABILTIES
# * ICY_Interac_System
# * ICY_Scene_Chapter 
# * ICY_Clan_System
# * YEZ_Main_Menu_Zealous
#------------------------------------------------------------------------------#
# ~KNOWN ISSUES
# ** Non at the moment.
#
#------------------------------------------------------------------------------#
# ~CHANGE LOG
# 3/31/2010 Finished Script V_1.0
# 7/19/2010 Added Compatabilty with Interaction System
#           Fixed up Code
#           Added Customizations
#------------------------------------------------------------------------------#
$imported = {} if $imported == nil
$imported["ICY_Scene_File"] = true
#------------------------------------------------------------------------------#
# ** Start Customization
#------------------------------------------------------------------------------#
module ICY
  module Saving_Scene
    Total_Saves = 8
    Location_Icon = 2040
    Gold_Icon = 205 #If my ICY_Scene_Shop is not present use this icon.
    Save_Present_Icon = 100
    No_Save_Icon = 98
    Save_Data_Name = "Earthen" # If you want it to be default just put "Save"
    LoadData = "Load Game"
    SAVE_WIN_SIZES = {
    "Total" => [20, 56, 196, 56, 255],
    "Save" => [20, 124, 504, 286, 255],
    "Help" => [216, 56, 308, 56, 255],
    "Header" => [0, 0, 544, 56, 255],
    }
  end
end
#------------------------------------------------------------------------------#
# ** End Customization
#------------------------------------------------------------------------------#

#ICY_W1.10
#==============================================================================
# ** ICY_Window_SaveFile
#------------------------------------------------------------------------------
#  This window displays save files on the save and load screens.
#==============================================================================

class ICY_Window_SaveFile < Window_Base
  #--------------------------------------------------------------------------
  # * Public Instance Variables
  #--------------------------------------------------------------------------
  attr_reader   :filename                 # filename
  attr_reader   :file_exist               # file existence flag
  attr_reader   :time_stamp               # timestamp
  attr_reader   :selected                 # selected
  #--------------------------------------------------------------------------
  # * Object Initialization
  #     file_index : save file index (0-3)
  #     filename   : filename
  #--------------------------------------------------------------------------
  def initialize(file_index, filename)
    super(20, 124, 504, 286)
    @file_index = file_index
    @filename = filename
    load_gamedata
    refresh
    @selected = false
  end
  
  def set_coords(coords)
    self.x = coords[0]
    self.y = coords[1]
    self.width = coords[2]
    self.height = coords[3]
    self.opacity = coords[4]
  end
  #--------------------------------------------------------------------------
  # * Load Partial Game Data
  #    By default, switches and variables are not used (for expansion use,
  #    such as displaying place names)
  #--------------------------------------------------------------------------
  def load_gamedata
    @time_stamp = Time.at(0)
    @file_exist = FileTest.exist?(@filename)
    if @file_exist
      file = File.open(@filename, "r")
      @time_stamp = file.mtime
      begin
        @characters     = Marshal.load(file)
        @frame_count    = Marshal.load(file)
        @last_bgm       = Marshal.load(file)
        @last_bgs       = Marshal.load(file)
        @game_system    = Marshal.load(file)
        @game_message   = Marshal.load(file)
        @game_switches  = Marshal.load(file)
        @game_variables = Marshal.load(file)
        
        @game_self_switches  = Marshal.load(file)
        @game_actors         = Marshal.load(file)
        @game_party          = Marshal.load(file)
        @game_troop          = Marshal.load(file)
        @game_map            = Marshal.load(file)
        @game_player         = Marshal.load(file)
    
        @total_sec = @frame_count / Graphics.frame_rate
        
        map_infos = load_data("Data/MapInfos.rvdata")
        mname = map_infos[@game_map.map_id].name.clone
        mname.gsub!(/\\N\[([0-9]+)\]/i) { $game_actors[$1.to_i].name }
        mname.gsub!(/\\PN\[([0-9]+)\]/i) { $game_party.members[$1.to_i].name }
        mname.gsub!(/\\V\[([0-9]+)\]/i) { $game_variables[$1.to_i] } 
        @mapp_name = mname
     rescue
        @file_exist = false
      ensure
        file.close
      end
    end
  end
  #--------------------------------------------------------------------------
  # * Refresh
  #--------------------------------------------------------------------------
  def refresh
    self.contents.clear
    self.contents.font.color = normal_color
    self.contents.font.size = 18
    name = Vocab::File + " #{@file_index + 1}"
    self.contents.draw_text(28, 0, 200, WLH, name)
    
    @name_width = contents.text_size(name).width + 24
    if @file_exist
      draw_icon(ICY::Saving_Scene::Save_Present_Icon,0, 0, true)
      draw_party_characters(24, 58)
      draw_playtime(0, 34, contents.width - 4, 2)
      draw_money(32, self.height - 64)
      draw_map_name(32, self.height - 128)
     # Option for Interaction System     
     if $imported["ICY_Interact_System"] == true 
       draw_partner(246, self.height - 128)
      end
    # Option for Chapter System
     if $imported["ICY_Chapter_Scene"] == true
      draw_current_chapter(32, self.height - 96)
    end
     # Option for Clan System
     if $imported["ICY_Clan_System"] == true
      draw_party_name(246, self.height - 96)
    end
    
    else
      draw_icon(ICY::Saving_Scene::No_Save_Icon, 0, 0, true)
      self.contents.font.size = 64
      self.contents.draw_text((self.width / 4), (self.height / 3), 300, 64, "NO DATA", 0)
    end
  end
  
  #--------------------------------------------------------------------------
  # * Draw Sprite Actor Face
  #     character_name  : Character graphic filename
  #     character_index : Character graphic index
  #     x     : draw spot x-coordinate
  #     y     : draw spot y-coordinate
  #--------------------------------------------------------------------------
  def draw_sprite_actor_face(actor, x, y)
    character_name = actor.character_name
    character_index = actor.character_index
    return if character_name == nil
    trim_bitmap = Bitmap.new(32, 20)
    bitmap = Cache.character(character_name)
    sign = character_name[/^[\!\$]./]
    if sign != nil and sign.include?('$')
      cw = bitmap.width / 3
      ch = bitmap.height / 4
    else
      cw = bitmap.width / 12
      ch = bitmap.height / 8
    end
    n = character_index
    src_rect = Rect.new((n%4*3+1)*cw, (n/4*4)*ch, cw, ch)
    trim_bitmap.blt(0, 0, bitmap, src_rect)
    self.contents.blt(x, y, trim_bitmap, trim_bitmap.rect)
  end
  #--------------------------------------------------------------------------
  # * Draw Partner
  #     x     : draw spot x-coordinate
  #     y     : draw spot y-coordinate
  #-------------------------------------------------------------------------- 
  def draw_partner(x, y)
    if $imported["ICY_Interact_System"] == true
      if @game_party.partner != nil
        partner = $game_actors[@game_party.partner] 
      draw_sprite_actor_face(partner, x, y)
      self.contents.draw_text(x + 32, y, 200, WLH, "Partner: #{partner.name}")
      end
    end
  end
  #--------------------------------------------------------------------------
  # * Draw Party Name
  #     x     : draw spot x-coordinate
  #     y     : draw spot y-coordinate
  #--------------------------------------------------------------------------   
  def draw_party_name(x, y)
    if $imported["ICY_Clan_System"] == true
      if @game_party.party_name != nil
      self.contents.draw_text(x, y, 200, WLH, "Party: #{@game_party.party_name}")
      if @game_party.party_icon != nil
      icon = @game_party.party_icon 
      icon = icon.to_i 
      draw_icon(icon, x - 24, y, true) 
      end
     end
    end
  end
  #--------------------------------------------------------------------------
  # * Draw Current Chapter
  #     x     : draw spot x-coordinate
  #     y     : draw spot y-coordinate
  #--------------------------------------------------------------------------   
  def draw_current_chapter(x, y)
    if $imported["ICY_Chapter_Scene"] == true
    self.contents.font.size = 18
    self.contents.font.color = normal_color
    draw_icon(ICY::Chapters::Chapter_Icon, x - 24, y, true)
    self.contents.draw_text(x, y, 200, WLH, "Chapter : #{@game_system.current_chapter}-#{ICY::Chapters::Chapters[@game_system.current_chapter]}")
    end
  end
  #--------------------------------------------------------------------------
  # * Draw Map Name
  #     x     : draw spot x-coordinate
  #     y     : draw spot y-coordinate
  #--------------------------------------------------------------------------     
  def draw_map_name(x, y)
    draw_icon(ICY::Saving_Scene::Location_Icon, x - 24, y, true)
    self.contents.font.size = 18
    self.contents.draw_text(x, y, 255, WLH, "Location : #{@mapp_name}")
  end
  #--------------------------------------------------------------------------
  # * Draw Money
  #     x     : draw spot x-coordinate
  #     y     : draw spot y-coordinate
  #-------------------------------------------------------------------------- 
  def draw_money(x, y)
    draw_currency_value(@game_party.gold, x, y, 120)
    if $imported["ICY_ISS_Gold"] == true
    draw_icon(ICY::ISS::Goldcon, x - 24, y, true)
      else
     draw_icon(ICY::Saving_Scene::Gold_Icon, x - 24, y, true)
    end
  end
  #--------------------------------------------------------------------------
  # * Draw Party Characters
  #     x : Draw spot X coordinate
  #     y : Draw spot Y coordinate
  #--------------------------------------------------------------------------
  def draw_party_characters(x, y)
    y_move = y 
    chars_count = 0
    i2 = 0
    iset = 0
    for i in 0...@characters.size
      i2 = i 
      i2 -= iset
      name = @characters[i][0]
      index = @characters[i][1]
      draw_character(name, index, x + i2 * 32, y_move)
      chars_count += 1
      if chars_count >= 5
      chars_count = 0
      iset = i2 + 1
      y_move += 32
      end
    end
  end
  #--------------------------------------------------------------------------
  # * Draw Play Time
  #     x : Draw spot X coordinate
  #     y : Draw spot Y coordinate
  #     width : Width
  #     align : Alignment
  #--------------------------------------------------------------------------
  def draw_playtime(x, y, width, align)
    hour = @total_sec / 60 / 60
    min = @total_sec / 60 % 60
    sec = @total_sec % 60
    time_string = sprintf("%02d:%02d:%02d", hour, min, sec)
    self.contents.draw_text(x, y - 24 , width, WLH, "Play Time", 2)
    self.contents.font.color = ICY::Colors::Blue
    self.contents.draw_text(x, y, width, WLH, time_string, 2)
  end
  #--------------------------------------------------------------------------
  # * Set Selected
  #     selected : new selected (true = selected, false = unselected)
  #--------------------------------------------------------------------------
  def selected=(selected)
    @selected = selected
    update_cursor
  end
  #--------------------------------------------------------------------------
  # * Update cursor
  #--------------------------------------------------------------------------
  def update_cursor
    if @selected
      self.cursor_rect.set(0, 0, @name_width + 8, WLH)
    else
      self.cursor_rect.empty
    end
  end
end
#--------------------------------------------------------------------------
# ** Total Saves Window
#--------------------------------------------------------------------------
class ICY_Window_Total_Saves < Window_Base
  attr_accessor :fontsize
  #--------------------------------------------------------------------------
  # * Object Initialization
  #--------------------------------------------------------------------------
  def initialize(x = 0, y = 0, width = 128, height = 128, fontsize = 18)
    super(x, y, width, height)
    @fontsize = fontsize
  end
  
  def set_coords(coords)
    self.x = coords[0]
    self.y = coords[1]
    self.width = coords[2]
    self.height = coords[3]
    self.opacity = coords[4]
  end
  #--------------------------------------------------------------------------
  # * Set Text
  #  text  : character string displayed in window
  #  align : alignment (0..flush left, 1..center, 2..flush right)
  #--------------------------------------------------------------------------
  def set_text(indexy, maxitw)
    if indexy != @indexy 
      self.contents.clear
      self.contents.font.size = @fontsize
      self.contents.font.color = normal_color
      self.contents.draw_text(4, 0, self.width - 40, WLH, "Save #{indexy + 1} out of #{maxitw}", 1)
      @indexy = indexy
    end
  end
  
end
#ICY_W1.3
#==============================================================================
# ** ICY_Window_Help
#------------------------------------------------------------------------------
#  This window shows explanations.
#==============================================================================

class ICY_Window_Help < Window_Base
  #--------------------------------------------------------------------------
  # * Object Initialization
  #--------------------------------------------------------------------------
  def initialize(x, y, width, height, fontsize = 18)
    super(x, y, width, height)
    @fontsize = fontsize
  end
  #--------------------------------------------------------------------------
  # * Set Text
  #  text  : character string displayed in window
  #  align : alignment (0..flush left, 1..center, 2..flush right)
  #--------------------------------------------------------------------------
  def set_text(text, align = 0)
    if text != @text or align != @align
      self.contents.clear
      self.contents.font.size = @fontsize
      self.contents.font.color = normal_color
      self.contents.draw_text(4, 0, self.width - 40, WLH, text, align)
      @text = text
      @align = align
    end
  end
end

#==============================================================================
# ** ICY_Header_Window
#------------------------------------------------------------------------------
#  This window displays a header.
#==============================================================================
class ICY_Header_Window < Window_Base
  attr_accessor :font_size
  #--------------------------------------------------------------------------
  # * Object Initialization
  #     x      : window x-coordinate
  #     y      : window y-coordinate
  #     width  : window width
  #     height : window height
  #-------------------------------------------------------------------------- 
  def initialize(x, y, width, height)
    super(x, y, width, height)
    @font_size = 26
  end
  #--------------------------------------------------------------------------
  # ** Reset Font Size
  #   Resets the font size to 26
  #--------------------------------------------------------------------------
  def reset_font_size
    @font_size = 26
  end
  #--------------------------------------------------------------------------
  # ** Set Header
  #     text   : text
  #     icon   : icon_index
  #     align  : align
  #-------------------------------------------------------------------------- 
  def set_header(text = "" , icon = nil, align = 0)
    icon_offset = 32
    if icon == nil
      x = 0
    else
      x = icon_offset
      draw_icon(icon, 0, 0)
    end
     y = 0
     old_font_size = self.contents.font.size
     self.contents.font.size = @font_size
     self.contents.font.color = system_color
     self.contents.draw_text(x, y, (self.width - x) - 48, WLH, text, align)
     self.contents.font.color = normal_color
     self.contents.font.size = old_font_size
  end
  
end

#==============================================================================
# ** Scene_File
#------------------------------------------------------------------------------
#  This class performs the save and load screen processing.
#==============================================================================
class Scene_File < Scene_Base
  #--------------------------------------------------------------------------
  # * Object Initialization
  #     saving     : save flag (if false, load screen)
  #     from_title : flag: it was called from "Continue" on the title screen
  #     from_event : flag: it was called from the "Call Save Screen" event
  #--------------------------------------------------------------------------
  def initialize(saving, from_title, from_event)
    @saving = saving
    @from_title = from_title
    @from_event = from_event
  end
  #--------------------------------------------------------------------------
  # * Start processing
  #--------------------------------------------------------------------------
  def start
    super
    create_menu_background
    hws = ICY::Saving_Scene::SAVE_WIN_SIZES["Help"]
    @help_window = ICY_Window_Help.new(hws[0],hws[1],hws[2],hws[3]) 
    @help_window.opacity = hws[4]
    headws = ICY::Saving_Scene::SAVE_WIN_SIZES["Header"]
    @header_window = ICY_Header_Window.new(headws[0],headws[1],headws[2],headws[3])
    @header_window.opacity = headws[4]
    create_savefile_windows
    icy_total_saves_win
    
    if $imported["MainMenuZealous"] 
     icon = YEZ::MENU::MENU_ICONS[:save]
     else
     icon = nil
    end
    if @saving
      @index = $game_temp.last_file_index
      @help_window.set_text(Vocab::SaveMessage)
      @header_window.set_header(Vocab.save, icon)
    else
      @index = self.latest_file_index
      @help_window.set_text(Vocab::LoadMessage)
      @header_window.set_header(ICY::Saving_Scene::LoadData, icon)
    end
    @savefile_windows[@index].selected = true
    for i in 0..@item_max - 1
      @savefile_windows[i].visible = false
      end
      @savefile_windows[@index].visible = true
  end
  #--------------------------------------------------------------------------
  # * Termination Processing
  #--------------------------------------------------------------------------
  def terminate
    super
    dispose_menu_background
    @help_window.dispose
    @header_window.dispose
    dispose_item_windows
  end
  #--------------------------------------------------------------------------
  # * Return to Original Screen
  #--------------------------------------------------------------------------
  def return_scene
    if @from_title
      $scene = Scene_Title.new
    elsif @from_event
      $scene = Scene_Map.new
    else
      $scene = Scene_Menu.new(4)
    end
  end
    def icy_total_saves_win
    @total_saves = ICY_Window_Total_Saves.new(20, 56, 196, 56)
    @total_saves.set_coords(ICY::Saving_Scene::SAVE_WIN_SIZES["Total"])
  end
  #--------------------------------------------------------------------------
  # * Frame Update
  #--------------------------------------------------------------------------
  def update
    super
    @total_saves.set_text(@index, @item_max)
    update_menu_background
    @help_window.update
    update_savefile_windows
    update_savefile_selection
  end
  #--------------------------------------------------------------------------
  # * Create Save File Window
  #--------------------------------------------------------------------------
  def create_savefile_windows
    @savefile_windows = []
    for i in 0..ICY::Saving_Scene::Total_Saves - 1
      @savefile_windows.push(ICY_Window_SaveFile.new(i, make_filename(i)))
      @savefile_windows[i].set_coords(ICY::Saving_Scene::SAVE_WIN_SIZES["Save"])
    end
    @item_max = ICY::Saving_Scene::Total_Saves
  end
  #--------------------------------------------------------------------------
  # * Dispose of Save File Window
  #--------------------------------------------------------------------------
  def dispose_item_windows
    @total_saves.dispose
    for window in @savefile_windows
      window.dispose
    end
  end
  #--------------------------------------------------------------------------
  # * Update Save File Window
  #--------------------------------------------------------------------------
  def update_savefile_windows
    for window in @savefile_windows
      window.update
    end
  end
  #--------------------------------------------------------------------------
  # * Update Save File Selection
  #--------------------------------------------------------------------------
  def update_savefile_selection
    if Input.trigger?(Input::C)
      determine_savefile
    elsif Input.trigger?(Input::B)
      Sound.play_cancel
      return_scene
    else
      last_index = @index
      if Input.repeat?(Input::DOWN)
        cursor_down(Input.trigger?(Input::DOWN))
      end
      if Input.repeat?(Input::UP)
        cursor_up(Input.trigger?(Input::UP))
      end
      if @index != last_index
        Sound.play_cursor
        @savefile_windows[last_index].selected = false
        @savefile_windows[@index].selected = true
      end
    end
  end
  #--------------------------------------------------------------------------
  # * Confirm Save File
  #--------------------------------------------------------------------------
  def determine_savefile
    if @saving
      Sound.play_save
      do_save
    else
      if @savefile_windows[@index].file_exist
        Sound.play_load
        do_load
      else
        Sound.play_buzzer
        return
      end
    end
    $game_temp.last_file_index = @index
  end
  #--------------------------------------------------------------------------
  # * Move cursor down
  #     wrap : Wraparound allowed
  #--------------------------------------------------------------------------
  def cursor_down(wrap)
    if @index < @item_max - 1 or wrap
      @index = (@index + 1) % @item_max
    end
      for i in 0..@item_max - 1
      @savefile_windows[i].visible = false
      end
      @savefile_windows[@index].visible = true
  end
  #--------------------------------------------------------------------------
  # * Move cursor up
  #     wrap : Wraparound allowed
  #--------------------------------------------------------------------------
  def cursor_up(wrap)
    if @index > 0 or wrap
      @index = (@index - 1 + @item_max) % @item_max
    end
      for i in 0..@item_max - 1
      @savefile_windows[i].visible = false
    end
    @savefile_windows[@index].visible = true
  end

  #--------------------------------------------------------------------------
  # * Create Filename
  #     file_index : save file index (0-3)
  #--------------------------------------------------------------------------
  def make_filename(file_index)
    return "#{ICY::Saving_Scene::Save_Data_Name}#{file_index + 1}.rvdata"
  end
  #--------------------------------------------------------------------------
  # * Select File With Newest Timestamp
  #--------------------------------------------------------------------------
  def latest_file_index
    index = 0
    latest_time = Time.at(0)
    for i in 0...@savefile_windows.size
      if @savefile_windows[i].time_stamp > latest_time
        latest_time = @savefile_windows[i].time_stamp
        index = i
      end
    end
    return index
  end
  #--------------------------------------------------------------------------
  # * Execute Save
  #--------------------------------------------------------------------------
  def do_save
    file = File.open(@savefile_windows[@index].filename, "wb")
    write_save_data(file)
    file.close
    return_scene
  end
  #--------------------------------------------------------------------------
  # * Execute Load
  #--------------------------------------------------------------------------
  def do_load
    file = File.open(@savefile_windows[@index].filename, "rb")
    read_save_data(file)
    file.close
    $scene = Scene_Map.new
    RPG::BGM.fade(900)
    Graphics.fadeout(60)
    @last_bgm.play
    @last_bgs.play
  end
end

#-----------------------------------------------------------------------------
#  ** Scene_Title Patch to fix continue 
#-----------------------------------------------------------------------------
class Scene_Title < Scene_Base
  #--------------------------------------------------------------------------
  # * Determine if Continue is Enabled
  #--------------------------------------------------------------------------
  def check_continue
    @continue_enabled = (Dir.glob("#{ICY::Saving_Scene::Save_Data_Name}*.rvdata").size > 0)
  end
end
