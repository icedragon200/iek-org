# ~ICY_Icon_Events
#==============================================================================#
#------------------------------------------------------------------------------#
# ** Icon Hover
# ** Created by : IceDragon
# ** Script-Status : Snippet
# ** Date Created : (I don't remember!)
# ** Date Modified : 7/17/2010
# ** Version : 1.1
#------------------------------------------------------------------------------#
#==============================================================================#
# THERE ARE NO CUSTOMIZATIONS FOR THIS!!!
# How to use?
# In an event..
# Use a Script with this in it
#
#------------------------------------------------------------------------------#
# icy_set_icon_event(char_index, active, icon)
# char_index is for the event it should show up over
#   -1 for player 
#    0 for current event
#    1 and above for other events
# active Should the icon be shown or not
#
# icon The icon_index in the IconSet.png
#
# icy_set_item_icon(char_index, active, type, item_id)
#  type 
#   0 - Item
#   1 - Weapon
#   2 - Armor
#
#------------------------------------------------------------------------------#
# CHANGE LOG
# V 1.0 Finished Script
# V 1.1 Added icy_set_item_icon
#       fixed x_offset
#
#------------------------------------------------------------------------------#
# KNOWN ISSUES
# Non at the moment.
#
#------------------------------------------------------------------------------#
# Touch not beyond this point, unless you know what your doing
#==============================================================================
# ** Game Character
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#==============================================================================
class Game_Character
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # * Public Instance Variable
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  attr_accessor :icy_icon      # The base icon name
  attr_accessor :icy_icon_active
end

#==============================================================================
# ** Game Interpreter
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#==============================================================================

class Game_Interpreter
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # * Set Event Icon
  #    char_index   : the index of the character
  #    Icon         : the icon_index
  #    active       : Active
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  def icy_set_icon_event(char_index = 0, active = false, icon = 0)
    # Get Character
    char_index = @event_id if char_index == 0
    target = char_index == -1 ? $game_player : $game_map.events[char_index]
    return if target == nil
    target.icy_icon = icon
    target.icy_icon_active = active
  end
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # * Set Item Icon
  #    char_index   : the index of the character
  #    Icon         : the icon_index
  #    active       : Active
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  def icy_set_item_icon(char_index = 0, active = false, type = 0, id = 0)
    # Get Character
    char_index = @event_id if char_index == 0
    target = char_index == -1 ? $game_player : $game_map.events[char_index]
    return if target == nil
    icon = 0
    icon = $data_items[id].icon_index if type == 0 
    icon = $data_weapons[id].icon_index if type == 1
    icon = $data_armors[id].icon_index if type == 2
    target.icy_icon = icon
    target.icy_icon_active = active
   end
 end
 
#==============================================================================
# ** Sprite Character
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#============================================================================== 
class Sprite_Character < Sprite_Base
  alias icy_icon_event_initialize initialize
  def initialize(*args)
    icy_icon_event_initialize(*args)
    @icy_icon_last_xy = []
    @event_iconz = nil
  end
  
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # * Dispose
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  alias icy_icon_event_dispose dispose
  def dispose
    # Dispose icon
    icy_dispose_icon_event
    # Run Original Method
    icy_icon_event_dispose
  end
  
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # * Frame Update
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  alias icy_icon_event_update update
  def update
    # Run Original Method
    icy_icon_event_update
    # Update event icons
      icy_start_icon_event if @event_iconz == nil and @character.icy_icon_active

      icy_update_icon_event
  end

  def icy_start_icon_event 
    icy_dispose_icon_event
    @event_iconz = ::Sprite.new(viewport)
    begin
    bicon = Cache.system("Iconset")
    @event_iconz.bitmap = Bitmap.new(32, 32)
    icon_index = @character.icy_icon
    rect = Rect.new(icon_index % 16 * 24, icon_index / 16 * 24, 24, 24)
    @event_iconz.bitmap.blt(0, 0, bicon, rect)
    end
    icy_icon_refresh
   end
  
  def icy_icon_refresh
     if self.bitmap == nil
      @event_iconz.x = self.x - (@event_iconz.bitmap.width / 2)
      @event_iconz.y = self.y - self.height + 6 
      @event_iconz.z = self.z + 2
    else
      x_off = (@event_iconz.bitmap.width / 2)
      @event_iconz.x = (self.x - x_off) + 4
      @event_iconz.y = self.y - self.height - @event_iconz.bitmap.height + 6
      @event_iconz.z = self.z + 2
      end
      @icy_icon_last_xy = [self.x, self.y]
      
  end
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # * Update Hover Graphic
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  def icy_update_icon_event 
    return if @event_iconz == nil
    icy_icon_refresh if something_changed?    
  end
  
  def something_changed?
    return true if @icy_icon_last_xy != [self.x, self.y]
    return false
  end
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # * Dispose Hover Graphic
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  def icy_dispose_icon_event
    if @event_iconz != nil
       @event_iconz.dispose
      @event_iconz = nil
      @icy_icon_last_xy = nil
    end
  end

end
