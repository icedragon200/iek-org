# ~ICY_Scene_Shop
#------------------------------------------------------------------------------#
# ** ICY_Scene_Shop ReWrite
# ** Created by : IceDragon
# ** Script-Status : ReWrite
# ** Date Created : 3/28/2010
# ** Date Modified : 7/17/2010
#------------------------------------------------------------------------------#
# This is a simple Scene_Shop ReWrite
# This script is now a Stand Alone.
#
module ICY
  module ICY_SHOP
    BUYCON   = 144
    SELLCON  = 147
    CANCELCON= 213
    #If this is enabled, all windows will be fully transperent and will only show
    #the contents
    Blank_Out = false
    TABS = 1
  end
end

#ICY_W1.7
#==============================================================================
# ** ICY_Window_Current_Items
#------------------------------------------------------------------------------
#  This window displays items in possession for selling on the shop screen.
#==============================================================================

class ICY_Window_CurrentItems < ICY_Window_Item
  #--------------------------------------------------------------------------
  # * Object Initialization
  #     x      : window x-coordinate
  #     y      : window y-coordinate
  #     width  : window width
  #     height : window height
  #--------------------------------------------------------------------------
  def initialize(x, y, width, height)
    super(x, y, width, height)
  end
  #--------------------------------------------------------------------------
  # * Whether or not to include in item list
  #     item : item
  #--------------------------------------------------------------------------
  def include?(item)
    return item != nil
  end
  #--------------------------------------------------------------------------
  # * Whether or not to display in enabled state
  #     item : item
  #--------------------------------------------------------------------------
  def enable?(item)
    return (item.price > 0)
  end
end

#ICY_W1.8

#==============================================================================
# ** ICY_Window_Gold
#------------------------------------------------------------------------------
#  This window displays the amount of gold.
#==============================================================================

class ICY_Window_Gold < Window_Base
  #--------------------------------------------------------------------------
  # * Object Initialization
  #     x : window X coordinate
  #     y : window Y coordinate
  #--------------------------------------------------------------------------
  def initialize(x, y)
    super(x, y, 160, WLH + 32)
    refresh
  end
  #--------------------------------------------------------------------------
  # * Refresh
  #--------------------------------------------------------------------------
  def refresh
    self.contents.clear
    draw_currency_value($game_party.gold, 4, 0, 120)
    draw_icon(ICY::ISS::Goldcon, 0, 0, true)
  end
end

#ICY_W1.9
#==============================================================================
# ** ICY_Window_ShopBuy
#------------------------------------------------------------------------------
#  This window displays buyable goods on the shop screen.
#==============================================================================

class ICY_Window_ShopBuy < ICY_Window_Selectable
  #--------------------------------------------------------------------------
  # * Object Initialization
  #     x : window X coordinate
  #     y : window Y coordinate
  #--------------------------------------------------------------------------
  def initialize(x, y, width, height, columns = 1, spacing = 32)
    super(x, y, width, height, columns)
    @shop_goods = $game_temp.shop_goods
    @columns = columns
    refresh
    self.index = 0
  end
  #--------------------------------------------------------------------------
  # * Get Item
  #--------------------------------------------------------------------------
  def item
    return @data[self.index]
  end
  #--------------------------------------------------------------------------
  # * Refresh
  #--------------------------------------------------------------------------
  def refresh
    @data = []
    for goods_item in @shop_goods
      case goods_item[0]
      when 0
        item = $data_items[goods_item[1]]
      when 1
        item = $data_weapons[goods_item[1]]
      when 2
        item = $data_armors[goods_item[1]]
      end
      if item != nil
        @data.push(item)
      end
    end
    @item_max = @data.size
    create_contents
    for i in 0...@item_max
      draw_item(i)
    end
  end
  #--------------------------------------------------------------------------
  # * Draw Item
  #     index : item number
  #--------------------------------------------------------------------------
  def draw_item(index)
    item = @data[index]
    number = $game_party.item_number(item)
    enabled = (item.price <= $game_party.gold and number < 99)
    rect = item_rect(index)
    self.contents.clear_rect(rect)
    draw_item_name(item, rect.x, rect.y, enabled)
    rect.width -= 4
    self.contents.draw_text(rect.x - 24, rect.y, rect.width, rect.height, item.price, 2)
    if @columns > 1
     draw_icon (ICY::ISS::Goldcon, rect.x + (rect.width - 24),rect.y, enabled)
     else 
    self.draw_icon (ICY::ISS::Goldcon, rect.width - 24, rect.y, enabled)
    end
  end
  #--------------------------------------------------------------------------
  # * Help Text Update
  #--------------------------------------------------------------------------
  def update_help
    @help_window.set_text(item == nil ? "" : item.description)
  end
end

#ICY_W1.2
#==============================================================================
# ** ICY_Window_Command
#------------------------------------------------------------------------------
#  This window deals with general command choices.
#==============================================================================

class ICY_Window_Command < Window_Selectable
  #--------------------------------------------------------------------------
  # * Public Instance Variables
  #--------------------------------------------------------------------------
  attr_reader   :commands                 # command
  #--------------------------------------------------------------------------
  # * Object Initialization
  #     width      : window width
  #     commands   : command string array
  #     commandcons: command icon array
  #     column_max : digit count (if 2 or more, horizontal selection)
  #     row_max    : row count (0: match command count)
  #     spacing    : blank space when items are arrange horizontally
  #--------------------------------------------------------------------------
  def initialize(width, commands, commandcons, fontsize = 18, column_max = 1, row_max = 0, spacing = 32)
    if row_max == 0
      row_max = (commands.size + column_max - 1) / column_max
    end
    super(0, 0, width, row_max * WLH + 32, spacing)
    @commands = commands
    @item_max = commands.size
    @column_max = column_max
    @commandcons = commandcons
    @fontsize = fontsize
    refresh
    self.index = 0
  end
  #--------------------------------------------------------------------------
  # * Refresh
  #--------------------------------------------------------------------------
  def refresh
   self.contents.clear
    for i in 0...@item_max
      command = @commands[i]
      icon = @commandcons[command]        
       icy_recdraw_item(i, icon)
    end
  end
  #--------------------------------------------------------------------------
  # * Draw Item
  #     index   : item number
  #     enabled : enabled flag. When false, draw semi-transparently.
  #--------------------------------------------------------------------------
  def icy_recdraw_item(index, icon_index = 0, enabled = true)
    rect = item_rect(index)
    rect.x += 24
    rect.width -= 8
    if icon_index == nil
      icon_index = 0
    end
    
    if @column_max == 1
    draw_icon(icon_index, x, rect.y, enabled)
     else
    draw_icon(icon_index, (rect.x - 24), y, enabled)
    end
  
    self.contents.clear_rect(rect)
    self.contents.font.size = @fontsize
    self.contents.font.color = normal_color
    self.contents.font.color.alpha = enabled ? 255 : 128
    self.contents.draw_text(rect, @commands[index])
  end
end

#ICY_W1.3
#==============================================================================
# ** ICY_Window_Help
#------------------------------------------------------------------------------
#  This window shows explanations.
#==============================================================================

class ICY_Window_Help < Window_Base
  #--------------------------------------------------------------------------
  # * Object Initialization
  #--------------------------------------------------------------------------
  def initialize(x, y, width, height, fontsize = 18)
    super(x, y, width, height)
    @fontsize = fontsize
  end
  #--------------------------------------------------------------------------
  # * Set Text
  #  text  : character string displayed in window
  #  align : alignment (0..flush left, 1..center, 2..flush right)
  #--------------------------------------------------------------------------
  def set_text(text, align = 0)
    if text != @text or align != @align
      self.contents.clear
      self.contents.font.size = @fontsize
      self.contents.font.color = normal_color
      self.contents.draw_text(4, 0, self.width - 40, WLH, text, align)
      @text = text
      @align = align
    end
  end
end
#==============================================================================
# ** ICY_Scene_Shop
#------------------------------------------------------------------------------
#  This class performs shop screen processing.
#==============================================================================
class Scene_Shop < Scene_Base
  #--------------------------------------------------------------------------
  # * Start processing
  #--------------------------------------------------------------------------
  def start
    super
    create_menu_background
    create_command_window
    @help_window = ICY_Window_Help.new(136, 64, 406, 64)
    @gold_window = ICY_Window_Gold.new(374, 10)
    @dummy_window = ICY_Window_ShopBuy.new(10, 112, 524, 304, ICY::ICY_SHOP::TABS)
    @dummy_window.index = -1
    @dummy_window.active = false
    @buy_window = ICY_Window_ShopBuy.new(252, 112, 288, 210, 1)
    @buy_window.active = false
    @buy_window.visible = false
    @buy_window.help_window = @help_window
    @sell_window = ICY_Window_CurrentItems.new(0, 112, 304, 304)
    @sell_window.active = false
    @sell_window.visible = false
    @sell_window.help_window = @help_window
    @number_window = Window_ShopNumber.new(252, 112)
    @number_window.active = false
    @number_window.visible = false
    @status_window = Window_ShopStatus.new(10, 112)
    @status_window.visible = false
    if ICY::ICY_SHOP::Blank_Out
    blank_out
    end
  end
  
  def blank_out
    @command_window.opacity = 0
    @help_window.opacity = 0
    @gold_window.opacity = 0
    @dummy_window.opacity = 0
    @buy_window.opacity = 0
    @sell_window.opacity = 0
    @number_window.opacity = 0
    @status_window.opacity = 0
  end
  #--------------------------------------------------------------------------
  # * Termination Processing
  #--------------------------------------------------------------------------
  def terminate
    super
    dispose_menu_background
    dispose_command_window
    @help_window.dispose
    @gold_window.dispose
    @dummy_window.dispose
    @buy_window.dispose
    @sell_window.dispose
    @number_window.dispose
    @status_window.dispose
  end
  #--------------------------------------------------------------------------
  # * Frame Update
  #--------------------------------------------------------------------------
  def update
    super
    update_menu_background
    @help_window.update
    @command_window.update
    @gold_window.update
    @dummy_window.update
    @buy_window.update
    @sell_window.update
    @number_window.update
    @status_window.update
    if @command_window.active
      update_command_selection
    elsif @buy_window.active
      update_buy_selection
    elsif @sell_window.active
      update_sell_selection
    elsif @number_window.active
      update_number_input
    end
  end
  #--------------------------------------------------------------------------
  # * Create Command Window
  #--------------------------------------------------------------------------
  def create_command_window
    i1 = ICY::ICY_SHOP::BUYCON
    i2 = ICY::ICY_SHOP::SELLCON
    i3 = ICY::ICY_SHOP::CANCELCON
    s1 = Vocab::ShopBuy
    s2 = Vocab::ShopSell
    s3 = Vocab::ShopCancel
    icoso = {s1 => i1, s2 => i2, s3 => i3}    
    @command_window = ICY_Window_Command.new(126, [s1, s2, s3],icoso, 16)
    @command_window.y = 10
    @command_window.x = 10
    if $game_temp.shop_purchase_only
      @command_window.draw_item(1, false)
    end
  end
  #--------------------------------------------------------------------------
  # * Dispose of Command Window
  #--------------------------------------------------------------------------
  def dispose_command_window
    @command_window.dispose
  end
  #--------------------------------------------------------------------------
  # * Update Command Selection
  #--------------------------------------------------------------------------
  def update_command_selection
    if Input.trigger?(Input::B)
      Sound.play_cancel
      $scene = Scene_Map.new
    elsif Input.trigger?(Input::C)
      case @command_window.index
      when 0  # buy
        Sound.play_decision
        @command_window.active = false
        @dummy_window.visible = false
        @buy_window.active = true
        @buy_window.visible = true
        @buy_window.refresh
        @status_window.visible = true
      when 1  # sell
        if $game_temp.shop_purchase_only
          Sound.play_buzzer
        else
          Sound.play_decision
          @command_window.active = false
          @dummy_window.visible = false
          @sell_window.active = true
          @sell_window.visible = true
          @sell_window.refresh
        end
      when 2  # Quit
        Sound.play_decision
        $scene = Scene_Map.new
      end
    end
  end
  #--------------------------------------------------------------------------
  # * Update Buy Item Selection
  #--------------------------------------------------------------------------
  def update_buy_selection
    @status_window.item = @buy_window.item
    if Input.trigger?(Input::B)
      Sound.play_cancel
      @command_window.active = true
      @dummy_window.visible = true
      @buy_window.active = false
      @buy_window.visible = false
      @status_window.visible = false
      @status_window.item = nil
      @help_window.set_text("")
      return
    end
    if Input.trigger?(Input::C)
      @item = @buy_window.item
      number = $game_party.item_number(@item)
      if @item == nil or @item.price > $game_party.gold or number == 99
        Sound.play_buzzer
      else
        Sound.play_decision
        max = @item.price == 0 ? 99 : $game_party.gold / @item.price
        max = [max, 99 - number].min
        @buy_window.active = false
        @buy_window.visible = false
        @number_window.set(@item, max, @item.price)
        @number_window.active = true
        @number_window.visible = true
      end
    end
  end
  #--------------------------------------------------------------------------
  # * Update Sell Item Selection
  #--------------------------------------------------------------------------
  def update_sell_selection
    if Input.trigger?(Input::B)
      Sound.play_cancel
      @command_window.active = true
      @dummy_window.visible = true
      @sell_window.active = false
      @sell_window.visible = false
      @status_window.item = nil
      @help_window.set_text("")
    elsif Input.trigger?(Input::C)
      @item = @sell_window.item
      @status_window.item = @item
      if @item == nil or @item.price == 0
        Sound.play_buzzer
      else
        Sound.play_decision
        max = $game_party.item_number(@item)
        @sell_window.active = false
        @sell_window.visible = false
        @number_window.set(@item, max, @item.price / 2)
        @number_window.active = true
        @number_window.visible = true
        @status_window.visible = true
      end
    end
  end
  #--------------------------------------------------------------------------
  # * Update Number Input
  #--------------------------------------------------------------------------
  def update_number_input
    if Input.trigger?(Input::B)
      cancel_number_input
    elsif Input.trigger?(Input::C)
      decide_number_input
    end
  end
  #--------------------------------------------------------------------------
  # * Cancel Number Input
  #--------------------------------------------------------------------------
  def cancel_number_input
    Sound.play_cancel
    @number_window.active = false
    @number_window.visible = false
    case @command_window.index
    when 0  # Buy
      @buy_window.active = true
      @buy_window.visible = true
    when 1  # Sell
      @sell_window.active = true
      @sell_window.visible = true
      @status_window.visible = false
    end
  end
  #--------------------------------------------------------------------------
  # * Confirm Number Input
  #--------------------------------------------------------------------------
  def decide_number_input
    Sound.play_shop
    @number_window.active = false
    @number_window.visible = false
    case @command_window.index
    when 0  # Buy
      $game_party.lose_gold(@number_window.number * @item.price)
      $game_party.gain_item(@item, @number_window.number)
      @gold_window.refresh
      @buy_window.refresh
      @status_window.refresh
      @buy_window.active = true
      @buy_window.visible = true
    when 1  # sell
      $game_party.gain_gold(@number_window.number * (@item.price / 2))
      $game_party.lose_item(@item, @number_window.number)
      @gold_window.refresh
      @sell_window.refresh
      @status_window.refresh
      @sell_window.active = true
      @sell_window.visible = true
      @status_window.visible = false
    end
  end
end
