# ICY_Window_Base_Extend
#==============================================================================
# ** Window_Base
#------------------------------------------------------------------------------
#  This is a superclass of all windows in the game.
#  IceDragon - Adding some new stuff to it.
#==============================================================================
class Window_Base < Window
  #--------------------------------------------------------------------------
  # * Draw Actor Sprite = This a X, Y accurate version of the draw_actor_graphic
  #   actor   : actor 
  #     x     : draw spot x-coordinate
  #     y     : draw spot y-coordinate 
  #--------------------------------------------------------------------------
  def draw_actor_sprite(actor, x, y, enabled = true)
    character_name = actor.character_name
    character_index = actor.character_index
    return if character_name == nil
    bitmap = Cache.character(character_name)
    sign = character_name[/^[\!\$]./]
    if sign != nil and sign.include?('$')
      cw = bitmap.width / 3
      ch = bitmap.height / 4
    else
      cw = bitmap.width / 12
      ch = bitmap.height / 8
    end
    n = character_index
    trim_bitmap = Bitmap.new(cw, ch)
    src_rect = Rect.new((n%4*3+1)*cw, (n/4*4)*ch, cw, ch)
    trim_bitmap.blt(0, 0, bitmap, src_rect)
    dy = y - (ch - 32)
    dx = x - (cw - 32)
    self.contents.blt(dx, dy, trim_bitmap, trim_bitmap.rect, enabled ? 255 : 128)
  end
  #--------------------------------------------------------------------------
  # * Draw Format Text
  #     x     : draw spot x-coordinate
  #     y     : draw spot y-coordinate
  #    width  : draw_text width
  #    height : draw_text height
  #     text  : draw_text
  #    align  : draw_text align
  #    font   : font size
  #    color  : text color
  #--------------------------------------------------------------------------  
  def draw_format_text(x, y, width, height, text = "", align = 0, font = Font.default_size, color = normal_color, enabled = true)
      old_font_color = self.contents.font.color
      old_font_size = self.contents.font.size
      self.contents.font.color = color
      self.contents.font.color.alpha = enabled ? 255 : 128
      self.contents.font.size = font
      self.contents.draw_text(x, y, width, height, text, align)
      self.contents.font.color = old_font_color
      self.contents.font.size = old_font_size
    end
  #--------------------------------------------------------------------------
  # * Draw Icon Format Text
  #     x     : draw spot x-coordinate
  #     y     : draw spot y-coordinate
  #    width  : draw_text width
  #    height : draw_text height
  #     text  : draw_text
  #     icon  : draw_icon
  #    align  : draw_text align
  #    font   : font size
  #    color  : text color
  #--------------------------------------------------------------------------    
  def draw_icon_format_text(x, y, width, height, text = "", icon = nil, outlined = false, align = 0, font = Font.default_size, color = normal_color, enabled = true)
    off_set = 0
    if icon != nil
      draw_icon(icon, x, y) if outlined == false
      draw_outlined_icon(icon, x, y, 32, 32, 4) if outlined
      off_set = 32
    end    
    draw_format_text(x + off_set, y, width, height, text, align, font, color, enabled)
  end
  
  #--------------------------------------------------------------------------
  # * Draw Filled Rect
  #     x     : draw spot x-coordinate
  #     y     : draw spot y-coordinate
  #    width  : Rect width
  #    height : Rect height
  #    color  : Rect Color
  #-------------------------------------------------------------------------- 
  def draw_filled_rect(x, y, width, height, color = system_color, enabled = true)
    outline = Rect.new(0, 0, width, height)
    outline_sprite = Bitmap.new(outline.width, outline.height)
    outline_sprite.fill_rect(outline, color)
    self.contents.blt(x, y, outline_sprite, outline, enabled ? 255 : 128)
  end
  #--------------------------------------------------------------------------
  # * Draw Border Rect
  #     x     : draw spot x-coordinate
  #     y     : draw spot y-coordinate
  #    width  : Rect width
  #    height : Rect height
  #    border : Border Size
  #    color  : Rect Color
  #--------------------------------------------------------------------------  
  def draw_border_rect(x, y, width, height, border = 4, color = system_color, enabled = true)
    outline = Rect.new(0, 0, width, height)
    sub_rect = Rect.new((border / 2), (border / 2), (outline.width - border), (outline.height - border))
    outline_sprite = Bitmap.new(outline.width, outline.height)
    outline_sprite.fill_rect(outline, color)
    outline_sprite.clear_rect(sub_rect)
    self.contents.blt(x, y, outline_sprite, outline, enabled ? 255 : 128)
  end
    
  #--------------------------------------------------------------------------
  # * Draw Outlined Icon 
  #     x     : draw spot x-coordinate
  #     y     : draw spot y-coordinate
  #    width  : Rect width
  #    height : Rect height
  #    border : Border Size
  #    color  : Rect Color
  # icon_index: Icon Index
  #--------------------------------------------------------------------------  
  def draw_outlined_icon(icon_index, x, y, width, height, border = 4, color = system_color, enabled = true)
    draw_border_rect(x, y, width, height, border, color, enabled)
    draw_icon(icon_index, x + ((width - 24) / 2), y + ((height - 24) / 2), enabled = true)
  end
  
  #--------------------------------------------------------------------------
  # * Draw Outlined Item 
  #     x     : draw spot x-coordinate
  #     y     : draw spot y-coordinate
  #    width  : Rect width
  #    height : Rect height
  #    border : Border Size
  #    color  : Rect Color
  #    item   : Item
  #--------------------------------------------------------------------------  
  def draw_outlined_item(item, x, y, width, height, border = 4, color = system_color, enabled = true)
    draw_outlined_icon(item.icon_index, x, y, width, height, border, color, enabled = true)
  end
  
  #--------------------------------------------------------------------------
  # * Draw Outlined Actor 
  #     x     : draw spot x-coordinate
  #     y     : draw spot y-coordinate
  #    width  : Rect width
  #    height : Rect height
  #    border : Border Size
  #    color  : Rect Color
  #    actor  : actor
  #--------------------------------------------------------------------------  
  def draw_outlined_actor(actor, x, y, width, height, border = 4, color = system_color, enabled = true)
    draw_border_rect(x, y, width, height, border, color)
    draw_actor_sprite(actor, x + ((width - 32) / 2), y + ((height - 32) / 2), enabled = true)
  end
  
  def draw_extended_actor_parameter(actor, x, y, type)
    case type
    when 0
      icon = 1
    when 1
      icon = 53
    when 2
      icon = 20 
    when 3
      icon = 137
    end
    draw_icon(icon, x, y)
    draw_actor_parameter(actor, x + 32, y, type)
  end
  
  def draw_all_actor_parameters(actor, x, y)
    for i in 0..3
    draw_extended_actor_parameter(actor, x, y, i)
    y += 32
    end
  end
  
  def draw_jagged_rect(x, y, width, height, spac = 2, border = 4, color = system_color, color2 = normal_color, enabled = true)
     draw_border_rect(x, y, width, height, border, color, enabled = true)
     draw_dotted_rect(x, y, width, height, spac, border, color2, enabled = true)
     border_halv = border / 2
     bitmap_bite_out(x + border_halv, y + border_halv, width - border, height - border)
  end
  
  def draw_dotted_rect(x, y, width, height, spac = 2, border = 4, color = normal_color, enabled = true)
     color.alpha = enabled ? 255 : 128
     border_hal = border / 2
     restx = x - border_hal
     resty = y - border_hal
     spotx = restx
     spoty = resty
     coun = 0
     max_spotter = (width / spac) + border_hal
     aew = width + border_hal
     aeh = height + border_hal
     ae = aew * aeh
     for i in 0..ae
     self.contents.set_pixel(spotx, spoty, color)
     spotx += spac
     coun += 1
     if coun == max_spotter
       coun = 0
       spotx = restx
       spoty += spac
     end
     break if spoty > height
   end   
 end
 
   def bitmap_bite_out(x, y, width, height) 
     bite = Rect.new(x, y, width, height)
     self.contents.clear_rect(bite)
   end
   
   def draw_command_icon_box(x, y, commandi, icon_index, boxsize = 32, color = system_color, enabled = true)
     draw_border_rect(x, y, boxsize, boxsize, 4, color)
     draw_icon(icon_index, x + ((boxsize - 24) / 2), y + ((boxsize - 24) / 3), enabled)
     draw_format_text(x, y + (boxsize - 24), boxsize, 16, commandi, 1, 16)
   end
   
   def draw_text_fraction_style(x, y, text1, text2, enabled = true)
      old_font_color = self.contents.font.color
      old_font_size = self.contents.font.size
      self.contents.font.color = normal_color
      self.contents.font.color.alpha = enabled ? 255 : 128
      self.contents.font.size = 14
      self.contents.draw_text(x - 8, y - 4, 32, WLH, text1)
      self.contents.font.size = 18
      self.contents.draw_text(x + 8, y + 4, 32, WLH, "/")
      self.contents.font.size = 14
      self.contents.draw_text(x + 16, y + 8, 32, WLH, text2)
      self.contents.font.color = old_font_color
      self.contents.font.size = old_font_size
    end
    
   def draw_grad_bar(x, y, width, height, value, max, color1 = normal_color, color2 = system_color)
     draw_border_rect(x, y, width, height, 4, Color.new(20, 20, 20))
     barwidth = (width - 4)* value / max
     self.contents.gradation_rect(x + 2, y + 2, barwidth, height - 4, color1, color2)
     if barwidth > 16
     rect = Rect.new(0, 0, barwidth - 4, height / 1.5)
     bitm = Bitmap.new(rect.width, rect.height)
     bitm.gradient_fill_rect(rect, Color.new(255, 255, 255, 128), Color.new(255, 255, 255, 10), true)
     bitm.blur
     self.contents.blt(x + 4, y + 2, bitm, bitm.rect)
     end
   end
   
   def draw_unusable_pic(x, y, enabled = true)
     picz = Cache.system("UNUSABLE")
     self.contents.blt(x, y, picz, picz.rect, enabled ? 255 : 128)
   end
   
   def draw_short_hp(actor, x, y, width = 64)
     draw_actor_hp_gauge(actor, x, y, width)
     self.contents.draw_text(x, y, width, WLH, Vocab.hp, 0)
     self.contents.draw_text(x, y, width, WLH, actor.hp, 2)
   end
   
   def draw_short_mp(actor, x, y, width = 64)
     draw_actor_mp_gauge(actor, x, y, width)
     self.contents.draw_text(x, y, width, WLH, Vocab.mp, 0)
     self.contents.draw_text(x, y, width, WLH, actor.mp, 2)
   end
   
   def draw_fill_border_rect(x, y, width, height, border = 4, color1 = system_color, color2 = normal_color, enabled = true)
     border_halv = border / 2
     draw_border_rect(x, y, width, height, border, color2, enabled)
     draw_filled_rect(x + border_halv, y + border_halv, width - border, height - border, color1, enabled)
   end
   
   #range = [max_rg, field, line_to_target?, exclude_caster?, min_rg]
   def draw_rect_range(x, y, range, size = 8, spacing = 2, color1 = Color.new(200, 200, 200), color2 = Color.new(50, 50, 50), color3 = system_color, color4 = crisis_color)
     draw_rect_range_sys(x, y, range, size, spacing, color1, color2)
     if range[4] > 0
       minrg = range
       minrg[0] = range[4] 
       draw_rect_range_sys(x, y, minrg, size, spacing, color3, color4)
      end
    end
    
    def draw_rect_range_field(x, y, range, size = 8, spacing = 2, color1 = Color.new(200, 200, 200), color2 = Color.new(50, 50, 50))
      frange = range
      frange[0] = range[1]
      frange[3] = false
      draw_rect_range_sys(x, y, frange, size, spacing, color1, color2)
    end
    
   def draw_rect_range_sys(x, y, range, size = 8, spacing = 2, color1 = Color.new(200, 200, 200), color2 = Color.new(50, 50, 50))
     range_diag = range[0] / 2
     tcolor = color1
     bcolor = color2
     tilesize = size
     border = size / 4
     draw_border_rect(x, y, tilesize, tilesize, 2, Color.new(20, 20, 20))
     for i in 1..range[0]
      draw_fill_border_rect(x + ((tilesize + spacing)* i), y, tilesize, tilesize, border, tcolor, bcolor)
      draw_fill_border_rect(x - ((tilesize + spacing)* i), y, tilesize, tilesize, border, tcolor, bcolor)
      draw_fill_border_rect(x, y + ((tilesize + spacing)* i), tilesize, tilesize, border, tcolor, bcolor)
      draw_fill_border_rect(x, y - ((tilesize + spacing)* i), tilesize, tilesize, border, tcolor, bcolor)
    end
    if range[2] == false
    for iy in 1..range[0]
     for ix in 1..range[0]
       riy = iy % ix 
       rix = ix % iy 
       draw_fill_border_rect(x + ((tilesize + spacing)* rix), y + ((tilesize + spacing)* riy), tilesize, tilesize, border, tcolor, bcolor)
       draw_fill_border_rect(x - ((tilesize + spacing)* rix), y - ((tilesize + spacing)* riy), tilesize, tilesize, border, tcolor, bcolor)
       draw_fill_border_rect(x - ((tilesize + spacing)* rix), y + ((tilesize + spacing)* riy), tilesize, tilesize, border, tcolor, bcolor)
       draw_fill_border_rect(x + ((tilesize + spacing)* rix), y - ((tilesize + spacing)* riy), tilesize, tilesize, border, tcolor, bcolor)
     end
   end
   
    for di in 1..range_diag
      draw_fill_border_rect(x + ((tilesize + spacing)* di), y + ((tilesize + spacing)* di), tilesize, tilesize, border, tcolor, bcolor)
      draw_fill_border_rect(x - ((tilesize + spacing)* di), y - ((tilesize + spacing)* di), tilesize, tilesize, border, tcolor, bcolor)
      draw_fill_border_rect(x - ((tilesize + spacing)* di), y + ((tilesize + spacing)* di), tilesize, tilesize, border, tcolor, bcolor)
      draw_fill_border_rect(x + ((tilesize + spacing)* di), y - ((tilesize + spacing)* di), tilesize, tilesize, border, tcolor, bcolor)
    end
  end
  
  if range[3]
    bitmap_bite_out(x, y, tilesize, tilesize)
  else
    draw_fill_border_rect(x, y, tilesize, tilesize, border, Color.new(20, 20, 20), Color.new(50, 50, 50))
  end
  end
   
end
