# ~ICY_Random_BattleBGM
#------------------------------------------------------------------------------#
#ICY Random Battle BGM
#Created by : IceDragon
#Script-Status : Addon
#Date Created : 4/19/2010
#Date Modified: 7/01/2010
#------------------------------------------------------------------------------#
#This script allows the application of random BGMs
#They can be placed into groups, by numbers.
#If you want to play a set, change the variable to the corresponding set
#And start the battle....

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
#-#-#-#-#START CUSTOMIZATION#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
module ICY
  module Rand_BatBGM
    #This is the BGM list
    #[BGM Name, Volume, Pitch]
    BGMs = {
    #NormalSet
    0 =>[
    ["001-Battle01", 100, 90],
    ["002-Battle02", 100, 100],
    ["003-Battle03", 100, 120],
    ["004-Battle04", 100, 50]
      ],
    #Boss Set
    1 =>[
    ["005-Boss01.mid", 100, 100],
    ["006-Boss02.mid", 100, 100],
    ["007-Boss03.mid", 100, 100],
    ["008-Boss04.mid", 100, 100]     
      ],
    }
    #This is the switch to turn off the random BGM
    Switch = 1
    #This is the variable which controls the BGM sets..
    Variable = 1
  end
end
################################################################################
#------------------------------------------------------------------------------#
#END\\\END\\\END\\\END\\\END\\\END\\\END///END///END///END///END///END///END///#
#------------------------------------------------------------------------------#
################################################################################

#-----------------------------------------------------------------------------
# **Class Scene_Map
#-----------------------------------------------------------------------------
class Scene_Map < Scene_Base
  
  #--------------------------------------------------------------------------
  # alias call battle
  #--------------------------------------------------------------------------
 alias icy_call_battle_old call_battle unless $@
  def call_battle
    stored_battle_bgm = $game_system.battle_bgm
    unless $game_switches[ICY::Rand_BatBGM::Switch]
      bgms = ICY::Rand_BatBGM::BGMs[$game_variables[ICY::Rand_BatBGM::Variable]]
      selection = rand(bgms.size)
      tracki = bgms.at(selection)
      batbgm = RPG::BGM.new(tracki[0], tracki[1], tracki[2])
      $game_system.battle_bgm = batbgm
    end
    icy_call_battle_old
    $game_system.battle_bgm = stored_battle_bgm
  end
  
end # Scene Map
################################################################################
#------------------------------------------------------------------------------#
#END\\\END\\\END\\\END\\\END\\\END\\\END///END///END///END///END///END///END///#
#------------------------------------------------------------------------------#
################################################################################
