# ~ICY_Scene_Title
#------------------------------------------------------------------------------#
#ICY Title Screen
#Created by : IceDragon
#Script-Status : Addon
#Date Created : 3/16/2010
#------------------------------------------------------------------------------#
module ICY
  module TScreen
    #Icon used for new game
    NEW      = 253
    #Icon used for continue
    CON      = 209
    #Icon used for shut down
    SHTDWN   = 136
    #Icon used for YE Icon View
    ICONVIEW = 2
    #Font size
    FONTSIZE = 16
    
    #Command Window Position
    WIN_X = (544 - 172) / 2 
    WIN_Y = 262
  end
end
$imported = {} if $imported == nil
$imported["ICY_TitleScreen"] = true
#ICY_W1.2
#==============================================================================
# ** ICY_Window_Command
#------------------------------------------------------------------------------
#  This window deals with general command choices.
#==============================================================================

class ICY_Window_Command < Window_Selectable
  #--------------------------------------------------------------------------
  # * Public Instance Variables
  #--------------------------------------------------------------------------
  attr_reader   :commands                 # command
  #--------------------------------------------------------------------------
  # * Object Initialization
  #     width      : window width
  #     commands   : command string array
  #     commandcons: command icon array
  #     column_max : digit count (if 2 or more, horizontal selection)
  #     row_max    : row count (0: match command count)
  #     spacing    : blank space when items are arrange horizontally
  #--------------------------------------------------------------------------
  def initialize(width, commands, commandcons, fontsize = 18, column_max = 1, row_max = 0, spacing = 32)
    if row_max == 0
      row_max = (commands.size + column_max - 1) / column_max
    end
    super(0, 0, width, row_max * WLH + 32, spacing)
    @commands = commands
    @item_max = commands.size
    @column_max = column_max
    @commandcons = commandcons
    @fontsize = fontsize
    refresh
    self.index = 0
  end
  #--------------------------------------------------------------------------
  # * Refresh
  #--------------------------------------------------------------------------
  def refresh
   self.contents.clear
    for i in 0...@item_max
      command = @commands[i]
      icon = @commandcons[command]        
       icy_recdraw_item(i, icon)
    end
  end
  #--------------------------------------------------------------------------
  # * Draw Item
  #     index   : item number
  #     enabled : enabled flag. When false, draw semi-transparently.
  #--------------------------------------------------------------------------
  def icy_recdraw_item(index, icon_index, enabled = true)
    rect = item_rect(index)
    rect.x += 24
    rect.width -= 8
    
    if @column_max == 1
    draw_icon(icon_index, x, rect.y, enabled)
  else
    draw_icon(icon_index, (rect.x - 24), y, enabled)
  end
  
    self.contents.clear_rect(rect)
    self.contents.font.size = @fontsize
    self.contents.font.color = normal_color
    self.contents.font.color.alpha = enabled ? 255 : 128
    self.contents.draw_text(rect, @commands[index])
  end
end

#==============================================================================
# ** Scene_Title
#------------------------------------------------------------------------------
#  This class performs the title screen processing.
#==============================================================================

class Scene_Title < Scene_Base
 
  #--------------------------------------------------------------------------
  # * Create Command Window
  #--------------------------------------------------------------------------
  def create_command_window
    i1 = ICY::TScreen::NEW
    i2 = ICY::TScreen::CON
    i3 = ICY::TScreen::SHTDWN
    
    s1 = Vocab::new_game
    s2 = Vocab::continue
    s3 = Vocab::shutdown
   
    iconsoi = {s1 => i1, s2 => i2, s3 => i3}
    sets = [s1, s2, s3]
    
   if $imported["IconView"] == true
    i4 = ICY::TScreen::ICONVIEW
    s4 = YE::TOOLS::ICONVIEW::IV_NAME
    iconsoi[s4] = i4
    sets.push(s4)
    end  
  
    @command_window = ICY_Window_Command.new(172, sets, iconsoi, ICY::TScreen::FONTSIZE)
    @command_window.x = ICY::TScreen::WIN_X
    @command_window.y = ICY::TScreen::WIN_Y
    
    if @continue_enabled                    # If continue is enabled
      @command_window.index = 1             # Move cursor over command
    else                                    # If disabled
      @command_window.icy_recdraw_item(1, i2, false)   # Make command semi-transparent
    end
    @command_window.openness = 0
    @command_window.open
  end
  
   def update
    super
    @command_window.update
    if Input.trigger?(Input::C)
      case @command_window.index
      when 0
        command_new_game
      when 1
        command_continue
      when 2
        command_shutdown
      when 3
        command_iconview
      end
    end
  end
  
  #--------------------------------------------------------------------------
  # * Open Command Window
  #--------------------------------------------------------------------------
  def open_command_window
    @command_window.open
    begin
      @command_window.update
      Graphics.update
    end until @command_window.openness == 255
  end
  #--------------------------------------------------------------------------
  # * Close Command Window
  #--------------------------------------------------------------------------
  def close_command_window
    @command_window.close
    begin
      @command_window.update
      Graphics.update
    end until @command_window.openness == 0
  end
  
end
