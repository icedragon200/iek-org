# ~ICY_Scene_Item_BattleFix
module ICY
  module Battle_HM_Style_Items
    WINDOW_SIZES = {
    "Help" => [0, 164, 544, 128, 255], # This is the window which shows the description
    "Item" => [0, 0, 544, 164, 255], # This is the window which shows the items
    }
    COLUMN_MAX = 10
  end
end

class Scene_Battle < Scene_Base
  if $imported["ICY_HM_Style_Inventory"] 
  #--------------------------------------------------------------------------
  # * Start Item Selection
  #--------------------------------------------------------------------------
  def start_item_selection
    window_sizes = ICY::Battle_HM_Style_Items::WINDOW_SIZES
    help_size = window_sizes["Help"]
    @help_window = ICY_Item_Window_Help.new(help_size[0], help_size[1], help_size[2], help_size[3])
    @help_window.opacity = help_size[4]
    item_win_size = window_sizes["Item"]
    @item_window = ICY_SC_Window_Item.new(item_win_size[0], item_win_size[1],
    item_win_size[2], item_win_size[3])
    @item_window.opacity = item_win_size[4]
    @item_window.column_max = ICY::Battle_HM_Style_Items::COLUMN_MAX
    @item_window.refresh
    @item_window.help_window = @help_window
    @actor_command_window.active = false
  end
  end
end
