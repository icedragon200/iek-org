=begin
  
  ♥ IEI - Exectine
  by IceDragon
  dc 04/28/2012
  dm 05/26/2012
  vr 1.0
  
 ────────────────────────────────────────────────────────────────────────────── 
=end
($imported||={})['IEI::Exectine'] = 0x01000
# ╒╕ ■                                                        IEI::Exectine ╒╕
# └┴────────────────────────────────────────────────────────────────────────┴┘
module IEI
  module Exectine

    module Constants
      EXC_NULL     = 0 # // No Skill Execution
      EXC_NORMAL   = 1 # // Normal MP/HP/TP
      EXC_ALCHEMY  = 2 # // Equivalent Exchange
      EXC_CHARGE   = 3 # // Charge
      EXC_SEQUENCE = 4 # // Sequenced
    end

    module Include

      include Constants

      def pre_init_iei
        super
        @exc_types = Hash.new
        @exc_types.default = EXC_NORMAL
      end

      def skill_exc(skill_id)
        @exc_types[skill_id]
      end

      def skill_cost_payable?(skill)
        case skill_exc skill.id
        when EXC_NULL     ; true
        when EXC_NORMAL   ; super skill
        when EXC_ALCHEMY  ;
        when EXC_CHARGE   ; skill_charged? skill
        when EXC_SEQUENCE ; true
        end
      end

      def skill_charged?(skill)
        @skl_charges[skill.id] >= 100
      end

      def set_skill_charge(id, n)
        @skl_charges[id] = (@skl_charges[id] + n).clamp(0,100)
      end

    end

  end

end