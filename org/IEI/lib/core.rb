=begin
  
  ♥ IEI - Core
  by IceDragon
  dc 02/22/2012
  dm 05/25/2012
  vr 1.0
  
 ────────────────────────────────────────────────────────────────────────────── 
=end
($imported||={})["IEI::Core"] = 0x10001
# ╒╕ ■                                                            IEI::Core ╒╕
# └┴────────────────────────────────────────────────────────────────────────┴┘
module IEI
  module Core

    @data_load_stack = []

    def self.on_data_load &block
      @data_load_stack << block
    end

    def self.exc_load_stack
      @data_load_stack.each &:call
    end

    def self.do_obj_cache obj
      obj.note_eval if Devi.enabled? "NoteEval"
    end

    NoteFolder = Struct.new :header,:body

    def self.mk_notefolder_tags str
      return [/<#{str}>/i,/<\/#{str}>/i]
    end

    def self.get_note_folders((open_tag,close_tag),note)
      lines  = note.split(/[\r\n]+/i)
      i,line,result,arra = 0, nil,nil,[]
      while i < lines.size
        line = lines[i]
        if n = line.match(open_tag)
          result = NoteFolder.new n,[]
          until line =~ close_tag
            i += 1
            line = lines[i]
            result.body << line
            raise "End of note reached!" if(i > lines.size)
          end
          arra << notef; result = nil
        end
        i += 1
      end
      arra
    end

  end

  module Sprite
  end
  module Window
  end
  module Scene
  end

  class Tileset

    def initialize columns,rows,cell_width,cell_height
      @grid = IEI::Grid.new columns,rows,cell_width,cell_height
      @bitmap = Bitmap.new columns*cell_width,rows*cell_height
    end

    attr_reader :bitmap

    def cell_r *args,&block
      @grid.cell_r *args,&block
    end

    def columns
      @grid.columns
    end

    def rows
      @grid.rows
    end

    def width
      @bitmap.width
    end

    def height
      @bitmap.height
    end

    def cell_width
      @grid.cell_width
    end

    def cell_height
      @grid.cell_height
    end

    def disposed?
      @bitmap.nil? || @bitmap.disposed?
    end

  end
end

# ╒╕ ♥                                                        RPG::BaseItem ╒╕
# └┴────────────────────────────────────────────────────────────────────────┴┘
class RPG::BaseItem

  # // 04/28/2012
  def get_note_folders(tags)
    IEI::Core.get_note_folders(tags,@note)
  end

end

# ╒╕ ■                                                          DataManager ╒╕
# └┴────────────────────────────────────────────────────────────────────────┴┘
class << DataManager

  # // Because Element makes calls to the Vocab module
  # // .x. The main database has to be loaded BEFORE the elements are created
  alias :core_post_db_load :post_db_load
  def post_db_load
    core_post_db_load
    IEI::Core.exc_load_stack
  end

end

IEI::Core.on_data_load do
  [$data_actors,$data_items,$data_skills,$data_weapons,$data_armors,
   $data_states,$data_enemies,$data_classes].each do |dat|
    dat.compact.each { |o| IEI::Core.do_obj_cache o  }
  end
end

# ┌┬────────────────────────────────────────────────────────────────────────┬┐
# ╘╛ ● End of File ●                                                        ╘╛