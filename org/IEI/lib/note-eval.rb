=begin
  
  ♥ IEI - NoteEval
  by IceDragon
  dc 04/28/2012
  dm 05/26/2012
  vr 0x01000
  
 ────────────────────────────────────────────────────────────────────────────── 
=end
($imported||={})['IEI::NoteEval'] = 0x01000
# ╒╕ ■                                                        IEI::NoteEval ╒╕
# └┴────────────────────────────────────────────────────────────────────────┴┘
# ╒╕ ♥                                                        RPG::BaseItem ╒╕
# └┴────────────────────────────────────────────────────────────────────────┴┘
class RPG::BaseItem
  def note_eval
    get_note_folders(IEI::Core.mk_notefolder_tags("note[_ ]?eval")).each{|a|eval(a.join("\n"))}
  end
end
# ┌┬────────────────────────────────────────────────────────────────────────┬┐
# ╘╛ ● End of File ●                                                        ╘╛