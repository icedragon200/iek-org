=begin
  
  ♥ IEI - Game_Strings
  by IceDragon
  dc 23/06/2012
  dm 23/06/2012
  vr 1.0
  
 ─┐ ● Introduction ┌───────────────────────────────────────────────────────────
  └────────────────┘ 
  Nothing big, just a class similar to Game_Variables,
  only difference it holds strings

 ─┐ ● How To Use ┌─────────────────────────────────────────────────────────────
  └──────────────┘ 
  $game_strings[id] = "string"

 ────────────────────────────────────────────────────────────────────────────── 
=end
($imported||={})['IEI::Game_Strings'] = 0x10000
# ╒╕ ■                                                          DataManager ╒╕
# └┴────────────────────────────────────────────────────────────────────────┴┘
class << DataManager
  # // Create the Game_Strings object on new games
  alias :gms_crt_gm_objs :create_game_objects
  def create_game_objects
    gms_crt_gm_objs
    $game_strings = Game_Strings.new
  end
  # // Save the Game_Strings, or create a new one if it doesn't exist
  alias :gms_mk_sv_cont :make_save_contents
  def make_save_contents
    contents = gms_mk_sv_cont
    contents[:strings] = $game_strings||Game_Strings.new
    contents
  end
  # // Pull the Game_Strings or create a new one if it doesn't exist
  alias :gms_ex_sv_cont :make_save_contents
  def extract_save_contents contents
    gms_ex_sv_cont contents
    $game_strings = contents[:strings]||Game_Strings.new
  end
end
# ╒╕ ♥                                                         Game_Strings ╒╕
# └┴────────────────────────────────────────────────────────────────────────┴┘
class Game_Strings
  def initialize
    @data = Array.new
  end
  def [] id
    @data[id] || ""
  end
  def []= id,v
    @data[id] = v
  end
  include IEI::Tracker if $imported['IEI::Tracker']
end
# ┌┬────────────────────────────────────────────────────────────────────────┬┐
# ╘╛ ● End of File ●                                                        ╘╛