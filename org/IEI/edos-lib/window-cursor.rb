=begin
  
  ♥ IEI - Window Cursor
  by IceDragon
  dc 28/07/2012
  dm 22/10/2012
  vr 0x10001
  
 ────────────────────────────────────────────────────────────────────────────── 
=end
($imported||={})['IEI::WindowCursor']=0x10001
# ╒╕ ■                                                                  IEI ╒╕
# └┴────────────────────────────────────────────────────────────────────────┴┘
module IEI
# ╒╕ ■                                                               Config ╒╕
# └┴────────────────────────────────────────────────────────────────────────┴┘
  module Config
    WC_CURSOR_NAME = "win_cursor"
    WC_CELL_COUNT  = 8
    WC_FRAME_RATE  = 3
    WC_MOVE_FRAME_RATE = 6
  end  

# ╒╕ ■                                                         WindowCursor ╒╕
# └┴────────────────────────────────────────────────────────────────────────┴┘
  module WindowCursor  

    NULL_VIEWPORT = Struct.new(:x, :y, :z, :ox, :oy).new(0, 0, 0, 0, 0)
    @@wcursor_spr = nil
    @@wcwindows = []   

    def self.wcursor_spr
      if !@@wcursor_spr or @@wcursor_spr.disposed?
        @@wcursor_spr = Sprite::WindowCursor.new
      end
      @@wcursor_spr
    end

    def self.wcwindows
      @@wcwindows
    end    

    # // Include
    def initialize *args, &block
      super *args, &block
      @@wcwindows << self
    end

    def dispose *args, &block
      @@wcwindows.delete self   
      super *args, &block
    end

    def _wcursor_active?
      (@@wcwindows.delete self ;return false) if self.disposed?
      self.active and self.open? and self.visible and self.index > -1
    end

    def cursor_rect_abs
      cursor_rect
    end

    def _wcursor_calc_a 
      r = cursor_rect_abs.clone
      if v = self.viewport
        vr = v.rect
      else
        vr = v = NULL_VIEWPORT
      end
      off = standard_padding
      return [
        r.x + self.x - v.ox + vr.x + off,# + (r.width - crect.width) / 2,
        r.y + self.y - v.oy + vr.y + off + r.height / 2, 
        self.z + v.z + 9999
      ]
    end

  end
end 

# ╒╕ ■                                                             Graphics ╒╕
# └┴────────────────────────────────────────────────────────────────────────┴┘
module Graphics

  class << self

    alias wc_update update
    def update
      wc_update
      curs = IEI::WindowCursor.wcursor_spr
      curs.window = IEI::WindowCursor.wcwindows.find &:_wcursor_active?
      curs.update
    end

  end

end
# ╒╕ ♥                                                                 Rect ╒╕
# └┴────────────────────────────────────────────────────────────────────────┴┘
class Rect

  def empty?
    self.width == 0 or self.height == 0
  end

end

# ╒╕ ♥                                                 Sprite::WindowCursor ╒╕
# └┴────────────────────────────────────────────────────────────────────────┴┘
class Sprite::WindowCursor < Sprite

  attr_accessor :window  

  def initialize viewport=nil
    @last_target = [-1, -1, -1]
    @target      = [-1, -1, -1]
    @lerps       = [ 0,  0,  0]
    @time        = 0
    @index       = 0
    @timemax     = IEI::Config::WC_MOVE_FRAME_RATE.to_f
    @frame_rate  = IEI::Config::WC_FRAME_RATE
    @cell_count  = IEI::Config::WC_CELL_COUNT
    super viewport
    self.bitmap  = Cache.system(IEI::Config::WC_CURSOR_NAME)
    @cell_width  = self.bitmap.width / @cell_count
    @cell_height = self.bitmap.height
    refresh
  end

  def dispose
    self.bitmap.dispose unless self.bitmap or !self.bitmap.disposed?
    super
  end

  def refresh
    self.ox = @cell_width * 0.75
    self.oy = @cell_height / 2
    self.src_rect.set(@cell_width * @index, 0, @cell_width, @cell_height)
    self
  end

  def update
    super
    self.visible = !!@window
    return unless visible
    a = @window._wcursor_calc_a 
    @time = @time - 1 if @time > 0
    if @target != a
      @last_target = @target
      @target = a
      3.times do |i| @lerps[i] = @target[i] - @last_target[i] end
      @time = @timemax
    end  
    if @time > 0
      r = [0, 0, 0]
      3.times do |i| 
        r[i] = @last_target[i] + @lerps[i] * (@timemax - @time.to_f) / @timemax 
      end
      self.x, self.y, self.z = r
    else
      self.x, self.y, self.z = @target
    end
    if Graphics.frame_count % @frame_rate == 0
      @index = (@index + 1) % @cell_count
      self.src_rect.set(@cell_width * @index, 0, @cell_width, @cell_height)
    end  
  end

end

# ╒╕ ♥                                                   Window::Selectable ╒╕
# └┴────────────────────────────────────────────────────────────────────────┴┘
class Window::Selectable
  include IEI::WindowCursor
end

# ╒╕ ♥                                                     Window::SaveFile ╒╕
# └┴────────────────────────────────────────────────────────────────────────┴┘
class Window::SaveFile
  include IEI::WindowCursor
  def _wcursor_active?
    self.active and self.open? and self.visible and !self.cursor_rect.empty?
  end  
end

# ┌┬────────────────────────────────────────────────────────────────────────┬┐
# ╘╛ ● End of File ●                                                        ╘╛