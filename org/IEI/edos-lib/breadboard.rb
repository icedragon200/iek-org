=begin
  
  ♥ IEI - Breadboard
  by IceDragon
  dc 05/07/2012
  dm 05/07/2012
  vr 0x10000
  
 ────────────────────────────────────────────────────────────────────────────── 
 ─┐ ● Requirements ┌───────────────────────────────────────────────────────────
  └────────────────┘ 

 ─┐ ● Introduction ┌───────────────────────────────────────────────────────────
  └────────────────┘ 

 ─┐ ● Instruction Manual ┌─────────────────────────────────────────────────────
  └──────────────────────┘ 

 ─┐ ● Reference Manual ┌───────────────────────────────────────────────────────
  └────────────────────┘ 

 ────────────────────────────────────────────────────────────────────────────── 
=end
module IEI
  class Breadboard

  end
end

class IEI::Breadboard::Component

  def initialize(inputs, outputs)
    @io_interface = {
      in: Array.new()
      out: {

      }
    }
  end

  def connect()
  end

  # Input
  def retrieve
  end

  # Output
  def transmit
  end

end