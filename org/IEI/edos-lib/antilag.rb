=begin
  
  ♥ IEI - AntiLag
  by IceDragon
  dc 03/27/2012
  dm 05/26/2012
  vr 1.0
  
 ────────────────────────────────────────────────────────────────────────────── 
=end
($imported||={})['IEI::AntiLag'] = 0x01000

# ╒╕ ■                                                         IEI::AntiLag ╒╕
# └┴────────────────────────────────────────────────────────────────────────┴┘
module IEI
  module AntiLag

  end
end

# ╒╕ ♥                                                      Game::Character ╒╕
# └┴────────────────────────────────────────────────────────────────────────┴┘
class Game::Character

  def on_screen?
    self.screen_x.between?(-32, Graphics.width + 32) and
     self.screen_y.between?(-32, Graphics.height + 32)
  end

end

# ╒╕ ♥                                                    Sprite::Character ╒╕
# └┴────────────────────────────────────────────────────────────────────────┴┘
class Sprite::Character

  def update
    super
    if @character && @on_screen = @character.on_screen?
      update_bitmap
      update_src_rect
      update_position
      update_other
    elsif
      self.visible = false
    end
    update_balloon
    setup_new_effect
  end

end