# ╒╕ ♥                                                        Game::Picture ╒╕
# └┴────────────────────────────────────────────────────────────────────────┴┘
class Game::Picture

  attr_accessor :flw_char_id

  alias flw_update update
  def update
    flw_update
    update_follow if @flw_char_id
  end

  def update_follow
    chara = $game_map.interpreter.get_character(@flw_char_id)
    @x = chara.screen_x
    @y = chara.screen_y
  end

end

# ╒╕ ♥                                                    Game::Interpreter ╒╕
# └┴────────────────────────────────────────────────────────────────────────┴┘
class Game::Interpreter

  def picture_follow(picture_id, character_id)
    character_id = @event_id if character_id == 0
    screen.pictures[picture_id].flw_char_id = character_id
  end

end
# ┌┬────────────────────────────────────────────────────────────────────────┬┐
# ╘╛ ● End of File ●                                                        ╘╛