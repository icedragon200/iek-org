=begin
  
  ♥ IEI - Statrix
  by IceDragon
  dc 07/07/2012
  dm 07/07/2012
  vr 0x10000
  
 ────────────────────────────────────────────────────────────────────────────── 
 ────────────────────────────────────────────────────────────────────────────── 
=end
($imported||={})['IEI::Statrix'] = 0x10000
# ╒╕ ♥                                                         IEI::Statrix ╒╕
# └┴────────────────────────────────────────────────────────────────────────┴┘
module IEI
  module Statrix
# ─┤ ● Start Customization ├──────────────────────────────────────────────────
    RATE_ID = {
      'mhp' => 0,
      'mmp' => 1,
      'atk' => 2,
      'def' => 3,
      'mat' => 4,
      'mdf' => 5,
      'agi' => 6,
      'luk' => 7,
    }
# ─┤ ● End Customization ├────────────────────────────────────────────────────
    def self.objs_param_rate param_id,objs
      objs.inject(1.0) do |r,obj| r *= obj.param_rate[param_id] end
    end
  end
end
# ╒╕ ♥                                                       RPG::EquipItem ╒╕
# └┴────────────────────────────────────────────────────────────────────────┴┘
class RPG::EquipItem
  def param_rate
    unless @param_rate
      @param_rate = []
      IEI::RATE_ID.each_pair do |name,id|
        @param_rate[id] = (@note.match(/\<#{name}(?:[_ ]?rate)?:\s*(\d+)\%\>/i)||[0,100])[1].to_i/100.0
      end
    end
    @param_rate
  end
end
# ╒╕ ♥                                                        Game::Battler ╒╕
# └┴────────────────────────────────────────────────────────────────────────┴┘
class Game::Battler
  def equips
    []
  end unless method_defined? :equips
  alias :iei_statrix_param_rate :param_rate
  def param_rate param_id
    iei_statrix_param_rate(param_id) * IEI::Statrix.objs_param_rate(param_id,equips)
  end
end
# ┌┬────────────────────────────────────────────────────────────────────────┬┐
# ╘╛ ● End of File ●                                                        ╘╛