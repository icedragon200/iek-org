=begin
  
  ♥ IEI - KeyNValue
  by IceDragon
  dc 04/28/2012
  dm 05/26/2012
  vr 0x01000
  
 ────────────────────────────────────────────────────────────────────────────── 
=end
($imported||={})['IEI::KeyNValue'] = 0x01000
# ╒╕ ♥                                                        RPG::BaseItem ╒╕
# └┴────────────────────────────────────────────────────────────────────────┴┘
class RPG::BaseItem
  def knv
    unless @knv
      str = "note[_ ]?knv"
      @knv = get_note_folders(IEI::Core.mk_notefolder_tags(str)).inject({}) do |r,a|
        a.each{|s|mtch = s.match(/(.*)=(.*)/i);r[s[1].downcase]=s[2] if(mtch)};r
      end
    end
    @knv
  end
  attr_writer :knv
end
# ┌┬────────────────────────────────────────────────────────────────────────┬┐
# ╘╛ ● End of File ●                                                        ╘╛