#-inject gen_module_header "IEI::Core (Base)"
($imported||={})["IEI::Core"]||=0x01001
unless $imported["IEI::Core"] >= 0x10000
  module IEI
    module Core
    end
    module Sprite
    end
    module Window
    end
    module Scene
    end
  end
  unless $imported["EDOS::Data"]
    module Scene
      Base = Scene_Base
    end  
    class Sprite
      Base = Sprite_Base
    end
    class Window
      Base = Window_Base
      Selectable = Window_Selectable
      Command = Window_Command
    end
  end  
end  