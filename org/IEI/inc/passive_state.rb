# // passive_state
def active
  !!@active 
end unless method_defined? :active

def active= state
  @active = !!state
end unless method_defined? :active=

alias :active? :active

def activate
  self.active = true
  self
end

def deactivate
  self.active = false
  self
end

def toggle_active
  self.active = !self.active
  self
end
