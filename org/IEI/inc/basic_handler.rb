# // basic_handler
def handler
  @handler ||= {}
end

attr_writer :handler
private :handler, :handler=

def set_handler symbol,function
  handler[symbol] = function
  self
end

def rem_handler symbol
  handler.delete(symbol)
  self
end

def call_handler symbol
  handler[symbol].call if handler.has_key? symbol
end
