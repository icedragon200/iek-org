# // Visibility
def visible
  !!@visible
end unless method_defined? :visible

def visible= n
  @visible = !!n
end unless method_defined? :visible=

def toggle_visible
  self.visible = !self.visible
  self
end

def show
  self.visible = true
  self
end

def hide
  self.visible = false
  self
end
