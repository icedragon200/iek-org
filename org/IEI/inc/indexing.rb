# // indexing
def index
  @index ||= 0
end unless method_defined? :index

def index= n
  @index = n.to_i
end unless method_defined? :index=

def item_max
  0
end unless method_defined? :item_max

def wrap_index wrap=item_max
  self.index %= wrap > 0 ? wrap : 1
  self
end

def select n
  self.index = n
  self
end

def deselect
  self.index = -1
  self
end

def next
  self.index += 1
  self
end

def prev
  self.index -= 1
  self
end
