=begin

  IEI - BitmapEx
  @depreceated

=end
#-skip:
=begin
#-define HDR_TYP :type=>"class"
#-define HDR_GNM :name=>"IEI - Bitmap Ex"
#-define HDR_GDC :dc=>"04/15/2012"
#-define HDR_GDM :dm=>"05/26/2012"
#-define HDR_GAUT :author=>"IceDragon"
#-define HDR_VER :version=>"1.0"
#-inject gen_script_header HDR_TYP,HDR_GNM,HDR_GAUT,HDR_GDC,HDR_GDM,HDR_VER
($imported||={})['IEI::BitmapEx'] = 0x01000
#-inject gen_class_header 'Bitmap'
class Bitmap
  def rrecolor(f_color,t_color=nil)
    if(f_color.is_a?(Color) && t_color.is_a?(Color))
      hsh = { f_color => t_color }
    elsif(f_color.is_a?(Array) && t_color)
      arra = t_color.is_a?(Enumerable) ? t_color : [t_color]*f_color.size
      hsh = {};f_color.each_with_index{|c,i|hsh[c]=arra[i]}
    else
      hsh = f_color
    end
    x,y,color = nil,nil,nil
    iterate_do { |x,y,color| hsh[color]||color }
  end
  def palletize()
    pallete = Set.new();x,y,color = nil,nil,nil
    iterate_do(true) {|x,y,color|pallete<<color.to_a}
    pallete.to_a.sort.collect{|a|Color.new(*a)}
  end
  def iterate_do(return_only=false)
    x, y = nil, nil
    if(return_only)
      for y in 0...height
        for x in 0...width
          yield(x,y,get_pixel(x,y))
        end
      end
    else
      for y in 0...height
        for x in 0...width
          set_pixel(x,y,yield(x,y,get_pixel(x,y)))
        end
      end
    end
  end
end
#-inject gen_script_footer
=end
#-end:
