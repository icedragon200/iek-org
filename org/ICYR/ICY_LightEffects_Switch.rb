# ICY_LightEffects_Switch
#------------------------------------------------------------------------------#
# ** ICY Light Effects Attach
# ** Created by : IceDragon
# ** Script-Status : Addon
# ** Date Created : 8/23/2010
# ** Date Modified : 8/23/2010
# ** Version : 1.0
#------------------------------------------------------------------------------#
# This script requires THOMAS_EDISON_LIGHT_FX to work
# This script attaches an in-game switch to the $light_switch variable
# This is mainly for eventing purposes
#------------------------------------------------------------------------------#
class Spriteset_Map
  # In game switch used
  SWITCHAAZAZ = 513 #Some crap was added to avoid trouble
  
  alias isale_update_light_effects update_light_effects unless $@
    def update_light_effects(*args)
      isale_update_light_effects(*args)
      if $game_switches[SWITCHAAZAZ] != $light_switch 
        $game_switches[SWITCHAAZAZ] = $light_switch 
        $game_map.need_refresh = true
      end
    end
    
end
