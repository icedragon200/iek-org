# IEX - Spellbook System - Scene
#==============================================================================
# ** Scene_SpellBookEquip
#------------------------------------------------------------------------------
#==============================================================================
class Scene_SpellBookEquip < Scene_Base
  
  include IEX::SCENE_ACTIONS
  
  def initialize(actor, called = :map, return_index=0)
    super()
    @index_call = false
    @actor = nil
    @act_index = 0
    @return_index = return_index
    if actor.kind_of?(Game_Battler)
      @actor = actor
    elsif actor != nil  
      @actor = $game_party.members[actor]
      @act_index = actor
      @index_call = true
    end  
    @calledfrom = called
  end
  
  def start
    super
    create_menu_background
    @windows = {}
    @winpos = [0, Graphics.width, Graphics.width / 2, Graphics.height, Graphics.height / 2]
    @windows["Equip"]   = Window_SpellbookEquip.new(0, 0, winpos[2])
    @windows["Party"] = Window_PartySpellbook.new(0, 0)
    @windows["Party"].width = Graphics.width
    @windows["Party"].y = Graphics.height - @windows["Party"].height
    @windows["Party"].refresh
    @windows["Party"].active = false
    @windows["Shelf"] = Window_SpellShelf.new(0, 0, 
      winpos[2], winpos[3]- @windows["Party"].height)
    @windows["Book"]  = Window_SpellBook.new(nil, winpos[2], 0, 
      winpos[2], winpos[3]- @windows["Party"].height)
    @windows["Equip"].y = @windows["Party"].y #
    @windows["Equip"].visible = false
    @windows["Equip"].active  = false
  end
  
  def winpos ; return @winpos end
    
  def terminate
    super
    dispose_menu_background
    for win in @windows.values
      next if win == nil
      win.dispose
    end  
  end
  
  def return_scene 
    case @calledfrom
    when :map
      $scene = Scene_Map.new
    when :menu  
      $scene = Scene_Menu.new(@return_index)
    end  
    GC.start
  end
  
  def update
    super
    if Input.trigger?(Input::C)
      action_accept
    elsif Input.trigger?(Input::B)  
      action_cancel
    elsif Input.trigger?(Input::R)  
      if @index_call 
        Sound.play_cursor
        @act_index = (@act_index + 1) % $game_party.members.size
        $scene = Scene_SpellBookEquip.new(@act_index)
      else
        Sound.play_buzzer
      end  
    elsif Input.trigger?(Input::L)  
      if @index_call 
        Sound.play_cursor
        @act_index = (@act_index - 1) % $game_party.members.size
        $scene = Scene_SpellBookEquip.new(@act_index)
      else  
        Sound.play_buzzer
      end 
    end  
    
    @windows["Book"].change_book(@windows["Shelf"].current_book)
    @windows["Book"].change_actor(@windows["Party"].current_actor)
    @windows["Equip"].change_actor(@windows["Party"].current_actor)
    
    for win in @windows.values
      next if win == nil
      win.update if win.active
    end  
  end
  
  def action_accept
    if @windows["Shelf"].active
      Sound.play_decision
      @windows["Party"].active = true
      @windows["Shelf"].active = false
    elsif @windows["Party"].active
      Sound.play_decision
      @windows["Party"].active = false
      start_equip_book
    elsif @windows["Equip"].active
      equip_current_book
    end  
  end
  
  def action_cancel
    if @windows["Shelf"].active
      Sound.play_cancel
      return_scene
    elsif @windows["Party"].active
      Sound.play_cancel
      @windows["Party"].active = false
      @windows["Shelf"].active = true
    elsif @windows["Equip"].active
      @windows["Party"].active = true
      close_equip_book
    end
  end
  
  def start_equip_book
    @windows["Equip"].visible = true
    loop do
      Graphics.update
      @windows["Equip"].y -= @windows["Equip"].height / 10
      @windows["Shelf"].height -= @windows["Equip"].height / 10
      break if @windows["Equip"].y <= @windows["Party"].y - @windows["Equip"].height
    end 
    @windows["Equip"].y = @windows["Party"].y - @windows["Equip"].height
    @windows["Equip"].active  = true
    
    @windows["Shelf"].height = winpos[3]- @windows["Party"].height - @windows["Equip"].height
    @windows["Shelf"].refresh
  end
  
  def close_equip_book
    loop do
      Graphics.update
      @windows["Equip"].y += @windows["Equip"].height / 10
      @windows["Shelf"].height += @windows["Equip"].height / 10
      break if @windows["Equip"].y >= @windows["Party"].y
    end
    @windows["Equip"].y = @windows["Party"].y
    @windows["Equip"].visible = false
    @windows["Equip"].active  = false
    
    @windows["Shelf"].height = winpos[3]- @windows["Party"].height
    @windows["Shelf"].refresh
  end
  
  def equip_current_book
    actor = @windows["Party"].current_actor
    book  = @windows["Shelf"].current_book
    index = @windows["Equip"].index
    if actor.nil? 
      Sound.play_buzzer
    else
      oldbook = actor.get_spellbook_at(index)
      if book != nil 
        if $game_party.spellbook_count(book) <= 0
          Sound.play_buzzer
          return
        end  
      end  
      actor.equip_spellbook(index, book)
      $game_party.lose_spellbook(book, 1)
      $game_party.gain_spellbook(oldbook, 1)
      @windows["Party"].refresh
      @windows["Shelf"].refresh
      @windows["Equip"].refresh
      @windows["Book"].refresh
    end 
    close_equip_book
    @windows["Equip"].active = false
    @windows["Party"].active = false
    @windows["Shelf"].active = true
  end  
  
end
