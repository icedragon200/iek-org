# #~IEX - DnD Status
#~ #------------------------------------------------------------------------------#
#~ # ** IEX(Icy Engine Xelion) - DnD Status Window
#~ # ** Created by : IceDragon
#~ # ** Script-Status : ReWrite >> Window_Status
#~ # ** Date Created : 10/12/2010
#~ # ** Date Modified : 10/12/2010
#~ # ** Requested by : lvlercenary
#~ # ** Vesrion : 0.5a (You try lineing up text!)
#~ #------------------------------------------------------------------------------#
#~ module IEX
#~   module DND_STATUS
#~     BACKGROUND_IMAGE = "CampaignCharsheet" # Located in System Folder
#~     
#~     ACTOR_PERSONAL_DATA = {}
#~     
#~   # ACTOR_PERSONAL_DATA[actor_id] => {
#~     ACTOR_PERSONAL_DATA[1] = {
#~   # :somedata   => "some string",
#~     :race       => "Human",
#~     :alignment  => "Goodie",
#~     :deity      => "???",
#~     :size       => "Smarl",
#~     :age        => "19",
#~     :gender     => "Male",
#~     :height     => "154cm",
#~     :weight     => "???",
#~     :eyes       => "Gold",
#~     :hair       => "White",
#~     :skin       => "???",
#~     
#~     } # DO NOT REMOVE THIS
#~  
#~     FONT_COLOR = Color.new(0, 255, 255)#  Color.new(255, 255, 255) # White
#~   end
#~ end

#~ #==============================================================================
#~ # ** Window_Status
#~ #------------------------------------------------------------------------------
#~ #  This window displays full status specs on the status screen.
#~ #==============================================================================

#~ class Window_Status < Window_Base
#~   
#~   #--------------------------------------------------------------------------
#~   # * Object Initialization
#~   #     actor : actor
#~   #--------------------------------------------------------------------------
#~   def initialize(actor)
#~     super(0, 0, Graphics.width, Graphics.height)
#~     @actor = actor
#~     @back_sprite = Sprite.new
#~     @back_sprite.bitmap = Cache.system(IEX::DND_STATUS::BACKGROUND_IMAGE)
#~     @back_sprite.x = self.x
#~     @back_sprite.y = self.y
#~     self.opacity = 0
#~     refresh
#~   end
#~   
#~   def dispose
#~     if @back_sprite != nil
#~       @back_sprite.dispose
#~       @back_sprite = nil
#~     end
#~     super
#~   end
#~   
#~   def update
#~     super
#~     if @back_sprite != nil
#~       @back_sprite.visible = self.visible
#~       @back_sprite.x = self.x
#~       @back_sprite.y = self.y
#~     end  
#~   end
#~   
#~   def visible=(vis)
#~     @back_sprite.visible = vis
#~     super(vis)
#~   end
#~   
#~   def refresh
#~     create_contents
#~     self.contents.clear
#~     self.contents.font.color = IEX::DND_STATUS::FONT_COLOR
#~     self.contents.font.size = 18
#~     self.contents.font.bold = false
#~     edge_x = 20
#~     edge_y = 16
#~     # ~~Personal Data and stuff of that sort
#~     
#~     # Actor Name
#~       self.contents.draw_text(34 - edge_x, 12 - edge_y, self.width - 64, WLH, @actor.name)
#~       
#~     # Class And Level
#~       self.contents.draw_text(34 - edge_x, 37 - edge_y, self.width - 64, WLH, $data_classes[@actor.class_id].name)
#~       self.contents.draw_text(125 - edge_x, 37 - edge_y, self.width - 64, WLH, @actor.level)
#~      
#~     if IEX::DND_STATUS::ACTOR_PERSONAL_DATA.has_key?(@actor.id)
#~       act_dat = IEX::DND_STATUS::ACTOR_PERSONAL_DATA[@actor.id]
#~     # Race Alignment and Deity
#~       self.contents.draw_text(224 - edge_x, 37 - edge_y, self.width - 64, WLH, act_dat[:race].to_s)
#~       self.contents.draw_text(287 - edge_x, 37 - edge_y, self.width - 64, WLH, act_dat[:alignment].to_s)
#~       self.contents.draw_text(348 - edge_x, 37 - edge_y, self.width - 64, WLH, act_dat[:deity].to_s)
#~       
#~     # Size Age Gender Height
#~       self.contents.draw_text(34 - edge_x, 64 - edge_y, self.width - 64, WLH, act_dat[:size].to_s)
#~       self.contents.draw_text(77 - edge_x, 64 - edge_y, self.width - 64, WLH, act_dat[:age].to_s)
#~       self.contents.draw_text(120 - edge_x, 64 - edge_y, self.width - 64, WLH, act_dat[:gender].to_s)
#~       self.contents.draw_text(164 - edge_x, 64 - edge_y, self.width - 64, WLH, act_dat[:height].to_s)
#~       
#~     # Weight Eyes Hair Skin
#~       self.contents.draw_text(220 - edge_x, 64 - edge_y, self.width - 64, WLH, act_dat[:weight].to_s)
#~       self.contents.draw_text(263 - edge_x, 64 - edge_y, self.width - 64, WLH, act_dat[:eyes].to_s)
#~       self.contents.draw_text(312 - edge_x, 64 - edge_y, self.width - 64, WLH, act_dat[:hair].to_s)
#~       self.contents.draw_text(360 - edge_x, 64 - edge_y, self.width - 64, WLH, act_dat[:skin].to_s)
#~     end
#~       
#~     # ~~ Stats
#~     # x85 y120 / y Addition == 20
#~     
#~     # Str Dex Con Int Wis Cha
#~       self.contents.draw_text(85 - edge_x, 114 - edge_y, self.width - 64, WLH, @actor.atk)
#~       self.contents.draw_text(85 - edge_x, 136 - edge_y, self.width - 64, WLH, @actor.def)
#~       self.contents.draw_text(85 - edge_x, 158 - edge_y, self.width - 64, WLH, @actor.agi)
#~       self.contents.draw_text(85 - edge_x, 180 - edge_y, self.width - 64, WLH, @actor.spi)
#~       self.contents.draw_text(85 - edge_x, 202 - edge_y, self.width - 64, WLH, @actor.spi)
#~       self.contents.draw_text(85 - edge_x, 225 - edge_y, self.width - 64, WLH, @actor.agi)
#~      
#~     # Hp Ac
#~       self.contents.draw_text(187 - edge_x, 118 - edge_y, self.width - 64, WLH, @actor.maxhp)
#~       ac_def = 0
#~       for equ in @actor.equips
#~         next if equ == nil
#~         ac_def += equ.def
#~       end   
#~       self.contents.draw_text(187 - edge_x, 138 - edge_y, self.width - 64, WLH, ac_def)
#~       # x225 y120 width 126
#~       draw_actor_hp_gauge(@actor, 239 - edge_x, 110 - edge_y, 126)
#~       self.contents.draw_text(217 - edge_x, 118 - edge_y, self.width - 64, WLH, @actor.hp)
#~     #  
#~      
#~     # Face and Sprite x140 y180
#~       #draw_actor_face(@actor, 140 - edge_x, 180 - edge_y)
#~       draw_actor_graphic(@actor, (140 + 64) - edge_x, (180 - edge_y) + 74)
#~       
#~     # EXP  x320 y220
#~       self.contents.font.size = 23
#~       self.contents.draw_text(320 - edge_x, 218 - edge_y, self.width - 320, WLH, @actor.exp)
#~       self.contents.font.size = 18
#~       
#~     # Equipment x15 y305
#~     # Main Weapon
#~       draw_item_name(@actor.equips[0], 24 - edge_x, 272 - edge_y)
#~         if @actor.equips[0] != nil
#~           self.contents.draw_text(96, 290, 226, WLH, @actor.equips[0].description)
#~           self.contents.draw_text(128, 272 - edge_y, 226, WLH, @actor.equips[0].atk)
#~         end  
#~      
#~     # Shield or Two Swords
#~       if @actor.two_swords_style
#~         draw_item_name(@actor.equips[1], 24 - edge_x, 356 - edge_y)
#~         if @actor.equips[1] != nil
#~           self.contents.draw_text(96, 372, 226, WLH, @actor.equips[1].description)
#~           self.contents.draw_text(128, 356 - edge_y, 226, WLH, @actor.equips[1].atk)
#~         end  
#~       else  
#~         draw_item_name(@actor.equips[1], 342 - edge_x, 362 - edge_y)
#~         if @actor.equips[1] != nil
#~           self.contents.draw_text(386, 380, 216, WLH, @actor.equips[1].description)
#~           self.contents.draw_text(466, 346, 226, WLH, @actor.equips[1].def)
#~         end  
#~       end  
#~      
#~     # Armor  
#~       draw_item_name(@actor.equips[3], 342 - edge_x, 276 - edge_y)
#~       if @actor.equips[3] != nil
#~         self.contents.draw_text(528 - edge_x, 260, 226, WLH, @actor.equips[3].def)
#~         self.contents.draw_text(458 - edge_x, 296, 226, WLH, @actor.equips[3].agi)
#~       end
#~       
#~     # Helmet
#~       draw_item_name(@actor.equips[2], 20 - edge_x, 440 - edge_y)
#~       if @actor.equips[2] != nil
#~         self.contents.draw_text(140 - edge_x, 440 - edge_y, 36, WLH, @actor.equips[2].def, 1)
#~       end
#~       
#~     # Accessory
#~       draw_item_name(@actor.equips[4], 342 - edge_x, 440 - edge_y)
#~       if @actor.equips[4] != nil
#~         self.contents.draw_text(462 - edge_x, 440 - edge_y, 36, WLH, @actor.equips[4].def, 1)
#~       end
#~       
#~   end  
#~   
#~   #--------------------------------------------------------------------------
#~   # * Draw Item Name
#~   #     item    : Item (skill, weapon, armor are also possible)
#~   #     x       : draw spot x-coordinate
#~   #     y       : draw spot y-coordinate
#~   #     enabled : Enabled flag. When false, draw semi-transparently.
#~   #--------------------------------------------------------------------------
#~   def draw_item_name(item, x, y, enabled = true)
#~     if item != nil
#~       draw_icon(item.icon_index, x, y, enabled)
     # self.contents.font.color = normal_color
     # self.contents.font.color.alpha = enabled ? 255 : 128
#~       self.contents.draw_text(x + 24, y, 172, WLH, item.name)
#~     end
#~   end
#~   
#~ end
