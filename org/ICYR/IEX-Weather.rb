# IEX - Weather
#==============================================================================
# ** Spriteset_Weather
#------------------------------------------------------------------------------
#  Weather effect (rain, storm, snow) class. This class is used within the
# Spriteset_Map class.
#==============================================================================

class Spriteset_Weather
  #--------------------------------------------------------------------------
  # * Public Instance Variables
  #--------------------------------------------------------------------------
  attr_reader :type
  attr_reader :max
  attr_reader :ox
  attr_reader :oy
  #--------------------------------------------------------------------------
  # * Object Initialization
  #--------------------------------------------------------------------------
  alias iex_weather_initialize initialize unless $@
  def initialize(viewport = nil)
    iex_weather_initialize(viewport)
    pcolor1 = Color.new(255,182,193)
    pcolor2 = Color.new(255,0,255)
    @petal_bitmap = Bitmap.new(6, 6)
    @petal_bitmap.fill_rect(0, 1, 6, 4, pcolor2)
    @petal_bitmap.fill_rect(1, 0, 4, 6, pcolor2)
    @petal_bitmap.fill_rect(1, 2, 4, 2, pcolor1)
    @petal_bitmap.fill_rect(2, 1, 2, 4, pcolor1)
  end
  #--------------------------------------------------------------------------
  # * Dispose
  #--------------------------------------------------------------------------
  def dispose
    for sprite in @sprites
      sprite.dispose
    end
    @rain_bitmap.dispose
    @storm_bitmap.dispose
    @snow_bitmap.dispose
    @petal_bitmap.dispose
  end
  #--------------------------------------------------------------------------
  # * Set weather type
  #     type : new weather type
  #--------------------------------------------------------------------------
  def type=(type)
    return if @type == type
    @type = type
    case @type
    when 1
      bitmap = @rain_bitmap
    when 2
      bitmap = @storm_bitmap
    when 3
      bitmap = @snow_bitmap
    else
      bitmap = nil
    end
    for i in 0...@sprites.size
      sprite = @sprites[i]
      sprite.visible = (i <= @max)
      sprite.bitmap = bitmap
    end
  end

  #--------------------------------------------------------------------------
  # * Frame Update
  #--------------------------------------------------------------------------
  def update
    return if @type == 0
    for i in 1..@max
      sprite = @sprites[i]
      if sprite == nil
        break
      end
      if @type == 1
        sprite.x -= 2
        sprite.y += 16
        sprite.opacity -= 8
      end
      if @type == 2
        sprite.x -= 8
        sprite.y += 16
        sprite.opacity -= 12
      end
      if @type == 3
        sprite.x -= 2
        sprite.y += 8
        sprite.opacity -= 8
      end
      x = sprite.x - @ox
      y = sprite.y - @oy
      if sprite.opacity < 64
        sprite.x = rand(800) - 100 + @ox
        sprite.y = rand(600) - 200 + @oy
        sprite.opacity = 255
      end
    end
  end
end
