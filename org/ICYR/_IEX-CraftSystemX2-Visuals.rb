# *IEX - Craft System X2 - Visuals
#~ #==============================================================================#
#~ # ** IEX(Icy Engine Xelion) - Craft System X2
#~ #------------------------------------------------------------------------------#
#~ # ** Created by    : IceDragon (http://www.rpgmakervx.net/)
#~ # ** Script-Status : Addon (Actors)
#~ # ** Script Type   : State Modifier
#~ # ** Date Created  : 01/28/2011 (DD/MM/YYYY)
#~ # ** Date Modified : 01/28/2011 (DD/MM/YYYY)
#~ # ** Script Tag    : IEX - Craft System X2
#~ # ** Difficulty    : Hard, Lunatic
#~ # ** Version       : 2.0
#~ #------------------------------------------------------------------------------#
#~ $imported = {} if $imported == nil
#~ $imported["IEX_CraftSystem-X2"] = true

#~ class IEX_Window_ECS_Help < Window_Base
#~   
#~   def initialize(x, y, width, height)
#~     super(x, y, width, height)
#~     @item = 0
#~   end
#~   
#~   def set_item(item)
#~     item = RPG::BaseItem.new if item == nil
#~     return if @item == item
#~     self.contents.clear
#~     self.contents.draw_text(0, 0, self.contents.width, 24, item.name, 1)
#~     if item.is_a?(RPG::UsableItem)
#~       draw_item_eq_growths(0, 24, item)
#~     elsif item.is_a?(RPG::Weapon)
#~       draw_parameters(0, 24, item, 0)
#~     elsif item.is_a?(RPG::Armor)
#~       draw_parameters(0, 24, item, 0)
#~     end  
#~     @item = item
#~   end
#~    
#~   def draw_item_eq_growths(x, y, item)
#~     draw_parameters(x, y, item, 1)
#~     draw_parameters(x, y+72, item, 2)
#~   end
#~   
#~   def draw_parameters(x, y, item, type)
#~     wd = self.contents.width / 2
#~     draw_stat(x, y, item, :atk, type)
#~     draw_stat(x+wd, y, item, :def, type)
#~     
#~     draw_stat(x, y+24, item, :spi, type)
#~     draw_stat(x+wd, y+24, item, :res, type)
#~     
#~     draw_stat(x, y+48, item, :agi, type)
#~     draw_stat(x+wd, y+48, item, :dex, type) 
#~     if type == 0
#~       draw_stat(x, y+72, item, :recraft) 
#~       
#~       draw_stat(x, y+96, item, :clevel)
#~       draw_stat(x+wd, y+96, item, :cpoint)
#~       
#~       draw_stat(x, y+120, item, :cexp)
#~       draw_stat(x+wd, y+120, item, :cexpnxt)
#~     end  
#~   end
#~   
#~   def draw_stat(x, y, item, stat, type = 0)
#~     vo = ""
#~     val = 0
#~     icon = 0
#~     case stat
#~     when :atk
#~       vo = sprintf("%s :", Vocab.atk)
#~       case type 
#~       when 0
#~         val = item.atk
#~       when 1
#~         val = item.eq_stat_rate("ATK")
#~       when 2  
#~         val = item.eq_stat_change("ATK")
#~       end  
#~      icon = IEX::CRAFT_SYSTEM::STATS_ICONS[:atk_icon]
#~     when :def
#~       vo = sprintf("%s :", Vocab.def)
#~       case type 
#~       when 0
#~         val = item.def
#~       when 1
#~         val = item.eq_stat_rate("DEF")
#~       when 2  
#~         val = item.eq_stat_change("DEF")
#~       end  
#~       icon = IEX::CRAFT_SYSTEM::STATS_ICONS[:def_icon]
#~     when :spi
#~       vo = sprintf("%s :", Vocab.spi)
#~       case type 
#~       when 0
#~         val = item.spi
#~       when 1
#~         val = item.eq_stat_rate("SPI")
#~       when 2  
#~         val = item.eq_stat_change("SPI")
#~       end  
#~       icon = IEX::CRAFT_SYSTEM::STATS_ICONS[:spi_icon]
#~     when :agi
#~       vo = sprintf("%s :", Vocab.agi)
#~       case type 
#~       when 0
#~         val = item.agi
#~       when 1
#~         val = item.eq_stat_rate("AGI")
#~       when 2  
#~         val = item.eq_stat_change("AGI")
#~       end  
#~       icon = IEX::CRAFT_SYSTEM::STATS_ICONS[:agi_icon]
#~     when :dex
#~       vo = sprintf("%s :", Vocab.dex)
#~       case type 
#~       when 0
#~         val = item.dex
#~       when 1
#~         val = item.eq_stat_rate("DEX")
#~       when 2  
#~         val = item.eq_stat_change("DEX")
#~       end  
#~       icon = IEX::CRAFT_SYSTEM::STATS_ICONS[:dex_icon]
#~     when :res
#~       vo = sprintf("%s :", Vocab.res)
#~       case type 
#~       when 0
#~         val = item.res
#~       when 1
#~         val = item.eq_stat_rate("RES")
#~       when 2  
#~         val = item.eq_stat_change("RES")
#~       end  
#~       icon = IEX::CRAFT_SYSTEM::STATS_ICONS[:res_icon]
#~     when :clevel
#~       vo = sprintf("%s :", "C-Level")
#~       val = item.craft_level
#~       icon= IEX::CRAFT_SYSTEM::CRAFT_ICONS[:clevel]
#~     when :cexp
#~       vo = sprintf("%s :", "C-Exp")
#~       val = item.craft_exp
#~       icon= IEX::CRAFT_SYSTEM::CRAFT_ICONS[:cexp]
#~     when :recraft
#~       vo = sprintf("%s :", "RC")
#~       val = item.recraft_count
#~       icon= IEX::CRAFT_SYSTEM::CRAFT_ICONS[:rccount]
#~     when :cpoint
#~       vo = sprintf("%s :", "C-Points")
#~       val = item.craft_points
#~       icon= IEX::CRAFT_SYSTEM::CRAFT_ICONS[:cpoint]
#~     when :cexpnxt
#~       vo = sprintf("%s :", "C-Next")
#~       val = item.craft_exp_next
#~       icon= IEX::CRAFT_SYSTEM::CRAFT_ICONS[:cexpnxt]
#~     end  
#~     #val = 0 if val == nil
#~     tx_wd = (self.contents.width / 2) - 32
#~     draw_icon(icon, x, y, val != 0)
#~     case stat
#~     when :clevel, :cpoint, :recraft, :cexp, :cexpnxt
#~       val = val
#~     else
#~       if val.to_i > 0
#~         val = sprintf("+%s", val.to_s)
#~       end 
#~     end  
#~     if type == 1
#~       val = sprintf("%s%", val.to_s)
#~       vo = sprintf("%s%", vo.to_s)
#~     elsif type == 2
#~       vo = sprintf("%s+", vo.to_s)
#~     end  
#~     self.contents.font.color = system_color
#~     self.contents.draw_text(x + 24, y, tx_wd, 24, vo, 0)
#~     self.contents.font.color = normal_color
#~     self.contents.font.size = 18
#~     self.contents.draw_text(x + 24, y, tx_wd, 28, val, 2)
#~     self.contents.font.size = Font.default_size 
#~   end
#~   
#~ end

#~ class IEX_Window_Item_List < Window_Selectable
#~   
#~   def initialize(x, y, width, height)
#~     super(x, y, width, height)
#~     @filter = -1
#~     @data = []
#~     @index = 0
#~     refresh
#~   end
#~   
#~   def selected_item
#~     return @data[@index]
#~   end
#~   
#~   def change_filter(new_filter)
#~     return if new_filter == nil
#~     @filter = new_filter
#~     refresh
#~   end
#~   
#~   def refresh
#~     @data = []
#~     case @filter.to_i
#~     when 0
#~       for ite in $game_party.items
#~         @data.push(ite) if ite.is_a?(RPG::UsableItem)
#~       end  
#~     when 1
#~       unless $imported["IEX_WeapArm_Isolate"]
#~         for ite in $game_party.items
#~           @data.push(ite) if ite.is_a?(RPG::Weapon)
#~         end
#~       else  
#~         for ite in $game_party.isol_items
#~           @data.push(ite) if ite.is_a?(RPG::Weapon)
#~         end
#~       end  
#~     when 2
#~       unless $imported["IEX_WeapArm_Isolate"]
#~         for ite in $game_party.items
#~           @data.push(ite) if ite.is_a?(RPG::Armor)
#~         end  
#~       else
#~         for ite in $game_party.isol_items
#~           @data.push(ite) if ite.is_a?(RPG::Armor)
#~         end
#~       end  
#~     end
#~     @item_max = @data.size
#~     self.contents.clear
#~     create_contents
#~     for i in 0...@item_max
#~       draw_item(i)
#~     end  
#~     update
#~   end

#~   #--------------------------------------------------------------------------
#~   # * Draw Item
#~   #     index   : item number
#~   #     enabled : enabled flag. When false, draw semi-transparently.
#~   #--------------------------------------------------------------------------
#~   def draw_item(index, enabled = true)
#~     rect = item_rect(index)
#~     rect.x += 4
#~     rect.width -= 8
#~     self.contents.clear_rect(rect)
#~     self.contents.font.size = 18
#~     self.contents.font.color = normal_color
#~     self.contents.font.color.alpha = enabled ? 255 : 128
#~     if @data[index] != nil
#~       text = @data[index].name
#~       icon = @data[index].icon_index
#~       num  = $game_party.item_number(@data[index])
#~       clevel = @data[index].craft_level
#~     else
#~       text = "-----------------"
#~       icon = 98
#~       num  = 0
#~       clevel = 0
#~     end  
#~     draw_icon(icon, rect.x, rect.y, enabled)
#~     rect.x += 24
#~     rect.width -= 8
#~     self.contents.draw_text(rect, text)
#~     draw_icon(144, self.contents.width - 24, rect.y)
#~     self.contents.draw_text(0, rect.y, self.contents.width - 24, rect.height, num, 2)
#~     self.contents.font.color = system_color
#~     draw_icon(IEX::CRAFT_SYSTEM::CRAFT_ICONS[:clevel], self.contents.width - 64, rect.y)
#~     self.contents.draw_text(0, rect.y, self.contents.width - 64, rect.height, clevel, 2)
#~   end
#~   
#~ end

#~ class IEX_ECS_Item_Window < ICY_HM_Window_Selectable
#~   
#~   def initialize(dat, x = 0, y = 0, width = Graphics.width, height = Graphics.height)
#~     @data = [] 
#~     super(x, y, width, height)
#~     @column_max = 12
#~     @item_sq_spacing = 42 #(self.width / 4) - 32
#~     @rect_size = 32
#~     @selection_size = 38
#~     @index = 0
#~     @data = dat
#~     refresh
#~   end
#~     
#~   def current_item
#~     return @data[@index]
#~   end
#~   
#~   def change_data(new_data)
#~     @data = new_data.to_a
#~     @index = 0
#~     refresh
#~   end
#~   
#~   def refresh 
#~     @item_max = 12 * 4
#~     self.contents.clear
#~     create_contents
#~     prep_coord_vars
#~     for i in 0...@item_max
#~       draw_item(i)
#~     end
#~   end
#~   
#~   def draw_item(index)
#~     rect = Rect.new(@nw_x, @nw_y, @rect_size, @rect_size)
#~     ite = @data[index]
#~     color1 = Color.new(166, 124, 82, 128)
#~     color2 = Color.new(126, 84, 42)
#~     self.contents.fill_rect(rect, color1)
#~     draw_border_rect(@nw_x, @nw_y, @rect_size, @rect_size, 4, color2)
#~     draw_icon(6482, @nw_x + 4, @nw_y + 4)
#~     unless ite == nil  
#~       draw_icon(ite.icon_index, @nw_x + 4, @nw_y + 4) 
#~     end  
#~     advance_space
#~   end
#~   
#~ end

#~ #==============================================================================
#~ # ** Window_PartyCraft
#~ #------------------------------------------------------------------------------
#~ #==============================================================================
#~ class Window_PartyCraft < Window_PartyReserve
#~       
#~   def refresh
#~ 	  self.contents.clear
#~ 	  @actors = $game_party.all_members    
#~     @item_max = @actors.size
#~     @column_max = [@item_max,1].max
#~     create_contents
#~ 	  for i in 0...@item_max
#~   	  draw_item(i)
#~   	end
#~   end
#~     
#~   def draw_item(index)
#~     rect = item_rect(index)
#~     self.contents.clear_rect(rect)
#~ 	  actor = @actors[index]
#~     unless actor.nil?
#~ 	    aname  = actor.name
#~ 	    aclass = actor.class.name
#~ 	    alevel = actor.level
#~       ash = (rect.height-@character_rect.height)/2
#~ 	    locked = actor.craft_en < (actor.maxcraft_en/2)
#~       ch = @character_rect.height
#~       draw_actor_graphic(actor, rect.x+16, rect.y+@character_rect.height+ash+4, !locked)
#~       coun = 0 
#~ 	    self.contents.font.color.alpha = locked ? 128 : 255
#~ 	    self.contents.font.size = 12
#~       draw_grad_bar(rect.x+6, rect.y+16, rect.width-12, 8, 
#~         actor.craft_en, actor.maxcraft_en, # Values
#~         hp_gauge_color1, hp_gauge_color2, Color.new(20, 20, 20, 168), # Colors
#~         2)
#~ 	    self.contents.draw_text(rect.x+3,rect.y - 4,rect.width,24,aname)
#~ 	    self.contents.draw_text(rect.x,rect.y+18,rect.width-2,24,"Lv.",2)
#~ 	    self.contents.draw_text(rect.x,rect.y+28,rect.width-2,24,alevel,2)
#~ 	    self.contents.draw_text(rect.x,rect.y+17+ch,rect.width-3,24,aclass,2)      
#~       self.contents.font.size = 14
#~       s1 = actor.craft_en ; s2 = actor.maxcraft_en
#~       rect.x += 12
#~       rect.y += 8
#~       rect.height = 24
#~       rect.width -= 12
#~       self.contents.draw_text(rect, "#{s1}/#{s2}", 1)
#~ 	  end
#~   end
#~   
#~ end

#~ class Window_ActorCraftStats < Window_Base
#~   
#~   include IEX::CRAFT_SYSTEM
#~   
#~   def initialize(x, y, width, height)
#~     super(x, y, width, height)
#~     @actor = nil
#~     refresh
#~   end
#~   
#~   def change_actor(new_actor)
#~     if @actor != new_actor
#~       @actor = new_actor
#~       refresh
#~     end  
#~   end
#~   
#~   def refresh
#~     self.contents.clear
#~     create_contents
#~     unless @actor.nil?
#~       coun = 0
#~       for key in CRAFT_EXPSET_ORDER
#~         x = 64 * (coun / 4)
#~         y = 24 * (coun % 4)
#~         rect = Rect.new(x, y, 48, 24)
#~         draw_grad_bar(rect.x+6, rect.y+rect.height-12, rect.width, 8, 
#~           @actor.craft_expset[key][0], @actor.craft_expset[key][1], # Values
#~           mp_gauge_color1, mp_gauge_color2, Color.new(20, 20, 20, 168), # Colors
#~           2)
#~         draw_icon(CRAFT_EXSETICONS[key], rect.x, rect.y)
#~         s1 = @actor.craft_expset[key][0]
#~         s2 = @actor.craft_expset[key][1]
#~         self.contents.font.size = 14
#~         rect.x += 18
#~         rect.y -= 4
#~         self.contents.draw_text(rect, "#{s1}/#{s2}")
#~         coun += 1  
#~       end  
#~     end  
#~   end
#~   
#~ end

#~ class Scene_Earthen_Forge < Scene_Base
#~   
#~   include IEX::CRAFT_SYSTEM
#~   
#~   def initialize
#~     super
#~   end

#~   def start
#~     super
#~     create_menu_background
#~     @windows = {}
#~     iwps = [Graphics.width / 2, Graphics.height / 2, Graphics.width, Graphics.height]
#~     @windows["Comm1"]      = Window_Command.new(192, ["Craft", "Recraft", "Upgrade", "Cancel"])
#~     @windows["Comm1"].x    = iwps[0]
#~     @windows["Comm1"].y    = iwps[1]
#~     @windows["Party"]      = Window_PartyCraft.new(0, 0)
#~     @windows["Party"].width= Graphics.width
#~     @windows["Party"].y    = Graphics.height - @windows["Party"].height     
#~     @windows["Party"].refresh
#~     @windows["Cstat"]      = Window_ActorCraftStats.new(0, 0, iwps[0], 128)
#~     @windows["Cstat"].y    = iwps[1]
#~     @windows["Cstat"].height = Graphics.height - @windows["Cstat"].y - @windows["Party"].height
#~     @windows["Cstat"].change_actor(@windows["Party"].current_actor)
#~     @item_list = ["ItemList", "WeaponList", "ArmorList"]
#~     coun = 0
#~     for win in @item_list 
#~       @windows[win]   = IEX_Window_Item_List.new(0, 0, iwps[0], iwps[1])
#~       @windows[win].change_filter(coun)
#~       @windows[win].visible  = false
#~       @windows[win].active   = false
#~       coun += 1
#~     end
#~     @index = 0
#~     
#~     @windows["Help"] = IEX_Window_ECS_Help.new(iwps[0], 0, iwps[0], iwps[1])
#~     @windows["Help"].set_item(item_window_at_index_item)    
#~     
#~     @windows["Help"].visible = false
#~     @windows["Party"].active = false
#~     #item_window_at_index_visible(@index, true)
#~   end

#~   def terminate
#~     super
#~     for win in @windows.values
#~       next if win == nil
#~       win.dispose
#~       win = nil
#~     end  
#~     @windows.clear
#~     @windows = {}
#~   end
#~   
#~   def update
#~     super
#~     if Input.trigger?(Input::C)
#~       Sound.play_decision
#~       obj = item_window_at_index_item
#~       $game_party.lose_item(obj, 1)
#~       props = ["Atk: +2", "Def: +1", "Agi: +3"]
#~       orobj = IEX::ECS_Craft_System.original_obj(obj)
#~       opobj = IEX::ECS_Craft_System.craft(obj, props)
#~       opobj.recraft
#~       opobj.name = IEX::ECS_Craft_System.generate_name(orobj, opobj)
#~       for ele in opobj.element_set+[0] 
#~         unless TYPE_ELEMENTS2[ele].nil?
#~           @windows["Party"].current_actor.increase_craft_expset(TYPE_ELEMENTS2[ele], 5)
#~         end  
#~       end  
#~       $game_party.gain_item(opobj, 1)
#~       @windows[@item_list[@index]].refresh
#~       @windows["Cstat"].refresh
#~     elsif Input.trigger?(Input::B)
#~       $scene = Scene_Map.new
#~     elsif Input.trigger?(Input::RIGHT)
#~       Sound.play_cursor
#~       item_window_at_index_visible(@index, false)
#~       @index = (@index + 1) % @item_list.size
#~       item_window_at_index_visible(@index, true)
#~     elsif Input.trigger?(Input::LEFT)  
#~       Sound.play_cursor
#~       item_window_at_index_visible(@index, false)
#~       @index = (@index - 1) % @item_list.size
#~       item_window_at_index_visible(@index, true)
#~     end  
#~     @windows["Help"].set_item(item_window_at_index_item)
#~     @windows["Cstat"].change_actor(@windows["Party"].current_actor)
#~     for win in @windows.values
#~       next if win == nil
#~       win.update if win.active
#~     end  
#~   end
#~   
#~   def item_window_at_index_visible(index, bool)
#~     @windows[@item_list[index]].visible = bool
#~     @windows[@item_list[index]].active  = bool
#~   end
#~   
#~   def item_window_at_index_item
#~     return @windows[@item_list[@index]].selected_item
#~   end
#~   
#~ end

