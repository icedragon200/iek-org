# #~IEX - Next Actor Face
#~ #------------------------------------------------------------------------------#
#~ # ** IEX(Icy Engine Xelion) - Status Next Actor
#~ # ** Created by : IceDragon
#~ # ** Script-Status : Addon 
#~ # ** Date Created : 9/27/2010
#~ # ** Date Modified : 9/27/2010
#~ # ** Version : 1.0
#~ #------------------------------------------------------------------------------#

#~ #------------------------------------------------------------------------------#
#~ # ** Start Customization
#~ #------------------------------------------------------------------------------#
#~ module IEX
#~   module Actor_Next_Status
#~     NX_X = 0
#~     NX_Y = Graphics.height - 128
#~   end
#~ end
#~ #------------------------------------------------------------------------------#
#~ # ** End Customization
#~ #------------------------------------------------------------------------------#

#~ #==============================================================================
#~ # ** IEX_Next_Actor_Window
#~ #------------------------------------------------------------------------------
#~ #==============================================================================
#~ class IEX_Next_Actor_Window < Window_Selectable
#~    
#~   def initialize(mem_index, x = 0, y = 0)
#~     super(x, y, 332, 128)
#~     self.x = IEX::Actor_Next_Status::NX_X
#~     self.y = IEX::Actor_Next_Status::NX_Y
#~     self.opacity = 0
#~     @member_index = mem_index
#~     @index = 1
#~     @item_max = 3
#~     @selection_size = 100
#~     @item_sq_spacing = 100
#~     @column_max = 3
#~     @rect_size = 96
#~     refresh
#~   end

#~   def off_x ; return 0 end
#~   def off_y ; return 0 end
#~   
#~   def refresh 
#~     # I was lazy okay, so it looks like shit...
#~     x_pla = (self.contents.width - 300) / 2
#~     y_pla = (self.contents.height - 96) / 2
#~     actor1 = $game_party.members[(@member_index - 1) % $game_party.members.size]
#~     actor2 = $game_party.members[@member_index]
#~     actor3 = $game_party.members[(@member_index + 1) % $game_party.members.size]
#~     draw_actor_face(actor1, x_pla, y_pla)
#~     x_pla += @item_sq_spacing
#~     draw_actor_face(actor2, x_pla, y_pla)
#~     x_pla += @item_sq_spacing
#~     draw_actor_face(actor3, x_pla, y_pla)
#~     update
#~   end
#~   
#~   #--------------------------------------------------------------------------
#~   # * Update cursor
#~   #--------------------------------------------------------------------------
#~   def update_cursor
#~     if @index < 0                   # If the cursor position is less than 0
#~       self.cursor_rect.empty        # Empty cursor
#~     else                            # If the cursor position is 0 or more
#~       row = @index / @column_max    # Get current row
#~       if row < top_row              # If before the currently displayed
#~         self.top_row = row          # Scroll up
#~       end
#~       if row > bottom_row           # If after the currently displayed
#~         self.bottom_row = row       # Scroll down
#~       end
#~        y_l = @index / @column_max
#~        y_pos = ((@item_sq_spacing * y_l)- self.oy) + off_y
#~        x_l = self.index - (@column_max * y_l)
#~        x_pos = (x_l * @item_sq_spacing) + off_x
#~        if @selection_size > @rect_size
#~          subitive = (@selection_size - @rect_size) / 2
#~          self.cursor_rect.set(x_pos.to_i - subitive, y_pos.to_i - subitive, @selection_size, @selection_size)
#~        elsif @selection_size < @rect_size
#~          additive = (@rect_size - @selection_size) / 2
#~          self.cursor_rect.set(x_pos.to_i + additive, y_pos.to_i + additive, @selection_size, @selection_size)
#~         else
#~         self.cursor_rect.set(x_pos.to_i, y_pos.to_i, @selection_size, @selection_size)
#~        end
#~     end
#~   end
#~   
#~ end

#~ #==============================================================================
#~ # ** Scene_Status
#~ #------------------------------------------------------------------------------
#~ #==============================================================================

#~ class Scene_Status < Scene_Base

#~   #--------------------------------------------------------------------------
#~   # * Start processing * Aliased
#~   #--------------------------------------------------------------------------
#~   alias iex_next_act_face_start start unless $@
#~   def start
#~     iex_next_act_face_start
#~     @iex_next_act_win = IEX_Next_Actor_Window.new(@actor_index)
#~   end
#~   
#~   #--------------------------------------------------------------------------
#~   # * Termination Processing * Aliased
#~   #--------------------------------------------------------------------------
#~   alias iex_next_act_face_terminate terminate unless $@
#~   def terminate
#~     iex_next_act_face_terminate
#~     if @iex_next_act_win != nil
#~       @iex_next_act_win.dispose
#~       @iex_next_act_win = nil
#~     end  
#~   end
#~   
#~ end
