# #~IEX - Rated Open Windows
#~ module IEX
#~   module WIN_BASE
#~     WAIT_TIME = 10
#~   end
#~ end

#~ class Window_Base < Window
#~   #--------------------------------------------------------------------------
#~   # * Frame Update
#~   #--------------------------------------------------------------------------
#~   def update
#~     super
#~     update_opening
#~   end
#~   
#~   def update_opening
#~     if @opening
#~       amt = (255 / IEX::WIN_BASE::WAIT_TIME)
#~       self.openness += amt
#~       self.opacity += amt
#~       @opening = false if self.openness == 255
#~     elsif @closing
#~       amt = (255 / IEX::WIN_BASE::WAIT_TIME)
#~       self.openness -= amt
#~       self.opacity -= amt
#~       @closing = false if self.openness == 0
#~     end
#~   end
#~   
#~ end
