# IEX - Set Stat to 0
# Original by Mithran and fixed by Mithran 
#(So what did Ice do? >.> He made the regexp module and added in Notebox support) 
$imported = {} if $imported == nil
$imported["IEX_Set_Stat"] = true

module IEX
  module REGEXP
    module STAT_0
      SET_STAT = /<(\w+)[ ](?:IEX_SET_STAT|iex set stat)[ ]*(\d+)>/i
    end
  end
end

class RPG::Enemy
  
  alias iex_set_stat012_rpge_initialize initialize unless $@
  def initialize(*args)
    iex_set_stat012_rpge_initialize(*args)
    iex_set_stat_cache
  end
  
  def iex_set_stat_cache
    @set_stat = {}
    self.note.split(/[\r\n]+/).each { |line|
    case line
    when IEX::REGEXP::STAT_0::SET_STAT
      @set_stat[$1.to_s.upcase] = $2.to_i
    end
    }
  end
  
  def get_stat_set(stat)
    iex_set_stat_cache if @set_stat == nil
    t_st = stat.upcase
    if @set_stat.has_key?(t_st)
      return @set_stat[t_st]
    else
      return nil
    end  
  end
  
end

class Game_Enemy < Game_Battler
  
  ['atk', 'def', 'spi', 'agi', 'maxhp', 'maxmp'].each { |meth|
  aStr = %Q(
  alias iex_set_stat012_#{meth} #{meth} unless $@
  def #{meth}
    val = enemy.get_stat_set('#{meth}') # we want the string value to be passed as an argument, not the actual method
    return val if val != nil
    iex_set_stat012_#{meth}
  end
  )
  module_eval(aStr)
  }
  
end
