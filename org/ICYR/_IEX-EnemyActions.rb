# ~IEX - Enemy Actions
#~ module IEX
#~   module ENEMY_ACTION
#~     MANUAL_TROOPS = [2]
#~   end  
#~ end

#~ class Game_Enemy < Game_Battler
#~   
#~   def process_ipackage
#~     case @ipackage.to_sym
#~     when :assault
#~       process_assault_package
#~     when :support
#~       process_support_package
#~     when :healy
#~       process_healy_package
#~     when :wild
#~       process_wild_package
#~     when :basher  
#~       process_basher_package
#~     when :weaklink  
#~       process_weaklink_package
#~     when :stronglink
#~       process_stronglink_package
#~     else
#~       @ipackage = nil
#~       iex_eny_act_make_action
#~     end
#~   end
#~   
#~   def process_assault_package
#~   end
#~   
#~   def process_support_package
#~   end
#~   
#~   def process_healy_package
#~   end
#~   
#~   def strongest_action(acts)
#~     for act in acts
#~     case act.kind
#~     when 0
#~     end  
#~     end  
#~   end
#~   
#~   def randomize_actions(acts) 
#~     racts = acts.dup
#~     new_order = Array.new(acts.size)  
#~     used = []
#~     val = 0
#~     while racts.size > 0
#~       naction = racts.shift
#~       while new_order[val] != nil
#~         val = rand(acts.size+1)
#~       end  
#~       new_order[val] = naction
#~     end 
#~     return new_order
#~   end  
#~   
#~   def process_wild_package
#~     @action.clear
#~     return unless movable?
#~     available_actions = []
#~     for action in enemy.actions
#~       next unless conditions_met?(action)
#~       if action.kind == 1
#~         next unless skill_can_use?($data_skills[action.skill_id])
#~         next unless $data_skills[action.skill_id].base_damage > 0
#~       end
#~       available_actions.push(action)
#~     end
#~     available_actions = randomize_actions(available_actions)
#~     action = available_actions[0]
#~       @action.kind = action.kind
#~       @action.basic = action.basic
#~       @action.skill_id = action.skill_id
#~       @action.decide_random_target
#~       return
#~   end
#~   
#~   def process_basher_package
#~     @action.clear
#~     return unless movable?
#~     available_actions = []
#~     for action in enemy.actions
#~       next unless conditions_met?(action)
#~       if action.kind == 1
#~         next unless skill_can_use?($data_skills[action.skill_id])
#~         next unless $data_skills[action.skill_id].base_damage > 0
#~       end
#~       available_actions.push(action)
#~     end
#~   end
#~   
#~   def process_weaklink_package
#~     make_offensive_action
#~     targ = get_weaklink  
#~     if targ != nil
#~       action.target_index = targ.index
#~     end  
#~   end
#~   
#~   def process_stronglink_package
#~     make_offensive_action
#~     targ = get_stronglink
#~     if targ != nil
#~       action.target_index = targ.index
#~     end 
#~   end
#~   
#~   def get_weaklink(type = :enemy)
#~     case type
#~     when :ally
#~       return $game_troop.lowest_hp
#~     when :enemy
#~       return $game_party.lowest_hp
#~     else
#~       return nil  
#~     end  
#~   end  
#~   
#~   def get_stronglink(type = :enemy)
#~     case type
#~     when :ally
#~       return $game_troop.highest_hp
#~     when :enemy
#~       return $game_party.highest_hp
#~     else
#~       return nil
#~     end  
#~   end
#~    
#~   def make_offensive_action
#~     @action.clear
#~     return unless movable?
#~     available_actions = []
#~     rating_max = 0
#~     for action in enemy.actions
#~       next unless conditions_met?(action)
#~       if action.kind == 1
#~         next unless skill_can_use?($data_skills[action.skill_id])
#~         next unless $data_skills[action.skill_id].base_damage > 0
#~       end
#~       available_actions.push(action)
#~       rating_max = [rating_max, action.rating].max
#~     end
#~     ratings_total = 0
#~     rating_zero = rating_max - 3
#~     for action in available_actions
#~       next if action.rating <= rating_zero
#~       ratings_total += action.rating - rating_zero
#~     end
#~     return if ratings_total == 0
#~     value = rand(ratings_total)
#~     for action in available_actions
#~       next if action.rating <= rating_zero
#~       if value < action.rating - rating_zero
#~         @action.kind = action.kind
#~         @action.basic = action.basic
#~         @action.skill_id = action.skill_id
#~         case @isubpackage
#~         when :weaklink 
#~           targ = get_weaklink
#~           if targ != nil
#~             @action.target_index = targ.index
#~             return
#~           end  
#~         when :stronglink 
#~           targ = get_stronglink
#~           if targ != nil
#~             @action.target_index = targ.index
#~             return
#~           end 
#~         end 
#~         @action.decide_random_target
#~         return
#~       else
#~         value -= action.rating - rating_zero
#~       end
#~     end
#~   end
#~     
#~ end  

#~ class Game_Unit
#~   
#~   def sickly
#~     for mem in members
#~     end  
#~   end
#~   
#~   def dead_guy
#~     for mem in members
#~       return mem if mem.dead?
#~     end  
#~     return nil
#~   end
#~   
#~   def highest_hp
#~     value = 0
#~     for mem in members
#~       if mem.hp > value
#~         value = mem.hp
#~         retmem = mem
#~       end  
#~     end  
#~     return retmem
#~   end
#~   
#~   def highest_mp
#~     value = 0
#~     for mem in members
#~       if mem.mp > value
#~         value = mem.mp
#~         retmem = mem
#~       end  
#~     end  
#~     return retmem
#~   end
#~   
#~   def lowest_hp
#~     value = 999999999
#~     for mem in members
#~       if mem.hp < value
#~         value = mem.hp
#~         retmem = mem
#~       end  
#~     end  
#~     return retmem
#~   end
#~   
#~   def lowest_mp
#~     value = 999999999
#~     for mem in members
#~       if mem.mp < value
#~         value = mem.mp
#~         retmem = mem
#~       end  
#~     end  
#~     return retmem
#~   end
#~   
#~ end  
#~ #===============================================================================
#~ # Game_Troop
#~ #===============================================================================

#~ class Game_Troop < Game_Unit
#~   
#~   include IEX::ENEMY_ACTION
#~   
#~   def custom_troop_action
#~     case @troop_id
#~     when 2
#~       process_SAR_action
#~     end  
#~   end
#~   
#~   def process_SAR_action
#~     silica = [nil, false]
#~     ravage = [nil, false]
#~     arc = [nil, false]
#~     for mem in members
#~       silica = [mem, false] if mem.enemy_id == 5
#~       ravage = [mem, false] if mem.enemy_id == 4
#~       arc    = [mem, false] if mem.enemy_id == 3 
#~     end
#~     if ravage[0] != nil
#~       ravage[0].set_ipackage(:wild)
#~     end  
#~     if silica[0] != nil
#~       silica[0].set_ipackage(:wild)
#~     end  
#~     if arc[0] != nil 
#~       arc[0].set_ipackage(:wild)
#~     end  
   # if silica[0] != nil
   #   if silica[0].skill_can_use?($data_skills[2])
   #     silica[0].action.set_skill(2)
   #     silica[1] = true
   #   else
   #     silica[0].make_action
   #     silica[1] = true
   #   end  
   #   if silica[0].action.skill_id == 2
   #     arc[0].action.set_skill(48)
   #     arc[1] = true
   #  else
   #     arc[0].make_action
   #     arc[1] = true
   #   end 
   # end
   # if arc[0] != nil
   #   if arc[0].hp < IEX::IMath.cal_percent(50, arc[0].maxhp)
   #     ravage[0].action.set_skill(3)
   #     ravage[0].action.target_index = arc[0].troop_index
   #     ravage[1] = true
   #   end 
   # end
   # if silica[0] != nil and ravage[1] == false
   #   if silica[0].hp < IEX::IMath.cal_percent(50, silica[0].maxhp)  
   #     ravage[0].package = :protect  
   #     ravage[0].make_action
   #     ravage[1] = true
   #   elsif silica[0].mp < IEX::IMath.cal_percent(50, silica[0].maxmp)   
   #     ravage[0].action.set_skill(4)
   #     ravage[0].action.target_index = silica[0].index
   #     ravage[1] = true
   #   end  
   # end 
#~     if arc[0] != nil and arc[1] == false
#~       arc[0].make_action
#~     end
#~     if ravage[0] != nil and ravage[1] == false
#~       ravage[0].make_action
#~     end
#~     if silica[0] != nil and silica[1] == false
#~       silica[0].make_action
#~     end  
#~   end
#~   
#~ end

#~ class Game_Battler
#~   
#~   def iex_check_action_condition(action)
#~     return false if action == nil
#~     actis = action.condition_param1
#~     actis = [action.condition_param1] unless action.condition_param1.is_a?(Array)
#~     for act in actis
#~       case act.to_s.upcase
#~       when "HP"
#~         hp_rate = hp * 100.0 / maxhp
#~         return false if hp_rate < action.condition_param2[0]
#~         return false if hp_rate > action.condition_param2[1]
#~       when "MP"  
#~         mp_rate = mp * 100.0 / maxmp
#~         return false if mp_rate < action.condition_param2[0]
#~         return false if mp_rate > action.condition_param2[1]
#~       when "HAS_STATE", "HASSTATE", "HAS STATE"  
#~         return false unless state?($data_states[action.condition_param2])
#~       when "NOT_STATE", "NOTSTATE", "NOT STATE"  
#~         return false if state?($data_states[action.condition_param2])
#~       end  
#~     end
#~     return true
#~   end
#~     
#~ end

#~ class RPG::Enemy
#~   
#~   def iex_pack_cache
#~     @iex_pack_cache_complete = false
#~     self.note.split(/[\r\n]+/).each { |line|
#~     case line
#~     when /<(?:PACKAGE):[ ]*(.*)>/i
#~       @ipackages = $1.to_s.downcase.intern
#~     when /<(?:SUB_PACKAGE|sub package):[ ]*(.*)>/i
#~       @isubpackages = $1.to_s.downcase.intern
#~     end  
#~     }
#~     @iex_pack_cache_complete = true
#~   end
#~   
#~   def ipackages
#~     iex_pack_cache unless @iex_pack_cache_complete
#~     return @ipackages
#~   end
#~   
#~   def isubpackages
#~     iex_pack_cache unless @iex_pack_cache_complete
#~     return @isubpackages
#~   end
#~   
#~ end

#~ class Game_Enemy < Game_Battler
#~   
#~   alias iex_epp_initialize initialize unless $@
#~   def initialize(*args)
#~     iex_epp_initialize(*args)
#~     set_isub_package(enemy.isubpackages)
#~     set_ipackage(enemy.ipackages) 
#~   end
#~   
#~   def set_isub_package(spacka)
#~     @isubpackage = spacka
#~   end
#~   
#~   def set_ipackage(packa)
#~     @ipackage = packa
#~   end
#~   
#~   def troop_index
#~     return @index
#~   end  
  ##--------------------------------------------------------------------------
  ## * Determine if Action Conditions are Met
  ##     action : battle action
  ##--------------------------------------------------------------------------
  #alias iex_eny_actions_conditions_met conditions_met? unless $@
  #def conditions_met?(action)
  #  ans = iex_eny_actions_conditions_met(action)
  #  case action.condition_type
  #  when 7 # Custom Triggers
  #    ans = iex_check_action_condition(action)
  #  end
  #  return ans
  #end
#~   
#~   alias iex_eny_act_make_action make_action unless $@
#~   def make_action
#~     if @ipackage.nil?
#~       iex_eny_act_make_action
#~       return 
#~     end
#~     process_ipackage
#~   end
#~   
#~ end

#~ #===============================================================================
#~ # Game_Troop
#~ #===============================================================================

#~ class Game_Troop < Game_Unit
#~     
#~   #--------------------------------------------------------------------------
#~   # overwrite method: make_actions
#~   #--------------------------------------------------------------------------
#~   def make_actions
#~     if @preemptive and ($scene.is_a?(Scene_Battle) and !$scene.atb?)
#~       clear_actions
#~     else
#~       if MANUAL_TROOPS.include?(@troop_id)
#~         custom_troop_action
#~       else  
#~         for enemy in members; enemy.make_action; end
#~       end   
#~     end
#~   end
#~   
#~ end
