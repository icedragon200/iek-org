# ICY_ClanSystem
#==============================================================================#
# ** ICY Clan System
#------------------------------------------------------------------------------#
# ** Created by : IceDragon
# ** Script-Status : Addon
# ** Date Created :  5/13/2010
# ** Date Modified : 8/08/2010
# ** Version : 0.3
#------------------------------------------------------------------------------#
#==============================================================================#
# JUMPS
# 1-ICY_SN_Scene
# 2-
#
imported = {} if $imported == nil
$imported["ICY_Clan_System"] = true

module ICY
  module Clan_System
    #Selectable Icons to use for clan : 16 icons
    Party_Icons = [2, 4, 6, 7, 9, 10, 11, 12, 13, 14, 
                   16, 18, 24, 36, 38, 42, 53, 57, 68, 74, 
                   86, 91, 128, 132, 144]
                   
    RECT_SIZE = 32
    BORDER_SIZE = 4
    ITEM_SQ_SPACING = 48
    SELECTION_SIZE = 40
    ITEM_COLUMN_MAX = 10
    NMIC_ACCEPT_TEXT = "Is this okay?"
    
    def self.get_party_icon
     return $game_party.party_icon
    end
   
    def self.get_party_name
      return $game_party.party_name
    end
    
    CLAN_STATS = [
   #["name", icon_index, variable, maxvalue]
    ["Smithing", 9, 50, 200],
    ["Communication", 10, 51, 200],
    ["Teamwork", 11, 52, 200],
    ["Combat", 12, 53, 200],
    ["Technique", 13, 54, 200],
    ["Hunting", 2, 55, 200],
    ]
    
    CLAN_RANKS = [
   #["Name", icon_index],
    ["Clay"], 
    ["Stone"], 
    ["Iron"], 
    ["Reinforced Iron"], 
    ["Steel"],
    ]
    
    CONFIRM_WINDOW_CONFIG = {
  # "Commands" => [["Accept",icon_index],["Decline",icon_index]]
    "Commands" => [["Accept", 27],["Decline", 28]],
    "HelpText" => ["Take this as the Clan Icon", "Choose a differnet Icon"],
  # "Size" => [x, y, width, height, opacity]  
    "Size" => [(544 - 256) / 2, (416 - 138) / 2, 256, 138, 255],
    "Spacing" => 56,
    "RectSize" => 48,
    "SelectionSize" => 52,
    "Columns" => 2,
    }
    CLAN_WINDOW_SIZES = {}
    SETUP_WINDOW_SIZES = {
    "Header" => [0, 0, 544, 56, 255],
    "Icon"   => [386, 168, 544, 248 , 255],
    "Name"   => [0, 56, 386, 56, 255],
    "Input"  => [0, 168, 386, 248, 255],
    "Party"  => [386, 56, 158, 56, 255],
    "Help"   => [0, 112, 544, 56, 255],
    }
  end
end

#==============================================================================
# ** Window_Base
#------------------------------------------------------------------------------
# ** Adding a new draw function
#==============================================================================
class Window_Base < Window

  def draw_clan_stat(stat, x, y, bar = false)
    clan_stats = ICY::Clan_System::CLAN_STATS
    if clan_stats[stat] != nil
      color1 = Color.new(255, 255, 255, 128)
      color2 = Color.new(128, 128, 128, 128)
      vals = $game_variables[clan_stats[stat][2]]
      maxs = clan_stats[stat][3]
      text = "#{clan_stats[stat][0]} :"
      draw_icon_format_text(x, y, 128, WLH, text, clan_stats[stat][1], true, 0, 18)
      tsize = self.contents.text_size(text)
      off_ox = 96#tsize.width
      draw_text_fraction_style(x + off_ox + 64, y, vals, maxs)
      if bar
        draw_grad_bar(x + 32, y + 20, 128, 12, vals, maxs, color1, color2)
      end
    end
  end
  
end

#==============================================================================
# ** Game_Party
#------------------------------------------------------------------------------
# Adding
#    Party_Name
#    Party_Icon
#==============================================================================
class Game_Party < Game_Unit
  attr_accessor :party_name
  attr_accessor :party_icon
  
  alias icy_gm_party_name_init initialize
   def initialize(*args)
    icy_gm_party_name_init(*args)
    @party_name = ''
    @party_icon = 0
  end  
end

#==============================================================================
# ** Game_Interpreter
#------------------------------------------------------------------------------
# Adding 
#    Name_Party
#==============================================================================
class Game_Interpreter
  
 def name_party
   $game_temp.name_max_char = 8
   $scene = ICY_Scene_Party_Name.new
 end
 
end

#==============================================================================
# ** ICY_Clan_Confirm_Window
#------------------------------------------------------------------------------
#  This window displays a confirmation Window.
#==============================================================================
class ICY_Clan_Confirm_Window < Window_Selectable
  include ICY::Clan_System
  attr_accessor :text_head
  
  def initialize
    @conf_set = CONFIRM_WINDOW_CONFIG
    super(@conf_set["Size"][0], @conf_set["Size"][1], @conf_set["Size"][2], @conf_set["Size"][3])
    self.opacity = @conf_set["Size"][4]
    self.index = 0
    @column_max = @conf_set["Columns"]
    @data = []
    @text_head = nil
    @old_index = nil
    refresh
  end
  
  def update
    super
    if @help_window != nil
      if @old_index != self.index
       @help_window.set_text(@conf_set["HelpText"][self.index])
       @old_index = self.index
     end
    end
  end
  
  #--------------------------------------------------------------------------
  # * Create Window Contents
  #--------------------------------------------------------------------------
  def create_contents
    self.contents.dispose
    maxbitmap = 8192
    dw = [width - 32, maxbitmap].min
    dh = [[height - 32, row_max * @conf_set["Spacing"]].max, maxbitmap].min
    bitmap = Bitmap.new(dw, dh)
    self.contents = bitmap
  end
  
  def off_x
    return (((self.width - 32) - (@conf_set["Spacing"] * @column_max)) / 2)
  end
  
  def off_y
    return ((self.height - 32) - @conf_set["Spacing"]) / 2
  end
  
  def refresh
    @data = @conf_set["Commands"]
    @item_max = @data.size
    create_contents
    @reset = off_x
    @nw_y = off_y
    @nw_x = @reset
    @coun = 0
    create_contents
    self.contents.clear
    for i in @data
      draw_item(i)
    end
    self.contents.draw_text(0,0, self.width, WLH, @text_head, 1)
  end
  
  def draw_item(i)
    draw_command_icon_box(@nw_x, @nw_y, i[0], i[1], @conf_set["RectSize"], 4)
   
    @coun += 1
    @nw_x += @conf_set["Spacing"]
    if @coun == @column_max
      @nw_x = @reset
      @coun = 0
      @nw_y += @conf_set["Spacing"]
    end
  end
  
  #--------------------------------------------------------------------------
  # * Get Top Row
  #--------------------------------------------------------------------------
  def top_row
    return self.oy / @conf_set["Spacing"]#WLH
  end
  #--------------------------------------------------------------------------
  # * Set Top Row
  #     row : row shown on top
  #--------------------------------------------------------------------------
  def top_row=(row)
    row = 0 if row < 0
    row = row_max - 1 if row > row_max - 1
    self.oy = row * @conf_set["Spacing"]#WLH
  end
  #--------------------------------------------------------------------------
  # * Get Number of Rows Displayable on 1 Page
  #--------------------------------------------------------------------------
  def page_row_max
    return (self.height / @conf_set["Spacing"]) 
  end
  #--------------------------------------------------------------------------
  # * Get Number of Items Displayable on 1 Page
  #--------------------------------------------------------------------------
  def page_item_max
    return page_row_max * @column_max 
  end
  #--------------------------------------------------------------------------
  # * Move cursor one page down
  #--------------------------------------------------------------------------
  def cursor_pagedown
    if top_row + page_row_max < row_max
      @index = [@index + page_item_max, @item_max - 1].min
      self.top_row += page_row_max - @conf_set["Spacing"] 
    end
  end
  #--------------------------------------------------------------------------
  # * Move cursor one page up
  #--------------------------------------------------------------------------
  def cursor_pageup
    if top_row > 0
      @index = [@index - page_item_max, 0].max
      self.top_row -= page_row_max - @conf_set["Spacing"] 
    end
  end
  #--------------------------------------------------------------------------
  # * Update cursor
  #--------------------------------------------------------------------------
  def update_cursor
    if @index < 0                   # If the cursor position is less than 0
      self.cursor_rect.empty        # Empty cursor
    else                            # If the cursor position is 0 or more
      row = @index / @column_max    # Get current row
      if row < top_row              # If before the currently displayed
        self.top_row = row          # Scroll up
      end
      if row > bottom_row           # If after the currently displayed
        self.bottom_row = row       # Scroll down
      end
       y_l = @index / @column_max
       y_pos = ((@conf_set["Spacing"] * y_l)- self.oy) + off_y
       x_l = self.index - (@column_max * y_l)
       x_pos = (x_l * @conf_set["Spacing"]) + off_x
       if @conf_set["SelectionSize"] > @conf_set["RectSize"]
         subitive = (@conf_set["SelectionSize"] - @conf_set["RectSize"]) / 2
         self.cursor_rect.set(x_pos.to_i - subitive, y_pos.to_i - subitive, @conf_set["SelectionSize"], @conf_set["SelectionSize"])
       elsif @conf_set["SelectionSize"] < @conf_set["RectSize"]
         additive = (@conf_set["RectSize"] - @conf_set["SelectionSize"]) / 2
         self.cursor_rect.set(x_pos.to_i + additive, y_pos.to_i + additive, @conf_set["SelectionSize"], @conf_set["SelectionSize"])
        else
        self.cursor_rect.set(x_pos.to_i, y_pos.to_i, @conf_set["SelectionSize"], @conf_set["SelectionSize"])
       end
    end
  end
  
end

# WORKPOINT
class ICY_Clan_Stat_Window < Window_Base
  
   include ICY::Clan_System
   
  def initialize(x, y, width, height)
    super(x, y, width, height)
    self.opacity = 255
    refresh
  end

  def refresh
    self.contents.clear
    draw_icon(90, 20, 20)
    self.contents.font.color = Color.new(255, 255, 205, 255)
    self.contents.font.size = 26
    self.contents.draw_text(52, 20, self.width, 26, 'Clan Data')
        draw_parameters(20, 100)
  end

  def draw_parameters(x, y)
    for i in 0...CLAN_STATS.size
      draw_clan_stat(i, x, y + (36 * i), true)
    end
  end
  
end

class ICY_Clan_Info_Window < Window_Base
  
   include ICY::Clan_System
   
  def initialize(x, y, width, height)
    super(x, y, width, height)
    self.opacity = 255
    refresh
  end
  
  def refresh
    draw_icon($game_party.party_icon, 32, 56)
    self.contents.font.color = Color.new(255, 255, 205, 255)
    self.contents.font.size = 18
    self.contents.draw_text(64, 56, (self.width - 76), 24, $game_party.party_name)
  end
  
end
    
#==============================================================================
# ** ICY_Window_Party_Icons
#------------------------------------------------------------------------------
#  This window displays a icon Window.
#==============================================================================
class ICY_Window_Party_Icons < Window_Selectable
  
  include ICY::Clan_System
  
  def initialize(x, y, width, height, columns)
    super(x, y, width, height)
    self.index = 0
    @column_max = columns
    @item_max = Party_Icons.size
    @commands = Party_Icons
    refresh
  end
  
  def current_icon
    return @commands[self.index]
  end
  
  def off_x
    return (((self.width - 32) - (ITEM_SQ_SPACING * @column_max)) / @column_max)
  end
  
  def off_y
    return 0
  end
  #--------------------------------------------------------------------------
  # * Create Window Contents
  #--------------------------------------------------------------------------
  def create_contents
    self.contents.dispose
    maxbitmap = 8192
    dw = [width - 32, maxbitmap].min
    dh = [[height - 32, row_max * ITEM_SQ_SPACING].max, maxbitmap].min
    bitmap = Bitmap.new(dw, dh)
    self.contents = bitmap
  end
  
  def refresh
    create_contents
    @reset = 0 + off_x
    @nw_x = @reset
    @nw_y = off_y 
    @coun = 0
    @full_count = 1
    self.contents.clear 
    for i in Party_Icons 
      draw_item(i)
    end
  end
  
  def draw_item(i)
    outline = Rect.new(0, 0, RECT_SIZE, RECT_SIZE)
    sub_rect = Rect.new((BORDER_SIZE / 2), (BORDER_SIZE / 2), 
       (RECT_SIZE - BORDER_SIZE), (RECT_SIZE - BORDER_SIZE))
 
      srect_color = system_color
      outline_sprite = Bitmap.new(RECT_SIZE, RECT_SIZE)
      outline_sprite.fill_rect(outline, srect_color)
      outline_sprite.clear_rect(sub_rect)
      self.contents.blt(@nw_x, @nw_y, outline_sprite, outline)
      
      draw_icon(i, @nw_x + 4, @nw_y + 4)
      old_font_size = self.contents.font.size
      self.contents.font.size = 14
      self.contents.draw_text(@nw_x, (@nw_y + RECT_SIZE) - 6, 48, WLH, "ID#{i}")
      #self.contents.draw_text(@nw_x - 12, @nw_y, 48, WLH, "##{@full_count}")
      self.contents.font.size = old_font_size
    @full_count += 1   
    @coun += 1
    @nw_x += ITEM_SQ_SPACING
    if @coun == @column_max
      @coun = 0
      @nw_x = @reset
      @nw_y += ITEM_SQ_SPACING   
    end
  end
  
  #--------------------------------------------------------------------------
  # * Get Top Row
  #--------------------------------------------------------------------------
  def top_row
    return self.oy / ITEM_SQ_SPACING#WLH
  end
  #--------------------------------------------------------------------------
  # * Set Top Row
  #     row : row shown on top
  #--------------------------------------------------------------------------
  def top_row=(row)
    row = 0 if row < 0
    row = row_max - 1 if row > row_max - 1
    self.oy = row * ITEM_SQ_SPACING#WLH
  end
  #--------------------------------------------------------------------------
  # * Get Number of Rows Displayable on 1 Page
  #--------------------------------------------------------------------------
  def page_row_max
    return (self.height / ITEM_SQ_SPACING) 
  end
  #--------------------------------------------------------------------------
  # * Get Number of Items Displayable on 1 Page
  #--------------------------------------------------------------------------
  def page_item_max
    return page_row_max * @column_max #+ ITEM_SQ_SPACING
  end
  #--------------------------------------------------------------------------
  # * Move cursor one page down
  #--------------------------------------------------------------------------
  def cursor_pagedown
    if top_row + page_row_max < row_max
      @index = [@index + page_item_max, @item_max - 1].min
      self.top_row += page_row_max - ITEM_SQ_SPACING 
    end
  end
  #--------------------------------------------------------------------------
  # * Move cursor one page up
  #--------------------------------------------------------------------------
  def cursor_pageup
    if top_row > 0
      @index = [@index - page_item_max, 0].max
      self.top_row -= page_row_max - ITEM_SQ_SPACING 
    end
  end
  
  #--------------------------------------------------------------------------
  # * Update cursor
  #--------------------------------------------------------------------------
  def update_cursor
    if @index < 0                   # If the cursor position is less than 0
      self.cursor_rect.empty        # Empty cursor
    else                            # If the cursor position is 0 or more
      row = @index / @column_max    # Get current row
      if row < top_row              # If before the currently displayed
        self.top_row = row          # Scroll up
      end
      if row > bottom_row           # If after the currently displayed
        self.bottom_row = row       # Scroll down
      end
       y_l = @index / @column_max
       y_pos = ((ITEM_SQ_SPACING * y_l)- self.oy) + off_y
       x_l = self.index - (@column_max * y_l)
       x_pos = (x_l * ITEM_SQ_SPACING) + off_x
       if SELECTION_SIZE > RECT_SIZE
         subitive = (SELECTION_SIZE - RECT_SIZE) / 2
         self.cursor_rect.set(x_pos.to_i - subitive, y_pos.to_i - subitive, SELECTION_SIZE, SELECTION_SIZE)
       elsif SELECTION_SIZE < RECT_SIZE
         additive = (RECT_SIZE - SELECTION_SIZE) / 2
         self.cursor_rect.set(x_pos.to_i + additive, y_pos.to_i + additive, SELECTION_SIZE, SELECTION_SIZE)
        else
        self.cursor_rect.set(x_pos.to_i, y_pos.to_i, SELECTION_SIZE, SELECTION_SIZE)
       end
    end
  end
  
end

#==============================================================================
# ** ICY_Clan_Header_Window
#------------------------------------------------------------------------------
#  This window displays a header.
#==============================================================================
class ICY_Clan_Header_Window < Window_Base
  attr_accessor :font_size
  #--------------------------------------------------------------------------
  # * Object Initialization
  #     x      : window x-coordinate
  #     y      : window y-coordinate
  #     width  : window width
  #     height : window height
  #-------------------------------------------------------------------------- 
  def initialize(x, y, width, height)
    super(x, y, width, height)
    @font_size = 26
  end
  #--------------------------------------------------------------------------
  # ** Reset Font Size
  #   Resets the font size to 26
  #--------------------------------------------------------------------------
  def reset_font_size
    @font_size = 26
  end
  #--------------------------------------------------------------------------
  # ** Set Header
  #     text   : text
  #     icon   : icon_index
  #     align  : align
  #-------------------------------------------------------------------------- 
  def set_header(text = "" , icon = nil, align = 0)
    icon_offset = 32
    if icon == nil
      x = 0
    else
      x = icon_offset
      draw_icon(icon, 0, 0)
    end
     y = 0
     old_font_size = self.contents.font.size
     self.contents.font.size = @font_size
     self.contents.font.color = system_color
     self.contents.draw_text(x, y, (self.width - x) - 48, WLH, text, align)
     self.contents.font.color = normal_color
     self.contents.font.size = old_font_size
  end
  
end

#ICY_W1.5
#==============================================================================
# ** ICY_Window_NameInput
#------------------------------------------------------------------------------
#  This window is used to select text characters on the name input screen.
#==============================================================================

class ICY_Window_NameInput < Window_Base
  #--------------------------------------------------------------------------
  # * Text Character Table
  #--------------------------------------------------------------------------
  ENGLISH = [ 'A','B','C','D','E',  'F','G','H','I','J',
              'K','L','M','N','O',  'P','Q','R','S','T',
              'U','V','W','X','Y',  'Z',' ',' ',' ',' ',
              'a','b','c','d','e',  'f','g','h','i','j',
              'k','l','m','n','o',  'p','q','r','s','t',
              'u','v','w','x','y',  'z',' ',' ',' ',' ',
              ' ',' ',' ',' ',' ',  ' ',' ',' ',' ',' ',
              ' ',' ',' ',' ',' ',  ' ',' ',' ',' ',' ',
              ' ',' ',' ',' ',' ',  ' ',' ',' ','Mode','OK']
  NUMBER =  [ '1','2','3','4','5',  '~','`',' ',' ',' ',
              '6','7','8','9','0',  ' ',' ',' ',' ',' ',
              ' ',' ',' ',' ',' ',  ' ',' ',' ',' ',' ',
              '!','@','#','$','%',  ' ',' ',' ',' ',' ',
              '^','&','*','(',')',  ' ',' ',' ',' ',' ',
              '_','-','=','+','?',  ' ',' ',' ',' ',' ',
              ' ',' ',' ',' ',' ',  ' ',' ',' ',' ',' ',
              ' ',' ',' ',' ',' ',  ' ',' ',' ',' ',' ',
              ' ',' ',' ',' ',' ',  ' ',' ',' ','Mode','OK']
  TABLE = [ENGLISH, NUMBER]
  #--------------------------------------------------------------------------
  # * Object Initialization
  #     mode : Defeault input mode (always 0 in English version)
  #--------------------------------------------------------------------------
  def initialize(mode = 0)
    super(0, 120, 386, 248)
    @mode = mode
    @index = 0
    refresh
    update_cursor
  end
  #--------------------------------------------------------------------------
  # * Text Character Acquisition
  #--------------------------------------------------------------------------
  def character
    if @index < 88
      return TABLE[@mode][@index]
    else
      return ""
    end
  end
  #--------------------------------------------------------------------------
  # * Determine Cursor Position: Mode Switch
  #--------------------------------------------------------------------------
  def is_mode_change
    return (@index == 88)
  end
  #--------------------------------------------------------------------------
  # * Determine Cursor Location: Confirmation
  #--------------------------------------------------------------------------
  def is_decision
    return (@index == 89)
  end
  #--------------------------------------------------------------------------
  # * Get rectangle for displaying items
  #     index : item number
  #--------------------------------------------------------------------------
  def item_rect(index)
    rect = Rect.new(0, 0, 0, 0)
    rect.x = index % 10 * 32 + index % 10 / 5 * 16
    rect.y = index / 10 * WLH
    rect.width = 32
    rect.height = WLH
    return rect
  end
  #--------------------------------------------------------------------------
  # * Refresh
  #--------------------------------------------------------------------------
  def refresh
    self.contents.clear
    for i in 0..89
      rect = item_rect(i)
      rect.x += 2
      rect.width -= 4
      self.contents.draw_text(rect, TABLE[@mode][i], 1)
    end
  end
  #--------------------------------------------------------------------------
  # * Update cursor
  #--------------------------------------------------------------------------
  def update_cursor
    self.cursor_rect = item_rect(@index)
  end
  #--------------------------------------------------------------------------
  # * Move cursor down
  #     wrap : Wraparound allowed
  #--------------------------------------------------------------------------
  def cursor_down(wrap)
    if @index < 80
      @index += 10
    elsif wrap
      @index -= 80
    end
  end
  #--------------------------------------------------------------------------
  # * Move cursor up
  #     wrap : Wraparound allowed
  #--------------------------------------------------------------------------
  def cursor_up(wrap)
    if @index >= 10
      @index -= 10
    elsif wrap
      @index += 80
    end
  end
  #--------------------------------------------------------------------------
  # * Move cursor right
  #     wrap : Wraparound allowed
  #--------------------------------------------------------------------------
  def cursor_right(wrap)
    if @index % 10 < 9
      @index += 1
    elsif wrap
      @index -= 9
    end
  end
  #--------------------------------------------------------------------------
  # * Move cursor left
  #     wrap : Wraparound allowed
  #--------------------------------------------------------------------------
  def cursor_left(wrap)
    if @index % 10 > 0
      @index -= 1
    elsif wrap
      @index += 9
    end
  end
  #--------------------------------------------------------------------------
  # * Move Cursor to [OK]
  #--------------------------------------------------------------------------
  def cursor_to_decision
    @index = 89
  end
  #--------------------------------------------------------------------------
  # * Move to Next Page
  #--------------------------------------------------------------------------
  def cursor_pagedown
    @mode = (@mode + 1) % TABLE.size
    refresh
  end
  #--------------------------------------------------------------------------
  # * Move to Previous Page
  #--------------------------------------------------------------------------
  def cursor_pageup
    @mode = (@mode + TABLE.size - 1) % TABLE.size
    refresh
  end
  #--------------------------------------------------------------------------
  # * Frame Update
  #--------------------------------------------------------------------------
  def update
    super
    last_mode = @mode
    last_index = @index
    if Input.repeat?(Input::DOWN)
      cursor_down(Input.trigger?(Input::DOWN))
    end
    if Input.repeat?(Input::UP)
      cursor_up(Input.trigger?(Input::UP))
    end
    if Input.repeat?(Input::RIGHT)
      cursor_right(Input.trigger?(Input::RIGHT))
    end
    if Input.repeat?(Input::LEFT)
      cursor_left(Input.trigger?(Input::LEFT))
    end
    if Input.trigger?(Input::A)
      cursor_to_decision
    end
    if Input.trigger?(Input::R)
      cursor_pagedown
    end
    if Input.trigger?(Input::L)
      cursor_pageup
    end
    if Input.trigger?(Input::C) and is_mode_change
      cursor_pagedown
    end
    if @index != last_index or @mode != last_mode
      Sound.play_cursor
    end
    update_cursor
  end
end

#==============================================================================
# ** ICY_Window_NameEdit
#------------------------------------------------------------------------------
#  This window is used to edit an actor's name on the name input screen.
#==============================================================================

class ICY_Window_PartyNameEdit < Window_Base
  #--------------------------------------------------------------------------
  # * Public Instance Variables
  #--------------------------------------------------------------------------
  attr_reader   :name                     # name
  attr_reader   :index                    # cursor position
  attr_reader   :max_char                 # maximum number of characters
  #--------------------------------------------------------------------------
  # * Object Initialization
  #     actor    : actor
  #     max_char : maximum number of characters
  #--------------------------------------------------------------------------
  def initialize(name, max_char)
    super(0, 56, 386, 64)
    @name = name
    @max_char = max_char
    name_array = @name.split(//)[0...@max_char]   # Fit within max length
    @name = ""
    for i in 0...name_array.size
      @name += name_array[i]
    end
    @default_name = @name
    @index = name_array.size
    self.active = false
    refresh
    update_cursor
  end
  #--------------------------------------------------------------------------
  # * Return to Default Name
  #--------------------------------------------------------------------------
  def restore_default
    @name = @default_name
    @index = @name.split(//).size
    refresh
    update_cursor
  end
  #--------------------------------------------------------------------------
  # * Add Text Character
  #     character : text character to be added
  #--------------------------------------------------------------------------
  def add(character)
    if @index < @max_char and character != ""
      @name += character
      @index += 1
      refresh
      update_cursor
    end
  end
  #--------------------------------------------------------------------------
  # * Delete Text Character
  #--------------------------------------------------------------------------
  def back
    if @index > 0
      name_array = @name.split(//)          # Delete one character
      @name = ""
      for i in 0...name_array.size-1
        @name += name_array[i]
      end
      @index -= 1
      refresh
      update_cursor
    end
  end
  #--------------------------------------------------------------------------
  # * Get rectangle for displaying items
  #     index : item number
  #--------------------------------------------------------------------------
  def item_rect(index)
    rect = Rect.new(0, 0, 0, 0)
    rect.x = 128 - (@max_char + 1) * 12 + index * 24
    rect.y = 0
    rect.width = 24
    rect.height = WLH
    return rect
  end
  #--------------------------------------------------------------------------
  # * Refresh
  #--------------------------------------------------------------------------
  def refresh
    self.contents.clear
    name_array = @name.split(//)
    for i in 0...@max_char
      c = name_array[i]
      c = '_' if c == nil
      self.contents.draw_text(item_rect(i), c, 1)
    end
  end
  #--------------------------------------------------------------------------
  # * Update cursor
  #--------------------------------------------------------------------------
  def update_cursor
    self.cursor_rect = item_rect(@index)
  end
  #--------------------------------------------------------------------------
  # * Frame Update
  #--------------------------------------------------------------------------
  def update
    super
    update_cursor
  end
end

#ICY_W1.3
#==============================================================================
# ** ICY_Clan_Window_Help
#------------------------------------------------------------------------------
#  This window shows explanations.
#==============================================================================

class ICY_Clan_Window_Help < Window_Base
  #--------------------------------------------------------------------------
  # * Object Initialization
  #--------------------------------------------------------------------------
  def initialize(x, y, width, height, fontsize = 18)
    super(x, y, width, height)
    @fontsize = fontsize
    create_contents
  end
  #--------------------------------------------------------------------------
  # * Create Window Contents
  #--------------------------------------------------------------------------
  def create_contents
    self.contents.dispose
    self.contents = Bitmap.new(width, height)
  end
  
  #--------------------------------------------------------------------------
  # * Set Text
  #  text  : character string displayed in window
  #  align : alignment (0..flush left, 1..center, 2..flush right)
  #--------------------------------------------------------------------------
  def set_text(text, align = 0)
    if text != @text or align != @align
      self.contents.clear
      self.contents.font.size = @fontsize
      self.contents.font.color = normal_color
      self.contents.draw_text(4, 0, self.width - 40, WLH, text, align)
      @text = text
      @align = align
    end
  end
end
#--
# 1-ICY_SN_Scene
#
#==============================================================================
# ** Scene_Name
#------------------------------------------------------------------------------
#  This class performs name input screen processing.
#==============================================================================

class ICY_Scene_Party_Name < Scene_Base
  include ICY::Clan_System
  #--------------------------------------------------------------------------
  # * Start processing
  #--------------------------------------------------------------------------
  def start
    super
    create_menu_background
    @sliding = false
    @slide_name = false
    @icon_slide = false
    @icon_edit = false
    @name_edit = true
	  @windows = {}
    @windows["Help"] = ICY_Clan_Window_Help.new(0, 0, 544, 56)
    @windows["Edit"] = ICY_Window_PartyNameEdit.new("Team", $game_temp.name_max_char)
    @windows["Input"] = ICY_Window_NameInput.new
    @windows["Header"] = ICY_Clan_Header_Window.new(0, 0, 544, 56)
    @windows["Header"].set_header("Party Setup", 2)
    @windows["Icon"] = ICY_Window_Party_Icons.new(386, 120, 544, 248, ITEM_COLUMN_MAX)
    @windows["Icon"].update
    @windows["PartyName"] = ICY_Clan_Header_Window.new(0, 0, 544, 56)
    @windows["Confirm"] = ICY_Clan_Confirm_Window.new
    @windows["Confirm"].help_window = @windows["Help"]
    @windows["Confirm"].active = false
    @windows["Confirm"].visible = false
    edit_window_sizes
  end
  
  def edit_window_sizes
    @windows["Help"].x = SETUP_WINDOW_SIZES["Help"][0]
    @windows["Help"].y = SETUP_WINDOW_SIZES["Help"][1]
    @windows["Help"].width = SETUP_WINDOW_SIZES["Help"][2]
    @windows["Help"].height = SETUP_WINDOW_SIZES["Help"][3]
    @windows["Help"].opacity = SETUP_WINDOW_SIZES["Help"][4]
    
    @windows["Header"].x = SETUP_WINDOW_SIZES["Header"][0]
    @windows["Header"].y = SETUP_WINDOW_SIZES["Header"][1]
    @windows["Header"].width = SETUP_WINDOW_SIZES["Header"][2]
    @windows["Header"].height = SETUP_WINDOW_SIZES["Header"][3]
    @windows["Header"].opacity = SETUP_WINDOW_SIZES["Header"][4]
    @windows["Header"].set_header("Party Setup", 2)
    
    @windows["Icon"].x = SETUP_WINDOW_SIZES["Icon"][0]
    @windows["Icon"].y = SETUP_WINDOW_SIZES["Icon"][1]
    @windows["Icon"].width = SETUP_WINDOW_SIZES["Icon"][2]
    @windows["Icon"].height = SETUP_WINDOW_SIZES["Icon"][3]
    @windows["Icon"].opacity = SETUP_WINDOW_SIZES["Icon"][4]
    @windows["Icon"].update
    
    @windows["Edit"].x = SETUP_WINDOW_SIZES["Name"][0]
    @windows["Edit"].y = SETUP_WINDOW_SIZES["Name"][1]
    @windows["Edit"].width = SETUP_WINDOW_SIZES["Name"][2]
    @windows["Edit"].height = SETUP_WINDOW_SIZES["Name"][3]
    @windows["Edit"].opacity = SETUP_WINDOW_SIZES["Name"][4]
    @windows["Edit"].update
    
    @windows["Input"].x = SETUP_WINDOW_SIZES["Input"][0]
    @windows["Input"].y = SETUP_WINDOW_SIZES["Input"][1]
    @windows["Input"].width = SETUP_WINDOW_SIZES["Input"][2]
    @windows["Input"].height = SETUP_WINDOW_SIZES["Input"][3]
    @windows["Input"].opacity = SETUP_WINDOW_SIZES["Input"][4]
    @windows["Input"].update
    
    set_party_name_window
  end
  
  def set_party_name_window
    @windows["PartyName"].x = SETUP_WINDOW_SIZES["Party"][0]
    @windows["PartyName"].y = SETUP_WINDOW_SIZES["Party"][1]
    @windows["PartyName"].width = SETUP_WINDOW_SIZES["Party"][2]
    @windows["PartyName"].height = SETUP_WINDOW_SIZES["Party"][3]
    @windows["PartyName"].opacity = SETUP_WINDOW_SIZES["Party"][4]
  end
  #--------------------------------------------------------------------------
  # * Termination Processing
  #--------------------------------------------------------------------------
  def terminate
    super
   for i in @windows.keys
    @windows[i].dispose if @windows[i] != nil
    end
  end
  
  #--------------------------------------------------------------------------
  # * Return to Original Screen
  #--------------------------------------------------------------------------
  def return_scene
   $scene = Scene_Map.new
  end
 
 def slide_over_window(name_slide = true, icon_slide = false)
   @sliding = true
   @slide_name = name_slide
   @icon_slide = icon_slide
 end
 
 def slide_name_win
  if @windows["Icon"].x != SETUP_WINDOW_SIZES["Input"][0]
     @windows["Icon"].x -= 8
    end
     if @windows["Icon"].x < SETUP_WINDOW_SIZES["Input"][0]
       @windows["Icon"].x = SETUP_WINDOW_SIZES["Input"][0]
       @sliding = false
       @icon_edit = true
       @name_edit = false
       update
    end
 end
 
 def slide_icon_wind
  if @windows["Icon"].x != SETUP_WINDOW_SIZES["Icon"][0]
     @windows["Icon"].x += 8
  end
  if @windows["Icon"].x > SETUP_WINDOW_SIZES["Icon"][0]
       @windows["Icon"].x = SETUP_WINDOW_SIZES["Icon"][0]
       @sliding = false
       @name_edit = true
       @icon_edit = false
       update
     end
  end
  
 def normal_update
    @windows["Edit"].update
    @windows["Input"].update
    if Input.repeat?(Input::B)
      if @windows["Edit"].index > 0             # Not at the left edge
        Sound.play_cancel
        @windows["Edit"].back
      end
    elsif Input.trigger?(Input::C)
      if @windows["Input"].is_decision          # If cursor is positioned on [OK]
        if @windows["Edit"].name == ""          # If name is empty
          @windows["Edit"].restore_default      # Return to default name
          if @windows["Edit"].name == ""
            Sound.play_buzzer
          else
            Sound.play_decision
          end
        else
          Sound.play_decision
          $game_party.party_name = @windows["Edit"].name   # Change party name
          @windows["PartyName"].font_size = 20
          @windows["PartyName"].set_header($game_party.party_name)
          @windows["PartyName"].reset_font_size
          slide_over_window
        end
      elsif @windows["Input"].character != ""   # If text characters are not empty
        if @windows["Edit"].index == @windows["Edit"].max_char    # at the right edge
          Sound.play_buzzer
        else
          Sound.play_decision
          @windows["Edit"].add(@windows["Input"].character)       # Add text character
        end
      end
    end
  end
  
  def set_party_icon_name
    $game_party.party_icon = @windows["Icon"].current_icon
    @windows["PartyName"].contents.clear
    @windows["PartyName"].font_size = 20
    @windows["PartyName"].set_header($game_party.party_name, $game_party.party_icon)
    @windows["PartyName"].reset_font_size
  end
  
  def confirm_update
    @windows["Confirm"].update
    if Input.trigger?(Input::C)
      case @windows["Confirm"].index
      when 0
        Sound.play_decision
        set_party_icon_name
        return_scene
      when 1
        Sound.play_cancel
        confirm_window_vis
        set_party_name_window
      end
    elsif Input.trigger?(Input::B)
        Sound.play_cancel
        confirm_window_vis
        set_party_name_window
    end
  end
  
  def confirm_window_vis(set = false)
    @windows["Confirm"].visible = set
    @windows["Confirm"].active = set
  end
  
  def icon_window_update
    @windows["Icon"].update
    if Input.trigger?(Input::B)
      Sound.play_cancel
     @windows["PartyName"].contents.clear
     slide_over_window(false, true)
   elsif Input.trigger?(Input::C)
     Sound.play_decision
     set_party_icon_name
     confirm_window_vis(true)
     @windows["Confirm"].text_head = NMIC_ACCEPT_TEXT
     @windows["Confirm"].refresh
     @windows["PartyName"].x = @windows["Confirm"].x
     @windows["PartyName"].y = @windows["Confirm"].y - @windows["PartyName"].height
    end
  end
  
  #--------------------------------------------------------------------------
  # * Frame Update
  #--------------------------------------------------------------------------
  def update
    super
    update_menu_background
    if @sliding
      slide_name_win if @slide_name
      slide_icon_wind if @icon_slide
      return
    end
    if @windows["Confirm"].active
      confirm_update
    end
    
    if @icon_edit and not @windows["Confirm"].active
      icon_window_update
    end
    
    if @name_edit
      normal_update
    end
    
  end #update
  
end  
