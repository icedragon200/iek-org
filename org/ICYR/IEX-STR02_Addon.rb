# IEX - STR02_Addon
#~ #==============================================================================#
#~ # ** IEX(Icy Engine Xelion) - STR02 Event Fading Addon
#~ #------------------------------------------------------------------------------#
#~ # ** Created by : IceDragon
#~ # ** Script-Status : Addon
#~ # ** Date Created : 10/19/2010
#~ # ** Date Modified : 10/20/2010
#~ # ** Version : ??
#~ #------------------------------------------------------------------------------#
#~ # ** INTRODUCTION
#~ #   *Reserved for when I actually get around to typing this*
#~ #
#~ #
#~ #------------------------------------------------------------------------------#
#~ # ** FEATURES
#~ #
#~ #
#~ #------------------------------------------------------------------------------#
#~ # ** CHANGE LOG
#~ #
#~ #------------------------------------------------------------------------------#
#~ # ** KNOWN ISSUES
#~ #  Non at the moment
#~ #
#~ #------------------------------------------------------------------------------#
#~ $imported = {} if $imported == nil
#~ if $imported["IEX_Environment"]
#~   $iex_star_antilag_fade_on = IEX::ENVIRONMENT::STAR_ANTI_LAG_FADER 
#~ else
#~   $iex_star_antilag_fade_on = true
#~ end

#~ if $iex_star_antilag_fade_on
#~   $imported["IEX_Star_Anti_Lag_Fader"] = true
#~   
#~ module IEX
#~   module Star_Anti_Lag_Addon
#~     FADE_TIME = 30 #60
#~   end
#~ end

#~ class Game_Event < Game_Character
#~   
#~   alias iex_str02_addon_fader_initialize initialize unless $@
#~   def initialize(*args)
#~     iex_str02_addon_fader_initialize(*args)
#~     @str_fading = true
#~     
#~     if $imported["IEX_Event_Fader"] == true
#~       @str_fading = false if @fader_effect
#~     end    
#~   end
#~   
#~   alias iex_str02_addon_fader_update update unless $@
#~   def update(*args)
#~     iex_str02_addon_fader_update(*args)

#~     update_anti_lag_fade if @str_fading
#~   end
#~   
#~   def update_anti_lag_fade
#~     unless @screenin_str02
#~       return if @opacity == 0
#~       @opacity -= 255 / IEX::Star_Anti_Lag_Addon::FADE_TIME unless @opacity <= 0
#~       if @opacity < 0
#~         @opacity = 0
#~       end  
#~     else
#~       return if @opacity == 255
#~       @opacity += 255 / IEX::Star_Anti_Lag_Addon::FADE_TIME unless @opacity == 255
#~       if @opacity > 255
#~         @opacity = 255
#~       end  
#~     end
#~   end
#~   
#~ end

#~ end
