# ICY_Stepping_Override
#==============================================================================#
#------------------------------------------------------------------------------#
#= ** Icy Stepping Override
#= ** Created by    : IceDragon
#= ** Script-Status : Snippet
#= ** Date Created  : 7/08/2010
#= ** Date Modified : 7/10/2010
#= ** Version       : 0.5
#------------------------------------------------------------------------------#
#==============================================================================#
# This disregards the normal VX stepping pattern
# and use the XP method for select events.

# If your wondering what the difference is
# arrows represent frame pos and direction of next frame
# VX pattern 1 |<|
#            2 <||
#            3 |>|
#            4 ||> 

# XP pattern 1 |>|
#            2 ||>
#            3 >||

# to use this place [ISTOV] in an event name, preferably at the end..
# eg. Tuna[ISTOV]
# 
# ques. What does ISTOV mean?
# ans. Icy Stepping Override
$imported = {} if $imported == nil
$imported["ICY_Stepping_Override"] = true

class Game_Event < Game_Character

  def icy_override_step_anim?
    name = @event.name
    return name.scan(/\[ISTOV\]/i).size > 0
  end
  
  #--------------------------------------------------------------------------
  # * Update Animation Count
  #--------------------------------------------------------------------------
  def update_animation
    speed = @move_speed + (dash? ? 1 : 0)
    if @anime_count > 18 - speed * 2
      if not @step_anime and @stop_count > 0
        @pattern = @original_pattern
      else
        if !icy_override_step_anim?
          @pattern = (@pattern + 1) % 4 
         else
          @pattern = (@pattern + 1) % 3 #Overide 4!
        end
      @anime_count = 0
    end
  end
end    
end


