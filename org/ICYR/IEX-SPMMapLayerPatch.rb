# IEX - SPM Map Layer Patch
#~ #==============================================================================
#~ # ** Spriteset_Map
#~ #------------------------------------------------------------------------------
#~ #  This class brings together map screen sprites, tilemaps, etc. It's used
#~ # within the Scene_Map class.
#~ #==============================================================================

#~ class Spriteset_Map
#~   #--------------------------------------------------------------------------
#~   # * Update Viewport
#~   #--------------------------------------------------------------------------
#~   def update_viewports
#~     @viewport1.ox = $game_map.screen.shake
#~     @viewport2.color = $game_map.screen.flash_color
#~     @viewport3.color.set(0, 0, 0, 255 - $game_map.screen.brightness)
#~     @viewport1.update
#~     @viewport2.update
#~     @viewport3.update
#~   end
#~   
#~ end
