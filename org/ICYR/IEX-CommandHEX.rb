# IEX - Command HEX
#~ # Date Created 01/26/2011
#~ class Command_HEX < Sprite_Battler
#~   
#~   attr_accessor :hang_frame
#~   attr_accessor :hang_frames
#~   attr_accessor :frame_hang
#~   attr_accessor :win_visible
#~   
#~   HEX_BUTTON_FRAMES = 5
#~   HEX_BUTTON_LAYOUT = {
#~   # Index => Animation_ID
#~     0 => 189,
#~     1 => 190,
#~     2 => 191,
#~     3 => 192,
#~     4 => 193,
#~     5 => 194,
#~     }
#~     
#~   def initialize(*args)
#~     super(*args)
#~     @index = 0
#~     @frame_hang = false
#~     @win_visible = false
#~   end
#~   
#~   def index ; return @index end
#~     
#~   def index=(val)
#~     @index = val
#~   end
#~   
#~   def accept_index(index)
#~     anim = $data_animations[HEX_BUTTON_LAYOUT[index%6]] 
#~     start_animation(anim)
#~   end
#~   
#~   def start_ring
#~     start_state_animation($data_animations[187])
#~   end
#~   
#~   def open_ring
#~     anim = $data_animations[186] 
#~     start_animation(anim)
#~   end  
#~   
#~   def close_ring
#~     anim = $data_animations[188] 
#~     start_animation(anim)
#~   end 
#~   
#~   def setup_new_effect ; end
#~   def update_effect ; end
#~   
#~   def reset_frames
#~     @state_frame_index = -1
#~   end  
#~   
#~   #--------------------------------------------------------------------------
#~   # new method: update_state_animation
#~   #--------------------------------------------------------------------------
#~   def update_state_animation
#~     return if @state_animation == nil
#~     update_state_animation_position
#~     @state_animation_duration -= 1
#~     unless @frame_hang
#~       unless @state_animation_duration % RATE == 0 
#~         if @state_animation_duration >= 0
#~           @state_frame_index = @state_animation.frame_max 
#~           @state_frame_index -= (@state_animation_duration + RATE - 1) / RATE
#~           for timing in @state_animation.timings
#~             next unless timing.frame == @state_frame_index
#~             state_animation_process_timing(timing)
#~           end
#~         else
#~           @state_frame_index = 0 
#~           @state_animation_duration = @state_animation.frame_max * RATE
#~         end
#~       end
#~     end  
#~     if @hang_frames != nil 
#~       @state_frame_index = @hang_frames[@state_frame_index%5] 
#~       @frame_hang = false
#~       if @hang_frame != nil
#~         if @hang_frames[@state_frame_index%5] == @hang_frame
#~           @state_frame_index = @hang_frame 
#~           @frame_hang = true 
#~         end  
#~       end
#~     elsif @hang_frame != nil
#~       @state_frame_index = @hang_frame
#~     end  
#~     return if @state_frame_index == nil
#~     state_animation_set_sprites(@state_animation.frames[@state_frame_index]) 
#~   end
#~   
#~   def update_state_visible
#~     if @state_animation_sprites != nil
#~       for spr in @state_animation_sprites
#~         spr.visible = @win_visible
#~       end 
#~     end
#~   end
#~   
#~   def ianimation_complete?
#~     return @animation.nil?
#~   end
#~   
#~ end

#~ class Window_ActorCommand < Window_Command
#~   
#~   alias iex_siren_wac_initialize initialize unless $@
#~   def initialize(*args)
#~     @hex_viewport = Viewport.new(0, 0, Graphics.width, Graphics.width)
#~     @hex_viewport.z = 320
#~     @command_hex = Command_HEX.new(@hex_viewport)
#~     iex_siren_wac_initialize(*args)
#~     self.opacity = 0
#~     @command_hex.start_ring
#~   end
#~   
#~   def update_command_ring
#~     @command_hex.win_visible = (self.active and self.visible)
#~     if @command_hex != nil 
#~       @command_hex.hang_frame = (@index * 5)+19 # +4
#~       @command_hex.hang_frame %= 30
#~       @command_hex.hang_frames = []
#~       for i in 0...5 
#~         @command_hex.hang_frames[i] = (@index*5)+15
#~         @command_hex.hang_frames[i]+= i
#~         @command_hex.hang_frames[i]%= 30
#~       end  
#~       @command_hex.update
#~       @command_hex.update_state_animation
#~       @command_hex.update_state_visible
#~     end
#~   end
#~   
#~   def update
#~     #update_command_ring
#~     @old_index = self.index
#~     super  
#~     if @old_index != self.index and self.active
#~       @command_hex.reset_frames
#~       @command_hex.frame_hang = false
#~     end 
#~     if Input.trigger?(Input::C) and self.active
#~       @command_hex.accept_index(@index)
#~     end  
#~   end
#~   
#~   alias iex_commandhex_refresh refresh unless $@
#~   def refresh
#~     iex_commandhex_refresh
#~     @command_hex.battler = @actor
#~     @command_hex.update
#~     @command_hex.open_ring
#~   end

#~ end

#~ class Scene_Battle
#~   
#~   alias iex_hex_update_basic update_basic unless $@
#~   def update_basic(*args)
#~     if @actor_command_window != nil
#~       @actor_command_window.update_command_ring
#~     end  
#~     iex_hex_update_basic(*args)
#~   end  
#~   
#~ end
