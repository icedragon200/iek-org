# >IEX - Chain Cast System
#~ #==============================================================================#
#~ # ** IEX(Icy Engine Xelion) - Chain Cast System
#~ #------------------------------------------------------------------------------#
#~ # ** Created by    : IceDragon (http://www.rpgmakervx.net/)
#~ # ** Script-Status : Addon (Battle, Skill)
#~ # ** Script Type   : Skill Chaining
#~ # ** Date Created  : 01/03/2011
#~ # ** Date Modified : 01/03/2011
#~ # ** Difficulty    : Easy
#~ # ** Version       : 1.0
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** INTRODUCTION
#~ #------------------------------------------------------------------------------#
#~ #
#~ #
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** FEATURES
#~ #------------------------------------------------------------------------------#
#~ # V1.0 - Notetags - Skills
#~ #------------------------------------------------------------------------------#
#~ # 
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** CHANGES 
#~ #------------------------------------------------------------------------------#
#~ # Classes
#~ #   RPG::Skill
#~ #     new-method :iex_chainCast_cache
#~ #     new-method :cant_chain?
#~ #     new-method :chain_size
#~ #     new-method :chain_skill?
#~ #     new-method :chain_skills
#~ #   Game_BattleAction
#~ #     new-method :chain_size 
#~ #   Game_Battler
#~ #     alias      :initialize
#~ #     new-method :chain_castsize
#~ #     new-method :can_chain?
#~ #     new-method :add_chaincast
#~ #     new-method :remove_last_chaincast
#~ #   Scene_Battle
#~ #     alias      :execute_action_skill
#~ #     new-method :execute_chain_skill
#~ #
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** COMPATABLITIES
#~ #------------------------------------------------------------------------------#
#~ #
#~ # BEM, Yggdrasil, Probably Takentai not sure about GTBS
#~ #
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** CHANGE LOG
#~ #------------------------------------------------------------------------------#
#~ # 
#~ #  01/03/2011 - V1.0 Started Script
#~ #
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** KNOWN ISSUES
#~ #------------------------------------------------------------------------------#
#~ #  Non at the moment. 
#~ #
#~ #------------------------------------------------------------------------------#
#~ class RPG::Skill
#~   
#~   def iex_chainCast_cache
#~     @iex_chainCast_cache_complete = false
#~     @chain_size = 1
#~     @chain_skills = []
#~     @cant_chain = false
#~     self.note.split(/[\r\n]+/).each { |line|
#~       case line.to_s.upcase
#~       when /<(?:CHAIN_SKILL|CHAIN SKILL|CHAINSKILL)s?:?[ ]*(\d+(?:\s*,\s*\d+)*)>/i
#~         $1.scan(/\d+/).each { |num| @chain_skills.push(num) if num > 0 }          
#~       when /<(?:CHAIN_SIZE|chain size|chainsize):[ ]*(\d+)>/i
#~         @chain_size = $1.to_i
#~       when "CANT_CHAIN", "CANTCHAIN", "CANT CHAIN"  
#~         @cant_chain  = true
#~       end  
#~     }
#~     @iex_chainCast_cache_complete = true
#~   end
#~   
#~   def cant_chain?
#~     iex_chainCast_cache unless @iex_chainCast_cache_complete
#~     return @cant_chain
#~   end
#~   
#~   def chain_size
#~     iex_chainCast_cache unless @iex_chainCast_cache_complete
#~     return @chain_size
#~   end
#~   
#~   def chain_skill?
#~     iex_chainCast_cache unless @iex_chainCast_cache_complete
#~     return @chain_skills.size > 0
#~   end
#~   
#~   def chain_skills
#~     iex_chainCast_cache unless @iex_chainCast_cache_complete
#~     return @chain_skills
#~   end
#~   
#~ end

#~ class Game_BattleAction
#~   
#~   def chain_size 
#~     if @skill_id > 0 ; return $data_skills[@skill_id].chain_size end ; return 0
#~   end
#~   
#~ end

#~ class Game_Battler
#~   
#~   attr_accessor :chain_castset
#~   
#~   alias iex_chainCast_initialize initialize unless $@
#~   def initialize(*args)
#~     @chain_castset = []
#~     @maxchain = 2
#~     iex_chainCast_initialize(*args)
#~   end
#~   
#~   def chain_castsize
#~     result = 0
#~     for chn in @chain_castset
#~       result += chn.chain_size 
#~     end  
#~     return result
#~   end
#~   
#~   def can_chain?(skill)
#~     return false if skill.cant_chain?
#~     return false if (chain_castsize + skill.chain_size) > @maxchain
#~     return true
#~   end
#~   
#~   def add_chaincast(skill, target)
#~     skill = $data_skills[skill] unless skill.is_a?(RPG::Skill)
#~     chn = Game_BattleAction.new(self)
#~     chn.set_skill(skill.id)
#~     @chain_castset.push(chn)
#~   end
#~   
#~   def remove_last_chaincast
#~     if @chain_castset.size > 0
#~       @chain_castset.delete_at(@chain_castset.size)
#~     end  
#~   end
#~   
#~ end

#~ class Window_CastChain < Window_Selectable
#~   
#~   def initialize(x, y, width, height)
#~     super(x, y, width, height)
#~     @index = -1
#~     refresh
#~   end
#~   
#~   def refresh
#~     self.contents.clear
#~     create_contents
#~     @data = []
#~     @item_max = @data.size
#~     for i in 0..@item_max-1
#~       draw_item(i)
#~     end  
#~   end  
#~   
#~   def add_chain(newobj)
#~     @data.push(newobj) ; draw_item(@data.size)
#~   end
#~   
#~   def remove_lastchain
#~     oldsize = @data.size ; @data.delete_at(@data.size) ; draw_item(oldsize)
#~   end
#~   
#~   def clear_chain
#~     @data.clear ; refresh
#~   end  
#~   #--------------------------------------------------------------------------
#~   # * Get rectangle for displaying items
#~   #     index : item number
#~   #--------------------------------------------------------------------------
#~   def item_rect(index)
#~     rect = Rect.new(0, 0, 0, 0)
#~     rect.width = 24
#~     rect.height = 24
#~     rect.x = index % @column_max * 24
#~     rect.y = index / @column_max * 24
#~     return rect
#~   end
#~   
#~   def draw_item(index, enabled = true)
#~     rect = item_rect(index)
#~     self.contents.clear_rect(rect)
#~     obj = @data[index]
#~     draw_icon(obj.icon_index, rect.x, rect.y, enabled) unless obj.nil?
#~   end
#~   
#~ end

#~ class Scene_Battle < Scene_Base
#~   
#~   alias iex_chainCast_execute_action_skill execute_action_skill unless $@
#~   def execute_action_skill
#~     iex_chainCast_execute_action_skill
#~     execute_chain_skill
#~     while @active_battler.chain_castset.size > 0
#~       @message_window.clear
#~       @active_battler.action = @active_battler.chain_castset.shift
#~       iex_chainCast_execute_action_skill
#~       execute_chain_skill
#~     end  
#~   end
#~   
#~   def execute_chain_skill
#~   end
#~   
#~ end
