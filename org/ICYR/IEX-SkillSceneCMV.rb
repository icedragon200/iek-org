# IEX - Skill Scene CMV
# Date Created : 01/29/2011
module IEX
  module SCENE_SKILL
    SKILL_TYPES = {
      "HEALING" => 2,
      "CHAOS"   => 18,
      "BATTLE"  => 20,
      "ALL"     => 1,
    }   
    SKILL_TYPE_ORDER = ["HEALING", "BATTLE", "CHAOS", "ALL"]
  end
end

class RPG::Skill
  
  def sceski_cmv_cache
    @sceski_cmv_cache_complete = false
    @sorting_group = ["ALL"]
    self.note.split(/[\r\n]+/).each { |line|
    case line
    when /<(?:SKILL_GROUP|SKILLGROUP|SKILL GROUP):[ ]*(.*)>/i
      @sorting_group << $1.to_s.upcase
    end  
    }
    @sceski_cmv_cache_complete = true
  end
  
  def sorting_group
    sceski_cmv_cache unless @sceski_cmv_cache_complete
    return @sorting_group
  end
  
end

class Window_Skill < Window_Selectable 
  
  attr_accessor :column_max 
  attr_accessor :sorting_group
  
  alias iex_wscmv_initialize initialize unless $@
  def initialize(*args)
    @sorting_group = nil
    @data = []
    @first_time = false
    iex_wscmv_initialize(*args)
  end  
  
  unless $imported["BattleEngineMelody"]
    #--------------------------------------------------------------------------
    # overwrite method: refresh
    #--------------------------------------------------------------------------
    def refresh
      @spacing = 4 if $game_temp.less_spacing
      @data = []
      for skill in @actor.skills
        next unless include?(skill)
        @data.push(skill)
        if skill.id == @actor.last_skill_id
          self.index = @data.size - 1 unless @first_time
        end
      end
      @item_max = @data.size
      self.index = [self.index, @data.size - 1].min
      create_contents
      @first_time = true
      for i in 0...@item_max; draw_item(i); end
    end
    
    #--------------------------------------------------------------------------
    # new method: include?
    #--------------------------------------------------------------------------
    def include?(iskill)
      if iskill != nil
        return false if iskill.name == ""
      end
      return true
    end
  end
  
  alias iex_wscmv_include? include? unless $@
  def include?(iskill)
    unless @sorting_group.nil?
      return false unless iskill.sorting_group.include?(@sorting_group)
    end  
    return iex_wscmv_include?(iskill)
  end
  
  def change_sortingroup(new_group)
    if @sorting_group != new_group
      @sorting_group = new_group
      refresh
    end  
  end
  
end
  
#==============================================================================
# ** IEX_SS_IconSprite
#------------------------------------------------------------------------------
#==============================================================================
class IEX_SS_IconSprite < Sprite
  
  def initialize(viewport = nil)
    super(viewport)
    self.bitmap = Cache.system("Iconset")
    set_icon(0)
  end
  
  def set_icon(icon_index)
    self.src_rect.set(icon_index % 16 * 24, icon_index / 16 * 24, 24, 24)
  end
  
end

#==============================================================================
# ** IEX_SkillType_BarWindow
#------------------------------------------------------------------------------
#==============================================================================
class IEX_SkillType_BarWindow < Window_Selectable
  
  attr_accessor :back_sprite
  
  def initialize(*args)
    super(*args)
    @back_sprite = Sprite.new
    @back_sprite.x = self.x
    @back_sprite.y = self.y
    @type = "ALL"
    @type_icons = {}
    for wi in IEX::SCENE_SKILL::SKILL_TYPES.keys
      @type_icons[wi]  = IEX_SS_IconSprite.new
      @type_icons[wi].z = 200
      @type_icons[wi].set_icon(IEX::SCENE_SKILL::SKILL_TYPES[wi])
    end 
    @icon_order = IEX::SCENE_SKILL::SKILL_TYPE_ORDER
    @item_max   = @icon_order.size
    @column_max = @item_max
    self.index  = 0
    update_icon_pos
    update_icon_type
  end  
  
  def set_coords(coords)
    self.x = coords[0]
    self.y = coords[1]
    self.width = coords[2]
    self.height = coords[3]
    self.opacity = coords[4]
    create_contents
    update
  end
  
  def dispose
    if @back_sprite != nil
      @back_sprite.dispose
      @back_sprite = nil
    end
    for spr in @type_icons.values
      next if spr == nil
      spr.dispose
      spr = nil
    end  
    super
  end
  
  def set_icon_opacity(val)
    for spr in @type_icons.values
      next if spr == nil
      spr.opacity = val
    end 
  end

  def update_fadein
    for key in @type_icons.keys
      spr = @type_icons[key]
      next if spr == nil
      olimit = 128 if @type == key
      olimit = 255 if @type == key
      spr.opacity = [spr.opacity + (255/60), olimit].min
    end  
  end
  
  def update_fadeout
  end
  
  def update
    super
    update_win_type(@icon_order[self.index])
    update_icon_pos
    if @back_sprite != nil
      @back_sprite.visible = self.visible
      @back_sprite.x = self.x
      @back_sprite.y = self.y
    end 
  end
  
  def current_type
    return @type
  end
  
  def update_win_type(new_type)
    if new_type != @type and new_type != nil
      @type = new_type
      update_icon_type
    end  
  end
  
  def update_icon_pos
    coun = 0
    for ty in @icon_order
      spr = @type_icons[ty]
      next if spr == nil
      perwidth = self.contents.width / @icon_order.size
      sx = (self.contents.width - (24 * @icon_order.size)) / @icon_order.size / 2
      spr.x = 16+self.x+sx+(perwidth * coun)
      spr.y = self.y + (self.height-24) / 2
      coun += 1
    end 
  end
  
  #--------------------------------------------------------------------------
  # * Get rectangle for displaying items
  #     index : item number
  #--------------------------------------------------------------------------
  def item_rect(index)
    rect = Rect.new(0, 0, 0, 0)
    rect.width = (contents.width + @spacing) / @column_max - @spacing
    rect.height = WLH
    rect.x = index % @column_max * (rect.width + @spacing)
    rect.y = index / @column_max * WLH
    unless @type_icons.nil?
      unless @type_icons[@type].nil?
        rect.width = 32 ; rect.height = 32
        rect.x = @type_icons[@type].x - self.x - 16 - 4
        rect.y = @type_icons[@type].y - self.y - 16 - 4
      end  
    end  
    return rect
  end
  
  def update_icon_type
    for spr in @type_icons.values
      next if spr == nil
      spr.opacity = 128
    end  
    case @type.upcase
    when "ALL"
      for spr in @type_icons.values
        next if spr == nil
        spr.opacity = 255
      end  
    else 
      if @type_icons.has_key?(@type)
        @type_icons[@type].opacity = 255
      end  
    end 
  end
  
  def visible=(vis)
    @back_sprite.visible = vis
    for spr in @type_icons.values
      next if spr == nil
      spr.visible = vis
    end 
    super(vis)
  end
      
end  

class Scene_Skill < Scene_Base
  
  #--------------------------------------------------------------------------
  # * Start processing
  #--------------------------------------------------------------------------
  def start
    super
    create_menu_background
    @actor          = $game_party.members[@actor_index]
    # ------------------------------------------------------------------------ #
    @viewport       = Viewport.new(0, 0, Graphics.width, Graphics.height)
    @help_window    = Window_Help.new
    @status_window  = Window_SkillStatus.new(0, 56, @actor)
    @skill_type_bar = IEX_SkillType_BarWindow.new(0, 56, Graphics.width, 56)
    @skill_window   = Window_Skill.new(0, 112, Graphics.width/2, 304, @actor)
    @target_window  = Window_MenuStatus.new(0, 0)
    # ------------------------------------------------------------------------ #
    hide_target_window
    #@help_window.viewport     = @viewport
    #@status_window.viewport   = @viewport
    #@skill_window.viewport    = @viewport
    @status_window.width       = Graphics.width
    @status_window.refresh
    # ------------------------------------------------------------------------ #
    @skill_type_bar.y         = 56*2
    @skill_type_bar.update
    @skill_type_bar.active    = true
    # ------------------------------------------------------------------------ #
    @skill_window.y           = 56*3
    @skill_window.height      = Graphics.height - @skill_window.y
    @skill_window.help_window = @help_window
    @skill_window.active      = false
    @skill_window.column_max  = 1
    # ------------------------------------------------------------------------ #
    @skill_type_bar.update
    @skill_window.change_sortingroup(@skill_type_bar.current_type)
    @skill_window.refresh
    # ------------------------------------------------------------------------ #
  end
  
  #--------------------------------------------------------------------------
  # * Termination Processing
  #--------------------------------------------------------------------------
  alias iex_skisce_terminate terminate unless $@
  def terminate
    @skill_type_bar.dispose unless @skill_type_bar.nil?
    iex_skisce_terminate
  end
  
  #--------------------------------------------------------------------------
  # * Frame Update
  #--------------------------------------------------------------------------
  def update
    super
    update_menu_background
    @help_window.update
    @status_window.update
    @skill_window.update
    @target_window.update
    @skill_type_bar.update
    @skill_window.change_sortingroup(@skill_type_bar.current_type)
    if @skill_type_bar.active
      update_skillset_selection
    elsif @skill_window.active
      update_skill_selection
    elsif @target_window.active
      update_target_selection
    end
  end
  
  def update_skillset_selection
    if Input.trigger?(Input::C)
      Sound.play_decision
      @skill_window.active   = true
      @skill_type_bar.active = false
      @skill_window.index = 0 if @skill_window.index == -1
    elsif Input.trigger?(Input::B)  
      Sound.play_cancel
      return_scene
    end  
  end
  
  #--------------------------------------------------------------------------
  # * Update Skill Selection
  #--------------------------------------------------------------------------
  alias iex_skisce_update_skill_selection update_skill_selection unless $@
  def update_skill_selection
    if Input.trigger?(Input::B)
      @skill_type_bar.active = true
      @skill_window.active   = false
      return
    end  
    iex_skisce_update_skill_selection  
  end
  
end  
  
