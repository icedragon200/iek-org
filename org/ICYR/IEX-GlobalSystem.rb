# IEX - Global System
module IEX
  module GLOBAL_SYSTEM
    # This is the tint that will be applied for dusk.
    DUSK_TINT = Tone.new(17, -51, -102, 0)
    # This is the tint that will be applied for night.
    NIGHT_TINT = Tone.new(-187, -119, -17, 68)
    # This is the tint that will be applied for dawn.
    DAWN_TINT = Tone.new(17, -51, -102, 0)
    # This is the tint that will be applied for day.
    DAY_TINT = Tone.new(0, 0, 0, 0) 
    
    NO_TINT = Tone.new(0, 0, 0, 0) 
    
    PHASE_ICONS = {
      nil    => 0,
     "DUSK"  => 2584,
     "NIGHT" => 2581,
     "DAWN"  => 2583,
     "DAY"   => 2582,
    }
  end
end

GLOSYS = IEX::GLOBAL_SYSTEM

class Scene_Map < Scene_Base
  
  alias iex_globsys_start start unless $@
  def start
    @mapstate_window = Window_Base.new(0, -56, 96, 56)
    @mapstate_window.opacity = 0
    @mapstate_window.create_contents
    iex_globsys_start
  end
  
  def dispose_mapstate_window
    @mapstate_window.dispose unless @mapstate_window.nil?
  end
  
  alias iex_globsys_terminate terminate unless $@
  def terminate
    dispose_mapstate_window
    iex_globsys_terminate
  end 
  
  alias iex_globsys_update update unless $@
  def update
    iex_globsys_update
    update_mapstate_window
  end  
  
  #--------------------------------------------------------------------------
  # * Update MapState Sprite
  #--------------------------------------------------------------------------  
  def update_mapstate_window
    return if @mapstate_window.nil?
    if $game_global_sys.current_phase_time > 0 
      @mapstate_window.y += (32/60.0).abs unless @mapstate_window.y >= -12
    else
      @mapstate_window.y -= (56/60.0).abs unless @mapstate_window.y <= -56
    end  
    unless @old_time == $game_global_sys.current_phase_time
      @mapstate_window.contents.clear
      time = $game_global_sys.current_phase_time
      align = 2
      text_size = 18
      rect = Rect.new(0, 0, @mapstate_window.contents.width, 24)
      rect.y -= 4
      @mapstate_window.contents.font.size = text_size
      @mapstate_window.contents.font.color = Color.new(255, 255, 255, 255)
      @mapstate_window.contents.draw_text(rect, time, align)
      @old_time = $game_global_sys.current_phase_time
      @mapstate_window.draw_grad_bar(rect.x+12, rect.y+20, rect.width-12, 8, 
        time.abs, 360, # Values
        # Colors
        @mapstate_window.mp_gauge_color1, @mapstate_window.mp_gauge_color2,
        Color.new(20, 20, 20, 168), 
        2)
      phase_icon = GLOSYS::PHASE_ICONS[$game_global_sys.current_phase]           
      @mapstate_window.draw_icon(phase_icon, 0, 0)
      @old_phase_icon = phase_icon 
    end  
  end
   
end

class Game_Interpreter
  
  def tone_change(tone, fadetime=60)
    screen.start_tone_change(tone, fadetime)
  end
  
end

# $game_global_sys
class Global_System < Game_Battler
   
  include IEX::GLOBAL_SYSTEM
  
  attr_accessor :level
  
  def initialize
    super
    @level = 1
    @battle_states = [] # Array Containing State IDs
    @battle_skills = [] # Array Containing Skill IDs
    @battle_state_times = {}
    # Special array containing strings, instead of IDS
    @map_states = []
    @map_state_times = {} # The state time here, is done in steps instead of turns.
    
    @targ_tone = nil
  end
  
  def map_state_time(user, state)
    case state.to_s.upcase
    when "NIGHT"
      return 360  
    when "DAY"  
      return 360  
    when "DUSK"  
      return 360  
    when "DAWN"  
      return 360  
    else ; return 60  
    end 
  end
  
  def undo_effect(state)
    case state.to_s.upcase
    when "NIGHT", "DAY", "DUSK", "DAWN"
      $game_map.interpreter.tone_change(NO_TINT, 10)  
    end 
  end
  
  def apply_new_effects(state)
    case state.to_s.upcase
    when "NIGHT"
      @targ_tone = NIGHT_TINT
    when "DAY"  
      @targ_tone = DAY_TINT
    when "DUSK"  
      @targ_tone = DUSK_TINT
    when "DAWN"  
      @targ_tone = DAWN_TINT
    end 
    unless @targ_tone.nil?
      $game_map.interpreter.tone_change(@targ_tone)
    end  
  end
  
  def displace_map_state(state)
    case state.to_s.upcase
    when "NIGHT"
      remove_map_state("DAY")
      remove_map_state("DUSK")
      remove_map_state("DAWN")
    when "DAY"  
      remove_map_state("NIGHT")
      remove_map_state("DUSK")
      remove_map_state("DAWN")
    when "DUSK"  
      remove_map_state("NIGHT")
      remove_map_state("DAY")
      remove_map_state("DAWN")
    when "DAWN"  
      remove_map_state("NIGHT")
      remove_map_state("DAY")
      remove_map_state("DUSK")
    end  
  end
  
  def add_map_state(state, user=nil)
    @map_states << state unless @map_states.include?(state)
    @map_state_times[state] = map_state_time(user, state)
    displace_map_state(state)
    apply_new_effects(state)
  end
  
  def remove_map_state(state)
    @map_states.delete(state)
    @map_state_times.delete(state)
    undo_effect(state)
  end
  
  def update_map_states
    for key in @map_state_times.keys
      @map_state_times[key] -= 1 unless @map_state_times[key].zero?
      remove_map_state(key) if @map_state_times[key].zero?
    end  
  end
  
  def map_states
    return @map_states
  end
  
  def current_phase
    for st in map_states
      return st if ["NIGHT", "DAY", "DUSK", "DAWN"].include?(st)
    end  
    return nil
  end
  
  def current_phase_time
    return @map_state_times[current_phase].to_i
  end
  
  def night?; return map_states.include?("NIGHT") end
  def day?  ; return map_states.include?("DAY")   end
  def dusk? ; return map_states.include?("DUSK")  end
  def dawn? ; return map_states.include?("DAWN")  end
    
  def global_refresh
    ($game_party.members + $game_troop.members).each { |bat| 
      bat.update_states = true
      bat.update_maxhp = true
      bat.update_maxmp = true
      bat.update_commands = true
      bat.clear_battle_cache
      bat.reload_state_animation 
     }
    $scene.refresh_battle_windows
  end
  
  def remove_states_auto
    super
    for i in @battle_state_times.keys.clone
      if @battle_state_times[i] > 0
        @battle_state_times[i] -= 1
      elsif rand(100) < $data_states[i].auto_release_prob 
        @battle_states.delete(i)
      end  
    end  
  end
  
  def clear_battle_cache
    @states.clear
    @battle_states.clear
    @battle_skills.clear
  end
    
  def state_ids
    return @states + @battle_states
  end
    
  def foAddState(state_id)
    state_id = state_id.to_i
    @battle_states.push(state_id)
    @battle_state_times[state_id] = $data_states[state_id].hold_turn
  end
    
  def get_battle_states
    result = []
    for i in @battle_states
      result.push($data_states[i])
    end  
    return result
  end
    
  #alias iex_gbs_gs_states states unless $@
  def states
    result = super #iex_gbs_gs_states
    result |= get_battle_states
    return result
  end
  
  def skills
    result = []
    for i in @battle_skills 
      result |= [$data_skills[i]] 
    end 
    return result
  end
  
  def hp ; return 999999 end
  def mp ; return 999999 end  
  def maxhp ; return 999999 end
  def maxmp ; return 999999 end
  def eva ; return 0 end  
  def cri ; return 0 end
  def stack(*args) ; return 1 end    
    
  def actor? ; return false end
  def dead? ; return false end
  def screen_x ; return Graphics.width / 2 end
  def screen_y ; return Graphics.height / 2 end
  def screen_z ; return 100 end  
  def create_level ; return 1 end  
  def rage_modifiers(*args) ; return 0 end  
  def perform_collapse ; return false end
    
end

class Scene_Title < Scene_Base
   
  alias iex_gbs_create_game_objects create_game_objects unless $@
  def create_game_objects
    iex_gbs_create_game_objects
    $game_global_sys = Global_System.new
  end
  
end

class Scene_File < Scene_Base
  
  alias iex_gbs_write_save_data write_save_data unless $@
  def write_save_data(file)
    iex_gbs_write_save_data(file)
    Marshal.dump($game_global_sys,    file)
  end
  
  alias iex_gbs_read_save_data read_save_data unless $@
  def read_save_data(file)
    iex_gbs_read_save_data(file)
    $game_global_sys  = Marshal.load(file)
  end 
  
end

class Scene_Battle < Scene_Base
  #--------------------------------------------------------------------------
  # * End Turn
  #--------------------------------------------------------------------------
  alias iex_gbs_remove_states_auto remove_states_auto unless $@
  def remove_states_auto
    iex_gbs_remove_states_auto
    $game_global_sys.remove_states_auto
  end
  
  #--------------------------------------------------------------------------
  # * End Battle
  #     result : Results (0: win, 1: escape, 2:lose)
  #--------------------------------------------------------------------------
  alias iex_gbs_battle_end battle_end unless $@
  def battle_end(result)
    iex_gbs_battle_end(result)
    $game_global_sys.clear_battle_cache
  end
  
end

class Game_Enemy < Game_Battler
  
  alias iex_gbs_estates states unless $@
  def states
    result = iex_gbs_estates
    result |= $game_global_sys.states
    return result
  end
    
end

class Game_Actor < Game_Battler
  
  alias iex_gbs_astates states unless $@
  def states
    result = iex_gbs_astates
    result |= $game_global_sys.states
    return result
  end
  
  alias iex_gbs_skills skills unless $@
  def skills
    result = iex_gbs_skills
    result |= $game_global_sys.skills
    return result
  end
  
end

