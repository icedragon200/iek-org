# IEX - Armor Endurance
#~ #==============================================================================#
#~ # ** IEX(Icy Engine Xelion) - Armor Endurance
#~ #------------------------------------------------------------------------------#
#~ # ** Created by    : IceDragon (http://www.rpgmakervx.net/)
#~ # ** Script-Status : Addon (Armor)
#~ # ** Script Type   : New Stat
#~ # ** Date Created  : 01/03/2011 (DD/MM/YYYY)
#~ # ** Date Modified : 01/08/2011 (DD/MM/YYYY)
#~ # ** Script Tag    : IEX - Armor Endurance
#~ # ** Difficulty    : Easy
#~ # ** Version       : 1.0
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** INTRODUCTION
#~ #------------------------------------------------------------------------------#
#~ # *WARNING* DO NOT USE THIS SCRIPT WITHOUT A DATABASE CLONER (Duplicater)
#~ # *WARNING* THIS SCRIPT MAKES DATABASE CHANGES (Which will be lost on close, 
#~ #                                               unless saved)
#~ # You are no longer invinclible in your armor!
#~ # All armors now have Endurance, so they can break if you get hit too much.
#~ # >.> This script should not be used on its own.
#~ # This is because it has to edit indiviual armors.
#~ # Only use this script with another one that copies the Armors data.
#~ #
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** HOW TO USE
#~ #------------------------------------------------------------------------------#
#~ # Notetags - Armors (Apply to Armor's Notebox)
#~ #------------------------------------------------------------------------------#
#~ # <ENDUR CAP: x>
#~ # This is the armors endurance maximum amount.
#~ #
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** INSTALLATION
#~ #------------------------------------------------------------------------------#
#~ # 
#~ # Below 
#~ #  Materials
#~ #  Custom Battle Systems
#~ #  Anything that Changes the attack_effect
#~ #
#~ # Above 
#~ #   Main
#~ #
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** CHANGES 
#~ #------------------------------------------------------------------------------#
#~ # Classes
#~ #   RPG::Armor
#~ #     new-method :iex_ArmEndCache
#~ #     new-method :endurance
#~ #     new-method :endurance=
#~ #     new-method :endurance_cap
#~ #     new-method :endurance_max # Same as endurance_cap
#~ #     new-method :broken?
#~ #   Game_Battler
#~ #     alias      :attack_effect
#~ #     new-method :increase_endur
#~ #     new-method :decrease_endur
#~ #   Game_Actor
#~ #     new-method :remove_broken_equipment
#~ #     overwrite  :increase_endur
#~ #     overwrite  :increase_tec
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** CHANGE LOG
#~ #------------------------------------------------------------------------------#
#~ # (DD/MM/YYYY)
#~ #  01/03/2011 - V1.0  Started and Finsihed Script
#~ #  01/08/2011 - V1.0a Small Change
#~ #
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** KNOWN ISSUES
#~ #------------------------------------------------------------------------------#  
#~ #
#~ #  Non at the moment
#~ #
#~ #------------------------------------------------------------------------------#
#~ $imported = {} if $imported == nil
#~ $imported["IEX_ArmorEndurance"] = true

#~ #==============================================================================
#~ # ** IEX::ARMOR_ENDURANCE
#~ #------------------------------------------------------------------------------
#~ #==============================================================================
#~ module IEX
#~   module ARMOR_ENDURANCE
#~     DEFAULT_ENDURANCE = 400 # Endurance is like HP so, make it BIG!
#~     DAMAGE_RATE = 20
#~   end
#~ end

#~ #==============================================================================
#~ # ** RPG::Armor
#~ #------------------------------------------------------------------------------
#~ #==============================================================================
#~ class RPG::Armor
#~   
#~   def iex_ArmEndCache
#~     @iex_ArmEndCache_complete = false
#~     @endurance_cap = IEX::ARMOR_ENDURANCE::DEFAULT_ENDURANCE
#~     self.note.split(/[\r\n]+/).each { |line|
#~       case line
#~       when /<(?:ENDURANCE_CAP|endurance cap|endcap|end_cap|end cap):?[ ]*(\d+)>/i
#~         @endurance_cap = $1.to_i if $1.to_i > 0
#~       end  
#~     }
#~     @endurance = @endurance_cap
#~     @iex_ArmEndCache_complete = true
#~   end
#~   
#~   def endurance
#~     iex_ArmEndCache unless @iex_ArmEndCache_complete
#~     return Integer(@endurance)
#~   end
#~   
#~   def endurance=(val)
#~     iex_ArmEndCache unless @iex_ArmEndCache_complete
#~     @endurance = [[Integer(val), Integer(@endurance_cap)].min, 0].max
#~   end
#~   
#~   def endurance_cap
#~     iex_ArmEndCache unless @iex_ArmEndCache_complete
#~     return Integer(@endurance_cap)
#~   end
#~   
#~   def endurance_max ; return endurance_cap end
#~   def broken? ; return endurance == 0 end
#~   
#~ end  

#~ #==============================================================================
#~ # ** Game_Battler
#~ #------------------------------------------------------------------------------
#~ #==============================================================================
#~ class Game_Battler
#~      
#~   def increase_endur(val, armor_type = -1)
#~   end
#~     
#~   def decrease_endur(val, armor_type = -1)
#~     increase_endur(-val, armor_type)
#~   end
#~   
#~   alias iex_armend_attack_effect attack_effect unless $@
#~   def attack_effect(*args)
#~     iex_armend_attack_effect(*args)
#~     eq_damage = IEX::ARMOR_ENDURANCE::DAMAGE_RATE * @hp_damage / 100.0
#~     decrease_endur(Integer(eq_damage)) if @hp_damage > 0
#~   end
#~   
#~ end  

#~ #==============================================================================
#~ # ** Game_Actor
#~ #------------------------------------------------------------------------------
#~ #==============================================================================
#~ class Game_Actor < Game_Battler
#~   
#~   def remove_broken_equipment
#~     range = 1..4
#~     range = 2..4 if two_swords_style
#~     for i in range
#~       armor = eval("$data_armors[@armor#{i}_id]")  
#~       unless armor.nil? ; eval("@armor#{i}_id = 0") if armor.broken? end
#~     end  
#~   end
#~   
#~   def increase_endur(val, armor_type = -1)
#~     case armor_type
#~     when -1
#~       for eq in equips.compact
#~         eq.endurance += val if eq.is_a?(RPG::Armor)
#~       end  
#~     when 1
#~       unless two_swords_style
#~         $data_armors[@armor1_id].endurance += val if eq.is_a?(RPG::Armor)
#~       end  
#~     when 2
#~       eq = $data_armors[@armor2_id]
#~       eq.endurance += val if eq.is_a?(RPG::Armor)
#~     when 3
#~       eq = $data_armors[@armor3_id]
#~       eq.endurance += val if eq.is_a?(RPG::Armor)
#~     when 4
#~       eq = $data_armors[@armor4_id]
#~       eq.endurance += val if eq.is_a?(RPG::Armor)
#~     end  
#~     remove_broken_equipment
#~   end
#~   
#~ end
