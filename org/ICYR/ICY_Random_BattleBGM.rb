# ICY_Random_BattleBGM
#~ #------------------------------------------------------------------------------#
#~ # ** ICY Random Battle BGM
#~ # ** Created by : IceDragon
#~ # ** Script-Status : Addon
#~ # ** Date Created : 4/19/2010
#~ # ** Date Modified: 7/01/2010
#~ #------------------------------------------------------------------------------#
#~ # This script allows the application of random BGMs
#~ # They can be placed into groups, by numbers.
#~ # If you want to play a set, change the variable to the corresponding set
#~ # And start the battle....

#~ #-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
#~ #-#-#-#-#START CUSTOMIZATION#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
#~ #-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
#~ module ICY
#~   module Rand_BatBGM
#~     #This is the BGM list
#~     #[BGM Name, Volume, Pitch]
#~     BGMs = {
#~     #NormalSet
#~     0 =>[
#~     ["TBS_Battle01", 90, 100], 
#~     ["TBS_Battle02", 90, 100],
#~     ["TBS_Battle03", 90, 100],
#~     ["TBS_Battle04", 90, 100],
#~       ],
#~     #Desert Set
#~     1 =>[
#~     ["207 Sleeping Treasure in the Sand", 90, 100],
#~     ["11 - Daijiru, City Of The Burning Sands", 90, 100]      
#~       ],
#~     #Jungle Set
#~     2 =>[
#~     ["62 - Mountain Pass - Conde Petie", 90, 100],
#~     ["110 Echoes at the Mountain Peak.ogg", 90, 100]
#~     ],
#~     #Serene Cave Set
#~     3 =>[
#~     ["121 Voice of Wind, Song of Time", 90, 100],
#~     ["109 Twilight in Dreamland", 90, 100]
#~     ]
#~     }
#~     #This is the switch to turn off the random BGM
#~     Switch = 499
#~     #This is the variable which controls the BGM sets..
#~     Variable = 518
#~   end
#~ end


#~ #-----------------------------------------------------------------------------
#~ # ** Class Scene_Map
#~ #-----------------------------------------------------------------------------
#~ class Scene_Map < Scene_Base
#~   
#~   #--------------------------------------------------------------------------
#~   # alias call battle
#~   #--------------------------------------------------------------------------
#~  alias icy_call_battle_old call_battle unless $@
#~   def call_battle
#~     stored_battle_bgm = $game_system.battle_bgm
#~     unless $game_switches[ICY::Rand_BatBGM::Switch]
#~       all_bgms = ICY::Rand_BatBGM::BGMs
#~       if all_bgms.has_key?($game_variables[ICY::Rand_BatBGM::Variable])
#~         bgms = ICY::Rand_BatBGM::BGMs[$game_variables[ICY::Rand_BatBGM::Variable]]
#~         selection = rand(bgms.size)
#~         tracki = bgms.at(selection)
#~         batbgm = RPG::BGM.new(tracki[0], tracki[1], tracki[2])
#~         $game_system.battle_bgm = batbgm
#~       else
#~         $game_system.battle_bgm = stored_battle_bgm
#~       end
#~     end
#~     icy_call_battle_old
#~     $game_system.battle_bgm = stored_battle_bgm
#~   end
#~   
#~ end # Scene Map
#~ ################################################################################
#~ #------------------------------------------------------------------------------#
#~ #END\\\END\\\END\\\END\\\END\\\END\\\END///END///END///END///END///END///END///#
#~ #------------------------------------------------------------------------------#
#~ ################################################################################
