# #~IEX - Menu Status
#~ #------------------------------------------------------------------------------#
#~ # ** IEX(Icy Engine Xelion) - Menu Status
#~ # ** Created by :IceDragon 
#~ # ** Script-Status : ReWrite
#~ # ** Date Modified : 7/20/2010
#~ # ** Version 1.1
#~ #------------------------------------------------------------------------------#
#~ #------------------------------------------------------------------------------#
#~ # ** This here is a Amped up version of the menu status to match with my item window
#~ #------------------------------------------------------------------------------#
#~ # INTRODUCTION
#~ # So after I kinda played around with my new HM_Style_Items.
#~ # I decided, "Why not mess with the other windows too!"
#~ # And so the birth of this came along!
#~ #------------------------------------------------------------------------------#
#~ # PLACEMENT
#~ # Above 
#~ #   Main
#~ #
#~ # Below
#~ #   Materials
#~ #   Custom Item Windows
#~ #   Custom Skill Windows
#~ #   Custom Menu Systems
#~ #------------------------------------------------------------------------------#
#~ # CHANGES
#~ # This script overwrites all of the Window_Menu_Status.
#~ #
#~ #------------------------------------------------------------------------------#
#~ # COMPATABILTIES
#~ # Works nicely with IEX_Scene_Skill(HM_Styled) and the IEX_Scene_Item(HM_Styled)
#~ # Requires SYVKAL_MenuBars
#~ #------------------------------------------------------------------------------#
#~ # KNOWN ISSUES
#~ # Non at the moment.
#~ #
#~ #------------------------------------------------------------------------------#
#~ # CHANGE LOG
#~ # 7/17/2010 Finished Script V_1.0
#~ # 8/28/2010 Added in Draw States (Yeah I forgot to..)
#~ #
#~ #------------------------------------------------------------------------------#
#~ $imported = {} if $imported == nil
#~ $imported["IEX_HM_Style_Menu_Status"] = true
#~ #------------------------------------------------------------------------------#
#~ # ** Start Customization
#~ #------------------------------------------------------------------------------#
#~ module IEX
#~   module Menu_Status
#~     RECT_SIZE = 72
#~     BORDER_SIZE = 4
#~     ITEM_SQ_SPACING = 86
#~     SELECTION_SIZE = 80
#~     NUMBER_COLUMNS = 4
#~   end
#~ end
#~ #------------------------------------------------------------------------------#
#~ # ** End Customization
#~ #------------------------------------------------------------------------------#
#~ # DON'T TOUCH ANYTHING BEYOND THIS POINT!!!!
#~ # UNLESS YOU KNOW WHAT YOUR DOING!
#~ #------------------------------------------------------------------------------#

#~ class Sprite
#~   #--------------------------------------------------------------------------
#~   # * Draw Face Graphic
#~   #     face_name  : Face graphic filename
#~   #     face_index : Face graphic index
#~   #     x     : draw spot x-coordinate
#~   #     y     : draw spot y-coordinate
#~   #     size       : Display size
#~   #--------------------------------------------------------------------------
#~   def draw_face(face_name, face_index, x, y, size = 96)
#~     dbitmap = Cache.face(face_name)
#~     rect = Rect.new(0, 0, 0, 0)
#~     rect.x = face_index % 4 * 96 + (96 - size) / 2
#~     rect.y = face_index / 4 * 96 + (96 - size) / 2
#~     rect.width = size
#~     rect.height = size
#~     self.bitmap.blt(x, y, dbitmap, rect)
#~     dbitmap.dispose
#~     dbitmap = nil
#~   end
#~   
#~ end

#~ #==============================================================================
#~ # ** Iex_Window_Menu_Status_Help
#~ #------------------------------------------------------------------------------
#~ #  Exclusive Iex_Menu_Status Script!
#~ #==============================================================================
#~ class IEX_Window_Menu_Status_Help < Window_Base
#~   include IEX::Menu_Status
#~   def initialize(x, y)
#~     super(x, y, 384, 416 / 2)
#~     @stored_actor = nil
#~     @forced_reset = true
#~   end
#~   
#~   def re_initialize
#~     self.contents.clear
#~     create_contents
#~     @stored_actor = nil
#~     @forced_reset = true
#~   end
#~   
#~   #--------------------------------------------------------------------------
#~   # * Draw Sprite Actor = This a X, Y accurate version of the draw_actor_graphic
#~   #   actor   : actor 
#~   #     x     : draw spot x-coordinate
#~   #     y     : draw spot y-coordinate 
#~   #--------------------------------------------------------------------------
#~   def draw_sprite_actor(actor, x, y)
#~     draw_actor_sprite(actor, x, y)
#~   end
#~   #------------------------------------------------------------------------
#~   #  * Set Item / Draw_Item
#~   #------------------------------------------------------------------------
#~   def set_item(actor)
#~     return if @stored_actor == actor unless @forced_reset
#~     return if actor == nil
#~     self.contents.clear
#~     @reset = 0 
#~     @nw_x = @reset
#~     @nw_y = 0
#~     @coun = 0
#~     outline = Rect.new(0, 0, RECT_SIZE, RECT_SIZE)
#~     sub_rect = Rect.new((BORDER_SIZE / 2), (BORDER_SIZE / 2), 
#~     (RECT_SIZE - BORDER_SIZE), (RECT_SIZE - BORDER_SIZE))
#~        
#~     srect_color = system_color
#~     outline_sprite = Bitmap.new(RECT_SIZE, RECT_SIZE)
#~     outline_sprite.fill_rect(outline, srect_color)
#~     outline_sprite.clear_rect(sub_rect)
#~     self.contents.blt(@nw_x, @nw_y, outline_sprite, outline)
#~     old_font_size = self.contents.font.size
#~     self.contents.font.size = 21
#~     self.contents.draw_text(@nw_x + RECT_SIZE + 8, @nw_y, 128, WLH, actor.name)
#~     self.contents.font.size = old_font_size
#~     draw_actor_hp(actor, (@nw_x + RECT_SIZE) + 32, @nw_y + 16, 128)
#~     draw_actor_mp(actor, (@nw_x + RECT_SIZE) + 32, @nw_y + 48, 128)
#~     
#~     act_x = @nw_x + (RECT_SIZE / 2)
#~     act_y = @nw_y + (RECT_SIZE / 4) + 28
#~     draw_actor_graphic(actor, act_x, act_y)
#~     dx = @nw_x
#~     if $imported["SubclassSelectionSystem"]
#~       if YE::SUBCLASS::CLASS_ICONS.include?(actor.class.id)
#~         icon = YE::SUBCLASS::CLASS_ICONS[actor.class.id]
#~       else
#~         icon = YE::SUBCLASS::CLASS_ICONS[0]
#~       end
#~       draw_icon(icon, dx, (@nw_y + RECT_SIZE) + 4)
#~       dx += 24
#~       if actor.subclass != nil
#~         if YE::SUBCLASS::CLASS_ICONS.include?(actor.subclass.id)
#~           icon = YE::SUBCLASS::CLASS_ICONS[actor.subclass.id]
#~         else
#~           icon = YE::SUBCLASS::CLASS_ICONS[0]
#~         end
#~         draw_icon(icon, dx, (@nw_y + RECT_SIZE) + 4)
#~         dx += 24
#~       end
#~     end
#~     
#~     old_font_size = self.contents.font.size
#~     self.contents.font.size = 18
#~     self.contents.font.color = system_color
#~     lv_text = "#{Vocab::level_a}:#{actor.level}"
#~     self.contents.draw_text(@nw_x, (@nw_y + RECT_SIZE) - 28, RECT_SIZE, 18, lv_text, 1)
#~     self.contents.font.size = 16
#~     draw_actor_class(actor, dx, (@nw_y + RECT_SIZE) + 4)
#~     self.contents.font.color = normal_color
#~     self.contents.font.size = old_font_size
#~     
#~     draw_equipment(@nw_x, (@nw_y + RECT_SIZE) + 32, actor)
#~     iex_draw_states(@nw_x + (self.width / 2) + 48, @nw_y, actor)
#~     xtra_y = 0
#~      if $imported["SkillOverhaul"]
#~        jp_x = @nw_x + (self.width / 2) + 48
#~        jp_y = @nw_y + 64
#~        draw_actor_jp(actor, actor.class_id, jp_x, jp_y)
#~        xtra_y += 32
#~      end
#~      exp_x = @nw_x + (self.width / 2) + 48
#~      exp_y = @nw_y + 64 + xtra_y
#~      draw_actor_exp(actor, exp_x, exp_y)
#~      
#~     @stored_actor = actor
#~     @forced_reset = false
#~   end

#~   #-------------------------------------------------------------------------
#~   # * Draw Equipment
#~   #-------------------------------------------------------------------------
#~   def draw_equipment(x, y, actor)
#~     srect_color = system_color
#~     outline = Rect.new(0, 0, 32, 32)
#~     sub_rect = Rect.new((4 / 2), (4 / 2),(32 - 4), (32 - 4))
#~     outline_sprite = Bitmap.new(32, 32)
#~     outline_sprite.fill_rect(outline, srect_color)
#~     outline_sprite.clear_rect(sub_rect)
#~     if $imported["MainMenuZealous"]
#~       draw_icon(YEZ::MENU::MENU_ICONS[:equip], x, y)
#~     elsif $imported["MainMenuMelody"]
#~       draw_icon(YEM::MENU::MENU_ICONS[:equip], x, y)
#~     end
#~     self.contents.draw_text(x + 32, y, self.width, WLH, "#{Vocab.equip}")
#~     spac = 40 * actor.equips.size
#~     e_x = x + (((self.width - spac) - x) / 2)
#~     e_y = y + 32
#~     for i in actor.equips
#~       self.contents.blt(e_x, e_y, outline_sprite, outline)
#~       if i != nil
#~         draw_icon(i.icon_index, e_x + 4, e_y + 4)
#~       end
#~       e_x += 36
#~     end
#~   end
#~   
#~   def iex_draw_states(x, y, actor)
#~     width  = 26
#~     height = 26
#~     border = 2
#~     color  = system_color
#~     count = 0
#~     space = 28
#~     stat = []
#~     next_line = 0
#~     max_stuff = 7
#~     for state in actor.states
#~       stat.push(state.icon_index)
#~     end
#~     
#~     for i in 0..max_stuff
#~       next_x = x + (space * count)
#~       next_y = y + (space * next_line)
#~        if stat[i] != nil
#~          draw_outlined_icon(stat[i].to_i, next_x, next_y, width, height, border, color)
#~         else
#~          draw_border_rect(next_x, next_y, width, height, border, color)
#~        end
#~        count += 1
#~        if count == 4
#~          next_line += 1
#~          count = 0
#~        end
#~      end
#~   end
#~   
#~   #--------------------------------------------------------------------------
#~   # draw_actor_exp
#~   #--------------------------------------------------------------------------
#~   def draw_actor_exp(actor, dx, dy)
#~       #exp_icon = 2
#~       wid = (contents.width - dx) - 24# 96
#~       self.contents.font.color = system_color
#~       self.contents.draw_text(dx, dy, 120, WLH, Vocab.exp, 0)
#~       #draw_icon(exp_icon, dx + wid, dy)
#~       exp = actor.exp
#~       self.contents.font.color = normal_color
#~       self.contents.draw_text(dx, dy, wid, WLH, exp, 2)
#~   end
#~   
#~   #--------------------------------------------------------------------------
#~   # draw_actor_jp
#~   #--------------------------------------------------------------------------
#~   def draw_actor_jp(actor, class_id, dx, dy)
#~     if $imported["SkillOverhaul"]
#~       wid = (contents.width - dx) - 24# 96
#~       return if YEM::SKILL::HIDE_JP
#~       self.contents.font.color = system_color
#~       self.contents.draw_text(dx, dy, 120, WLH, Vocab.jp, 0)
#~       draw_icon(Icon.jp, dx + wid, dy)
#~       jp = actor.jp#(class_id)
#~       self.contents.font.color = normal_color
#~       self.contents.draw_text(dx, dy, wid, WLH, jp, 2)
#~     end
#~   end
#~   
#~ end

#~ #==============================================================================
#~ # ** Window_MenuStatus
#~ #------------------------------------------------------------------------------
#~ #  This window displays party member status on the menu screen.
#~ #==============================================================================

#~ class Window_MenuStatus < ICY_HM_Window_Selectable
#~   include IEX::Menu_Status
#~   #--------------------------------------------------------------------------
#~   # * Object Initialization
#~   #     x : window X coordinate
#~   #     y : window Y coordinate
#~   #--------------------------------------------------------------------------
#~   def initialize(x, y)
#~     super(x, y, 384, 416 / 2)
#~     @column_max = NUMBER_COLUMNS
#~     self.active = false
#~     self.index = 0
#~     @column_max = NUMBER_COLUMNS
#~     @item_sq_spacing = ITEM_SQ_SPACING
#~     @rect_size = RECT_SIZE
#~     @selection_size = SELECTION_SIZE
#~     refresh
#~   end
#~     
#~   #--------------------------------------------------------------------------
#~   # * Draw Sprite Actor = This a X, Y accurate version of the draw_actor_graphic
#~   #   actor   : actor 
#~   #     x     : draw spot x-coordinate
#~   #     y     : draw spot y-coordinate 
#~   #--------------------------------------------------------------------------
#~   def draw_sprite_actor(actor, x, y)
#~      draw_actor_sprite(actor, x, y)
#~    end
#~   #--------------------------------------------------------------------------
#~   # * Refresh
#~   #--------------------------------------------------------------------------
#~   def refresh
#~     super
#~     @item_max = $game_party.members.size
#~     @commands = $game_party.members
#~     create_contents
#~     self.contents.clear
#~     for actor in $game_party.members
#~       draw_item(actor)
#~     end
#~   end
#~   #--------------------------------------------------------------------------
#~   # * Draw Item
#~   #--------------------------------------------------------------------------
#~    def draw_item(actor)
#~     outline = Rect.new(0, 0, @rect_size, @rect_size)
#~     sub_rect = Rect.new((BORDER_SIZE / 2), (BORDER_SIZE / 2), 
#~     (@rect_size - BORDER_SIZE), (@rect_size - BORDER_SIZE))
#~        
#~     srect_color = system_color
#~     outline_sprite = Bitmap.new(@rect_size, @rect_size)
#~     outline_sprite.fill_rect(outline, srect_color)
#~     outline_sprite.clear_rect(sub_rect)
#~     self.contents.blt(@nw_x, @nw_y, outline_sprite, outline)
#~     old_font_size = self.contents.font.size
#~     self.contents.font.size = 16
#~     self.contents.draw_text(@nw_x + 8, (@nw_y + @rect_size) - WLH, @rect_size, WLH, actor.name)
#~     self.contents.font.size = old_font_size
#~     draw_actor_hp_gauge(actor, (@nw_x + 40), @nw_y - 4, (@rect_size / 2), true)
#~     draw_actor_mp_gauge(actor, (@nw_x + 52), @nw_y - 4, (@rect_size / 2), true)
#~     draw_sprite_actor(actor, @nw_x, @nw_y)
#~     old_font_size = self.contents.font.size
#~     self.contents.font.size = 14
#~     self.contents.font.color = system_color
#~     self.contents.draw_text(@nw_x + 8, @nw_y + (@rect_size / 2), 32, WLH, "#{Vocab::level_a}:#{actor.level}")
#~     self.contents.font.color = normal_color
#~     self.contents.font.size = old_font_size
#~     
#~     advance_space
#~   end
#~   
#~   #--------------------------------------------------------------------------
#~   # * Update Help Text
#~   #--------------------------------------------------------------------------
#~   def update_help
#~     if @help_window != nil
#~      @help_window.set_item(@commands[self.index]) 
#~     end
#~   end
#~   
#~ end

#~ #==============================================================================
#~ # ** Scene_Menu
#~ #------------------------------------------------------------------------------
#~ #  This class performs the menu screen processing.
#~ #==============================================================================
#~ class Scene_Menu < Scene_Base
#~   
#~   alias iex_hm_style_menstat_start start unless $@
#~   def start
#~     iex_hm_style_menstat_start
#~     @help_window = IEX_Window_Menu_Status_Help.new(@status_window.x, 416 / 2)
#~     @status_window.help_window = @help_window
#~     @help_window.visible = false
#~   end
#~   
#~   alias iex_hm_style_menstat_terminate terminate unless $@
#~   def terminate
#~     iex_hm_style_menstat_terminate
#~     if @help_window != nil
#~       @help_window.dispose
#~       @help_window = nil
#~     end
#~   end
#~   #--------------------------------------------------------------------------
#~   # * Start Actor Selection
#~   #--------------------------------------------------------------------------
#~   alias iex_hm_style_menstat_start_actor_selection start_actor_selection unless $@
#~   def start_actor_selection
#~     iex_hm_style_menstat_start_actor_selection
#~     @help_window.visible = true unless @help_window == nil
#~   end
#~   #--------------------------------------------------------------------------
#~   # * End Actor Selection
#~   #--------------------------------------------------------------------------
#~   alias iex_hm_style_menstat_end_actor_selection end_actor_selection unless $@
#~   def end_actor_selection
#~     iex_hm_style_menstat_end_actor_selection
#~     @help_window.visible = false unless @help_window == nil
#~   end
#~   
#~   #--------------------------------------------------------------------------
#~   # * Frame Update
#~   #--------------------------------------------------------------------------
#~   alias iex_hm_style_menstat_update update unless $@
#~   def update
#~     iex_hm_style_menstat_update
#~     if @status_window.active
#~       @status_window.update_help
#~     end
#~   end
#~   
#~ end

#~ #==============================================================================
#~ # ** Scene_Item
#~ #------------------------------------------------------------------------------
#~ #  This class performs the item screen processing.
#~ #==============================================================================
#~ unless $imported["IEX_HM_Style_Inventory"]
#~ class Scene_Item < Scene_Base
#~   
#~   alias iex_new_item_menu_status_start start unless $@
#~   def start(*args)
#~     iex_new_item_menu_status_start(*args)
#~      if $imported["IEX_HM_Style_Menu_Status"]
#~       @target_win_help = IEX_Window_Menu_Status_Help.new(@target_window.x, @target_window.height)
#~       @target_window.help_window = @target_win_help
#~       @target_win_help.visible = false
#~     end
#~     hide_target_window
#~   end
#~   #--------------------------------------------------------------------------
#~   # * Termination Processing
#~   # ** Aliased
#~   #--------------------------------------------------------------------------
#~   alias iex_hm_style_inve_terminate terminate unless $@
#~   def terminate
#~     iex_hm_style_inve_terminate
#~     if $imported["IEX_HM_Style_Menu_Status"]
#~       @target_win_help.dispose if @target_win_help != nil
#~     end
#~   end
#~   #--------------------------------------------------------------------------
#~   # * Show Target Window
#~   #     right : Right justification flag (if false, left justification)
#~   #--------------------------------------------------------------------------
#~   alias iex_hm_item_show_target_window show_target_window unless $@
#~   def show_target_window(*args)
#~     iex_hm_item_show_target_window(*args)
#~     if $imported["IEX_HM_Style_Menu_Status"]
#~       @target_win_help.x = @target_window.x
#~       @target_win_help.y = @target_window.height
#~       @target_win_help.visible = true 
#~     end
#~   end
#~   
#~   #--------------------------------------------------------------------------
#~   # * Hide Target Window
#~   #--------------------------------------------------------------------------
#~   alias iex_hm_item_hide_target_window hide_target_window unless $@
#~   def hide_target_window(*args)
#~     iex_hm_item_hide_target_window(*args)
#~     if $imported["IEX_HM_Style_Menu_Status"]
#~       @target_win_help.visible = false if @target_win_help != nil
#~     end
#~   end
#~   #--------------------------------------------------------------------------
#~   # * Update Target Selection
#~   #--------------------------------------------------------------------------
#~   alias iex_hm_item_update_target_selection update_target_selection unless $@  
#~   def update_target_selection(*args)
#~     if $imported["IEX_HM_Style_Menu_Status"]
#~       @target_win_help.update
#~     end
#~     iex_hm_item_update_target_selection(*args)
#~   end
#~ end
#~ end

#~ #==============================================================================
#~ # ** Scene_Skill
#~ #------------------------------------------------------------------------------
#~ #  This class performs the skill screen processing.
#~ #==============================================================================
#~ unless $imported["IEX_HM_Style_Skill_Window"] 
#~ class Scene_Skill < Scene_Base
#~   
#~   alias iex_new_skill_menu_status_start start unless $@
#~   def start(*args)
#~     iex_new_skill_menu_status_start(*args)
#~     if $imported["IEX_HM_Style_Menu_Status"]
#~       @target_win_help = IEX_Window_Menu_Status_Help.new(@target_window.x, @target_window.height)
#~       @target_window.help_window = @target_win_help
#~       @target_win_help.visible = false
#~     end
#~     hide_target_window
#~   end
#~   #--------------------------------------------------------------------------
#~   # * Termination Processing
#~   # ** Aliased
#~   #--------------------------------------------------------------------------
#~   alias iex_hm_style_skill_terminate terminate unless $@
#~   def terminate
#~     iex_hm_style_skill_terminate
#~     if $imported["IEX_HM_Style_Menu_Status"]
#~       @target_win_help.dispose if @target_win_help != nil
#~     end
#~   end
#~   #--------------------------------------------------------------------------
#~   # * Show Target Window
#~   #     right : Right justification flag (if false, left justification)
#~   #--------------------------------------------------------------------------
#~   alias iex_hm_skill_show_target_window show_target_window unless $@
#~   def show_target_window(*args)
#~     iex_hm_skill_show_target_window(*args)
#~     if $imported["IEX_HM_Style_Menu_Status"]
#~       @target_win_help.x = @target_window.x
#~       @target_win_help.y = @target_window.height
#~       @target_win_help.visible = true 
#~     end
#~   end
#~   
#~   #--------------------------------------------------------------------------
#~   # * Hide Target Window
#~   #--------------------------------------------------------------------------
#~   alias iex_hm_skill_hide_target_window hide_target_window unless $@
#~   def hide_target_window(*args)
#~     iex_hm_skill_hide_target_window(*args)
#~     if $imported["IEX_HM_Style_Menu_Status"]
#~       @target_win_help.visible = false if @target_win_help != nil
#~     end
#~   end
#~   #--------------------------------------------------------------------------
#~   # * Update Target Selection
#~   #--------------------------------------------------------------------------
#~   alias iex_hm_skill_update_target_selection update_target_selection unless $@  
#~   def update_target_selection(*args)
#~     if $imported["IEX_HM_Style_Menu_Status"]
#~       @target_win_help.update
#~     end
#~     iex_hm_skill_update_target_selection(*args)
#~   end
#~   
#~ end
#~ end

