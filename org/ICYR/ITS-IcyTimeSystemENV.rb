# ITS - Icy Time System ~ENV~
#==============================================================================#
# ** ITS - Icy Time System ~ ENV Version
#------------------------------------------------------------------------------#
# ** Created by    : IceDragon
# ** Script-Status : Addon (ENV)
# ** Date Created  : 8/23/2010
# ** Date Modified : 9/01/2010
# ** Version       : 1.1
#------------------------------------------------------------------------------#
# This is a simple time system script
# It is not as fancy as Jets but it gets the job done
#------------------------------------------------------------------------------#
$imported = {} if $imported == nil
if $imported["IEX_Environment"]
  $its_time_on = IEX::ENVIRONMENT::TIME_SYSTEM
else
  $its_time_on = true
end

if $its_time_on
$imported["ICY_TimeSystem"] = true

module ICY
  module ITS
  # Ingame Variables *Note if you try to change the variables amount
  #                   The time will reset them back.
  # To change time call one of these from a script command
  #  Used to set the time specifically
  #   its_change_hour(new_hour) 
  #   its_change_minute(new_minute)
  #  Used to add time specifically
  #   its_advance_hour(add_hour)
  #   its_advance_minute(add_minute)
  #
  # 
  # Can I freeze time? YES
  # its_freeze_time(bool) bool being true or false
  # Time will not advance while this is true
  #
  # Can I remove the tint on the map temporary / I can't change the tone!
  # its_freeze_tone(bool) bool being true or false
  # The tone will not change while this is true
  #
  # Can I hide the Time Sprite?
  # its_visible_time(bool) bool being true or false
  # The time sprite will be hidden Note if you open the menu or a another scene
  # when you return to the Map the sprite will be shown again.
  #
    HOUR_VAR = 9 # Variable used to store the hour
    MINUTE_VAR = 10 # Variable used to store the minute
  
  # Time settings  
    HOURS_IN_DAY = 24 # How many hours in a day
    MINUTE_IN_HOUR = 60 # How many minutes in a hour
    
    MINUTE_LENGHT = 10 #60 # How many frames make a minute
    
    # Initial Time = [Hour, Minute]
    INITIAL_TIME = [19, 0]
    
    AM_TEXT = 'AM'
    PM_TEXT = 'PM' 
    
    DISPLAY_FORMAT = '%s:%2.2d %s' 
    DISPLAY_ALIGN = 2
    DISPLAY_FONT_SIZE = 18
    
    TIME_SPRITE_POS = [Graphics.width - 104, Graphics.height - 28, 9999]#[Graphics.width + 108, Graphics.height + 32, 9999]#[Graphics.width - 104, Graphics.height - 28, 9999]
    TIME_SPRITE_SIZE = [104, 28]
    TIME_SPRITE_SKIN = "Time_Skin"
    TIME_SPRITE_SKIN_OFF = [0, 2]
    TIME_SPRITE_SKIN_OPACITY = 255
    
    # Thanks to Jet for these
    DUSK_HOURS = [16, 17, 18, 19]
    NIGHT_HOURS = [20, 21, 22, 23, 24, 1, 2, 3]
    DAWN_HOURS = [4, 5, 6, 7, 8]
    DAY_HOURS = [9, 10, 11, 12, 13, 14, 15]
    # This is the tint that will be applied for dusk.
    DUSK_TINT = Tone.new(17, -51, -102, 0)
    # This is the tint that will be applied for night.
    NIGHT_TINT = Tone.new(-187, -119, -17, 68)
    # This is the tint that will be applied for dawn.
    DAWN_TINT = Tone.new(17, -51, -102, 0)
    # This is the tint that will be applied for day.
    DAY_TINT = Tone.new(0, 0, 0, 0)    
    # The switch that will turn on when it is Dusk.
    DUSK_SWITCH = 10
    # The switch that will turn on when it is Night.
    NIGHT_SWITCH = 11
    # The switch that will turn on when it is Dawn.
    DAWN_SWITCH = 12
    # The switch that will turn on when it is Day.
    DAY_SWITCH = 13
    
    FADE_LENGTH = 120
    
  #--------------------------------------------------------
  # ** Phase icons * These are shown on the time sprite
  #--------------------------------------------------------
    USE_PHASE_ICONS = true
    PHASE_ICON_POS = [6, 0]
    PHASE_WAIT = 60 # When changing tints how long should it be
    DUSK_ICON  = 2584
    NIGHT_ICON = 2581
    DAWN_ICON  = 2583
    DAY_ICON   = 2582
    
    # This will add compatability with Thomas Edison VX the same as if you were
    # using KGC's day/night.
    # Meaning, lights will be on during night and off during day.
    THOMAS_EDISON_COMPATABILITY = true
   
  # So you can set your own tags which show if the map is indoors or not
  # Note that anything in this array is counted as indoor tags
  # [INDOOR]Mapname or Mapname[INDOOR] will work 
  # **NOTE Don't put any quotes in the map name Tag also enclose it in the []
    INDOOR_TAGS = ["JDN", "INDOOR", "WDN", "TDN"]
  end
end

#==============================================================================
# ** Game_Interpreter
#------------------------------------------------------------------------------
#  These are the Game Interpreter commands for ITS
#==============================================================================
class Game_Interpreter
  
  def its_change_hour(hour = $game_system.its_game_time.hour)
    $game_system.its_game_time.hour = hour
  end
  
  def its_advance_hour(hour = 0)
    $game_system.its_game_time.hour += hour
  end
  
  def its_change_minute(minute = $game_system.its_game_time.minute)
    $game_system.its_game_time.minute = minute
  end
  
  def its_advance_minute(minute = 0)
    $game_system.its_game_time.minute += minute
  end
  
  def its_time_access
    return $game_system.its_game_time
  end
  
  def its_freeze_time(bool = false)
    $game_system.its_game_time.freeze_time = bool
  end
  
  def its_freeze_tone(bool = false)
    $game_system.its_game_time.freeze_tone = bool
  end
  
  def its_visible_time(bool = false)
    return if $scene.is_a?(Scene_Map) == false
    $scene.visible_time_sprite(bool)
    $game_system.its_game_time.hide_time = !bool
  end
  
end

#==============================================================================
# ** Bitmap
#------------------------------------------------------------------------------
#==============================================================================
class Bitmap
  
  def draw_icon(x, y, icon_index, enabled = true)
    bitmap = Cache.system("Iconset")
    rect = Rect.new(icon_index % 16 * 24, icon_index / 16 * 24, 24, 24)
    self.blt(x, y, bitmap, rect, enabled ? 255 : 128)
  end
  
end

#==============================================================================
# ** RPG::MapInfo
#------------------------------------------------------------------------------
#==============================================================================
class RPG::MapInfo
  
  alias its_rpg_mapinfo_name name unless $@
  def name
    prename = its_rpg_mapinfo_name#@name.clone
    for tag in ICY::ITS::INDOOR_TAGS
      prename = prename.gsub(/\[#{tag}\]/) {""}
    end
    return prename
  end
  
  def indoors?
    arra = []
    prename = @name.clone
    for tag in ICY::ITS::INDOOR_TAGS
       arra.push(prename.scan(/\[#{tag}\]/).size > 0)
    end
    return arra.any?
  end
  
end

#==============================================================================
# ** Game_Map
#------------------------------------------------------------------------------
#  This class handles maps. It includes scrolling and passage determination
# functions. The instance of this class is referenced by $game_map.
#==============================================================================
class Game_Map
  
  #--------------------------------------------------------------------------
  # * Setup
  #     map_id : map ID
  #--------------------------------------------------------------------------
  alias its_gmp_setup setup unless $@
  def setup(*args)
    its_gmp_setup(*args)
    if $game_system.its_game_time != nil
      $game_system.its_game_time.update_tone
    end  
  end
  
end

#==============================================================================
# ** Game_System
#------------------------------------------------------------------------------
#  This class handles system-related data. Also manages vehicles and BGM, etc.
# The instance of this class is referenced by $game_system.
#==============================================================================
class Game_System 
  
  attr_accessor :its_game_time
  
  #--------------------------------------------------------------------------
  # * Object Initialization
  #--------------------------------------------------------------------------
  alias its_gs_initialize initialize unless $@
  def initialize(*args)
    its_gs_initialize(*args)
    @its_game_time = ITS_Game_Time.new
  end
  
  #--------------------------------------------------------------------------
  # * Frame Update
  #--------------------------------------------------------------------------
  alias its_gs_update update unless $@
  def update(*args)
    its_gs_update(*args)
    if @its_game_time == nil
      @its_game_time = ITS_Game_Time.new
    end
    if $scene.is_a?(Scene_Map)
      if @its_game_time.started 
        @its_game_time.update if $BTEST == false
      else
        @its_game_time.start_time_system if $BTEST == false
      end
    end  
  end
  
end

#==============================================================================
# ** ITS_Game_Time
#------------------------------------------------------------------------------
# This class is attached to the Game_System
#  It manages time
#==============================================================================
class ITS_Game_Time
  
  include ICY::ITS
  
  attr_accessor :started
  attr_accessor :halfday # AM or PM
  attr_accessor :hour_time # Limited to Half of the HOUR
  attr_accessor :display_format
  attr_accessor :hour
  attr_accessor :minute
  attr_accessor :phase_icon
  attr_accessor :indoors
  attr_accessor :updated_tone
  attr_accessor :freeze_time
  attr_accessor :freeze_tone
  attr_accessor :hide_time
  #--------------------------------------------------------------------------
  # * Object Initialization
  #--------------------------------------------------------------------------
  def initialize
    @started = false
    @display_format = DISPLAY_FORMAT
    @hour = INITIAL_TIME[0]
    @minute = INITIAL_TIME[1]
    @freeze_time = false
    @freeze_tone = false
    @hide_time = false
    @map_infos = load_data("Data/MapInfos.rvdata")
  end
  
  #--------------------------------------------------------------------------
  # * Start Time System
  #--------------------------------------------------------------------------
  def start_time_system
    @started = true
    @old_tone = $game_map.screen.tone
    @old_time_of_day = time_of_day
    @updated_tone = false
    @wait_update = 0
    $game_variables[HOUR_VAR] = @hour
    $game_variables[MINUTE_VAR] = @minute
    @map_infos = load_data("Data/MapInfos.rvdata")
    @indoors = @map_infos[$game_map.map_id].indoors?
    @old_map_id = $game_map.map_id
    set_half_day
    set_new_time_of_day
  end
  
  #--------------------------------------------------------------------------
  # * Change Time Switches
  #--------------------------------------------------------------------------
  def change_time_variables(dusk, night, dawn, day)
    $game_switches[DUSK_SWITCH]  = dusk
    $game_switches[NIGHT_SWITCH] = night
    $game_switches[DAWN_SWITCH]  = dawn
    $game_switches[DAY_SWITCH]   = day
  end
  
  #--------------------------------------------------------------------------
  # * Changes time of day
  #--------------------------------------------------------------------------
  def set_new_time_of_day
    time_o_day = time_of_day
    case time_o_day
    when 0
      change_time_variables(true, false, false, false)
      $light_switch = true
      @phase_icon = DUSK_ICON
    when 1
      change_time_variables(false, true, false, false)
      $light_switch = true
      @phase_icon = NIGHT_ICON
    when 2
      change_time_variables(false, false, true, false)
      $light_switch = false
      @phase_icon = DAWN_ICON
    when 3
      change_time_variables(false, false, false, true)
      $light_switch = false
      @phase_icon = DAY_ICON
    end
    if @old_time_of_day != time_o_day
      @updated_tone = false
      $game_map.need_refresh = true
      @old_time_of_day = time_o_day
    end
  end
  
  #--------------------------------------------------------------------------
  # * Time of day
  #--------------------------------------------------------------------------  
  def time_of_day
    if DUSK_HOURS.include?(@hour)
      return 0
    elsif NIGHT_HOURS.include?(@hour)
      return 1
    elsif DAWN_HOURS.include?(@hour)
      return 2
    elsif DAY_HOURS.include?(@hour)
      return 3
    end
  end
  
  #--------------------------------------------------------------------------
  # * Set Half Day * Used for getting the AM and PM
  #-------------------------------------------------------------------------- 
  def set_half_day
    if @hour == HOURS_IN_DAY
      @halfday = AM_TEXT
      @hour_time = @hour - (HOURS_IN_DAY / 2)
    elsif @hour == (HOURS_IN_DAY / 2)
      @halfday = PM_TEXT
      @hour_time = @hour
    elsif @hour > (HOURS_IN_DAY / 2)
      @halfday = PM_TEXT
      @hour_time = @hour - (HOURS_IN_DAY / 2)
    elsif @hour < (HOURS_IN_DAY / 2)
      @halfday = AM_TEXT
      @hour_time = @hour     
    end
  end
  
  #--------------------------------------------------------------------------
  # * Game Screen * Used for getting the game screen
  #--------------------------------------------------------------------------   
  def screen
    if $game_temp.in_battle
      return $game_troop.screen
    else
      return $game_map.screen
    end
  end
  
  #--------------------------------------------------------------------------
  # * Frame Update
  #-------------------------------------------------------------------------- 
  def update
    return if @started == false
    update_time
    update_tone
  end

  #--------------------------------------------------------------------------
  # * Update Time
  #--------------------------------------------------------------------------
  def update_time
    return if @freeze_time
     if @wait_update == MINUTE_LENGHT
       @minute = (@minute + 1) % (MINUTE_IN_HOUR + 1)
       if @minute == MINUTE_IN_HOUR 
         @hour = (@hour + 1) % (HOURS_IN_DAY + 2) 
         @minute = 0
       end
       if @hour == (HOURS_IN_DAY + 1)
          @hour = 1
       end
       set_half_day
       set_new_time_of_day
       @wait_update = 0
       $game_variables[HOUR_VAR] = @hour
       $game_variables[MINUTE_VAR] = @minute
      else
     @wait_update += 1
    end
  end

  #--------------------------------------------------------------------------
  # * Update Tone
  #--------------------------------------------------------------------------
  def update_tone
    return if $game_map == nil
    return if @freeze_tone
    if @old_map_id != $game_map.map_id
      @updated_tone = false
      @old_map_id = $game_map.map_id
      fade_lenght = 1
      set_new_time_of_day
    else
      fade_lenght = FADE_LENGTH
    end
    if @map_infos[$game_map.map_id] != nil
      @indoors = @map_infos[$game_map.map_id].indoors?
    else
      @indoors = false
    end      
    time_o_day = time_of_day
    if @indoors
      def_tone = Tone.new(0, 0, 0, 0)
      return if @updated_tone
      screen.start_tone_change(def_tone, fade_lenght)
      @old_tone = def_tone
      @updated_tone = true
    else
    case time_o_day
    when 0 
      return if @old_tone == DUSK_TINT or @updated_tone
      screen.start_tone_change(DUSK_TINT, fade_lenght)
      @old_tone = DUSK_TINT
      @updated_tone = true
    when 1
      return if @old_tone == NIGHT_TINT or @updated_tone
      screen.start_tone_change(NIGHT_TINT, fade_lenght)
      @old_tone = NIGHT_TINT
      @updated_tone = true
    when 2
      return if @old_tone == DAWN_TINT or @updated_tone
      screen.start_tone_change(DAWN_TINT, fade_lenght)
      @old_tone = DAWN_TINT
      @updated_tone = true
    when 3
      return if @old_tone == DAY_TINT or @updated_tone
      screen.start_tone_change(DAY_TINT, fade_lenght)
      @old_tone = DAY_TINT
      @updated_tone = true
    end
  end
end

  
end

#==============================================================================
# ** Scene_Map
#------------------------------------------------------------------------------
#  This class performs the map screen processing.
#==============================================================================
class Scene_Map < Scene_Base
  
  #--------------------------------------------------------------------------
  # * Object Initialization
  #--------------------------------------------------------------------------
  alias its_sm_initialize initialize unless $@
  def initialize
    its_sm_initialize
    $game_system.its_game_time.update_tone
    @time_sprite = []
    @time_sprite[0] = ::Sprite.new
    if ICY::ITS::TIME_SPRITE_SKIN == nil
      @time_sprite[0].bitmap = Bitmap.new(ICY::ITS::TIME_SPRITE_SIZE[0], ICY::ITS::TIME_SPRITE_SIZE[1])
      rect = Rect.new(0, 0, ICY::ITS::TIME_SPRITE_SIZE[0], ICY::ITS::TIME_SPRITE_SIZE[1])
      color1 = Color.new(0, 0, 0, 128)
      color2 = Color.new(255, 255, 255, 128)
      @time_sprite[0].bitmap.gradient_fill_rect(rect, color1, color2)
    else
      @time_sprite[0].bitmap = Cache.system(ICY::ITS::TIME_SPRITE_SKIN)
      @time_sprite[0].ox = ICY::ITS::TIME_SPRITE_SKIN_OFF[0]
      @time_sprite[0].oy = ICY::ITS::TIME_SPRITE_SKIN_OFF[1]
      @time_sprite[0].opacity = ICY::ITS::TIME_SPRITE_SKIN_OPACITY
    end  
    @time_sprite[1] = ::Sprite.new
    @time_sprite[1].bitmap = Bitmap.new(ICY::ITS::TIME_SPRITE_SIZE[0], ICY::ITS::TIME_SPRITE_SIZE[1])
    @time_sprite[2] = ::Sprite.new
    @time_sprite[2].bitmap = Bitmap.new(ICY::ITS::TIME_SPRITE_SIZE[0], ICY::ITS::TIME_SPRITE_SIZE[1])
    @old_minute = 0 
    @old_hour = 0
    @first_phaseing = true
    @phase_in = false
    @phase_out = false
    coun = 0
    for spr in @time_sprite
      next if spr == nil
      spr.x = ICY::ITS::TIME_SPRITE_POS[0]
      spr.y = ICY::ITS::TIME_SPRITE_POS[1]
      spr.z = ICY::ITS::TIME_SPRITE_POS[2] + coun
      coun += 1
    end
    update_time_sprite
    visible_time_sprite(false)
  end
  
  #--------------------------------------------------------------------------
  # * Alias Start 
  #--------------------------------------------------------------------------
  alias its_sm_start start unless $@
  def start(*args)
    its_sm_start(*args)
    visible_time_sprite(true)
  end
    
  #--------------------------------------------------------------------------
  # * Visible Time Sprite
  #     bool : Boolean 
  #--------------------------------------------------------------------------
  def visible_time_sprite(bool = false)
    for spr in @time_sprite
     next if spr == nil
       spr.visible = bool
    end
  end

  #--------------------------------------------------------------------------
  # * Dispose Time Sprite
  #--------------------------------------------------------------------------
  def dispose_time_sprite
    for spr in @time_sprite
      next if spr == nil
        spr.dispose
        spr = nil
      end
      @time_sprite.clear
      @time_sprite = []
  end

  #--------------------------------------------------------------------------
  # * Termination Processing
  #--------------------------------------------------------------------------  
  alias its_sm_terminate terminate unless $@
  def terminate
    dispose_time_sprite
    its_sm_terminate
  end

  #--------------------------------------------------------------------------
  # * Frame Update
  #--------------------------------------------------------------------------  
  alias its_sm_update update unless $@
  def update
    its_sm_update
    if $game_message.visible
      visible_time_sprite(false)
    elsif $game_system.its_game_time.hide_time
      visible_time_sprite(false)
    else
      visible_time_sprite(true)
      update_time_sprite
    end
  end

  #--------------------------------------------------------------------------
  # * Update Time Sprite
  #--------------------------------------------------------------------------  
  def update_time_sprite
    return if @time_sprite == nil
    update_phase_icon if ICY::ITS::USE_PHASE_ICONS
    return if @old_minute == $game_system.its_game_time.minute and @old_hour == $game_system.its_game_time.hour
     @time_sprite[1].bitmap.clear
     hour = $game_system.its_game_time.hour_time
     minute = $game_system.its_game_time.minute
     time_o_day = $game_system.its_game_time.halfday
     display_format = ICY::ITS::DISPLAY_FORMAT
     time = sprintf(display_format, hour, minute, time_o_day)
     width = @time_sprite[1].bitmap.width - (@time_sprite[1].bitmap.width / 16) - 4
     height = @time_sprite[1].bitmap.height
     align = ICY::ITS::DISPLAY_ALIGN
     text_size = ICY::ITS::DISPLAY_FONT_SIZE
     @time_sprite[1].bitmap.font.size = text_size
     @time_sprite[1].bitmap.font.color = Color.new(255, 255, 255, 255)
     @time_sprite[1].bitmap.draw_text(0, 0, width, height, time, align)
     @old_minute = minute
     @old_hour = $game_system.its_game_time.hour
  end

  #--------------------------------------------------------------------------
  # * Update Phase Icon
  #-------------------------------------------------------------------------- 
  def update_phase_icon
    phase_icon = $game_system.its_game_time.phase_icon
    case ICY::ITS::DISPLAY_ALIGN
    when 0
      icon_x = (@time_sprite[2].bitmap.width - 24)
    when 1
      icon_x = 0 + ICY::ITS::PHASE_ICON_POS[0]
    when 2  
      icon_x = 0 + ICY::ITS::PHASE_ICON_POS[0]
    end
    icon_y = 0 + ICY::ITS::PHASE_ICON_POS[1]
      if @first_phaseing
        @time_sprite[2].bitmap.clear
        @time_sprite[2].bitmap.draw_icon(icon_x, icon_y, phase_icon)
        @time_sprite[2].opacity = 0
        @first_phaseing = false
        @phase_in = true
        @phase_out = false
        @old_phase_icon = phase_icon      
     elsif @phase_out
        @time_sprite[2].opacity -= 255 / ICY::ITS::PHASE_WAIT
       if @time_sprite[2].opacity == 0
         @time_sprite[2].bitmap.clear
         @time_sprite[2].bitmap.draw_icon(icon_x, icon_y, phase_icon)
         @time_sprite[2].opacity = 0
         @phase_out = false
         @phase_in = true
       end     
     elsif @phase_in
       @time_sprite[2].opacity += 255 / ICY::ITS::PHASE_WAIT
       if @time_sprite[2].opacity == 255
         @phase_in = false
         @phase_out = false
       end     
     elsif @old_phase_icon != phase_icon       
       @phase_out = true
       @phase_in = false
       @old_phase_icon = phase_icon
     end
   end
   
end

#==============================================================================
# ** Spriteset_Map
#------------------------------------------------------------------------------
#  This class brings together map screen sprites, tilemaps, etc. It's used
# within the Scene_Map class.
#==============================================================================
class Spriteset_Map
  
  if ICY::ITS::THOMAS_EDISON_COMPATABILITY and !$bulletxt_day_check.nil?
    alias its_spm_update_light_effects update_light_effects unless $@
    def update_light_effects(*args)
      if $light_switch #|| $game_time.map_infos[$game_map.map_id].indoors?
        its_spm_update_light_effects(*args)
      else
        for effect in @light_effects
          effect.light.visible = false
        end
      end
    end
  end
  
end
end
