# #~IEX - Colosseum Wip
#~ module ICY
#~   module REGEXP
#~     module COLOSSEUM
#~        COLOSSEUM_USE = /<(?:COLOSSEUM)>/i # Item for Colosseum?
#~        TROOP_MON = /<(?:TROOP_MON|troop mon)[ ]*(\d+)>/i # Monster to add to troop
#~      end
#~    end
#~  end
#~  $colosseum_battle = false
#~  $colosseum_item = nil
#~  
#~ class RPG::BaseItem

#~   alias icy_col_basit_initialize initialize unless $@
#~   def initialize(*args)
#~     icy_col_basit_initialize(*args)
#~     @col_cache_ready = false
#~     icy_col_basit_cache
#~   end
#~   
#~   def icy_col_basit_cache
#~     @colosseum = false
#~     @troop_mons = []
#~     self.note.split(/[\r\n]+/).each { |line|
#~      case line
#~      when ICY::REGEXP::COLOSSEUM::COLOSSEUM_USE
#~        @colosseum = true
#~      when ICY::REGEXP::COLOSSEUM::TROOP_MON
#~        @troop_mons.push($1.to_i)
#~      end
#~      } # End each
#~      @col_cache_ready = true
#~   end

#~   def colosseum_use?
#~     icy_col_basit_cache unless @col_cache_ready 
#~     return @colosseum
#~   end

#~   def colosseum_troop
#~     icy_col_basit_cache unless @col_cache_ready 
#~     return @troop_mons
#~   end
#~   
#~ end

#~ class Game_Troop < Game_Unit
#~   
#~   def setup_colosseum_troop(item)
#~     clear
#~     @enemies = []
#~     item_enemy_ids = item.colosseum_troop
#~     for eny_id in item_enemy_ids
#~       next if $data_enemies[eny_id] == nil
#~       enemy = Game_Enemy.new(@enemies.size, eny_id)
#~       # This part you will have to figure out
#~       enemy.screen_x = 0 #
#~       enemy.screen_y = 0
#~       # Then we simplay push our newly created enemy into the array
#~       @enemies.push(enemy)
#~     end
#~     make_unique_names
#~   end
#~   
#~ end

#~ class Game_Interpreter
#~   
#~   #--------------------------------------------------------------------------
#~   # * Battle Processing
#~   #--------------------------------------------------------------------------
#~   def command_301
#~     return true if $game_temp.in_battle
#~     if @params[0] == 0                      # Direct designation
#~       troop_id = @params[1]
#~     else                                    # Designation with variables
#~       troop_id = $game_variables[@params[1]]
#~     end
#~     if $colosseum_battle == true and $colosseum_item != nil
#~         $game_troop.setup_colosseum_troop($colosseum_item)
#~         $game_troop.can_escape = false
#~         $game_troop.can_lose = true
#~         $game_temp.battle_proc = Proc.new { |n| @branch[@indent] = n }
#~         $game_temp.next_scene = "battle"
#~     else
#~       if $data_troops[troop_id] != nil
#~         $game_troop.setup(troop_id)
#~         $game_troop.can_escape = @params[2]
#~         $game_troop.can_lose = @params[3]
#~         $game_temp.battle_proc = Proc.new { |n| @branch[@indent] = n }
#~         $game_temp.next_scene = "battle"
#~       end
#~     end
#~   
#~     @index += 1
#~     return false
#~   end
#~   
#~ end

