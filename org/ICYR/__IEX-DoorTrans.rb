# #~IEX - Door Trans
#~ $imported = {} if $imported == nil
#~ $imported["IEX_Door_Transition"] = true

#~ class Game_Character
#~   
#~   alias iex_door_trans_initialize initialize unless $@
#~   def initialize(*args)
#~     iex_door_trans_initialize(*args) 
#~     @door_trans_in = false
#~     @door_trans_out = false
#~   end
#~   
#~   alias iex_door_trans_update update unless $@
#~   def update(*args)
#~     if @door_trans_in
#~       update_door_trans(0)
#~     elsif @door_trans_out
#~       update_door_trans(1)
#~     end  
#~     iex_door_trans_update(*args)
#~   end
#~   
#~   def update_door_trans(mode)
#~     if mode == 0
#~       @door_trans_out = false
#~       @opacity += 255 / 60
#~       if @opacity == 255
#~         @door_trans_in = false
#~       end  
#~     elsif mode == 1
#~       @door_trans_in = false
#~       @opacity -= 255 / 60
#~       if @opacity == 0
#~         @door_trans_out = false
#~       end  
#~     end  
#~   end
#~   
#~   def door_trans(mode = 2)
#~     if mode == 0
#~       @door_trans_in = true
#~       update_door_trans(0)
#~     elsif mode == 1
#~       @door_trans_out = true
#~       update_door_trans(1)
#~     elsif mode == 2
#~       @door_trans_out = false
#~       @door_trans_in = false
#~       @opacity = 255
#~     end
#~   end
#~   
#~ end
