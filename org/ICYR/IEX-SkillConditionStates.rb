# IEX - Skill Condition States
#~ #==============================================================================#
#~ # ** IEX(Icy Engine Xelion) - Skill Conditional States
#~ #------------------------------------------------------------------------------#
#~ # ** Created by    : IceDragon
#~ # ** Script-Status : Addon (Item and Skill)
#~ # ** Script Type   : State Modifier
#~ # ** Date Created  : 10/29/2010
#~ # ** Date Modified : 10/29/2010
#~ # ** Requested By  : Deus Ex Procella
#~ # ** Version       : 1.0
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** INTRODUCTION
#~ #------------------------------------------------------------------------------#
#~ # >.> Okay so this script adds a ... can't call it new...
#~ # Anyway this adds a conditional states effect to skills and items.. 
#~ # How this works is, if the target has certain state or states, you can remove
#~ # those to put a different set of states.
#~ # 
#~ # Deus Ex Procella:
#~ # Enemy is frozen and player uses Fire. Instead of inflicting "Burn", 
#~ #  it thaws the enemy and inflicts "Soaked". 
#~ #
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** FEATURES
#~ #------------------------------------------------------------------------------#
#~ # V1.0
#~ #  Notetags! Can be placed in Skill and Item noteboxes
#~ #------------------------------------------------------------------------------#
#~ #  <iex required state> or <iex required states> or
#~ #  <iex required state: id, id, id> or <iex required states: id, id, id>
#~ #  This will allow you to create a condition under which the state will work..
#~ #  <iex required state> this is a non condition requirement.
#~ #  Therefore the state effects will be applied regardless of the situtaion.
#~ #  On the other hand <iex required state: id, id, id>
#~ #  Will require the states marked by Id in order for the effects to be applied
#~ #------------------------------------------------------------------------------#
#~ # **Note this tag will be voided if the required state tag was not used.
#~ #  iex add state: id, id, id or iex add states: id, id, id 
#~ #  This will add the state(s) marked by id.
#~ #------------------------------------------------------------------------------#
#~ # **Note this tag will be voided if the required state tag was not used.
#~ #  iex remove state: id, id, id or iex remove states: id, id, id
#~ #  This will remove the state(s) marked by id.
#~ #------------------------------------------------------------------------------#
#~ #  </iex required state> or </iex required states>
#~ #  This closes the required tags
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** HOW TO USE
#~ #------------------------------------------------------------------------------#
#~ #  >.> Do I have to write this....
#~ #
#~ #  Anyway in an item / skill notebox do something like this..
#~ # **Note anything marked with ~ is compulsory
#~ #
#~ #  <iex required state> ~~
#~ #   iex add state: 2
#~ #   iex remove state: 1
#~ #  </iex required state> ~~
#~ #  
#~ #  This effect would add state 2 (Poison) and Remove State 1 (Incapacitated)
#~ #  Regardless.
#~ #
#~ #  <iex required state: 1> ~~
#~ #   iex add state: 122
#~ #   iex remove state: 1
#~ #  </iex required state> ~~
#~ # 
#~ #  This effect would add state 122 (Zombied) and Remove State 1 (Incapacitated)
#~ #  This requires state 1 (Incapacitated).
#~ #
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** CHANGE LOG
#~ #------------------------------------------------------------------------------#
#~ # 
#~ #  10/29/2010 - V1.0 Finished Script
#~ #
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** KNOWN ISSUES
#~ #------------------------------------------------------------------------------#
#~ #  Non at the moment. It works with YEM and its suppose to work with the normal
#~ #  BS, probably Takentai and GTBS...(Don't ask me I'm not sure about the last 2!!)
#~ #
#~ #------------------------------------------------------------------------------#
#~ $imported = {} if $imported == nil
#~ $imported["IEX_Skill_Condition_States"] = true
#~ #==============================================================================
#~ # ** IEX::REGEXP::CONDITIONAL_STATES
#~ #------------------------------------------------------------------------------
#~ #==============================================================================
#~ module IEX
#~   module REGEXP
#~     module CONDITION_STATES
#~       ADD_STATES         = /(?:IEX_ADD_STATE|iex add state)s?:[ ]*(\d+(?:\s*,\s*\d+)*)/i
#~       REMOVE_STATES      = /(?:IEX_REMOVE_STATE|iex remove state)s?:[ ]*(\d+(?:\s*,\s*\d+)*)/i
#~       REQUIRED_STATES_ON = /<(?:IEX_REQUIRED_STATE|iex required state)s?(?::?:[ ]*(\d+(?:\s*,\s*\d+)*))*>/i
#~       REQUIRED_STATES_OFF= /<\/(?:IEX_REQUIRED_STATE|iex required state)s?>/i
#~     end
#~   end
#~ end
#~ #==============================================================================
#~ # ** RPG::BaseItem
#~ #------------------------------------------------------------------------------
#~ #==============================================================================
#~ class RPG::BaseItem
#~   
#~   alias iex_condition_states_rpgbi_initialize initialize unless $@
#~   def initialize(*args)
#~     iex_condition_states_rpgbi_initialize(*args)
#~     iex_condition_states_cache
#~   end
#~   
#~   def iex_condition_states_cache
#~     @iex_required_states = {}
#~     @in_req_state = false
#~     @key_a = []
#~     self.note.split(/[\r\n]+/).each { |line| 
#~     case line
#~     when IEX::REGEXP::CONDITION_STATES::REQUIRED_STATES_ON
#~       @in_req_state = true
#~       @key_a = []
#~       if $1 != nil
#~         $1.scan(/\d+/).each { |sta_id|
#~         @key_a.push(sta_id.to_i) }
#~       end  
#~       @iex_required_states[@key_a.clone] = {:remove_states => [], :add_states => []}
#~     when IEX::REGEXP::CONDITION_STATES::REQUIRED_STATES_OFF
#~       @in_req_state = false
#~       @key_a = nil
#~     when IEX::REGEXP::CONDITION_STATES::ADD_STATES
#~       next unless @in_req_state
#~       $1.scan(/\d+/).each { |sta_id|
#~       @iex_required_states[@key_a][:add_states].push(sta_id.to_i) }
#~     when IEX::REGEXP::CONDITION_STATES::REMOVE_STATES
#~       next unless @in_req_state
#~       $1.scan(/\d+/).each { |sta_id|
#~       @iex_required_states[@key_a][:remove_states].push(sta_id.to_i) }
#~     end
#~     }
#~     @in_req_state = false
#~     @key_a = nil
#~   end
#~    
#~   def iex_required_states
#~     iex_condition_states_cache if @iex_required_states == nil
#~     return @iex_required_states
#~   end
#~   
#~ end

#~ #==============================================================================
#~ # ** Game_Battler
#~ #------------------------------------------------------------------------------
#~ #==============================================================================
#~ class Game_Battler
#~   
#~   #--------------------------------------------------------------------------
#~   # * Apply State Changes
#~   #     obj : Skill, item, or attacker
#~   #--------------------------------------------------------------------------
#~   alias iex_condition_states_apply_state_changes apply_state_changes unless $@
#~   def apply_state_changes(obj)
#~     iex_condition_states_apply_state_changes(obj)
#~     if obj.kind_of?(RPG::BaseItem)
#~       iex_swap_states(obj)
#~     end  
#~   end
#~   
#~   def iex_swap_states(obj)
#~     reque = obj.iex_required_states
#~     for req_sta in reque.keys
#~       req_ret = false
#~       for sta_id in req_sta
#~         next if sta_id == nil
#~         unless @states.include?(sta_id)
#~           req_ret = true 
#~           break
#~         end
#~       end  
#~       return if req_ret
#~       rem_sta = reque[req_sta][:remove_states]
#~       add_sta = reque[req_sta][:add_states]
#~       for rem_sta_id in rem_sta
#~         next if rem_sta_id == nil
#~         remove_state(rem_sta_id)
#~         @removed_states.delete(rem_sta_id)
#~       end  
#~       for add_sta_id in add_sta
#~         next if add_sta_id == nil
#~         add_state(add_sta_id)
#~       end
#~     end
#~   end
#~   
#~ end
#~ #==============================================================================
#~ # ** END OF FILE
#~ #------------------------------------------------------------------------------
