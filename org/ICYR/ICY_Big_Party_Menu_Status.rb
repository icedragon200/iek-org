# #~ICY_Big_Party_Menu_Status
#~ #==============================================================================#
#~ #=----------------------------------------------------------------------------=#
#~ #= **ICY Big Party Menu Status **                                             =#
#~ #= **Created by : IceDragon **                                                =#
#~ #= **Script-Status : ReWrite  **                                              =#
#~ #= **Date Created : 8/30/2010 **                                              =#
#~ #= **Date Modified : 8/30/2010 **                                             =#
#~ #= **Version : 1.0 **                                                         =#
#~ #=----------------------------------------------------------------------------=#
#~ #==============================================================================
#~ # ** Window_MenuStatus
#~ #------------------------------------------------------------------------------
#~ #  This window displays party member status on the menu screen.
#~ #==============================================================================
#~ class Window_MenuStatus < Window_Selectable
#~   
#~   #--------------------------------------------------------------------------
#~   # * Object Initialization
#~   #     x : window X coordinate
#~   #     y : window Y coordinate
#~   #--------------------------------------------------------------------------
#~   def initialize(x, y)
#~     @item_sq_spacing = 96
#~     @selection_size = 96
#~     @rect_size = 86
#~     super(x, y, 384, 416)
#~     @column_max = 1
#~     refresh
#~     self.active = false
#~     self.index = -1
#~   end
#~   
#~   #--------------------------------------------------------------------------
#~   # * Create Window Contents
#~   #--------------------------------------------------------------------------
#~   def create_contents
#~     self.contents.dispose
#~     maxbitmap = 8192
#~     dw = [width - 32, maxbitmap].min
#~     dh = [[height - 32, row_max * @item_sq_spacing].max, maxbitmap].min
#~     bitmap = Bitmap.new(dw, dh)
#~     self.contents = bitmap
#~   end
#~   
#~   #--------------------------------------------------------------------------
#~   # * Refresh
#~   #--------------------------------------------------------------------------
#~   def refresh
#~     @item_max = $game_party.members.size
#~     self.contents.clear
#~     create_contents
#~     for actor in $game_party.members
#~       draw_actor_face(actor, 2, actor.index * 96 + 2, 92)
#~       draw_actor_graphic(actor, 34, actor.index * 96 + 92)
#~       
#~       x = 104
#~       y = actor.index * 96 + WLH / 2      
#~       draw_actor_name(actor, x, y)
#~       draw_actor_class(actor, x + 120, y)
#~       draw_actor_level(actor, x, y + WLH * 1)
#~       draw_actor_state(actor, x, y + WLH * 2)
#~       draw_actor_hp(actor, x + 120, y + WLH * 1)
#~       draw_actor_mp(actor, x + 120, y + WLH * 2)
#~     end
#~   end
#~   
#~   #--------------------------------------------------------------------------
#~   # * Get Top Row
#~   #--------------------------------------------------------------------------
#~   def top_row
#~     return self.oy / @item_sq_spacing#WLH
#~   end
#~   
#~   #--------------------------------------------------------------------------
#~   # * Set Top Row
#~   #     row : row shown on top
#~   #--------------------------------------------------------------------------
#~   def top_row=(row)
#~     row = 0 if row < 0
#~     row = row_max - 1 if row > row_max - 1
#~     self.oy = row * @item_sq_spacing#WLH
#~   end
#~   
#~   #--------------------------------------------------------------------------
#~   # * Get Number of Rows Displayable on 1 Page
#~   #--------------------------------------------------------------------------
#~   def page_row_max
#~     return (self.height / @item_sq_spacing) 
#~   end
#~   
#~   #--------------------------------------------------------------------------
#~   # * Get Number of Items Displayable on 1 Page
#~   #--------------------------------------------------------------------------
#~   def page_item_max
#~     return page_row_max * @column_max #+ @item_sq_spacing
#~   end
#~   
#~   #--------------------------------------------------------------------------
#~   # * Move cursor one page down
#~   #--------------------------------------------------------------------------
#~   def cursor_pagedown
#~     if top_row + page_row_max < row_max
#~       @index = [@index + page_item_max, @item_max - 1].min
#~       self.top_row += page_row_max - @item_sq_spacing 
#~     end
#~   end
#~   
#~   #--------------------------------------------------------------------------
#~   # * Move cursor one page up
#~   #--------------------------------------------------------------------------
#~   def cursor_pageup
#~     if top_row > 0
#~       @index = [@index - page_item_max, 0].max
#~       self.top_row -= page_row_max - @item_sq_spacing 
#~     end
#~   end
#~   
#~   #--------------------------------------------------------------------------
#~   # * Update cursor
#~   #--------------------------------------------------------------------------
#~   def update_cursor
#~     if @index < 0                   # If the cursor position is less than 0
#~       self.cursor_rect.empty        # Empty cursor
#~     else                            # If the cursor position is 0 or more
#~       row = @index / @column_max    # Get current row
#~       if row < top_row              # If before the currently displayed
#~         self.top_row = row          # Scroll up
#~       end
#~       if row > bottom_row           # If after the currently displayed
#~         self.bottom_row = row       # Scroll down
#~       end
#~        y_l = @index / @column_max
#~        y_pos = ((@item_sq_spacing * y_l)- self.oy) #+ off_y
#~        x_l = self.index - (@column_max * y_l)
#~        x_pos = (x_l * @item_sq_spacing) #+ off_x
#~        height = @selection_size
#~        width  = self.contents.width #@selection_size
#~        if @selection_size > @rect_size
#~          subitive = (@selection_size - @rect_size) / 2
#~          self.cursor_rect.set(x_pos.to_i - subitive, y_pos.to_i - subitive, width, height)
#~        elsif @selection_size < @rect_size
#~          additive = (@rect_size - @selection_size) / 2
#~          self.cursor_rect.set(x_pos.to_i + additive, y_pos.to_i + additive, width, height)
#~         else
#~         self.cursor_rect.set(x_pos.to_i, y_pos.to_i, width, height)
#~        end
#~     end
#~   end
#~   
#~ end
#~   
