# IEX - Skill Effective By States
#~ #==============================================================================#
#~ # ** IEX(Icy Engine Xelion) - Skill Effective By States
#~ #------------------------------------------------------------------------------#
#~ # ** Created by    : IceDragon (http://www.rpgmakervx.net/)
#~ # ** Script-Status : Addon (Skills)
#~ # ** Script Type   : Skill Effective?
#~ # ** Date Created  : 12/07/2010
#~ # ** Date Modified : 12/07/2010
#~ # ** Version       : 1.0a
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** INTRODUCTION
#~ #------------------------------------------------------------------------------#
#~ # This script adds a new feature to your skills which affects the effective-ness
#~ # These skills rely on states. If the target doesn't have the required states
#~ # It will fail.
#~ #
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** FEATURES
#~ #------------------------------------------------------------------------------#
#~ # 1.0
#~ #  Notetags! Can be placed in Skill noteboxes.
#~ #------------------------------------------------------------------------------#
#~ # <effective_state: state_id, state_id, state_id> (or) <effs: state_id>
#~ # If the target does not have all of the required states, the skill will fail.
#~ #
#~ # EG. <effs: 6>
#~ # If the target is asleep the skill will be effective.
#~ #
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** COMPATABLITIES
#~ #------------------------------------------------------------------------------#
#~ #
#~ # BEM, DBS, Yggdrasil, Probably Takentai, not GTBS
#~ #
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** INSTALLATION
#~ #------------------------------------------------------------------------------#
#~ # 
#~ # Below 
#~ #  Materials
#~ #
#~ # Above 
#~ #   Main
#~ #
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** CHANGE LOG
#~ #------------------------------------------------------------------------------#
#~ # (DD/MM/YYYY)
#~ #  12/07/2010 - V1.0  Started and Completed Script
#~ #  12/07/2010 - V1.0a Bug Fix
#~ #  01/08/2011 - V1.0b Some Minor Changes and Improvements
#~ #
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** KNOWN ISSUES
#~ #------------------------------------------------------------------------------#  
#~ #
#~ #  Non at the moment
#~ #
#~ #------------------------------------------------------------------------------#
#~ $imported = {} if $imported == nil
#~ $imported["IEX_SEFFS"] = true
#~ #==============================================================================#
#~ # ** IEX::REGEXP::SEFFS
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ module IEX
#~   module REGEXP
#~     module SEFFS
#~       EFFECTIVE_STATE = /<(?:EFFECTIVE_STATE|effective state|effs)s?:?[ ]*(\d+(?:\s*,\s*\d+)*)>/i
#~     end  
#~   end
#~ end

#~ #==============================================================================#
#~ # ** RPG::Skill
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ class RPG::Skill
#~   
#~   def iex_seffs_cache
#~     @iex_seffs_cache_complete = false
#~     @iex_effective_states = []
#~     self.note.split(/[\r\n]+/).each { |line|
#~     case line
#~     when IEX::REGEXP::SEFFS::EFFECTIVE_STATE
#~       $1.scan(/\d+/).each { |sta_id|
#~       @iex_effective_states.push(sta_id.to_i) if sta_id.to_i > 0}
#~     end  
#~     }
#~     @iex_seffs_cache_complete = true
#~   end
#~   
#~   def req_effect_states
#~     iex_seffs_cache unless @iex_seffs_cache_complete
#~     return @iex_effective_states
#~   end
#~   
#~ end

#~ #==============================================================================#
#~ # ** Game_Battler
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ class Game_Battler
#~   #--------------------------------------------------------------------------
#~   # * Determine if a Skill can be Applied
#~   #     user  : Skill user
#~   #     skill : Skill
#~   #--------------------------------------------------------------------------
#~   alias iex_seffs_skill_effective? skill_effective? unless $@
#~   def skill_effective?(user, skill)
#~     for sta in skill.req_effect_states
#~       next if sta == nil
#~       return false unless @states.include?(sta.to_i)
#~     end  
#~     iex_seffs_skill_effective?(user, skill)
#~   end
#~   
#~ end
