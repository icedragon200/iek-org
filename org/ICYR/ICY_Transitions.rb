# ICY_Transitions
module Cache
  
  #--------------------------------------------------------------------------
  # * Get Transition Graphic
  #     filename : Filename
  #--------------------------------------------------------------------------
  def self.transition(filename)
   load_bitmap("Graphics/Transitions/", filename) 
  end
  
end
