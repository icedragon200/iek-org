# #~IEX - Thread Scaleing
#~ #==============================================================================
#~ # Scale
#~ #==============================================================================

#~ class Scale
#~   #--------------------------------------------------------------------------
#~   # Scale Window (by Mechacrash & Kylock)
#~   #--------------------------------------------------------------------------
#~   def self.game_window(w, h)
#~     size = Win32API.new('user32', 'GetSystemMetrics', 'I', 'I')
#~     move = Win32API.new('user32', 'MoveWindow', ['l','i','i','i','i','l'], 'l')
#~     find = Win32API.new('user32', 'FindWindowEx', ['l','l','p','p'], 'i')
#~     window = find.call(0, 0, "RGSS Player", 0)
#~     window_w = size.call(0)
#~     window_h = size.call(1)
#~     move.call(window,(window_w - w) / 2,(window_h - h) / 2, w, h, 1)
#~   end
#~   
#~ end

#~ class Scene_Title < Scene_Base
#~   
#~   alias iex_thread_prac_initialize initialize unless $@
#~   def initialize(*args)
#~     iex_thread_prac_initialize(*args)
#~     gm_width = 640 * 1.47
#~     gm_height = 480 * 1.47 
#~     Scale.game_window(gm_width, gm_height)
#~   end
#~   
#~ end

#~   
