# ICY_Scene_Title
#------------------------------------------------------------------------------#
# ** ICY Title Screen
# ** Created by : IceDragon
# ** Script-Status : ReWrite
# ** Date Created : 3/16/2010
# ** Date Modified : 8/08/2010
#------------------------------------------------------------------------------#
module ICY
  module TScreen
    #Icon used for new game
    NEW      = 464
    #Icon used for continue
    CON      = 811
    #Icon used for shut down
    SHTDWN   = 848
    #Icon used for YE Icon View
    ICONVIEW = 1078
    #Font size
    FONTSIZE = 16
    
    #Command Window Position
    COMMAND_POS = [20, 20]
    COMMAND_SIZE = [212, 212]
    RECT_SIZE = 64
    ITEM_SQ_SPACING = 72
    SELECTION_SIZE = 72
    COLUMNS_MAX = 2
    COMMAND_ITEMS = [    
    {"ID"           => 0,
     "Command_Name" => "Start!",
     "Icon_Index"   => NEW,
     "Command"      => 'command_new_game',
     "Enabled"      => 'true',
     },
    {"ID"           => 1,
     "Command_Name" => "Continue",
     "Icon_Index"   => CON,
     "Command"      => 'command_continue',
     "Enabled"      => '@continue_enabled',
     },
    {"ID"           => 2,
     "Command_Name" => "Shutdown..",
     "Icon_Index"   => SHTDWN,
     "Command"      => 'command_shutdown',
     "Enabled"      => 'true',
     },
    {"ID"           => 3,
     "Command_Name" => "Icon View",
     "Icon_Index"   => ICONVIEW,
     "Command"      => 'command_iconview',
     "Enabled"      => '$TEST',
     }
     ]
     
    CONTINUE_ID = 1
    NEWGAME_ID = 0
    TITLE_IMAGES = ["TitleBack", "EarthFlower", "DawnLogo", "TitleText"]
  end
end

#ICY_W1.2
#==============================================================================
# ** ICY_Title_Window_Command
#------------------------------------------------------------------------------
#  This window deals with title command choices.
#==============================================================================

class ICY_Title_Window_Command < ICY_HM_Window_Selectable
  
  include ICY::TScreen
  #--------------------------------------------------------------------------
  # * Public Instance Variables
  #--------------------------------------------------------------------------
  attr_reader   :commands                 # command
  #--------------------------------------------------------------------------
  # * Object Initialization
  #     width      : window width
  #     commands   : command string array
  #     commandcons: command icon array
  #     column_max : digit count (if 2 or more, horizontal selection)
  #     row_max    : row count (0: match command count)
  #     spacing    : blank space when items are arrange horizontally
  #--------------------------------------------------------------------------
  def initialize(commands, fontsize = 18, continue = false)
    super(0, 0, COMMAND_SIZE[0], COMMAND_SIZE[1])
    @continue_enabled = continue
    @commands = commands
    @item_max = commands.size
    @fontsize = fontsize
    @column_max = COLUMNS_MAX
    @item_sq_spacing = ITEM_SQ_SPACING
    @rect_size = RECT_SIZE
    @selection_size = SELECTION_SIZE
    self.width = (@column_max * ITEM_SQ_SPACING) + 32
    self.height = (page_row_max * ITEM_SQ_SPACING) + 32
    self.index = 0
    refresh
  end
  
  #--------------------------------------------------------------------------
  # * Refresh
  #--------------------------------------------------------------------------
  def refresh
    create_contents
    @reset = off_x
    @nw_x = @reset
    @nw_y = off_y
    @coun = 0
    self.contents.clear
    for i in 0...@item_max
      icy_recdraw_item(@commands[i]["Command_Name"], @commands[i]["Icon_Index"],
                       @commands[i]["Enabled"])
    end
  end
 
  #--------------------------------------------------------------------------
  # * Draw Item
  #     index   : item number
  #     enabled : enabled flag. When false, draw semi-transparently.
  #--------------------------------------------------------------------------
  def icy_recdraw_item(command, icon_index, enabled = 'true')
    usable = eval(enabled)
    color = Color.new(0, 255, 0)
    draw_command_icon_box(@nw_x, @nw_y, command, icon_index, RECT_SIZE, 4,  color, usable)
    advance_space
  end
  
  def match_index_with_id(idz)
    for i in 0...@commands.size
      if @commands[i]["ID"] == idz
       self.index = i
       return
     end
    end
  end
  
  def current_command
    return @commands[self.index]
  end
  
end

#==============================================================================
# ** Scene_Title
#------------------------------------------------------------------------------
#  This class performs the title screen processing.
#==============================================================================
class Scene_Title < Scene_Base
  
  #--------------------------------------------------------------------------
  # * Create Title Graphic
  #--------------------------------------------------------------------------
  def create_title_graphic
    @sprite = []
    imag = ICY::TScreen::TITLE_IMAGES
    coun = 0
    for img in imag
      next if img == nil
      @sprite[coun] = Sprite.new
      @sprite[coun].bitmap = Cache.system(img.to_s)
      @sprite[coun].opacity = 0
      coun += 1
    end
    @needs_fade_in = true
  end
        
  def initiate_fade_in
    wait_time = 255 / 5
    opac_div = 255 / wait_time
    opac_spr_div = 255 / @sprite.size
    begin 
      coun = 0
    for spr in @sprite
      next if spr == nil
      next_break = coun * opac_spr_div
      if @sprite[0].opacity >= next_break
        spr.opacity += opac_div 
        spr.update
      end
      coun += 1
    end
    Graphics.update
  end until @sprite[-1].opacity == 255 
    @command_window.openness = 0
    @command_window.visible = true
    @command_window.open
    @needs_fade_in = false
  end
  
  #--------------------------------------------------------------------------
  # * Dispose of Title Graphic
  #--------------------------------------------------------------------------
  def dispose_title_graphic
    for i in 0...@sprite.size
      @sprite[i].bitmap.dispose
      @sprite[i].dispose
      @sprite[i] = nil
    end
    @sprite.clear
    @sprite = nil
  end
  
  #--------------------------------------------------------------------------
  # * Create Command Window
  #--------------------------------------------------------------------------
  def create_command_window 
    @command_window = ICY_Title_Window_Command.new(ICY::TScreen::COMMAND_ITEMS, ICY::TScreen::FONTSIZE, @continue_enabled)
    @command_window.x = ICY::TScreen::COMMAND_POS[0]
    @command_window.y = ICY::TScreen::COMMAND_POS[1]
    
    if @continue_enabled                    # If continue is enabled
      @command_window.match_index_with_id(ICY::TScreen::CONTINUE_ID)# Move cursor over command
    else
      @command_window.match_index_with_id(ICY::TScreen::NEWGAME_ID)
    end
     @command_window.visible = false
    
#~     #@command_window.open   
  end
  
  def update
    super
    if @needs_fade_in
      initiate_fade_in
      return
    end    
    @command_window.update
    if Input.trigger?(Input::C)
     comma = @command_window.current_command
     eval(comma["Command"])
    end
  end
  
end
