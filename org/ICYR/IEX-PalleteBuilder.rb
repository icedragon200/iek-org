# IEX - Pallete Builder
#==============================================================================
# ** IEX(Icy Engine Xelion) - Pallete Maker
#------------------------------------------------------------------------------
# ** Created by    : IceDragon
# ** Script-Status : Tool
# ** Date Created  : 10/15/2010
# ** Date Modified : 10/15/2010
# ** Version       : 1.0
#------------------------------------------------------------------------------
#
# To use this script
# 1. Create an event with the following script command 
#    IEX_Image_To_Pallete.new(filename) filename being an image in your picture 
#                                       folder >.> I reccommend not using
#                                       VERY large images.
#
# 2. In the editor here.. do the same thing for an event right before main.
#
# After the script has done its job it will close the game.
$imported = {} if $imported == nil
$imported["IEX_PalleteBuilder"] = true

class IEX_ImageToPallete 
  
  def initialize(filename = "")
    process_image(Cache.picture(filename))
  end
  
  def process_image(bitmap = nil)
    return if bitmap == nil
    @working_bitmap = bitmap
    @pixel_colors = []
    
    for y in 0..@working_bitmap.width
      for x in 0..@working_bitmap.width
        new_co = @working_bitmap.get_pixel(x, y)
        new_co.alpha = 255
        @pixel_colors.push(new_co) unless @pixel_colors.include?(new_co)
      end
    end
    width_size = @pixel_colors.size
    width_size = 32 if width_size < 32
    
    @new_pallete_bit = Bitmap.new(width_size, 32) 
    x_c = 0
    y_c = 0
    for i in 0..@pixel_colors.size
      next if @pixel_colors[i].nil?
      next unless @pixel_colors[i].is_a?(Color)
      @new_pallete_bit.set_pixel(x_c, y_c, @pixel_colors[i])
      x_c += 1
      if x_c == @new_pallete_bit.width
        y_c += 1
        x_c = 0
      end  
    end  
    name = "Pallete_" + rand(20000).to_s + ".png"
    @new_pallete_bit.make_png(name)
    
    print "Pallete Creation Complete :#{name}
           Now exiting...."
    exit
  end
  
end

# =============================================================================
# PNG Saver by Woratana.
# =============================================================================

module Zlib
  class Png_File < GzipWriter
    
    def make_png(bitmap, mode = 0)
      @bitmap, @mode = bitmap, mode
      self.write(make_header)
      self.write(make_ihdr)
      self.write(make_idat)
      self.write(make_iend)
    end
    
    def make_header
      return [0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a].pack('C*')
    end
    
    def make_ihdr
      ih_size               = [13].pack('N')
      ih_sign               = 'IHDR'
      ih_width              = [@bitmap.width].pack('N')
      ih_height             = [@bitmap.height].pack('N')
      ih_bit_depth          = [8].pack('C')
      ih_color_type         = [6].pack('C')
      ih_compression_method = [0].pack('C')
      ih_filter_method      = [0].pack('C')
      ih_interlace_method   = [0].pack('C')
      string = ih_sign + ih_width + ih_height + ih_bit_depth + ih_color_type +
               ih_compression_method + ih_filter_method + ih_interlace_method
      ih_crc = [Zlib.crc32(string)].pack('N')
      return ih_size + string + ih_crc
    end
    
    def make_idat
      header  = "\x49\x44\x41\x54"
      data    = @mode == 0 ? make_bitmap_data0 : make_bitmap_data1
      data    = Zlib::Deflate.deflate(data, 8)
      crc     = [Zlib.crc32(header + data)].pack('N')
      size    = [data.length].pack('N')
      return size + header + data + crc
    end
    
    def make_bitmap_data0
      gz = Zlib::GzipWriter.open('png2.tmp')
      t_Fx = 0
      w = @bitmap.width
      h = @bitmap.height
      data = []
      for y in 0...h
        data.push(0)
        for x in 0...w
          t_Fx += 1
          if t_Fx % 10000 == 0
            Graphics.update
          end
          if t_Fx % 100000 == 0
            s = data.pack('C*')
            gz.write(s)
            data.clear
          end
          color = @bitmap.get_pixel(x, y)
          red = color.red
          green = color.green
          blue = color.blue
          alpha = color.alpha
          data.push(red)
          data.push(green)
          data.push(blue)
          data.push(alpha)
        end
      end
      s = data.pack('C*')
      gz.write(s)
      gz.close   
      data.clear
      gz = Zlib::GzipReader.open('png2.tmp')
      data = gz.read
      gz.close
      File.delete('png2.tmp')
      return data
    end
    
    def make_bitmap_data1
      w = @bitmap.width
      h = @bitmap.height
      data = []
      for y in 0...h
        data.push(0)
        for x in 0...w
          color = @bitmap.get_pixel(x, y)
          red = color.red
          green = color.green
          blue = color.blue
          alpha = color.alpha
          data.push(red)
          data.push(green)
          data.push(blue)
          data.push(alpha)
        end
      end
      return data.pack('C*')
    end
    
    def make_iend
      ie_size = [0].pack('N')
      ie_sign = 'IEND'
      ie_crc  = [Zlib.crc32(ie_sign)].pack('N')
      return ie_size + ie_sign + ie_crc
    end
    
  end
end

#=============================================================================
# ** Bitmap
#=============================================================================
class Bitmap
  def make_png(name = 'like', path = '', mode = 0)
    Zlib::Png_File.open('png.tmp')   { |gz| gz.make_png(self, mode) }
    Zlib::GzipReader.open('png.tmp') { |gz| $read = gz.read }
    f = File.open(path + name + '.png', 'wb')
    f.write($read)
    f.close
    File.delete('png.tmp')
  end
end
