# IEX - Equipment States
#~ #==============================================================================#
#~ # ** IEX(Icy Engine Xelion) - Equipment States
#~ #------------------------------------------------------------------------------#
#~ # ** Created by    : IceDragon (http://www.rpgmakervx.net/)
#~ # ** Script-Status : Addon (Equipment)
#~ # ** Script Type   : States from Equipment
#~ # ** Date Created  : 11/28/2010
#~ # ** Date Modified : 11/28/2010
#~ # ** Version       : 1.0
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** INTRODUCTION
#~ #------------------------------------------------------------------------------#
#~ # This script gives equipment the ability to have autosates for there wielders.
#~ #
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** FEATURES
#~ #------------------------------------------------------------------------------#
#~ # V1.0
#~ #  Notetags! Can be placed in Equipment noteboxes
#~ #------------------------------------------------------------------------------#
#~ #  <EQUIP_STATE: id, id, id> (or) <equip state: id, id, id>
#~ #
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** HOW TO USE
#~ #------------------------------------------------------------------------------#
#~ #  In an equipments notebox (Weapon or Armor)
#~ #  put <EQUIP_STATE: id, id, id> or <equip state: id, id, id>
#~ #  The actor will gain the states marked by Id while that piece of equipment
#~ #  is equipped.
#~ #
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** CHANGE LOG
#~ #------------------------------------------------------------------------------#
#~ # 
#~ #  11/28/2010 - V1.0 Finished Script
#~ #
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** KNOWN ISSUES
#~ #------------------------------------------------------------------------------#
#~ #  Non at the moment. 
#~ #
#~ #------------------------------------------------------------------------------#
#~ $imported = {} if $imported == nil
#~ $imported["IEX_Equipment_States"] = true
#~ #==============================================================================
#~ # ** IEX::REGEXP::EQUIPMENT_STATES
#~ #------------------------------------------------------------------------------
#~ #==============================================================================
#~ module IEX
#~   module REGEXP
#~     module EQUIPMENT_STATES
#~       EQUIPMENT_STATE = /<(?:EQUIP_STATE|equip state)s?:[ ]*(\d+(?:\s*,\s*\d+)*)>/i
#~     end
#~   end
#~ end
#~  
#~ #==============================================================================
#~ # ** RPG::BaseItem
#~ #------------------------------------------------------------------------------
#~ #==============================================================================
#~ class RPG::BaseItem
#~   
#~   alias iex_equipment_states_rpgbi_initialize initialize unless $@
#~   def initialize(*args)
#~     iex_equipment_states_rpgbi_initialize(*args)
#~     iex_state_equipment_cache
#~   end
#~   
#~   def iex_state_equipment_cache
#~     @iex_equip_states = []
#~     self.note.split(/[\r\n]+/).each { |line| 
#~     case line
#~     when IEX::REGEXP::EQUIPMENT_STATES::EQUIPMENT_STATE
#~       $1.scan(/\d+/).each { |state_id|
#~       @iex_equip_states.push(state_id.to_i) }
#~     end
#~     }
#~   end
#~   
#~   def iex_equip_states
#~     iex_state_equipment_cache if @iex_equip_states == nil
#~     return @iex_equip_states
#~   end
#~   
#~ end

#~ #==============================================================================
#~ # ** Game_Actor
#~ #------------------------------------------------------------------------------
#~ #==============================================================================
#~ class Game_Actor < Game_Battler
#~   
#~   alias iex_equipment_ga_states states unless $@
#~   def states
#~     result = iex_equipment_ga_states
#~     more_states = []
#~     for eq in equips
#~       next if eq == nil
#~       for ski_id in eq.iex_equip_states
#~         next if ski_id == nil
#~         more_states << $data_states[ski_id]
#~       end
#~     end
#~     return result |= more_states
#~   end
#~   
#~ end
