# IEX - State Effect Skills
#~ #==============================================================================#
#~ # ** IEX(Icy Engine Xelion) - State Effect Skills
#~ #------------------------------------------------------------------------------#
#~ # ** Created by    : IceDragon (http://www.rpgmakervx.net/)
#~ # ** Script-Status : Addon (Skills, Equipment)
#~ # ** Script Type   : State Rely Skills / State Damage Change
#~ # ** Date Created  : 12/04/2010
#~ # ** Date Modified : 12/04/2010
#~ # ** Version       : 2.0c
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** INTRODUCTION
#~ #------------------------------------------------------------------------------#
#~ # This script was originally the State Only Skills. 
#~ # That script was used to limit skill useage based on states.
#~ #
#~ # I haven't changed the concept, but rather improved it.
#~ #
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** FEATURES
#~ #------------------------------------------------------------------------------#
#~ # 2.0
#~ #  Notetags! Can be placed in Skill noteboxes
#~ #------------------------------------------------------------------------------#
#~ # <require_state: state_id, state_id, state_id>
#~ # The skill cannot be used unless the user has all states marked by
#~ # state_id.
#~ #
#~ # You can have as many as you like
#~ # EG <require_state: 1>
#~ # EG <require_state: 5, 6>
#~ # EG <require_state: 4, 7, 8, 12, 19, 20, 25>
#~ #
#~ #------------------------------------------------------------------------------#
#~ # 2.0
#~ #  Notetags! Can be placed in Skill and Equipment noteboxes
#~ #------------------------------------------------------------------------------# 
#~ # NOTES
#~ # tt -
#~ # user
#~ # target
#~ #------------------------------------------------------------------------------# 
#~ # <tt stm state_id: +/-x>
#~ # This is a set amount that will be added to the damage
#~ #
#~ # EG: <user stm 2: +200>
#~ # If the user has state 2, 200 is added to the damage
#~ #
#~ # EG2: <target stm 2: -200>
#~ # If the target has state 2, 200 is subtracted from the damage
#~ #
#~ #------------------------------------------------------------------------------# 
#~ # <tt stm state_id: x%>
#~ # This is a damage rate mod
#~ # Values above 100 will increase the damage while below will, decrease it.
#~ #
#~ # EG: <user stm 2: 150%>
#~ # If the user has state 2, the damage will change to 150% of it self.
#~ #
#~ # EG: <target stm 2: 80%>
#~ # If the target has state 2, the damage will change to 80% of it self.
#~ #
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** COMPATABLITIES
#~ #------------------------------------------------------------------------------#
#~ #
#~ # BEM, DBS, Probably Takentai, not GTBS
#~ # Not compatable with Yggdrasil (Yet)
#~ #
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** CHANGE LOG
#~ #------------------------------------------------------------------------------#
#~ #
#~ # ?????????? - V1.0  Completed Script
#~ # 11/06/2010 - V1.0  Editted Script Header
#~ # 12/04/2010 - V2.0  Rewrote State Only Skills, and is now, State Effect Skills
#~ # 12/08/2010 - V2.0a Bug Fix
#~ # 12/12/2010 - V2.0b Bug Fix
#~ # 12/17/2010 - V2.0c Bug Fix - If the character had no skills, 
#~ #                    an error would be thrown.
#~ #
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** KNOWN ISSUES
#~ #------------------------------------------------------------------------------#  
#~ #
#~ #  Non at the moment
#~ #
#~ #------------------------------------------------------------------------------#
#~ $imported = {} if $imported == nil
#~ $imported["IEX_State_Effect_Skills"] = true
#~ #==============================================================================
#~ # ** IEX
#~ #------------------------------------------------------------------------------
#~ #==============================================================================
#~ module IEX
#~ #==============================================================================
#~ # ** IMath
#~ #------------------------------------------------------------------------------
#~ #==============================================================================
#~   module IMath
#~     def self.cal_percent(perc, val)
#~       ans = val.to_i
#~       ans *= 100.0
#~       ans = ans * (perc.to_i / 100.0)
#~       ans /= 100.0
#~       return Integer(ans)
#~     end  
#~   end
#~ #==============================================================================
#~ # ** REGEXP::State_Effect_Skills
#~ #------------------------------------------------------------------------------
#~ #==============================================================================  
#~   module REGEXP
#~     module State_Effect_Skills
#~       REQ_STATE = /<(?:REQUIRE_STATE|require state)s?:?[ ]*(\d+(?:\s*,\s*\d+)*)>/i
#~       DMG_ST_MULTI1 = /<(\w+)[ ]*STM[ ]*(\d+):?[ ]*([\+\-]?\d+)>/i
#~       DMG_ST_MULTI2 = /<(\w+)[ ]*STM[ ]*(\d+):?[ ]*(\d+)([%%])>/i
#~     end
#~   end
#~ end
#~ #==============================================================================
#~ # ** RPG::BaseItem
#~ #------------------------------------------------------------------------------
#~ #==============================================================================
#~ class RPG::BaseItem
#~     
#~   def iex_ses_cache
#~     @iex_ses_cache_complete= false
#~     @state_skill           = false
#~     @states_needed         = []
#~     @ses_user_dm_set       = {}
#~     @ses_target_dm_set     = {}
#~     @ses_user_rate         = {}
#~     @ses_target_rate       = {}
#~     self.note.split(/[\r\n]+/).each { |line|
#~     case line
#~     when IEX::REGEXP::State_Effect_Skills::REQ_STATE
#~       @state_skill = true
#~       $1.scan(/\d+/).each { |num|
#~       @states_needed.push(num.to_i) if num > 0 }
#~     when IEX::REGEXP::State_Effect_Skills::DMG_ST_MULTI1
#~       sid = $2.to_i
#~       set = $3.to_i
#~       case $1.to_s.upcase
#~       when "USER"
#~         @ses_user_dm_set[sid]   = set
#~       when "TARGET"  
#~         @ses_target_dm_set[sid] = set
#~       end
#~     when IEX::REGEXP::State_Effect_Skills::DMG_ST_MULTI2
#~       sid = $2.to_i
#~       per = $3.to_i
#~       case $1.to_s.upcase
#~       when "USER"
#~         @ses_user_rate[sid]   = per
#~       when "TARGET"  
#~         @ses_target_rate[sid] = per
#~       end  
#~     end
#~     }
#~     @iex_ses_cache_complete = true
#~   end
#~   
#~   def req_state?
#~     iex_ses_cache unless @iex_ses_cache_complete 
#~     return @state_skill
#~   end
#~   
#~   def states_needed
#~     iex_ses_cache unless @iex_ses_cache_complete 
#~     return @states_needed
#~   end
#~   
#~   def state_dm_set(type, state)
#~     iex_ses_cache unless @iex_ses_cache_complete 
#~     return 1 if state == nil
#~     case type
#~     when 0      
#~       return @ses_user_dm_set[state.id] if @ses_user_dm_set.has_key?(state.id)
#~     when 1
#~       return @ses_target_dm_set[state.id] if @ses_target_dm_set.has_key?(state.id)
#~     end  
#~     return 1
#~   end
#~   
#~   def state_rate(type, state)
#~     iex_ses_cache unless @iex_ses_cache_complete 
#~     return 100 if state == nil
#~     case type
#~     when 0      
#~       return @ses_user_rate[state.id] if @ses_user_rate.has_key?(state.id)
#~     when 1
#~       return @ses_target_rate[state.id] if @ses_target_rate.has_key?(state.id)
#~     end  
#~     return 100 
#~   end
#~   
#~ end

#~ #==============================================================================
#~ # ** RPG::Enemy
#~ #------------------------------------------------------------------------------
#~ #==============================================================================
#~ class RPG::Enemy
#~   
#~   def iex_ses_cache
#~     @iex_ses_cache_complete= false
#~     @ses_user_dm_set   = {}
#~     @ses_target_dm_set = {}
#~     @ses_user_rate         = {}
#~     @ses_target_rate       = {}
#~     self.note.split(/[\r\n]+/).each { |line|
#~     case line
#~     when IEX::REGEXP::State_Effect_Skills::DMG_ST_MULTI1
#~       sid = $2.to_i
#~       set = $3.to_i
#~       case $1.to_s.upcase
#~       when "USER"
#~         @ses_user_dm_set[sid]   = set
#~       when "TARGET"  
#~         @ses_target_dm_set[sid] = set
#~       end
#~     when IEX::REGEXP::State_Effect_Skills::DMG_ST_MULTI2
#~       sid = $2.to_i
#~       per = $3.to_i
#~       case $1.to_s.upcase
#~       when "USER"
#~         @ses_user_rate[sid]   = per
#~       when "TARGET"  
#~         @ses_target_rate[sid] = per
#~       end  
#~     end
#~     }
#~     @iex_ses_cache_complete = true
#~   end
#~   
#~   def state_dm_set(type, state)
#~     iex_ses_cache unless @iex_ses_cache_complete 
#~     return 0 if state == nil
#~     case type
#~     when 0      
#~       return @ses_user_dm_set[state.id] if @ses_user_dm_set.has_key?(state.id)
#~     when 1
#~       return @ses_target_dm_set[state.id] if @ses_target_dm_set.has_key?(state.id)
#~     end  
#~     return 0
#~   end
#~   
#~   def state_rate(type, state)
#~     iex_ses_cache unless @iex_ses_cache_complete 
#~     return 100 if state == nil
#~     case type
#~     when 0      
#~       return @ses_user_rate[state.id] if @ses_user_rate.has_key?(state.id)
#~     when 1
#~       return @ses_target_rate[state.id] if @ses_target_rate.has_key?(state.id)
#~     end  
#~     return 100 
#~   end
#~   
#~ end

#~ #==============================================================================
#~ # ** Game_Battler
#~ #------------------------------------------------------------------------------
#~ #==============================================================================
#~ class Game_Battler
#~   
#~   def ses_mult_change(type, obj, dam, state)
#~     dam = dam.to_i
#~     return Integer(dam) if obj == nil
#~     dam *= 100.0
#~     dam += obj.state_dm_set(type, state).to_i
#~     dam = IEX::IMath.cal_percent(obj.state_rate(type, state), dam)
#~     dam /= 100.0
#~     return Integer(dam)
#~   end
#~     
#~   #--------------------------------------------------------------------------
#~   # * Determine Usable Skills
#~   #     skill : skill
#~   #--------------------------------------------------------------------------
#~   alias iex_ses_skill_can_use? skill_can_use? unless $@
#~   def skill_can_use?(skill)
#~     if skill != nil
#~       if skill.req_state?
#~         for sta_id in skill.states_needed
#~           return false unless @states.include?(sta_id)
#~         end
#~       end
#~     end  
#~     iex_ses_skill_can_use?(skill)
#~   end 
#~     
#~   alias iex_ses_make_obj_damage_value make_obj_damage_value unless $@
#~   def make_obj_damage_value(user, obj)
#~     iex_ses_make_obj_damage_value(user, obj)
#~     dama = 0
#~     dama = @hp_damage if @hp_damage > 0
#~     dama = @mp_damage if @mp_damage > 0
#~     for st in states
#~       dama = ses_mult_change(1, obj, dama, st)
#~     end
#~     for st2 in user.states
#~       dama = ses_mult_change(0, obj, dama, st2)
#~     end
#~     @hp_damage = Integer(dama) if @hp_damage > 0
#~     @mp_damage = Integer(dama) if @mp_damage > 0
#~   end
#~   
#~   alias iex_ses_make_attack_damage_value make_attack_damage_value unless $@
#~   def make_attack_damage_value(attacker)
#~     iex_ses_make_attack_damage_value(attacker)
#~     dama = 0
#~     dama = @hp_damage if @hp_damage > 0
#~     dama = @mp_damage if @mp_damage > 0
#~     if attacker.actor?
#~       for eq in attacker.equips
#~         next if eq == nil
#~         for st in states
#~           dama = ses_mult_change(1, eq, dama, st)
#~         end
#~         for st2 in attacker.states
#~           dama = ses_mult_change(0, eq, dama, st2)
#~         end 
#~       end 
#~     else
#~       for st in states
#~         dama = ses_mult_change(1, attacker.enemy, dama, st)
#~       end
#~       for st2 in attacker.states
#~         dama = ses_mult_change(0, attacker.enemy, dama, st2)
#~       end 
#~     end  
#~     @hp_damage = Integer(dama) if @hp_damage > 0
#~     @mp_damage = Integer(dama) if @mp_damage > 0
#~   end
#~   
#~ end
