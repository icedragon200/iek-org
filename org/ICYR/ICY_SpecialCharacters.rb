# ICY_Special Characters
# Code List
# \map[id] displays the name of the map with the given id
# \mapc    displays the current maps name
# 
# ICY_Interaction_System Addons
# \panm    displays partners name 
#
# ICY_Clan_System
# \clan    displays party name
#
$imported = {} if $imported == nil
$imported["ICY_Special_Characters"] = true
#==============================================================================
# ** Window_Message
#------------------------------------------------------------------------------
#  This message window is used to display text.
#==============================================================================
class Window_Message < Window_Selectable
  # Alias stuff 
  alias icy1_special_characters convert_special_characters unless $@
  def convert_special_characters(*args)
  icy1_special_characters(*args)
  
#==============================================================================
# + NEW MESSAGE CODES
#==============================================================================

  # Code for specific map name
  @text.gsub!(/\\MAP\[(\d+)\]/i)    {  
    map_infos = load_data("Data/MapInfos.rvdata")
    map_infos[$1.to_i].name }
    
  # Code for returning current map name  
  @text.gsub!(/\\MAPC/i)   { $game_map.name }
  
  # Support for interaction system
  if $imported["ICY_Interact_System"]
    @text.gsub!(/\\PANM/i)     {
    if $game_party.partner != nil
      $game_actors[$game_party.partner].name 
    end}
  end
  
  # Support for clan system
  if $imported["ICY_Clan_System"] 
    @text.gsub!(/\\CLAN/i)    { $game_party.party_name }
  end
  
end

end

#===============================================================================
# GAME MAP SETUP                                                
#===============================================================================
class Game_Map
  def name
    map_infos = load_data("Data/MapInfos.rvdata")
    name = map_infos[@map_id].name.clone
    name.gsub!(/\\N\[([0-9]+)\]/i) { $game_actors[$1.to_i].name }
    name.gsub!(/\\PN\[([0-9]+)\]/i) { $game_party.members[$1.to_i].name }
    name.gsub!(/\\V\[([0-9]+)\]/i) { $game_variables[$1.to_i] }  
    return name
  end
end
