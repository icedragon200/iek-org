# ICY_Actor_BattleTrait
#------------------------------------------------------------------------------#
# ** Actor Battle Traits
# ** Created by : IceDragon
# ** Script-Status : Beta
# ** Date Created : 5/15/2010
# ** Date Modified : 8/18/2010
# ** Version : 0.4a
#------------------------------------------------------------------------------#
$imported = {} if $imported == nil
$imported["ICY_ActorBattleTraits"] = true
module ICY
  module BATTLE_TRAITS_SYSTEM 
    TRAITS_SET = {}
    
    TRAITS_SET[0] = { # DON"T CHANGE THESE
    "ID" => 0, # DON"T CHANGE THESE
    "Type" => "DUD", # DON"T CHANGE THESE
    "Name" => "DUD", # DON"T CHANGE THESE
    "Icon_Index" => 0, # DON"T CHANGE THESE
    "Effect" => "USELESS", # DON"T CHANGE THESE
    }
    TRAITS_SET[1] = {
    "ID" => 1,
    "Type" => "ATK",
    "Name" => "Air Logistics",
    "Icon_Index" => 2049,
    "Effect" => "AIR_LOGIC",
    }
    TRAITS_SET[2] = {
    "ID" => 2,
    "Type" => "ATK",
    "Name" => "Gifted Lancer",
    "Icon_Index" => 1390,
    "Effect" => "GIFT_LANCE",
    }
    
    TRAITS_SET[3] = {
    "ID" => 3,
    "Type" => "DEF",
    "Name" => "Fortress",
    "Icon_Index" => 0,
    "Effect" => "FORT",
    }
    TRAITS_SET[4] = {
    "ID" => 4,
    "Type" => "DEF",
    "Name" => "Cat Rights",
    "Icon_Index" => 2242,
    "Effect" => "NIGHT_MOVE",
    }
    TRAITS_SET[5] = {
    "ID" => 5,
    "Type" => "DEF",
    "Name" => "Dancer's Run",
    "Icon_Index" => 2244,
    "Effect" => [""],
    }  
   
    DOESNT_EXIST = ["Doesn't Exist!", 0]
    def self.get_trait_info(num = 0)
      if TRAITS_SET.has_key?(num)
         return [TRAITS_SET[num]["Name"], TRAITS_SET[num]["Icon_Index"]]
       else
         return DOESNT_EXIST
      end
    end
    
   end
   
   module TRAITS_SETUP_EFFECTS
     CONDITIONS = { # Eval Expressions
     "NIL"     => '',
     "HALF_HP" => 'self.hp < (maxhp / 2)',
     "HALF_MP" => 'self.mp < (maxmp / 2)',
     "NIGHT"   => '$game_switches[Jet_DayNight::NIGHT_SWITCH]',
     "DAY"     => '$game_switches[Jet_DayNight::DAY_SWITCH]',
     "DUSK"    => '$game_switches[Jet_DayNight::DUSK_SWITCH]',
     "DAWN"    => '$game_switches[Jet_DayNight::DAWN_SWITCH]',
     "POISONED"=> 'states.include?($data_states[2])',
     "SLEEPING"=> 'states.include?($data_states[6])',
     "DEAD"    => 'self.dead?',
"ACTOR_HALF_HP"=> '$game_actors[%n].hp < ($game_actors[%n].maxhp / 2)',
"ACTOR_HALF_MP"=> '$game_actors[%n].mp < ($game_actors[%n].maxmp / 2)',
"ACTOR_DEAD"   => '$game_actors[%n].dead?',

# TBS ADDED_CONDITIONS
       # USE ONLY WITH RANGE TRAITS = 
"LINE_TO_TARGET"=> '@line_to_target', 
"IS_BOW"        => '@is_bow',
     }
     EFFECTS = {
     "HALVE"   => '%n / 2',
     "DOUBLE"  => '%n * 2',
     "FORT"    => 'def_add = [@temp_atk + @temp_def, 999].min',
     "RANDOM_2"  => 'rand(2)', # Why the hell!
     }
     # TYPES == "MOVE", "RANGE", "MAXHP+", "MAXMP+", "ATK+", "DEF+", "SPI+", "AGI+" 
     # Effect == range, move, atk_ed, def_ed, spi_ed, agi_ed,
     # TBS Range == max_rng, min_rng, is_bow, line_to_target, aoe
     MIXES = {}
     MIXES["USELESS"] = {
     "Conditions" => [CONDITIONS["NIL"]],
     "Type" => "NIL",
     "Effect" => '',
     "FailEffect" => ''
     }
     MIXES["NIGHT_MOVE"] = {
     "Conditions" => [CONDITIONS["NIGHT"]],
     "Type" => "MOVE",
     "Effect" => 'move += 2',
     "FailEffect" => 'move += 0'
     }
     MIXES["AIR_LOGIC"] = {
     "Conditions" => [CONDITIONS["IS_BOW"]],
     "Type" => "RANGE",
     "Effect" => '@max_rng += 1',
     "FailEffect" => '@max_rng += 0'
     }
     MIXES["GIFT_LANCE"] = {
     "Conditions" => [CONDITIONS["LINE_TO_TARGET"]],
     "Type" => "RANGE",
     "Effect" => '@max_rng += 1',
     "FailEffect" => '@max_rng += 0'
     }
     MIXES["FORT"] = {
     "Conditions" => [CONDITIONS["HALF_HP"]],
     "Type" => "DEF+",
     "Effect" => EFFECTS["FORT"],
     "FailEffect" => ''
     }
   end # TRAITS_SETUP Module
   
end # ICY Module    
   
#Understanding weapon's range 
#The array conatins
#MaxRg = [0]
#MinRg = [1]
#IsBow = [2]
#LToTar = [3]
#AoE = [4]

class Game_Actor < Game_Battler
  
  attr_accessor :trait_ids
  attr_accessor :trait_cal
  
  include ICY::TRAITS_SETUP_EFFECTS
  include ICY::BATTLE_TRAITS_SYSTEM
    
      def trait_effect_type_valid?(effect, type_valid)
       if MIXES[effect]["Type"] == type_valid
         return true
        else 
         return false
       end
     end
     
     def get_trait_effect(trait_id)
       return MIXES[TRAITS_SET[trait_id]["Effect"]]
     end
     
     def get_trait_effect_array(trait_id)
       return TRAITS_SET[trait_id]["Effect"]
     end
     
     def trait_effect_array?(trait_id)
       if TRAITS_SET[trait_id]["Effect"].is_a?(Array)
         return true
       else
         return false
       end
     end
      
    def build_trait_eval_list_set(trait_id, type)
      eval_list_set = []
      if TRAITS_SET.has_key?(trait_id)
        effects = get_effects_array(trait_id, type)
         for effec in effects
           next if effec == nil
           mixset = MIXES[effec]
           answers = build_trait_ans(mixset)
           if answers.all?
             eval_list_set.push(mixset["Effect"])
           else
             eval_list_set.push(mixset["FailEffect"])
           end
         end
       end
       return eval_list_set
     end
     
    def get_effects_array(trait_id, valid_type)
       effects = []
       trait_array = get_trait_effect_array(trait_id)
       if trait_effect_array?(trait_id)
         for tra in trait_array
           if trait_effect_type_valid?(tra, valid_type)
             effects.push(tra)
           end
         end
       else
         effects.push(trait_array)
       end
        return effects
      end
      
      def build_trait_ans(mixset)
        answers = []
        if mixset["Conditions"].is_a?(Array)
          for i in mixset["Conditions"]
           answers.push(eval(i))
          end
         else ; answers.push(eval(mixset["Conditions"]))
         end
         return answers
       end
      
    def trait_evalua 
      text = 'if eval_list_set.empty? == false
               for eva in eval_list_set
                next if eva == nil
                new_eva = eva.to_s
                eval(new_eva)
               end
              end'
      return text
    end
    
 alias icy_actor_traits_initialize initialize unless $@
  def initialize(*args)
    icy_actor_traits_initialize(*args)  
    prepare_traits
  end
  
  def prepare_traits
     @trait_ids = {
     "ATK" => 0,
     "DEF" => 3,
     "SPI" => 0,
     "AGI" => 0,
     }
  end
  
  #--------------------------------------------------------------------------
  # * Get Maximum HP
  #--------------------------------------------------------------------------
  alias icy_actor_traits_maxhp maxhp unless $@
  def maxhp
    _maxhp = icy_actor_traits_maxhp
    return _maxhp
  end
  #--------------------------------------------------------------------------
  # * Get Maximum MP
  #--------------------------------------------------------------------------
  alias icy_actor_traits_maxmp maxmp unless $@
  def maxmp
    _maxmp = icy_actor_traits_maxmp
    return _maxmp
  end
  
  #--------------------------------------------------------------------------
  # * Get Attack
  #--------------------------------------------------------------------------
  alias icy_actor_traits_atk_var_atk atk unless $@
  def atk
    prep_temp_all_stats
    stat_name = "ATK+"
    atk_add = icy_actor_traits_atk_var_atk
    trait_id = @trait_ids["ATK"]
      if TRAITS_SET.has_key?(trait_id)
        if TRAITS_SET[trait_id]["Type"] != "DUD"
         eval_list_set = []
         eval_list_set = build_trait_eval_list_set(trait_id, stat_name)
         useing_text = trait_evalua
         eval(useing_text)
         end
       end 

    return atk_add
  end
  #--------------------------------------------------------------------------
  # * Get Defense
  #--------------------------------------------------------------------------
  alias icy_actor_traits_def_var_def def unless $@
  def def
    prep_temp_all_stats
    stat_name = "DEF+"
    def_add = icy_actor_traits_def_var_def
    trait_id = @trait_ids["DEF"]
      if TRAITS_SET.has_key?(trait_id)
        if TRAITS_SET[trait_id]["Type"] != "DUD"
         eval_list_set = []
         eval_list_set = build_trait_eval_list_set(trait_id, stat_name)
         useing_text = trait_evalua
         eval(useing_text)
         end
       end 
    return def_add 
  end
  #--------------------------------------------------------------------------
  # * Get Spirit
  #--------------------------------------------------------------------------
  alias icy_actor_traits_spi_var_spi spi unless $@
  def spi
    prep_temp_all_stats
    stat_name = "SPI+"
    spi_add  = icy_actor_traits_spi_var_spi
    trait_id = @trait_ids["SPI"]
      if TRAITS_SET.has_key?(trait_id)
        if TRAITS_SET[trait_id]["Type"] != "DUD"
         eval_list_set = []
         eval_list_set = build_trait_eval_list_set(trait_id, stat_name)
         useing_text = trait_evalua
         eval(useing_text)
         end
       end 
    return spi_add
  end
  #--------------------------------------------------------------------------
  # * Get Agility
  #--------------------------------------------------------------------------
  alias icy_actor_traits_agi_var_agi agi unless $@
  def agi
    prep_temp_all_stats
    stat_name = "AGI+"
    agi_add = icy_actor_traits_agi_var_agi
    trait_id = @trait_ids["AGI"]
      if TRAITS_SET.has_key?(trait_id)
        if TRAITS_SET[trait_id]["Type"] != "DUD"
         eval_list_set = []
         eval_list_set = build_trait_eval_list_set(trait_id, stat_name)
         useing_text = trait_evalua
         eval(useing_text)
         end
       end 
    return agi_add
  end
  
  def prep_temp_all_stats
    @temp_atk = icy_actor_traits_atk_var_atk
    @temp_def = icy_actor_traits_def_var_def
    @temp_spi = icy_actor_traits_spi_var_spi
    @temp_agi = icy_actor_traits_agi_var_agi
  end      
     
end
