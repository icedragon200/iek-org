# IEX - ManaBurn
#~ #==============================================================================#
#~ # ** IEX(Icy Engine Xelion) - ManaBurn
#~ #------------------------------------------------------------------------------#
#~ # ** Created by    : IceDragon (http://www.rpgmakervx.net/)
#~ # ** Script-Status : Addon (Skill, Item)
#~ # ** Script Type   : Damage Modifier
#~ # ** Date Created  : 01/06/2011
#~ # ** Date Modified : 01/06/2011
#~ # ** Version       : 1.0
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** INTRODUCTION + HOW-TO-USE
#~ #------------------------------------------------------------------------------#
#~ # This script gives Items and Skills the ability to do mana burning.
#~ # What is mana burning?
#~ # Its damage thats done based on the targets Mp.
#~ # Anyway in a skill, and/or items notebox
#~ # Put 
#~ # <manaburn> (or) <mana burn> (or) <mana_burn>
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================
#~ # ** RPG::BaseItem
#~ #------------------------------------------------------------------------------
#~ #==============================================================================
#~ class RPG::BaseItem
#~   
#~   def mana_burn_cache
#~     @mana_burn_cache_complete = false
#~     @mana_burn = false
#~     self.note.split(/[\r\n]+/).each { |line|
#~       case line
#~       when /<(?:MANA_BURN|MANABURN|mana burn)>/i
#~         @mana_burn = true
#~       end  
#~     }
#~     @mana_burn_cache_complete = true
#~   end
#~   
#~   def mana_burn?
#~     mana_burn_cache unless @mana_burn_cache_complete 
#~     return @mana_burn
#~   end
#~   
#~ end

#~ #==============================================================================
#~ # ** Game_Battler
#~ #------------------------------------------------------------------------------
#~ #==============================================================================
#~ class Game_Battler
#~   #--------------------------------------------------------------------------
#~   # * Calculation of Damage Caused by Skills or Items
#~   #     user : User of skill or item
#~   #     obj  : Skill or item (for normal attacks, this is nil)
#~   #    The results are substituted for @hp_damage or @mp_damage.
#~   #--------------------------------------------------------------------------
#~   alias iex_manaburn_make_obj_damage_value make_obj_damage_value unless $@
#~   def make_obj_damage_value(user, obj)
#~     iex_manaburn_make_obj_damage_value(user, obj)
#~     unless obj.nil?
#~       if obj.mana_burn?
#~         if obj.damage_to_mp  
#~           @mp_damage = self.mp
#~         else
#~           @hp_damage = self.mp
#~         end
#~       end  
#~     end
#~   end
#~   
#~ end

