# #~IEX - Minimap
#~ module IEX
#~   module MINIMAP
#~     
#~   end
#~ end

#~ class IEX_Minimap_Sys < Window_Base
#~   
#~   def initialize(x, y, width, height)
#~     super(x, y, width, height)
#~     self.opacity = 0
#~     @viewport = Viewport.new(x, y, width, height)
#~     @viewport.z = 200
#~     @x = x
#~     @y = y
#~     @width = width
#~     @height = height
#~     @sprites = {}
#~     passa = calculate_passes
#~     draw_map(passa)
#~   end
#~   
#~   def calculate_passes
#~     arra = []
#~     for m_y in 0..$game_map.width
#~       for m_x in 0..$game_map.height
#~         pass = $game_map.passable?(m_x, m_y)
#~         arra.push([m_x, m_y, pass])
#~       end  
#~     end 
#~     return arra
#~   end
#~   
#~   def draw_map(data = nil)
#~     return if data == nil
#~     @sprites = {}
#~     size = 4
#~     @sprites["Background"] = Sprite.new(@viewport)
#~     @sprites["Background"].bitmap = Bitmap.new($game_map.width * size, $game_map.height * size)
#~     rect = Rect.new(0, 0, @width, @height)
#~     @sprites["Background"].bitmap.fill_rect(rect, Color.new(200, 200, 200))
#~     @sprites["Background"].z = 0
#~     
#~     @sprites["Map"] = Sprite.new(@viewport)
#~     @sprites["Map"].bitmap = Bitmap.new($game_map.width * size, $game_map.height * size)
#~     @sprites["Map"].z = 10
#~     
#~     @sprites["Events"] = Sprite.new(@viewport)
#~     @sprites["Events"].bitmap = Bitmap.new($game_map.width * size, $game_map.height * size)
#~     @sprites["Events"].z = 20
#~     
#~     @sprites["Player"] = Sprite.new(@viewport)
#~     @sprites["Player"].bitmap = Bitmap.new(32, 32)
#~     play_rect = Rect.new(0, 0, size, size)
#~     @sprites["Player"].bitmap.fill_rect(play_rect, ICY::Colors::LimeGreen)
#~     @sprites["Player"].z = 20
#~     
#~     for ti in data
#~       tiny_rect = Rect.new(ti[0] * size, ti[1] * size, size, size)
#~       color = ti[2] == true ? Color.new(40, 40, 180) : Color.new(180, 40, 40)
#~       @sprites["Map"].bitmap.fill_rect(tiny_rect, color)
#~     end
#~     
#~     for spr in @sprites.values
#~       next if spr == nil
#~       #next unless spr.is_a?(Sprite)
      #spr.x = @x
      #spr.y = @y
#~       spr.opacity = 128
#~     end
#~     update_mini
#~   end
#~   
#~   def visible=(bool)
#~     
#~   end
#~   
#~   def dispose
#~     if @viewport != nil
#~       @viewport.dispose
#~       @viewport = nil
#~     end
#~     for spr in @sprites.values
#~       next if spr == nil
#~       spr.dispose
#~       spr = nil
#~     end
#~     @sprites.clear
#~     @sprites = []
#~   end
#~   
#~   def update
#~     super
#~     update_mini
#~     self.contents.clear
#~     self.contents.draw_text(0, self.height - 96, self.width - 64, 24, $game_player.x)
#~     self.contents.draw_text(0, self.height - 64, self.width - 64, 24, $game_player.y)
#~   end
#~   
#~   def update_mini
#~     @sprites["Player"].x = @x + ($game_player.x * 4)
#~     @sprites["Player"].y = @y + ($game_player.y * 4)
#~     if @sprites["Player"].opacity > 0
#~       @sprites["Player"].opacity -= 255 / 60
#~     elsif @sprites["Player"].opacity == 0 
#~       @sprites["Player"].opacity = 255 
#~     end  
#~     for spr in @sprites.values
#~       next if spr == nil
#~       spr.update
#~       spr = nil
#~     end
#~     @viewport.update
#~   end
#~     
#~ end

#~ class Scene_Map < Scene_Base
#~   
#~   alias iex_scmp_minimap_start start unless $@
#~   def start(*args)
#~     iex_scmp_minimap_start(*args)
#~     @minimap = IEX_Minimap_Sys.new(20, 20, 320, 320)
#~     @minimap.update
#~   end
#~   
#~   
#~   alias iex_scmp_minimap_update update unless $@
#~   def update(*args)
#~     iex_scmp_minimap_update(*args)
#~     @minimap.update
#~   end
#~   
#~ end
