# IEX - Event Tiles
$imported = {} if $imported == nil 
$imported["IEX_EventTiles"] = true

class Game_Map
  
  alias iex_evtile_setup setup unless $@
  def setup(*args)
    iex_evtile_setup(*args)
    setup_evtiles
  end
  
  def setup_evtiles
    @evtiles = [] if @evtiles == nil
    @evtiles.clear
  end
  
  def register_evtile(event_id)
    @evtiles.push(event_id) unless @evtiles.include?(event_id)
  end  
  
  def unregister_evtile(event_id)
    @evtiles.delete(event_id)
  end 
  
  def getEvtiles(x, y)
    evs = []
    setup_evtiles if @evtiles == nil
    for evid in @evtiles
      next if evid == nil
      next unless $game_map.events[evid].pos?(x, y)
      evs.push(evid)
    end
    return evs
  end  
  
  #--------------------------------------------------------------------------
  # * Determine bush
  #     x : x-coordinate
  #     y : y-coordinate
  #--------------------------------------------------------------------------
  alias iex_evtile_bush bush? unless $@
  def bush?(x, y)
    evt = getEvtiles(x, y)
    for i in evt
      return true if $game_map.events[i].evTileProps.include?(:bush)
    end 
    iex_evtile_bush(x, y)
  end
  
  #--------------------------------------------------------------------------
  # * Determine if Passable
  #     x    : x coordinate
  #     y    : y coordinate
  #     flag : The impassable bit to be looked up
  #            (normally 0x01, only changed for vehicles)
  #--------------------------------------------------------------------------
  alias iex_evtile_passable passable? unless $@
  def passable?(x, y, flag = 0x01)
    for ev in getEvtiles(x, y)
      return false if $game_map.events[ev].evTileProps.include?(:block)
      return true if $game_map.events[ev].evTileProps.include?(:pass)
    end  
    iex_evtile_passable(x, y, flag)
  end
  
end

class Game_Event < Game_Character
  
  alias iex_evtile_ge_setup setup unless $@
  def setup(*args)
    iex_evtile_ge_setup(*args)
    setup_evTile    
  end
  
  def setup_evTile
    $game_map.unregister_evtile(@id)
    @evTileProps = []
    return if @list == nil
    for i in 0..@list.size
      next if @list[i] == nil
      if @list[i].code == 108
        @list[i].parameters.to_s.split(/[\r\n]+/).each { |line| 
        case line
        when /<(?:BUSH)>/i
          $game_map.register_evtile(@id)
          @evTileProps.push(:bush)
        when /<(?:PASS|PASSABLE)>/i
          $game_map.register_evtile(@id)
          @evTileProps.push(:pass)
        when /<(?:BLOCK|X)>/i  
          $game_map.register_evtile(@id)
          @evTileProps.push(:block)
        end
       }  
      end
    end
  end
  
  def evTileProps
    setup_evTile if @evTileProps == nil
    return @evTileProps
  end
  
end
