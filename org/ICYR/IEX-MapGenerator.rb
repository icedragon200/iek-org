# IEX - Map Generator

module IEX
  module MAP_GEN_KIT
    
    def self.generate_map(width, height, type)
      map = RPG::Map.new(width, height)
      br_rt = 3
      #case type
        for x in 0..map.width
          for y in 0..map.height
            map.data[x, y, 0] = 2960 # Default Grass
            if rand(br_rt) == 0
              map.data[x, y, 0] = 3344
            end
            if rand(5) == 0
              map.data[x, y, 0] = 2240
            end  
            if rand(2) == 0
              if map.data[x, y, 0] == 2960
                map.data[x, y, 1] = 3008 
              end  
            end
          end
        end
        for x in 0..map.width
          for y in 0..map.height
            if map.data[x, y, 0] == 2960
              place_autotile(map, x, y, 0, map.data[x, y, 0])
            end  
            if map.data[x, y, 0] == 3344
              place_autotile(map, x, y, 0, map.data[x, y, 0])
            end  
            if map.data[x, y, 0] == 2240
              place_autotile(map, x, y, 0, map.data[x, y, 0])
            end 
            if map.data[x, y, 1] == 3008                
              place_autotile(map, x, y, 1, map.data[x, y, 0])
            end 
          end
        end  
        for x in 0..map.width
          for y in 0..map.height
            for tile_id in [2960, 3344]
              z = 0
              next if map.data[x, y, z] == nil
              map.data[x, y, z] = map.data[x, y, z].to_i
              if !in_tile_Range(tile_id, map.data[x - 1, y, z]) 
                map.data[x, y, z] -= 1
              end
              if !in_tile_Range(tile_id, map.data[x + 1, y, z]) 
                map.data[x, y, z] -= 4
              end
            end
            
          end
        end
      #end
      return map
    end
    
    def self.in_tile_Range(basetile, seltile)
      rng = basetile..basetile + 46
      return (rng.to_a.include?(seltile) == true)
    end
    
    def self.place_autotile(map, x, y, z, tile_id)
      map.data[x, y, z] = tile_id 
      n = cal_auto_tile(map, x, y, z, tile_id)
      map.data[x, y, z] += n
    end
    
    def self.cal_auto_tile(map, x, y, z, basetile)
        n = 0
      #if map.data[x, y, 0] == basetile   # 床タイプのオートタイル
        n += 1 unless same_tile?(map, x - 1, y, z, basetile)   # 左がない
        n += 1 unless same_tile?(map, x + 1, y, z, basetile)   # 右がない
        n += 1 unless same_tile?(map, x, y - 1, z, basetile)   # 上がない
        n += 1 unless same_tile?(map, x, y + 1, z, basetile)   # 下がない
        if n == 0
          n += 1 unless same_tile?(map, x - 1, y - 1, z, basetile)
          n += 2 unless same_tile?(map, x + 1, y - 1, z, basetile)
          n += 4 unless same_tile?(map, x + 1, y + 1, z, basetile)
          n += 8 unless same_tile?(map, x - 1, y + 1, z, basetile)
        elsif n == 1
          if !same_tile?(map, x - 1, y, z, basetile)      # 左がない
            n = 16
            n += 1 unless same_tile?(map, x + 1, y - 1, z, basetile)   # 右上がない
            n += 2 unless same_tile?(map, x + 1, y + 1, z, basetile)   # 右下がない
          elsif !same_tile?(map, x, y - 1, z, basetile)   # 上がない
            n = 20
            n += 1 unless same_tile?(map, x + 1, y + 1, z, basetile)   # 右下がない
            n += 2 unless same_tile?(map, x - 1, y + 1, z, basetile)   # 左下がない
          elsif !same_tile?(map, x + 1, y, z, basetile)   # 右がない
            n = 24
            n += 1 unless same_tile?(map, x - 1, y + 1, z, basetile)   # 左下がない
            n += 2 unless same_tile?(map, x - 1, y - 1, z, basetile)   # 左上がない
          else                                    # 下がない
            n = 28
            n += 1 unless same_tile?(map, x - 1, y - 1, z, basetile)   # 左上がない
            n += 2 unless same_tile?(map, x + 1, y - 1, z, basetile)   # 右上がない
          end
        elsif n == 2
          if !same_tile?(map, x - 1, y, z, basetile)
            n = !same_tile?(map, x + 1, y, z, basetile) ? 32 :
                 same_tile?(map, x, y - 1, z, basetile) ? 40 : 34
          else
            n = same_tile?(map, x + 1, y, z, basetile) ? 33 :
                same_tile?(map, x, y - 1, z, basetile) ? 38 : 36
          end
        elsif n == 3
          n = same_tile?(map, x, y + 1, z, basetile) ? 42 :
              same_tile?(map, x + 1, y, z, basetile) ? 43 :
              same_tile?(map, x, y - 1, z, basetile) ? 44 : 45
        else
          n = 46
        end
        return n
      #end
    end
    
    def self.same_tile?(map, x, y, z, id)
      return true if y < 0      # 画面外なら同じタイルがあるとする
      #return false if map.data[x, y, 0] == nil
      n = map.data[x, y, z].to_i
      return (n >= id and n < id + 46)
    end
  
  end
end

  
