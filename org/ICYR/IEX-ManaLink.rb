# IEX - ManaLink
#~ #==============================================================================#
#~ # ** IEX(Icy Engine Xelion) - Mana Link
#~ #------------------------------------------------------------------------------#
#~ # ** Created by    : IceDragon (http://www.rpgmakervx.net/)
#~ # ** Script-Status : Addon (Equipment)
#~ # ** Script Type   : Actor, Skill Mp Cost Modifier
#~ # ** Date Created  : 10/29/2010
#~ # ** Date Modified : 10/29/2010
#~ # ** Version       : 1.0
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** INTRODUCTION
#~ #------------------------------------------------------------------------------#
#~ #
#~ #
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** FEATURES
#~ #------------------------------------------------------------------------------#
#~ # V1.0
#~ #  Notetags! Can be placed in Equipment noteboxes
#~ #------------------------------------------------------------------------------#
#~ #  <mana link>
#~ #  This cause the equipers to share Mp
#~ #
#~ #  <mana link id: something>
#~ #  This allows specific equipment to have a link with other equip holders with
#~ #  the same id..
#~ #  This Id can be anything.
#~ #
#~ #  Let's say you have a broadsword with <mana link id: cheese>
#~ #  and a shield with the <mana link id: cheese>
#~ #  If the actor with the broadsword uses a skill.
#~ #  The Mp cost is divided by number of actors in that mana link.
#~ #  The division is then subtracted from all the actors in the link
#~ #
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** HOW TO USE
#~ #------------------------------------------------------------------------------#
#~ #  In an equipments notebox (Weapon or Armor)
#~ #  put <mana link id: something>
#~ #  or <mana link> (This is actually an id of nil)
#~ #
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** CHANGE LOG
#~ #------------------------------------------------------------------------------#
#~ # 
#~ #  10/29/2010 - V1.0 Finished Script
#~ #
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** KNOWN ISSUES
#~ #------------------------------------------------------------------------------#
#~ #  Non at the moment. 
#~ #
#~ #------------------------------------------------------------------------------#
#~ $imported = {} if $imported == nil
#~ $imported["IEX_Mana_Link"] = true
#~ #==============================================================================
#~ # ** IEX::REGEXP::MANA_LINK
#~ #------------------------------------------------------------------------------
#~ #==============================================================================
#~ module IEX
#~   module REGEXP
#~     module MANA_LINK
#~       MANA_LINK = /<(?:MANA_LINK|mana link)>/i
#~    MANA_LINK_ID = /<(?:MANA_LINK_ID|mana link id):[ ]*(.*)>/i
#~     end
#~   end
#~ end

#~ #==============================================================================
#~ # ** RPG::BaseItem
#~ #------------------------------------------------------------------------------
#~ #==============================================================================
#~ class RPG::BaseItem
#~   
#~   alias iex_mana_link_cache_rpgbi_initialize initialize unless $@
#~   def initialize(*args)
#~     iex_mana_link_cache_rpgbi_initialize(*args)
#~     @iex_mana_done_cache = false
#~     iex_mana_link_cache
#~   end
#~   
#~   def iex_mana_link_cache
#~     @iex_mana_link = false
#~     @iex_mana_link_id = nil
#~     @iex_mana_done_cache = false
#~     self.note.split(/[\r\n]+/).each { |line|
#~     case line
#~     when IEX::REGEXP::MANA_LINK::MANA_LINK
#~       @iex_mana_link = true
#~     when IEX::REGEXP::MANA_LINK::MANA_LINK_ID
#~       @iex_mana_link = true
#~       @iex_mana_link_id = $1
#~     end  
#~     }
#~     @iex_mana_done_cache = true
#~   end
#~   
#~   def iex_mana_link?
#~     iex_mana_link_cache if @iex_mana_link == nil
#~     return @iex_mana_link
#~   end  
#~     
#~   def iex_mana_link_id
#~     iex_mana_link_cache if @iex_mana_done_cache
#~     return @iex_mana_link_id
#~   end
#~   
#~ end

#~ #==============================================================================
#~ # ** Game_Battler
#~ #------------------------------------------------------------------------------
#~ #==============================================================================
#~ class Game_Battler
#~   
#~   def iex_mana_link?
#~     return false
#~   end

#~ end

#~ #==============================================================================
#~ # ** Game_Actor
#~ #------------------------------------------------------------------------------
#~ #==============================================================================
#~ class Game_Actor < Game_Battler
#~     
#~   def iex_mana_link?
#~     for eq in equips
#~       next if eq == nil
#~       if eq.iex_mana_link?
#~         return true
#~       end  
#~     end
#~     super
#~   end
#~   
#~   def iex_mana_link_id?(id)
#~     for eq in equips
#~       next if eq == nil
#~       if eq.iex_mana_link_id == id
#~         return true
#~       end  
#~     end
#~     return false
#~   end
#~   
#~   def iex_mana_link_types
#~     link_ids = []
#~     for eq in equips
#~       next if eq == nil
#~       if eq.iex_mana_link?
#~         link_ids.push(eq.iex_mana_link_id)
#~       end  
#~     end
#~     return link_ids
#~   end
#~   
#~   def iex_mana_link_actors(link_id)
#~     acts = []
#~     for act in $game_party.members
#~       next if act == nil
#~       next if act == self
#~       if act.iex_mana_link?
#~         if act.iex_mana_link_id?(link_id)
#~           acts.push(act)
#~         end  
#~       end  
#~     end  
#~     return acts
#~   end
#~   
#~ end

#~ #==============================================================================
#~ # ** Scene_Battle
#~ #------------------------------------------------------------------------------
#~ #==============================================================================
#~ class Scene_Battle < Scene_Base
#~   
#~   #--------------------------------------------------------------------------
#~   # * Execute Battle Action: Skill
#~   #--------------------------------------------------------------------------
#~   alias iex_mana_link_execute_action_skill execute_action_skill unless $@
#~   def execute_action_skill(*args)
#~     iex_mana_link_execute_action_skill(*args)
#~     if @active_battler.iex_mana_link?
#~       for link_id in @active_battler.iex_mana_link_types
#~         next if link_id == nil
#~         acts = @active_battler.iex_mana_link_actors(link_id)
#~         skill = @active_battler.action.skill
#~         iex_mp_cost = @active_battler.calc_mp_cost(skill)
#~         mp_div = iex_mp_cost / acts.size
#~         @active_battler.mp += mp_div
#~         for linked_actor in acts  
#~           mp_shave = linked_actor.mp
#~           mp_shave -= iex_mp_cost
#~           if mp_shave < 0
#~             iex_mp_cost += -mp_shave
#~           end  
#~           linked_actor.mp -= iex_mp_cost
#~           iex_mp_cost -= mp_div
#~         end
#~         iex_mp_cost = 0 if iex_mp_cost < 0
#~         @active_battler.mp -= iex_mp_cost
#~       end  
#~     end  
#~   end
#~   
#~ end

