# IEX - Spellbook System - Core
#==============================================================================#
# ** IEX(Icy Engine Xelion) - Spellbook System
#------------------------------------------------------------------------------#
# ** Created by    : IceDragon (http://www.rpgmakervx.net/)
# ** Script-Status : Addon (Actors, States)
# ** Script Type   : Skills Learning
# ** Date Created  : 12/30/2010
# ** Date Modified : 01/01/2010
# ** Script Tag    : IEX - Spellbook System
# ** Difficulty    : Hard, Lunatic
# ** Version       : 1.0
#------------------------------------------------------------------------------#
#==============================================================================#
# ** INTRODUCTION
#------------------------------------------------------------------------------#
# Spellbooks?, well actually its a take from the FF6 Esper system
# I just didn't want to call it espers, since I was using spellbooks.
# So yeah...
#------------------------------------------------------------------------------#
#==============================================================================#
# ** FEATURES
#------------------------------------------------------------------------------#
# V1.0 - Notetags - States
#------------------------------------------------------------------------------#
# <SPELL_BOOK>
#------------------------------------------------------------------------------#
#==============================================================================#
# ** COMPATABLITIES
#------------------------------------------------------------------------------#
#
# BEM, Yggdrasil, Probably Takentai not sure about GTBS
#
#------------------------------------------------------------------------------#
#==============================================================================#
# ** INSTALLATION
#------------------------------------------------------------------------------#
# 
# Below 
#  Materials
#
# Above 
#   Main
#
#------------------------------------------------------------------------------#
#==============================================================================#
# ** CHANGES 
#------------------------------------------------------------------------------#
# Classes
#   RPG::State
#     new-method :iex_spellBook_cache
#     new-method :spellbook?
#     new-method :spells
#     new-method :spell_rate
#   Game_Party
#     alias      :initialize
#     new-method :gain_spellbook
#     new-method :lose_spellbook
#     new-method :spellbook_count
#     new-method :spellbooks
#   Game_Actor
#     alias      :initialize
#     alias      :skills
#     new-method :spellbooks
#     new-method :spellgrowth_rate
#     new-method :spell_points
#     new-method :increase_spell_points
#     new-method :decrease_spell_points
#     new-method :gain_spell_skill
#     new-method :spellbook_skills
#   Window_Base
#     new-method :draw_spellbook_object
#
#------------------------------------------------------------------------------#
#==============================================================================#
# ** CHANGE LOG
#------------------------------------------------------------------------------#
# 
#  12/29/2010 - BETA  Started Script
#  01/08/2011 - V1.0  Finished Script
#
#------------------------------------------------------------------------------#
#==============================================================================#
# ** KNOWN ISSUES
#------------------------------------------------------------------------------#
#  Non at the moment. 
#
#------------------------------------------------------------------------------#
$imported = {} if $imported == nil
$imported["IEX_SpellbookSystem"] = true

#==============================================================================
# ** IEX::SPELL_BOOK
#------------------------------------------------------------------------------
#==============================================================================
module IEX
  module SPELL_BOOK
    # 0 - Full Books, 1 - Pages
    BOOK_MODE = 1
    
    MAX_EQUIP = 2
    TOTAL_SPELLBOOKS = 27
    
    CommonBooks = []
    ACTOR_EQUIP_BOOKS = {}
    ACTOR_EQUIP_BOOKS[0] = CommonBooks
    ACTOR_EQUIP_BOOKS[1] = CommonBooks + [
    ]
    ACTOR_EQUIP_BOOKS[2] = CommonBooks + [
    ]
    #BOOKS
    
  end
end

class RPG::BaseItem
  
  def iex_spellBook_cache
    @spellbook_cache_complete = false
    @spellbook = false
    @spells = {}
    @pagespells = {}
    active = false
    self.note.split(/[\r\n]+/).each { |line|
    case line
    when /<(?:SPELLBOOK|SPELL_BOOK|spell book)>/i
      @spellbook = true
      active = true
    when /<\/(?:SPELLBOOK|SPELL_BOOK|spell book)>/i  
      @spellbook = true
      active = false
    when /(?:PAGESPELL|PAGESKILL)(\d+)[ ](\d+):[ ]*(\d+)/i 
      if active 
        @spellbook = true
        # ID = Page
        @pagespells[$2.to_i] = $1.to_i
        # ID = Rate
        @spells[$2.to_i] = $3.to_i
      end
    when /(?:SPELL|SKILL)[ ](\d+):[ ]*(\d+)/i
      if active 
        @spellbook = true
        # ID = Rate
        @spells[$1.to_i] = $2.to_i
      end  
    end  
    }
    active = false
    @spellbook_cache_complete = true
  end
  
  def spellbook?
    iex_spellBook_cache unless @spellbook_cache_complete  
    return @spellbook 
  end
  
  def pagespell_at(sid)
    iex_spellBook_cache unless @spellbook_cache_complete 
    return @pagespells[sid]
  end
  
  def pagespell?(sid)
    iex_spellBook_cache unless @spellbook_cache_complete  
    return !@pagespells[sid].nil?
  end
  
  def spells
    iex_spellBook_cache unless @spellbook_cache_complete
    return @spells.keys.sort
  end
  
  def spell_rate(skill_id)
    iex_spellBook_cache unless @spellbook_cache_complete
    return @spells[skill_id] if @spells.has_key?(skill_id)
    return 0
  end
  
end
  
class Game_Party < Game_Unit
  
  alias iex_spellBookPrt_initialize initialize unless $@
  def initialize(*args)
    iex_spellBookPrt_initialize(*args)
    @spellbooks = {}
    @spellpages = {}
  end
  
  def gain_spellbook(sid, value)
    sid = sid.id if sid.is_a?(RPG::Item)
    sid = sid.to_i
    return if $data_items[sid] == nil
    return unless $data_items[sid].spellbook?
    @spellbooks[sid] = 0 if @spellbooks[sid].nil?
    @spellbooks[sid] = [@spellbooks[sid]+value, 0].max
  end
  
  def lose_spellbook(sid, value)
    gain_spellbook(sid, -value)
  end
  
  def spellbook_count(sid)
    sid = sid.id if sid.is_a?(RPG::Item)
    sid = sid.to_i
    @spellbooks[sid] = 0 if @spellbooks[sid].nil?
    return @spellbooks[sid]
  end
  
  def spellbooks
    result = []
    for st in @spellbooks.keys.sort
      next unless st.to_i > 0
      result << $data_items[st] #if spellbook_count(st) > 0
    end
    return result
  end
  
  #                  spellbook id, skill_id
  def has_spellpage?(spid, sid)
    sid = sid.id if sid.is_a?(RPG::Skill)
    spid = spid.id if spid.is_a?(RPG::Item)
    return true unless $data_items[spid].pagespell?(sid)
    return @spellpages[spid].include?(sid)
  end
  
  def get_spellpage(spid, sid)
    sid = sid.id if sid.is_a?(RPG::Skill)
    spid = spid.id if spid.is_a?(RPG::Item)
    return 0 unless $data_items[spid].pagespell?(sid)
    return $data_items[spid].pagespell_at(sid)
  end
  
end

class Game_Actor < Game_Battler
  
  alias iex_spellBookAct_setup setup unless $@
  def setup(*args)
    iex_spellBookAct_setup(*args)
    @spellbooks = Array.new(IEX::SPELL_BOOK::MAX_EQUIP).map { |a| a=0}
    @spell_points = {}
    #@skills = []
  end
  
  def equip_spellbook(index, book=0)
    @spellbooks = [] if @spellbooks.nil?
    sid = book
    sid = book.id if book.is_a?(RPG::UsableItem)
    @spellbooks[index] = sid.to_i
  end
  
  def get_spellbook_at(index)
    @spellbooks = [] if @spellbooks.nil?
    return @spellbooks[index]
  end  
  
  def spellbooks
    @spellbooks = [] if @spellbooks.nil?
    result = []
    for sid in @spellbooks
      result << $data_items[sid]
    end  
    return result 
  end
  
  def spellgrowth_rate(skill_id)
    rate = 0
    for spellbook in spellbooks.compact!
      rate += spellbook.spell_rate(skill_id)    
    end 
    return rate
  end
  
  def spell_points(skill_id)
    @spell_points = {} if @spell_points.nil?
    @spell_points[skill_id] = 0 if @spell_points[skill_id].nil?
    @spell_points[skill_id] = 100 if @skills.include?(skill_id)
    return @spell_points[skill_id]
  end
  
  def increase_spell_points(skill_id, value)
    @spell_points = {} if @spell_points.nil?
    @spell_points[skill_id] = 0 if @spell_points[skill_id].nil?
    @spell_points[skill_id] = 100 if @skills.include?(skill_id)
    rate = spellgrowth_rate(skill_id)
    @spell_points[skill_id] += [[value * rate, 0].max, 100].min
    gain_spell_skill(skill_id) if @spell_points[skill_id] == 100
    return @spell_points[skill_id]
  end
  
  def decrease_spell_points(skill_id, value)
    increase_spell_points(skill_id, -value)
  end
  
  def gain_spell_skill(skill_id)
    @skills |= [skill_id]
  end
  
  def spellbook_skills
    result = []
    spbks = spellbooks.compact
    for spellbook in spbks
      for sp in spellbook.spells
        next unless $game_party.has_spellpage?(spellbook.id, sp)
        next if @skills.include?(sp)
        ski = $data_skills[sp]
        result |= [ski] 
      end  
    end  
    return result
  end
  
  alias iex_spellBookAct_skills skills unless $@
  def skills
    result = iex_spellBookAct_skills
    result |= spellbook_skills
    return result
  end
  
end  

class Window_Base < Window
  
  def draw_spellbook_object(obj, rx, y = 0, width = 128, height = 24)
    if rx.is_a?(Rect)
      x = rx.x
      y = rx.y
      width = rx.width
      height = rx.height
    else
      x = rx
    end  
    draw_icon(obj.icon_index, x, y)
    old_size = self.contents.font.size 
    self.contents.font.size = 18
    #self.contents.font.color = normal_color
    self.contents.draw_text(x + 36, y, width, height, obj.name)
    self.contents.font.size = old_size
  end
  
end

class Window_SpellBook < Window_Selectable
  
  def initialize(spbook, x, y, width, height)
    super(x, y, width, height)
    self.index = -1
    @spellbook = spbook
    @actor = nil
    refresh
  end
  
  def change_actor(new_actor)
    if new_actor != @actor
      @actor = new_actor
      refresh
    end  
  end
  
  def change_book(new_book)
    if new_book != @spellbook
      @spellbook = new_book
      refresh
    end  
  end
  
  #--------------------------------------------------------------------------
  # * Draw Item Name
  #     item    : Item (skill, weapon, armor are also possible)
  #     x       : draw spot x-coordinate
  #     y       : draw spot y-coordinate
  #     enabled : Enabled flag. When false, draw semi-transparently.
  #--------------------------------------------------------------------------
  def draw_item_name(item, x, y, enabled = true)
    if item != nil
      draw_icon(item.icon_index, x, y, enabled)
      self.contents.font.color = normal_color
      self.contents.font.color.alpha = enabled ? 255 : 128
      self.contents.draw_text(x + 24, y, 196, WLH, item.name)
    end
  end
  
  def refresh
    self.contents.clear
    create_contents
    return if @spellbook == nil
    spells = @spellbook.spells
    @item_max = spells.size
    count = 0
    self.contents.font.size = 18
    draw_item_name(@spellbook, 4, 4)
    yoff = 32
    for sp in spells
      ski = $data_skills[sp]
      rect = Rect.new(4, 4+yoff+ (count*24), self.contents.width-24, 24)
      self.contents.font.color = normal_color
      nrect = rect.clone ; nrect.height = 20
      draw_spellbook_object(ski, nrect)
      self.contents.font.color = system_color
      self.contents.draw_text(nrect, @spellbook.spell_rate(sp), 2)
      self.contents.font.color = normal_color
      rect.width -= 32; self.contents.draw_text(rect, "x", 2)
      val = 0
      val = @actor.spell_points(sp) unless @actor.nil?
      max = 100
      draw_grad_bar(rect.x+28, rect.y+rect.height-8, rect.width - 92, 8, 
        val.abs, max, # Values
        mp_gauge_color1, mp_gauge_color2, Color.new(20, 20, 20, 168), # Colors
        2)
      count += 1
    end  
  end
  
end  

class Window_SpellShelf < Window_Selectable
  
  def initialize(x, y, width, height)
    @rect_size = 32
    @selection_size = 42
    super(x, y, width, height)
    self.index = 0
    @column_max = 6
    refresh
  end
  
  def current_book
    return @data[self.index]
  end
  
  def refresh
    @data = [] ; @data = $game_party.spellbooks ; @data << nil
    @item_max = @data.size
    self.contents.clear
    create_contents
    for i in 0...@data.size  
      draw_item(i)
    end
  end
  
  #--------------------------------------------------------------------------
  # * Get rectangle for displaying items
  #     index : item number
  #--------------------------------------------------------------------------
  def item_rect(index)
  	rect = Rect.new(0, 0, 0, 0)
  	rect.width = @selection_size
  	rect.height = @selection_size
    spac = @selection_size * @column_max
    spac = self.contents.width / spac
    #spac /= 2
  	rect.x = index % @column_max * (rect.width+spac)
    rect.y = index / @column_max * rect.height
    rect.x += spac
    rect.y += spac
  	return rect
  end
  
  def draw_item(index)
    rect = item_rect(index)
    rect.width = @rect_size
  	rect.height = @rect_size
    rect.x += (@selection_size - @rect_size) / 2
    rect.y += (@selection_size - @rect_size) / 2
    
    emb = @data[index]
    enabled = enabled?(emb)
    number = $game_party.spellbook_count(emb)
    color1 = Color.new(166, 124, 82, 128)
    color2 = Color.new(126, 84, 42)
    self.contents.fill_rect(rect, color1)
    draw_border_rect(rect.x, rect.y, rect.width, rect.height, 4, color2)
    draw_icon(emb.icon_index, rect.x+4, rect.y+4, enabled) unless emb.nil?
    self.contents.font.size = 16
    self.contents.draw_text(rect.x, rect.y-8, rect.width, 24, "x#{number}", 2) 
  end
  
  def enabled?(spellbook)
    return $game_party.spellbook_count(spellbook) > 0
  end
  
end 

#==============================================================================
# ** Window_PartySpellbook
#------------------------------------------------------------------------------
#==============================================================================
class Window_PartySpellbook < Window_PartyReserve
      
  def refresh
	  self.contents.clear
	  @actors = $game_party.all_members
    @item_max = @actors.size
    @column_max = [@item_max,1].max
    create_contents
	  for i in 0...@item_max
  	  draw_item(i)
  	end
  end
    
  def draw_item(index)
    rect = item_rect(index)
    self.contents.clear_rect(rect)
	  actor = @actors[index]
    unless actor.nil?
	    aname  = actor.name
	    aclass = actor.class.name
	    alevel = actor.level
      ash = (rect.height-@character_rect.height)/2
	    locked = false
      ch = @character_rect.height
      draw_actor_graphic(actor, rect.x+16, rect.y+@character_rect.height+ash+4, !locked)
      coun = 0
      for i in 0...IEX::SPELL_BOOK::MAX_EQUIP
        sp = actor.spellbooks[i]
        draw_icon(sp.icon_index, rect.x+rect.width-24-(24*coun), rect.y) unless sp.nil?
        coun += 1
      end  
	    self.contents.font.color.alpha = locked ? 128 : 255
	    self.contents.font.size = 12
	    self.contents.draw_text(rect.x+3,rect.y - 4,rect.width,24,aname)
	    self.contents.draw_text(rect.x,rect.y+18,rect.width-2,24,"Lv.",2)
	    self.contents.draw_text(rect.x,rect.y+28,rect.width-2,24,alevel,2)
	    self.contents.draw_text(rect.x,rect.y+17+ch,rect.width-3,24,aclass,2)
	  end
  end
  
end

class Window_SpellbookEquip < Window_Selectable
  
  def initialize(x, y, width)
    super(x, y, width, 128)
    self.height = 32 + (WLH*IEX::SPELL_BOOK::MAX_EQUIP)
    @actor = nil
    self.index = 0
    refresh
  end
  
  def change_actor(new_actor)
    if new_actor != @actor
      @actor = new_actor
      refresh
    end  
  end
  
  def refresh
    self.contents.clear
    create_contents
    @item_max = IEX::SPELL_BOOK::MAX_EQUIP
    rect = Rect.new(0, 0, self.width+96, WLH)
    for i in 0...IEX::SPELL_BOOK::MAX_EQUIP
      sp = nil
      sp = @actor.spellbooks[i] unless @actor.nil?
      unless sp.nil?
        draw_item_name(sp, rect.x, rect.y) 
      else
        self.contents.draw_text(rect.x, rect.y, 
          rect.width, rect.height,
          "----------------------------------")
      end  
      rect.y += 24
    end  
  end
  
end  
