# IEX - Map Info Builder
$imported = {} if $imported == nil
$imported["IEX_MapInfoBuilder"] = true

class RPG::MapInfo
  
  def real_name
    return @name
  end
  
end

module IEX
  module MAP_INFO_KIT
    
    def self.break_down(mapinfo, outputname)
      file = File.new(outputname, "a")
      for key in mapinfo.keys
        tx = ''
        tx += "<MAP_DATA: #{key}> \n"
        tx += "\n"
        tx += "NAME: #{mapinfo[key].real_name} \n"
        tx += "PARENT_ID: #{mapinfo[key].parent_id} \n"
        tx += "ORDER: #{mapinfo[key].order} \n"
        tx += "EXPANDED: #{mapinfo[key].expanded.to_s.upcase} \n"
        tx += "SCROLL_X: #{mapinfo[key].scroll_x} \n"
        tx += "SCROLL_Y: #{mapinfo[key].scroll_y} \n"
        tx += "\n"
        tx += "</MAP_DATA> \n"
        tx += "\n"
        file.write(tx)
      end  
      file.close
    end
    
    def self.rebuild(text, output_name)
      new_map_info = {}
      last_map_id = 0
      scan_on = false
      text.each_line { |line| 
      case line.to_s
      when /<(?:MAP|MAP_DATA):[ ]*(\d+)>/i
        last_map_id = $1.to_i
        new_map_info[last_map_id] = RPG::MapInfo.new
        scan_on = true
      when /<\/(?:MAP|MAP_DATA)>/i
        scan_on = false
      when /(?:NAME):[ ]*(.*)/i
        if scan_on
          new_map_info[last_map_id].name = $1.to_s
        end  
      when /(?:PARENT_ID|parent id):[ ]*(\d+)/i
        if scan_on
          new_map_info[last_map_id].parent_id = $1.to_i
        end   
      when /(?:ORDER):[ ]*(\d+)/i
        if scan_on
          new_map_info[last_map_id].order = $1.to_i
        end    
      when /(?:EXPANDED):[ ]*(\w+)/i
        if scan_on
          case $1.to_s
          when /(?:TRUE|ON)/i
            new_map_info[last_map_id].expanded = true
          when /(?:FALSE|OFF)/i  
            new_map_info[last_map_id].expanded = false
          end  
        end
      when /(?:SCROLL_|scroll )(\w+):[ ]*(\d+)/i
        if scan_on  
          val = $2.to_i
          case $1.to_s.upcase
          when "X"
            new_map_info[last_map_id].scroll_x = val
          when "Y"  
            new_map_info[last_map_id].scroll_y = val
          end  
        end  
      end  
      }
      save_data(new_map_info, output_name)
      return new_map_info
    end
    
  end
end
