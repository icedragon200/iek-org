# ICY_Party_Max_Break
$imported = {} if $imported == nil
$imported["ICY_Max_Party_Break"] = true

class Game_Party < Game_Unit
  attr_accessor :party_max
  #--------------------------------------------------------------------------
  # * Constants
  #--------------------------------------------------------------------------
  MAX_MEMBERS = 6 
  
  alias max_members_addi_initialize initialize unless $@
  def initialize(*args)
    max_members_addi_initialize(*args)
    @party_max = MAX_MEMBERS
  end
  
end
