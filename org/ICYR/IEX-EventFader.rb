# IEX - Event Fader
#==============================================================================#
# ** IEX(Icy Engine Xelion) - Event Fader
#------------------------------------------------------------------------------#
# ** Created by    : IceDragon (http://www.rpgmakervx.net/)
# ** Script-Status : Addon (Event)
# ** Script Type   : Passabilty Changes
# ** Date Created  : 10/19/2010 (DD/MM/YYYY)
# ** Date Modified : 01/08/2011 (DD/MM/YYYY)
# ** Script Tag    : IEX - Event Fader
# ** Difficulty    : Normal
# ** Version       : 1.0a
#------------------------------------------------------------------------------#
#==============================================================================#
# ** INTRODUCTION
#------------------------------------------------------------------------------#
# Whats the purpose of this script you ask?
# Well its just me, being the lazy person I am, wanting to fade an event
# when the player was on the same tile as it, without eventing.
# 
#------------------------------------------------------------------------------#
#==============================================================================#
# ** HOW TO USE 
#------------------------------------------------------------------------------#
# Tags - Comments - Events
#------------------------------------------------------------------------------#
# <fader_effect> (or) <fade effect>
# Yup when the player is on the same tile as the event, it will fade out
# and when not, it fades back in.
#
#------------------------------------------------------------------------------#
#==============================================================================#
# ** COMPATABLITIES
#------------------------------------------------------------------------------#
#
# Should have no problems
#
#------------------------------------------------------------------------------#
#==============================================================================#
# ** INSTALLATION
#------------------------------------------------------------------------------#
# 
# Below 
#  Materials
#  Anything that makes changes to events.
#
# Above 
#   Main
#
#------------------------------------------------------------------------------#
#==============================================================================#
# ** CHANGES 
#------------------------------------------------------------------------------# 
# Classes
#   Game_Event
#     alias      :setup
#     alias      :update
#     new-method :set_special_effect
#     new-method :update_fader_effect
#
#------------------------------------------------------------------------------#
#==============================================================================#
# ** CHANGE LOG
#------------------------------------------------------------------------------#
# (DD/MM/YYYY)
#  11/29/2010 - V1.0  Finished Script
#  01/02/2011 - V1.0  Released Script
#  01/08/2011 - V1.0a Small Changes
#
#------------------------------------------------------------------------------#
#==============================================================================#
# ** KNOWN ISSUES
#------------------------------------------------------------------------------#
#  Non at the moment. 
#
#------------------------------------------------------------------------------#
$imported = {} if $imported == nil 
$imported["IEX_EventFader"] = true

#==============================================================================
# ** Game_Event
#------------------------------------------------------------------------------
#==============================================================================
class Game_Event < Game_Character
  
  alias iex_event_fader_setup setup unless $@
  def setup(*args)
    iex_event_fader_setup(*args)
    set_special_effect
  end
  
  def set_special_effect
    @fader_effect = false
    if @list != nil
      for i in 0...@list.size
        if @list[i].code == 108 
          @list[i].parameters.to_s.split(/[\r\n]+/).each { |line| 
          case line
          when /<(?:FADER_EFFECT|fader effect|FADE_EFFECT|fade effect)>/i
            @fader_effect = true
          end   }
        end
      end
    end
  end
   
  alias iex_event_fader_update update unless $@
  def update(*args)
    if @fader_effect
      update_fader_effect
    end
    iex_event_fader_update(*args)
  end
  
  def update_fader_effect
    if $game_player.x == self.x and $game_player.y == self.y
      @opacity -= 255 / 60 unless @opacity <= 128
      if @opacity < 128
        @opacity = 128
      end  
    else
      @opacity += 255 / 60 unless @opacity == 255
      if @opacity > 255
        @opacity = 255
      end  
    end
  end
  
end

