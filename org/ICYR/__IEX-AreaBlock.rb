# #~IEX - Area Block
#~ #------------------------------------------------------------------------------#
#~ # ** IEX(Icy Engine Xelion) - Area Block
#~ # ** Created by : IceDragon
#~ # ** Script-Status : Addon
#~ # ** Date Created : 9/11/2010
#~ # ** Date Modified : 9/11/2010
#~ # ** Version : 1.0
#~ #------------------------------------------------------------------------------#
#~ # This puts those good for nothing Areas to use!
#~ #
#~ # FEATURES
#~ # Allows you make an area on the map Impassable by simply creating an area and 
#~ # putting <BLOCK> in its name.
#~ # 
#~ # You can also limit movemnt to inside the area by using <LIMIT>
#~ # 
#~ #------------------------------------------------------------------------------#
#~ # CHANGES
#~ # Totally overwrote the Game_Map Passable method.
#~ #
#~ #------------------------------------------------------------------------------#
#~ # CHANGE LOG
#~ # V 1.0 9/12/2010 Finished Script.
#~ #
#~ #------------------------------------------------------------------------------#
#~ # KNOWN ISSUES
#~ # Non at the moment.
#~ #
#~ #------------------------------------------------------------------------------#
#~ $imported = {} if $imported == nil
#~ $imported["IEX_Area_Block"] = true

#~ #==============================================================================
#~ # ** RPG::Area
#~ #------------------------------------------------------------------------------
#~ #==============================================================================  
#~ class RPG::Area
#~    
#~   alias iex_area_blocker_limiter_initialize initialize unless $@
#~   def initialize(*args)
#~     iex_area_blocker_limiter_initialize(*args)
#~     iex_lim_blck_cache
#~   end
#~   
#~   def iex_lim_blck_cache
#~     @blocker = @name.scan(/\<BLOCK\>/i).size > 0
#~     @limiter = @name.scan(/\<LIMIT\>/i).size > 0
#~   end
#~   
#~   def blocker_area?
#~     iex_lim_blck_cache if @blocker == nil
#~     return @blocker
#~   end
#~   
#~   def limiter_area?
#~     iex_lim_blck_cache if @limiter == nil
#~     return @limiter
#~   end
#~   
#~   def valid_area?(x_x, y_y)
#~     stack_ans = []
#~     if x_x >= @rect.x and x_x < @rect.x + @rect.width
#~       stack_ans.push(true)
#~     else
#~       stack_ans.push(false)
#~     end
#~     if y_y >= @rect.y and y_y < @rect.y + @rect.height
#~       stack_ans.push(true)
#~     else
#~       stack_ans.push(false)
#~     end
#~     return stack_ans
#~   end
#~   
#~ end

#~ #==============================================================================
#~ # ** Game Map
#~ #------------------------------------------------------------------------------
#~ #==============================================================================  
#~ class Game_Map
#~   #--------------------------------------------------------------------------
#~   # * Determine if Passable
#~   #     x    : x coordinate
#~   #     y    : y coordinate
#~   #     flag : The impassable bit to be looked up
#~   #            (normally 0x01, only changed for vehicles)
#~   #--------------------------------------------------------------------------
#~   def passable?(x, y, flag = 0x01)
#~     stack = []
#~     for area in $data_areas.values
#~       next if area == nil
#~       next if area.map_id != $game_map.map_id
#~       if area.limiter_area? and $game_player.in_area?(area)
#~         av = area.valid_area?(x, y)
#~         if av.all?
#~           stack.push(true)  
#~         else
#~           stack.push(false)
#~         end
#~       elsif area.blocker_area? 
#~         av = area.valid_area?(x, y)
#~         if av.all?
#~           stack.push(false)  
#~         end
#~       end
#~     end
#~     for event in events_xy(x, y)            # events with matching coordinates
#~       next if event.tile_id == 0            # graphics are not tiled
#~       next if event.priority_type > 0       # not [Below characters]
#~       next if event.through                 # pass-through state
#~       pass = @passages[event.tile_id]       # get passable attribute
#~       next if pass & 0x10 == 0x10           # *: Does not affect passage
#~       if pass & flag == 0x00                # o: Passable
#~         unless stack.empty?
#~           return stack.all? 
#~         else
#~           return true
#~         end
#~       end  
#~       return false if pass & flag == flag   # x: Impassable
#~     end
#~     for i in [2, 1, 0]                      # in order from on top of layer
#~       tile_id = @map.data[x, y, i]          # get tile ID
#~       return false if tile_id == nil        # failed to get tile: Impassable
#~       pass = @passages[tile_id]             # get passable attribute
#~       next if pass & 0x10 == 0x10           # *: Does not affect passage
#~       if pass & flag == 0x00                # o: Passable
#~         unless stack.empty?
#~           return stack.all? 
#~         else
#~           return true
#~         end
#~       end 
#~       return false if pass & flag == flag   # x: Impassable
#~     end
#~     return false                            # Impassable
#~   end
#~   
#~ end
#~   
