# #~IEX - Actor Status Bypass
#~ #------------------------------------------------------------------------------#
#~ # ** IEX(Icy Engine Xelion) - Actor Status (Bypass Party)
#~ # ** Created by : IceDragon
#~ # ** Script-Status : Addon
#~ # ** Date Created : 10/1/2010
#~ # ** Date Modified : 10/1/2010
#~ # ** Request By : chriskay
#~ # ** Version : 1.0
#~ #------------------------------------------------------------------------------#
#~ #
#~ # HOW TO USE
#~ #
#~ # In a script call.
#~ # Use this
#~ #  call_actor_status(actor_id) actor_id being whatever actor you want
#~ #
#~ #==============================================================================
#~ # ** Scene_Status
#~ #------------------------------------------------------------------------------
#~ #==============================================================================
#~ $iex_actor_status = nil # Actor Id

#~ class Scene_Status < Scene_Base
#~   
#~   if $imported["StatusMenuMelody"]
#~     #--------------------------------------------------------------------------
#~     # overwrite method: start
#~     #--------------------------------------------------------------------------
#~     def start
#~       super
#~       create_menu_background
#~       if $iex_actor_status == nil
#~         @actor = $game_party.members[@actor_index]
#~       else
#~         @actor = $game_actors[$iex_actor_status]
#~       end
#~       @status_window = Window_Actor_Status.new(@actor)
#~       @help_window = Window_Help.new
#~       @help_window.y = @status_window.height
#~       create_command_window
#~     end
#~   
#~   elsif $imported["SceneStatusReDux"]
#~       #--------------------------------------------------------------------------
#~       # overwrite initialize
#~       #--------------------------------------------------------------------------
#~       alias iex_stat_for_non_redux_initialize initialize unless $@
#~       def initialize(*args)
#~         iex_stat_for_non_redux_initialize(*args)
#~         if $iex_actor_status != nil
#~           @actor = $game_actors[$iex_actor_status]
#~         end
#~       end
#~   else
#~     
#~     #--------------------------------------------------------------------------
#~     # * Start processing
#~     #--------------------------------------------------------------------------
#~     alias iex_stat_for_non_party_start start unless $@
#~     def start(*args)
#~       iex_stat_for_non_party_start(*args)
#~       if $iex_actor_status == nil
#~         @actor = $game_party.members[@actor_index]
#~       else
#~         @actor = $game_actors[$iex_actor_status]
#~       end
#~       @status_window.dispose if @status_window != nil
#~       @status_window = Window_Status.new(@actor)
#~     end
#~    end
#~   
#~   
#~   #--------------------------------------------------------------------------
#~   # * Return to Original Screen
#~   #--------------------------------------------------------------------------
#~   alias iex_stat_for_non_party_return_scene return_scene unless $@
#~   def return_scene(*args)
#~     if $iex_actor_status == nil
#~       iex_stat_for_non_party_return_scene(*args)
#~     else
#~       $scene = Scene_Map.new
#~     end  
#~     $iex_actor_status = nil
#~     if $imported["SceneStatusReDux"]
#~       $game_temp.status_menu_flag = false
#~     end  
#~   end
#~   
#~ end

#~ #==============================================================================
#~ # ** Game_Interpreter
#~ #------------------------------------------------------------------------------
#~ #==============================================================================
#~ class Game_Interpreter
#~   
#~   def call_actor_status(actor_id)
#~     $iex_actor_status = actor_id
#~     $scene = Scene_Status.new
#~   end
#~   
#~ end
