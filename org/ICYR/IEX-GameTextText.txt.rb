# IEX - Game Text >> Text.txt
#~ # Text >> .txt
#~ # Created By IceDragon
#~ # Date Created : 01/20/2011
#~ # -- Description
#~ # This script outputs all the event text in your game to text files
#~ # They are seperated like this
#~ # Map >> Event >> Page on which they appear.txt
#~ #
#~ # Common Events are also processed
#~ # CommonEvents >> Event >> Page
#~ # -- Installation
#~ # Below "Everything"
#~ # Above Main
#~ #
#~ # I reccomend Deleting any old output from this script
#~ # Because it will simply append to the existing files if not.
#~ # We don't want confusion now.
#~ module IEX
#~   module TEXT_TO_TXT
#~     
#~     OUTPUT_FOLDER = "Text_Output"
#~     # No touching this
#~     def self.map_events_text(map, map_id)
#~       main_text = OUTPUT_FOLDER
#~       @map_infos = load_data("Data/MapInfos.rvdata") if @map_infos.nil?
#~       # ---------------------------------------------------------#
#~       # Name of Map folder
#~       name = @map_infos[map_id.to_i].name
#~       # Create main Output Directory
#~       if !FileTest.directory?(main_text) ; Dir.mkdir(main_text) end
#~       # Create Map Output Directory
#~       if !FileTest.directory?("#{main_text}/Maps") ; Dir.mkdir("#{main_text}/Maps") end
#~       mapfold_name = sprintf("Maps/MAP%03d-(%s)", map_id.to_i, name)
#~       mapfold_name.gsub!(/([ ]*)\)/) { ")" } # Remove White Space Before Bracket
#~       # ---------------------------------------------------------#
#~       # Go Through All Events on the Map
#~       map.events.keys.each { |eid| event = map.events[eid]        
#~       # ---------------------------------------------------------#
#~         event_text(event, eid, main_text, mapfold_name, 0)
#~       # ---------------------------------------------------------#  
#~       } # Maps
#~       # ---------------------------------------------------------#
#~     end
#~     
#~     def self.common_events_to_text(data)
#~       main_text = OUTPUT_FOLDER
#~       data.each { |event| next if event == nil
#~         event_text(event, event.id, main_text, "CommonEvents", 1) } 
#~     end
#~     
#~     def self.event_text(event, eid, main_text, mapfold_name, type)
#~       # Name of Event folder
#~       case type
#~       when 0 ; 
#~         evfold_name = sprintf("EV%03d-(%s)", eid, event.name)
#~         page_count = 0 # Used when naming txt files
#~         event.pages.each { |page| # Go Through Events Pages
#~           list_count = 0
#~       # ---------------------------------------------------------# 
#~           list_count = process_page(page, main_text, mapfold_name, evfold_name, 
#~             list_count, page_count)
#~           page_count += 1
#~       # ---------------------------------------------------------#  
#~         } # End Events
#~       when 1 ; 
#~         evfold_name = sprintf("CMEV%03d-(%s)", eid, event.name)
#~       # ---------------------------------------------------------# 
#~         list_count = process_page(event, main_text, mapfold_name, evfold_name)
#~       # ---------------------------------------------------------#  
#~         # End Common Events
#~       end  
#~       
#~     end
#~     
#~     def self.process_page(page, main_text, mapfold_name, evfold_name, 
#~       list_count = 0, page_count = 0)
#~       page.list.each { |evc|  # Event Commands 
#~         if evc.code == 401 #101(Show Text) # Text Command?
#~       # ---------------------------------------------------------#        
#~           # Create Map Output Directory
#~           if !FileTest.directory?(main_text+"/"+mapfold_name) 
#~             Dir.mkdir(main_text+"/"+mapfold_name) 
#~           end
#~           # Create Event Output Directory
#~           if !FileTest.directory?(main_text+"/"+mapfold_name+"/"+evfold_name)  
#~             Dir.mkdir(main_text+"/"+mapfold_name+"/"+evfold_name) 
#~           end  
#~       # ---------------------------------------------------------#
#~             full_path = main_text+"/"+mapfold_name+"/"+evfold_name+"/"
#~             filename = full_path+page_count.to_s+".txt"
#~       # ---------------------------------------------------------#      
#~             File.open(filename.to_s, "a") { |file| 
#~               evc.parameters.each { |tx| file.write(tx+"\n")} 
#~             }  
#~       # ---------------------------------------------------------#   
#~               #file = nil
#~       # ---------------------------------------------------------#        
#~         end 
#~         list_count += 1
#~       } # Event Commands
#~       return list_count
#~     end
#~     
#~   end
#~ end

#~ # ---------------------------------------------------------#
#~ # Will throw an error if directoy isn't empty
#~ #Dir.delete(IEX::TEXT_TO_TXT::OUTPUT_FOLDER) 
#~ if FileTest.exist?(IEX::TEXT_TO_TXT::OUTPUT_FOLDER)
#~   print "Delete old '#{IEX::TEXT_TO_TXT::OUTPUT_FOLDER}' directory!" 
#~   exit
#~ end  
#~ Dir.glob("Data/Map*.rvdata").each { |filename|
#~   next unless filename.scan(/MAP(\d{3})/i).size > 0
#~   # ---------------------------------------------------------#
#~   map_id = 0
#~   filename.gsub(/MAP(\d{3})/i) { map_id = $1.to_i }
#~   next if map_id == 0
#~   # ---------------------------------------------------------#
#~   IEX::TEXT_TO_TXT.map_events_text(load_data(filename), map_id)
#~ }  
#~ IEX::TEXT_TO_TXT.common_events_to_text(load_data("Data/CommonEvents.rvdata"))
#~ # ---------------------------------------------------------#
