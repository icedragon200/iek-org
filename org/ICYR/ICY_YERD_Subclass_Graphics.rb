# ICY_YERD_Subclass_Graphics
#~ module ICY
#~   module YERD_SUBCLASS

#~     #---------------------------------------------------------------------------
#~     # SUBCLASS CHANGE GRAPHIC
#~     #---------------------------------------------------------------------------
#~     
#~     # This array lists which actors will change their graphic upon changing
#~     # classes. If an actor's ID isn't listed here, that
#~     # actor's graphic is locked from being able to be changed.
#~     SUB_CHANGE_ACTOR_GRAPHICS = [1]
#~     
#~     # This hash determines which graphics each individual actor will switch to
#~     # when changing classes. Note that if you don't list an actor ID, the hash
#~     # will automatically pool the actor together with Actor ID 0. For that
#~     # reason, do NOT remove actor 0's hash information. If a class ID doesn't
#~     # appear, the actor will retain its current graphic settings.
#~     ACTOR_DEFAULT_GRAPHICS = {
#~       1 => ["$Baron", 0, "Baron", 0]
#~     }
#~     SUB_GRAPHIC_HASH ={
#~     # Actor.ID => Do not remove Actor 0. It's a common pool for unlisted actors.
#~              1 => [#ClassID, CharName, Ind, FaceName, Ind],
#~                    [     20, "$Baron_ConjourLance",   0, "Baron",   0],
#~                    [     21, "$Baron_SouheiLance" ,   0, "Baron",   0],
#~                    [     22, "$Baron_ColossLance" ,   0, "Baron",   0],
#~                    [     23, "$Baron_FortLance"   ,   0, "Baron",   0],
#~                   ],#Next Instance
#~     } # Do not remove this.
#~   end
#~ end

#~ class Game_Actor < Game_Battler
#~   
#~   def reset_normal_graphics
#~     if ICY::YERD_SUBCLASS::ACTOR_DEFAULT_GRAPHICS.has_key?(self.id)
#~       garray = ICY::YERD_SUBCLASS::ACTOR_DEFAULT_GRAPHICS[self.id] 
#~       val1 = garray[0]; val2 = garray[1]; val3 = garray[2]; val4 = garray[3]
#~       self.set_graphic(val1, val2, val3, val4)
#~       return unless $game_party.members[0].id == self.id
#~       $game_player.set_graphic(val1, val2)
#~     else
#~       class_actor_graphic
#~       return
#~     end
#~   end
#~   
#~   #--------------------------------------------------------------------------
#~   # sub class actor graphic
#~   #--------------------------------------------------------------------------
#~   def sub_class_actor_graphic
#~     if @subclass_id == 0 or @subclass_id == nil 
#~       reset_normal_graphics
#~       return
#~     end
#~     return unless ICY::YERD_SUBCLASS::SUB_CHANGE_ACTOR_GRAPHICS.include?(self.id)
#~     if ICY::YERD_SUBCLASS::SUB_GRAPHIC_HASH.has_key?(self.id)
#~       narray = ICY::YERD_SUBCLASS::SUB_GRAPHIC_HASH[self.id]
#~     else
#~      reset_normal_graphics
#~     end
#~     parray = [0, self.character_name, self.character_index, 
#~     self.face_name, self.face_index]
#~     for tarray in narray
#~       if @subclass_id == tarray[0]
#~         parray = tarray
#~         break
#~       end
#~     end
#~     val1 = parray[1]; val2 = parray[2]; val3 = parray[3]; val4 = parray[4]
#~     self.set_graphic(val1, val2, val3, val4)
#~     return unless $game_party.members[0].id == self.id
#~     $game_player.set_graphic(val1, val2)
#~   end
#~   
#~   #--------------------------------------------------------------------------
#~   # change Subclass ID
#~   #--------------------------------------------------------------------------
#~   alias icy_change_subclass subclass_id= unless $@
#~   def subclass_id=(*args)
#~     icy_change_subclass(*args)
#~     sub_class_actor_graphic
#~   end
#~   
#~ end
