# ICY_Map_Name_Subs
#------------------------------------------------------------------------------#
# ** ICY Map Name Substitutes
# ** Created by : IceDragon
# ** Script-Status : Addon
# ** Date Created : 8/23/2010
# ** Date Modified : 8/23/2010
# ** Version : 1.0
#------------------------------------------------------------------------------#
# This is a simple script snippet which allows you to substitute a string into 
# a map name
#------------------------------------------------------------------------------#
$imported = {} if $imported == nil
$imported["ICY_MapNameSubs"] = true

module ICY
  module MAP_NAME_SUBS
    #--------------------------------------------------------
    # To use substitution put '<string>' into a map name
    # So if you have sub tag "CH" => "Church"
    #  You would put this into your map name "<CH>"
    # That would produce "Church" as the map name
    # Please note don't put the quotes around the <>
    #-------------------------------------------------------
    SUBS = {
# Substitute tag => Substitute String
   # Common Tags
    "RN" => "Ruins",
    "FL" => "Floor",
    "FRS"=> "Forest",
    "BSM"=> "Black Smith",
    "SMT"=> "Smith",
    "RM" => "Room",
    "CTY"=> "Courtyard",
    "CST"=> "Castle",
    "ENT"=> "Entrance",
    "ISHP" => "Item Shop",
    "ASHP" => "Accessory Shop",
    "CSHP" => "Crests Shop",
    "STRHS" => "Storage House",
    
   # Map Tags 
    "DM" => "Demiscu",
    "LIL"=> "Lilisa",
    "PHM"=> "Phimorous",
    "CAM"=> "Combat And Magic",
    "SHR"=> "Shrine",
    "TEM"=> "Temple",
    "MNDS"=> "Mineral Desert",
    
   # Area Tags
    "AR" => "Area",
    "A1" => "Area 1",
    "A2" => "Area 2",
    "A3" => "Area 3",
    "A4" => "Area 4",
    "A5" => "Area 5",
    "A6" => "Area 6",
    "A7" => "Area 7",
    "A8" => "Area 8",
    "A9" => "Area 9",
    "A10" => "Area 10",
    "A11" => "Area 11",
    "A12" => "Area 12",
    "A?" => "Area ?",
   
   # Directional Tags
    "ES" => "East",
    "WS" => "West",
    "NOR"=> "North",
    "SOU"=> "South",
    
    }
  end
end

#==============================================================================
# ** RPG::MapInfo
#------------------------------------------------------------------------------
#==============================================================================
class RPG::MapInfo
  
  alias icy_map_name_subs_name name unless $@
  def name
    subs = ICY::MAP_NAME_SUBS::SUBS
    strip_name = icy_map_name_subs_name
    for sub in subs.keys
      next if sub == nil
      if strip_name.scan(/\<#{sub}\>/).size > 0
        strip_name = strip_name.gsub(/\<#{sub}\>/) {subs[sub]}
      end
    end
    return strip_name
  end
  
end
################################################################################
#------------------------------------------------------------------------------#
#END\\\END\\\END\\\END\\\END\\\END\\\END///END///END///END///END///END///END///#
#------------------------------------------------------------------------------#
################################################################################
