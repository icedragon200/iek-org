# IEX - State Change Skills
#~ #==============================================================================#
#~ # ** IEX(Icy Engine Xelion) - State Change Skills
#~ #------------------------------------------------------------------------------#
#~ # ** Created by    : IceDragon (http://www.rpgmakervx.net/)
#~ # ** Script-Status : Addon (Skills)
#~ # ** Script Type   : State Change Skills
#~ # ** Date Created  : 11/07/2010
#~ # ** Date Modified : 01/01/2010
#~ # ** Version       : 2.0c
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** INTRODUCTION
#~ #------------------------------------------------------------------------------#
#~ # This script was originally the Overlimit skills, which required a certain
#~ # state in order for the skill to be changed.
#~ # Anyway, it was a bit hard coded so I decided to break it a bit to allow
#~ # multiple states instead.
#~ #
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** FEATURES
#~ #------------------------------------------------------------------------------#
#~ # 2.0
#~ #  Notetags! Placed in Skill noteboxes
#~ #------------------------------------------------------------------------------#
#~ # <skill_id sts: state_id, state_id, state_id>
#~ # This will replace the current skill, with the one marked by skill_id if the 
#~ # user has all the needed states
#~ # 
#~ # EG: 
#~ # Skill
#~ # 400 - Breath (Non Elemental)
#~ # 401 - Freeze Breath (Ice)
#~ #
#~ # State
#~ # 150 - Icecore (Attacks are all Ice Based)
#~ #
#~ # <401 sts: 150>
#~ # If the user has state 150, they will use 'Freeze Breath' instead of 'Breath'
#~ #------------------------------------------------------------------------------#
#~ #
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** COMPATABLITIES
#~ #------------------------------------------------------------------------------#
#~ #
#~ # BEM, DBS, Probably Takentai, not GTBS
#~ # Not compatable with Yggdrasil (Yet)
#~ #
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** CHANGE LOG
#~ #------------------------------------------------------------------------------#
#~ #
#~ # 11/07/2010 - V1.0  Completed Script 
#~ # 12/03/2010 - V2.0  Rewrote Script (Overwritten, Overlimit Skills)
#~ # 12/29/2010 - V2.0a Critical Bug Fix regarding State_Ids
#~ # 12/30/2010 - V2.0b Only single digit id skills would work.
#~ # 01/01/2011 - V2.0c Bug fix for the year!
#~ #
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** KNOWN ISSUES
#~ #------------------------------------------------------------------------------#  
#~ #  Non at the moment
#~ #------------------------------------------------------------------------------#
#~ $imported = {} if $imported == nil
#~ $imported["IEX_State_Change_Skills"] = true

#~ #==============================================================================
#~ # ** IEX::REGEXP::STS
#~ #------------------------------------------------------------------------------
#~ #==============================================================================
#~ module IEX
#~   module REGEXP
#~     module STS
#~       # Skill Note Tags
#~       STA_SKILL = /<(\d+)[ ]*(?:REQ_STATE_SKILL|req state skill|STS):?[ ]*(\d+(?:\s*,\s*\d+)*)>/i           
#~     end
#~   end
#~ end

#~ #==============================================================================
#~ # ** RPG::BaseItem
#~ #------------------------------------------------------------------------------
#~ #==============================================================================
#~ class RPG::BaseItem
#~   
#~   def iex_sts_skill_cache
#~     @sts_cache_complete = false
#~     @sts_skill = false
#~     @sts_skills = {}
#~     self.note.split(/[\r\n]+/).each { |line|
#~     case line
#~     when IEX::REGEXP::STS::STA_SKILL
#~       ski_id = $1.to_i
#~       @sts_skill = true
#~       ski_sta = []
#~       $2.scan(/\d+/).each { |num| 
#~         ski_sta.push(num.to_i) if num.to_i > 0 }
#~       @sts_skills[ski_sta.clone] = ski_id
#~     end
#~     }
#~     @sts_cache_complete = true
#~   end
#~   
#~   def sts_skill?
#~     iex_sts_skill_cache unless @sts_cache_complete 
#~     return @sts_skill 
#~   end
#~   
#~   def get_sts_skills
#~     iex_sts_skill_cache unless @sts_cache_complete 
#~     return @sts_skills
#~   end
#~   
#~ end

#~ #==============================================================================
#~ # ** Game_Battler
#~ #------------------------------------------------------------------------------
#~ #==============================================================================  
#~ class Game_Battler
#~    
#~   def get_sts_skill(skil)
#~     return skil unless skil.is_a?(RPG::Skill)
#~     ret_skill = skil
#~     if skil.sts_skill?
#~       ski_cons = skil.get_sts_skills
#~       for sta_set in ski_cons.keys
#~         next if sta_set == nil
#~         valid = false
#~         sta_set.each { |sta_id|
#~           valid = false
#~           break unless @states.include?(sta_id) 
#~           valid = true }
#~         if valid 
#~           ret_skill = $data_skills[ski_cons[sta_set]]
#~           break
#~         end  
#~       end  
#~     else
#~       ret_skill = skil
#~     end 
#~     return ret_skill
#~   end
#~   
#~ end

#~ #==============================================================================
#~ # ** Scene_Battle
#~ #------------------------------------------------------------------------------
#~ #==============================================================================
#~ class Scene_Battle < Scene_Base
#~   
#~   #--------------------------------------------------------------------------
#~   # * Execute Battle Action: Skill
#~   #--------------------------------------------------------------------------
#~   alias iex_overlimit_scb_execute_action_skill execute_action_skill unless $@
#~   def execute_action_skill(*args)
#~     ol_skill = @active_battler.action.skill
#~     swa_skill = @active_battler.get_sts_skill(ol_skill)
#~     @active_battler.action.skill_id = swa_skill.id
#~     iex_overlimit_scb_execute_action_skill(*args)
#~   end
#~   
#~ end

