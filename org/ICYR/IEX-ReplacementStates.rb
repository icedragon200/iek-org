# IEX - Replacement States
#==============================================================================#
# ** IEX(Icy Engine Xelion) - Replacement States
#------------------------------------------------------------------------------#
# ** Created by    : IceDragon (http://www.rpgmakervx.net/)
# ** Script-Status : Addon (States)
# ** Script Type   : State Replacement
# ** Date Created  : 11/15/2010
# ** Date Modified : 11/15/2010
# ** Version       : 1.0
#------------------------------------------------------------------------------#
#==============================================================================#
# ** INTRODUCTION
#------------------------------------------------------------------------------#
# This script adds a new feature to states, it is a replacement feature. 
# When the state has a turncount, if that turn count runs out, it will add a
# new state.
#
# EG
# If an actor is dead, and you use Necromancy, which adds the Zombie(17) state
# That Zombie state has a turn countdown of 5,
# when it hits zero the Zombie(17) is removed and Icapacitated(1) is added
# You can link as many states as you like.
#
#------------------------------------------------------------------------------#
#==============================================================================#
# ** FEATURES
#------------------------------------------------------------------------------#
# 1.0
#  Notetags! Placed in States noteboxes
#------------------------------------------------------------------------------#
#
# <replacement state: x, x, x> (or) <replace states: x, x, x>
# If the state is to be removed, by turncount, it will add another state(s) 
# marked by x. However if the state is removed before the turncount, nothing
# will happen
#
#------------------------------------------------------------------------------#
#==============================================================================#
# ** INSTALLATION
#------------------------------------------------------------------------------#
# 
# Below 
#  Custom Battle Systems
#
# Above 
#   Main
#
#------------------------------------------------------------------------------#
#==============================================================================#
# ** CHANGE LOG
#------------------------------------------------------------------------------#
#
# 11/15/2010 - V1.0 Completed Script
#
#------------------------------------------------------------------------------#
#==============================================================================#
# ** KNOWN ISSUES
#------------------------------------------------------------------------------#  
#
#  Non at the moment
#
#------------------------------------------------------------------------------#
$imported = {} if $imported == nil
$imported["IEX_Replacement_States"] = true

#==============================================================================
# ** IEX::REGEXP::REP_STATES
#------------------------------------------------------------------------------
#==============================================================================
module IEX
  module REGEXP
    module REP_STATES
      REP_STATE = /<(?:REPLACE_|replace |replacement_|replacement )(?:STATE)s?:[ ]*(\d+(?:\s*,\s*\d+)*)>/i
    end
  end
end

#==============================================================================
# ** RPG::State
#------------------------------------------------------------------------------
#==============================================================================
class RPG::State
  
  def iex_rep_state_cache
    @iex_rep_state_cache_complete = false
    @iex_replacement_state = false
    @iex_replacement_states = []
    self.note.split(/[\r\n]+/).each { |line|
    case line
    when IEX::REGEXP::REP_STATES::REP_STATE
      @iex_replacement_state = true
      $1.scan(/\d+/).each { |sid| 
      @iex_replacement_states.push(sid.to_i) }
    end  
    }
    @iex_rep_state_cache_complete = true
  end
  
  def iex_replacement_state?
    iex_rep_state_cache unless @iex_rep_state_cache_complete
    return @iex_replacement_state 
  end
  
  def iex_replacement_states
    iex_rep_state_cache unless @iex_rep_state_cache_complete
    return @iex_replacement_states
  end
  
end

#==============================================================================
# ** Game_Battler
#------------------------------------------------------------------------------
#==============================================================================
class Game_Battler
  
  #--------------------------------------------------------------------------
  # * Natural Removal of States (called up each turn)
  #--------------------------------------------------------------------------
  alias iex_repl_states_remove_states_auto remove_states_auto unless $@
  def remove_states_auto
    for state in states
      next unless @state_turns[state.id] != nil
      turn_drop = @state_turns[state.id] - 1
      if turn_drop <= 0
        if state.iex_replacement_state?
          for rs in state.iex_replacement_states
            next if rs == nil
            add_state(rs)
            @added_states.push(rs)
          end
          remove_state(state.id)
          @removed_states.push(state.id)
        end  
      end  
    end 
    iex_repl_states_remove_states_auto
  end
  
end
