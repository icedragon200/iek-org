# *IXCS_Icy Xtra_Custom_Stats
$imported = {} if $imported == nil
$imported["ICY_Xtra_Custom_Stats"] = true

module ICY
  module XCS
    CUSTOM_STATS = {
    1 => {
            :max => 10,
            :negative => false,
            :name => "Action Point",
            :abv => "AP",
            :id => 1,
            :icon_index => 3984,
            :description => "",
            :hidden => false,
            },
    2 => {
            :max => 10,
            :negative => false,
            :name => "Custom Point",
            :abv => "CP",
            :id => 2,
            :icon_index => 3986,
            :description => "",
            :hidden => false,
            },
    3 => {
            :max => 10,
            :negative => false,
            :name => "Dolt Point",
            :abv => "DP",
            :id => 3,
            :icon_index => 3987,
            :description => "",
            :hidden => false,
            },
    4 => {
            :max => 10,
            :negative => false,
            :name => "G Point",
            :abv => "GP",
            :id => 4,
            :icon_index => 3990,
            :description => "",
            :hidden => false,
            },
    5 => {
            :max => 10,
            :negative => true,
            :name => "Love Point",
            :abv => "LP",
            :id => 5,
            :icon_index => 3995,
            :description => "",
            :hidden => false,
            },
    6 => {
            :max => 10,
            :negative => false,
            :name => "Option Point",
            :abv => "OP",
            :id => 6,
            :icon_index => 3998,
            :description => "",
            :hidden => false,
            },
    }
    
    # Sets stats specific to actor, if non is set the actor is assumed to have
    # ALL the custom stats
    ACTOR_STATS = { }
  end
end

module ICY
  module REGEXP
    module XCS
      ACTOREFF  = /<(?:XCS_ACTOR_EFFECT|xcs actor effect)[ ]*(\d+)>/i
      NOTSELF   = /<(?:XCS_NOT_SELF|xcs not self)>/i
      RAISESTAT = /<(?:XCS_RAISE_STAT|xcs raise stat)[ ]*(\d+):(\d+)>/i
      LOWERSTAT = /<(?:XCS_LOWER_STAT|xcs lower stat)[ ]*(\d+):(\d+)>/i
    end
  end
end

#===============================================================================
# RPG::Skill
#===============================================================================
class RPG::Skill
end

class RPG::BaseItem
  
  alias icy_ixcs_basit_initialize initialize unless $@
  def initialize(*args)
    icy_ixcs_basit_initialize(*args)
    icy_ixcs_basit_cache
  end
  
  def icy_ixcs_basit_cache
    @cs_not_for_self = false
    @cs_actors_to_effect = []
    @cs_raise_stats = []
    @cs_lower_stats = []
    self.note.split(/[\r\n]+/).each { |line|
     case line
     when ICY::REGEXP::XCS::NOTSELF
       @cs_not_for_self = true
     when ICY::REGEXP::XCS::ACTOREFF
       @cs_actors_to_effect.push($1.to_i)
     when ICY::REGEXP::XCS::RAISESTAT
       @cs_raise_stats.push([$1.to_i, $2.to_i])
     when ICY::REGEXP::XCS::LOWERSTAT
       @cs_lower_stats.push([$1.to_i, $2.to_i])
     end
     } # End each
  end
  
  def cs_not_self?
    icy_ixcs_basit_cache if @cs_not_for_self == nil
    return @cs_not_for_self
  end
  
  def cs_actor_effect
    icy_ixcs_basit_cache if @cs_actors_to_effect == nil
    return @cs_actors_to_effect
  end
  
  def cs_raise_stats
    icy_ixcs_basit_cache if @cs_raise_stats == nil
    return @cs_raise_stats
  end
  
  def cs_lower_stats
    icy_ixcs_basit_cache if @cs_lower_stats == nil
    return @cs_lower_stats
  end
  
end

class IXCS_Stat < RPG::BaseItem
    
  def initialize(cs)
    stat = ICY::XCS::CUSTOM_STATS[cs]
    @current = 0
    @name = stat[:name]
    @maximum = stat[:max]
    @id = stat[:id]
    @description = stat[:description]
    @icon_index = stat[:icon_index]
    @negative_stat = stat[:negative]
    @abrev = stat[:abv]
    if stat[:hidden] != nil
      @hide_points = stat[:hidden]
      else
      @hide_points = false
    end
   #@battle_skill = stat[:battle]
 end
 
  def hide_points(bool = false)
    @hide_points = bool
  end
  
  def hidden?
    return @hide_points
  end
  
  def raise_points(amt = 0)
    if @negative_stat == true
      lest = -max_points
    else
      lest = 0
    end
    @current = [[@current + amt, lest].max, max_points].min
  end
  
  def lose_points(amt = 0)
    raise_points(-amt)
  end
  
  def points
    return @current
  end
  
  def max_points
    return @maximum
  end
  
  def abv
    return @abrev
  end
  
end

class Game_Actor < Game_Battler 
  
  attr_accessor :ixcs_stats
  
  alias icy_ics_ga_initialize initialize unless $@
  def initialize(*args)
    icy_ics_ga_initialize(*args)
    @ixcs_stats = {}
    if ICY::XCS::ACTOR_STATS.has_key?(self.id)
       for cs in ICY::XCS::ACTOR_STATS[self.id]
         next if cs == nil
         @ixcs_stats[cs] = IXCS_Stat.new(cs)
       end
      else
       for cs in ICY::XCS::CUSTOM_STATS.keys
         next if cs == nil
         @ixcs_stats[cs] = IXCS_Stat.new(cs)
       end
    end
  end
  
  #--------------------------------------------------------------------------
  # alias item effective?
  #--------------------------------------------------------------------------
  alias ixcs_item_effective? item_effective? unless $@
  def item_effective?(user, item)
    can_effect = []
    if item.cs_actor_effect.include?(self.id)
      can_effect.push(true)
    elsif item.cs_actor_effect == []
      can_effect.push(true)
    else
      can_effect.push(false)
    end
    if item.cs_not_self? 
      if user.id != self.id
        can_effect.push(true)
      else
        can_effect.push(false)
      end
    else
      can_effect.push(true)
    end
    if can_effect.all?
      return true
    end
    return ixcs_item_effective?(user, item)
  end
  
  #--------------------------------------------------------------------------
  # alias item effect
  #--------------------------------------------------------------------------
  alias ixcs_item_effect item_effect unless $@
  def item_effect(user, item)
    ixcs_item_effect(user, item)
    
    if item.cs_actor_effect.include?(self.id)
      ixcs_can_effect = true
    elsif item.cs_actor_effect == []
      ixcs_can_effect = true
    else
      ixcs_can_effect = false
    end
    
    if ixcs_can_effect
      for sta in item.cs_raise_stats
        next if sta == nil
        raise_ixcs_stat(sta[0], sta[1])
      end
      for sta in item.cs_lower_stats
        next if sta == nil
        lower_ixcs_stat(sta[0], sta[1])
      end
    end
    
  end
   
  #--------------------------------------------------------------------------
  # alias method: skill_effect
  #--------------------------------------------------------------------------
  alias ixcs_skill_effect skill_effect unless $@  
  def skill_effect(user, skill)
    ixcs_skill_effect(user, skill)
    if skill.cs_actor_effect.include?(self.id)
      ixcs_can_effect = true
    elsif skill.cs_actor_effect == []
      ixcs_can_effect = true
    else
      ixcs_can_effect = false
    end
    
    if ixcs_can_effect
      for sta in skill.cs_raise_stats
        next if sta == nil
        raise_ixcs_stat(sta[0], sta[1])
      end
      for sta in skill.cs_lower_stats
        next if sta == nil
        lower_ixcs_stat(sta[0], sta[1])
      end
    end
  end
  
  def raise_ixcs_stat(stat, amt)
    if @ixcs_stats.has_key?(stat)
      @ixcs_stats[stat].raise_points(amt)
    end
  end
  
  def lower_ixcs_stat(stat, amt)
    if @ixcs_stats.has_key?(stat)
      @ixcs_stats[stat].lose_points(amt)
    end
  end
  
  def get_ixcs_stat(stat)
    if @ixcs_stats.has_key?(stat)
     return @ixcs_stats[stat]
    else
     return nil
    end
  end
  
end

#==============================================================================
# ** Window_Base
#------------------------------------------------------------------------------
# ** Adding a new draw function
#==============================================================================
class Window_Base < Window

  def draw_actor_ixcs_stat(stat, x, y, bar = false)
    return if stat == nil
      color1 = ICY::Colors::LightBlue #Color.new(255, 255, 255, 128)
      color2 = ICY::Colors::Blue #Color.new(128, 128, 128, 128)
      color1.alpha = 128
      color2.alpha = 128
      vals = stat.points
      maxs = stat.max_points
      text = stat.name
      draw_icon_format_text(x, y, 128, WLH, text, stat.icon_index, true, 0, 18)
      tsize = self.contents.text_size(text)
      off_ox = 96#tsize.width
      draw_text_fraction_style(x + off_ox + 64, y, vals, maxs)
      if bar
        draw_grad_bar(x + 32, y + 20, 128, 12, vals, maxs, color1, color2)
      end
    end
  
  end
  
class ICY_Window_IXCS_Stat < ICY_HM_Window_Selectable 
  
  def initialize(actor, arx, y = 0, width = 544, height = 416)
    if arx.is_a?(Array)
      x = arx[0]
      y = arx[1]
      width = arx[2]
      height = arx[3]
    else 
      x = arx
    end
    @selection_rect = Rect.new(0, 0, 128, 58)
    super(x, y, width, height)
    @actor = actor
    @column_max = 1
    self.index = 0
    @item_sq_spacing = 42
    @rect_size = 32
    @selection_size = 32
    @selection_rect = Rect.new(0, 0, 196, @selection_size)
    refresh
  end
  
  def off_x ; return 0 end
    
  def refresh
    super
    stats = $game_actors[@actor.id].ixcs_stats
    @item_max = stats.size
    self.contents.clear    
    for lp in stats.values
      next if lp == nil
      next if lp.hidden?
      draw_stat(lp) 
    end
  end
  
  def draw_stat(lp)
    draw_actor_ixcs_stat(lp, @nw_x, @nw_y, true)
    advance_space
  end
  
  #--------------------------------------------------------------------------
  # * Update cursor
  #--------------------------------------------------------------------------
  def update_cursor
    if @index < 0                   # If the cursor position is less than 0
      self.cursor_rect.empty        # Empty cursor
    else                            # If the cursor position is 0 or more
      row = @index / @column_max    # Get current row
      if row < top_row              # If before the currently displayed
        self.top_row = row          # Scroll up
      end
      if row > bottom_row           # If after the currently displayed
        self.bottom_row = row       # Scroll down
      end
       y_l = @index / @column_max
       y_pos = ((@item_sq_spacing * y_l)- self.oy) + off_y
       x_l = self.index - (@column_max * y_l)
       x_pos = (x_l * @item_sq_spacing) + off_x
       if @selection_size > @rect_size
         subitive = (@selection_size - @rect_size) / 2
         self.cursor_rect.set(x_pos.to_i - subitive, y_pos.to_i - subitive, @selection_rect.width, @selection_rect.height)
       elsif @selection_size < @rect_size
         additive = (@rect_size - @selection_size) / 2
         self.cursor_rect.set(x_pos.to_i + additive, y_pos.to_i + additive, @selection_rect.width, @selection_rect.height)
       else
        self.cursor_rect.set(x_pos.to_i, y_pos.to_i, @selection_rect.width, @selection_rect.height)
       end
    end
  end
  
end
