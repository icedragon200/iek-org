# IEX - Equipment Skills
#~ #==============================================================================#
#~ # ** IEX(Icy Engine Xelion) - Equipment Skills
#~ #------------------------------------------------------------------------------#
#~ # ** Created by    : IceDragon (http://www.rpgmakervx.net/)
#~ # ** Script-Status : Addon (Equipment)
#~ # ** Script Type   : Skills from Equipment
#~ # ** Date Created  : 10/22/2010
#~ # ** Date Modified : 01/31/2011
#~ # ** Version       : 1.1
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** INTRODUCTION
#~ #------------------------------------------------------------------------------#
#~ # The script is a bit primitive, once a an item is equipped the wielder/equipee
#~ # will have access to whatever skill is stated.
#~ # If the item is removed, the skills go with it.
#~ #
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** FEATURES
#~ #------------------------------------------------------------------------------#
#~ # V1.0
#~ #  Notetags! Can be placed in Equipment noteboxes
#~ #------------------------------------------------------------------------------#
#~ #  <EQUIP_SKILL: id, id, id> (or) <equip skill: id, id, id>
#~ #  <LEVEL_SKILL level: id, id, id> (or) <level skill level: id, id, id>
#~ #
#~ # EG . <equip skill: 2, 3, 4>
#~ #   Adds skills 2, 3, 4
#~ # EG2. <levek skill 15: 12, 50, 51>
#~ #   Requires that the weilder be at least level 15.
#~ #
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** HOW TO USE
#~ #------------------------------------------------------------------------------#
#~ #  In an equipments notebox (Weapon or Armor)
#~ #  put <EQUIP_SKILL: id, id, id> or <equip skill: id, id, id>
#~ #  The actor will gain the skills marked by Id while that piece of equipment
#~ #  is equipped.
#~ #
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** CHANGE LOG
#~ #------------------------------------------------------------------------------#
#~ # 
#~ #  10/22/2010 - V1.0  Finished Script
#~ #  01/24/2011 - V1.0a Fixed disabled skills problem when using the DBS
#~ #  01/31/2011 - V1.1  Added Level Skills
#~ #
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # ** KNOWN ISSUES
#~ #------------------------------------------------------------------------------#
#~ #  Non at the moment. 
#~ #
#~ #------------------------------------------------------------------------------#
#~ $imported = {} if $imported == nil
#~ $imported["IEX_Equipment_Skills"] = true
#~ #==============================================================================
#~ # ** IEX::REGEXP::EQUIPMENT_SKILLS
#~ #------------------------------------------------------------------------------
#~ #==============================================================================
#~ module IEX
#~   module REGEXP
#~     module EQUIPMENT_SKILLS
#~       EQUIPMENT_SKILL = /<(?:EQUIP_SKILL|equip skill|equipskill)s?:[ ]*(\d+(?:\s*,\s*\d+)*)>/i
#~       LEVEL_SKILL     = /<(?:LEVEL_SKILL|level skill|levelskill)s?[ ](\d+):[ ]*(\d+(?:\s*,\s*\d+)*)>/i
#~     end
#~   end
#~ end
#~  
#~ #==============================================================================
#~ # ** RPG::BaseItem
#~ #------------------------------------------------------------------------------
#~ #==============================================================================
#~ class RPG::BaseItem
#~     
#~   def iex_skill_equipment_cache
#~     @iex_skill_equipment_cache_complete = false
#~     @iex_equip_skills = []
#~     @iex_level_skills = {}
#~     self.note.split(/[\r\n]+/).each { |line| 
#~     case line
#~     when IEX::REGEXP::EQUIPMENT_SKILLS::EQUIPMENT_SKILL
#~       $1.scan(/\d+/).each { |skill_id|
#~       @iex_equip_skills.push(skill_id.to_i) }
#~     when IEX::REGEXP::EQUIPMENT_SKILLS::LEVEL_SKILL  
#~       level = $1.to_i
#~       @iex_level_skills[level] = [] if @iex_level_skills[level].nil?
#~       $2.scan(/\d+/).each { |skill_id|
#~         @iex_level_skills[level].push(skill_id.to_i) }
#~     end
#~     }
#~     @iex_skill_equipment_cache_complete = true
#~   end
#~   
#~   def iex_equip_skills
#~     iex_skill_equipment_cache unless @iex_skill_equipment_cache_complete
#~     return @iex_equip_skills
#~   end
#~   
#~   def iex_level_skills
#~     iex_skill_equipment_cache unless @iex_skill_equipment_cache_complete
#~     return @iex_level_skills
#~   end
#~   
#~ end

#~ #==============================================================================
#~ # ** Game_Actor
#~ #------------------------------------------------------------------------------
#~ #==============================================================================
#~ class Game_Actor < Game_Battler
#~   
#~   unless $imported["BattleEngineMelody"]
#~     #--------------------------------------------------------------------------
#~     # * Determine if Finished Learning Skill
#~     #     skill : skill
#~     #--------------------------------------------------------------------------
#~     alias iex_esk_skill_learn? skill_learn? unless $@
#~     def skill_learn?(skill)
#~       return true if equipment_skills.include?(skill)
#~       return iex_esk_skill_learn?(skill)
#~     end
#~   end  
#~   
#~   def equipment_skills
#~     result = []
#~     for eq in equips
#~       next if eq == nil
#~       for ski_id in eq.iex_equip_skills
#~         next if ski_id.nil?
#~         result |= [$data_skills[ski_id]]
#~       end
#~       for key in eq.iex_level_skills.keys
#~         next if @level < key
#~         ski_ids = eq.iex_level_skills[key]
#~         ski_ids.each { |ski_id| result |= [$data_skills[ski_id]] }
#~       end  
#~     end
#~     return result
#~   end
#~   
#~   alias iex_equipment_skilss_ga_skills skills unless $@
#~   def skills
#~     result = iex_equipment_skilss_ga_skills
#~     result |= equipment_skills
#~     return result
#~   end
#~   
#~ end
