# IXTP_Icy_Xtra_Text_Pop
#------------------------------------------------------------------------------#
# ** IXTP - Icy Xtra Text Pop
# ** Created by : IceDragon
# ** Script-Status : Addon
# ** Date Created : 8/29/2010
# ** Date Modified : 9/27/2010
# ** Version : 1.2
#------------------------------------------------------------------------------#
# Just a simple text popping script. 
# How does this work?
# You will do all these in a Move Route Call (Sorry its just easier that way)
# You first script call prep_text_stack (This clears the old text pop cache for the event)
# You then stack text with the script call stack_text_pop("text", color)
# Color is optional (The deafult color will be used if not defined)
# After you have stacked text (Yes you can call stack_text_pop as much times as you like)
# Just call pop_texts and watch the magic.
#
# Credit goes to Xideworg (Did I spell it right?) for his Pop Engine (XRXS Scripts)
#
# To use text substitution.
# <var[id]> for variables
# <name[id]> for actor name
#
# Sorry that the format is so weird, the regular \v and \n don't work!
#------------------------------------------------------------------------------#
# KNOWN ISSUES
# Non at the moment.
#
#------------------------------------------------------------------------------#
# CHANGE LOG
# 9/01/2010 1.0 Finished Script
# 9/27/2010 1.1 Bug Fix + Variable and Name Addons
# 9/27/2010 1.2 Changed the text change format
#------------------------------------------------------------------------------#
$imported = {} if $imported == nil
$imported["ICY_Xtra_Text_Pop"] = true

class XRXS_POP_ENGINE # Taken form XRXS
    attr_accessor :finished
    attr_accessor :sprite
    attr_accessor :stop_update
    
    GRAVITY = 0.58
    TRANCEPARENT_START = 40
    TRANCEPARENT_X_SLIDE = 0    
    
    def initialize(sprite)
      @finished = false
      @working_sprite = sprite
      @stop_update = false
      prep_pop
    end
    
    def work_sprite
      return @working_sprite
    end
    
    def damage_x_init_velocity
      return 0.4 * (rand(8))  # 0.2 * (rand(5) - 2) 
    end
    
    def damage_y_init_velocity
      return 1.5 * (rand(4) + 4)  # 0.2 * (rand(5) - 2) 
    end
    
    def prep_pop
      @now_x_speed = damage_x_init_velocity
      @now_y_speed = damage_y_init_velocity
      @potential_x_energy = 0.0
      @potential_y_energy = 0.0
      @speed_off_x = rand(10)
      @pop_duration = 80
    end
    
    def pop_update
      sprite = work_sprite
      return if sprite == nil
      return if @finished
      return if @stop_update
      if @pop_duration <= TRANCEPARENT_START
        sprite.opacity -= (256 / TRANCEPARENT_START)
        sprite.x += TRANCEPARENT_X_SLIDE if @speed_off_x < 6
        sprite.x -= TRANCEPARENT_X_SLIDE if @speed_off_x >= 6
      end
      return if sprite == nil
      n = sprite.oy + @now_y_speed
      if n <= 0
        @now_y_speed *= -1
        @now_y_speed /=  2
        @now_x_speed /=  2
      end      
      sprite.oy = [n, 0].max
      @potential_y_energy += GRAVITY
      speed = @potential_y_energy.floor
      @now_y_speed        -= speed
      @potential_y_energy -= speed
      @potential_x_energy += @now_x_speed
      speed = @potential_x_energy.floor
      sprite.ox  += speed if @speed_off_x < 6
      sprite.ox  -= speed if @speed_off_x >= 6
      @potential_x_energy -= speed 
      @pop_duration -= 1
      if @pop_duration == 0
        @finished = true
      end
    end
    
  end
  
class Game_Character
  attr_accessor :texts_to_pop
  attr_accessor :texts_ready
  
  alias ixtp_gc_text_pop_initialize initialize unless $@
  def initialize(*args)
    ixtp_gc_text_pop_initialize(*args)
    @texts_to_pop = []
    @texts_ready = false
    @ptext = ''
  end
  
  def prep_text_stack
    if @texts_to_pop != nil
       @texts_to_pop.clear
    end   
    @texts_to_pop = []
  end
  
  def stack_text_pop(text = nil, color = Color.new(255, 255, 255), size = 21)
    return if text == nil
    text = text.to_s
    text = text.gsub(/<(?:var|v)\[(\d+)\]>/i) { $game_variables[$1.to_i] } 
    text = text.gsub(/<(?:name|n)\[(\d+)\]>/i) { $game_actors[$1.to_i].name }
    @texts_to_pop.push([text.to_s, color, size.to_i])
  end
  
  def pop_texts
    @texts_ready = true
  end
  
end

class Sprite_Character < Sprite_Base
  
  alias ixtp_scm_text_pop_initialize initialize unless $@
  def initialize(*args)
    @ixtp_text_pop = []
    ixtp_scm_text_pop_initialize(*args)
  end
  
  alias ixtp_update_text_pop_update update unless $@
  def update
    ixtp_update_text_pop_update
    if @character.texts_ready
      stack = @character.texts_to_pop
      ixtp_create_text_pop(stack)
    end
    ixtp_update_text_pop
  end
  
  def ixtp_create_text_pop(stack)
    ixtp_dispose_all_text_pop
    coun = 0
    @ixtp_text_pop = []
    for sst in stack
      next if sst == nil
      text = sst[0]
      color = sst[1]
      textsize = text.size * 24
      textsize += 32 if textsize < 32
      text_height = sst[2] + 2
      @ixtp_text_pop[coun] = []
      @ixtp_text_pop[coun][0] = Sprite.new
      @ixtp_text_pop[coun][0].bitmap = Bitmap.new(textsize, text_height)
      @ixtp_text_pop[coun][0].bitmap.font.size = sst[2]
      @ixtp_text_pop[coun][0].bitmap.font.color = color
      @ixtp_text_pop[coun][0].bitmap.draw_text(0, 0, textsize, text_height, text, 0)
      @ixtp_text_pop[coun][0].x = self.x + (self.ox - (@ixtp_text_pop[coun][0].bitmap.width) / 2) 
      @ixtp_text_pop[coun][0].y = (self.y - self.height) + 16
      @ixtp_text_pop[coun][0].z = 280
      @ixtp_text_pop[coun][1] = XRXS_POP_ENGINE.new(@ixtp_text_pop[coun][0])
      coun += 1
    end
    @character.texts_ready = false
  end
  
  def ixtp_update_text_pop
    return if @ixtp_text_pop.empty?
    stack_fin = []
    for spri in @ixtp_text_pop
      if spri[1].finished
        ixtp_dispose_text_pop(spri)
      else
        spri[1].pop_update
      end
      stack_fin.push(spri[1].finished)
    end
    ixtp_dispose_all_text_pop if stack_fin.all?
  end
  
  def ixtp_dispose_text_pop(spri)
    return if spri[0] == nil
    spri[1].stop_update = true
    spri[0].dispose
  end
    
  def ixtp_dispose_all_text_pop
    for spri in @ixtp_text_pop
      ixtp_dispose_text_pop(spri)
    end
      @ixtp_text_pop.clear
      @ixtp_text_pop = []
  end
  
  alias ixtp_scm_text_pop_dispose dispose unless $@
  def dispose
    ixtp_scm_text_pop_dispose
    ixtp_dispose_all_text_pop
  end
  
end
