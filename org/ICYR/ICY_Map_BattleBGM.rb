# ICY_Map_BattleBGM
#------------------------------------------------------------------------------#
# ** ICY Map Battle BGM
# ** Created by : IceDragon
# ** Script-Status : Addon
# ** Date Created : 8/16/2010
# ** Date Modified: 8/27/2010
# ** Version : 1.2
#------------------------------------------------------------------------------#
# This script sets the battle BGM depending on the map.
$imported = {} if $imported == nil
$imported["ICY_Map_Battle_Bgm"] = true

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
#-#-#-#-#START CUSTOMIZATION#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
module ICY
  module MAP_BATTLE_BGM
    #This is the switch to turn off the Map Battle BGM
    OFF_SWITCH = 498
    BGMS = ["BATTLE_Alignment"] # For convience purposes
    MAP_BGM = {
  # Map_ID => ["BGM_Name", Volume, Pitch]
    70 => [BGMS[0], 90, 100]
    }
    
    USE_MAPS_BGM = true #If no bgm is set for map use the maps BGM instead
  end
end
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
#-#-#-#-#END CUSTOMIZATION#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
class Game_Map
  attr_reader :map
end

#-----------------------------------------------------------------------------
# ** Class Scene_Map
#-----------------------------------------------------------------------------
class Scene_Map < Scene_Base
  
  alias icy_map_battle_bgm_call_battle call_battle unless $@
  def call_battle
    old_battle_bgm = $game_system.battle_bgm
   unless $game_switches[ICY::MAP_BATTLE_BGM::OFF_SWITCH]
    if ICY::MAP_BATTLE_BGM::MAP_BGM.has_key?($game_map.map_id)
      bgmz = ICY::MAP_BATTLE_BGM::MAP_BGM[$game_map.map_id]
      bat_map_bgm = RPG::BGM.new(bgmz[0], bgmz[1], bgmz[2])
      $game_system.battle_bgm = bat_map_bgm
    elsif ICY::MAP_BATTLE_BGM::USE_MAPS_BGM
      $game_system.battle_bgm = $game_map.map.bgm
    end
  end
    icy_map_battle_bgm_call_battle
    $game_system.battle_bgm = old_battle_bgm
  end
  
end
