# IEX - Map Merge
#==============================================================================
# ** IEX(Icy Engine Xelion) - Map Merge
#------------------------------------------------------------------------------
# ** Created by    : IceDragon (http://www.rpgmakervx.net/)
# ** Script-Status : Addon (Maps)
# ** Script Type   : Multi-Map Layering
# ** Date Created  : ??/??/2010 (DD/MM/YYYY)
# ** Date Modified : 01/08/2011 (DD/MM/YYYY)
# ** Script Tag    : IEX - Map Merge
# ** Difficulty    : Hard
# ** Version       : 1.0a
#------------------------------------------------------------------------------#
#==============================================================================#
# ** INTRODUCTION
#------------------------------------------------------------------------------#
# This script allows you to merge multiple maps together (No, not pokemon style)
# You can then replicate the Harvest Moon, houses upgrade and such.
# It doesn't stop there.
#
#------------------------------------------------------------------------------#
#==============================================================================#
# ** HOW TO USE?
#------------------------------------------------------------------------------#
# V1.0 - Tags - Maps - Place in map's name
#------------------------------------------------------------------------------#
# You create multiple maps, and then you create the one you want to use for 
# the merging, place <map merge: map_id, map_id, etc...> map_id being the maps
# you want to merge with this one. Note, if there are tile overlaps, the last map
# is above the ones before it.
# 
# To have a switch merge operated map, put <switch: switch_id>, when that Game 
# Switch is set to true. That map will be merged with the base map.
# NOTE : Even though this type of map is switch operated, remember to add it to 
# the <map merge>
# 
# You can place maps on specific X, Y pos using <map coords: x, y>.
# NOTE : The X, Y will start on the base map, and will start in the 0,0 position 
# of merging map (Or the map to be merged)
#
# If your using a switch map, do a script call with imm_merge_reset
# This will rebuild the maps cache and restart the Scene_Map
# That way you can see the changes.
#
#------------------------------------------------------------------------------#
#==============================================================================#
# ** COMPATABLITIES
#------------------------------------------------------------------------------#
#
# Works with SwapXT (Just place this script below it)
#
#------------------------------------------------------------------------------#
#==============================================================================#
# ** INSTALLATION
#------------------------------------------------------------------------------#
# 
# Below 
#  Materials
#  Anything that makes changes to the Game_Map
#
# Above 
#   Main
#
#------------------------------------------------------------------------------#
#==============================================================================#
# ** CHANGES 
#------------------------------------------------------------------------------# 
# Classes
# new-class :IMM_Map_Store 
# new-class :IMM_Map_Storage
#   RPG::MapInfo
#     alias      :name
#     new-method :get_merge_maps
#     new-method :switch_map
#     new-method :imm_map_coords
#     new-method :imm_No_TileA?
#   Game_Map
#     alias      :initialize
#     alias      :setup
#     new-method :imm_map_merge_create
#   Game_Interpreter
#     new-method :imm_merge_reset
#
#------------------------------------------------------------------------------#
#==============================================================================#
# ** CHANGE LOG
#------------------------------------------------------------------------------#
# (DD/MM/YYYY)
#  IMM
#  10/04/2010 - V1.0  Finished Script
#  10/06/2010 - V1.2  Updated with bug fixes
#  
#  IEX
#  01/01/2011 - V1.0  Ported to IEX + Finished Docing
#  01/08/2011 - V1.0a Small Changes
#
#------------------------------------------------------------------------------#
#==============================================================================#
# ** KNOWN ISSUES
#------------------------------------------------------------------------------#
#  Non at the moment. 
#
#------------------------------------------------------------------------------#
$imported = {} if $imported == nil
$imported["IEX_MapMerge"] = true

#==============================================================================
# ** IEX::MAP_MERGER
#------------------------------------------------------------------------------
#==============================================================================
module IEX
  module MAP_MERGER
    TILE_VOID = [0, 1544]
  end
end

#==============================================================================
# ** IEX::REGEXP::MAP_MERGER
#------------------------------------------------------------------------------
#==============================================================================
module IEX
  module REGEXP
    module MAP_MERGER
      MAP_MERGE_TAG = /<(?:IMM|MERGE|MAP_MERGE|map merge):[ ]*(\d+(?:\s*,\s*\d+)*)>/i
      SWITCH_MAP = /<(?:SWITCH|switch_map|switch map):[ ]*(\d+)>/i
      MAP_COORDS = /<(?:COORDS|MAP_COORDS|map coords|xy_pos|xy pos):[ ]*(\d+),[ ]*(\d+)>/i 
   OV_TILES_ONLY = /<(?:NO_TILEA|no tilea)>/i
    end
  end
end

#==============================================================================
# ** RPG::MapInfo
#------------------------------------------------------------------------------
#==============================================================================
class RPG::MapInfo
  
  alias imm_map_merge_name name unless $@
  def name
    imm_strip_name = imm_map_merge_name
    imm_strip_name.gsub!(IEX::REGEXP::MAP_MERGER::MAP_MERGE_TAG) { "" }
    imm_strip_name.gsub!(IEX::REGEXP::MAP_MERGER::SWITCH_MAP) { "" }
    imm_strip_name.gsub!(IEX::REGEXP::MAP_MERGER::MAP_COORDS) { "" }
    return imm_strip_name
  end
  
  def get_merge_maps
    name_sr = @name.clone
    imms = []
    case name_sr
    when IEX::REGEXP::MAP_MERGER::MAP_MERGE_TAG
      $1.scan(/\d+/).each { |num| 
      imms.push(num.to_i) if num.to_i > 0 }
    end
    return imms
  end
  
  def switch_map
    name_sr = @name.clone
    imms = []
    case name_sr
    when IEX::REGEXP::MAP_MERGER::SWITCH_MAP
      return $1.to_i
    end
    return nil
  end
  
  def imm_map_coords
    name_sr = @name.clone
    imms = []
    case name_sr
    when IEX::REGEXP::MAP_MERGER::MAP_COORDS
      return [$1.to_i, $2.to_i]
    end
    return nil
  end
  
  def imm_No_TileA?
    name_sr = @name.clone
    imms = []
    case name_sr
    when IEX::REGEXP::MAP_MERGER::OV_TILES_ONLY
      return true
    end
    return false
  end
  
end

#==============================================================================
# ** IMM_Map_Store
#------------------------------------------------------------------------------
#==============================================================================
class IMM_Map_Store 
  
  def initialize(map, map_id, x = 0, y = 0)
    @map = map
    @x = x
    @y = y
    @map_id = map_id
  end
  
  def x_pos ; return @x end
  def y_pos ; return @y end
  def data ;  return @map.data end
  def map ;   return @map end
  def map_id ;return @map_id end
  
  
end

#==============================================================================
# ** IMM_Map_Storage
#------------------------------------------------------------------------------
#==============================================================================
class IMM_Map_Storage
  
  def initialize
    @data_store = {}
  end
  
  def has_map?(map_id = nil)
    return @data_store.has_key?(map_id)
  end
  
  def map_data(map_id = nil)
    return @data_store[map_id]
  end
  
  def add_map(map_id = nil, map_data = nil)
    if map_id != nil and map_data != nil
      @data_store[map_id] = map_data
    end
  end
  
  def remove_map(map_id = nil)
    if map_id != nil
      @data_store.delete(map_id)
    end
  end
  
  def clear_maps
    @data_store.clear
    @data_store = {}
  end
    
end

#==============================================================================
# ** Game Map
#------------------------------------------------------------------------------
#==============================================================================
class Game_Map
  
  alias imm_map_merge_initialize initialize unless $@
  def initialize(*args)
    imm_map_merge_initialize(*args)
    @imm_map_store = IMM_Map_Storage.new
    @map_infos = load_data("Data/MapInfos.rvdata")
  end
  
  alias imm_map_merge_setup setup unless $@
  def setup(*args)
    imm_map_merge_setup(*args)
    if @map_infos == nil
      @map_infos = load_data("Data/MapInfos.rvdata")
    end  
    imm_map_merge_create
  end
  
  def imm_map_merge_create
    #@map = load_data(sprintf("Data/Map%03d.rvdata", @map_id))
    imms = @map_infos[@map_id].get_merge_maps
    return if imms == nil
    return if imms.empty?
    @imms_maps = []
    for ma in imms
      if @map_infos[ma].switch_map != nil
        next unless $game_switches[@map_infos[ma].switch_map]
      end 
      if @map_infos[ma].imm_map_coords != nil
        coords = @map_infos[ma].imm_map_coords
      else ; coords = [0, 0]  
      end  
      imm_map = IMM_Map_Store.new(load_data(sprintf("Data/Map%03d.rvdata", ma)), ma, coords[0], coords[1])
      @imms_maps.push(imm_map)
    end  
    
    for y in 0..height
      for x in 0...width
        
        for m_map in @imms_maps
          next if m_map == nil
          next unless x >= m_map.x_pos
          next unless y >= m_map.y_pos
          imm_map_y = m_map.y_pos
          imm_map_x = m_map.x_pos
          next if m_map.data[x - imm_map_x, y - imm_map_y, 0] == nil and m_map.data[x - imm_map_x, y - imm_map_y, 1] == nil and m_map.data[x - imm_map_x, y - imm_map_y, 2] == nil
          for i in 0..2
            next if (@map_infos[imm_map.map_id].imm_No_TileA? and i == 0)
            unless IEX::MAP_MERGER::TILE_VOID.include?(m_map.data[x - imm_map_x, y - imm_map_y, i] )
              @map.data[x, y, i] = m_map.data[x - imm_map_x, y - imm_map_y, i] 
            end
          end
        end
        
      end
    end
  end
  
end

#==============================================================================
# ** Game Interpreter
#------------------------------------------------------------------------------
#==============================================================================
class Game_Interpreter
  
  def imm_merge_reset
    $game_map.imm_map_merge_create
    $scene = Scene_Map.new
  end
  
end
