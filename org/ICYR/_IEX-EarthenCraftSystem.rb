# *IEX - Earthen Craft System
#~ #==============================================================================#
#~ # ** IEX(Icy Engine Xelion) - Craft System
#~ #------------------------------------------------------------------------------#
#~ # ** Created by    : IceDragon (http://www.rpgmakervx.net/)
#~ # ** Script-Status : Addon (Actors)
#~ # ** Script Type   : State Modifier
#~ # ** Date Created  : 01/17/2011 (DD/MM/YYYY)
#~ # ** Date Modified : 01/17/2011 (DD/MM/YYYY)
#~ # ** Script Tag    : IEX - Earthen Craft System
#~ # ** Difficulty    : Hard, Lunatic
#~ # ** Version       : 1.0
#~ #------------------------------------------------------------------------------#
#~ $imported = {} if $imported == nil
#~ $imported["IEX_CraftSystem"] = true

#~ module IEX
#~   module CRAFT_SYSTEM
#~     STATS_ICONS = IEX::ICONS::STATS_ICONS
#~     CRAFT_ICONS = {
#~       :rccount=> 6363,
#~       :clevel => 6330,
#~       :cexp   => 6395,
#~       :cpoint => 6329,
#~       :cexpnxt=> 6360,
#~     }
#~     MAX_CRAFT_LEVEL = 50
#~     CRAFT_STATS = ['maxhp', 'maxmp', 'atk', 'def', 'spi', 'agi', 'dex', 'res']
#~   end
#~ end  

#~ class RPG::BaseItem
#~   
#~   def process_craft_growth
#~     iex_erth_crft_rpgui_cache unless @iex_eq_stat_complete
#~     @atk = 0 if @atk.nil?
#~     @def = 0 if @def.nil?
#~     @spi = 0 if @spi.nil?
#~     @agi = 0 if @agi.nil?
#~     @dex = 0 if @dex.nil?
#~     @res = 0 if @res.nil?
#~     for growth in @iex_craftgrowths
#~       case growth.upcase
#~       #======================================================#  
#~       # Start Editting
#~       #======================================================#
#~       # Growth Classes
#~       when "NORMAL", "DEFAULT"
#~         @atk += 1
#~         @def += 1
#~         @spi += 1
#~         @agi += 1
#~         @dex += 1
#~         @res += 1
#~       when "MAGIC"
#~         @atk += 0
#~         @def += 0
#~         @spi += 2
#~         @agi += 1
#~         @dex += 1
#~         @res += 2
#~       when "SWIFT"  
#~         @atk += 1
#~         @def += 1
#~         @spi += 0
#~         @agi += 2
#~         @dex += 2
#~         @res += 0
#~       when "HEAVY"  
#~         @atk += 2
#~         @def += 2
#~         @spi += 0
#~         @agi += 1
#~         @dex += 1
#~         @res += 0
#~       when "DUAL"
#~         @atk += 0
#~         @def += 0
#~         @spi += 0
#~         @agi += 3
#~         @dex += 3
#~         @res += 0
#~       # Weapon Type Growths  
#~       when "SWORD"
#~         @atk += 1
#~         @def += 1
#~         @spi += 1
#~         @agi += 1
#~         @dex += 1
#~         @res += 1
#~       when "SPEAR"  
#~         @atk += 2
#~         @def += 1
#~         @spi += 0
#~         @agi += 1
#~         @dex += 2
#~         @res += 0
#~       when "SCYTHE" 
#~         @atk += 2
#~         @def += 0
#~         @spi += 2
#~         @agi += 0
#~         @dex += 0
#~         @res += 2
#~       when "BRUSH"   
#~         @atk += 1
#~         @def += 0
#~         @spi += 2
#~         @agi += 0
#~         @dex += 1
#~         @res += 2
#~       when "HAMMER"  
#~         @atk += 3
#~         @def += 2
#~         @spi += 0
#~         @agi += 0
#~         @dex += 1
#~         @res += 0
#~       when "BOMB"  
#~         @atk += 1
#~         @def += 1
#~         @spi += 0
#~         @agi += 0
#~         @dex += 3
#~         @res += 1
#~       when "FAN"  
#~         @atk += 1
#~         @def += 2
#~         @spi += 1
#~         @agi += 1
#~         @dex += 0
#~         @res += 1
#~       when "BOW"  
#~         @atk += 2
#~         @def += 0
#~         @spi += 0
#~         @agi += 2
#~         @dex += 2
#~         @res += 0 
#~       when "STAFF"  
#~         @atk += 1
#~         @def += 0
#~         @spi += 3
#~         @agi += 0
#~         @dex += 0
#~         @res += 2
#~       when "CLAW" 
#~         @atk += 2
#~         @def += 0
#~         @spi += 1
#~         @agi += 1
#~         @dex += 1
#~         @res += 1
#~       when "KNIFE" 
#~         @atk += 1
#~         @def += 0
#~         @spi += 0
#~         @agi += 3
#~         @dex += 2
#~         @res += 0
#~       when "GUN" 
#~         @atk += 2
#~         @def += 1
#~         @spi += 0
#~         @agi += 1
#~         @dex += 2
#~         @res += 0 
#~       when "CHAINS"      
#~         @atk += 1
#~         @def += 1
#~         @spi += 1
#~         @agi += 1
#~         @dex += 1
#~         @res += 1
#~       #======================================================#  
#~       # Stop Editting
#~       #======================================================#
#~       end  
#~     end  
#~   end
#~   
#~ end

#~ module IEX
#~   module REGEXP
#~     module CRAFT_SYSTEM
#~       EQ_GROWTH  = /<(\w+)[ ]*(?:EQ_GROWTH|eq growth):?[ ]*([\+\-]?\d+)>/i
#~       EQ_GROWTH2 = /<(\w+)[ ]*(?:EQ_GROWTH|eq growth):?[ ]*([\+\-]?\d+)([%％])>/i
#~       MAX_LEVEL  = /<(?:CRAFT_MAX|craft max):?[ ]*(\d+)>/i 
#~       GROWTHS    = /<(?:GROWTH_CLASS|growth class):?[ ]*(.*)>/i
#~     end  
#~   end
#~ end

#~ class RPG::BaseItem
#~   
#~   attr_accessor :iex_eq_original_id
#~   
#~   def iex_erth_crft_rpgui_cache
#~     @iex_eq_stat_complete = true
#~     @iex_craft_exp = 0
#~     @iex_craft_level = 0 
#~     @iex_craft_points = 0
#~     @iex_craftexp_list = []
#~     @iex_craftgrowths = []
#~     @iex_recrafts = 0
#~     @craft_level_limit = IEX::CRAFT_SYSTEM::MAX_CRAFT_LEVEL
#~  
#~     @iex_eq_original_id = @id
#~     @iex_eq_stat_rate = {}
#~     @iex_eq_stat_change = {}
#~     IEX::CRAFT_SYSTEM::CRAFT_STATS.each { |sta|
#~       @iex_eq_stat_rate[sta.to_s.upcase] = 0
#~       @iex_eq_stat_change[sta.to_s.upcase] = 0
#~     }
#~     self.note.split(/[\r\n]+/).each { |line|
#~     case line
#~     when IEX::REGEXP::CRAFT_SYSTEM::EQ_GROWTH
#~       @iex_eq_stat_change[$1.to_s.upcase] = $2.to_i
#~     when IEX::REGEXP::CRAFT_SYSTEM::EQ_GROWTH2
#~       @iex_eq_stat_rate[$1.to_s.upcase] = $2.to_i
#~     when IEX::REGEXP::CRAFT_SYSTEM::MAX_LEVEL
#~       @craft_level_limit = $1.to_i
#~     when IEX::REGEXP::CRAFT_SYSTEM::GROWTHS
#~       @iex_craftgrowths.push($1.to_s)
#~     end  
#~     }
#~     make_craft_exp_list
#~     @iex_eq_stat_complete = true
#~   end  
#~   
#~   def original_name
#~     return @name
#~   end
#~   
#~   def name
#~     case self
#~     when RPG::Weapon, RPG::Armor
#~       return @name + " #{Vocab.level_a} #{craft_level}"
#~     else ; return @name
#~     end  
#~   end
#~   
#~   def make_craft_exp_list
#~     iex_erth_crft_rpgui_cache unless @iex_eq_stat_complete
#~     for i in 0..@craft_level_limit
#~       @iex_craftexp_list[i] = (100 * i) + (@iex_recrafts * 20)
#~     end 
#~   end
#~   
#~   def eq_stat_change(stat)
#~     iex_erth_crft_rpgui_cache unless @iex_eq_stat_complete
#~     return @iex_eq_stat_change[stat.to_s.upcase]
#~   end
#~   
#~   def eq_stat_rate(stat)
#~     iex_erth_crft_rpgui_cache unless @iex_eq_stat_complete
#~     return @iex_eq_stat_rate[stat.to_s.upcase]
#~   end
#~   
#~   def original_id
#~     iex_erth_crft_rpgui_cache unless @iex_eq_stat_complete
#~     return @iex_eq_original_id 
#~   end
#~   
#~   def craft_exp
#~     iex_erth_crft_rpgui_cache unless @iex_eq_stat_complete
#~     check_clevel_up
#~     return @iex_craft_exp
#~   end
#~   
#~   def craft_current_level_exp
#~     iex_erth_crft_rpgui_cache unless @iex_eq_stat_complete
#~     check_clevel_up
#~     return @iex_craftexp_list[@iex_craft_level + 1] - @iex_craftexp_list[@iex_craft_level]
#~   end
#~   
#~   def craft_current_exp
#~     iex_erth_crft_rpgui_cache unless @iex_eq_stat_complete
#~     check_clevel_up
#~     return @iex_craft_exp - @iex_craftexp_list[@iex_craft_level]
#~   end
#~   
#~   def craft_level_exp
#~     iex_erth_crft_rpgui_cache unless @iex_eq_stat_complete
#~     return @iex_craftexp_list[@iex_craft_level + 1]
#~   end
#~   
#~   def craft_exp_next
#~     iex_erth_crft_rpgui_cache unless @iex_eq_stat_complete
#~     return @iex_craftexp_list[@iex_craft_level + 1] - craft_exp
#~   end
#~   
#~   def craft_level
#~     iex_erth_crft_rpgui_cache unless @iex_eq_stat_complete
#~     check_clevel_up
#~     return @iex_craft_level
#~   end
#~   
#~   def recraft_count
#~     iex_erth_crft_rpgui_cache unless @iex_eq_stat_complete
#~     return @iex_recrafts
#~   end
#~   
#~   def craft_points
#~     iex_erth_crft_rpgui_cache unless @iex_eq_stat_complete
#~     return @iex_craft_points
#~   end  
#~   
#~   def craft_gain_exp(amt)
#~     iex_erth_crft_rpgui_cache unless @iex_eq_stat_complete
#~     self.craft_exp += amt
#~   end
#~   
#~   def craft_exp=(val) 
#~     iex_erth_crft_rpgui_cache unless @iex_eq_stat_complete
#~     @iex_craft_exp = [[val, 0].max, @iex_craftexp_list[@craft_level_limit]].min
#~     check_clevel_up
#~   end
#~    
#~   def check_clevel_up
#~     iex_erth_crft_rpgui_cache unless @iex_eq_stat_complete
#~     while @iex_craft_exp >= @iex_craftexp_list[@iex_craft_level+1] and @iex_craftexp_list[@iex_craft_level+1] > 0
#~       craft_level_up
#~     end 
#~   end
#~   
#~   def craft_level_up
#~     iex_erth_crft_rpgui_cache unless @iex_eq_stat_complete
#~     @iex_craft_level += 1
#~     @iex_craft_points += 1
#~     process_craft_growth
#~   end
#~   
#~   def recraft
#~     iex_erth_crft_rpgui_cache unless @iex_eq_stat_complete
#~     @iex_craft_exp = 0
#~     @iex_craft_level = 0 
#~     @iex_craft_points += 1
#~     @iex_recrafts += 1
#~     make_craft_exp_list
#~   end
#~   
#~ end

#~ module IEX
#~   module ObjectSeperation
#~   
#~     def self.create(obj, type)
#~       case type
#~       when 0
#~         new_i_obj = obj.dup
#~         new_i_obj.id = $data_items.size
#~         $data_items.push(new_i_obj)
#~         return new_i_obj.id
#~       when 1
#~         new_w_obj = obj.dup
#~         new_w_obj.id = $data_weapons.size
#~         $data_weapons.push(new_w_obj)
#~         for i in 0..$data_classes.size
#~           next if $data_classes[i].nil?
#~           if $data_classes[i].weapon_set.include?(obj.id)
#~             $data_classes[i].weapon_set.push(new_w_obj.id)
#~           end
#~         end
#~         return new_w_obj.id
#~       when 2
#~         new_a_obj = obj.dup
#~         new_a_obj.id = $data_armors.size
#~         $data_armors.push(new_a_obj)
#~         for i in 0..$data_classes.size
#~           next if $data_classes[i].nil?
#~           if $data_classes[i].armor_set.include?(obj.id)
#~             $data_classes[i].armor_set.push(new_a_obj.id)
#~           end
#~         end
#~         return new_a_obj.id
#~       end  
#~     end
#~     
#~   end
#~ end

#~ module IEX
#~   module ECS_Craft_System
#~       
#~     def self.greatest_stat(obj)
#~       stat_ar = []
#~       stat_ar.push([obj.atk, 'atk'])
#~       stat_ar.push([obj.def, 'def'])
#~       stat_ar.push([obj.spi, 'spi'])
#~       stat_ar.push([obj.agi, 'agi'])
#~       stat_ar.sort!
#~       stat_ar.reverse!
#~       return stat_ar
#~     end
#~     
#~     def self.apply_properties(obj, props)
#~       mode = 0
#~       case obj
#~       when RPG::UsableItem
#~         mode = 1
#~       when RPG::Weapon
#~         mode = 2
#~       when RPG::Armor
#~         mode = 2
#~       end 
#~       case mode 
#~       when 1 
#~         for pro in props
#~           case pro.to_s
#~           when /(\w+):[ ]*([\+\-]?\d+)/i
#~             case $1.to_s.upcase
#~             when "DMG"
#~               obj.base_damage += $2.to_i
#~             when "ATF"
#~               obj.atk_f += $2.to_i
#~             when "SPF"
#~               obj.spi_f += $2.to_i
#~             when "HP"
#~               obj.hp_recovery += $2.to_i
#~             when "HPR"  
#~               obj.hp_recovery_rate += $2.to_i
#~             when "MP"
#~               obj.mp_recovery += $2.to_i
#~             when "MPR"      
#~               obj.hp_recovery_rate += $2.to_i
#~             end
#~           end  
#~         end  
#~       when 2
#~         for pro in props
#~           case pro.to_s
#~           when /(\w+):[ ]*([\+\-]?\d+)/i
#~             case $1.to_s.upcase
#~             when "ATK"
#~               obj.atk += $2.to_i
#~             when "DEF"
#~               obj.def += $2.to_i
#~             when "SPI"
#~               obj.spi += $2.to_i
#~             when "AGI"  
#~               obj.agi += $2.to_i
#~             when "DEX"  
#~               obj.dex += $2.to_i
#~             when "RES"
#~               obj.res += $2.to_i
#~             when "MAXHP"  
#~               obj.maxhp += $2.to_i
#~             when "MAXMP"  
#~               obj.maxmp += $2.to_i
#~             end  
#~           end  
#~         end 
#~       end
#~       return obj
#~     end

#~     def self.original_obj(obj)
#~       case obj
#~       when RPG::UsableItem
#~         return $data_items[obj.original_id]
#~       when RPG::Weapon
#~         return $data_weapons[obj.original_id]
#~       when RPG::Armor
#~         return $data_armors[obj.original_id]
#~       end 
#~       return nil
#~     end
#~     
#~     def self.generate_name(orobj, newobj)
#~       grst = greatest_stat(newobj)
#~       addition = '%s'
#~       dif = grst[0][0] - grst[1][0]
#~       if (0..5).to_a.include?(dif)
#~         case [grst[0][1].upcase, grst[1][1].upcase]
#~         when ["ATK", "DEF"]
#~           addition = "SlayerX %s"
#~         when ["ATK", "SPI"]
#~           addition = "Ninja %s"
#~         when ["ATK", "AGI"]
#~           addition = "Beserker %s"
#~         when ["DEF", "ATK"]
#~           addition = "FortX %s"
#~         when ["DEF", "SPI"]
#~           addition = "Guard Mage %s"
#~         when ["DEF", "AGI"] 
#~           addition = "Blockader %s"
#~         when ["SPI", "DEF"]
#~           addition = "Mage Fort %s"
#~         when ["SPI", "ATK"]
#~           addition = "MageX %s"
#~         when ["SPI", "AGI"]  
#~           addition = "Tactician %s"
#~         when ["AGI", "DEF"]
#~           addition = "Parry %s"
#~         when ["AGI", "SPI"]
#~           addition = "Shogun %s"
#~         when ["AGI", "ATK"] 
#~           addition = "Assasin %s"
#~         end  
#~       else
#~         case grst[0][1].upcase
#~         when "ATK"
#~           addition = "Slayer %s"
#~         when "DEF"
#~           addition = "Fort %s"
#~         when "SPI" 
#~           addition = "Mage's %s"
#~         when "AGI" 
#~           addition = "Escapee %s"
#~         end  
#~       end  
#~       name = sprintf(addition, orobj.original_name)
#~       return name
#~     end
#~    
#~     def self.craft(obj, props)
#~       case obj
#~       when RPG::UsableItem
#~         newid = IEX::ObjectSeperation.create(obj, 0)
#~         return self.apply_properties($data_items[newid], props)
#~       when RPG::Weapon
#~         newid = IEX::ObjectSeperation.create(obj, 1)
#~         return self.apply_properties($data_weapons[newid], props)
#~       when RPG::Armor
#~         newid = IEX::ObjectSeperation.create(obj, 2)
#~         return self.apply_properties($data_armors[newid], props)
#~       end  
#~       return nil
#~     end
#~     
#~   end
#~ end

#~ class Game_Battler
#~   
#~   def gain_craft_exp(amt, type)
#~   end
#~   
#~   alias iex_ecs_attack_effect attack_effect unless $@
#~   def attack_effect(attacker)
#~     ecs_grab = iex_ecs_attack_effect(attacker) 
#~     if self.actor?
#~       if @missed or @evaded
#~         gain_craft_exp(5, 1)
#~       end  
#~       gain_craft_exp(1, 1)
#~     end    
#~     if attacker.actor?
#~       if self.dead?
#~         attacker.gain_craft_exp(20, 2)
#~       end  
#~       unless (@skipped or @missed or @evaded)
#~         attacker.gain_craft_exp(1, 2)
#~       end  
#~     end 
#~     return ecs_grab
#~   end
#~   
#~ end

#~ class Game_Actor < Game_Battler
#~     
#~   def gain_craft_exp(amt, type)
#~     case type
#~     when 0
#~       for eq in equips
#~         next if eq == nil
#~         eq.craft_gain_exp(amt)
#~       end  
#~     when 1
#~       for arm in armors
#~         next if arm == nil
#~         arm.craft_gain_exp(amt)
#~       end
#~     when 2
#~       for wep in weapons
#~         next if wep == nil
#~         wep.craft_gain_exp(amt)
#~       end
#~     end  
#~   end
#~   
#~ end

#~ class Game_Party < Game_Unit
#~   
#~   #--------------------------------------------------------------------------
#~   # * Gain Items (or lose)
#~   #     item          : Item
#~   #     n             : Number
#~   #     include_equip : Include equipped items
#~   #--------------------------------------------------------------------------
#~   alias iex_ecs_gain_item gain_item unless $@
#~   def gain_item(item, n, include_equip = false)
#~     if n > 0
#~       n.times do
#~         case item 
#~         when RPG::UsableItem
#~           item = item
#~         when RPG::Weapon
#~           newid = IEX::ObjectSeperation.create(item, 1)
#~           item = $data_weapons[newid]
#~         when RPG::Armor
#~           newid = IEX::ObjectSeperation.create(item, 2)
#~           item = $data_armors[newid]   
#~         end  
#~         iex_ecs_gain_item(item, 1, include_equip) 
#~       end
#~     else
#~       iex_ecs_gain_item(item, n, include_equip) 
#~     end
#~   end
#~   
#~ end

#~ class Scene_Title < Scene_Base

#~   alias iex_ecs_load_database load_database unless $@
#~   def load_database
#~     iex_ecs_load_database
#~     load_ecs_cache
#~   end
#~   
#~   def load_ecs_cache
#~     groups = [$data_weapons, $data_armors, $data_items]
#~     for group in groups
#~       for obj in group
#~         next if obj == nil
#~         obj.iex_erth_crft_rpgui_cache
#~       end  
#~     end  
#~   end
#~   
#~ end

#~ class Scene_File < Scene_Base
#~   
#~   alias iex_erth_crft_write_save_data write_save_data unless $@
#~   def write_save_data(file)
#~     iex_erth_crft_write_save_data(file)
#~     Marshal.dump($data_armors,    file)
#~     Marshal.dump($data_weapons,   file)
#~     Marshal.dump($data_items,     file)
#~     Marshal.dump($data_classes,   file)
#~   end
#~   
#~   alias iex_erth_crft_read_save_data read_save_data unless $@
#~   def read_save_data(file)
#~     iex_erth_crft_read_save_data(file)
#~     $data_armors  = Marshal.load(file)
#~     $data_weapons = Marshal.load(file)
#~     $data_items   = Marshal.load(file)
#~     $data_classes = Marshal.load(file)
#~   end  
#~   
#~ end

#~ class Scene_Earthen_Item_Catalog < Scene_Base
#~   
#~   def start
#~     super
#~     create_menu_background
#~     @windows = {}
#~     iwps = [Graphics.width / 2, Graphics.height / 2, Graphics.width / 2, Graphics.height / 2]
#~     @windows["ItemWindow"] = IEX_ECS_Item_Window.new([ ], 0, Graphics.height / 2, Graphics.width, Graphics.height / 2)
#~     @block_index = 0
#~     @intern_index = 0
#~     
#~     @item_blocks = {}
#~     @item_blocks[0] = []
#~     @item_blocks[1] = []
#~     @item_blocks[2] = []
#~     
#~     for filter in 0..2
#~       data = []
#~       case filter.to_i
#~       when 0 
#~         for ite in $game_party.items
#~           data.push(ite) if ite.is_a?(RPG::UsableItem)
#~         end  
#~       when 1
#~         for ite in $game_party.items
#~           data.push(ite) if ite.is_a?(RPG::Weapon)
#~         end 
#~       when 2
#~         for ite in $game_party.items
#~           data.push(ite) if ite.is_a?(RPG::Armor)
#~         end 
#~       end 
#~       stack = []
#~       for obj in data
#~         stack.push(obj) 
#~         if stack.size == 16
#~           @item_blocks[filter].push(stack.clone)
#~           stack.clear
#~           stack = []
#~         end  
#~       end 
#~       unless stack.empty?
#~         @item_blocks[filter].push(stack.clone)
#~         stack.clear
#~         stack = []
#~       end  
#~     end
#~     @windows["ItemWindow"].change_data(@item_blocks[@block_index][@intern_index])
#~     @windows["Help"] = IEX_Window_ECS_Help.new(iwps[0], 0, iwps[2], iwps[3])
#~     @windows["Help"].set_item(@windows["ItemWindow"].current_item) 
#~   end
#~   
#~   def terminate
#~     super
#~     dispose_menu_background
#~     for win in @windows.values
#~       next if win == nil
#~       win.dispose
#~       win = nil
#~     end  
#~     @windows.clear
#~     @windows = {}
#~   end
#~   
#~   def update
#~     super
#~     if Input.trigger?(Input::C)
#~       Sound.play_decision
#~     elsif Input.trigger?(Input::B)
#~       $scene = Scene_Map.new
#~     elsif Input.trigger?(Input::Y)
#~       if @item_blocks[@block_index].size > 0
#~         Sound.play_cursor      
#~         @intern_index = (@intern_index + 1) % @item_blocks[@block_index].size
#~         @windows["ItemWindow"].change_data(@item_blocks[@block_index][@intern_index])
#~       else
#~         Sound.play_buzzer
#~       end  
#~     elsif Input.trigger?(Input::X)  
#~       if @item_blocks[@block_index].size > 0
#~         Sound.play_cursor      
#~         @intern_index = (@intern_index - 1) % @item_blocks[@block_index].size
#~         @windows["ItemWindow"].change_data(@item_blocks[@block_index][@intern_index])
#~       else
#~         Sound.play_buzzer
#~       end
#~     elsif Input.trigger?(Input::Z)  
#~       Sound.play_cursor
#~       @intern_index = 0
#~       @block_index = (@block_index - 1) % 3
#~       @windows["ItemWindow"].change_data(@item_blocks[@block_index][@intern_index])
#~     end  
#~     @windows["Help"].set_item(@windows["ItemWindow"].current_item) 
#~     for win in @windows.values
#~       next if win == nil
#~       win.update if win.active
#~     end 
#~   end
#~   
#~ end

#~ class Scene_Earthen_Forge < Scene_Base
#~   
#~   def initialize
#~     super
#~   end

#~   def start
#~     super
#~     create_menu_background
#~     @windows = {}
#~     iwps = [Graphics.width / 2, Graphics.height / 2, Graphics.width, Graphics.height]
#~     @windows["ItemList"] = IEX_Window_Item_List.new(0, 0, iwps[0], iwps[3])
#~     @windows["WeaponList"] = IEX_Window_Item_List.new(0, 0, iwps[0], iwps[3])
#~     @windows["ArmorList"] = IEX_Window_Item_List.new(0, 0, iwps[0], iwps[3])
#~     @windows["ItemList"].change_filter(0)
#~     @windows["WeaponList"].change_filter(1)
#~     @windows["ArmorList"].change_filter(2)
#~     @windows["ItemList"].visible = false
#~     @windows["WeaponList"].visible = false
#~     @windows["ArmorList"].visible = false
#~     @item_list = ["ItemList", "WeaponList", "ArmorList"]
#~     @index = 0
#~     item_window_at_index_visible(@index, true)
#~     
#~     @windows["Help"] = IEX_Window_ECS_Help.new(iwps[0], 0, iwps[0], iwps[1])
#~     @windows["Help"].set_item(item_window_at_index_item)    
#~   end

#~   def terminate
#~     super
#~     for win in @windows.values
#~       next if win == nil
#~       win.dispose
#~       win = nil
#~     end  
#~     @windows.clear
#~     @windows = {}
#~   end
#~   
#~   def update
#~     super
#~     if Input.trigger?(Input::C)
#~       Sound.play_decision
#~       obj = item_window_at_index_item
#~       $game_party.lose_item(obj, 1)
#~       props = ["Atk: +2", "Def: +1", "Agi: +3"]
#~       orobj = IEX::ECS_Craft_System.original_obj(obj)
#~       opobj = IEX::ECS_Craft_System.craft(obj, props)
#~       opobj.recraft
#~       opobj.name = IEX::ECS_Craft_System.generate_name(orobj, opobj)
#~       $game_party.gain_item(opobj, 1)
#~       @windows[@item_list[@index]].refresh
#~     elsif Input.trigger?(Input::B)
#~       $scene = Scene_Map.new
#~     elsif Input.trigger?(Input::RIGHT)
#~       Sound.play_cursor
#~       item_window_at_index_visible(@index, false)
#~       @index = (@index + 1) % @item_list.size
#~       item_window_at_index_visible(@index, true)
#~     elsif Input.trigger?(Input::LEFT)  
#~       Sound.play_cursor
#~       item_window_at_index_visible(@index, false)
#~       @index = (@index - 1) % @item_list.size
#~       item_window_at_index_visible(@index, true)
#~     end  
#~     @windows["Help"].set_item(item_window_at_index_item)
#~     for win in @windows.values
#~       next if win == nil
#~       win.update if win.active
#~     end  
#~   end
#~   
#~   def item_window_at_index_visible(index, bool)
#~     @windows[@item_list[index]].visible = bool
#~     @windows[@item_list[index]].active  = bool
#~   end
#~   
#~   def item_window_at_index_item
#~     return @windows[@item_list[@index]].selected_item
#~   end
#~   
#~ end

#~ class IEX_Window_ECS_Help < Window_Base
#~   
#~   def initialize(x, y, width, height)
#~     super(x, y, width, height)
#~     @item = 0
#~   end
#~   
#~   def set_item(item)
#~     item = RPG::BaseItem.new if item == nil
#~     return if @item == item
#~     self.contents.clear
#~     self.contents.draw_text(0, 0, self.contents.width, 24, item.name, 1)
#~     if item.is_a?(RPG::UsableItem)
#~       draw_item_eq_growths(0, 24, item)
#~     elsif item.is_a?(RPG::Weapon)
#~       draw_parameters(0, 24, item, 0)
#~     elsif item.is_a?(RPG::Armor)
#~       draw_parameters(0, 24, item, 0)
#~     end  
#~     @item = item
#~   end
#~    
#~   def draw_item_eq_growths(x, y, item)
#~     draw_parameters(x, y, item, 1)
#~     draw_parameters(x, y+72, item, 2)
#~   end
#~   
#~   def draw_parameters(x, y, item, type)
#~     wd = self.contents.width / 2
#~     draw_stat(x, y, item, :atk, type)
#~     draw_stat(x+wd, y, item, :def, type)
#~     
#~     draw_stat(x, y+24, item, :spi, type)
#~     draw_stat(x+wd, y+24, item, :res, type)
#~     
#~     draw_stat(x, y+48, item, :agi, type)
#~     draw_stat(x+wd, y+48, item, :dex, type) 
#~     if type == 0
#~       draw_stat(x, y+72, item, :recraft) 
#~       
#~       draw_stat(x, y+96, item, :clevel)
#~       draw_stat(x+wd, y+96, item, :cpoint)
#~       
#~       draw_stat(x, y+120, item, :cexp)
#~       draw_stat(x+wd, y+120, item, :cexpnxt)
#~     end  
#~   end
#~   
#~   def draw_stat(x, y, item, stat, type = 0)
#~     vo = ""
#~     val = 0
#~     icon = 0
#~     case stat
#~     when :atk
#~       vo = sprintf("%s :", Vocab.atk)
#~       case type 
#~       when 0
#~         val = item.atk
#~       when 1
#~         val = item.eq_stat_rate("ATK")
#~       when 2  
#~         val = item.eq_stat_change("ATK")
#~       end  
#~      icon = IEX::CRAFT_SYSTEM::STATS_ICONS[:atk_icon]
#~     when :def
#~       vo = sprintf("%s :", Vocab.def)
#~       case type 
#~       when 0
#~         val = item.def
#~       when 1
#~         val = item.eq_stat_rate("DEF")
#~       when 2  
#~         val = item.eq_stat_change("DEF")
#~       end  
#~       icon = IEX::CRAFT_SYSTEM::STATS_ICONS[:def_icon]
#~     when :spi
#~       vo = sprintf("%s :", Vocab.spi)
#~       case type 
#~       when 0
#~         val = item.spi
#~       when 1
#~         val = item.eq_stat_rate("SPI")
#~       when 2  
#~         val = item.eq_stat_change("SPI")
#~       end  
#~       icon = IEX::CRAFT_SYSTEM::STATS_ICONS[:spi_icon]
#~     when :agi
#~       vo = sprintf("%s :", Vocab.agi)
#~       case type 
#~       when 0
#~         val = item.agi
#~       when 1
#~         val = item.eq_stat_rate("AGI")
#~       when 2  
#~         val = item.eq_stat_change("AGI")
#~       end  
#~       icon = IEX::CRAFT_SYSTEM::STATS_ICONS[:agi_icon]
#~     when :dex
#~       vo = sprintf("%s :", Vocab.dex)
#~       case type 
#~       when 0
#~         val = item.dex
#~       when 1
#~         val = item.eq_stat_rate("DEX")
#~       when 2  
#~         val = item.eq_stat_change("DEX")
#~       end  
#~       icon = IEX::CRAFT_SYSTEM::STATS_ICONS[:dex_icon]
#~     when :res
#~       vo = sprintf("%s :", Vocab.res)
#~       case type 
#~       when 0
#~         val = item.res
#~       when 1
#~         val = item.eq_stat_rate("RES")
#~       when 2  
#~         val = item.eq_stat_change("RES")
#~       end  
#~       icon = IEX::CRAFT_SYSTEM::STATS_ICONS[:res_icon]
#~     when :clevel
#~       vo = sprintf("%s :", "C-Level")
#~       val = item.craft_level
#~       icon= IEX::CRAFT_SYSTEM::CRAFT_ICONS[:clevel]
#~     when :cexp
#~       vo = sprintf("%s :", "C-Exp")
#~       val = item.craft_exp
#~       icon= IEX::CRAFT_SYSTEM::CRAFT_ICONS[:cexp]
#~     when :recraft
#~       vo = sprintf("%s :", "RC")
#~       val = item.recraft_count
#~       icon= IEX::CRAFT_SYSTEM::CRAFT_ICONS[:rccount]
#~     when :cpoint
#~       vo = sprintf("%s :", "C-Points")
#~       val = item.craft_points
#~       icon= IEX::CRAFT_SYSTEM::CRAFT_ICONS[:cpoint]
#~     when :cexpnxt
#~       vo = sprintf("%s :", "C-Next")
#~       val = item.craft_exp_next
#~       icon= IEX::CRAFT_SYSTEM::CRAFT_ICONS[:cexpnxt]
#~     end  
#~     #val = 0 if val == nil
#~     tx_wd = (self.contents.width / 2) - 32
#~     draw_icon(icon, x, y, val != 0)
#~     case stat
#~     when :clevel, :cpoint, :recraft, :cexp, :cexpnxt
#~       val = val
#~     else
#~       if val.to_i > 0
#~         val = sprintf("+%s", val.to_s)
#~       end 
#~     end  
#~     if type == 1
#~       val = sprintf("%s%", val.to_s)
#~       vo = sprintf("%s%", vo.to_s)
#~     elsif type == 2
#~       vo = sprintf("%s+", vo.to_s)
#~     end  
#~     self.contents.font.color = system_color
#~     self.contents.draw_text(x + 24, y, tx_wd, 24, vo, 0)
#~     self.contents.font.color = normal_color
#~     self.contents.font.size = 18
#~     self.contents.draw_text(x + 24, y, tx_wd, 28, val, 2)
#~     self.contents.font.size = Font.default_size 
#~   end
#~   
#~ end

#~ class IEX_Window_Item_List < Window_Selectable
#~   
#~   def initialize(x, y, width, height)
#~     super(x, y, width, height)
#~     @filter = -1
#~     @data = []
#~     @index = 0
#~     refresh
#~   end
#~   
#~   def selected_item
#~     return @data[@index]
#~   end
#~   
#~   def change_filter(new_filter)
#~     return if new_filter == nil
#~     @filter = new_filter
#~     refresh
#~   end
#~   
#~   def refresh
#~     @data = []
#~     case @filter.to_i
#~     when 0
#~       for ite in $game_party.items
#~         @data.push(ite) if ite.is_a?(RPG::UsableItem)
#~       end  
#~     when 1
#~       for ite in $game_party.items
#~         @data.push(ite) if ite.is_a?(RPG::Weapon)
#~       end   
#~     when 2
#~       for ite in $game_party.items
#~         @data.push(ite) if ite.is_a?(RPG::Armor)
#~       end  
#~     end
#~     @item_max = @data.size
#~     self.contents.clear
#~     create_contents
#~     for i in 0...@item_max
#~       draw_item(i)
#~     end  
#~     update
#~   end

#~   #--------------------------------------------------------------------------
#~   # * Draw Item
#~   #     index   : item number
#~   #     enabled : enabled flag. When false, draw semi-transparently.
#~   #--------------------------------------------------------------------------
#~   def draw_item(index, enabled = true)
#~     rect = item_rect(index)
#~     rect.x += 4
#~     rect.width -= 8
#~     self.contents.clear_rect(rect)
#~     self.contents.font.size = 18
#~     self.contents.font.color = normal_color
#~     self.contents.font.color.alpha = enabled ? 255 : 128
#~     if @data[index] != nil
#~       text = @data[index].name
#~       icon = @data[index].icon_index
#~       num  = $game_party.item_number(@data[index])
#~       clevel = @data[index].craft_level
#~     else
#~       text = "-----------------"
#~       icon = 98
#~       num  = 0
#~       clevel = 0
#~     end  
#~     draw_icon(icon, rect.x, rect.y, enabled)
#~     rect.x += 24
#~     rect.width -= 8
#~     self.contents.draw_text(rect, text)
#~     draw_icon(144, self.contents.width - 24, rect.y)
#~     self.contents.draw_text(0, rect.y, self.contents.width - 24, rect.height, num, 2)
#~     self.contents.font.color = system_color
#~     draw_icon(IEX::CRAFT_SYSTEM::CRAFT_ICONS[:clevel], self.contents.width - 64, rect.y)
#~     self.contents.draw_text(0, rect.y, self.contents.width - 64, rect.height, clevel, 2)
#~   end
#~   
#~ end

#~ class IEX_ECS_Item_Window < ICY_HM_Window_Selectable
#~   
#~   def initialize(dat, x = 0, y = 0, width = Graphics.width, height = Graphics.height)
#~     @data = [] 
#~     super(x, y, width, height)
#~     @column_max = 12
#~     @item_sq_spacing = 42 #(self.width / 4) - 32
#~     @rect_size = 32
#~     @selection_size = 38
#~     @index = 0
#~     @data = dat
#~     refresh
#~   end
#~     
#~   def current_item
#~     return @data[@index]
#~   end
#~   
#~   def change_data(new_data)
#~     @data = new_data.to_a
#~     @index = 0
#~     refresh
#~   end
#~   
#~   def refresh 
#~     @item_max = 12 * 4
#~     self.contents.clear
#~     create_contents
#~     prep_coord_vars
#~     for i in 0...@item_max
#~       draw_item(i)
#~     end
#~   end
#~   
#~   def draw_item(index)
#~     rect = Rect.new(@nw_x, @nw_y, @rect_size, @rect_size)
#~     ite = @data[index]
#~     color1 = Color.new(166, 124, 82, 128)
#~     color2 = Color.new(126, 84, 42)
#~     self.contents.fill_rect(rect, color1)
#~     draw_border_rect(@nw_x, @nw_y, @rect_size, @rect_size, 4, color2)
#~     draw_icon(6482, @nw_x + 4, @nw_y + 4)
#~     unless ite == nil  
#~       draw_icon(ite.icon_index, @nw_x + 4, @nw_y + 4) 
#~     end  
#~     advance_space
#~   end
#~   
#~ end



