# IEX - Stat Points
#~ class Game_Actor < Game_Battler
#~   
#~   attr_reader :iex_base_points
#~   attr_reader :iex_bonus_points
#~   attr_reader :iex_temp_points
#~   
#~   alias iex_sta_pt_initialize initialize unless $@
#~   def initialize(*args)
#~     @iex_base_points = {}
#~     @iex_bonus_points = {}
#~     @iex_temp_points = {}
#~     ['maxhp', 'maxmp', 'atk', 'def', 'spi', 'agi', 'dex', 'res'].each { |sta|
#~       @iex_base_points[sta.to_s.downcase] = 1
#~       @iex_bonus_points[sta.to_s.downcase] = 0    
#~       @iex_temp_points[sta.to_s.downcase] = 0
#~     }
#~     @iex_free_points = 0
#~     iex_sta_pt_initialize(*args)
#~   end
#~   
#~   alias iex_sta_pt_setup setup unless $@
#~   def setup(*args)
#~     @iex_base_points = {}
#~     @iex_bonus_points = {}
#~     @iex_temp_points = {}
#~     ['maxhp', 'maxmp', 'atk', 'def', 'spi', 'agi', 'dex', 'res'].each { |sta|
#~       @iex_base_points[sta.to_s.downcase] = 1
#~       @iex_bonus_points[sta.to_s.downcase] = 0    
#~       @iex_temp_points[sta.to_s.downcase] = 0
#~     }
#~     @iex_free_points = 0
#~     iex_sta_pt_setup(*args)
#~     iex_assignLevelPoints
#~   end
#~   
#~   def iex_assignLevelPoints
#~     for sta in @iex_base_points.keys
#~       @iex_base_points[sta] = @level.to_i
#~     end  
#~     @hp = maxhp
#~     @mp = maxmp
#~   end
#~   
#~   def iex_free_points
#~     return @iex_free_points
#~   end
#~   
#~   def iex_free_points=(new_point)
#~     @iex_free_points = [new_point, 0].max
#~   end
#~   
#~   def iex_available_points
#~     po = @iex_free_points 
#~     for sta in @iex_temp_points.values
#~       po -= sta
#~     end  
#~     return po
#~   end
#~   
#~   IEX::BASE_STAT_FORMULAE::STATS.each { |stat|
#~     stat.to_s.downcase
#~   aStr = %Q(
#~   def #{stat}_base_points
#~     return Integer(@iex_base_points['#{stat}'])
#~   end
#~   
#~   def #{stat}_bonus_points
#~     return Integer(@iex_bonus_points['#{stat}'])
#~   end
#~   
#~   def #{stat}_temp_points
#~     return Integer(@iex_temp_points['#{stat}'])
#~   end
#~   
#~   def #{stat}_base_points=(new_#{stat})
#~     @iex_base_points['#{stat}'] = [new_#{stat}, 1].max
#~   end
#~   
#~   def #{stat}_bonus_points=(new_#{stat})
#~     @iex_bonus_points['#{stat}'] = [new_#{stat}, 1].max
#~   end
#~   
#~   def #{stat}_temp_points=(new_#{stat})
#~     @iex_temp_points['#{stat}'] = [new_#{stat}, 0].max
#~   end
#~   
#~   def #{stat}_stat_points
#~     points = #{stat}_base_points + #{stat}_bonus_points + #{stat}_temp_points
#~     return Integer(points)
#~   end
#~   
#~   )
#~   module_eval(aStr)
#~   }
#~   
#~   alias iex_stp_level_up level_up unless $@
#~   def level_up
#~     iex_stp_level_up
#~     level_up_stat_points
#~   end
#~   
#~   def level_up_stat_points
#~     for key in @iex_base_points.keys
#~       next if key == nil
#~       @iex_base_points[key] += 1
#~     end  
#~     self.iex_free_points += 1
#~   end
#~   
#~ end

#~ class IEX_Scene_Stat_Dist < Scene_Base
#~   
#~   def initialize(actor, called = false)
#~     super()
#~     @index_call = false
#~     @act_index = 0
#~     if actor.kind_of?(Game_Battler)
#~       @actor = actor
#~     elsif actor != nil  
#~       @actor = $game_party.members[actor]
#~       @act_index = actor
#~       @index_call = true
#~     else 
#~       @actor = nil
#~     end  
#~     @call_from_menu = called
#~   end
#~   
#~   def start
#~     super
#~     create_menu_background
#~     @windows = {}
#~     win_pos = [0, Graphics.width, Graphics.width / 2, Graphics.height, Graphics.height / 2]
#~     @windows["ActStats"] = IEX_Window_Stat_Points.new(@actor, win_pos[0], win_pos[0], win_pos[2], win_pos[4] + 16)
#~     @windows["Status"] = IEX_PT_Window_Status.new(@actor, win_pos[2], win_pos[0], win_pos[2], win_pos[4] + 48)
#~     for tms in @actor.iex_temp_points.keys
#~       @actor.iex_temp_points[tms] = 0
#~     end
#~   end
#~   
#~   def terminate
#~     super
#~     dispose_menu_background
#~     for tms in @actor.iex_temp_points.keys
#~       @actor.iex_temp_points[tms] = 0
#~     end
#~     for win in @windows.values
#~       next if win == nil
#~       win.dispose
#~       win = nil
#~     end  
#~     @windows.clear
#~     @windows = {}
#~   end
#~   
#~   def update
#~     super
#~     if Input.trigger?(Input::C)
#~       Sound.play_decision
#~       remo = 0
#~       for tms in @actor.iex_temp_points.keys
#~         next if tms == nil
#~         @actor.iex_base_points[tms] += @actor.iex_temp_points[tms]
#~         remo += @actor.iex_temp_points[tms]
#~         @actor.iex_temp_points[tms] = 0
#~       end  
#~       @actor.iex_free_points -= remo
#~       @windows["Status"].refresh
#~       @windows["ActStats"].refresh
#~     elsif Input.repeat?(Input::RIGHT)
#~       Sound.play_cursor
#~       sta = @windows["ActStats"].get_selected_item
#~       new_val = [@actor.iex_temp_points[sta.to_s] + 1, @actor.iex_available_points + @actor.iex_temp_points[sta.to_s]].min
#~       change_temp_stat(sta.to_sym, new_val.to_i)
#~       @windows["ActStats"].draw_item(@windows["ActStats"].index)
#~       @windows["Status"].refresh
#~     elsif Input.repeat?(Input::LEFT)
#~       Sound.play_cursor
#~       sta = @windows["ActStats"].get_selected_item
#~       new_val = [@actor.iex_temp_points[sta.to_s] - 1, 0].max
#~       change_temp_stat(sta.to_sym, new_val.to_i)
#~       @windows["ActStats"].draw_item(@windows["ActStats"].index)
#~       @windows["Status"].refresh
#~     elsif Input.trigger?(Input::R)  
#~       if @index_call 
#~         Sound.play_cursor
#~         @act_index = (@act_index + 1) % $game_party.members.size
#~         $scene = IEX_Scene_Stat_Dist.new(@act_index)
#~       else
#~         Sound.play_buzzer
#~       end  
#~     elsif Input.trigger?(Input::L)  
#~       if @index_call 
#~         Sound.play_cursor
#~         @act_index = (@act_index - 1) % $game_party.members.size
#~         $scene = IEX_Scene_Stat_Dist.new(@act_index)
#~       else  
#~         Sound.play_buzzer
#~       end 
#~     elsif Input.trigger?(Input::B)  
#~       Sound.play_cancel
#~       $scene = Scene_Map.new
#~     end  
#~     for win in @windows.values
#~       next if win == nil
#~       win.update if win.active
#~     end  
#~   end
#~   
#~   def change_temp_stat(stat, val)
#~     @actor.iex_temp_points[stat.to_s.downcase] = val
#~   end
#~   
#~ end

#~ class IEX_Window_Stat_Points < Window_Selectable
#~   
#~   def initialize(actor, x, y, width, height)
#~     super(x, y, width, height)
#~     @actor = actor
#~     @index = 0
#~     refresh
#~   end
#~   
#~   def get_selected_item
#~     return @data[@index]
#~   end  
#~   
#~   def change_actor(new_actor)
#~     @actor = new_actor
#~   end
#~   
#~   def refresh
#~     @data = [:maxhp, :maxmp, :atk, :def, :spi, :agi, :dex, :res]
#~     @item_max = @data.size
#~     self.contents.clear
#~     create_contents
#~     for i in 0...@item_max
#~       draw_item(i)
#~     end  
#~   end
#~   
#~   #--------------------------------------------------------------------------
#~   # * Draw Item
#~   #     index   : item number
#~   #     enabled : enabled flag. When false, draw semi-transparently.
#~   #--------------------------------------------------------------------------
#~   def draw_item(index, enabled = true)
#~     rect = item_rect(index)
#~     rect.x += 4
#~     rect.width -= 8
#~     self.contents.clear_rect(rect)
#~     self.contents.font.color = normal_color
#~     self.contents.font.color.alpha = enabled ? 255 : 128
#~     vo = ""
#~     bval = 0
#~     bnval = 0
#~     tmval = 0
#~     icon = 0
#~     case @data[index]
#~     when :maxhp
#~       vo   = sprintf("%s :", Vocab.hp_a)
#~       bval = @actor.maxhp_base_points 
#~       bnval= @actor.maxhp_bonus_points
#~       tmval= @actor.maxhp_temp_points
#~       icon = IEX::CRAFT_SYSTEM::STATS_ICONS[:hp_icon]
#~     when :maxmp  
#~       vo   = sprintf("%s :", Vocab.mp_a)
#~       bval = @actor.maxmp_base_points 
#~       bnval= @actor.maxmp_bonus_points
#~       tmval= @actor.maxmp_temp_points
#~       icon = IEX::CRAFT_SYSTEM::STATS_ICONS[:mp_icon]
#~     when :atk
#~       vo   = sprintf("%s :", Vocab.atk)
#~       bval = @actor.atk_base_points 
#~       bnval= @actor.atk_bonus_points
#~       tmval= @actor.atk_temp_points
#~       icon = IEX::CRAFT_SYSTEM::STATS_ICONS[:atk_icon]
#~     when :def
#~       vo   = sprintf("%s :", Vocab.def)
#~       bval = @actor.def_base_points  
#~       bnval= @actor.def_bonus_points
#~       tmval= @actor.def_temp_points
#~       icon = IEX::CRAFT_SYSTEM::STATS_ICONS[:def_icon]
#~     when :spi
#~       vo   = sprintf("%s :", Vocab.spi)
#~       bval = @actor.spi_base_points   
#~       bnval= @actor.spi_bonus_points
#~       tmval= @actor.spi_temp_points
#~       icon = IEX::CRAFT_SYSTEM::STATS_ICONS[:spi_icon]
#~     when :agi
#~       vo   = sprintf("%s :", Vocab.agi)
#~       bval = @actor.agi_base_points   
#~       bnval= @actor.agi_bonus_points
#~       tmval= @actor.agi_temp_points
#~       icon = IEX::CRAFT_SYSTEM::STATS_ICONS[:agi_icon]
#~     when :dex
#~       vo   = sprintf("%s :", Vocab.dex)
#~       bval = @actor.dex_base_points   
#~       bnval= @actor.dex_bonus_points
#~       tmval= @actor.dex_temp_points
#~       icon = IEX::CRAFT_SYSTEM::STATS_ICONS[:dex_icon]
#~     when :res
#~       vo   = sprintf("%s :", Vocab.res)
#~       bval = @actor.res_base_points   
#~       bnval= @actor.res_bonus_points
#~       tmval= @actor.res_temp_points
#~       icon = IEX::CRAFT_SYSTEM::STATS_ICONS[:res_icon]
#~     end   
#~     tx_wd = (self.contents.width / 2) - 32
#~     draw_icon(icon, rect.x, rect.y, tmval != 0) 
#~     mxval = bval + bnval + tmval
#~     self.contents.font.color = system_color
#~     self.contents.draw_text(rect.x + 24, rect.y, tx_wd, 24, vo, 0)
#~     self.contents.font.color = normal_color
#~     self.contents.font.size = 18
#~     self.contents.draw_text(rect.x + 24, rect.y, tx_wd, 28, bval, 2)
#~     bnval = sprintf("+ %s", bnval.to_s)
#~     self.contents.draw_text(rect.x + 24, rect.y, tx_wd + 32, 28, bnval, 2)
#~     tmval = sprintf("+ %s", tmval.to_s)
#~     self.contents.draw_text(rect.x + 24, rect.y, tx_wd + 64, 28, tmval, 2)
#~     mxval = sprintf("= %s", mxval.to_s)
#~     self.contents.draw_text(rect.x + 24, rect.y, tx_wd + 96, 28, mxval, 2)
#~     self.contents.font.size = Font.default_size 
#~   end
#~   
#~ end

#~ class IEX_PT_Window_Status < Window_Selectable
#~   
#~   def initialize(actor, x, y, width, height)
#~     super(x, y, width, height)
#~     @actor = actor
#~     refresh
#~   end
#~   
#~   def change_actor(new_actor)
#~     @actor = new_actor
#~   end
#~   
#~   def refresh
#~     @data = [:fr_pn, :maxhp, :maxmp, :atk, :def, :spi, :agi, :dex, :res]
#~     @item_max = @data.size
#~     self.contents.clear
#~     create_contents
#~     for i in 0...@item_max
#~       draw_item(i)
#~     end  
#~   end
#~   
#~   #--------------------------------------------------------------------------
#~   # * Draw Item
#~   #     index   : item number
#~   #     enabled : enabled flag. When false, draw semi-transparently.
#~   #--------------------------------------------------------------------------
#~   def draw_item(index, enabled = true)
#~     rect = item_rect(index)
#~     rect.x += 4
#~     rect.width -= 8
#~     self.contents.clear_rect(rect)
#~     self.contents.font.color = normal_color
#~     self.contents.font.color.alpha = enabled ? 255 : 128
#~     vo = ""
#~     bval = 0
#~     icon = 0
#~     case @data[index]
#~     when :fr_pn
#~       fr_pn = sprintf("Available Points: %d", @actor.iex_available_points)
#~       self.contents.draw_text(rect.x, rect.y, self.contents.width, 28, fr_pn, 1)
#~       return
#~     when :maxhp
#~       vo   = sprintf("%s :", Vocab.hp_a)
#~       bval = @actor.base_maxhp
#~       icon = IEX::CRAFT_SYSTEM::STATS_ICONS[:hp_icon]
#~     when :maxmp  
#~       vo   = sprintf("%s :", Vocab.mp_a)
#~       bval = @actor.base_maxmp
#~       icon = IEX::CRAFT_SYSTEM::STATS_ICONS[:mp_icon]
#~     when :atk
#~       vo   = sprintf("%s :", Vocab.atk)
#~       bval = @actor.base_atk 
#~       icon = IEX::CRAFT_SYSTEM::STATS_ICONS[:atk_icon]
#~     when :def
#~       vo   = sprintf("%s :", Vocab.def)
#~       bval = @actor.base_def
#~       icon = IEX::CRAFT_SYSTEM::STATS_ICONS[:def_icon]
#~     when :spi
#~       vo   = sprintf("%s :", Vocab.spi)
#~       bval = @actor.base_spi
#~       icon = IEX::CRAFT_SYSTEM::STATS_ICONS[:spi_icon]
#~     when :agi
#~       vo   = sprintf("%s :", Vocab.agi)
#~       bval = @actor.base_agi
#~       icon = IEX::CRAFT_SYSTEM::STATS_ICONS[:agi_icon]
#~     when :dex
#~       vo   = sprintf("%s :", Vocab.dex)
#~       bval = @actor.base_dex
#~       icon = IEX::CRAFT_SYSTEM::STATS_ICONS[:dex_icon]
#~     when :res
#~       vo   = sprintf("%s :", Vocab.res)
#~       bval = @actor.base_res
#~       icon = IEX::CRAFT_SYSTEM::STATS_ICONS[:res_icon]
#~     end   
#~     tx_wd = (self.contents.width / 2) - 32
#~     draw_icon(icon, rect.x, rect.y) 
#~     self.contents.font.color = system_color
#~     self.contents.draw_text(rect.x + 24, rect.y, tx_wd, 24, vo, 0)
#~     self.contents.font.color = normal_color
#~     self.contents.font.size = 18
#~     self.contents.draw_text(rect.x + 24, rect.y, tx_wd, 28, bval, 2)
#~     self.contents.font.size = Font.default_size 
#~   end
#~   
#~ end

