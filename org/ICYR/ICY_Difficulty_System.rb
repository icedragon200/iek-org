# ICY_Difficulty_System
module ICY
  module Difficulty_System
    RECT_SIZE = 72
    ITEM_SQ_SPACING = 86
    SELECTION_SIZE = 80
    BORDER_SIZE = 4
    COLUMNS_MAX = 1
    DIFFICULTY_WINDOW_SIZE = [((544 - 356) / 2), 56, 356, 360, 255]
    DIFFICULTY_WINDOW_HEADER_SIZE = [((544 - 356) / 2), 0, 356, 56, 255]
    DIFFICULTY_WINDOW_HEADER_TEXT = "Choose your difficulty level."
    DIFFICULTY_WINDOW_HEADER_ICON = 3306
    DIFFICULTY_LEVELS = [
   {"ID"          => 0,
    "Name"        => "~Squire~",
    "Icon_Index"  => 1,
    "Color"       => ICY::Colors::LightGreen,
    "Explanation" => ["Enemies are push overs.",
                      "Items cost less.", 
                      "Exp is easy to get."],
    "Effect"      => "exp",
   },
   
   {"ID"          => 1,
    "Name"        => "~Lancer~",
    "Icon_Index"  => 2,
    "Color"       => ICY::Colors::LightBlue,
    "Explanation" => ["Balanced Game.",
                      "No Changes."],
    "Effect"      => "exp",
   },
   
   {"ID"          => 2,
    "Name"        => "~Collusus Lancer~",
    "Icon_Index"  => 3,
    "Color"       => ICY::Colors::LightCoral,
    "Explanation" => ["Enemies will try to maul you.",
                      "Items cost a fortune!", 
                      "Exp is harder to get!"],
    "Effect"      => "exp",
   },
   
    ]
    DIFFICULTY_VARIABLE = 14
  end
end

class Game_System
  
  def difficulty_level
    for i in ICY::Difficulty_System::DIFFICULTY_LEVELS
      if i["ID"] == $game_variables[ICY::Difficulty_System::DIFFICULTY_VARIABLE]
        return i
      end
    end
  end
  
  def difficulty_name
    diff_lv = difficulty_level
    return diff_lv["Name"]
  end
  
   def difficulty_id
    diff_lv = difficulty_level
    return diff_lv["ID"]
  end
  
end

#==============================================================================
# ** Window_Message
#------------------------------------------------------------------------------
#  This message window is used to display text.
#==============================================================================
class Window_Message < Window_Selectable
  # Alias stuff 
  alias icy_difficulty_convert_special_characters convert_special_characters unless $@
  def convert_special_characters(*args)
  icy_difficulty_convert_special_characters(*args)
  # Code for specific map name
  @text.gsub!(/\\DIFLV/i)    {$game_system.difficulty_name}
  end
end

#==============================================================================
# ** ICY_Difficulty_Header_Window
#------------------------------------------------------------------------------
#  This window displays a header.
#==============================================================================
class ICY_Difficulty_Header_Window < Window_Base
  attr_accessor :font_size
  #--------------------------------------------------------------------------
  # * Object Initialization
  #     x      : window x-coordinate
  #     y      : window y-coordinate
  #     width  : window width
  #     height : window height
  #-------------------------------------------------------------------------- 
  def initialize(arx = 0, y = 0, width = 544, height = 56)
    if arx.is_a?(Array)
      x = arx[0]
      y = arx[1]
      width = arx[2]
      height = arx[3]
    else 
      x = arx
    end
    super(x, y, width, height)
    @font_size = 26
  end
  #--------------------------------------------------------------------------
  # ** Reset Font Size
  #   Resets the font size to 26
  #--------------------------------------------------------------------------
  def reset_font_size
    @font_size = 26
  end
  #--------------------------------------------------------------------------
  # ** Set Header
  #     text   : text
  #     icon   : icon_index
  #     align  : align
  #-------------------------------------------------------------------------- 
  def set_header(text = "" , icon = nil, align = 0)
    icon_offset = 32
    if icon == nil
      ix = 0
    else
      ix = icon_offset
      draw_icon(icon, 0, 0)
    end
     iy = 0
     old_font_size = self.contents.font.size
     self.contents.font.size = @font_size
     self.contents.font.color = system_color
     self.contents.draw_text(ix, iy, (self.width - ix) - 48, WLH, text, align)
     self.contents.font.color = normal_color
     self.contents.font.size = old_font_size
  end
  
end

#==============================================================================
# ** ICY_Difficulty_Window
#------------------------------------------------------------------------------
#  This window deals with difficulty command choices.
#==============================================================================
class ICY_Difficulty_Window < ICY_HM_Window_Selectable
  
  include ICY::Difficulty_System
  
  def initialize(arx = 0, y = 0, width = 312, height = 370)
    if arx.is_a?(Array)
      x = arx[0]
      y = arx[1]
      width = arx[2]
      height = arx[3]
    else 
      x = arx
    end
    super(x, y, width, height)
    self.opacity = arx[4]
    @column_max = COLUMNS_MAX
    @data = []
    @column_max = COLUMNS_MAX
    @item_sq_spacing = ITEM_SQ_SPACING
    @rect_size = RECT_SIZE
    @selection_size = SELECTION_SIZE
    self.index = 0
    refresh
  end
  
  def off_x
    return 12
  end
  
  def off_y
    return 32
  end
  
  def refresh
    @reset = off_x
    @nw_x = @reset
    @nw_y = off_y
    @coun = 0
    @data = DIFFICULTY_LEVELS
    @item_max = @data.size
    create_contents
    self.contents.clear
    for i in @data
      draw_item(i)
    end
  end
  
  def draw_item(set)
    self.contents.font.color = normal_color
    draw_outlined_icon(set["Icon_Index"], 
                       @nw_x, @nw_y, @rect_size, @rect_size, BORDER_SIZE, 
                       set["Color"])
    self.contents.font.size = 21
    self.contents.draw_text(@nw_x + @rect_size + 4, @nw_y, self.width - (@rect_size + 4), 21, set["Name"])
    self.contents.font.size = 18
    for i in 0..set["Explanation"].size
      self.contents.draw_text(@nw_x + @rect_size + 8, (@nw_y + (18 * i)) + 18, 
                              self.width - (@rect_size + 8), 18,
                              set["Explanation"][i])
    end
    advance_space
  end
  
  def current_difficulty
    return @data[self.index]
  end
  
end

#==============================================================================
# ** Scene_Title
#------------------------------------------------------------------------------
#  This class performs the title screen processing.
#==============================================================================
class Scene_Title < Scene_Base
  
  alias icy_difficulty_start start unless $@
  def start
    icy_difficulty_start
    @difficulty_window = ICY_Difficulty_Window.new(ICY::Difficulty_System::DIFFICULTY_WINDOW_SIZE)
    @difficulty_window.active = false
    @difficulty_window.visible = false
    @difficulty_window_header = ICY_Difficulty_Header_Window.new(ICY::Difficulty_System::DIFFICULTY_WINDOW_HEADER_SIZE)
    @difficulty_window_header.set_header(ICY::Difficulty_System::DIFFICULTY_WINDOW_HEADER_TEXT, 
                                         ICY::Difficulty_System::DIFFICULTY_WINDOW_HEADER_ICON)
    @difficulty_window_header.visible = false
  end
  
  alias icy_difficulty_terminate terminate unless $@
  def terminate
    icy_difficulty_terminate
    dispose_difficulty_window
  end
  
  def dispose_difficulty_window
    if @difficulty_window != nil
      @difficulty_window.dispose
      @difficulty_window = nil
    end
    if @difficulty_window_header != nil
      @difficulty_window_header.dispose
      @difficulty_window_header = nil
    end
  end
  
  alias icy_difficulty_command_new_game command_new_game unless $@
  def command_new_game
    Sound.play_decision
    command_difficulty
  end
  
  def command_difficulty
    @command_window.active = false
    @difficulty_window.active = true
    @difficulty_window.visible = true
    @difficulty_window_header.visible = true
  end
  
  alias icy_difficulty_command_update update unless $@
  def update
    if @difficulty_window.active
      @difficulty_window.update
       if Input.trigger?(Input::C)
         difficulty = @difficulty_window.current_difficulty
         $game_variables[ICY::Difficulty_System::DIFFICULTY_VARIABLE] = difficulty["ID"]
         @difficulty_window.active = false
         @difficulty_window.visible = false
         @difficulty_window_header.visible = false
         icy_difficulty_command_new_game
         return
       elsif Input.trigger?(Input::B)
         Sound.play_cancel
         @command_window.active = true
         @difficulty_window.active = false
         @difficulty_window.visible = false
         @difficulty_window_header.visible = false
         @difficulty_window.index = 0
        end 
    end
    icy_difficulty_command_update
  end
  
end
