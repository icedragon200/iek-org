# ICY_Interactac_System
#------------------------------------------------------------------------------#
#Interative Menu V 2
#Created by :IceDragon 
#Date Modified : 7/19/2010
#------------------------------------------------------------------------------#
#This is an Earthen Exclusive Script
#This is the interaction System..
$imported = {} if $imported == nil
$imported["ICY_Interact_System"] = true
module ICY
  module Interaction_Menu
    Trustmax = 200
    Trustees = [2, 4, 5, 6, 7, 8]
    TrusteesVariables = {
  #Actor_id => Friendship_Variable
    2 => 200, # Elli
    4 => 201, # Chika
    5 => 202, # Spyet
    6 => 203, # Nekome
    7 => 204, # Icaria
    8 => 205, # Len
    }
    def self.get_heart_image(val)
      case val
      when 0..30
        return "DullHeart"
      when 31..90
        return "StoneHeart"
      when 91..130
        return "PaleHeart"
      when 131..180
        return "LoveHeart"
      when nil
        return "Question"
      else
        return "FullLoveHeart"
      end
    end
  
  end
end

class Game_Party < Game_Unit
  attr_accessor :partner
  
  alias icy_interac_partner_initialize initialize
  def initialize
    icy_interac_partner_initialize
    @partner = nil
  end
  
  def set_partner(id)
    if ICY::Interaction_Menu::Trustees.include?(id)
      @partner = id
    end
  end
  
end

class Scene_Interac_Menu < Scene_Base
  
  def initialize
    @windows = []
  end
  #--------------------------------------------------------------------------
  # * Start processing
  #--------------------------------------------------------------------------
  def start
    super
    create_menu_background
    create_windows
  end
  
  def create_windows
    @windows[0] = Interac_Window.new
    @windows[1] = ICY_Interac_Header_Window.new(0, 0, 544, 56)
    @windows[1].set_header("Interaction", 219)
  end
  
  def dispose_windows
    for windows in @windows
     windows.close
    begin
      windows.update
      Graphics.update
    end until windows.openness == 0
    end
  end
  
  def update
    super
    if Input.trigger?(Input::B)
      Sound.play_cancel
      dispose_windows
      $scene = Scene_Menu.new
    end
  end
  
  def terminate
    dispose_menu_background
  end
  
  def perform_transition
   Graphics.transition(60, "Graphics/Transitions/013-Square01") 
  end
  
end
#==============================================================================
# ** ICY_Header_Window
#------------------------------------------------------------------------------
#  This window displays a header.
#==============================================================================
class ICY_Interac_Header_Window < Window_Base
  attr_accessor :font_size
  #--------------------------------------------------------------------------
  # * Object Initialization
  #     x      : window x-coordinate
  #     y      : window y-coordinate
  #     width  : window width
  #     height : window height
  #-------------------------------------------------------------------------- 
  def initialize(x, y, width, height)
    super(x, y, width, height)
    @font_size = 26
  end
  #--------------------------------------------------------------------------
  # ** Reset Font Size
  #   Resets the font size to 26
  #--------------------------------------------------------------------------
  def reset_font_size
    @font_size = 26
  end
  #--------------------------------------------------------------------------
  # ** Set Header
  #     text   : text
  #     icon   : icon_index
  #     align  : align
  #-------------------------------------------------------------------------- 
  def set_header(text = "" , icon = nil, align = 0)
    icon_offset = 32
    if icon == nil
      x = 0
    else
      x = icon_offset
      draw_icon(icon, 0, 0)
    end
     y = 0
     old_font_size = self.contents.font.size
     self.contents.font.size = @font_size
     self.contents.font.color = system_color
     self.contents.draw_text(x, y, (self.width - x) - 48, WLH, text, align)
     self.contents.font.color = normal_color
     self.contents.font.size = old_font_size
  end
  
end

class Interac_Window < Window_Base
  
  def initialize
    super(0, 56, 544, 416 - 56)
    @main_bar = []
    @partner = $game_party.partner
    draw_main_actor
    draw_partner if @partner != nil
    draw_trustees
  end
  
  def draw_trust_bars(x, y, varz)
    if varz != nil
    var = $game_variables[varz.to_i]
     else
    var = 0
  end
    maxvaluez = ICY::Interaction_Menu::Trustmax
    width = 64
    height = 8
    @base = Bitmap.new(width, height)
    @base.gradation_rect(0, 0, width, height, ICY::Colors::Gray, ICY::Colors::Black)
    bar = Bitmap.new(width, height)
    bar.gradation_rect(2, 2, width - 4, height - 4, ICY::Colors::Pink, ICY::Colors::White)
    x += 24
    y -= @base.height #+ 16
    width += 2
    height += 2
    self.contents.fill_rect(x - 1, y - 1, width, height, Color.new(0,0,0,255))
    #make_rounded back
    self.contents.blt(x, y, @base, @base.rect)
    rect = Rect.new(0, 0, bar.width * var / maxvaluez, bar.height)
    self.contents.blt(x, y, bar, rect)
    self.contents.set_pixel(x + 1, y + 1, Color.new(0,0,0,0))
    self.contents.set_pixel(x , y + height, Color.new(0,0,0,0))
    self.contents.set_pixel(x + width, y + height, Color.new(0,0,0,0))
    self.contents.set_pixel(x + width, 0, Color.new(0,0,0,0))
  end
  
  def draw_love(x, y, vari)
    val = $game_variables[vari]
    imagename = ICY::Interaction_Menu.get_heart_image(val)
    heart = Cache.picture(imagename)
    y -= heart.height
    x += 112
    self.contents.blt(x, y, heart, heart.rect)
  end
  
  def draw_main_actor
    actor = $game_actors[1]
    x = 64
    y = 64
    self.draw_actor_graphic(actor, x, y)
    self.contents.draw_text(x + 32, y - 32, 128, WLH, actor.name)
  end
  
  def draw_partner
    if @partner != nil
      x = 242
      y = 64
      a_partner = $game_actors[@partner]
     self.draw_actor_graphic(a_partner, x, y)
     self.contents.draw_text(x + 32, y - 32, 128, WLH, a_partner.name)
     if ICY::Interaction_Menu::Trustees.include?(@partner)
     vari = ICY::Interaction_Menu::TrusteesVariables[@partner]
     draw_trust_bars(x, y, vari)
     draw_love(x, y, vari)
     end
    end
  end
  
  def draw_trustees
    actors = []
    for trus in ICY::Interaction_Menu::Trustees
      if trus != nil or trus == 0
      actors.push($game_actors[trus])
      end
    end
    x = 32
    y = 128
    coun = 0
    for actor in actors
      if actor != nil
       if $game_party.members.include?(actor) and not actor.id == @partner
        self.draw_actor_graphic(actor, x, y)
        vari = ICY::Interaction_Menu::TrusteesVariables[actor.id]
        self.contents.draw_text(x + 32, y - 32, 128, WLH, actor.name)
        draw_trust_bars(x, y, vari)
        y += 48 
        coun += 1 
        if coun == 3
          x += 128
          coun = 0
          y = 128
        end
        
      end
     end
   end
 end
 
  
end
