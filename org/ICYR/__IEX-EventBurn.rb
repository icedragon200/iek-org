# #~IEX - Event Burn
#~ class Spriteset_Map
#~   #--------------------------------------------------------------------------
#~   # * Object Initialization
#~   #--------------------------------------------------------------------------
#~   alias iex_eve_burn_sm_initialize initialize unless $@
#~   def initialize(*args)
#~     iex_eve_burn_sm_initialize(*args)
#~     @burn_layers = {}
#~   end
#~   
#~   #--------------------------------------------------------------------------
#~   # * Create Parallax
#~   #--------------------------------------------------------------------------
#~   alias iex_eve_burn_sm_create_parallax create_parallax unless $@
#~   def create_parallax(*args)
#~     @burn_layers = {}
#~     @burn_layers["Below"] = Plane.new(@viewport1)
#~     @burn_layers["Below"].z = 2
#~     @burn_layers["Below"].bitmap = Bitmap.new($game_map.width * 32, $game_map.height * 32)
#~     @burn_layers["Same"] = Plane.new(@viewport1)
#~     @burn_layers["Same"].z = 3
#~     @burn_layers["Same"].bitmap = Bitmap.new($game_map.width * 32, $game_map.height * 32)
#~     @burn_layers["Same"].visible = true
#~     @burn_layers["Above"] = Plane.new(@viewport1)
#~     @burn_layers["Above"].z = 4
#~     @burn_layers["Above"].bitmap = Bitmap.new($game_map.width * 32, $game_map.height * 32)
#~     @burn_layers["Above"].visible = true
#~     iex_eve_burn_sm_create_parallax(*args)
#~   end
#~   
#~   #--------------------------------------------------------------------------
#~   # * Create Character Sprite
#~   #--------------------------------------------------------------------------
#~   def create_characters
#~     @character_sprites = []
#~     for i in $game_map.events.keys.sort
#~       sprite = Sprite_Character.new(@viewport1, $game_map.events[i])
#~       if $game_map.events[i].iex_map_burn?
#~         x_po = $game_map.events[i].x
#~         y_po = $game_map.events[i].y 
#~         sprite.update
#~         bitm = sprite.bitmap
#~         rect = sprite.src_rect
#~         case $game_map.events[i].priority_type
#~         when 0
#~           burn = "Below"
#~         when 1
#~           burn = "Same"
#~         when 2  
#~           burn = "Above"
#~         else
#~           burn = "Below"
#~         end
#~         @burn_layers[burn].bitmap.blt(x_po, y_po, bitm, rect)
#~         $game_map.events[i].erase
#~       else  
#~         @character_sprites.push(sprite)
#~       end
#~     end
#~     for vehicle in $game_map.vehicles
#~       sprite = Sprite_Character.new(@viewport1, vehicle)
#~       @character_sprites.push(sprite)
#~     end
#~     @character_sprites.push(Sprite_Character.new(@viewport1, $game_player))
#~   end
#~   
#~   #--------------------------------------------------------------------------
#~   # * Dispose of Parallax
#~   #--------------------------------------------------------------------------
#~   alias iex_eve_burn_sm_dispose_parallax dispose_parallax unless $@
#~   def dispose_parallax(*args)
#~     for lay in @burn_layers.values
#~       next if lay == nil
#~       lay.dispose
#~       lay = nil
#~     end  
#~     @burn_layers.clear
#~     @burn_layers = nil
#~     iex_eve_burn_sm_dispose_parallax(*args)
#~   end
#~   
#~   #--------------------------------------------------------------------------
#~   # * Update Parallax
#~   #--------------------------------------------------------------------------
#~   alias iex_eve_burn_sm_update_parallax update_parallax unless $@
#~   def update_parallax(*args)
#~     #HANZO Ground Overlay
#~     for lay in @burn_layers.values
#~       next if lay == nil
#~       lay.tone = $game_map.screen.tone
#~       if lay.ox != -$game_map.display_x / 256 or lay.oy != -$game_map.display_y / 256 or lay.ox == 0 or lay.oy == 0 
#~         lay.ox = $game_map.display_x / 8
#~         lay.oy = $game_map.display_y / 8
#~       end  
#~     end
#~     iex_eve_burn_sm_update_parallax(*args)
#~   end
#~   
#~ end

#~ class Game_Character
#~   
#~   def iex_map_burn?
#~     return false
#~   end
#~   
#~ end

#~ class Game_Event < Game_Character
#~   
#~   def iex_map_burn?
#~     return false if @event == nil
#~     return @event.name.scan(/<(?:MAP_BURN|map burn)>/i).size > 0
#~   end

#~ end
#~   
