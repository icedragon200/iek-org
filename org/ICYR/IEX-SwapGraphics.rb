# #~IEX - Swap Graphics
#~ module IEX
#~   module SWAP_BAT_GRAPH
#~     ACTOR_SWAP_GRAPHICS = {
#~   #Actor_Id => ["Filename", Index], # Index is 0 - 7 for nomal sheets, 0 if $
#~     1 => ["Actor1", 1],
#~     }
#~   end
#~ end

#~ class Spriteset_Battle

#~   #--------------------------------------------------------------------------
#~   # overwrite method: create_actors
#~   #--------------------------------------------------------------------------
#~   def create_actors
#~     dispose_actors if @actor_sprites != nil
#~     $scene.characters = {}
#~     @actor_sprites = []
#~     unless [1, 2, 3].include?($scene.view_type)
#~       for member in $game_party.members
#~         @actor_sprites.push(Sprite_Battler.new(@viewport1, member))
#~       end
#~       return
#~     end
#~     for actor in $game_party.members
#~       character = Game_Character.new
#~       if IEX::SWAP_BAT_GRAPH::ACTOR_SWAP_GRAPHICS.has_key?(actor.id)
#~         iex_swap_gra = IEX::SWAP_BAT_GRAPH::ACTOR_SWAP_GRAPHICS[actor.id]
#~         character.set_graphic(iex_swap_gra[0], iex_swap_gra[1])
#~       else  
#~         character.set_graphic(actor.character_name, actor.character_index)
#~       end  
#~       character.step_anime = true
#~       character_sprite = Sprite_Character.new(@viewport1, character)
#~       create_actor_coordinates(character, actor)
#~       battler_sprite = Sprite_Battler.new(@viewport1, actor)
#~       battler_sprite.character_sprite = character_sprite
#~       @actor_sprites.push(battler_sprite)
#~       @actor_sprites[@actor_sprites.size-1].create_character_dimensions
#~       $scene.characters[actor.index] = character
#~       if actor.dead?
#~         battler_sprite.start_character_death
#~         battler_sprite.character_death_duration = 1
#~         battler_sprite.update_character_death
#~       end
#~     end
#~   end
#~   
#~ end

#~ class Sprite_Battler
#~   
#~   #--------------------------------------------------------------------------
#~   # new method: update_character_sprite
#~   #--------------------------------------------------------------------------
#~   def update_character_sprite
#~     return if @character_sprite == nil
#~     if @battler.is_a?(Game_Actor) and IEX::SWAP_BAT_GRAPH::ACTOR_SWAP_GRAPHICS.has_key?(@battler.id)
#~       iex_swap_gra = IEX::SWAP_BAT_GRAPH::ACTOR_SWAP_GRAPHICS[@battler.id]
#~       if @battler.character.character_name != iex_swap_gra[0] or
#~         @battler.character.character_index != iex_swap_gra[1]
#~         char_name = iex_swap_gra[0]
#~         char_index = iex_swap_gra[1]
#~         @battler.character.set_graphic(char_name, char_index)
#~       end
#~      else
#~       if @battler.character.character_name != @battler.character_name or
#~         @battler.character.character_index != @battler.character_index
#~         char_name = @battler.character_name
#~         char_index = @battler.character_index
#~         @battler.character.set_graphic(char_name, char_index)
#~       end
#~     end
#~     @character_sprite.update
#~     @character_sprite.z = self.z
#~   end
#~   
#~ end
