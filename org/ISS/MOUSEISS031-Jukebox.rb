# MOUSE_ISS031 - Jukebox
#~ #==============================================================================#
#~ # ** ISS - Jukebox (Mouse Wrapper)
#~ #==============================================================================# 
#~ # ** Date Created  : 10/04/2011
#~ # ** Date Modified : 10/04/2011
#~ # ** Created By    : IceDragon
#~ # ** For Game      : S.A.R.A
#~ # ** ID            : 031 (Mouse)
#~ # ** Version       : 1.0
#~ #==============================================================================#
#~ ($imported ||= {})["ISS-Jukebox(MouseWrapper)"] = true
#~ #==============================================================================#
#~ # ** ISS::Jukebox::StateReponse
#~ #==============================================================================#
#~ class ISS::Jukebox::StateReponse < ISS::KeyCursor::ResponseChecker
#~   
#~   #--------------------------------------------------------------------------#
#~   # * super-method :initialize
#~   #--------------------------------------------------------------------------#  
#~   def initialize( index, rng )
#~     super( rng )
#~     @index = index
#~   end
#~   
#~   #--------------------------------------------------------------------------#
#~   # * overwrite-method :on_cursor_over
#~   #--------------------------------------------------------------------------#  
#~   def on_cursor_over( cursor )
#~     if $game_jukebox.index != @index
#~       $game_jukebox.index = @index
#~       $scene.update_juke_state()
#~     end  
#~   end
#~   
#~   #--------------------------------------------------------------------------#
#~   # * overwrite-method :on_left_click
#~   #--------------------------------------------------------------------------#  
#~   def on_left_click( cursor )
#~     $scene.execute_state( $game_jukebox.button_at( @index ) )
#~   end
#~   
#~ end 

#~ #==============================================================================#
#~ # ** ISS::Jukebox::Jukebox_CaseSprite
#~ #==============================================================================#
#~ class ISS::Jukebox::Jukebox_CaseSprite < ::Sprite
#~   
#~   #--------------------------------------------------------------------------#
#~   # * alias-method :initialize
#~   #--------------------------------------------------------------------------#   
#~   alias :iss031_mouse_jcs_initialize :initialize unless $@
#~   def initialize( *args, &block )
#~     iss031_mouse_jcs_initialize( *args, &block )
#~     setup_responses()
#~   end
#~   
#~   #--------------------------------------------------------------------------#
#~   # * alias-method :x=
#~   #--------------------------------------------------------------------------#   
#~   alias :iss031_mouse_jcs_set_x :x= unless $@
#~   def x=( new_x )
#~     iss031_mouse_jcs_set_x( new_x )
#~     @responses.each { |r| r.x = new_x }
#~   end
#~   
#~   #--------------------------------------------------------------------------#
#~   # * alias-method :y=
#~   #--------------------------------------------------------------------------#   
#~   alias :iss031_mouse_jcs_set_y :y= unless $@
#~   def y=( new_y )
#~     iss031_mouse_jcs_set_y( new_y )
#~     @responses.each { |r| r.y = new_y }
#~   end
#~   
#~   #--------------------------------------------------------------------------#
#~   # * new-method :setup_responses
#~   #--------------------------------------------------------------------------#   
#~   def setup_responses()
#~     button_v = Vector4.new( 0, 42, 0, 42 )
#~     @responses = []
#~     for i in 0...4
#~       @responses << ISS::Jukebox::StateReponse.new( i, button_v )
#~       @responses[-1].ox = 24 + (44*i)
#~       @responses[-1].oy = 324 
#~     end  
#~   end
#~   
#~   #--------------------------------------------------------------------------#
#~   # * alias-method :update
#~   #--------------------------------------------------------------------------#   
#~   alias :iss031_mouse_jcs_update :update unless $@
#~   def update( *args, &block )
#~     iss031_mouse_jcs_update( *args, &block )
#~     @responses.each { |r| r.update( $main_cursor ) } 
#~   end
#~   
#~ end

#~ #==============================================================================#
#~ # ** Scene_Jukebox 
#~ #==============================================================================#
#~ class Scene_Jukebox < Scene_Base
#~   
#~   #--------------------------------------------------------------------------#
#~   # * alias-method :update_input
#~   #--------------------------------------------------------------------------#   
#~   alias :iss031_mouse_scnjkbx_update_input :update_input unless $@
#~   def update_input( *args, &block )
#~     return command_cancel() if $main_cursor.right_click?()
#~     iss031_mouse_scnjkbx_update_input( *args, &block )
#~   end
#~   
#~ end

#~ #=*==========================================================================*=#
#~ # ** END OF FILE
#~ #=*==========================================================================*=#
