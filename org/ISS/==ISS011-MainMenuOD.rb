# ==ISS011 - MainMenu OD
#~ # // Date Modified 06/09/2011
#~ #==============================================================================#
#~ # Sprite_MenuMap
#~ #==============================================================================#
#~ class Sprite_MenuMap < ::Sprite
#~   
#~   attr_accessor :map_id
#~   
#~   def initialize( viewport, x, y, width, height, tilesize )
#~     super( viewport )
#~     @mwidth, @mheight = width, height
#~     self.x = x
#~     self.y = y
#~     @map_id = $game_map.map_id
#~     @sprites = []
#~     @last_map_id = -1
#~     @tilesize = tilesize
#~   end
#~   
#~   def dispose
#~     @sprites.each { |sp| sp.dispose() }
#~     super
#~   end
#~   
#~   def refresh()
#~     @sprites.each { |sp| sp.dispose() }
#~     @sprites.clear()
#~     self.bitmap.dispose unless self.bitmap.nil?()
#~     self.bitmap = Bitmap.new( @mwidth*@tilesize, @mheight*@tilesize )
#~     rect = Rect.new( 0, 0, @mwidth*@tilesize, @mheight*@tilesize )
#~     self.bitmap.fill_rect( rect, Color.new( 0, 0, 0 ) )
#~     map = $game_map.get_map( @map_id )
#~     xo, yo = (@mwidth-map.data.xsize)/2 , (@mheight-map.data.ysize)/2
#~     c1 = system_color
#~     sub = 40
#~     c2 = c1.clone ; c2.red -= sub ; c2.green -= sub ; c2.blue -= sub
#~     c3 = normal_color
#~     for mx in 0...map.data.xsize
#~       for my in 0...map.data.ysize
#~         rect = Rect.new( (mx+xo)*@tilesize, (my+yo)*@tilesize, @tilesize, @tilesize )
#~         if $game_map.passable?( mx, my )
#~           self.bitmap.fill_rect( rect, c1 ) 
#~         else
#~           self.bitmap.fill_rect( rect, c2 ) 
#~         end  
#~       end
#~     end  
#~     for ev in $game_map.events.values + [$game_player]
#~       sp = ::Sprite.new
#~       sp.bitmap = Bitmap.new( @tilesize, @tilesize )
#~       rect = Rect.new( 0, 0, @tilesize, @tilesize )
#~       sp.bitmap.fill_rect( rect, c3 )
#~       sp.x = self.x + ((xo+ev.x) * @tilesize)
#~       sp.y = self.y + ((yo+ev.y) * @tilesize)
#~       sp.z = self.z + 2
#~       @sprites << sp
#~     end  
#~     @last_map_id = @map_id
#~   end
#~   
#~   def update()
#~     super()
#~     @map_id = $game_map.map_id
#~     refresh if @map_id != @last_map_id
#~     @sprites.each { |sp| sp.update }
#~   end
#~   
#~ end  

#~ #==============================================================================#
#~ # Window_ActorMenuStatus
#~ #==============================================================================#
#~ class Window_ActorMenuStatus < Window_Base

#~   attr_accessor :actor
#~   
#~   def initialize(x, y, actor)
#~     super(x, y, 320, 96)
#~     @actor = actor
#~     refresh
#~   end
#~   
#~   def refresh
#~     create_contents()
#~     draw_actor_name(@actor, 4, 0)
#~     draw_actor_level(@actor, 4, 32)
#~     draw_actor_hp(@actor, 128, 0, 96)
#~     draw_actor_mp(@actor, 128+24, 32, 96)
#~   end
#~   
#~ end

#~ #==============================================================================#
#~ # Window_ReverseMenuStatus
#~ #==============================================================================#
#~ class Window_ReverseMenuStatus < Window_Selectable
#~   
#~   def initialize( x, y, width, height )
#~     super( x, y, width, height )
#~     refresh()
#~     self.index = -1
#~   end
#~   
#~   def refresh()
#~     @data       = $game_party.members #$game_party.reserve_members() # // You may have to change this
#~     @item_max   = @data.size
#~     @column_max = 2
#~     create_contents()
#~     for i in 0...@item_max ; draw_item( i ) end
#~   end
#~   
#~   #--------------------------------------------------------------------------
#~   # * Get rectangle for displaying items
#~   #     index : item number
#~   #--------------------------------------------------------------------------
#~   def item_rect(index)
#~     rect = Rect.new(0, 0, 0, 0)
#~     rect.width = (contents.width + @spacing) / @column_max - @spacing
#~     rect.height = 48 #WLH
#~     rect.x = index % @column_max * (rect.width + @spacing)
#~     rect.y = index / @column_max * 48
#~     return rect
#~   end
#~   
#~   def draw_item( index, enabled=true )
#~     rect = item_rect( index )
#~     actor = @data[index]
#~     return if actor.nil?()
#~     self.contents.font.size = 18
#~     draw_actor_name( actor, rect.x+4, rect.y )
#~     draw_actor_level( actor, rect.x+72, rect.y )
#~     draw_actor_hp( actor, rect.x+4, rect.y+24, 96 )
#~     draw_actor_mp( actor, rect.x+6, rect.y+36, 96 )
#~   end
#~   
#~   def cursor_movable? # // stops the actor input
#~     return false
#~   end  
#~   
#~ end  

#~ #==============================================================================#
#~ # MenuStatus_FF8
#~ #==============================================================================#
#~ class MenuStatus_FF8 # // Container
#~  
#~   attr_reader :x
#~   attr_reader :y
#~   attr_accessor :index
#~   attr_accessor :item_max
#~   
#~   def initialize( sx, sy )
#~     @x, @y = sx, sy 
#~     @index = 0
#~     @windows = {}
#~     @actor_windows = []
#~     @reserve_index_start = 0
#~     @item_max = 0
#~     @active = true
#~     refresh()
#~   end
#~   
#~   def active=( bool )
#~     @active = bool
#~     if bool
#~       @windows.values.each { |w| 
#~         w.opacity = 255
#~         w.contents_opacity = 255
#~       }
#~     else
#~       @windows.values.each { |w| 
#~         w.opacity = 128
#~         w.contents_opacity = 128
#~       }
#~     end  
#~   end
#~   
#~   def active() ; return @active end
#~     
#~   def dispose()
#~     dispose_windows()
#~   end
#~   
#~   def dispose_windows()   
#~     @actor_windows.clear()
#~     @windows.values.each { |w| w.dispose }
#~     @windows.clear()
#~   end
#~   
#~   def x=( new_x )
#~     return if @x == new_x
#~     @x = new_x
#~     @windows.values.each { |w| w.x = @x }
#~   end
#~   
#~   def y=( new_y )
#~     return if @y == new_y
#~     @y = new_y
#~     @windows.values.each { |w| w.x = @y }
#~   end
#~   
#~   def refresh()
#~     i = 0
#~     dispose_windows()
#~     $game_party.members.each { |m|
#~       winm = "Actor#{i}"
#~       win = Window_ActorMenuStatus.new( 20, @y+(96*i), m )
#~       @windows[winm] = win
#~       @actor_windows[i] = win
#~       i += 1
#~     }
#~     yadd = @actor_windows.size() * 96
#~     @windows["Reserve"] = Window_ReverseMenuStatus.new( @x+20, @y+yadd, 320, 192)
#~     @act_item_max = @actor_windows.size()
#~     @reserve_index_start = @actor_windows.size()
#~     @item_max = @actor_windows.size() + @windows["Reserve"].item_max
#~     reset_window_positions()
#~     update_windows()
#~   end
#~   
#~   def update()
#~     return unless active()
#~     update_user_input()
#~     update_windows()
#~     update_window_positions()
#~     if @index >= @reserve_index_start
#~       @windows["Reserve"].active = true
#~       @windows["Reserve"].index = 0 if @windows["Reserve"].index == -1
#~       @windows["Reserve"].index = @index - @reserve_index_start
#~     else  
#~       @windows["Reserve"].active = false
#~       @windows["Reserve"].index = -1
#~     end  
#~   end
#~   
#~   def update_windows()
#~     @windows.values.each { |win| win.update() }
#~   end
#~   
#~   def update_window_positions()
#~     for i in 0...@act_item_max
#~       @actor_windows[i].x = @x + 20
#~     end  
#~     if @index < @reserve_index_start
#~       @actor_windows[@index].x = @x + 40
#~     end  
#~   end
#~   
#~   def update_user_input()
#~     return if @item_max == 0
#~     if Input.trigger?( Input::UP )
#~       Sound.play_cursor
#~       @index -= 1
#~       @index %= @item_max
#~     elsif Input.trigger?( Input::DOWN )
#~       Sound.play_cursor
#~       @index += 1
#~       @index %= @item_max
#~     end
#~   end
#~   
#~   def reset_window_positions()
#~     for i in 0...@act_item_max
#~       @actor_windows[i].x = @x + 20
#~     end
#~   end
#~   
#~ end

#~ #==============================================================================#
#~ # Scene_Menu
#~ #==============================================================================#
#~ class Scene_Menu < Scene_Base

#~   #--------------------------------------------------------------------------
#~   # * overwrite method :start 
#~   #--------------------------------------------------------------------------
#~   def start
#~     super
#~     create_menu_background
#~     create_command_window
#~     @gold_window = Window_Gold.new( 0, 360 )
#~     @status_window = MenuStatus_FF8.new( 160, 0 )
#~     @status_window.active = false
#~     @menu_music = RPG::BGM.new( "Investigating", 100, 100 )
#~     @menu_music.play()
#~     tsz = 4
#~     @minimap_sprite = Sprite_MenuMap.new( nil, 0, @command_window.height, 
#~      40, 40, tsz )
#~     @minimap_sprite.update()
#~   end

#~   #--------------------------------------------------------------------------
#~   # * Termination Processing
#~   #--------------------------------------------------------------------------
#~   def terminate()
#~     super()
#~     dispose_menu_background()
#~     @command_window.dispose()
#~     @gold_window.dispose()
#~     @status_window.dispose()
#~     @minimap_sprite.dispose()
#~     $game_map.autoplay() if $scene.is_a?( Scene_Map )
#~   end
#~   
#~   #--------------------------------------------------------------------------
#~   # * Create Command Window
#~   #--------------------------------------------------------------------------
#~   def create_command_window
#~     s1 = Vocab::item
#~     s2 = Vocab::skill
#~     s3 = Vocab::equip
#~     s4 = Vocab::status
#~     s5 = Vocab::save
#~     s6 = Vocab::game_end
#~     @command_window = Window_Command.new(160, [s1, s2, s3, s4, s5, s6])
#~     @command_window.index = @menu_index
#~     if $game_party.members.size == 0          # If number of party members is 0
#~       @command_window.draw_item(0, false)     # Disable item
#~       @command_window.draw_item(1, false)     # Disable skill
#~       @command_window.draw_item(2, false)     # Disable equipment
#~       @command_window.draw_item(3, false)     # Disable status
#~     end
#~     if $game_system.save_disabled             # If save is forbidden
#~       @command_window.draw_item(4, false)     # Disable save
#~     end
#~   end
#~   #--------------------------------------------------------------------------
#~   # * Update Command Selection
#~   #--------------------------------------------------------------------------
#~   def update_command_selection
#~     if Input.trigger?(Input::B)
#~       Sound.play_cancel
#~       $scene = Scene_Map.new
#~     elsif Input.trigger?(Input::C)
#~       if $game_party.members.size == 0 and @command_window.index < 4
#~         Sound.play_buzzer
#~         return
#~       elsif $game_system.save_disabled and @command_window.index == 4
#~         Sound.play_buzzer
#~         return
#~       end
#~       Sound.play_decision
#~       case @command_window.index
#~       when 0      # Item
#~         $scene = Scene_Item.new
#~       when 1,2,3  # Skill, equipment, status
#~         start_actor_selection
#~       when 4      # Save
#~         $scene = Scene_File.new(true, false, false)
#~       when 5      # End Game
#~         $scene = Scene_End.new
#~       end
#~     end
#~   end
#~   
#~   #--------------------------------------------------------------------------
#~   # * Start Actor Selection
#~   #--------------------------------------------------------------------------
#~   def start_actor_selection
#~     @command_window.active = false
#~     @status_window.active = true
#~     if $game_party.last_actor_index < @status_window.item_max
#~       @status_window.index = $game_party.last_actor_index
#~     else
#~       @status_window.index = 0
#~     end
#~   end
#~   #--------------------------------------------------------------------------
#~   # * End Actor Selection
#~   #--------------------------------------------------------------------------
#~   def end_actor_selection
#~     @command_window.active = true
#~     @status_window.active = false
#~     @status_window.index = -1
#~   end
#~   #--------------------------------------------------------------------------
#~   # * Update Actor Selection
#~   #--------------------------------------------------------------------------
#~   def update_actor_selection
#~     if Input.trigger?(Input::B)
#~       Sound.play_cancel
#~       end_actor_selection
#~     elsif Input.trigger?(Input::C)
#~       $game_party.last_actor_index = @status_window.index
#~       Sound.play_decision
#~       case @command_window.index
#~       when 1  # skill
#~         $scene = Scene_Skill.new(@status_window.index)
#~       when 2  # equipment
#~         $scene = Scene_Equip.new(@status_window.index)
#~       when 3  # status
#~         $scene = Scene_Status.new(@status_window.index)
#~       end
#~     end
#~   end
#~   
#~ end
