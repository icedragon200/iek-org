# -IEO-008(Level Parameters)
#~ #==============================================================================#
#~ # ** CHANGES 
#~ #-*--------------------------------------------------------------------------*-# 
#~ # Classes
#~ #   Game_BattleAction
#~ #     new-method :battle_vocab
#~ #     new-method :battle_commands
#~ #   Window_ActorCommand
#~ #     new-method :draw_command
#~ #     overwrite  :initialize
#~ #     overwrite  :setup
#~ #     overwrite  :refresh
#~ #     overwrite  :draw_item
#~ #   Scene_Battle
#~ #     overwrite  :execute_action_skill
#~ #
#~ #------------------------------------------------------------------------------#
#~ #==============================================================================#
#~ # $imported - Is mostly used by Japanese RPG Maker XP/VX scripters.
#~ #             This acts as a flag, or signal to show that "x" script is present.
#~ #             This is used for compatability with other future scripts.
#~ $imported ||= {}
#~ $imported["IEO-LevelParameters"] = true
#~ #==============================================================================#
#~ # $ieo_script - This is a hash specific to IEO scripts
#~ #               they work just like the $imported, but there key is slightly 
#~ #               different, it is an array conatining an integer
#~ #               and a string, since IEO script all have an ID, the value
#~ #               is the scripts version number.
#~ #               A version number of nil, or 0 means the script isn't present
#~ # EG. $ieo_script[[ScriptID, "ScriptName"]]
#~ $ieo_script = {} if $ieo_script == nil
#~ $ieo_script[[8, "LevelParameters"]] = 1.0
#~ #-*--------------------------------------------------------------------------*-#
#~ #==============================================================================#
#~ # IEO::LEVEL_PARAMETERS
#~ #==============================================================================#
#~ module IEO
#~   module LEVEL_PARAMETERS
#~     
#~     module_function
#~     
#~     def default_levels(actor)
#~       case actor.id
#~       when 0 ; return { }
#~       else ; 
#~         return { 
#~           :maxhp => actor.level, 
#~           :maxmp => actor.level, 
#~           :atk   => actor.level, 
#~           :def   => actor.level,
#~           :spi   => actor.level,
#~           :agi   => actor.level,
#~         }
#~       end  
#~     end
#~     
#~   end
#~ end

#~ #==============================================================================#
#~ # IEO::REGEX::RIVIERA_MAPNAVIGATION
#~ #==============================================================================#
#~ module IEO
#~   module REGEXP
#~     module LEVEL_PARAMETERS
#~       module BASEITEM
#~         STAT_SET = /<(.*):[ ]*([\+\-](\d+))>/i
#~       end
#~     end
#~   end
#~ end

#~ #==============================================================================#
#~ # RPG::BaseItem
#~ #==============================================================================#
#~ class RPG::BaseItem
#~   
#~   #--------------------------------------------------------------------------#
#~   # * Public Instance Variables
#~   #--------------------------------------------------------------------------#  
#~   attr_accessor :maxhp
#~   attr_accessor :maxmp
#~   
#~   #--------------------------------------------------------------------------#
#~   # * new method :ieo008_baseitemcache
#~   #--------------------------------------------------------------------------#  
#~   def ieo008_baseitemcache
#~     @maxhp = 0
#~     @maxmp = 0
#~     self.note.split(/[\r\n]+/).each { |line|
#~     case line
#~     when IEO::REGEXP::LEVEL_PARAMETERS::BASEITEM::STAT_SET 
#~       stat = $1
#~       val  = $2.to_i
#~       case stat.upcase
#~       when "MAXHP" ; @maxhp = val
#~       when "MAXMP" ; @maxmp = val 
#~       when "ATK"   ; @atk   = val
#~       when "DEF"   ; @def   = val
#~       when "SPI"   ; @spi   = val
#~       when "AGI"   ; @agi   = val
#~       end  
#~     end } 
#~     @ieo008_baseitemcache_complete = true
#~   end
#~   
#~ end  

#~ #==============================================================================#
#~ # Game_Actor
#~ #==============================================================================#
#~ class Game_Actor < Game_Battler

#~   #--------------------------------------------------------------------------#
#~   # * alias method :setup
#~   #--------------------------------------------------------------------------#
#~   alias ieo008_setup setup unless $@
#~   def setup(actor_id)
#~     ieo008_setup(actor_id)
#~     create_parameter_levels
#~   end  
#~   
#~   #--------------------------------------------------------------------------#
#~   # * new method :create_parameter_levels
#~   #--------------------------------------------------------------------------#
#~   def create_parameter_levels
#~     @parameter_levels = IEO::LEVEL_PARAMETERS.default_levels(self)
#~   end  
#~   
#~   #--------------------------------------------------------------------------#
#~   # * overwrite method :base_maxhp
#~   #--------------------------------------------------------------------------#
#~   def base_maxhp
#~     create_parameter_levels if @parameter_levels.nil?
#~     @parameter_levels[:maxhp] = 1 if @parameter_levels[:maxhp].nil?
#~     n = actor.parameters[0, @parameter_levels[:maxhp]]
#~     for item in equips.compact do n += item.maxhp end
#~     return Integer(n)
#~   end
#~   
#~   #--------------------------------------------------------------------------#
#~   # * overwrite method :base_maxmp
#~   #--------------------------------------------------------------------------#
#~   def base_maxmp
#~     create_parameter_levels if @parameter_levels.nil?
#~     @parameter_levels[:maxmp] = 1 if @parameter_levels[:maxmp].nil?
#~     n = actor.parameters[1, @parameter_levels[:maxmp]]
#~     for item in equips.compact do n += item.maxmp end
#~     return Integer(n)
#~   end
#~   
#~   #--------------------------------------------------------------------------#
#~   # * overwrite method :base_atk
#~   #--------------------------------------------------------------------------#
#~   def base_atk
#~     create_parameter_levels if @parameter_levels.nil?
#~     @parameter_levels[:atk] = 1 if @parameter_levels[:atk].nil?
#~     n = actor.parameters[2, @parameter_levels[:atk]]
#~     for item in equips.compact do n += item.atk end
#~     return Integer(n)
#~   end
#~   
#~   #--------------------------------------------------------------------------#
#~   # * overwrite method :base_def
#~   #--------------------------------------------------------------------------#
#~   def base_def
#~     create_parameter_levels if @parameter_levels.nil?
#~     @parameter_levels[:def] = 1 if @parameter_levels[:def].nil?
#~     n = actor.parameters[3, @parameter_levels[:def]]
#~     for item in equips.compact do n += item.def end
#~     return Integer(n)
#~   end
#~   
#~   #--------------------------------------------------------------------------#
#~   # * overwrite method :base_spi
#~   #--------------------------------------------------------------------------#
#~   def base_spi 
#~     create_parameter_levels if @parameter_levels.nil?
#~     @parameter_levels[:spi] = 1 if @parameter_levels[:spi].nil?
#~     n = actor.parameters[4, @parameter_levels[:spi]]
#~     for item in equips.compact do n += item.spi end
#~     return Integer(n)
#~   end
#~   
#~   #--------------------------------------------------------------------------#
#~   # * overwrite method :base_agi
#~   #--------------------------------------------------------------------------#
#~   def base_agi
#~     create_parameter_levels if @parameter_levels.nil?
#~     @parameter_levels[:agi] = 1 if @parameter_levels[:agi].nil?
#~     n = actor.parameters[5, @parameter_levels[:agi]]
#~     for item in equips.compact do n += item.agi end
#~     return Integer(n)
#~   end
#~   
#~ end  

#~ #==============================================================================#
#~ # Scene_Title
#~ #==============================================================================#
#~ class Scene_Title < Scene_Base
#~   
#~   #--------------------------------------------------------------------------#
#~   # * alias method :load_database
#~   #--------------------------------------------------------------------------#
#~   alias ieo008_load_database load_database unless $@
#~   def load_database
#~     ieo008_load_database
#~     load_ieo008_cache
#~   end
#~   
#~   #--------------------------------------------------------------------------#
#~   # * alias method :load_bt_database
#~   #--------------------------------------------------------------------------#
#~   alias ieo008_load_bt_database load_database unless $@
#~   def load_bt_database
#~     ieo008_load_bt_database
#~     load_ieo008_cache
#~   end  
#~   
#~   #--------------------------------------------------------------------------#
#~   # * new method :load_ieo008_cache
#~   #--------------------------------------------------------------------------#  
#~   def load_ieo008_cache
#~     objs = [$data_weapons, $data_armors]
#~     objs.each { |group| group.each { |obj| 
#~       next if obj.nil? ; obj.ieo008_baseitemcache } }
#~   end
#~   
#~ end 

#~ #==============================================================================#
#~ # Scene_LevelParameter
#~ #==============================================================================#
#~ class Scene_LevelParameter < Scene_Base
#~   
#~ end

#~ #=*==========================================================================*=#
#~ # ** END OF FILE
#~ #=*==========================================================================*=#
