# IEOC-002(SKY-MessageTabs)
#~ #===========================================================================
#~ #
#~ # Sky's Message Tabs
#~ # Version 1.0
#~ # December 2nd, 2010 Started
#~ # December 3rd, 2010 Completed
#~ #
#~ #===========================================================================
#~ #
#~ # Features :
#~ #    Version 1.0 - (December 3rd, 2010)
#~ #       - Infinite amount of tabs
#~ #       - B button cancels all remaining
#~ #         messages.
#~ #       - Separate whole sections of conversation
#~ #         with NPC's.
#~ #           
#~ #===========================================================================
#~ #
#~ # Credit
#~ # Sky00Valentine :creator and editor
#~ #       Celianna :for requesting the script.
#~ #
#~ #===========================================================================
#~ #
#~ # Terms of Use
#~ # ------------
#~ #
#~ #     Crediting Rpgmakervx.net and Celianna
#~ #     is the only thing I ask. However feel free
#~ #     to credit Sky00Valentine if you see fit.
#~ #
#~ #===========================================================================
#~ #
#~ # Future improvement
#~ # ------------------
#~ #   
#~ #    - None Planned
#~ #
#~ #===========================================================================
#~ #
#~ # Instructions & Installation
#~ # ---------------------------
#~ # - Place under materials.
#~ # 
#~ # - Select an empty game variable to use to store the number of the current
#~ #   tab.
#~ #
#~ # - Select an empty game variable to use to store the current tab using
#~ #   event.
#~ #
#~ # - Place all Images in the Tab folder that are going to be used into the
#~ #   Image array. Order doesn't matter here as long as you know which
#~ #   tabs are which.
#~ #
#~ # - In the TAB_SETUP create as many setups as needed.  By this I mean
#~ #   if you have 1 setup that uses tabs 1, 2, 3, and 4 you place
#~ #   TAB_SETUP Number => [1,2,3,4] with the numbers in the order you
#~ #   want them to appear.  ex. [4,2,1,3] 
#~ #
#~ # - There is a call script call line:(only meant to be used on the map
#~ #                                    (because it uses Scene_Map)
#~ #       $scene.start_tab(TAB_SETUP Number) (depending on what tabs you want
#~ #                                      (available)
#~ #
#~ # - Enjoy.
#~ #
#~ #===========================================================================
#~ #
#~ # Compatibility & Bugs
#~ # --------------------
#~ # - Through aliasing this script should be 100% Compatible with
#~ #   any normally coded scripts.
#~ #
#~ #============================================================================
#~ $imported = {} if $imported == nil
#~ $imported["SKY-MessageTabs"] = true

#~ # Icy Engine Omega Collab
#~ $ieoc_script = {} if $ieoc_script == nil
#~ $ieoc_script[[2, "SKY-MessageTabs"]] = true
#~ #==============================================================================
#~ # ** Tab_System
#~ #------------------------------------------------------------------------------
#~ #==============================================================================
#~ # Date Modified : 02/12/2011
#~ # Last Modification By: IceDragon
#~ module Tab_System
#~ #==============================================================================
#~ #                           Start Primary Customization
#~ #------------------------------------------------------------------------------
#~ #============================================================================== 
#~   TABINDEX_VAR = 17  # Variable that stores tab.
#~   EVENTID_VAR  = 21  # Variable that stores event id
#~     
#~   # Different arrangements for tabs.  For the default 4 tabs setup 1 works
#~   # It is setup like this.  I want to use images 1,2,3, and 4. for my tabs.
#~   # It uses the images in that order from left to right.
#~   # Allows for infite amount of tabs.
#~   TAB_SETUP = {
#~   #Setup Number =>  [first tab,second tab,third tab]
#~     1 => [1,2,3,4],
#~     2 => [2,3,4],
#~   } # Do Not Remove
#~   
#~   OPEN_CLOSE_TABS = false
#~   #         x,  y, width, closedheight, openheight
#~   ADDPOS = [56, 0, 96, 56, 80]
#~   ACTIVETAB_YADD = 12
#~   module_function # DO NOT REMOVE
#~   #--------------------------------------------------------------------------#
#~   # * draw_tab
#~   #--------------------------------------------------------------------------#
#~   #--------------------------------------------------------------------------#
#~   def draw_tab(tab, type, enabled)
#~     tab.create_contents ; bit = tab.contents ; bit.clear 
#~     rect = Rect.new(0, 12, bit.width, 24)
#~     bit.font.color.alpha = 255
#~     bit.font.color.alpha = 128 unless enabled
#~     case type
#~     # Tab 1 - Talk
#~     when 1
#~       tab.draw_icon(115, rect.x, rect.y-12, enabled)
#~       bit.draw_text(rect, "Talk", 2)
#~     # Tab 2 - Quest
#~     when 2
#~       tab.draw_icon(179, rect.x, rect.y-12, enabled)
#~       bit.draw_text(rect, "Quest", 2)
#~     # Tab 3 - Gift
#~     when 3
#~       tab.draw_icon(152, rect.x, rect.y-12, enabled)
#~       bit.draw_text(rect, "Gift", 2)
#~     # Tab 4 - Shop   
#~     when 4
#~       tab.draw_icon(144, rect.x, rect.y-12, enabled)
#~       bit.draw_text(rect, "Shop", 2)
#~     # Tab 5 - Forge  
#~     when 5  
#~       tab.draw_icon(194, rect.x, rect.y-12, enabled)
#~       bit.draw_text(rect, "Forge", 2)
#~     end  
#~   end
#~ #==============================================================================
#~ #                           End Primary Customization
#~ #------------------------------------------------------------------------------
#~ #==============================================================================    
#~ end

#~ #==============================================================================
#~ #
#~ # Any edits after this are at your own risk. Without knowledge of scripting
#~ # you may run into errors, along with other risky things.
#~ #
#~ #==============================================================================
#~ $tab = nil # Just to make sure this is NIL

#~ #==============================================================================
#~ # ** Game_Map
#~ #------------------------------------------------------------------------------
#~ #==============================================================================
#~ class Game_Map
#~   attr_accessor :cancel
#~   
#~   alias cancel_text initialize unless $@
#~   def initialize
#~     cancel_text
#~     @cancel = false
#~   end
#~   
#~ end

#~ #==============================================================================
#~ # ** Scene_Map  
#~ #------------------------------------------------------------------------------
#~ #==============================================================================
#~ class Scene_Map  
#~   
#~   attr_reader :message_window
#~   
#~   #method that begins use of script
#~   def start_tab(index)
#~     if $tab.nil?
#~       $tab = Window_Tabs.new(index) 
#~     else
#~       $tab.update_tab_index if !$tab.nil?
#~     end  
#~     $tab.reset = false
#~     $game_variables[Tab_System::EVENTID_VAR] = $game_map.interpreter.get_event_id
#~   end
#~   
#~   #method that ends use of script
#~   def end_tab
#~     unless $tab.nil?
#~       unless $tab.reset
#~         $tab.dispose
#~         $tab = nil
#~       end
#~     end
#~   end
#~   
#~   alias ieo_scmp_mestab_update update unless $@
#~   def update
#~     ieo_scmp_mestab_update
#~     $tab.update_tabs unless $tab.nil?
#~   end
#~   
#~ end

#~ #==============================================================================
#~ # ** Window_Tabs
#~ #------------------------------------------------------------------------------
#~ #==============================================================================
#~ class Window_Tabs
#~   
#~   include Tab_System
#~   
#~   attr_accessor :reset
#~   
#~   def initialize(index)
#~     Graphics.freeze
#~     @tab = []
#~     @sindex = index
#~     $game_variables[TABINDEX_VAR] == 0 ? $game_variables[TABINDEX_VAR] = 1 : $game_variables[Tab_System::TABINDEX_VAR] = $game_variables[Tab_System::TABINDEX_VAR]
#~     for i in 0...(TAB_SETUP[index].size)
#~       @h = Graphics.height-$scene.message_window.height-ADDPOS[3]
#~       @tab[i] = Window_Base.new(ADDPOS[0]+(72*i), ADDPOS[1]+@h, ADDPOS[2], ADDPOS[4])
#~       @tab[i].viewport = Viewport.new(0, 0, 
#~         Graphics.width, @h+ADDPOS[3])
#~       @tab[i].viewport.z = 200
#~       @tab[i].opacity = 128
#~       draw_tab(@tab[i], TAB_SETUP[@sindex][i], i==$game_variables[TABINDEX_VAR]-1)
#~       if OPEN_CLOSE_TABS
#~         @tab[i].openness = 0
#~         @tab[i].open 
#~       end  
#~     end
#~     #update_tabs
#~     update_tab_index
#~     @reset = false
#~     @cancel = false
#~     2.times { Graphics.update ; update_tabs} if OPEN_CLOSE_TABS
#~     Graphics.transition
#~   end
#~   
#~   def update_tab_index(index=$game_variables[TABINDEX_VAR])
#~     for i in 0...@tab.size
#~       @tab[i].opacity = 128
#~       @tab[i].viewport.z = 200
#~       @tab[i].height = ADDPOS[3]
#~       @tab[i].y = @h
#~       if i == index-1
#~         @tab[i].opacity = 255 
#~         @tab[i].viewport.z = 205
#~         @tab[i].height = ADDPOS[4]
#~         @tab[i].y = @h - ACTIVETAB_YADD
#~       end  
#~     end  
#~   end
#~   
#~   def close_tabs
#~     return if @tab.nil?
#~     return unless OPEN_CLOSE_TABS
#~     for tab in @tab
#~       next if tab.nil?
#~       tab.close
#~     end
#~   end
#~   
#~   def update_tabs
#~     return if @tab.nil?
#~     for tab in @tab
#~       next if tab.nil?
#~       tab.update
#~     end  
#~   end
#~   
#~   def dispose
#~     unless @tab.nil?
#~       for i in 0...@tab.size
#~         @tab[i].dispose unless @tab[i].nil?
#~       end
#~     end
#~   end
#~   
#~   def left
#~     $game_variables[Tab_System::TABINDEX_VAR] -= 1
#~     if $game_variables[Tab_System::TABINDEX_VAR] <= 0
#~       $game_variables[Tab_System::TABINDEX_VAR] = @tab.size
#~     end
#~     @reset = true
#~   end
#~   
#~   def right
#~     $game_variables[Tab_System::TABINDEX_VAR] += 1
#~     if $game_variables[Tab_System::TABINDEX_VAR] > @tab.size
#~       $game_variables[Tab_System::TABINDEX_VAR] = 1
#~     end
#~     @reset = true
#~   end
#~     
#~ end

#~ #==============================================================================
#~ # ** Game_Interpreter  
#~ #------------------------------------------------------------------------------
#~ #==============================================================================
#~ class Game_Interpreter  
#~   
#~   def get_event_id
#~     events = $game_map.events
#~     @x = events == nil ? nil : events[@event_id].id
#~     return @x
#~   end
#~   
#~   alias ieosky_close_command_end command_end unless $@
#~   def command_end
#~     ieosky_close_command_end
#~     if $tab != nil and @original_event_id == $game_variables[Tab_System::EVENTID_VAR]
#~       $tab.dispose
#~       $tab = nil
#~     end
#~   end
#~   
#~   alias ieosky_invisible_command_101 command_101 unless $@
#~   def command_101    
#~     unless $game_message.busy
#~       if @params[2] == 2
#~         unless $tab.nil?
#~           $tab.close_tabs
#~         end
#~       end
#~     end
#~     ieosky_invisible_command_101
#~   end
#~   
#~ end

#~ #==============================================================================
#~ # ** Window_Message
#~ #------------------------------------------------------------------------------
#~ #==============================================================================
#~ class Window_Message
#~   
#~   alias ieosky_change_tabs input_pause unless $@
#~   def input_pause
#~     if $tab.nil?
#~       ieosky_change_tabs
#~     else
#~       if Input.trigger?(Input::C)
#~         self.pause = false
#~         if @text != nil and not @text.empty?
#~           new_page if @line_count >= MAX_LINE
#~         else
#~           terminate_message
#~         end
#~         unless $tab.nil? 
#~           $tab.reset = false
#~         end
#~       elsif Input.trigger?( Input::B ) 
#~         $game_map.cancel = true
#~         self.pause = false
#~         terminate_message
#~         $game_map.interpreter.command_end
#~         $game_map.interpreter.command_end
#~         $game_map.events[$game_variables[Tab_System::EVENTID_VAR]].start
#~       elsif Input.trigger?(Input::RIGHT) and (not $tab.nil?) and not self.active
#~         $tab.right
#~         self.pause = false
#~         terminate_message
#~         $game_map.interpreter.command_end
#~         $game_map.interpreter.command_end
#~         $game_map.events[$game_variables[Tab_System::EVENTID_VAR]].start
#~       elsif Input.trigger?(Input::LEFT) and (not $tab.nil?) and not self.active
#~         $tab.left
#~         self.pause = false
#~         terminate_message
#~         $game_map.interpreter.command_end
#~         $game_map.interpreter.command_end
#~         $game_map.events[$game_variables[Tab_System::EVENTID_VAR]].start
#~       end
#~     end
#~   end
#~   
#~ end
