# IEO-027(Gammus Director)
#==============================================================================#
# ** IEO(Icy Engine Omega) - Gammus Director
#-*--------------------------------------------------------------------------*-#
# ** Author        : IceDragon (http://www.rpgmakervx.net/)
# ** Script-Status : Addon (Screen, Map)
# ** Script Type   : Screen Effects
# ** Date Created  : 03/29/2011
# ** Date Modified : 05/04/2011
# ** Script Tag    : IEO-027(Gammus Director)
# ** Difficulty    : Easy, Medium
# ** Version       : 1.0
# ** IEO ID        : 027
#-*--------------------------------------------------------------------------*-#
#==============================================================================#
# ** CREDITS/USED STUFF/EDITING
#-*--------------------------------------------------------------------------*-#
# You may:
# Edit and Adapt this script as long you credit aforementioned authors.
#
# You may not:
# Claim this as your own work, or redistribute without the consent of the author.
#
#------------------------------------------------------------------------------#
#==============================================================================#
# ** INTRODUCTION
#-*--------------------------------------------------------------------------*-# 
#
# *Pops out Camera*
# Alright take 27, action!
#
#------------------------------------------------------------------------------#
#==============================================================================#
# ** INSTRUCTIONS
#-*--------------------------------------------------------------------------*-#
#
# Plug 'n' Play
#
#------------------------------------------------------------------------------#
#==============================================================================#
# ** COMPATABLITIES
#-*--------------------------------------------------------------------------*-#
#
# Everything.... hopefully
#
#------------------------------------------------------------------------------#
#==============================================================================#
# ** INSTALLATION
#-*--------------------------------------------------------------------------*-#
# To install this script, open up your script editor and copy/paste this script
# to an open slot below ▼ Materials but above ▼ Main. Remember to save.
#
#-*--------------------------------------------------------------------------*-#
# Below 
#  Materials
#  
# Above 
#   Main
#   Everything else
#
#------------------------------------------------------------------------------#
#==============================================================================#
# ** CHANGES 
#-*--------------------------------------------------------------------------*-# 
# Classes
#   Game_Map
#     new-method   :gameviewrect
#   Game_Interpreter
#     new-method   :get_event
#     new-method   :jump_to
#     new-method   :jump_to_event
#     new-method   :scroll_with_event
#     new-method   :move_viewports
#     new-method   :gfreeze
#     new-method   :transition
#     new-method   :reset_viewports
#     new-method   :reset_director
#     new-method   :wait_for_animation
#   Sprite_Base
#     alias-method :dispose
#     alias-method :animation_set_sprites
#   Sprite_Character
#     alias-method :update
#   Sprite_Timer
#     overwrite    :initialize
#   Spriteset_Map
#     overwrite    :create_viewports
#   Spriteset_Battle
#     overwrite    :create_viewports
#   Window_Help
#     overwrite    :initialize
#
#------------------------------------------------------------------------------#
#==============================================================================#
# ** CHANGE LOG
#-*--------------------------------------------------------------------------*-#
# (DD/MM/YYYY)
#  03/29/2011 - V1.0 Started and Finished Script
#  05/04/2011 - V1.1 Added Camera Class ***
#
#------------------------------------------------------------------------------#
#==============================================================================#
# ** KNOWN ISSUES
#------------------------------------------------------------------------------#  
#
#  Non
#
#------------------------------------------------------------------------------#
#==============================================================================#
# ** FAQ
#-*--------------------------------------------------------------------------*-#  
# Acronyms -
#  BEM - Battle Engine Melody
#  CBS - Custom Battle System
#  DBS - Default Battle System
#  GTBS- Gubid's Tactical Battle System
#  IEO - Icy Engine Omega
#  IEX - Icy Engine Xellion
#  OHM - IEO-005(Ohmerion)
#  SRN - IEX - Siren
#  YGG - IEX - Yggdrasil
#  ATB - Active Turn Battle
#  DTB - Default Turn Battle
#  PTB - Press Turn Battle
#  CTB - Charge Turn Battle
#
# Q. Whats up with the IDs?
# A. Well due to some naming issues, I ended up with 5 scripts in IEX
#    all having similar names, this causes some issues for updating
#    and sorting.
#    I have decided to add some IDS so I can sort and find script with EASE.
#
# Q. Where is the name from?
# A. Roman Alphabet, thanks to PentagonBuddy and Jalen by the way.
#
# Q. Where did you learn scripting?
# A. Yanfly's scripts, read almost everyone of them, so my scripting style
#    kinda looks like his.
#
#-*--------------------------------------------------------------------------*-#
#==============================================================================#
# $imported - Is mostly used by Japanese RPG Maker XP/VX scripters.
#             This acts as a flag, or signal to show that "x" script is present.
#             This is used for compatability with other future scripts.
($imported ||= {})["IEO-GammusDirector"] = true
#==============================================================================#
# $ieo_script - This is a hash specific to IEO scripts
#               they work just like the $imported, but there key is slightly 
#               different, it is an array conatining an integer
#               and a string, since IEO script all have an ID, the value
#               is the scripts version number.
#               A version number of nil, or 0 means the script isn't present
# EG. $ieo_script[[ScriptID, "ScriptName"]]
($ieo_script = {})[[27, "GammusDirector"]] = 1.0
#-*--------------------------------------------------------------------------*-#
#==============================================================================#
#==============================================================================#
# ** IEO
#==============================================================================#
module IEO
  
  class GammusCamera
    
    CENTER_X = Game_Player::CENTER_X
    CENTER_Y = Game_Player::CENTER_Y
    
    attr_accessor :x
    attr_accessor :y
    attr_accessor :target_x
    attr_accessor :target_y
    attr_accessor :speed_x
    attr_accessor :speed_y
    
    def initialize
      @x = @y               = 0.0
      @target_x = @target_y = 0.0
      @speed_x  = @speed_y  = 0.0
      @last_x = @last_y     = 0.0
      @last_tx = @last_ty   = 0
      @idle_time = 0
    end
    
    def update
      movex = movey = 0
      if @speed_x > 0
        movex = @speed_x #16 #Graphics.width / @speed_x
      end
      if @speed_y > 0
        movey = @speed_y #16 #Graphics.height / @speed_y
      end  
      if @x > @target_x && @speed_x != 0
        @x = [@x - movex.abs, @target_x].max
      elsif @x < @target_x && @speed_x != 0  
        @x = [@x + movex.abs, @target_x].min
      end 
      if @y > @target_y && @speed_y != 0
        @y = [@y - movey.abs, @target_y].max
      elsif @y < @target_y && @speed_y != 0  
        @y = [@y + movey.abs, @target_y].min
      end
      @speed_x  = 0.0 if @x == @target_x
      @speed_y  = 0.0 if @y == @target_y
      @last_x = @x ; @last_y = @y
    end
    
    def set_target_to(*args)
      case args[0]
      when Game_Character ; x, y = args[0].x, args[0].y
      else                ; x, y = args[0], args[1] end
      x = 0 if x.nil? ; y = 0 if y.nil?
      display_x = x * 256 - CENTER_X                    # Calculate coordinates
      unless $game_map.loop_horizontal?                 # No loop horizontally?
        max_x = ($game_map.width - Integer(Graphics.width / 32)) * 256            # Calculate max value
        display_x = [0, [display_x, max_x].min].max     # Adjust coordinates
      end
      display_y = y * 256 - CENTER_Y                    # Calculate coordinates
      unless $game_map.loop_vertical?                   # No loop vertically?
        max_y = ($game_map.height - Integer(Graphics.height / 32)) * 256           # Calculate max value
        display_y = [0, [display_y, max_y].min].max     # Adjust coordinates
      end
      @target_x, @target_y = display_x, display_y
      @speed_x = @speed_y = 16.0
      @last_tx = x ; @last_tx = y
    end
    
  end
  
end  

#==============================================================================#
# ** Game_System
#==============================================================================#
class Game_System
  
  unless $imported["IEO-BugFixesUpgrades"]
    
    #--------------------------------------------------------------------------#
    # * Constants
    #--------------------------------------------------------------------------#
    GAMEVIEWX    = 0
    GAMEVIEWY    = 0
    GAMEWIDTH    = Graphics.width
    GAMEHEIGHT   = Graphics.height
  
    #--------------------------------------------------------------------------#
    # * new method :gameviewrect
    #--------------------------------------------------------------------------#
    def gameviewrect
      return Rect.new(GAMEVIEWX, GAMEVIEWY, GAMEWIDTH, GAMEHEIGHT)
    end
    
  end
  
end
=begin
#==============================================================================#
# Game_Screen
#==============================================================================#
class Game_Screen
  
  attr_accessor :camera
  attr_accessor :camera_event
  
  alias ieo027_gsc_initialize initialize unless $@
  def initialize
    ieo027_gsc_initialize()
    @camera = ::IEO::GammusCamera.new()
    @camera_event = $game_player
  end
  
  alias ieo027_gsc_update update unless $@
  def update
    ieo027_gsc_update()
    @camera.set_target_to(@camera_event)
    @camera.update()
    $game_map.set_display_pos(@camera.x, @camera.y)
  end
  
end

#==============================================================================#
# Game_Map
#==============================================================================#
class Game_Map
  
  alias ieo027_gmp_setup setup unless $@
  def setup( map_id )
    ieo027_gmp_setup( map_id )
    @screen.camera_event = $game_player
    @screen.camera.set_target_to($game_player)
    @screen.camera.x = @screen.camera.target_x
    @screen.camera.y = @screen.camera.target_y
  end
  
end

#==============================================================================#
# Game_Player
#==============================================================================#
class Game_Player
  
  #--------------------------------------------------------------------------
  # * Update Scroll
  #--------------------------------------------------------------------------
  def update_scroll(last_real_x, last_real_y)
    #ax1 = $game_map.adjust_x(last_real_x)
    #ay1 = $game_map.adjust_y(last_real_y)
    #ax2 = $game_map.adjust_x(@real_x)
    #ay2 = $game_map.adjust_y(@real_y)
    #if ay2 > ay1 and ay2 > CENTER_Y
    #  $game_map.scroll_down(ay2 - ay1)
    #end
    #if ax2 < ax1 and ax2 < CENTER_X
    #  $game_map.scroll_left(ax1 - ax2)
    #end
    #if ax2 > ax1 and ax2 > CENTER_X
    #  $game_map.scroll_right(ax2 - ax1)
    #end
    #if ay2 < ay1 and ay2 < CENTER_Y
    #  $game_map.scroll_up(ay1 - ay2)
    #end
  end
  
end
=end
#==============================================================================#
# ** Game_Interpreter
#==============================================================================#
class Game_Interpreter
  
  #--------------------------------------------------------------------------#
  # * new method :get_event
  #--------------------------------------------------------------------------#
  def get_event( event_id )
    ev = nil
    case event_id
    when -1 ; ev = $game_map.events[ @event_id ]
    when 0  ; ev = $game_player
    else    ; ev = $game_map.events[ event_id ]
    end
    ev
  end  
  
  #--------------------------------------------------------------------------#
  # * new method :jump_to
  #--------------------------------------------------------------------------#
  def jump_to( x, y, fadetype=0, fadetime=60 )
    # // Prepare
    Graphics.fadeout( fadetime )    if fadetype == 1
    Graphics.freeze               if fadetype == 2
    # // Center display
    $game_player.center( x, y )
    $game_map.need_refresh = true
    $game_map.update
    if $scene.is_a?( Scene_Map )
      $scene.spriteset.force_update_characters ; $scene.spriteset.update
    end
    # // End
    Graphics.fadein( fadetime )     if fadetype == 1
    Graphics.transition( fadetime ) if fadetype == 2
  end
  
  #--------------------------------------------------------------------------#
  # * new method :jump_to_event
  #--------------------------------------------------------------------------#
  def jump_to_event( event_id, fadetype=0, fadetime=60 )
    ev = get_event( event_id )
    return if ev.nil?
    x, y = ev.x, ev.y
    jump_to( x, y, fadetype, fadetime )
  end
  
  #--------------------------------------------------------------------------#
  # * new method :scroll_with_event
  #--------------------------------------------------------------------------#  
  def scroll_with_event( event_id )
    ev = get_event( event_id )
    return if ev.nil?
    jump_to_event( event_id )
    $scene.scroll_event = ev
    $scene.scroll_event = nil if $scene.scroll_event == $game_player
  end  
  
  #--------------------------------------------------------------------------#
  # * new method :move_viewports
  #--------------------------------------------------------------------------#  
  def move_viewports( movetype, amount=0, time=60, rate=240 )
    case movetype
    when :x
      $scene.spriteset.viewtarget_x     = amount
      $scene.spriteset.viewtarget_xtime = time
      $scene.spriteset.viewrate_x       = rate
    when :y
      $scene.spriteset.viewtarget_y     = amount
      $scene.spriteset.viewtarget_ytime = time
      $scene.spriteset.viewrate_y       = rate
    when :width
      $scene.spriteset.viewtarget_width = amount
      $scene.spriteset.viewtarget_widthtime = time
      $scene.spriteset.viewrate_width       = rate
    when :height
      $scene.spriteset.viewtarget_height = amount
      $scene.spriteset.viewtarget_heighttime = time
      $scene.spriteset.viewrate_height       = rate
    # Reset
    when :reset
      df = $scene.spriteset.defaultview
      move_viewports(:x,     df[0], time, rate)
      move_viewports(:y,     df[1], time, rate)
      move_viewports(:width, df[2], time, rate)
      move_viewports(:height,df[3], time, rate)
    # // Disabled (Offset Changing is currently Disabled) 
    when :ox  
      $scene.spriteset.viewtarget_ox     = amount
      $scene.spriteset.viewtarget_oxtime = time
      $scene.spriteset.viewrate_ox       = rate
    when :oy  
      $scene.spriteset.viewtarget_oy     = amount
      $scene.spriteset.viewtarget_oytime = time 
      $scene.spriteset.viewrate_oy       = rate
    end  
  end
  
  #--------------------------------------------------------------------------#
  # * new method :gfreeze
  #--------------------------------------------------------------------------# 
  def gfreeze ; Graphics.freeze end
    
  #--------------------------------------------------------------------------#
  # * new method :transition
  #--------------------------------------------------------------------------# 
  def transition(name="", time=60, ambiguity = 40)
    Graphics.transition(time, name, ambiguity)
  end  
  
  #--------------------------------------------------------------------------#
  # * new method :reset_viewports
  #--------------------------------------------------------------------------#   
  def reset_viewports ; $scene.spriteset.reset_viewport_variables end  
  #--------------------------------------------------------------------------#
  # * new method :reset_director
  #--------------------------------------------------------------------------#   
  def reset_director ; $scene.spriteset.reset_director end
    
  #--------------------------------------------------------------------------#
  # * new method :wait_for_animation
  #--------------------------------------------------------------------------# 
  def wait_for_animation( aid )
    frames = $data_animations[ aid ].frame_max
    if $imported["CoreFixesUpgradesMelody"]
      @wait_count = frames * Sprite_Base::RATE 
    else  
      @wait_count = frames * 4
    end  
  end
  
end

#==============================================================================#
# ** Spriteset_Map
#==============================================================================#
class Spriteset_Map
  
  #--------------------------------------------------------------------------#
  # * Public Instance Variables
  #--------------------------------------------------------------------------#      
  # // Target Values
  attr_accessor :viewtarget_x       
  attr_accessor :viewtarget_y       
  attr_accessor :viewtarget_ox      
  attr_accessor :viewtarget_oy      
  attr_accessor :viewtarget_width   
  attr_accessor :viewtarget_height  
  # // Target Times
  attr_accessor :viewtarget_xtime   
  attr_accessor :viewtarget_ytime   
  attr_accessor :viewtarget_oxtime  
  attr_accessor :viewtarget_oytime  
  attr_accessor :viewtarget_widthtime    
  attr_accessor :viewtarget_heighttime 
  # // Rates
  attr_accessor :viewrate_x       
  attr_accessor :viewrate_y       
  attr_accessor :viewrate_ox      
  attr_accessor :viewrate_oy      
  attr_accessor :viewrate_width   
  attr_accessor :viewrate_height 
  # // Defaultview
  attr_accessor :defaultview
  
  #--------------------------------------------------------------------------#
  # * alias method :initialize
  #--------------------------------------------------------------------------#
  alias :ieo027_spm_initialize :initialize unless $@
  def initialize()
    # // Arrays
    @viewport_variables = ['@viewport1', '@viewport2', '@viewport3']
    @target_variables   = ['x', 'y', 'width', 'height']#, 'ox', 'oy']
    @view_director      = []
    @defaultview = [$game_system.gameviewrect.x, $game_system.gameviewrect.y, 
      $game_system.gameviewrect.width, $game_system.gameviewrect.height, 0, 0]
    reset_director
    # // Init
    ieo027_spm_initialize()
  end
    
  #--------------------------------------------------------------------------#
  # * new method :reset_director
  #--------------------------------------------------------------------------#
  def reset_director()
    # // Target Values
    @viewtarget_x       = nil
    @viewtarget_y       = nil
    @viewtarget_width   = nil
    @viewtarget_height  = nil
    @viewtarget_ox      = nil
    @viewtarget_oy      = nil
    # // Target Times
    @viewtarget_xtime   = nil
    @viewtarget_ytime   = nil
    @viewtarget_widthtime    = nil
    @viewtarget_heighttime   = nil
    @viewtarget_oxtime  = nil
    @viewtarget_oytime  = nil
    # // Rates
    @viewrate_x         = nil
    @viewrate_y         = nil
    @viewrate_width     = nil
    @viewrate_height    = nil
    @viewrate_ox        = nil
    @viewrate_oy        = nil
    # // Current_Variables
    reset_viewport_variables
  end  
  
  #--------------------------------------------------------------------------#
  # * new method :reset_viewport_variables
  #--------------------------------------------------------------------------#
  def reset_viewport_variables()
    # // Reset Current_Variables
    @viewcurrent_x      = @defaultview[0]
    @viewcurrent_y      = @defaultview[1]
    @viewcurrent_width  = @defaultview[2]
    @viewcurrent_height = @defaultview[3]
    @viewcurrent_ox     = @defaultview[4]
    @viewcurrent_oy     = @defaultview[5]
  end
  
  #--------------------------------------------------------------------------#
  # * new method :force_update_characters
  #--------------------------------------------------------------------------#
  def force_update_characters()
    @character_sprites.compact.each { |sp| sp.update }
  end
  
  #--------------------------------------------------------------------------#
  # * alias method :update
  #--------------------------------------------------------------------------#
  alias :ieo027_spm_update :update unless $@
  def update()
    ieo027_spm_update()
    update_viewport_move()
  end  
  
  #--------------------------------------------------------------------------#
  # * new method :update_viewport_move
  #--------------------------------------------------------------------------#
  def update_viewport_move()
    # // Update Movement
    @target_variables.each { |n|
      next if eval("@viewtarget_#{n} == nil")
      str = %Q(
        move = @viewrate_#{n} / @viewtarget_#{n}time.to_f
        if @viewtarget_#{n} > @viewcurrent_#{n}
          @viewcurrent_#{n} = [@viewcurrent_#{n}+move, @viewtarget_#{n}].min
        elsif @viewtarget_#{n} < @viewcurrent_#{n}  
          @viewcurrent_#{n} = [@viewcurrent_#{n}-move, @viewtarget_#{n}].max
        end 
        # // Reset Variables
        if @viewcurrent_#{n} == @viewtarget_#{n} 
          @viewtarget_#{n}      = nil
          @viewtarget_#{n}time  = nil
        end  )
      eval(str) }
    # // Update Viewports
    @viewport_variables.each { |v|  
      vstr = %Q(
        #{v}.rect.set(@viewcurrent_x, @viewcurrent_y, 
          @viewcurrent_width, @viewcurrent_height) unless #{v}.nil?() )
      eval(vstr) }
  end  
  
end

#==============================================================================#
# ** Scene_Map
#==============================================================================#
class Scene_Map < Scene_Base
  
  #--------------------------------------------------------------------------#
  # * Public Instance Variables
  #--------------------------------------------------------------------------#  
  attr_accessor :spriteset
  attr_accessor :scroll_event
  
  #--------------------------------------------------------------------------#
  # * alias method :update
  #--------------------------------------------------------------------------#
  alias :ieo027_scmp_update :update unless $@
  def update()
    unless @scroll_event.nil?()
      last_real_x = @scroll_event.real_x
      last_real_y = @scroll_event.real_y 
    end
    ieo027_scmp_update()
    unless @scroll_event.nil? 
      update_event_scroll(last_real_x, last_real_y)
    end
  end
  
  #--------------------------------------------------------------------------#
  # * new method :update_event_scroll 
  #--------------------------------------------------------------------------#
  def update_event_scroll( last_real_x, last_real_y )
    return if @scroll_event.nil?()
    return if last_real_x.nil?()
    return if last_real_y.nil?()
    ax1 = $game_map.adjust_x( last_real_x )
    ay1 = $game_map.adjust_y( last_real_y )
    ax2 = $game_map.adjust_x( @scroll_event.real_x )
    ay2 = $game_map.adjust_y( @scroll_event.real_y )
    if ay2 > ay1 and ay2 > Game_Player::CENTER_X
      $game_map.scroll_down( ay2 - ay1 )
    end
    if ax2 < ax1 and ax2 < Game_Player::CENTER_X
      $game_map.scroll_left( ax1 - ax2 )
    end
    if ax2 > ax1 and ax2 > Game_Player::CENTER_Y
      $game_map.scroll_right( ax2 - ax1 )
    end
    if ay2 < ay1 and ay2 < Game_Player::CENTER_Y
      $game_map.scroll_up( ay1 - ay2 )
    end
  end
  
end
#==============================================================================#
IEO::REGISTER.log_script( 27, "GammusDirector", 1.0 ) if $imported["IEO-Register"]
#=*==========================================================================*=#
# ** END OF FILE
#=*==========================================================================*=#
