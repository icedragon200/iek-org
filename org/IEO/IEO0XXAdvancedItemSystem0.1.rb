# IEO0XX(AdvancedItemSystem) 0.1
#==============================================================================#
# ** IEO::ADVANCED_ITEM
#==============================================================================#
module IEO
  module ADVANCED_ITEM
  end
end

#==============================================================================#
# ** IEO::ADVANCED_ITEM::MIXES
#==============================================================================#
module IEO::ADVANCED_ITEM::MIXES
  module Item
    
    def ieo0xx_itemcache()
    end
  
    def ieo0xx_itemparseline( line )
      case line
      when /LINE/i
      end  
    end
    
  end  
end  

#==============================================================================#
# ** RPG::Item
#==============================================================================#
class RPG::Item
  
  include IEO::ADVANCED_ITEM::MIXES::Item
  
end

#==============================================================================#
# ** Game_Battler
#==============================================================================#
class Game_Battler
  
  def items
    result = []
    for i in @items.keys.sort
      result.push($data_items[i]) if @items[i] > 0
    end
    for i in @weapons.keys.sort
      result.push($data_weapons[i]) if @weapons[i] > 0
    end
    for i in @armors.keys.sort
      result.push($data_armors[i]) if @armors[i] > 0
    end
    return result
  end
  
  def item_number(item)
    case item
    when RPG::Item
      number = @items[item.id]
    when RPG::Weapon
      number = @weapons[item.id]
    when RPG::Armor
      number = @armors[item.id]
    end
    return number == nil ? 0 : number
  end

  def has_item?(item, include_equip = false)
    if item_number(item) > 0
      return true
    end
    if include_equip
      for actor in members
        return true if actor.equips.include?(item)
      end
    end
    return false
  end

  def gain_item( item, n, include_equip = false )
    number = item_number(item)
    case item
    when RPG::Item
      @items[item.id] = [[number + n, 0].max, 99].min
    when RPG::Weapon
      @weapons[item.id] = [[number + n, 0].max, 99].min
    when RPG::Armor
      @armors[item.id] = [[number + n, 0].max, 99].min
    end
    n += number
    if include_equip and n < 0
      for actor in members
        while n < 0 and actor.equips.include?(item)
          actor.discard_equip(item)
          n += 1
        end
      end
    end
  end
  
  def lose_item( item, n, include_equip = false )
    gain_item( item, -n, include_equip )
  end
  
  def consume_item( item )
    if item.is_a?(RPG::Item) and item.consumable
      lose_item( item, 1 )
    end
  end
  
  def item_can_use?( item )
    return false unless item.is_a?(RPG::Item)
    return false if item_number(item) == 0
    if $game_temp.in_battle
      return item.battle_ok?
    else
      return item.menu_ok?
    end
  end
  
end

#==============================================================================#
# ** Window_Item
#==============================================================================#
class Window_Item < Window_Selectable
  
  attr_accessor :actor
  
  def initialize( x, y, width, height, actor=$game_party )
    super( x, y, width, height )
    @actor = actor
    @column_max = 1
    self.index = 0
    refresh
  end
  
  def enable?(item)
    return @actor.item_can_use?(item)
  end
  
  def data_refresh()
    @data = []
    for item in @actor.items
      next unless include?(item)
      @data.push(item)
      if item.is_a?(RPG::Item) and item.id == @actor.last_item_id
        self.index = @data.size - 1
      end
    end
    @data.push( nil ) if include?( nil )
    @item_max = @data.size
  end
  
  def refresh()
    data_refresh()
    create_contents()
    for i in 0...@item_max ; draw_item(i) ; end
  end

  def draw_item( index )
    rect = item_rect( index )
    self.contents.clear_rect( rect )
    item = @data[index]
    unless item.nil?()
      number = @actor.item_number( item )
      enabled = enable?( item )
      rect.width -= 4
      self.contents.font.size = Font.default_size - 6
      rect.height -= 6
      draw_icon( item.icon_index, rect.x, rect.y )
      self.contents.draw_text( rect.x+24, rect.y, rect.width, rect.height, item.name )
      self.contents.draw_text( rect, sprintf(":%2d", number), 2 )
    end
  end
  
end

#==============================================================================#
# ** Window_ItemStatus
#==============================================================================#
class Window_ItemStatus < Window_Selectable
  
  def initialize( x, y )
    super( x, y, Graphics.width/2, Graphics.height-56 )
    refresh()
  end
  
  def item_rect( index )
    rect = Rect.new( 0, 0, 0, 0 )
    rect.width = (contents.width + @spacing) / @column_max - @spacing
    rect.height = 96
    rect.x = index % @column_max * (rect.width + @spacing)
    rect.y = (index / @column_max * (rect.height))
    return rect
  end
  
  def refresh()
    @data = $game_party.members
    @item_max = @data.size
    for i in 0...@item_max ; draw_item( i ) ; end
  end
  
  def draw_item( index )
    rect = item_rect( index )
    act = @data[index] 
    draw_actor_face( act, rect.x, rect.y )
    draw_actor_name( act, rect.x + 64, rect.y + 4 )
    draw_actor_state( act, rect.x + 64+12, rect.y + 32, 112 )
  end
  
end

#==============================================================================#
# ** Scene_Title
#==============================================================================#
class Scene_Title < Scene_Base
  
  #--------------------------------------------------------------------------#
  # * alias method :load_database
  #--------------------------------------------------------------------------#
  alias :ieo0xx_sct_load_database :load_database unless $@
  def load_database()
    ieo0xx_sct_load_database()
    load_ieo0xx_cache()
  end
  
  #--------------------------------------------------------------------------#
  # * alias method :load_bt_database
  #--------------------------------------------------------------------------#
  alias :ieo0xx_sct_load_bt_database :load_database unless $@
  def load_bt_database()
    ieo0xx_sct_load_bt_database()
    load_ieo0xx_cache()
  end  
  
  #--------------------------------------------------------------------------#
  # * new method :load_ieo0xx_cache
  #--------------------------------------------------------------------------#
  def load_ieo0xx_cache()
    objs = [$data_items]
    objs.each { |group| group.each { |obj| next if obj.nil?() 
      obj.ieo0xx_itemcache()  if obj.is_a?(RPG::Item) 
    } }
  end
  
end

#==============================================================================#
# ** Scene_Item
#==============================================================================#
class Scene_Item < Scene_Base
  
  def initialize( actor=$game_party, called=:menu, return_index=0 )
    super()
    # ---------------------------------------------------- #
    @actor = nil
    @act_index = 0
    @index_call = false
    # ---------------------------------------------------- #
    if actor.kind_of?( Game_Battler )
      @actor = actor
    elsif actor.kind_of?( Game_Unit )  
      @actor = actor
    elsif actor != nil  
      @actor = $game_party.members[actor]
      @act_index = actor
      @index_call = true
    end 
    # ---------------------------------------------------- #
    @calledfrom = called
    @return_index = return_index
    # ---------------------------------------------------- #
  end
  
  def start()
    super()
    create_menu_background()
    @viewport = Viewport.new( 0, 0, Graphics.width, Graphics.height )
    @help_window = Window_Help.new()
    @help_window.viewport = @viewport
    @item_window = Window_Item.new( 0, 56, Graphics.width/2, 360, @actor )
    @item_window.viewport = @viewport
    @item_window.help_window = @help_window
    @item_window.active = false
    @target_window = Window_MenuStatus.new( 0, 0 )
    hide_target_window()
  end
  
  def terminate()
    super()
    dispose_menu_background
    @viewport.dispose
    @help_window.dispose
    @item_window.dispose
    @target_window.dispose
  end
  
  def return_scene()
    case @calledfrom
    when :map
      $scene = Scene_Map.new()
    when :menu
      $scene = Scene_Menu.new( @return_index )
    end
  end
  
  def update()
    super()
    update_menu_background()
    @help_window.update()
    @item_window.update()
    @target_window.update()
    if @item_window.active
      update_item_selection()
    elsif @target_window.active
      update_target_selection()
    end
  end
  
  def update_item_selection()
    if Input.trigger?(Input::B)
      Sound.play_cancel
      return_scene()
    elsif Input.trigger?(Input::C)
      @item = @item_window.item
      if @item != nil
        @actor.last_item_id = @item.id
      end
      if @actor.item_can_use?( @item )
        Sound.play_decision()
        determine_item()
      else
        Sound.play_buzzer()
      end
    end
  end
  
  def determine_item
    if @item.for_friend?
      show_target_window(@item_window.index % 2 == 0)
      if @item.for_all?
        @target_window.index = 99
      else
        if @actor.last_target_index < @target_window.item_max
          @target_window.index = @actor.last_target_index
        else
          @target_window.index = 0
        end
      end
    else
      use_item_nontarget
    end
  end
  
  def update_target_selection
    if Input.trigger?(Input::B)
      Sound.play_cancel
      if @actor.item_number(@item) == 0    # If item is used up
        @item_window.refresh                    # Recreate the window contents
      end
      hide_target_window
    elsif Input.trigger?(Input::C)
      if not @actor.item_can_use?(@item)
        Sound.play_buzzer
      else
        determine_target
      end
    end
  end
  
  def determine_target
    used = false
    if @item.for_all?
      for target in $game_party.members
        target.item_effect(target, @item)
        used = true unless target.skipped
      end
    else
      $game_party.last_target_index = @target_window.index
      target = $game_party.members[@target_window.index]
      target.item_effect(target, @item)
      used = true unless target.skipped
    end
    if used
      use_item_nontarget
    else
      Sound.play_buzzer
    end
  end
  
  def show_target_window( right )
    @item_window.active = false
    width_remain = Graphics.width - @target_window.width
    @target_window.x = right ? width_remain : 0
    @target_window.visible = true
    @target_window.active = true
    if right
      @viewport.rect.set(0, 0, width_remain, Graphics.height)
      @viewport.ox = 0
    else
      @viewport.rect.set(@target_window.width, 0, width_remain, Graphics.height)
      @viewport.ox = @target_window.width
    end
  end
  
  def hide_target_window
    @item_window.active = true
    @target_window.visible = false
    @target_window.active = false
    @viewport.rect.set( 0, 0, Graphics.width, Graphics.height )
    @viewport.ox = 0
  end
  
  def use_item_nontarget
    Sound.play_use_item
    @actor.consume_item( @item )
    @item_window.draw_item( @item_window.index )
    @target_window.refresh
    if $game_party.all_dead?
      $scene = Scene_Gameover.new
    elsif @item.common_event_id > 0
      $game_temp.common_event_id = @item.common_event_id
      $scene = Scene_Map.new
    end
  end
  
end
#==============================================================================#
IEO::REGISTER.log_script( 0x00, "AdvancedItemSystem", 0.1 ) if $imported["IEO-Register"]
#=*==========================================================================*=#
# ** END OF FILE
#=*==========================================================================*=#
