begin
  require 'FileUtils'
  txts = Dir.glob("*.txt") 
  txts.each { |f|
    FileUtils.copy_file(f,File.basename(f,File.extname(f))+".rb")
  }  
rescue Exception => ex
  p ex
  sleep 4.0
end