#-// REI - Rogue Engine Icy
($imported||={})['REI'] = 0x08000
module REI
end
ygg = '/home/icy/Dropbox/code/RM/Yggdrasil2x6/'
require ygg + 'src/core/prototype.rb'
require ygg + 'src/core/cache.rb'
require ygg + 'src/constants/Constants.rb'
require ygg + 'src/mixins/_mixins.rb'
require ygg + 'src/containers/container_unit.rb'
require ygg + 'src/handlers/Handler_Pos.rb'
require ygg + 'src/handlers/Handler_Gauges.rb'
require ygg + 'src/spritesets/spriteset_base.rb'
require ygg + 'src/spritesets/spriteset_gauge.rb'
require_relative 'REI_BattlerUnit[final]'
require_relative 'game-characters'

module YGG::Cache
  GAUGES[14] = [56, 4, Color.new(  0,  0,  0),Color.new( 96, 96, 96),Color.new(198,233,203)]
end

module REI

  def self.parse_char_name name
    syms = name[0,1].split('')
    return { single: syms.include?(?$), object: syms.include?(?!) }
  end

  module Constants
    GAUGE_WT = 14
  end
  module Mixin
  end
  module Handler
    class Gauge_WT < YGG::Handler::Gauge_Param
      def gauge_bmp
      end
      define_as gauge_id: REI::Constants::GAUGE_WT
      def calc_xyz host_cube,gauge_cube
        x,y,z = super
        y -= host_cube.height + gauge_cube.height
        return x,y,z
      end
      def _value_abs
        battler.wt_rate
      end
      #def update
      #  super
      #  @opacity = 255
      #end
    end
  end
  module Container
    class Unit < YGG::Container::Unit_Base
      include REI::Constants
      attr_accessor :battler
      attr_accessor :character
      def initialize character,battler
        super
        @gauges[GAUGE_WT] = REI::Handler::Gauge_WT.new self
      end
      def wt_rate
        @battler.wt_rate
      end
    end
  end
  module Character
  end
  module Sprite
  end
end

class REI::Character::Base
  def pre_init_members
  end
  alias :rei_pre_init_members :pre_init_members
  def pre_init_members
    rei_pre_init_members
    @character = REI::Container::Character.new
    @unit = REI::Container::Unit.new
  end
  attr_accessor :unit
  def update
  end
  def update_logic
  end
  def update_visual
  end
  def update_turn
  end
end
class REI::Character::Unit < REI::Character::Base

end
class REI::Character::Trap < REI::Character::Unit

end
require_relative 'REI_Character'
require_relative 'REI_Sprites'

require_relative 'REI_BattlerUnit'
require_relative 'REI_BattlerUnit(Ex)'
require_relative 'REI_BattlerUnit(Turn)'
require_relative 'REI_BattlerUnit(IQ)'
require_relative 'REI_BattlerUnit(AI)'
require_relative 'REI_BattlerUnit[final]'

require_relative 'REI_BattlerUnit_Actor'
require_relative 'REI_BattlerUnit_Enemy'
require_relative 'REI_BattlerUnit_Trap'

#require_relative 'addons'
