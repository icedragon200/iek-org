=begin
#-inject gen_class_header 'REI::Unit_Battler'
class REI::BattlerUnit < Game::Battler

  attr_accessor :name
  attr_accessor :nickname

  def initialize
    super
    @name = ""
    @nickname = ""
  end

  memoize_as character_name: "''", character_index: 0
  memoize_as face_name: "''", face_index: 0

end

#-inject gen_class_header 'REI::Unit_Battler::Entity'
class REI::BattlerUnit::Entity < REI::BattlerUnit

  attr_accessor :id

  def initialize id
    super()
    @id = id
  end

  def entity
    nil
  end

  def character_name
    entity.character_name
  end

  def character_index
    entity.character_index
  end

  def face_name
    entity.face_name
  end

  def face_index
    entity.face_index
  end

end

#-inject gen_class_header 'REI::Unit_Battler::Actor'
class REI::BattlerUnit::Actor < REI::BattlerUnit::Entity

  def entity
    $data_actors[@id]
  end

end
=end
