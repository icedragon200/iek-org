Icy-Engine-Katara (IEK)
=================

Bringing back all the classic and unreleased RPG Maker VX/VXA scripts under one roof

## Introduction
  Even if I fell off the RM Scene, its still a nice feeling to rewrite some of
  my old classic and favourite scripts.
  "pratice becomes a habit" my favourite instructor says that.
  Anyway by rewriting older scripts, you can better solve the problems you had
  back then much faster, and add new features and fixes to existing scripts,
  rather than "re-inventing the wheel".

## Features
### ICY
  The original engine, containing memorable scripts such
  as the HM Style Inventory, and Recruitment System

### IEX
  Possibly the most memorable and buggiest engine I ever made,
  despite the number of bugs and incompatablity, it opened a gateway
  to many other scripts.

### IEO
  Unreleased engine for VX shortly before VXA's release, it was cancelled
  and later resurrected for IEK. Contains rather complex and large
  systems, such as the multi-class System, Skill Levels, and craft system

### IEX2
  Meant to be a rewrite of the original IEX scripts the project hit
  a deadlock after being off the scene for too long.

### ISS
  A special set of scripts written for my RPG Maker VX project S.A.R.A,
  though these scripts are mostly small edits and patches for other
  scripts, they do have a few nifty ones.

### IER
  Cross engine scripts for VX and VXA, mostly done by requests.

### IEI
  A side project started for RPG Maker VX Ace, contains many
  experimental scripts.

### IEK
  Current and possibly the largest project to date.

## Script List
  Too much stuff to keep track of here.
